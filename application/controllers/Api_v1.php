<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 0");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization");

class Api_v1 extends CI_Controller {

	function __construct() {
        parent::__construct();
		
		// set default time zone -
		//date_default_timezone_set('Asia/Kolkata');
		
  		$this->load->database();
		
  		$this->load->helper('url');
		$this->load->helper('file');		
  		$this->load->library('session');
  		
    }
	
	public function index()
	{												
		$response = array("status" => 0, "message" => "");
		
		// check request type -
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			//echo "<pre>"; print_r($_SERVER); die();
			
			// get API authentication token from request header
			$token = getBearerToken();
				
			// check if authentication token
			if($token != null)
			{
				// validate API authentication token from request header
				if(validateToken($token))
				{
					$_POST = json_decode(file_get_contents("php://input"), TRUE);
					
					$res = array();
					$this->api_log($_POST, $res);
					
					//========= This version deprecated now on 31-03-2021 ===========//
					
					die("API version v1 is deprecated now.");
					
					//========= This version deprecated now on 31-03-2021 ===========//
					
					if(isset($_POST['action']) && $_POST['action'] != "")
					{
						// get tag value -
						$action = $_POST['action'];
						
						if($action == "login")
						{
							$this->login();
						}
						else if($action == "forgot_password")
						{
							$this->forgot_password();
						} 	
						else if($action == "verify_otp")
						{
							$this->verify_otp();
						} 
						else if($action == "change_password")
						{
							$this->change_password();
						} 
						else if($action == "logout")
						{
							$this->logout();
						} 
						else if($action == "get_district_list")
						{ 
							$this->get_district_list();
						}
						else if($action == "get_revenue_list")
						{ 
							$this->get_revenue_list(); 
						}
						
						else if($action == "get_survey_list")
						{ 
							$this->get_survey_list(); 
						}
						else if($action == "get_category_list")
						{ 
							$this->get_category_list(); 
						}
						else if($action == "get_all_block")
						{ 
							$this->get_all_block();
						}	
						else if($action == "get_all_panchayat")
						{ 
							$this->get_all_panchayat();
						}
						else if($action == "get_all_district_list")
						{ 
							$this->get_all_district_list();
						}
						else if($action == "get_all_revenue_list")
						{ 
							$this->get_all_revenue_list();
						}
						else if($action == "get_all_block_list")
						{ 
							$this->get_all_block_list();
						}						
						else if($action == "get_all_village_list")
						{ 
							$this->get_all_village_list();
						}
						else if($action == "survey_district_list")
						{ 
							$this->survey_district_list();
						}
						else if($action == "survey_revenue_circle_list")
						{ 
							$this->survey_revenue_circle_list();
						}
						else if($action == "survey_block_list")
						{ 
							$this->survey_block_list(); 
						}
						else if($action == "survey_panchayat_list")
						{ 
							$this->survey_panchayat_list(); 
						}	
						else if($action == "survey_village_list")
						{ 
							$this->survey_village_list(); 
						}	
						else if($action == "survey_questionire")
						{ 
							$this->survey_questionire();
						}
						else if($action == "survey_questionire_v1")
						{ 
							$this->survey_questionire_v1();
						}	
						else if($action == "save_response")
						{ 
							$this->save_response();
						}
						else if($action == "survey_statistics")
						{ 
							$this->survey_statistics();
						}	
						else
						{
							$response['status']		= "0";
							$response['message'] 	= "Invalid request method.";
							
							$this->response($response);
						}
					}
					else
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Request can not be null.";
						
						$this->response($response);
					}
				}
				else
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Invalid Api Key.";
					
					$this->response($response);
				}
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "API authentication missing.";
				
				$this->response($response);
			}
		}
		else
		{
			$response['status'] 	= "0";
			$response['message'] 	= "Invalid request type. Must use Request type as POST.";
			
			$this->response($response);
		}
	}
	
	
	// function to login user
	public function login()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$username	= isset($_POST['username']) ? $_POST['username'] : '';
		$password 	= isset($_POST['password']) ? $_POST['password'] : '';
	
		if(empty($username) || empty($password))
		{
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Fields can not be blank.";
		}
		else
		{
			// encrypt password using sha1() -
			$password = md5($password);
			//surveyor_id
			$role_id = $this->config->item('surveyor_role_id');
			// check login username and password -
			$query = $this->db->query("SELECT first_name, last_name, user_id, session_token, username, email, role_id, district_id, revenue_id, status  FROM survey_users WHERE role_id= '$role_id' AND username = '$username' AND password = '$password' AND is_deleted = '0'");
			$cnt = $query->num_rows();
			//echo $this->db->last_query();
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					if($row['status'] == 'Active')
					{
						if($row['role_id'] == 999){
							$fullname = "Super Admin";
						} else {
							$fullname = ucfirst($row['first_name'])." ".ucfirst($row['last_name']);
						}
						
						$user_id = $row['user_id'];
						$edited_date = $last_login_on = date("Y-m-d H:i:s");

						$sql_update = "UPDATE survey_users SET is_login = '1', last_login_on = '".$last_login_on."', updated_on = '".$edited_date."' WHERE user_id = '".$user_id."'";
						$query_update = $this->db->query($sql_update);
						
						$session_token = generateSessionToken();
						setUserSessionToken($session_token, $user_id);
						//$data = array('session_token' => $session_token, 'username' => $row['username']);
						$data = array('session_token' => $session_token, 'username' => $row['username'], 'fullname' => $fullname, 'email' => $row['email'], 'role_id' => $row['role_id'], 'district_id' => $row['district_id'], 'revenue_id' => $row['revenue_id'], 'user_id' => $row['user_id']);
												
						//$response['data'] = array('user_id' => $row['user_id'], 'username' => $row['username'], 'fullname' => $fullname, 'email' => $row['email'], 'role_id' => $row['role_id']);
						
						$response['status'] 		= "1";
						$response['message'] 	= "Login Successfully.";
						$response['data'] 			= $data;
					}
					else
					{
						$response['status'] 	= "0";
						$response['message'] 	= "User status is Inactive.";
					}
				}			
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "Invalid Username or Password.";
			}
		}
		
		$this->response($response);
	}
	
	
	public function survey_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} else if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} else {
			
			
			$education_survey_id = $this->config->item('education_survey_id');
			
			if($survey_id == $education_survey_id) 
			{
				
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
				$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE status = 'Active' AND panchayat_id IN('".$panchayatArr."')");
				
				$countRes = $query_1->num_rows();
				
				if($countRes > 0)
				{ 
					$blockList = array();
					foreach($query_1->result_array() as $row)
					{
						array_push($blockList, $row['block_id']);
					}
				}
						
				$blockArr = "" . implode( "','", $blockList ) . "";
				
				
				$query_2 = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE status = 'Active' AND block_id IN('".$blockArr."')");
				
				$countDistrict = $query_2->num_rows();
				
				if($countDistrict > 0)
				{ 
					$districtList = array();
					foreach($query_2->result_array() as $row)
					{
						
						array_push($districtList, $row['district_id']);
					}
				}
			
				// Get District Result 				
				$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
				$disRow = $surveyDistrict->num_rows();			
				$dList = "";
				if($disRow > 0)
				{ 
					//$dList = array();
					
					foreach($surveyDistrict->result_array() as $row)
					{
						//array_push($dList, $row['district_id']);
						$dList = $row['district_id'];
					}
				}
				
				$user_district_id = explode(",",$dList);	
				
				$commonDistrict = array_intersect($user_district_id, $districtList);			
				
				
				$outputString = "" . implode( "','", $commonDistrict ) . "";				
				$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND district_id IN('".$outputString."')");
				$cnt = $query->num_rows();
				
			} 
			else 
			{
				
				$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $surveyDistrict->num_rows();			
				
				if($cntRow > 0)
				{ 
					$districtList = array();
					foreach($surveyDistrict->result_array() as $row)
					{
						array_push($districtList, $row['district_id']);
					}

					
				}
				
				$district_result = explode(",",$districtList[0]);
				
				$outputString = "" . implode( "','", $district_result ) . "";
				
				$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND district_id IN('".$outputString."')");
				$cnt = $query->num_rows();
				
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	
	public function survey_revenue_circle_list() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} else {
			
			$surveyRevenue = $this->db->query("SELECT revenue_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
			$cntRow = $surveyRevenue->num_rows();			
			
			if($cntRow > 0)
			{ 
				$revenueList = array();
				foreach($surveyRevenue->result_array() as $row)
				{
					array_push($revenueList, $row['revenue_id']);
				}

				
			}
			
			$revenue_result = explode(",",$revenueList[0]);
			
			$outputString = "" . implode( "','", $revenue_result ) . "";
			
			$query = $this->db->query("SELECT revenue_id, revenue_code, revenue_name  FROM survey_revenue_master WHERE status = 'Active' AND revenue_id IN('".$outputString."')");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'revenue_name' => $row['revenue_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Revenue Circle List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	
	// function to Get District List -
	public function get_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$state_id	= isset($_POST['state_id']) ? $_POST['state_id'] : '';
		
		if(empty($state_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State name is required.";
			
		} else {
			
			$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND state_id = '".$state_id."'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	
	// function to Get All District List -
	public function get_all_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if($survey_id == $education_survey_id) 
		{ 
			$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
			$cntRow = $villageMaster->num_rows();
			
			if($cntRow > 0)
			{ 
				$panchyatList = array();
				foreach($villageMaster->result_array() as $row)
				{
					array_push($panchyatList, $row['panchayat_id']);
				}
			}				
			
			$panchayatArr = "" . implode( "','", $panchyatList ) . "";
			
			$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
			
			$countRes = $query_1->num_rows();
			
			if($countRes > 0)
			{ 
				$blockList = array();
				foreach($query_1->result_array() as $row)
				{
					array_push($blockList, $row['block_id']);
				}
			}
					
			$blockArr = "" . implode( "','", $blockList ) . "";
			$query_2 = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE block_id IN('".$blockArr."')");
			
			$countDistrict = $query_2->num_rows();			
			if($countDistrict > 0)
			{ 
				$districtList = array();
				foreach($query_2->result_array() as $row)
				{
					
					array_push($districtList, $row['district_id']);
				}
			}
			
			$outputString = "" . implode( "','", $districtList ) . "";				
			$query = $this->db->query("SELECT district_id, district_name, status, is_deleted  FROM survey_district_master WHERE district_id IN('".$outputString."')");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No data Found.";
			}
			
		}
		else 
		{
			
			$query = $this->db->query("SELECT * FROM survey_district_master");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name'], 'state_id' => $row['state_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}		
		
		$this->response($response);
	}
	
	// function to Get Revenue Circle List
	public function get_revenue_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		if(empty($district_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "District name is required.";
			
		} else {
			
			$query = $this->db->query("SELECT revenue_id, revenue_code, revenue_name, district_id FROM survey_revenue_master WHERE status = 'Active' AND district_id = '".$district_id."'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'revenue_name' => $row['revenue_name'], 'district_id' => $row['district_id']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Revenue List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
			
		}
		
		$this->response($response);
	}
	
	// function to Get All Revenue Circle List
	public function get_all_revenue_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT revenue_id,revenue_code,district_id,revenue_name  FROM survey_revenue_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'district_id' => $row['district_id'], 'revenue_name' => $row['revenue_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
			}
			
			$response['status'] 	= "1";
			$response['message'] 	= "Revenue List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	
	// function to Get Development Block List
	public function survey_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} else if(empty($district_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "District name is required.";
			
		} else {
			
			$education_survey_id = $this->config->item('education_survey_id');
			if($survey_id == $education_survey_id) 
			{
				
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
				$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE status = 'Active' AND panchayat_id IN('".$panchayatArr."')");
				
				$countRes = $query_1->num_rows();
				
				if($countRes > 0)
				{ 
					$blockList = array();
					foreach($query_1->result_array() as $row)
					{
						array_push($blockList, $row['block_id']);
					}
				}
						
				$blockArr = "" . implode( "','", $blockList ) . "";
				
				
				$query = $this->db->query("SELECT * FROM survey_block_master WHERE status = 'Active' AND district_id = '".$district_id."' AND block_id IN('".$blockArr."')");
				$cnt = $query->num_rows();
				//echo $this->db->last_query();
			}
			else 
			{
				
				$query = $this->db->query("SELECT * FROM survey_block_master WHERE status = 'Active' AND district_id = '".$district_id."'");
				$cnt = $query->num_rows();
				
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
				}
				if($survey_id != $education_survey_id) 
				{
					$response['data'][] = array('block_id' => 'Other', 'block_name' => 'Other');
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Development block List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	} 

	
	// function to Get All Development Block List
	public function get_all_development_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT block_id,block_name,district_id  FROM survey_block_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
			}
			$response['data'][] = array('block_id' => 'Other', 'block_name' => 'Other');
			
			$response['status'] 	= "1";
			$response['message'] 	= "Development block List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	} 
	
	
	// Get All Block
	public function get_all_block()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		//$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
					$cntRow = $villageMaster->num_rows();
					
					if($cntRow > 0)
					{ 
						$panchyatList = array();
						foreach($villageMaster->result_array() as $row)
						{
							array_push($panchyatList, $row['panchayat_id']);
						}
					}				
					
					$panchayatArr = "" . implode( "','", $panchyatList ) . "";
					
					$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
					
					$countRes = $query_1->num_rows();
					
					if($countRes > 0)
					{ 
						$blockList = array();
						foreach($query_1->result_array() as $row)
						{
							array_push($blockList, $row['block_id']);
						}
					}
							
					$blockArr = "" . implode( "','", $blockList ) . "";
					
					
					$query = $this->db->query("SELECT * FROM survey_block_master WHERE block_id IN('".$blockArr."')");
					$cnt = $query->num_rows();
					//echo $this->db->last_query();
				
			}
			else 
			{
				$query = $this->db->query("SELECT *  FROM survey_block_master");
				$cnt = $query->num_rows();
			}	
				
				
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
			
				$response['status'] 	= "1";
				$response['message'] 	= "Development block List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
			
		}
		
		$this->response($response);
	}
	
	
	// Get All Block
	public function get_all_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT district_id,block_name,block_id FROM survey_block_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
			}
		
			$response['status'] 	= "1";
			$response['message'] 	= "Development block List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	
	// Get All Panchayat
	public function get_all_panchayat()
	{
		$response = array("status" => 0, "message" => "");
		
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
				$query = $this->db->query("SELECT * FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
				$cnt = $query->num_rows();
				//echo $this->db->last_query();
				
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_panchayat_master");
				$cnt = $query->num_rows();
			}			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('panchayat_id' => $row['panchayat_id'], 'block_id' => $row['block_id'], 'panchayat_name' => $row['panchayat_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Panchayat-Gaon List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}		
		
		$this->response($response);
	}
	
	// function to Get Panchayat/Gaon List -
	public function survey_panchayat_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$block_id	= isset($_POST['block_id']) ? $_POST['block_id'] : '';
		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey name is required.";
			
		} else if(empty($block_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Block name is required.";
			
		} else {
			
			$education_survey_id = $this->config->item('education_survey_id');
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
				$query = $this->db->query("SELECT panchayat_id, block_id, panchayat_name  FROM survey_panchayat_master WHERE status = 'Active' AND block_id = '".$block_id."' AND panchayat_id IN('".$panchayatArr."')");
				$cnt = $query->num_rows();
				//echo $this->db->last_query();
				
			}
			else 
			{
				$query = $this->db->query("SELECT panchayat_id, block_id, panchayat_name  FROM survey_panchayat_master WHERE status = 'Active' AND block_id = '".$block_id."'");
				$cnt = $query->num_rows();
			}
			
			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('panchayat_id' => $row['panchayat_id'], 'block_id' => $row['block_id'], 'panchayat_name' => $row['panchayat_name']);
				}
				
				if($survey_id != $education_survey_id) 
				{
					$response['data'][] = array('panchayat_id' => 'Other',  'block_id' => $row['block_id'], 'panchayat_name' => 'Other');
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Panchayat-Gaon List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get Village List
	public function survey_village_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$revenue_id	= isset($_POST['revenue_id']) ? $_POST['revenue_id'] : '';
		
		// get POST data -
		$panchayat_id	= isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		if(empty($revenue_id) && $survey_id != $education_survey_id){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Revenue circle name is required.";
			
		} else if(empty($panchayat_id) && $survey_id == $education_survey_id){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Gaon/Panchyat name is required.";
			
		}  else {
			
			if($survey_id == $education_survey_id) 
			{ 
				$query = $this->db->query("SELECT village_id, village_code, village_name  FROM survey_village_master WHERE panchayat_id = '".$panchayat_id."' AND status = 'Active' AND is_deleted = '0'");
				$cnt = $query->num_rows();
				
			}
			else 
			{	
				$query = $this->db->query("SELECT village_id, village_name, village_code FROM survey_village_master WHERE status = 'Active' AND revenue_id = '".$revenue_id."' AND is_deleted = '0'");
				$cnt = $query->num_rows();
			}
			
			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('village_id' => $row['village_id'], 'village_code' => $row['village_code'],  'village_name' => $row['village_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Village List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	
	// function to Get All Village List
	public function get_all_village_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT * FROM survey_village_master WHERE panchayat_id > 0");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						$response['data'][] = array('village_id' => $row['village_id'], 'village_code' => $row['village_code'],  'village_name' => $row['village_name'], 'panchayat_id' => $row['panchayat_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
					}
					
					$response['status'] 	= "1";
					$response['message'] 	= "Village List Get Successfully.";
				}	
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_village_master");
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{
						$response['data'][] = array('village_id' => $row['village_id'], 'revenue_id' => $row['revenue_id'], 'village_code' => $row['village_code'],  'village_name' => $row['village_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
					}
					
					$response['status'] 	= "1";
					$response['message'] 	= "Village List Get Successfully.";
				}
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
				
			}
			
		}		
		
		$this->response($response);
	}

	// function to Get Survey Category List
	public function get_category_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT category_name, category_id FROM survey_category_master WHERE status = 'Active' AND is_deleted = '0'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('category_name' => $row['category_name'], 'category_id' => $row['category_id']);
			}
			
			$response['status'] 	= "1";
			$response['message'] 	= "Survey Category List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	

	// function to Get Survey List
	public function get_survey_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		/*if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "All fields are mandatory.";
			
		} else {*/			
			if(!empty($user_id) && is_user_login($user_id))
            {
				
				$surveyData = $this->db->query("SELECT survey_id FROM survey_assign_users WHERE user_id = '".$user_id."' AND status != 'Draft' AND is_deleted = '0'");
				$cntRow = $surveyData->num_rows();			
				
				if($cntRow > 0)
				{
					$surveyList = array();
					foreach($surveyData->result_array() as $row)
					{
						array_push($surveyList, $row['survey_id']);
					}

					
				}
				
				$outputString = "" . implode( "','", $surveyList ) . "";
				$query = $this->db->query("SELECT survey_master.survey_id, survey_master.user_id, survey_master.category_id,  survey_category_master.category_name, survey_master.description, survey_master.title, survey_master.user_id FROM survey_master LEFT JOIN survey_category_master ON survey_master.category_id = survey_category_master.category_id 
				WHERE survey_master.survey_id IN('".$outputString."') AND survey_master.status != 'Draft' AND survey_master.is_deleted = '0'");
				
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					$arrayList = array();
					foreach($query->result_array() as $row)
					{
						
						array_push($arrayList, $row['survey_id']);
						
						$data[] = array('survey_id' => $row['survey_id'], 'user_id' => $row['user_id'], 'category_id' => $row['category_id'],  'category_name' => $row['category_name'],  'survey_title' => $row['title'],  'survey_description' => $row['description']);
					}
					
					//print_r($arrayList);
					$response['status'] 	= "1";
					$response['message'] 	= "Survey List Get Successfully.";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					$response['message'] 	= "No data Found.";
				}
			}
			else
			{
				$response['status']		= "0";
				$response['message']    = "User is not login.";
			}
		//}
		
		$this->response($response);
	}	
	
	// function to Get Survey Details
	public function get_survey_details()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey Title is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT survey_listing.survey_id, survey_listing.user_id, survey_listing.category_id, survey_category_master.category_name, survey_listing.description, survey_listing.title, survey_listing.user_id FROM survey_listing LEFT JOIN survey_category_master ON survey_listing.category_id = survey_category_master.category_id 
			WHERE survey_listing.status = 'Publish' AND survey_listing.survey_id = '".$survey_id."' AND is_deleted = '0'");
			
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('survey_id' => $row['survey_id'], 'user_id' => $row['user_id'], 'category_id' => $row['category_id'],  'category_name' => $row['category_name'],  'title' => $row['title'],  'description' => $row['description']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Survey Details Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// Forgot Password
	public function forgot_password(){
		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$email_id	= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		
		if(empty($email_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Email Address is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT first_name, last_name, username, contact_no, email, status, is_deleted FROM survey_users WHERE email = '".$email_id."'");
			
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					
					if($row['is_deleted'] == 1){
						
						$response['status'] 	= "0";
						$response['message'] 	= "User is deleted. Please contact to your administrator.";
						
					} else {
							
							$random_number = mt_rand(100000,999999);
							
							$fullname = ucfirst($row['first_name'])." ".ucfirst($row['last_name']);
							$username = $row['username'];
							$email_id = $row['email'];	
							$state_code = md5(time()."+".$email_id);
							
							$check_exist = $this->master_model->getRecords("verify_otp", array("email_id" => $email_id));
							if(count($check_exist) > 0){
								$values = '"'.$email_id.'"';	
								$updateArr = array('random_str_1' => $state_code, 'random_str_2' => '');			
								$updateQuery = $this->master_model->updateRecord('verify_otp',$updateArr,array('email_id' => $values));
							} else {								
								$insertData = array('verify_code' => $random_number, 'email_id' => $email_id, 'random_str_1' => $state_code);
								$this->master_model->insertRecord('verify_otp',$insertData);							
							}	
								
							//$this->send_forgot_password_email($fullname, $username, $random_number, $email_id);
							
							//$data = array('state_code' => $state_code);
							
							$response['status'] 	= "1";
							$response['message'] 	= "Forgot Password Email Sent Successfully.";
							$response['state_code']	= $state_code;						
					}	
				}
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
		
	}
	
	
	// Send an Forgot Email To Register User 
	public function send_forgot_password_email($fullname, $username, $random_number, $email_id)
	{	
		//echo $random_number;die();
		$slug = 'user_forgot_password';
		//survey_verify_otp
		$email_body = $this->master_model->getRecords("email_master", array("slug" => $slug));		
		if(count($email_body) > 0){			
			$from_admin = $email_body[0]['from_email'];
			$email_content = $email_body[0]['email_body'];
			$email_subject = $email_body[0]['email_subject'];
			$url = base_url('xAdmin/admin');
			$arr_words 	= ['[FULLNAME]', '[CODE]', '[EMAIL]'];
			$rep_array 	= [$fullname, $random_number, $email_id];		
			$content 	= str_replace($arr_words, $rep_array, $email_content);
			
			/*$contentArray = array(
									'to'			=>	$email,//$email
									'to_name'		=>	$fullname,	
									'cc'			=>	'',
									'from_email'	=>	$from_admin,
									'from_name'		=>	'UNDP Survey',
									'subject'		=> 	$email_subject,
									'description' 	=> 	$content,
									'view'          => 'common-file'	
								);
			$emailsend 	= 	$this->emailsending->sendmail_phpmailer($contentArray);*/
			
			//email admin sending code 
			$contentArray=array(
				'to'		=>	'vicky.kuwar@esds.co.in', //$email_id,					
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$email_subject,
				'view'		=>  'common-file'
				);
			
			$other_info=array('content'=>$content);
			
			
			$emailsend 	= 	$this->emailsending->sendmail($contentArray,$other_info);
			if($emailsend){
				return true;
			}else {
				return false;
			}
		}
	} // End Function	
	
	// OTP Verification
	public function verify_otp()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$email_id	= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		$code		= isset($_POST['code']) ? $_POST['code'] : '';
		$state_code	= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		//print_r($_POST);
		if(empty($email_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Email address is required.";
			
		} else if(empty($code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Verification code is required.";
			
		} else if(empty($state_code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State code is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT verify_code, status, email_id, random_str_1 FROM survey_verify_otp WHERE random_str_1 = '".$state_code."'");		
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{						
					if($row['email_id'] != $email_id)
					{							
						$response['status'] 	= "0";
						$response['message'] 	= "Enter email id is wrong.";
					}
					else if($row['verify_code'] != $code)
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Enter verification code is wrong.";
					}
					else if($row['random_str_1'] != $state_code)
					{
						$response['status'] 	= "0";
						$response['message'] 	= "State code is wrong.";
					}
					else 
					{
					
						if($row['email_id'] == $email_id && $row['verify_code'] == $code && $row['random_str_1'] == $state_code)
						{
							$state_code2 = md5(time()."+".$email_id);
							//$data = array("is_otp_verified" => 'TRUE', 'state_code' => $state_code2);
							$response['status'] 		= "1";
							$response['message'] 		= "OTP successfully verify.";
							$response['is_otp_verified']= 'TRUE';
							$response['random_str_1'] 	= $state_code2;
							
							$values = '"'.$email_id.'"';
							
							// UPDATE STATUS
							$updateArr = array('random_str_2' => $state_code2);			
							$updateQuery = $this->master_model->updateRecord('verify_otp',$updateArr,array('email_id' => $values));
							
						}
						else
						{
							$response['status'] 	= "0";
							$response['message'] 	= "Somthing wrong. Please try again.";
						}
					
					}					
				}
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
			
		}
		
		$this->response($response);
		
	} // End Of Function Verify OTP
	
	// Update Password 
	public function change_password(){
		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$password		= isset($_POST['password']) ? $_POST['password'] : '';
		$cnfpassword	= isset($_POST['cnfpassword']) ? $_POST['cnfpassword'] : '';
		$email_id		= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		$state_code		= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		
		$uppercase = preg_match('@[A-Z]@', $password);
		$lowercase = preg_match('@[a-z]@', $password);
		$number    = preg_match('@[0-9]@', $password);
		$specialChars = preg_match('@[^\w]@', $password);
		
		if(empty($password)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password is required.";
			
		} else if(empty($cnfpassword)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Confirm Password is required.";
			
		} else if(empty($state_code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State code is required.";
			
		} else if($password != $cnfpassword){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password & Confirm Password does not match.";
			
		} else if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 6){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password should be at least 6 characters in length and should include at least one upper case letter, one number, and one special character.";
			
		} else {

			$stateC = $this->db->query("SELECT * FROM survey_verify_otp WHERE random_str_2 = '".$state_code."'");
			$cnt_1 = $stateC->num_rows();
			
			if($cnt_1 > 0)
			{
				//$query = $this->db->query("SELECT * FROM survey_users WHERE email = '".$email_id."'");
				$query = $this->db->query("SELECT * FROM survey_verify_otp WHERE random_str_2 = '".$state_code."' AND email_id = '".$email_id."'");	
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{						
						$values = '"'.$email_id.'"';
						// UPDATE STATUS
						$updateArr = array('password' => md5($password));			
						$updateQuery = $this->master_model->updateRecord('users',$updateArr,array('email' => $values));
						
						$response['status'] 	= "1";
						$response['message'] 	= "Password successfully updated.";							
					}
				}
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
				
			} 
			else 		
			{
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "State code is invalid.";
			}
			
		}
		
		$this->response($response);
		
	}
	
	// Get Option Dependent Question 10thFeb2021
	public function getOptionbaseQuestionMultiple($question_id, $dependant_ques_id, $id, $option)
	{ 
		//ob_start();		
			
		$queryQuestion = $this->db->query("SELECT question_id,question_text,response_type_id,is_mandatory,parent_question_id,salutation,input_more_than_1,if_file_required,file_label, input_file_required,if_summation,help_label FROM survey_question_master WHERE question_id = '".$dependant_ques_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			$in_arr = array();
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			} 
			 // get the first row
			$in_arr['question_id'] 			= $questionData->question_id; 
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
			//$in_arr['salutation'] 			= $questionData->salutation;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['if_file_required']		= 'No';
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_summation']			= 'No';
			
			if($questionData->if_file_required == 'Yes')
			{
				$in_arr['if_file_required'] 	= $questionData->if_file_required;
				$in_arr['file_label'] 			= $questionData->file_label;
			}
			
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}	
			
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT *  FROM survey_questions_options_master WHERE question_id = '".$questionData->question_id."' AND is_deleted='0'");
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="")
					{
						array_push($optArr_1, $opt_1['option']);
					}
					
					
					/*************************** Multiple Option Dependent Question Data Start *********************************/
				
						$quetionOptionMultipleQuestion = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$questionData->question_id."' AND ques_option_id='".$opt_1['ques_option_id']."' AND is_deleted='0'");
						$multipleQuestionCnt = $quetionOptionMultipleQuestion->num_rows();
						if($multipleQuestionCnt > 0)
						{
							foreach($quetionOptionMultipleQuestion->result_array() as $multipleQues)
							{
								$queryMultipleQuestion = $this->db->query("SELECT * FROM survey_questions_options_master WHERE ques_option_id = '".$multipleQues['ques_option_id']."' AND is_deleted = '0'");
								$questionMultipleData = $queryMultipleQuestion->row();
								$getOption = $questionMultipleData->option;
								
								// Below Code for multiple level array questions
								$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);								
								//$in_arr['dependent_question_options'] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);
							}
							
						}
				
					/************************* Multiple Option Dependent Question Data Start ***********************************/
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}
					//$in_arr['validation_id'] 	= $opt_1['min_value'];
					//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}

				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
				//$in_arr['options'] = json_encode($optArr_1);					
			}
			
		}
		
		$dataArr = array("option" => $option, "option_dependent_question" => $in_arr); 		
		//return json_encode($dataArr);
		return $dataArr;
	} // Function End
	
	
	public function getSurveySectionList($survey_id)
	{
			$result = array();
			
			$surveySectionList = $this->db->query("SELECT DISTINCT `survey_section_questions`.`section_id`, `survey_section_master`.`section_name`, `survey_section_master`.`section_desc` FROM `survey_section_questions` JOIN `survey_section_master` ON `survey_section_master`.`section_id` = `survey_section_questions`.`section_id` WHERE `survey_section_questions`.`survey_id` = '".$survey_id."' AND `survey_section_questions`.`is_deleted` = 0 AND `survey_section_master`.`is_deleted` = 0 ORDER BY `survey_section_questions`.`sort_order` ASC");					
			//echo $this->db->last_query();
			$surveyCount = $surveySectionList->num_rows();
			
			if($surveyCount > 0)
			{		
				foreach($surveySectionList->result_array()  as $res) 
				{
					$result[] = array("section_id" => $res['section_id'], "section_name" => $res['section_name'], "section_description" => $res['section_desc']);
				}
			}
			
			return $result;
				
	}
	
	
	// Survey Question Listing 07 March 2021
	public function survey_questionire_v1()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// Get Survey ID
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey name is required.";
			
		} 
		else 
		{
				// Get Survey Details 
				$response['status'] 		= "1";
				$response['message'] 		= "Data successfully received";	
				
				$surveyData = $this->db->query("SELECT * FROM survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
				$surveyResult = $surveyData->row();			
				$response['survey_id'] 			= $surveyResult->survey_id;
				$response['survey_title'] 		= $surveyResult->title; 
				$response['survey_description'] = $surveyResult->description;
			
				// Ask Section Question Table For Section & Sub Section			
				$surveyQuestionList = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$survey_id."'  AND status = 'Active' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
				//echo $this->db->last_query();
				$sectionQuesitonCount = $surveyQuestionList->num_rows();
				
				if($sectionQuesitonCount > 0)
				{				
					$section_question = array();
					
					$surveySections = $this->getSurveySectionList($survey_id);
					
					foreach($surveyQuestionList->result_array() as $res)
					{	
						
						$querySection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id = '".$res['section_id']."' AND parent_section_id = 0 AND is_deleted = '0' AND status = 'Active'");
						$sectionData = $querySection->row();	
						//echo $this->db->last_query();
						$sectionArr = array();
						
						if($sectionData->section_id!="") 
						{
							$section_id 			= $sectionData->section_id;	
							$section_name 			= $sectionData->section_name; 
							$section_description 	= $sectionData->section_desc;
						}

						$sub_section_id 			= '';	
						$sub_section_name 			= ''; 
						$sub_section_description 	= '';
						if($res['sub_section_id'] > 0)
						{							
							$querySubSection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id='".$res['sub_section_id']."' AND is_deleted = '0' AND status = 'Active'");
							//echo $this->db->last_query();
							$subSectionData = $querySubSection->row();	
							$sub_section_id 			= @$subSectionData->section_id;	
							$sub_section_name 			= @$subSectionData->section_name; 
							$sub_section_description 	= @$subSectionData->section_desc;
						}
						
						$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
						$if_parents_count = $if_checked_parent->num_rows();
						
						if($if_parents_count == 0)
						{
							
							$in_arr = array();
							// Global Question 
							$global_question_id 		= $res['global_question_id'];
							if($global_question_id > 0)
							{							
								$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
							}
							
							$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
							$questionData = $queryQuestion->row();	
							$explodeSub =  array();
							if($questionData->salutation!="")
							{
								$explodeSub = explode("|", $questionData->salutation);
							} 

							$in_arr['section_id'] 			= $section_id;
							$in_arr['section_name'] 		= $section_name;
							$in_arr['section_desc'] 		= $section_description;
							
							$in_arr['sub_section_id'] 		= $sub_section_id;
							$in_arr['sub_section_name'] 	= $sub_section_name;
							$in_arr['sub_section_des'] 		= $sub_section_description;
							
							$in_arr['question_id'] 			= $questionData->question_id;
							$in_arr['sort_order'] 			= $res['sort_order'];
							$in_arr['question_label'] 		= $questionData->question_text;
							$in_arr['response_type'] 		= $questionData->response_type_id;
							$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
							$in_arr['salutation'] 			= json_encode($explodeSub);
							$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
							$in_arr['question_help_label'] 	= $questionData->help_label;
							$in_arr['upload_file_required']	= 'No';
							$in_arr['if_summation']			= 'No';
							$in_arr['if_file_required']		= 'No';
							if($questionData->input_file_required == 'Yes')
							{
								$in_arr['upload_file_required'] 	= $questionData->input_file_required;
							}
							
							if($questionData->if_file_required == 'Yes')
							{
								$in_arr['if_file_required'] 	= $questionData->if_file_required;
								$in_arr['file_label'] 			= $questionData->file_label;
							}
							
							if($questionData->if_summation == 'Yes')
							{
								$in_arr['if_summation'] 		= $questionData->if_summation;
							}													
							
							// Get Sub Question List On Main Question 								
							$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
							$sub_questuon_count = $subQuestion->num_rows();
							
							if($sub_questuon_count > 0)
							{									
								foreach($subQuestion->result_array() as $opt)
								{
									$sub_arr = array();
									$sub_arr['question_id'] 		= $opt['question_id'];
									$explodeSub =  array();
									if($opt['salutation']!="")
									{
										$explodeSub = explode("|", $opt['salutation']);
									} 
									else 
									{
										$blank = array();
										$explodeSub = $blank;
									}
									
									$sub_arr['section_id'] 			= $section_id;
									$sub_arr['section_name'] 		= $section_name;
									$sub_arr['section_desc'] 		= $section_description;
									
									$sub_arr['sub_section_id'] 		= $sub_section_id;
									$sub_arr['sub_section_name'] 	= $sub_section_name;
									$sub_arr['sub_section_des'] 	= $sub_section_description;
									
									$sub_arr['question_id'] 		= $opt['question_id'];
									$sub_arr['question_label'] 		= $opt['question_text'];
									$sub_arr['response_type'] 		= $opt['response_type_id'];
									$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
									$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
									$sub_arr['question_help_label'] = $opt['help_label'];
									$sub_arr['salutation'] 			= json_encode($explodeSub);
									$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
									$sub_arr['upload_file_required']	= 'No';
									$sub_arr['if_summation']			= 'No';	
									$sub_arr['if_file_required']		= 'No';	
									if($questionData->input_file_required == 'Yes')
									{
										$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
									}
								}	// Sub Array End							
							
								$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
								$option_count = $quetionOptions->num_rows();	
								if($option_count > 0)
								{
									$optArr = array();										
									foreach($quetionOptions->result_array() as $optSub)
									{
										if($optSub['option']!="")
										{	
											array_push($optArr, $optSub['option']);	
										}		
											$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
											$multi_option_count = $queryMultipleCall->num_rows();
											if($multi_option_count > 0)
											{ 
												foreach($queryMultipleCall->result_array() as $dpenSub)
												{
													$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
												}
											}
										
										if($optSub['validation_id'] > 0)
										{
											$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
											
											if($optSub['sub_validation_id'] > 0)
											{
												$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
											}
										}
										else 
										{
											$in_arr['validation_type'] = '';
											$in_arr['validation_sub_type'] = '';
										}	
								
										$in_arr['min_value'] 		= $optSub['min_value'];
										$in_arr['max_value'] 		= $optSub['max_value'];
										$in_arr['validation_label']	= $optSub['validation_label'];	
									}						

									if(count($optArr) > 0)
									{
										$in_arr['options'] = json_encode($optArr);
									}
									else
									{
										$blank = array("");
										$in_arr['options'] = json_encode($blank);
										//$in_arr['options'] = '[]';
									}	
																	
								}
								
								$in_arr['sub_question'] = $sub_arr;							
								//$in_arr['sub_question'][] = $sub_arr;
								
							} // End Sub Question If
							
							
							// Get Parent Question Option 
							$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
							$option_count_1 = $quetionOptions_1->num_rows();	
							
							if($option_count_1 > 0)
							{
								$optArr_1 = array();
								foreach($quetionOptions_1->result_array() as $opt_1)
								{
									if($opt_1['option']!="")
									{
										array_push($optArr_1, $opt_1['option']);
									}									
									// Multiple Question Loop
									$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
									$multi_option_count = $queryMultipleCall->num_rows();
									if($multi_option_count > 0)
									{ 
										foreach($queryMultipleCall->result_array() as $dpenSub)
										{
											$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
										}
									}
									
							
									if($opt_1['validation_id'] > 0)
									{
										$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
										
										if($opt_1['sub_validation_id'] > 0)
										{
											$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
										}
									}
									else 
									{
										$in_arr['validation_type'] = '';
										$in_arr['validation_sub_type'] = '';
									}	
									
									$in_arr['min_value'] 		= $opt_1['min_value'];
									$in_arr['max_value'] 		= $opt_1['max_value'];
									$in_arr['validation_label']	= $opt_1['validation_label'];
								}										
								
								if(count($optArr_1) > 0)
								{
									$in_arr['options'] = json_encode($optArr_1);
								}
								else
								{
									$blank = array("");
									$in_arr['options'] = json_encode($blank);
									//$in_arr['options'] = '[]';
								}
								
							} // End Option If End							
							
							$section_question[]	= $in_arr;							
							
						} // Check End Related Option Dependent Has Parent				
						
					} // End Foreach Related Section Question
					
					$sectionArr['section_list']		= $surveySections;
					$sectionArr['section_question']	= $section_question;
					
				}			
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No Question Found";
				}
				
				
			$response['data']['sections'][]	= $sectionArr;
			
		}
		
		$this->response($response);
		
	}	
	
	// Survey Question Listing New 11Feb2021
	public function survey_questionire1()
	{
		ob_start();
		error_reporting(0);
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey name is required.";
			
		} 
		else 
		{
			$response['status'] 		= "1";
			$response['message'] 		= "Data successfully received";	
			
			$surveyData = $this->db->query("SELECT * FROM survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
			$surveyResult = $surveyData->row();
			
			
			$response['survey_id'] 			= $surveyResult->survey_id;
			$response['survey_title'] 		= $surveyResult->title; 
			$response['survey_description'] = $surveyResult->description; 
			
			
			//START : QUESTION To Survey			
			$querySurveyQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='0' AND sub_section_id='0' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
			
			$count = $querySurveyQuestion->num_rows();						
			
			if($count > 0)
			{		
				$surveyQuestion = array();
				foreach($querySurveyQuestion->result_array() as $res)
				{	
					$in_arr = array();
					
					$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
					$if_parents_count = $if_checked_parent->num_rows();
					
					if($if_parents_count == 0)
					{									
						$global_question_id 		= $res['global_question_id'];
						if($global_question_id > 0)
						{
							
							$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
						}
						
						$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
						$questionData = $queryQuestion->row();	
						$explodeSub =  array();
						if($questionData->salutation!="")
						{
							$explodeSub = explode("|", $questionData->salutation);
						} 	
						$in_arr['question_id'] 			= $questionData->question_id;
						$in_arr['question_label'] 		= $questionData->question_text;
						$in_arr['response_type'] 		= $questionData->response_type_id;
						$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
						//$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
						//$in_arr['salutation'] 			= $questionData->salutation;
						$in_arr['salutation'] 			= json_encode($explodeSub);
						$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
						$in_arr['question_help_label'] 	= $questionData->help_label;
						$in_arr['upload_file_required']	= 'No';
						$in_arr['if_summation']			= 'No';
						$in_arr['if_file_required']		= 'No';
						if($questionData->input_file_required == 'Yes')
						{
							$in_arr['upload_file_required'] 	= $questionData->input_file_required;
						}
						
						if($questionData->if_file_required == 'Yes')
						{
							$in_arr['if_file_required'] 	= $questionData->if_file_required;
							$in_arr['file_label'] 			= $questionData->file_label;
						}
						
						if($questionData->if_summation == 'Yes')
						{
							$in_arr['if_summation'] 		= $questionData->if_summation;
						}													
						
						// Get Sub Question List On Main Question 								
						$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
						$sub_questuon_count = $subQuestion->num_rows();
						
						if($sub_questuon_count > 0)
						{									
							foreach($subQuestion->result_array() as $opt)
							{
								$sub_arr = array();
								$sub_arr['question_id'] 		= $opt['question_id'];
								$explodeSub =  array();
								if($opt['salutation']!="")
								{
									$explodeSub = explode("|", $opt['salutation']);
								} 
								else 
								{
									$blank = array();
									$explodeSub = $blank;
								}
								$sub_arr['question_id'] 		= $opt['question_id'];
								$sub_arr['question_label'] 		= $opt['question_text'];
								$sub_arr['response_type'] 		= $opt['response_type_id'];
								$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
								$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
								//$sub_arr['salutation'] 			= $opt['salutation'];
								$sub_arr['question_help_label'] = $opt['help_label'];
								$sub_arr['salutation'] 			= json_encode($explodeSub);
								$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
								$sub_arr['upload_file_required']	= 'No';
								$sub_arr['if_summation']			= 'No';	
								$sub_arr['if_file_required']		= 'No';	
								if($questionData->input_file_required == 'Yes')
								{
									$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
								}
							}	// Sub Array End							
						
							$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
							$option_count = $quetionOptions->num_rows();	
							if($option_count > 0)
							{
								$optArr = array();										
								foreach($quetionOptions->result_array() as $optSub)
								{
									if($optSub['option']!="")
									{	
										array_push($optArr, $optSub['option']);	
									}		
										$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
											}
										}
									
									if($optSub['validation_id'] > 0)
									{
										$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
										
										if($optSub['sub_validation_id'] > 0)
										{
											$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
										}
									}
									else 
									{
										$in_arr['validation_type'] = '';
										$in_arr['validation_sub_type'] = '';
									}	
							
									$in_arr['min_value'] 		= $optSub['min_value'];
									$in_arr['max_value'] 		= $optSub['max_value'];
									$in_arr['validation_label']	= $optSub['validation_label'];	
								}						

								if(count($optArr) > 0)
								{
									$in_arr['options'] = json_encode($optArr);
								}
								else
								{
									$blank = array("");
									$in_arr['options'] = json_encode($blank);
									//$in_arr['options'] = '[]';
								}	
																
							}
							
							$in_arr['sub_question'] = $sub_arr;							
							//$in_arr['sub_question'][] = $sub_arr;
							
						} // If SubQuestion Attached End
						
						// Get Option 
						$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
						$option_count_1 = $quetionOptions_1->num_rows();	
						
						if($option_count_1 > 0)
						{
							$optArr_1 = array();
							foreach($quetionOptions_1->result_array() as $opt_1)
							{
								if($opt_1['option']!="")
								{
									array_push($optArr_1, $opt_1['option']);
								}									
								// Multiple Question Loop
								$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
								$multi_option_count = $queryMultipleCall->num_rows();
								if($multi_option_count > 0)
								{ 
									foreach($queryMultipleCall->result_array() as $dpenSub)
									{
										$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
									}
								}
								
						
								if($opt_1['validation_id'] > 0)
								{
									$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
									
									if($opt_1['sub_validation_id'] > 0)
									{
										$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
									}
								}
								else 
								{
									$in_arr['validation_type'] = '';
									$in_arr['validation_sub_type'] = '';
								}	
								//$in_arr['validation_id'] 	= $opt_1['validation_id'];
								//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
								$in_arr['min_value'] 		= $opt_1['min_value'];
								$in_arr['max_value'] 		= $opt_1['max_value'];
								$in_arr['validation_label']	= $opt_1['validation_label'];
							}										
							
							if(count($optArr_1) > 0)
							{
								$in_arr['options'] = json_encode($optArr_1);
							}
							else
							{
								$blank = array("");
								$in_arr['options'] = json_encode($blank);
								//$in_arr['options'] = '[]';
							}
							
							//$in_arr['options'] = json_encode($optArr_1);										
						}									
						
						$surveyQuestion[] = $in_arr;									
					}								
													
				} // Foreach End	
				
				$response['data']['survey_question']	= $surveyQuestion;
			} 
			else 
			{
				$response['data']['survey_question'][]	= array();
			}
						
			//END : QUESTION SECTION
			
			// Start Section Code 
			// If Section Assign To Survey
			$sectionQuery = $this->db->query("SELECT SM.section_id, SM.section_name, SM.section_desc, SM.parent_section_id, SSM.survey_id, SSM.title, SSM.description  
			FROM survey_section_master as SM LEFT JOIN survey_section_mapping as SS_M ON SM.section_id = SS_M.section_id
			LEFT JOIN survey_master as SSM ON SS_M.survey_id = SSM.survey_id			
			WHERE SSM.survey_id = '".$survey_id."' AND SM.parent_section_id= '0' AND SM.status = 'Active' AND SM.is_deleted = '0'");
			$numRowSection = $sectionQuery->num_rows();
			if(count($numRowSection) > 0)
			{
				foreach($sectionQuery->result_array() as $sectionValues )
				{
					$sectionArr = array();
					$sectionArr['section_id'] 			= $sectionValues['section_id'];					
					//$sectionArr['sub_section_id'] 		= $sectionValues['parent_section_id'];	
					$sectionArr['section_name'] 		= $sectionValues['section_name']; 
					$sectionArr['section_description'] 	= $sectionValues['section_desc'];
					
					// If Question Assign To Section
					$querySectionQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='".$sectionValues['section_id']."' AND sub_section_id='0' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
					//echo $this->db->last_query();
					$sectionQuesitoncount = $querySectionQuestion->num_rows();	
					
					if($sectionQuesitoncount > 0)
					{		
						$sectionQuestion = array();
						foreach($querySectionQuestion->result_array() as $res)
						{	
							$in_arr = array();
							//$in_arr['section_id'] 		= $res['section_id'];
							//$in_arr['sub_section_id'] 	= $res['sub_section_id'];
							//$in_arr['question_id'] 		= $res['question_id'];
							
							$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
							$if_parents_count = $if_checked_parent->num_rows();
							
							if($if_parents_count == 0)
							{									
								$global_question_id 		= $res['global_question_id'];
								if($global_question_id > 0)
								{	
									$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
								}
								
								$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
								$questionData = $queryQuestion->row();								
								$explodeSub =  array();
								if($questionData->salutation!="")
								{
									$explodeSub = explode("|", $questionData->salutation);
								} 
								$in_arr['question_id'] 			= $questionData->question_id;
								$in_arr['sort_order'] 			= $res['sort_order'];
								$in_arr['question_label'] 		= $questionData->question_text;
								$in_arr['response_type'] 		= $questionData->response_type_id;
								$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
								$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
								$in_arr['question_help_label'] 	= $questionData->help_label;
								$in_arr['salutation'] 			= json_encode($explodeSub);
								$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
								$in_arr['upload_file_required']	= 'No';
								$in_arr['if_summation']			= 'No';	
								$in_arr['if_file_required']		= 'No';		
								if($questionData->input_file_required == 'Yes')
								{
									$in_arr['upload_file_required'] 	= $questionData->input_file_required;
								}
								
								if($questionData->if_file_required == 'Yes')
								{
									$in_arr['if_file_required'] 	= $questionData->if_file_required;
									$in_arr['file_label'] 			= $questionData->file_label;
								}
								
								if($questionData->if_summation == 'Yes')
								{
									$in_arr['if_summation'] 		= $questionData->if_summation;
								}									
								
								// Get Sub Question List On Main Question 								
								$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
								$sub_questuon_count = $subQuestion->num_rows();
								
								if($sub_questuon_count > 0)
								{									
									foreach($subQuestion->result_array() as $opt)
									{
										$sub_arr = array();
										$explodeSub =  array();
										if($opt['salutation']!="")
										{
											$explodeSub = explode("|", $opt['salutation']);
										} 
										$sub_arr['question_id'] 		= $opt['question_id'];
										$sub_arr['sort_order'] 			= $res['sort_order'];
										$sub_arr['question_label'] 		= $opt['question_text'];
										$sub_arr['response_type'] 		= $opt['response_type_id'];
										$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
										$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
										$sub_arr['question_help_label'] = $opt['help_label'];
										//$sub_arr['salutation'] 			= $opt['salutation'];
										$sub_arr['salutation'] 			= json_encode($explodeSub);
										$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
										$sub_arr['upload_file_required']	= 'No';
										$sub_arr['if_summation']			= 'No';
										$sub_arr['if_file_required']		= 'No';			
										if($opt['input_file_required'] == 'Yes')
										{
											$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
										}
									}								
								
									$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
									$option_count = $quetionOptions->num_rows();	
									if($option_count > 0)
									{
										$optArr = array();										
										foreach($quetionOptions->result_array() as $optSub)
										{
												if($optSub['option']!="")
												{	
													array_push($optArr, $optSub['option']);	
												}	
												$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
												$multi_option_count = $queryMultipleCall->num_rows();
												if($multi_option_count > 0)
												{ 
													foreach($queryMultipleCall->result_array() as $dpenSub)
													{
														$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $dpenSub['option']);	
													}
												}
												
											if($optSub['validation_id'] > 0)
											{
												$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
												
												if($optSub['sub_validation_id'] > 0)
												{
													$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
												}
											}
											else 
											{
												$in_arr['validation_type'] = '';
												$in_arr['validation_sub_type'] = '';
											}												
											
											$in_arr['min_value'] 		= $optSub['min_value'];
											$in_arr['max_value'] 		= $optSub['max_value'];
											$in_arr['validation_label']	= $optSub['validation_label'];		
										}
										
										//$sub_arr['sub_question_options'] = json_encode($optArr);
										if(count($optArr) > 0)
										{
											$sub_arr['sub_question_options'] = json_encode($optArr);
										} 
										else 
										{
											$blank = array('');
											//$blank = array();
											$sub_arr['sub_question_options'] = json_encode($blank);
										}
																		
									}
									//$in_arr['sub_question'] = json_encode($sub_arr);
									$in_arr['sub_question'] = $sub_arr;
									//$in_arr['sub_question'][] = $sub_arr;
									
									
									
								} // If SubQuestion Attached End
								
								// Get Option 
								$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
								$option_count_1 = $quetionOptions_1->num_rows();	
								
								if($option_count_1 > 0)
								{
									$optArr_1 = array();
									foreach($quetionOptions_1->result_array() as $opt_1)
									{
										if($opt_1['option']!="")
										{
											array_push($optArr_1, $opt_1['option']); 
										}
										
										// Multiple Question Loop
										$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
											}
										}
										
										if($opt_1['validation_id'] > 0)
										{
											$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
											
											if($opt_1['sub_validation_id'] > 0)
											{
												$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
											}
										}
										else 
										{
											$in_arr['validation_type'] = '';
											$in_arr['validation_sub_type'] = '';
										}	
										
										$in_arr['min_value'] 		= $opt_1['min_value'];
										$in_arr['max_value'] 		= $opt_1['max_value'];
										$in_arr['validation_label']	= $opt_1['validation_label'];	
									}										
									
									//$in_arr['options'] = json_encode($optArr_1);
									if(count($optArr_1) > 0)
									{
										$in_arr['options'] = json_encode($optArr_1);
									}
									else
									{
										$blank = array("");
										$in_arr['options'] = json_encode($blank);
										//$in_arr['options'] = '[]';
									}	
								}									
								
								$sectionQuestion[] = $in_arr;									
							}								
															
						} // Foreach End	
						
						$sectionArr['section_question']	= $sectionQuestion;
					} 
					else 
					{
						$blank = array();
						$sectionArr['section_question']	= $blank;
					}
					

					// Sub Section Start 
					$subsectionQuery = $this->db->query("SELECT section_id, section_name, section_desc, parent_section_id 
					FROM survey_section_master 			
					WHERE parent_section_id = '".$sectionValues['section_id']."' AND status = 'Active' AND is_deleted = '0'");
					$numRowSubSection = $subsectionQuery->num_rows();
					
					$subSectionResponseArr = array();
					if(count($numRowSubSection) > 0)
					{
						
						foreach($subsectionQuery->result_array() as $subsectionValues )
						{
							$subsectionArr = array();
							$subsectionArr['section_id'] 			= $subsectionValues['parent_section_id'];
							$subsectionArr['sub_section_id'] 		= $subsectionValues['section_id']; 
							$subsectionArr['section_name'] 			= $subsectionValues['section_name']; 
							$subsectionArr['section_description'] 	= $subsectionValues['section_desc'];
							
							
							// Sub Section Question Built Start							
							$querysubSectionQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='".$sectionValues['section_id']."' AND sub_section_id='".$subsectionValues['section_id']."' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
						
							$subsectionQuesitoncount = $querysubSectionQuestion->num_rows();
						
							if($subsectionQuesitoncount > 0)
							{		
								$subsectionQuestion = array();
								foreach($querysubSectionQuestion->result_array() as $res)
								{	
									$in_arr = array();
									//$in_arr['section_id'] 		= $res['section_id'];
									//$in_arr['sub_section_id'] 	= $res['sub_section_id'];
									//$in_arr['question_id'] 		= $res['question_id'];
									
									$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
									$if_parents_count = $if_checked_parent->num_rows();
									
									if($if_parents_count == 0)
									{									
										$global_question_id 		= $res['global_question_id'];
										if($global_question_id > 0)
										{
											$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
										}
										
										$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
										$questionData = $queryQuestion->row();	

										$explodeSub =  array();
										if($questionData->salutation!="")
										{
											$explodeSub = explode("|", $questionData->salutation);
										} 
										
										$in_arr['question_id'] 			= $questionData->question_id;
										$in_arr['sort_order'] 			= $res['sort_order'];
										$in_arr['question_label'] 		= $questionData->question_text;
										$in_arr['response_type'] 		= $questionData->response_type_id;
										$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
										$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
										$in_arr['question_help_label'] 	= $questionData->help_label;
										//$in_arr['salutation'] 			= $questionData->salutation;
										$in_arr['salutation'] 			= json_encode($explodeSub);
										$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
										$in_arr['upload_file_required']	= 'No';
										$in_arr['if_summation']			= 'No';	
										$in_arr['if_file_required']		= 'No';			
										if($questionData->input_file_required == 'Yes')
										{
											$in_arr['upload_file_required'] 	= $questionData->input_file_required;
										}
										
										if($questionData->if_file_required == 'Yes')
										{
											$in_arr['if_file_required'] 	= $questionData->if_file_required;
											$in_arr['file_label'] 			= $questionData->file_label;
										}
										
										if($questionData->if_summation == 'Yes')
										{
											$in_arr['if_summation'] 		= $questionData->if_summation;
										}									
										
										// Get Sub Question List On Main Question 								
										$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
										$sub_questuon_count = $subQuestion->num_rows();
										
										if($sub_questuon_count > 0)
										{									
											foreach($subQuestion->result_array() as $opt)
											{
												$sub_arr = array();
												$explodeSub =  array();
												if($opt['salutation']!="")
												{
													$explodeSub = explode("|", $opt['salutation']);
												}  
												$sub_arr['question_id'] 		= $opt['question_id'];
												$sub_arr['sort_order'] 			= $res['sort_order'];
												$sub_arr['question_label'] 		= $opt['question_text'];
												$sub_arr['response_type'] 		= $opt['response_type_id'];
												$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
												$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
												$sub_arr['question_help_label'] = $opt['help_label'];
												//$sub_arr['salutation'] 			= $opt['salutation'];
												$sub_arr['salutation'] 			= json_encode($explodeSub);
												$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
												$sub_arr['upload_file_required']	= 'No';
												$sub_arr['if_summation']			= 'No';	
												$sub_arr['if_file_required']		= 'No';			
												if($opt['input_file_required'] == 'Yes')
												{
													$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
												}
												if($opt['if_summation'] == 'Yes')
												{
													$sub_arr['if_summation'] 		= $opt['if_summation'];
												}
											}								
										
											$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
											$option_count = $quetionOptions->num_rows();	
											if($option_count > 0)
											{
												$optArr = array();										
												foreach($quetionOptions->result_array() as $optSub)
												{
													if($optSub['option']!="")
													{	
														array_push($optArr, $optSub['option']);	
													}		
														$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
														$multi_option_count = $queryMultipleCall->num_rows();
														if($multi_option_count > 0)
														{ 
															foreach($queryMultipleCall->result_array() as $dpenSub)
															{
																$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
															}
														}
														
														if($optSub['validation_id'] > 0)
														{
															$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
															
															if($optSub['sub_validation_id'] > 0)
															{
																$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
															}
														}
														else 
														{
															$in_arr['validation_type'] = '';
															$in_arr['validation_sub_type'] = '';
														}	
														$in_arr['min_value'] 		= $optSub['min_value'];
														$in_arr['max_value'] 		= $optSub['max_value'];
														$in_arr['validation_label']	= $optSub['validation_label'];	
																							
												}
													
												$sub_arr['sub_question_options'] = json_encode($optArr);		
																				
											}
											
											$in_arr['sub_question'] = $sub_arr;
											//$in_arr['sub_question'][] = $sub_arr;
											
										} // If SubQuestion Attached End
										
										// Get Option 
										$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
										$option_count_1 = $quetionOptions_1->num_rows();	
										
										if($option_count_1 > 0)
										{
											$optArr_1 = array();
											foreach($quetionOptions_1->result_array() as $opt_1)
											{
												if($opt_1['option']!="")
												{
													array_push($optArr_1, $opt_1['option']); 
												}
												
												// Multiple Question Loop
												$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
												$multi_option_count = $queryMultipleCall->num_rows();
												if($multi_option_count > 0)
												{ 
													foreach($queryMultipleCall->result_array() as $dpenSub)
													{
														$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
													}
												}
												
													if($opt_1['validation_id'] > 0)
													{
														$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
														
														if($opt_1['sub_validation_id'] > 0)
														{
															$in_arr['validation_sub_type'] =$this->validation_sub_response_type($opt_1['sub_validation_id']);
														}
													}
													else 
													{
														$in_arr['validation_type'] = '';
														$in_arr['validation_sub_type'] = '';
													}								
													
													$in_arr['min_value'] 		= $opt_1['min_value'];
													$in_arr['max_value'] 		= $opt_1['max_value'];
													$in_arr['validation_label']	= $opt_1['validation_label'];	
												
											}										
											
											//$in_arr['options'] = json_encode($optArr_1);	
											if(count($optArr_1) > 0)
											{
												$in_arr['options'] = json_encode($optArr_1);
											}
											else
											{
												$blank = array("");
												$in_arr['options'] = json_encode($blank);
												//$in_arr['options'] = '[]';
											}	
										}									
										
										$subsectionQuestion[] = $in_arr;									
									}								
																	
								} // Foreach End	
								
								$subsectionArr['sub_section_question']	= $subsectionQuestion;
							} 
							else 
							{
								$blank = array("");
								$subsectionArr['sub_section_question']	= json_encode($blank);
								//$blank = array();
								//$subsectionArr['sub_section_question']	= $blank;
							}
							
							
							// Sub Section Question Built End
							
							$subSectionResponseArr[] = $subsectionArr;
						}
						
						
					}
					$sectionArr['sub_section'] = $subSectionResponseArr;
					
					
					// Sub Section End
					
					$response['data']['sections'][]	= $sectionArr;
				}
			}
			else 
			{
				$blank = array();				
				$response['data']['sections'][]	= $blank;
			}
			
			// End Section Code
		}
		
		$this->response($response);
		
	}	
	
	
	// gloabal question function 
	public function is_global_question($question_id, $global_question_id)
	{
		$in_arr = array();		
		$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$global_question_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			 // get the first row
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			}  
			$in_arr['question_id'] 			= $questionData->question_id;
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $question_id;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_file_required']		= 'No';	
			$in_arr['if_summation']			= 'No';								
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}

			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$global_question_id."' AND is_deleted='0'");
		
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="") 
					{
						array_push($optArr_1, $opt_1['option']); 
					}
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] =$this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}	
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}
				
				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
												
			}
			
		}
		
		$dataArr = array("question_id" => $question_id, "global_question" => $in_arr);	
		
		return $in_arr;
	}
	
	
	// Validation Response Type 
	public function validation_response_type($id)
	{
		$responseValiation = $this->db->query("SELECT validation_type FROM survey_response_validation_master WHERE validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$responseData = $responseValiation->row();	
		return $responseData->validation_type;
	}
	
	
	// Survey Statics
	public function survey_statistics() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(!empty($user_id) && is_user_login($user_id))
        {		
			if(empty($survey_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Survey ID is required";
				
			} else {

				// Draft Survey Count
				$draftSurvey = $this->db->query("SELECT count(response_id) AS DRAFT_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Draft' AND is_deleted='0'");
				//$draftCount = $draftSurvey->num_rows();
				$draftData 	= $draftSurvey->row();
				$draftCount = $draftData->DRAFT_SURVEY;	
				
				// Cancel Survey Count
				$cancelSurvey = $this->db->query("SELECT count(response_id) AS CANCEL_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Cancel' AND is_deleted='0'");
				//$cancelCount = $cancelSurvey->num_rows();
				$cancelData 	= $cancelSurvey->row();
				$cancelCount	= $cancelData->CANCEL_SURVEY;	
				
				// Close Survey Count
				$closeSurvey = $this->db->query("SELECT count(response_id) AS CLOSE_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Close' AND is_deleted='0'");
				//$closeCount = $closeSurvey->num_rows();
				$closeData 	= $closeSurvey->row();
				$closeCount	= $closeData->CLOSE_SURVEY;
				
				
				// Publish Survey Count
				$publishSurvey = $this->db->query("SELECT count(response_id) AS TOTAL_SUBMITTED_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Submitted' AND is_deleted='0'");
				//$publishCount = $publishSurvey->num_rows();
				$publishData 	= $publishSurvey->row();
				$publishCount	= $publishData->TOTAL_SUBMITTED_SURVEY;
				
				$response['status'] 	= "1";
				$response['message'] 	= "Statistic result received successfully.";
				$response['data'] 		= array("Draft" => $draftCount, "Cancel" => $cancelCount, "Close" => $closeCount, "Total Submitted" => $publishCount);
				
			}
		}
		else
		{
			$response['status']		= "0";
			$response['message']    = "User is not login.";
		}
		
		$this->response($response);
	}
	
	// Validation Sub Response Type 
	public function validation_sub_response_type($id)
	{
		$responseSubValiation = $this->db->query("SELECT validation_sb_type FROM survey_response_validation_subtype_master WHERE sub_validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$subresponseData = $responseSubValiation->row();	
		return $subresponseData->validation_sb_type;
	}
	
		
	// Function To get response 
	public function save_response()
	{
		$response = array("status" => 0, "message" => "");
		
		$surveyData = $_POST;
		error_reporting(0);
		
		/*ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);*/
		
		if(count($surveyData['data']) > 0)
		{
			$education_survey_id = $this->config->item('education_survey_id');
			$surveyer_id 		= $surveyData['data']['surveyer_id'];
			$survey_id 			= $surveyData['data']['survey_id'];
			$response_id 		= $surveyData['data']['responses'][0]['response_id'];
			$ref_id 			= $surveyData['data']['responses'][0]['ref_id'];
			$submitted_date 	= $surveyData['data']['responses'][0]['submitted_date'];
			$submited_time 		= $surveyData['data']['responses'][0]['submited_time'];
			$district_id 		= $surveyData['data']['responses'][0]['district_id'];
			
			$start_date_time    = date("Y-m-d H:i:s");
			$end_date_time      = date("Y-m-d H:i:s");	
			if($surveyData['data']['responses'][0]['start_date_time']!="")
			{
				$start_date_time	= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['start_date_time']));
			}
			
			if($surveyData['data']['responses'][0]['end_date_time']!="")
			{
				$end_date_time 		= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['end_date_time']));
			}
			
			
			if($survey_id == $education_survey_id)
			{
				$revenue_id 		= 0;
			}
			else 
			{
				$revenue_id 		= $surveyData['data']['responses'][0]['revenue_id'];
			}
			
			$block_id 			= $surveyData['data']['responses'][0]['block_id'];
			$panchayat_id 		= $surveyData['data']['responses'][0]['panchayat_id'];
			$village_id 		= $surveyData['data']['responses'][0]['village_id'];
			$survey_status 		= $surveyData['data']['responses'][0]['status'];
			$submitDate 		= date('Y-m-d', strtotime($submitted_date));
			// Check ID Exist related response id 
			$query = $this->db->query("SELECT survey_id FROM survey_master WHERE survey_id = '".$survey_id."' AND is_deleted = '0' AND status != 'Draft'");
			//echo $this->db->last_query();
			$surveyCnt = $query->num_rows();
			
			// Replace when implement token value 
			$surveyor_id = 1;
			if($surveyCnt > 0) 
			{ 
				// Check ID Exist related response id 
				$query = $this->db->query("SELECT * FROM survey_response_headers WHERE response_id = '".$response_id."' AND is_deleted = '0'");
				
				$cnt = $query->num_rows();
				
				if($cnt == '0') 
				{ 
					$insertArr = array( "ref_id" 	=> $ref_id, 
										"survey_id" => $survey_id,
										"surveyer_id" => $surveyer_id,
										"submitted_date" => $submitDate,
										"submited_time" => $submited_time,
										"district_id" => $district_id,
										"revenue_id" => $revenue_id,
										"block_id" => $block_id,
										"panchayat_id" => $panchayat_id,
										"village_id" => $village_id,
										"start_date" => $start_date_time,
										"end_date" => $end_date_time,
										"status" => $survey_status,	
										);
					$surveyQuery = $this->master_model->insertRecord('response_headers',$insertArr);
					$cid = $this->db->insert_id();	
					$updateFlag = false;	
				}
				else 
				{
					$updateArr = array( "survey_id" => $survey_id,
										"surveyer_id" => $surveyer_id,
										"submitted_date" => $submitDate,
										"submited_time" => $submited_time,
										"district_id" => $district_id,
										"revenue_id" => $revenue_id,
										"block_id" => $block_id,
										"panchayat_id" => $panchayat_id,
										"village_id" => $village_id,
										"end_date" => $end_date_time,
										"status" => $survey_status	
										);
					$updateQuery = $this->master_model->updateRecord('response_headers',$updateArr,array('survey_id' => $survey_id, 'surveyer_id' => $surveyer_id, "response_id" => $response_id));					
					$cid = $response_id;
					$updateFlag = true;	
				}
				
				
				// Section Array Check
				if(count($surveyData['data']['responses'][0]['sections']) > 0)
				{			
					
					foreach($surveyData['data']['responses'][0]['sections'] as $key => $sections)
					{
						
						$section_id 	= $sections['section_id'];
						$section_name 	= $sections['section_name'];
						$section_desc 	= $sections['section_description'];	
						
						if(count($sections['section_question']) > 0)
						{					
							foreach($sections['section_question'] as $key_val => $sectionQuestions)
							{							
								$question_id 		= $sectionQuestions['question_id'];
								$option_type	 	= $sectionQuestions['response_type'];
								$inputMoreThanOne 	= $sectionQuestions['input_more_than_1'];
								$input_file_required= $sectionQuestions['upload_file_required'];							
								$salutation 		= $sectionQuestions['salutation'];
								$answer				= $sectionQuestions['answer'];
								$options			= $sectionQuestions['options'];
								$if_file_required   = @$sectionQuestions['if_file_required'];
								$answer 			= @$sectionQuestions['answer'];
								$global_question 	= @$sectionQuestions['global_question'];
								$dependent_question = @$sectionQuestions['dependent_question_options'];
								$sub_question 		= @$sectionQuestions['sub_question'];
								
								if(is_array($global_question))
								{
									$g_question_id = $global_question['question_id'];
									$g_parent_question_id = $global_question['parent_question_id'];
									$g_response_type = $global_question['response_type'];
									
										if(is_array($global_question['answer']))
										{										
											if(count($global_question['answer']) > 0) 
											{											
												if($global_question['upload_file_required'] == 'Yes' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
												{	
													$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
													$image_name = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
													
													$img_global_name = '';										
													if($image_name!="") 
													{
														$random_name = $survey_id."_".$g_question_id."_".time();
														$img_global_name = $this->generateImage($random_name, $image_name);
													}
												
													foreach($global_question['answer']['value'] as $key => $value) 
													{												
														//Table Name : survey_response_headers
														
														$img_multiple_name = '';										
														if($value['upload_file']!="") 
														{
															$random_name = $survey_id."_".$g_question_id."_".time();
															$img_multiple_name = $this->generateImage($random_name, $value['upload_file']);
														}
														
														$insertArr = array("response_id" => $cid,
																			"question_id" => $g_question_id,
																			"key_name" => '',
																			"answer_value" => $value['value'],
																			"file_name" => $img_global_name,
																			"salultation_value" => $salutation_name,
																			"input_file_required" => $img_multiple_name,
																			"parent_question_id" => $g_parent_question_id);
																	
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
														}													
																									
													}
												} 
											

											if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
											{	
											
												$image_name = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";

												$img_global_name = '';										
												if($image_name!="") 
												{
													$random_name = $survey_id."_".$g_question_id."_".time();
													$img_global_name = $this->generateImage($random_name, $image_name);
												}	
												
												foreach($global_question['answer']['value'] as $key => $value) 
												{										
													$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
																																				
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $g_question_id,
																		"key_name" => $key,
																		"answer_value" => $value,
																		"file_name" => $img_global_name,
																		"salultation_value" => $salutation_name,
																		"parent_question_id" => $g_parent_question_id);
																		
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
													}	
												}
											} 	

											
											if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
											{
												$image_name = isset($global_question['image_url'])? $global_question['image_url']:"";
												
												$img_global_name = '';										
												if($image_name!="") 
												{
													$random_name = $survey_id."_".$g_question_id."_".time();
													$img_global_name = $this->generateImage($random_name, $image_name);
												}
													
												foreach($global_question['answer'] as $key => $value) 
												{										
													//Table Name : survey_response_headers
													$image_name = "";
													
													if($key == "image_url") 
													{
														
													}
													else 
													{	
														$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";												
														
														$insertArr = array("response_id" => $cid,
																"question_id" => $g_question_id,
																"key_name" => $key,
																"answer_value" => $value,
																"file_name" => $img_global_name,
																"salultation_value" => $salutation_name,
																"parent_question_id" => $g_parent_question_id);
													
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id, "key_name" => $key));
															//echo ">>>".$this->db->last_query();die();
														}		
													}
												}
											}	

																						
											if($global_question['upload_file_required'] == 'Yes' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
											{ 	
																							
												foreach($global_question['answer']['value'] as $key => $value) 
												{										
													$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";

													$img_global_name = '';										
													if($value['upload_file']!="") 
													{
														$random_name = $survey_id."_".$g_question_id."_".time();
														$img_global_name = $this->generateImage($random_name, $value['upload_file']);
													}
												
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $g_question_id,
																		"answer_value" => $value['value'],
																		"salultation_value" => $salutation_name,
																		"input_file_required" => $img_global_name,
																		"parent_question_id" => $g_parent_question_id);
																
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
													}	
												}
											}

											
											if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
											{
													
												$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
												$insertArr = array("response_id" => $cid,
																	"question_id" => $g_question_id,
																	"answer_value" => $global_question['answer']['value'],
																	"salultation_value" => $salutation_name,
																	"parent_question_id" => $g_parent_question_id);
															
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND answer_value='".$answer['value']."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
													}	
											}	

											// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute

												
											if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "radio" || $g_response_type == "checkbox"  || $g_response_type == "single_select" || $g_response_type == "file"))
											{
												
												$imgUrl = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";

												$img_global_name = '';										
												if($imgUrl!="") 
												{
													$random_name = $survey_id."_".$g_question_id."_".time();
													$img_global_name = $this->generateImage($random_name, $imgUrl);
												}

												if($g_response_type == "file") 
												{
													
													$selectedFile = $global_question['answer']['value'];
													$img_global_name = '';										
													if($selectedFile!="") 
													{
														$random_name_2 = $survey_id."_".$g_question_id."_".time();
														$img_global_name_2 = $this->generateImage($random_name_2, $selectedFile);
													}
													$insertArr = array("response_id" => $cid,
																		"question_id" => $g_question_id,
																		"answer_value" => $img_global_name_2,
																		"file_name" => $img_global_name,
																		"salultation_value" => $salutation_name,
																		"parent_question_id" => $g_parent_question_id);
													
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																	
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
													}	
													
												}												
												else 
												{
													foreach($global_question['answer'] as $key => $value) 
													{
														$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
														
														if($key == "image_url") 
														{
															
														}
														else 
														{
															if(is_array($value))
															{
															
																foreach($value as $names => $namValue) 
																{
																	
																	if($g_response_type == 'checkbox') 
																	{
																		if(is_numeric($names))
																		{
																			$nm = "";
																		}
																		else 
																		{
																			$nm = $names;
																		}
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $g_question_id,
																						"key_name" => $nm,
																						"answer_value" => $namValue['value'],
																						"file_name" => $img_global_name,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $g_parent_question_id);
																	}
																	else 
																	{
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $g_question_id,
																						"answer_value" => $namValue['value'],
																						"file_name" => $img_global_name,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $g_parent_question_id);
																	}
																	
																
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																	
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $g_parent_question_id));
																	}														
																}
															}
															else
															{
																$insertArr = array("response_id" => $cid,
																					"question_id" => $g_question_id,
																					"answer_value" => $value,
																					"file_name" => $img_global_name,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $question_id);
															
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$question_id."'" );
																
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $question_id));
																}		
															}	
														}
													}

												}
														
											}
											
											
											// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute
																			
											if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "radio" || $g_response_type == "checkbox"  || $g_response_type == "single_select" || $g_response_type == "file"))
											{
												
												$imgUrl = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
												
												if($g_response_type == "file") 
												{
													$img_global_name = '';										
													if($global_question['answer']['value']!="") 
													{
														$random_name = $survey_id."_".$g_question_id."_".time();
														$img_global_name = $this->generateImage($random_name, $global_question['answer']['value']);
													}
													
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $g_question_id,
																		"answer_value" => $img_global_name,
																		"parent_question_id" => $g_parent_question_id);
																		
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																	
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id,"parent_question_id" => $g_parent_question_id));
													}					
													
												}
												else 
												{
													
													foreach($global_question['answer'] as $key => $value) 
													{	

														$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
														
														if($key == "image_url") 
														{
															
														}
														else 
														{
															
															if(is_array($value))
															{
																
																foreach($value as $names => $namValue) 
																{
																	
																	if($g_response_type == 'checkbox') 
																	{
																		if(is_numeric($names))
																		{
																			$nm = "";
																		}
																		else 
																		{
																			$nm = $names;
																		}
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $g_question_id,
																						"key_name" => $nm,
																						"answer_value" => $namValue,
																						"file_name" => $imgUrl,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $g_parent_question_id);
																						print_r($insertArr);
																	}
																	else 
																	{
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $g_question_id,
																						"answer_value" => $namValue,
																						"file_name" => $imgUrl,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $g_parent_question_id);
																	}
																	
																	
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																	
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $g_parent_question_id));
																	}														
																}												
																
															}
															else 
															{		
																if($g_response_type == 'checkbox') 
																{
																	
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $g_question_id,
																					"key_name" => $key,
																					"answer_value" => $value,
																					"file_name" => $imgUrl,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $question_id);
																					
																					
																}
																else 
																{	
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $g_question_id,
																					"answer_value" => $value,
																					"key_name" => $key,
																					"file_name" => $imgUrl,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $question_id);
																}
																	
																//Table Name : survey_response_headers														
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$question_id."'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $question_id, "key_name" => "'".$key."'"));
																}
															}														
																
														}
														
													}
												}											
												
											}												
										}
									}
								} // Globle Question End
								
								
								
								if(is_array($answer))
								{	
									
									if(count($answer) > 0) 
									{									
										// Condition First 									
										if($input_file_required == 'Yes' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
										{
										
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
											$image_name = isset($answer['image_url'])? $answer['image_url']:"";
											
											$img_origional_name = '';										
											if($image_name!="") 
											{
												$random_name = $survey_id."_".$question_id."_".time();
												$img_origional_name = $this->generateImage($random_name, $image_name);
											}
											
											foreach($answer['value'] as $key => $value) 
											{												
												$img_multiple_name = '';
												if($value['upload_file']!="") 
												{
													$random_name = $survey_id."_".$question_id."_".time();
													$img_multiple_name = $this->generateImage($random_name, $value['upload_file']);
												}
												//Table Name : survey_response_headers
												$insertArr = array("response_id" => $cid,
																	"question_id" => $question_id,
																	"answer_value" => $value['value'],
																	"file_name" => $img_origional_name,
																	"salultation_value" => $salutation_name,
																	"input_file_required" => $img_multiple_name);
															
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
												$cnt = $queryResponse->num_rows();
												
												if($cnt > 0){$flag = 1;}else{$flag = 0;}
												
												if($flag == 0)
												{
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
												}	
												else 
												{
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $question_id, "parent_question_id" => $question_id));
												}													
																							
											}
										} 
										
										
										if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
										{	
											$image_name = isset($answer['image_url'])? $answer['image_url']:"";	
											$img_origional_name = '';										
											if($image_name!="") 
											{
												$random_name = $survey_id."_".$question_id."_".time();
												$img_origional_name = $this->generateImage($random_name, $image_name);
											}	
											
											if(!is_array($answer['value']))
											{  
												foreach($answer as $key => $value) 
												{										
													
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $question_id,
																		"key_name" => $key,
																		"answer_value" => $value,
																		"file_name" => $img_origional_name,
																		"salultation_value" => $salutation_name);
																
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
													$cnt = $queryResponse->num_rows();
												
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
													}	
												}
											}
											else 
											{	
												foreach($answer['value'] as $key => $value) 
												{										
													
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													$image_name = isset($answer['image_url'])? $answer['image_url']:"";											
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $question_id,
																		"key_name" => $key,
																		"answer_value" => $value,
																		"file_name" => $image_name,
																		"salultation_value" => $salutation_name);
																	
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
													$cnt = $queryResponse->num_rows();
												
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
													}	
												}
											}
											
										} 	

										if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
										{
											$image_name = isset($answer['image_url'])? $answer['image_url']:"";
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
											
											$chkArrpush = array();
											foreach($answer as $key => $value) 
											{										
												//Table Name : survey_response_headers
												
												
												$image_name = "";
												
												if($key == "image_url") 
												{
													
												}
												else 
												{	
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													array_push($chkArrpush, $key);
													$insertArr = array("response_id" => $cid,
															"question_id" => $question_id,
															"key_name" => $key,
															"answer_value" => $value,
															"file_name" => $image_name,
															"salultation_value" => $salutation_name);
													//print_r($insertArr);
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = ".$cid." AND question_id = ".$question_id." AND key_name='".$key."' AND is_deleted = '0'");
													
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{ 
														
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
														//echo $this->db->last_query();die();
													}		
												}
											}
											if($updateFlag)
											{
												if(count($chkArrpush) > 0) 
												{
													$updatedAt = date('Y-m-d H:i:s');
													$this->db->where_not_in('key_name', $chkArrpush);
													$updateArr = array("is_deleted" => '1');
													$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
													//echo ">>>".$this->db->last_query();
												}														
											}
										}	

										
										if($input_file_required == 'Yes' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
										{
											//$image_name = isset($answer['image_url'])? $answer['image_url']:"";
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
											foreach($answer['value'] as $key => $value) 
											{										
												$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
												$image_name = isset($answer['image_url'])? $answer['image_url']:"";
												
												$img_origional_name = '';										
												if($image_name!="") 
												{
													$random_name = $survey_id."_".$question_id."_".time();
													$img_origional_name = $this->generateImage($random_name, $image_name);
												}	
																							
												//Table Name : survey_response_headers
												$insertArr = array("response_id" => $cid,
																	"question_id" => $question_id,
																	"answer_value" => $value['value'],
																	"salultation_value" => $salutation_name,
																	"input_file_required" => $img_origional_name);
															
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
													
												if($flag == 0)
												{
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
												}	
												else 
												{
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
												}	
											}
										}

										if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($option_type == "textbox" || $option_type == "textarea"))
										{
											
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";											
											$insertArr = array("response_id" => $cid,
																"question_id" => $question_id,
																"answer_value" => $answer['value'],
																"salultation_value" => $salutation_name);
													
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
												
												$cnt = $queryResponse->num_rows();
												
												if($cnt > 0){$flag = 1;}else{$flag = 0;}
													
												if($flag == 0)
												{
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
												}	
												else 
												{
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
													
												}


													
												
												
										}	

										// New Case
										if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($option_type == "textbox" || $option_type == "textarea"))
										{
											
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
											$image_name = isset($answer['image_url'])? $answer['image_url']:"";
											$img_origional_name = '';										
											if($image_name!="") 
											{
												$random_name = $survey_id."_".$question_id."_".time();
												$img_origional_name = $this->generateImage($random_name, $image_name);
											}
											
											
											$insertArr = array("response_id" 		=> $cid,
																"question_id" 		=> $question_id,
																"answer_value" 		=> $answer['value'],
																"salultation_value" => $salutation_name,
																"file_name" 		=> $img_origional_name);
													
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
												$cnt = $queryResponse->num_rows();
												
												if($cnt > 0){$flag = 1;}else{$flag = 0;}
												
												if($flag == 0)
												{
													
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													
												}	
												else 
												{
													
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
												
												}	
										}
										
										
										// For Checkbox|Radio|Select Box|File If File Is Required Yes 
										if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($option_type == "radio" || $option_type == "checkbox"  || $option_type == "single_select" || $option_type == "file"))
										{
											
											$imgUrl = isset($answer['image_url'])? $answer['image_url']:"";
											$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
											
											$img_multiple_name = '';
											if($imgUrl!="") 
											{
												$random_name = $survey_id."_".$question_id."_".time();
												$img_multiple_name = $this->generateImage($random_name, $imgUrl);
											}
											
											if($option_type == 'file') 
											{
												
												$insertArr = array("response_id" => $cid,
																	"question_id" => $question_id,
																	"answer_value" => $img_multiple_name);
												//Table Name : survey_response_headers	
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
												$cnt = $queryResponse->num_rows();												
												if($cnt > 0){$flag = 1;}else{$flag = 0;}												
												if($flag == 0)
												{
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
												}	
												else 
												{
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
												}
												
											}
											else 
											{	$chkArrpush = array();											
												foreach($answer as $key => $value) 
												{
													
													if($key ==  "image_url")
													{
														
													}
													else 
													{
														$insertArr = array("response_id" => $cid,
																		"question_id" => $question_id,
																		"key_name" => $key,
																		"answer_value" => $value,
																		"file_name" => $img_multiple_name,
																		"salultation_value" => $salutation_name);
															//print_r($insertArr);
															if($option_type == "checkbox") 
															{
																
																array_push($chkArrpush, $key);
															}	
															
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name =  '".$key."' AND is_deleted = '0'");
															
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																
															}
													}
													
												}
												
												// Delete Previous Answers regarding Checkbox and Selectbox
												if($updateFlag)
												{
													if(count($chkArrpush) > 1) 
													{
														$updatedAt = date('Y-m-d H:i:s');
														$this->db->where_not_in('key_name', $chkArrpush);
														$updateArr = array("is_deleted" => '1');
														$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
														//echo $this->db->last_query();
													}
																											
												}
												
											}
											
																				
										}
										
										
										// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute
										if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($option_type == "radio" || $option_type == "checkbox"  || $option_type == "single_select" || $option_type == "file"))
										{
											
											if($option_type == 'file') 
											{
												$image_name = isset($answer['image_url'])? $answer['image_url']:"";						
														
												$img_origional_name = '';										
												if($image_name!="") 
												{
													$random_name = $survey_id."_".$question_id."_".time();
													$img_origional_name = $this->generateImage($random_name, $image_name);
												}
												
												$insertArr = array("response_id" => $cid,
																	"question_id" => $question_id,
																	"answer_value" => $img_origional_name);
												//Table Name : survey_response_headers
												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
												$cnt = $queryResponse->num_rows();
												
												if($cnt > 0){$flag = 1;}else{$flag = 0;}
												
												if($flag == 0)
												{
													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
												}	
												else 
												{
													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
												}
												
											}
											else 
											{
												$chkArrpush = array();
												foreach($answer as $key => $value) 
												{	

													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													
													if($key == "image_url") 
													{
														
													}
													else 
													{
														$insertArr = array("response_id" => $cid,
																			"question_id" => $question_id,
																			"key_name" => $key,
																			"answer_value" => $value,
																			"salultation_value" => $salutation_name);
															//Table Name : survey_response_headers										
															
															if($option_type == "checkbox") 
															{
																
																array_push($chkArrpush, $key);
															}	
															
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
															}
																
													}
													
												}
												
												if($updateFlag)
												{
													if(count($chkArrpush) > 1) 
													{
														$updatedAt = date('Y-m-d H:i:s');
														$this->db->where_not_in('key_name', $chkArrpush);
														$updateArr = array("is_deleted" => '1');
														$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
													}														
												}
											}	


											/*if(is_array($sectionQuestions['dependent_question_options']))
											{	
												if(count($sectionQuestions['dependent_question_options']) > 0) 
												{
													foreach($sectionQuestions['dependent_question_options'] as $opt => $optionDependentQ) 
													{
															echo $values = $optionDependentQ[$opt]['option'];	
															echo "===".$two = $optionDependentQ[$opt]['option']['option_dependent_question']['question_id'];
													}
												}
											}*/
												
											
										}											
										
									}									
									
								}	// Answer Node End

								//echo "<pre>";print_r($dependent_question);
								if(is_array($dependent_question))
								{	
									if(count($dependent_question) > 0) 
									{
										foreach($dependent_question as $opt => $optionDependent) 
										{	

											$selectedOptionValues = $dependent_question[$opt]['option'];
											//$dependent_question[$opt]['option_dependent_question']['question_id'];
											
											//echo "===".$two = $dependent_question[$opt]['option']['option_dependent_question']['question_id'];
											//$parent_question_qid = $dependent_question['question_id'];
											$parent_question_qid = $question_id;
											$option_response_type = $optionDependent['option_dependent_question']['response_type'];
											$option_dependent_file = $optionDependent['option_dependent_question']['if_file_required'];
											$option_dependent_input = $optionDependent['option_dependent_question']['input_more_than_1'];
											$option_input_file_required = $optionDependent['option_dependent_question']['upload_file_required'];
											$q_id = $optionDependent['option_dependent_question']['question_id'];
											$option_dependent_salutation = $optionDependent['option_dependent_question']['salutation'];
											$option_dependent_answer = $optionDependent['option_dependent_question']['answer'];
																				
											$section_option_dependent_recursive = $optionDependent['option_dependent_question']['dependent_question_options'];
											
											//echo "<pre>";print_r($optionDependent['option_dependent_question']);
											
											
											if($option_input_file_required == 'Yes' && $option_dependent_file == 'Yes' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{
												$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
												$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
												
												$img_old_name = '';										
												if($image_name!="") 
												{
													$random_name2 = $survey_id."_".$q_id."_".time();
													$img_old_name = $this->generateImage($random_name2, $image_name);
												}	
												
												foreach($option_dependent_answer['value'] as $key => $value) 
												{			
													
													$img_old_name = '';										
													if($value['upload_file']!="") 
													{
														$random_name = $survey_id."_".$q_id."_".time();
														$img_old_name = $this->generateImage($random_name, $value['upload_file']);
													}
													
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $q_id,
																		"answer_value" => $value['value'],
																		"file_name" => $img_old_name,
																		"salultation_value" => $salutation_name,
																		"input_file_required" => $img_old_name,
																		"parent_question_id" => $parent_question_qid);
																
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
													
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
													}													
																								
												}											
												
											} 
											
											
											if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{	
												
												foreach($option_dependent_answer['value'] as $key => $value) 
												{										
												
													$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
													$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
													
													$img_old_name = '';										
													if($image_name!="") 
													{
														$random_name2 = $survey_id."_".$q_id."_".time();
														$img_old_name = $this->generateImage($random_name2, $image_name);
													}	
														
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $q_id,
																		"key_name" => $key,
																		"answer_value" => $value,
																		"file_name" => $img_old_name,
																		"salultation_value" => $salutation_name,
																		"parent_question_id" => $parent_question_qid);
																		
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
													}	
												}
											} 	

										
											if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{
												$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
												$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
												
												foreach($option_dependent_answer as $key => $value) 
												{										
													//Table Name : survey_response_headers
													$image_name = "";
													
													if($key == "image_url") 
													{
														
													}
													else 
													{											
														
														$insertArr = array("response_id" => $cid,
																"question_id" => $q_id,
																"key_name" => $key,
																"answer_value" => $value,
																"file_name" => $image_name,
																"salultation_value" => $salutation_name,
																"parent_question_id" => $parent_question_qid);
													
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
														}		
													}
												}
												
											}	

											
											if($option_input_file_required == 'Yes' && $option_dependent_file == 'No' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{
												//$image_name = isset($answer['image_url'])? $answer['image_url']:"";
												
												foreach($option_dependent_answer['value'] as $key => $value) 
												{										
													$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
													
													$img_old_name = '';										
													if($value['upload_file']!="") 
													{
														$random_name2 = $survey_id."_".$q_id."_".time();
														$img_old_name = $this->generateImage($random_name2, $value['upload_file']);
													}											
													//Table Name : survey_response_headers
													$insertArr = array("response_id" => $cid,
																		"question_id" => $q_id,
																		"answer_value" => $value['value'],
																		"salultation_value" => $salutation_name,
																		"input_file_required" => $img_old_name,
																		"parent_question_id" => $parent_question_qid);
																
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
													}	
												}
											}

											
											//done
											if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'No' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{
												
											$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
											if($option_dependent_answer['value']!="")
											{
												$insertArr = array("response_id" => $cid,
																	"question_id" => $q_id, 
																	"option_value" => $selectedOptionValues,
																	"answer_value" => $option_dependent_answer['value'],
																	"salultation_value" => $salutation_name,
																	"parent_question_id" => $parent_question_qid);													
													
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND option_value='".$selectedOptionValues."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "option_value" => "'".$selectedOptionValues."'"));
													}	
													
													
													if(count($option_dependent_recursive) > 0)
													{
														
														foreach($option_dependent_recursive as $nestedValue) 
														{
															
															$nestedTextQuestionId = $nestedValue['question_id'];
															$nestedTextQuestionLabel = $nestedValue['question_label'];
															$nestedTextQuestionResponseType = $nestedValue['response_type'];
															$nestedTextQuestionSalutation = $nestedValue['salutation'];
															$nestedTextQuestionInputMoreThanOne = $nestedValue['input_more_than_1'];
															$nestedTextQuestionFileRequired = $nestedValue['if_file_required'];
															$nestedTextQuestionFileUploadR = $nestedValue['upload_file_required'];
															$nestedTextQuestionLabel = $nestedValue['question_label'];
															$nestedTextQuestionAnswer = $nestedValue['answer'];
															
															
																if($nestedTextQuestionFileUploadR == 'No' && $nestedTextQuestionFileRequired == 'No' && $nestedTextQuestionInputMoreThanOne == 'No' && ($nestedTextQuestionResponseType == "textbox"  || $nestedTextQuestionResponseType == "textarea"))
																{
																																
																	$image_name = isset($nestedTextQuestionId['image_url'])? $nestedTextQuestionId['image_url']:"";
																		
																		foreach($nestedTextQuestionAnswer as $key => $value) 
																		{								
																			//Table Name : survey_response_headers
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $nestedTextQuestionId,
																								"key_name" => $key,
																								"answer_value" => $value);
																			//print_r($insertArr);			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedTextQuestionId."' AND key_name = '".$key."'  AND is_deleted = '0' AND parent_question_id = '".$q_id."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $nestedTextQuestionId, "parent_question_id" => $q_id, "key_name" => "'".$key."'"));
																			}													
																														
																		}	
																}
																
																													
														}
													}
													
													
													if(is_array($section_option_dependent_recursive))
													{													
														if(count($section_option_dependent_recursive) > 0)
														{
															foreach($section_option_dependent_recursive as $optionDependentQuestionlist) 
															{
																$nestedDependentQuestionID = $optionDependentQuestionlist['question_id'];
																$nestedDependentQuesResponseType = $optionDependentQuestionlist['response_type'];
																$nestedDependentQues_upload_file 	= $optionDependentQuestionlist['upload_file_required'];
																$nestedDependentQues_if_file_required 		= $optionDependentQuestionlist['if_file_required'];
																$nestedDependentQues_inputMoreThanOne 		= $optionDependentQuestionlist['input_more_than_1'];
																$nestedDependentQues_question_id 	= $optionDependentQuestionlist['parent_question_id'];
																$nestedDependentQues_salutation 	= $optionDependentQuestionlist['salutation'];
																$nestedDependentQues_answer = $optionDependentQuestionlist['answer'];
																
																if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "textbox" || $nestedDependentQuesResponseType == "textarea"))
																{  
																	$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																	
																	
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $nestedDependentQuestionID,
																						"answer_value" => $nestedDependentQues_answer['value'],
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $q_id);
																		//print_r($insertArr);	
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																		}
																}
																
																if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "radio" || $nestedDependentQuesResponseType == "checkbox"  || $nestedDependentQuesResponseType == "single_select" || $nestedDependentQuesResponseType == "file"))
																{ 
																	
																	$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																	
																	
																	if($nestedDependentQuesResponseType == "file") 
																	{
																		$img_old_name = '';										
																		if($nestedDependentQues_answer['value']!="") 
																		{
																			$random_name2 = $survey_id."_".$nestedDependentQuestionID."_".time();
																			$img_old_name = $this->generateImage($random_name2, $nestedDependentQues_answer['value']);
																			$insertArr = array("response_id" => $cid,
																							"question_id" => $nestedDependentQuestionID,
																							"answer_value" => $img_old_name,
																							"parent_question_id" => $q_id);
																					
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																			$cnt = $queryResponse->num_rows();
																			//echo $nestedDependentQuestionID.">>>>".$this->db->last_query();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																			}
																		}
																	}
																	else 
																	{
																		if(count($nestedDependentQues_answer)> 0)
																		{
																			foreach($nestedDependentQues_answer as $kCh => $value)
																			{
																				$insertArr = array("response_id" => $cid,
																							"question_id" => $nestedDependentQuestionID,
																							"key_name" => $kCh,
																							"answer_value" => $value,
																							"salultation_value" => $salutation_name,
																							"parent_question_id" => $q_id);
																					
																						$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name = '".$kCh."'");
																							$cnt = $queryResponse->num_rows();
																							
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}
																							
																						if($flag == 0)
																						{
																							$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																						}	
																						else 
																						{
																							$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id, "key_name" => "'".$kCh."'"));
																						}
																			}
																		}
																	}
																											
																}
																
															}
														}
													}
											}
												
													
													
											}	
											
											
											//done
											if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'No' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
											{
											$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
											
												$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
												$img_old_name = '';										
												if($imgUrl!="") 
												{
													$random_name2 = $survey_id."_".$q_id."_".time();
													$img_old_name = $this->generateImage($random_name2, $imgUrl);
												}
											
												$insertArr = array("response_id" => $cid,
																	"question_id" => $q_id,
																	"answer_value" => $option_dependent_answer['value'],
																	"file_name"	=> $img_old_name,
																	"salultation_value" => $salutation_name,
																	"parent_question_id" => $parent_question_qid);
													
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
													}	
													
													
													if(count($option_dependent_recursive) > 0)
													{
														//echo "TextBox<pre>";print_r($option_dependent_recursive);
														foreach($option_dependent_recursive as $nestedValue) 
														{
															
															$nestedTextQuestionId = $nestedValue['question_id'];
															$nestedTextQuestionLabel = $nestedValue['question_label'];
															$nestedTextQuestionResponseType = $nestedValue['response_type'];
															$nestedTextQuestionSalutation = $nestedValue['salutation'];
															$nestedTextQuestionInputMoreThanOne = $nestedValue['input_more_than_1'];
															$nestedTextQuestionFileRequired = $nestedValue['if_file_required'];
															$nestedTextQuestionFileUploadR = $nestedValue['upload_file_required'];
															$nestedTextQuestionLabel = $nestedValue['question_label'];
															$nestedTextQuestionAnswer = $nestedValue['answer'];
															
															
																if($nestedTextQuestionFileUploadR == 'No' && $nestedTextQuestionFileRequired == 'No' && $nestedTextQuestionInputMoreThanOne == 'No' && ($nestedTextQuestionResponseType == "textbox"  || $nestedTextQuestionResponseType == "textarea"))
																{
																																
																	$image_name = isset($nestedTextQuestionId['image_url'])? $nestedTextQuestionId['image_url']:"";
																		
																		foreach($nestedTextQuestionAnswer as $key => $value) 
																		{								
																			//Table Name : survey_response_headers
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $nestedTextQuestionId,
																								"key_name" => $key,
																								"answer_value" => $value,
																								"parent_question_id" => $q_id);
																			//print_r($insertArr);			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedTextQuestionId."' AND key_name = '".$key."'  AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $nestedTextQuestionId, "parent_question_id" => $q_id, "key_name" => "'".$key."'"));
																			}													
																														
																		}	
																}
																
																													
														}
													}
													
													
													
											}

											// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute											
											if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'No' && ($option_response_type == "radio" || $option_response_type == "checkbox"  || $option_response_type == "single_select" || $option_response_type == "file"))
											{
												
												
												$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
												
												$img_old_name = '';										
												if($imgUrl!="") 
												{
													$random_name2 = $survey_id."_".$q_id."_".time();
													$img_old_name = $this->generateImage($random_name2, $imgUrl);
												}
											
												if($option_response_type == "file") 
												{
													$depFile = $option_dependent_answer['value'];
													$img_origional_name = '';										
													if($depFile!="") 
													{
														$random_name = $survey_id."_".$q_id."_".time();
														$img_origional_name = $this->generateImage($random_name, $depFile);
													}
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $q_id,
																		"answer_value" => $img_origional_name,
																		"file_name" => $img_old_name,
																		"salultation_value" => $salutation_name,
																		"parent_question_id" => $parent_question_qid);
												
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
													
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "key_name" => "'".$names."'", "parent_question_id" => $parent_question_qid));
													}
												}
												else 
												{
													
													foreach($option_dependent_answer as $key => $value) 
													{
														$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
														
														if($key == "image_url") 
														{
															
														}
														else 
														{
															
															$i = 0;
															$chkArrpush = array();
															foreach($value as $names => $namValue) 
															{
																$insertArr = array("response_id" => $cid,
																					"question_id" => $q_id,
																					"key_name" => $names,
																					"answer_value" => $namValue,
																					"file_name" => $imgUrl,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $parent_question_qid);
															
																if($option_response_type == "checkbox") 
																{
																	array_push($chkArrpush, $names);
																}
																
																
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$names."'"));
																}														
															}
															
															// Delete Previous Answers regarding Checkbox and Selectbox
															if($updateFlag)
															{
																if(count($chkArrpush) > 1) 
																{
																			$updatedAt = date('Y-m-d H:i:s');
																			$this->db->where_not_in('key_name', $chkArrpush);
																			$updateArr = array("is_deleted" => '1');
																			$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
																}														
															}															
																
														}
													}
												}	
											}
											
											
											// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute
											
											if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'No' && ($option_response_type == "radio" || $option_response_type == "checkbox"  || $option_response_type == "single_select" || $option_response_type == "file"))
											{
												
												$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
												$img_old_name = '';										
												if($imgUrl!="") 
												{
													$random_name2 = $survey_id."_".$q_id."_".time();
													$img_old_name = $this->generateImage($random_name2, $imgUrl);
												}
												
												if($option_response_type == "file") 
												{
													$insertArr = array("response_id" => $cid,
																		"question_id" => $q_id,
																		"key_name" => $key,
																		"answer_value" => $img_old_name,
																		"salultation_value" => $salutation_name,
																		"parent_question_id" => $parent_question_qid);
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND parent_question_id='".$parent_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
													
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
													}					
												}
												else 
												{
													
													$chkArrpush = array();
													foreach($option_dependent_answer as $key => $value) 
													{	
														
														$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
														if($key == "image_url") 
														{
															
														}
														else 
														{
															$insertArr = array("response_id" => $cid,
																				"question_id" => $q_id,
																				"key_name" => $key,
																				"answer_value" => $value,
																				"salultation_value" => $salutation_name,
																				"parent_question_id" => $parent_question_qid);
																//Table Name : survey_response_headers
															//print_r($insertArr);
															if($option_response_type == "checkbox") 
															{
																
																array_push($chkArrpush, $key);
															}	
																
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND parent_question_id='".$parent_question_qid."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$parent_question_qid."'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
															}	
														}
													}
													
													// Delete Previous Answers regarding Checkbox and Selectbox
													if($updateFlag)
													{
														if(count($chkArrpush) > 1) 
														{
															$updatedAt = date('Y-m-d H:i:s');
															$this->db->where_not_in('key_name', $chkArrpush);
															$updateArr = array("is_deleted" => '1');
															$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));	
														}														
													}
												}
												
												
												if(is_array($section_option_dependent_recursive))
												{													
													if(count($section_option_dependent_recursive) > 0)
													{ 
														foreach($section_option_dependent_recursive as $optionDependentQuestionlist) 
														{
															
															$nestedDependentQuestionID = $optionDependentQuestionlist['question_id'];
															$nestedDependentQuesResponseType = $optionDependentQuestionlist['response_type'];
															$nestedDependentQues_upload_file 	= $optionDependentQuestionlist['upload_file_required'];
															$nestedDependentQues_if_file_required 		= $optionDependentQuestionlist['if_file_required'];
															$nestedDependentQues_inputMoreThanOne 		= $optionDependentQuestionlist['input_more_than_1'];
															$nestedDependentQues_question_id 	= $optionDependentQuestionlist['parent_question_id'];
															$nestedDependentQues_salutation 	= $optionDependentQuestionlist['salutation'];
															$nestedDependentQues_answer = $optionDependentQuestionlist['answer'];
															
															$nestedDependentOptionDependent = $optionDependentQuestionlist['option_dependent_question'];
															
															
															if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "textbox" || $nestedDependentQuesResponseType == "textarea"))
															{  
																$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																
																
																$insertArr = array("response_id" => $cid,
																					"question_id" => $nestedDependentQuestionID,
																					"answer_value" => $nestedDependentQues_answer['value'],
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $q_id);
																		
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																	}
															}
															
															if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "radio" || $nestedDependentQuesResponseType == "checkbox"  || $nestedDependentQuesResponseType == "single_select" || $nestedDependentQuesResponseType == "file"))
															{ 
																
																$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																
																
																if($nestedDependentQuesResponseType == "file") 
																{
																	$img_old_name = '';										
																	if($nestedDependentQues_answer['value']!="") 
																	{
																		$random_name2 = $survey_id."_".$nestedDependentQuestionID."_".time();
																		$img_old_name = $this->generateImage($random_name2, $nestedDependentQues_answer['value']);
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $nestedDependentQuestionID,
																						"answer_value" => $img_old_name,
																						"parent_question_id" => $q_id);
																				
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																		$cnt = $queryResponse->num_rows();
																		//echo $nestedDependentQuestionID.">>>>".$this->db->last_query();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																		}
																	}
																}
																else 
																{
																	if(count($nestedDependentQues_answer)> 0)
																	{
																		$chkArrpush = array();
																		
																		foreach($nestedDependentQues_answer as $kCh => $value)
																		{
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $nestedDependentQuestionID,
																								"key_name" => $kCh,
																								"answer_value" => $value,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $q_id);
																			//echo "<pre>";print_r($insertArr);		
																			if($nestedDependentQuesResponseType == "checkbox") 
																			{
																				
																				array_push($chkArrpush, $kCh);
																			}	
																		
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name = '".$kCh."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id, "key_name" => "'".$kCh."'"));
																			}
																		}
																		
																		// Delete Previous Answers regarding Checkbox and Selectbox
																		if($updateFlag)
																		{
																			if(count($chkArrpush) > 1) 
																			{
																				$updatedAt = date('Y-m-d H:i:s');
																				$this->db->where_not_in('key_name', $chkArrpush);
																				$updateArr = array("is_deleted" => '1');
																				$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																			}																			
																		}
																	}
																}
																										
															}
															//print_r($nestedDependentOptionDependent['option_dependent_question']);
															if(is_array($nestedDependentOptionDependent))
															{  
																if(count($nestedDependentOptionDependent) > 0)
																{
																		//echo "==".$q_id;
																		$p_qid = $nestedDependentOptionDependent['question_id'];
																		$p_response_type = $nestedDependentOptionDependent['response_type'];
																		$p_file_required = $nestedDependentOptionDependent['if_file_required'];
																		$p_input_more = $nestedDependentOptionDependent['input_more_than_1'];
																		$p_upload_file_required = $nestedDependentOptionDependent['upload_file_required'];
																		$p_salutation = $nestedDependentOptionDependent['salutation'];
																		$p_answer = $nestedDependentOptionDependent['answer'];
																		
																		
																		
																		if($p_upload_file_required == 'No' && $p_file_required == 'No' && $p_input_more == 'No' && ($p_response_type == "radio" || $p_response_type == "checkbox"  || $p_response_type == "single_select" || $p_response_type == "file"))
																		{ 
																			
																			$salutation_name = isset($p_salutation['salutation'])? $p_salutation['salutation']:"";
																			
																			
																			if($p_response_type == "file") 
																			{
																				$img_old_name = '';										
																				if($p_answer['value']!="") 
																				{
																					$random_name2 = $survey_id."_".$p_qid."_".time();
																					$img_old_name = $this->generateImage($random_name2, $p_answer['value']);
																					$insertArr = array("response_id" => $cid,
																									"question_id" => $p_qid,
																									"answer_value" => $img_old_name,
																									"parent_question_id" => $q_id);
																							
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																					$cnt = $queryResponse->num_rows();
																					
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}
																						
																					if($flag == 0)
																					{
																						$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																					}	
																					else 
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																					}
																				}
																			}
																			else 
																			{
																				if(count($p_answer)> 0)
																				{	$chkArrpush = array();
																					foreach($p_answer as $kCh => $value)
																					{
																						$insertArr = array("response_id" => $cid,
																									"question_id" => $p_qid,
																									"key_name" => $kCh,
																									"answer_value" => $value,
																									"salultation_value" => $salutation_name,
																									"parent_question_id" => $q_id);
																						
																							if($p_response_type == "checkbox") 
																							{
																								
																								array_push($chkArrpush, $kCh);
																							}
																						
																								$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name = '".$kCh."'");
																								//echo $this->db->last_query();
																									$cnt = $queryResponse->num_rows();
																									
																									if($cnt > 0){$flag = 1;}else{$flag = 0;}
																									
																								if($flag == 0)
																								{
																									$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																								}	
																								else 
																								{
																									$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id, "key_name" => "'".$kCh."'"));
																									
																									//echo $this->db->last_query();die();
																								}
																					}
																					// Delete Previous Answers regarding Checkbox and Selectbox
																					if($updateFlag)
																					{
																						if(count($chkArrpush) > 1) 
																						{
																						$updatedAt = date('Y-m-d H:i:s');
																						$this->db->where_not_in('key_name', $chkArrpush);
																						$updateArr = array("is_deleted" => '1');
																						$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																						//echo $this->db->last_query();
																						}														
																					}
																				}
																			}
																													
																		}
																	
																		if($p_upload_file_required == 'No' && $p_file_required == 'No' && $p_input_more == 'No' && ($p_response_type == "textbox" || $p_response_type == "textarea"))
																		{  
																			$salutation_name = isset($p_salutation['salutation'])? $p_salutation['salutation']:"";
																			
																			foreach($p_answer as $k => $val) 
																			{
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $p_qid,
																								"key_name" => $k,
																								"answer_value" => $val,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $q_id);
																				
																				if($k == "") 
																				{
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																				}
																				else 
																				{
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name='".$k."'");
																				}																					
																				
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					if($k != "") 
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id, "key_name" => "'".$k."'"));
																					}
																					else
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																					}
																					
																				}
																			}
																			
																		}
																		
																}
															}
															
														}
													}
												}												
											
											
											
											}
											
											
										}	// End Foreach								
									}
								} // End Dependent Question
								
								
								// Sub Question Node
								if(is_array($sub_question))
								{								
									if(count($sub_question) > 0) 
									{
										
										//foreach($sub_question as $subDependent) 
										//{
											/*$sub_question_qid = $subDependent['question_id'];
											$sub_option_response_type = $subDependent['response_type'];
											$sub_option_input_file = $subDependent['upload_file_required'];
											$sub_option_dependent_file = $subDependent['if_file_required'];
											$sub_option_dependent_input = $subDependent['input_more_than_1'];
											$sub_parent_question_id = $subDependent['parent_question_id'];
											$sub_option_dependent_salutation = $subDependent['salutation'];
											$sub_option_dependent_answer = $subDependent['answer'];	*/	


											$sub_question_qid = $sub_question['question_id'];
											$sub_option_response_type = $sub_question['response_type'];
											$sub_option_input_file = $sub_question['upload_file_required'];
											$sub_option_dependent_file = $sub_question['if_file_required'];
											$sub_option_dependent_input = $sub_question['input_more_than_1'];
											$sub_parent_question_id = $sub_question['parent_question_id'];
											$sub_option_dependent_salutation = $sub_question['salutation'];
											$sub_option_dependent_answer = $sub_question['answer'];	
											
											if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'No' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "textbox" || $sub_option_response_type == "textarea"))
											{ 
												$salutation_name = isset($sub_option_dependent_salutation['salutation'])? $sub_option_dependent_salutation['salutation']:"";
												$insertArr = array("response_id" => $cid,
																	"question_id" => $sub_question_qid,
																	"answer_value" => $sub_option_dependent_answer['value'],
																	"salultation_value" => $salutation_name);
													//print_r($insertArr);		
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND  is_deleted = '0'");
														$cnt = $queryResponse->num_rows();
													//echo ">>>".$this->db->last_query();	
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
													}	
											}	

											// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute

												
											if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'Yes' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "radio" || $sub_option_response_type == "checkbox"  || $sub_option_response_type == "single_select" || $sub_option_response_type == "file"))
											{
												$imgUrl = isset($sub_option_dependent_answer['image_url'])? $sub_option_dependent_answer['image_url']:"";
												
												$img_old_name1 = '';										
												if($imgUrl!="") 
												{
													$random_name2 = $survey_id."_".$sub_question_qid."_".time();
													$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
												}
												
												$salutation_name = isset($sub_option_dependent_answer['salutation'])? $sub_option_dependent_answer['salutation']:"";
												
												if($sub_option_response_type == "file") 
												{
													$img_old_name = '';										
													if($sub_option_dependent_salutation['value']!="") 
													{
														$random_name2 = $survey_id."_".$sub_question_qid."_".time();
														$img_old_name = $this->generateImage($random_name2, $sub_option_dependent_salutation['value']);
													}
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $sub_question_qid,
																		"answer_value" => $img_old_name,
																		"file_name" => $img_old_name1,
																		"salultation_value" => $salutation_name);
												
													$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'");
													
													$cnt = $queryResponse->num_rows();
													
													if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
													if($flag == 0)
													{
														$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
													}	
													else 
													{
														$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id));
													}
													
												}
												else 
												{
													foreach($sub_option_dependent_answer as $key => $value) 
													{
																											
														if($key == "image_url") 
														{
															
														}
														else 
														{
															
															$i = 0;
															
															$chkArrpush = array();
															foreach($value as $names => $namValue) 
															{
																
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_question_qid,
																					"key_name" => $names,
																					"answer_value" => $namValue,
																					"file_name" => $img_old_name1,
																					"salultation_value" => $salutation_name);
																					
																					
																if($sub_option_response_type == "checkbox") 
																{
																	array_push($chkArrpush, $names);
																}															
															
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'");
																
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "key_name" => "'".$names."'"));
																}														
															}
															// Delete Previous Answers regarding Checkbox and Selectbox
															if($updateFlag)
															{
																if(count($chkArrpush) > 1) 
																{
																	$updatedAt = date('Y-m-d H:i:s');
																	$this->db->where_not_in('key_name', $chkArrpush);
																	$updateArr = array("is_deleted" => '1');
																	$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id));
																}														
															}																
														}
													}
												}
												
											}
											
											
											// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute										
											if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'No' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "radio" || $sub_option_response_type == "checkbox"  || $sub_option_response_type == "single_select" || $sub_option_response_type == "file"))
											{
												
												$salutation_name = isset($sub_option_dependent_answer['salutation'])? $sub_option_dependent_answer['salutation']:"";
												if($sub_option_response_type == "file") 
												{
													
													$img_old_name1 = '';										
													if($imgUrl!="") 
													{
														$random_name2 = $survey_id."_".$sub_question_qid."_".time();
														$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
													}
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $sub_question_qid,
																		"answer_value" => $img_old_name1,
																		"salultation_value" => $salutation_name);
																											
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}
													
													
												} 
												else 
												{
													foreach($sub_option_dependent_answer as $key => $value) 
													{
														
														if($key == "image_url") 
														{
															
														}
														else 
														{
															$chkArrpush = array();
															$insertArr = array("response_id" => $cid,
																			"question_id" => $sub_question_qid,
																			"key_name" => $key,
																			"answer_value" => $value,
																			"salultation_value" => $salutation_name);
																
																if($sub_option_response_type == "checkbox") 
																{
																	
																	array_push($chkArrpush, $key);
																}		
																	
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
																	$cnt = $queryResponse->num_rows();
																	
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																}	
														}												
													}
													
													// Delete Previous Answers regarding Checkbox and Selectbox
													if($updateFlag)
													{
														if(count($chkArrpush) > 1) 
														{
															$updatedAt = date('Y-m-d H:i:s');
															$this->db->where_not_in('key_name', $chkArrpush);
															$updateArr = array("is_deleted" => '1');
															$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}														
													}
													
												}
												
																							
											}										
										//}
									}
								} // Sub Question End
								
								
							}	// Section Question List
								
							
							}	// Section Question Count Check
							
							
							
							// Sub Section List Start 							
								if(is_array($sections['sub_section']))
								{						
									if(count($sections['sub_section']) > 0)
									{						
										foreach($sections['sub_section'] as $node => $subSectionNode) 
										{		
											//echo "<pre>";print_r($subSectionNode);	
											$sub_section_id 	= $subSectionNode['sub_section_id'];
											$sub_section_name 	= $subSectionNode['section_name'];
											$sb_section_desc 	= $subSectionNode['section_description'];	
											foreach($subSectionNode['sub_section_question'] as $subKey => $subSectionQuestionire) 
											{
												
												$sub_question_qid 				= $subSectionQuestionire['question_id'];
												$sub_sec_response_type 			= $subSectionQuestionire['response_type'];
												$sub_upload_file_required 		= $subSectionQuestionire['upload_file_required'];
												$sub_if_file_required			= $subSectionQuestionire['if_file_required'];
												$sub_input_more_than_1 			= $subSectionQuestionire['input_more_than_1'];
												$parent_question_id 			= $subSectionQuestionire['parent_question_id'];
												$SubSecoption_dependent_salutation 	= $subSectionQuestionire['salutation'];
												$suboption_dependent_answer 		= $subSectionQuestionire['answer'];
												$dependent_question_node 		= $subSectionQuestionire['dependent_question_options'];
												$sub_global_question 			= @$subSectionQuestionire['global_question'];
												//echo "==Parent==".$sub_question_qid."==Child==".$sub_sec_response_type;
												
												//echo "<pre>";print_r($sub_global_question);
												
												if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'Yes' && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
												{
													//echo "==".$sub_question_qid."==".$sub_sec_response_type;
													$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
													
													$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
													
													foreach($suboption_dependent_answer as $ks => $val)
													{
														$insertArr = array("response_id" => $cid,
																		"question_id" => $sub_question_qid,
																		"key_name" => $ks,
																		"answer_value" => $val,
																		"salultation_value" => $salutation_name);
																
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name = '".$ks."' AND is_deleted = '0'");
														//echo ">>>>".$this->db->last_query();
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$ks."'"));
														}
													}
														
												}
												
												
												if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No' && ($sub_sec_response_type == "radio" || $sub_sec_response_type == "checkbox"  || $sub_sec_response_type == "single_select" || $sub_sec_response_type == "file"))
												{
													//echo "==".$sub_question_qid."==".$sub_sec_response_type;
													$imgUrl = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
													
													$img_old_name1 = '';										
													if($imgUrl!="") 
													{
														$random_name2 = $survey_id."_".$sub_question_qid."_".time();
														$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
													}
													
													
													if($sub_sec_response_type == "file") 
													{
														$upload_old_name1 = '';										
														if($suboption_dependent_answer['value']!="") 
														{
															$random_name = $survey_id."_".$sub_question_qid."_".time();
															$upload_old_name1 = $this->generateImage($random_name, $suboption_dependent_answer['value']);
														}
														
														$insertArr = array("response_id" => $cid,
																			"question_id" => $sub_question_qid,
																			"answer_value" => $upload_old_name1,
																			"file_name" => $img_old_name1);
																			
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'" );
														
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}	
													}
													else 
													{
														foreach($suboption_dependent_answer as $key => $value) 
														{	

															$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
															
															if($key == "image_url") 
															{
																
															}
															else 
															{
																
																if(is_array($value))
																{
																	$chkArrpush = array();
																	foreach($value as $names => $namValue) 
																	{
																		
																		if($sub_sec_response_type == 'checkbox') 
																		{
																			if(is_numeric($names))
																			{
																				$nm = "";
																			}
																			else 
																			{
																				$nm = $names;
																			}
																			
																			array_push($chkArrpush, $key);
																			$insertArr = array("response_id" => $cid,
																							"question_id" => $sub_question_qid,
																							"key_name" => $nm,
																							"answer_value" => $namValue['value'],
																							"file_name" => $img_old_name1,
																							"salultation_value" => $salutation_name);
																							
																		}
																		else 
																		{
																			$insertArr = array("response_id" => $cid,
																							"question_id" => $sub_question_qid,
																							"answer_value" => $namValue['value'],
																							"file_name" => $img_old_name1,
																							"salultation_value" => $salutation_name);
																		}
																		
																	
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'" );
																		
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$names."'"));
																		}														
																	}
																		
																	if($updateFlag)
																	{
																		if(count($chkArrpush) > 1) 
																		{
																			$updatedAt = date('Y-m-d H:i:s');
																			$this->db->where_not_in('key_name', $chkArrpush);
																			$updateArr = array("is_deleted" => '1');
																			$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																		}														
																	}	
																}
																else 
																{
																	
																	if($sub_sec_response_type == 'checkbox') 
																	{
																		
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_question_qid,
																						"key_name" => $key,
																						"answer_value" => $value,
																						"file_name" => $imgUrl,
																						"salultation_value" => $salutation_name);
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");	
																		
																			
																	}
																	else 
																	{
																		$insertArr = array("response_id" => $cid,
																							"question_id" => $sub_question_qid,
																							"answer_value" => $value,
																							"file_name" => $imgUrl,
																							"salultation_value" => $salutation_name);
																						
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");				
																	}
																		
																	$cnt = $queryResponse->num_rows();
																	//die();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																	}
																}														
																	
															}
															
														}
													}
													
													
													
												}
												
												
												if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No' && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
												{
													
													$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $sub_question_qid,
																		"answer_value" => $suboption_dependent_answer['value'],
																		"salultation_value" => $salutation_name);
														//print_r($insertArr);		
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}	
												}
												
												
												if($sub_if_file_required == 'Yes' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No'  && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
												{
												
													$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
													
													$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
													
													$img_old_name1 = '';										
													if($image_name!="") 
													{
														$random_name2 = $survey_id."_".$sub_question_qid."_".time();
														$img_old_name1 = $this->generateImage($random_name2, $image_name);
													}
													
													
													$insertArr = array("response_id" => $cid,
																		"question_id" => $sub_question_qid,
																		"answer_value" => $suboption_dependent_answer['value'],
																		"salultation_value" => $salutation_name,
																		"file_name" => $img_old_name1);
																
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}	
												}
												
												if($sub_if_file_required == 'No' && $sub_upload_file_required == 'Yes' && $sub_input_more_than_1 == 'No'  && ($sub_sec_response_type == "radio" || $sub_sec_response_type == "checkbox"  || $sub_sec_response_type == "single_select" || $sub_sec_response_type == "file"))
												{	
											
													if($sub_sec_response_type == "file") 
													{
														$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
														
														$img_old_name1 = '';										
														if($image_name!="") 
														{
															$random_name2 = $survey_id."_".$sub_question_qid."_".time();
															$img_old_name1 = $this->generateImage($random_name2, $image_name);
														}
														
														$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
														$insertArr = array("response_id" => $cid,
																			"question_id" => $sub_question_qid,
																			"answer_value" => $img_old_name1);
																
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}
													}
													else 
													{
														
														$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
														$insertArr = array("response_id" => $cid,
																			"question_id" => $sub_question_qid,
																			"answer_value" => $suboption_dependent_answer['value'],
																			"salultation_value" => $salutation_name);
																
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
															
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
														}
													}
												
																							
												} 
												
												
												
												
												// Sub Section Global Questions 
												if(is_array($sub_global_question))
												{
													$sub_global_question_id 			= $sub_global_question['question_id'];
													$sub_global_parent_question_id 	= $sub_global_question['parent_question_id'];
													$sub_global_response_type 			= $sub_global_question['response_type'];
													
													if(is_array($sub_global_question['answer']))
													{										
														if(count($sub_global_question['answer']) > 0) 
														{
															if($sub_global_question['upload_file_required'] == 'Yes' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
															{
																
																$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																$image_name = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
														
																$img_old_name1 = '';										
																if($image_name!="") 
																{
																	$random_name2 = $survey_id."_".$sub_global_question_id."_".time();
																	$img_old_name1 = $this->generateImage($random_name2, $image_name);
																}
															
																foreach($sub_global_question['answer']['value'] as $key => $value) 
																{			

																	$img_old_name = '';										
																	if($key['upload_file']!="") 
																	{
																		$random_name = $survey_id."_".$sub_global_question_id."_".time();
																		$img_old_name = $this->generateImage($random_name, $value);
																	}
																	//Table Name : survey_response_headers
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_global_question_id,
																						"answer_value" => $value['value'],
																						"file_name" => $img_old_name1,
																						"salultation_value" => $salutation_name,
																						"input_file_required" => $img_old_name,
																						"parent_question_id" => $sub_global_parent_question_id);
																				
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value = '".$value['value']."'  AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																	}													
																												
																}
															}
															
															
															if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
															{	
															
																$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																$image_name = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																
																$img_old_name1 = '';										
																if($image_name!="") 
																{
																	$random_name2 = $survey_id."_".$sub_global_question_id."_".time();
																	$img_old_name1 = $this->generateImage($random_name2, $image_name);
																}
																
																foreach($sub_global_question['answer']['value'] as $key => $value) 
																{										
																	$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																											
																	//Table Name : survey_response_headers
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_global_question_id,
																						"key_name" => $key,
																						"answer_value" => $value,
																						"file_name" => $img_old_name1,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $sub_global_parent_question_id);
																						
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id, "key_name" => "'".$key."'"));
																	}	
																}
															} 
															
															
															if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
															{
																$image_name = isset($sub_global_question['image_url'])? $sub_global_question['image_url']:"";
																	
																foreach($sub_global_question['answer'] as $key => $value) 
																{										
																	//Table Name : survey_response_headers
																	$image_name = "";
																	
																	if($key == "image_url") 
																	{
																		
																	}
																	else 
																	{	
																		$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";												
																		
																		$insertArr = array("response_id" => $cid,
																				"question_id" => $sub_global_question_id,
																				"key_name" => $key,
																				"answer_value" => $value,
																				"file_name" => $image_name,
																				"salultation_value" => $salutation_name,
																				"parent_question_id" => $sub_global_parent_question_id);
																	
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id, "key_name" => "'".$key."'"));
																		}		
																	}
																}
															}
															
															
															if($sub_global_question['upload_file_required'] == 'Yes' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
															{ 	
																
																
																foreach($sub_global_question['answer']['value'] as $key => $value) 
																{										
																	$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																	
																	$upload_file = '';										
																	if($key['upload_file']!="") 
																	{
																		$random_name2 = $survey_id."_".$sub_global_question_id."_".time();
																		$upload_file = $this->generateImage($random_name2, $value);
																	}
																												
																	//Table Name : survey_response_headers
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_global_question_id,
																						"answer_value" => $value['value'],
																						"salultation_value" => $salutation_name,
																						"input_file_required" => $upload_file,
																						"parent_question_id" => $sub_global_parent_question_id);
																				
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value='".$value['value']."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																	}	
																}
															}
															
															if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
															{
																	
																$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_global_question_id,
																					"answer_value" => $sub_global_question['answer']['value'],
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $sub_global_parent_question_id);
																			
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																	}	
															}
															
															if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "radio" || $sub_global_response_type == "checkbox"  || $sub_global_response_type == "single_select" || $sub_global_response_type == "file"))
															{
																$upload_file = '';
																$imgUrl = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																	
																	foreach($sub_global_question['answer'] as $key => $value) 
																	{
																		$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																		
																		if($key == "image_url") 
																		{
																			
																		}
																		else 
																		{
																			
																			
																			if(is_array($value))
																			{
																				$chkArrpush = array();
																				foreach($value as $names => $namValue) 
																				{
																					
																					if($sub_global_question == 'checkbox') 
																					{
																						if(is_numeric($names))
																						{
																							$nm = "";
																						}
																						else 
																						{
																							$nm = $names;
																						}
																						$insertArr = array("response_id" => $cid,
																										"question_id" => $sub_global_question_id,
																										"key_name" => $nm,
																										"answer_value" => $namValue['value'],
																										"file_name" => $upload_file,
																										"salultation_value" => $salutation_name,
																										"parent_question_id" => $sub_global_parent_question_id);
																										
																						if($sub_global_response_type == "checkbox") 
																						{
																							
																							array_push($chkArrpush, $key);
																						}				
																					}
																					else 
																					{
																						$insertArr = array("response_id" => $cid,
																										"question_id" => $sub_global_question_id,
																										"answer_value" => $namValue['value'],
																										"file_name" => $upload_file,
																										"salultation_value" => $salutation_name,
																										"parent_question_id" => $sub_global_parent_question_id);
																					}
																					
																				
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'" );
																					
																					$cnt = $queryResponse->num_rows();
																					
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}
																						
																					if($flag == 0)
																					{
																						$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																					}	
																					else 
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "key_name" => "'".$names."'", "parent_question_id" => $sub_global_parent_question_id));
																					}														
																				}
																				// Delete Previous Answers regarding Checkbox and Selectbox
																				if($updateFlag)
																				{
																					if(count($chkArrpush) > 1) 
																					{						
																						$updatedAt = date('Y-m-d H:i:s');
																						$this->db->where_not_in('key_name', $chkArrpush);
																						$updateArr = array("is_deleted" => '1');
																						$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));	
																					}																					
																				}
																			}
																			else
																			{
																				$insertArr = array("response_id" => $cid,
																									"question_id" => $sub_global_question_id,
																									"answer_value" => $value,
																									"file_name" => $imgUrl,
																									"salultation_value" => $salutation_name,
																									"parent_question_id" => $sub_global_parent_question_id);
																			
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value='".$value."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'" );
																				
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "key_name" => $names, "parent_question_id" => $sub_global_parent_question_id));
																				}		
																			}	
																		}
																	}
																			
															}
															
															
															
															if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "radio" || $sub_global_response_type == "checkbox"  || $sub_global_response_type == "single_select" || $sub_global_response_type == "file"))
															{
																$upload_file = '';
																$imgUrl = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																	
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_global_question_id,
																					"answer_value" => $sub_global_question['answer']['value'],
																					"parent_question_id" => $sub_global_parent_question_id);
																			
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																	
																	$cnt = $queryResponse->num_rows();
																		
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																		
																	}	
															}
														
															
														} //End
													}	
													
												}  // Sub Section Global Section End	
												
												
												//echo "<pre>";print_r($dependent_question_node);
												if(is_array($dependent_question_node))
												{									
													if(count($dependent_question_node) > 0)
													{										
														foreach($dependent_question_node as $dptKey => $optionDependentQuestionlist) 
														{											
															$sub_option_question_qid 				= $optionDependentQuestionlist['option_dependent_question']['question_id'];
															$sub_sec_option_response_type 			= $optionDependentQuestionlist['option_dependent_question']['response_type'];
															$upload_option_file_required 			= $optionDependentQuestionlist['option_dependent_question']['upload_file_required'];
															$subsec_if_file_required 			= $optionDependentQuestionlist['option_dependent_question']['if_file_required'];
															$subsec_option_inputMoreThanOne 			= $optionDependentQuestionlist['option_dependent_question']['input_more_than_1'];
															//$parent_option_question_id 				= $optionDependentQuestionlist['option_dependent_question']['parent_question_id'];
															$parent_option_question_id 				= $sub_question_qid;
															$selectedOptionName = $dependent_question_node[$dptKey]['option'];
															$SubSecoption_option_dependent_salutation 	= $optionDependentQuestionlist['option_dependent_question']['salutation'];
															$suboption_option_dependent_answer 		= $optionDependentQuestionlist['option_dependent_question']['answer'];
															$dependent_option_question_node 		= $optionDependentQuestionlist['option_dependent_question'];
															
															
															if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "textbox" || $sub_sec_option_response_type == "textarea"))
															{ 
																
																$salutation_name = isset($SubSecoption_option_dependent_salutation['answer']['salutation'])? $SubSecoption_option_dependent_salutation['answer']['salutation']:"";
																
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_option_question_qid,
																					"answer_value" => $suboption_option_dependent_answer['value'],
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $sub_question_qid);
																		
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND is_deleted = '0' AND parent_question_id='".$sub_question_qid."'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																	}	
																	
															}
																
																
															// Latest Code Updated
															if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'Yes' && ($sub_sec_option_response_type == "textbox" || $sub_sec_option_response_type == "textarea"))
															{
																
																foreach($suboption_option_dependent_answer as $key => $value) 
																{										
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_option_question_qid,
																						"key_name" => $key,
																						"answer_value" => $value,
																						"parent_question_id" => $sub_question_qid);
																	
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$sub_question_qid."'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																	}
																}
																
															}
															
															
															if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
															{ 
																$salutation_name = isset($SubSecoption_option_dependent_salutation['answer']['salutation'])? $SubSecoption_option_dependent_salutation['answer']['salutation']:"";
																
																//$imgUrl = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																
																if($sub_sec_option_response_type == "file") 
																{
																	
																	$upload_file = '';										
																	if($suboption_option_dependent_answer['value']!="") 
																	{
																		$random_name2 = $survey_id."_".$sub_global_question_id."_".time();
																		$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																	}

																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_option_question_qid,
																						"answer_value" => $upload_file,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_option_question_id);
																				
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND parent_question_id='".$parent_option_question_id."' AND is_deleted = '0'");
																		
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id));
																		}	
																}
																else
																{	
																	if(count($suboption_option_dependent_answer)> 0)
																	{
																		$chkArrpush = array();
																		foreach($suboption_option_dependent_answer as $kCh => $value)
																		{
																			$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_option_question_qid,
																						"key_name" => $kCh,
																						"answer_value" => $value,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_option_question_id);
																			//echo "<pre>";print_r($insertArr);
																			if($sub_sec_option_response_type == "checkbox") 
																			{
																				
																				array_push($chkArrpush, $kCh);
																			}	
																			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND parent_question_id='".$parent_option_question_id."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																			
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id, "key_name" =>  "'".$kCh."'"));
																				//echo ">>>For Checkbox Multiple==="$this->db->last_query();
																			}
																		}
																		
																		
																		// Delete Previous Answers regarding Checkbox and Selectbox
																		if($updateFlag)
																		{
																			if(count($chkArrpush) > 1) 
																				{
																					$updatedAt = date('Y-m-d H:i:s');
																					$this->db->where_not_in('key_name', $chkArrpush);
																					$updateArr = array("is_deleted" => '1');
																					$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id));
																				}														
																		}
																	}
																}	
																												
															}
															
															
															if($upload_option_file_required == 'Yes' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
															{ 
														
																$salutation_name = isset($suboption_option_dependent_answer['answer']['salutation'])? $suboption_option_dependent_answer['answer']['salutation']:"";
																$imgUrl = isset($suboption_option_dependent_answer['image_url'])? $suboption_option_dependent_answer['image_url']:"";
																
																$file_file = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$sub_option_question_qid."_".time();
																	$file_file = $this->generateImage($random_name2, $imgUrl);
																}
																
																if($sub_sec_option_response_type == "file") 
																{
																	
																	$upload_file = '';										
																	if($suboption_option_dependent_answer['value']!="") 
																	{
																		$random_name2 = $survey_id."_".$sub_option_question_qid."_".time();
																		$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																	}
																	
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_option_question_qid,
																					"answer_value" => $upload_file,
																					"file_name"	=> $file_file,
																					"parent_question_id" => $parent_option_question_id);
																			
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND is_deleted = '0'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid,"parent_question_id" => $parent_option_question_id));
																				}
																
																}
																else 
																{
																	
																	if(is_array($suboption_option_dependent_answer['value']))
																	{	$chkArrpush = array();
																		foreach($suboption_option_dependent_answer['value'] as $kCh => $value)
																		{
																			$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_option_question_qid,
																					"key_name" => $kCh,
																					"answer_value" => $value,
																					"file_name"	=> $file_file,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $sub_question_qid);
																			
																			if($sub_sec_option_response_type == "checkbox") 
																			{
																				
																				array_push($chkArrpush, $key);
																			}
																			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "key_name" => "'".$kCh."'", "parent_question_id" => $parent_option_question_id));
																			}
																		}
																		
																		// Delete Previous Answers regarding Checkbox and Selectbox
																		if($updateFlag)
																		{
																			if(count($chkArrpush) > 1) 
																			{
																				$updatedAt = date('Y-m-d H:i:s');
																				$this->db->where_not_in('key_name', $chkArrpush);
																				$updateArr = array("is_deleted" => '1');
																				$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																			}														
																		}
																	}
																}						
															}
														
															if($upload_option_file_required == 'No' && $subsec_if_file_required == 'Yes' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
															{ 
														
																$salutation_name = isset($suboption_option_dependent_answer['answer']['salutation'])? $suboption_option_dependent_answer['answer']['salutation']:"";
																$imgUrl = isset($suboption_option_dependent_answer['image_url'])? $suboption_option_dependent_answer['image_url']:"";
																
																$file_file = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$sub_option_question_qid."_".time();
																	$file_file = $this->generateImage($random_name2, $imgUrl);
																}
																
																if($sub_sec_option_response_type == "file") 
																{
																	
																	$upload_file = '';										
																	if($suboption_option_dependent_answer['value']!="") 
																	{
																		$random_name2 = $survey_id."_".$sub_option_question_qid."_".time();
																		$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																	}
																	
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_option_question_qid,
																					"answer_value" => $upload_file,
																					"file_name"	=> $file_file,
																					"parent_question_id" => $parent_option_question_id);
																			
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND is_deleted = '0'");
																					$cnt = $queryResponse->num_rows();
																					
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid,"parent_question_id" => $parent_option_question_id));
																				}
																
																}
																else 
																{
																	
																	if(is_array($suboption_option_dependent_answer['value']))
																	{
																		$chkArrpush = array();
																		foreach($suboption_option_dependent_answer['value'] as $kCh => $value)
																		{
																			$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_option_question_qid,
																					"key_name" => $kCh,
																					"answer_value" => $value,
																					"file_name"	=> $file_file,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $sub_question_qid);
																			
																			
																			if($sub_sec_option_response_type == "checkbox") 
																			{
																				
																				array_push($chkArrpush, $kCh);
																			}
																			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND parent_question_id = '".$sub_question_qid."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "key_name" => "'".$kCh."'", "parent_question_id" => $sub_question_qid));
																			}
																		}
																		
																		// Delete Previous Answers regarding Checkbox and Selectbox
																		if($updateFlag)
																		{
																			if(count($chkArrpush) > 1) 
																			{
																				$updatedAt = date('Y-m-d H:i:s');
																				$this->db->where_not_in('key_name', $chkArrpush);
																				$updateArr = array("is_deleted" => '1');
																				$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																			}														
																		}
																	}
																}						
															}
														}
													}
												}   // Sub Section Dependent Question
												
												
											}										
										}
									}
									
								} // End Sub Section End
								
							
							
							// Log Data Added
							$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
							$ipAddr		= $this->master_model->get_client_ip();						
							$postArr			= $_POST;	
							$json_encode_data 	= json_encode($postArr);
							$logData = array('user_id' => $surveyer_id,'action_name'=> "Add",'module_name'=> 'API CALL',
											'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
							//$this->master_model->insertRecord('api_log',$logData);
							
							$res = array();
							$this->api_log($postArr, $res);
							
							
							$response['status'] 	= "1";
							$response['message'] 	= "Survey Response Saved Successfully";
							$response['data']		= array("response_id" => $cid, "ref_id" =>$ref_id);
							
						
						/*else
						{						
							$response['status'] 	= "0";
							$response['message'] 	= "No Section Questions Found";
						}*/
							
					}	// Section Foreach End
				}
				else
				{
					//echo "No Sections Found";
					$response['status'] 	= "0";
					$response['message'] 	= "No Sections Found";
				}
		
			}
			else 
			{
				//echo "No Data Found";
				$response['status'] 	= "0";
				$response['message'] 	= "Survey ID does not exist";
			}
			
		}
		else
		{
			//echo "No Data Found";
			$response['status'] 	= "0";
			$response['message'] 	= "No Data Found";
		}
		
		$this->response($response);
		
	} // Function End
	
	// function to user logout
	public function logout()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		/*if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "All fields are mandatory.";
			
		} else {*/			
			if(!empty($user_id) && is_user_login($user_id))
            {
				$updated_date = date("Y-m-d H:i:s");

				$sql = "UPDATE survey_users SET is_login = '0', updated_on = '".$updated_date."' WHERE user_id = '".$user_id."'";
				if($result = $this->db->query($sql))
				{
					$response['status']		= "1";
					$response['message'] 	= "User logout successfully.";
				}
				else
				{
					$response['status'] 	= "1";
					$response['message'] 	= "No data Found.";
				}
			}
			else
			{
				$response['status']		= "0";
				$response['message']    = "User is not login.";
			}
		//}
		
		$this->response($response);
	}
	
	public function generateImage($random_name, $image_name)
	{
		define('UPLOAD_DIR', 'uploads/');
		//Survey_id_response_id_question_id
		$imgName = $random_name.'.jpg';
		$file_name = UPLOAD_DIR.$imgName;
		
		if (base64_encode(base64_decode($image_name, true)) === $image_name) {
			$image_base64 = base64_decode($image_name);
			//$file_name = UPLOAD_DIR . 'new.jpg';
			file_put_contents($file_name, $image_base64);		
			return $imgName;
		}
		else 
		{
			$imgName = '';
			return $imgName;
		}	
		
		/*$image_base64 = base64_decode($image_name);
		//$file_name = UPLOAD_DIR . 'new.jpg';
		file_put_contents($file_name, $image_base64);		
		return $imgName;		
		return 1;*/
	}
	
	// function to send JSON response -
	public function response($response)
	{
		// log api access -
		$this->api_log($_POST, $response);
		
		@header('Content-type: application/json');
		echo json_encode($response);
		
		die();
	}
	
	// function to log api access
	public function api_log($req, $res)
	{
		// get api user agent -
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		
		$req = json_encode($req);
		$res = json_encode($res);
		
		$access_date = date("Y-m-d H:i:s");
		
		//$query = $this->db->query("INSERT INTO api_log(user_agent, req, res, access_date) VALUES('$user_agent', '$req', '$res', '$access_date')");
		
		$headers = "";
		foreach (@getallheaders() as $name => $value) { 
			//echo "$name: $value \n";
			$headers .= "$name: $value \n";
		}
		
		// $_SERVER
		$server = print_r($_SERVER, TRUE);
		
		$myfile = @fopen("logs_".date("dmY").".txt", "a") or die("Unable to open file!");
		$txt = "
		=================== ".$access_date." ====================
					
		# Headers:
		".$headers."
		
		# Server:
		".$server."
		
		# Request:
		".$req."
		
		# Response:
		".$res."
		";
		@fwrite($myfile, $txt);
		@fclose($myfile);
	}
	
} // Class End
