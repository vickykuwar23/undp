<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper('captcha');
		$this->load->helper('security');
		
		$this->load->library("session");
    }
	
	public function index()
	{		
		redirect(base_url('xAdmin/dashboard'));
		/*$data['page_title']='Home';
		$data['middle_content']='index';
		$this->load->view('front/front_combo',$data);*/
	}
	
	// Terms & Conditions
	public function termsOfuse()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"4"));

		$dataArr = array();		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		
		$data['privacy_data']= $dataArr;
		$data['page_title']='Terms';
		$data['middle_content']='terms';
		$this->load->view('front/front_combo',$data);
	}	
	
	// About Us
	public function aboutUs()
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"1"));

		$dataArr = array();		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		$consortium_mgt  = $this->master_model->getRecords('partners',array('status'=>"Active", 'partner_type' => "C"),'',array('xOrder' => "ASC"));
		// Consortium Partners 
		$consortArr = array();
		if(count($consortium_mgt)){				
					
			foreach($consortium_mgt as $con_val){
				
				$con_val['partner_name'] 	= $encrptopenssl->decrypt($con_val['partner_name']);
				$con_val['partner_img'] 	= $encrptopenssl->decrypt($con_val['partner_img']);
				$consortArr[] = $con_val;
				
			}
			
		}
		$data['consortium_list']= $consortArr;
		$data['privacy_data']= $dataArr;
		$data['page_title']='About';
		$data['middle_content']='about';
		$this->load->view('front/front_combo',$data);
	}
	
	// Contact Us
	public function contactUs()
	{	
		error_reporting(E_ALL);
		$encrptopenssl =  New Opensslencryptdecrypt();		
		$privacy_data = $this->master_model->getRecords('cms',array('id'=>"2"));
		$data['country_codes']  = $this->master_model->getRecords('country','','',array('iso'=>'asc'));
		$dataArr = array();	
		
		if(count($privacy_data)){
				
				$privacy_data['page_title'] 		= $encrptopenssl->decrypt($privacy_data[0]['page_title']);
				$privacy_data['page_description'] 	= $encrptopenssl->decrypt($privacy_data[0]['page_description']);
				$dataArr[] = $privacy_data;			
		}
		
		// Check Validation
		$this->form_validation->set_rules('cname', 'Fullname', 'required|min_length[2]|xss_clean');
		$this->form_validation->set_rules('email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');	
		$this->form_validation->set_rules('contents', 'Description', 'required|xss_clean');
		$this->form_validation->set_rules('mobile', 'Mobile No', 'required|xss_clean');
		$this->form_validation->set_rules('country_code', 'Country Code', 'required|xss_clean');
		$this->form_validation->set_rules('captcha', 'Capcha', 'trim|required|callback_check_captcha_code|xss_clean');
		if($this->form_validation->run())
		{
			$cname 			= $encrptopenssl->encrypt($this->input->post('cname'));
			$email_id 		= $encrptopenssl->encrypt($this->input->post('email_id'));
			$subject  		= $encrptopenssl->encrypt($this->input->post('subject'));
			$contents  		= $encrptopenssl->encrypt($this->input->post('contents'));
			$country_code	= $encrptopenssl->encrypt($this->input->post('country_code'));
			$mobile  		= $encrptopenssl->encrypt($this->input->post('mobile'));
			$combineMo      = $encrptopenssl->encrypt($this->input->post('country_code')."-".$this->input->post('mobile'));
			$c_c 			= $this->input->post('country_code');
			$inputCaptcha 	= $this->input->post('captcha');
			$countryCodeValue = $this->master_model->getRecords('country',array('id'=>$c_c));
			$ccVal			= $countryCodeValue[0]['phonecode'];
			
			$contactCode	= $this->input->post('country_code');
			$contactName	= ucwords($this->input->post('cname'));
			$contactEmail	= $this->input->post('email_id');
			$contactSubject	= $this->input->post('subject');
			$contactDesc	= $this->input->post('contents');
			$contactPhone1	= $this->input->post('mobile');
			$contactPhone	= $ccVal."-".$contactPhone1;
			
			$insertArr = array( 
								'fullname' 			=> $cname, 
								'email_id' 			=> $email_id,
								'country_code'		=> $country_code,
								'mobile_no' 		=> $mobile,
								'subject_details' 	=> $subject,
								'content_received' 	=> $contents
								
								);		
			
			// Insert SQL For Challenge	
			$insertQuery = $this->master_model->insertRecord('contactus',$insertArr);
			
			if($insertQuery){
				
				// Log Capture
			$postArr			= $this->input->post();
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->get_client_ip();
			$createdAt			= date('Y-m-d H:i:s');
			$user_id 			= $this->session->userdata('user_id');
			$logDetails 	= array(
									'user_id' 		=> $user_id,
									'action_name' 	=> "Add",
									'module_name'	=> 'ContactUs',
									'store_data'	=> $json_encode_data,
									'ip_address'	=> $ipAddr,
									'createdAt'		=> $createdAt
								);
			$logData = $this->master_model->insertRecord('logs',$logDetails);
			
			
			// Contact User Email Functionality
				$slug 			= $encrptopenssl->encrypt('contact_us_subscriber');
				$contact_mail 	= $this->master_model->getRecords("email_template", array("slug" => $slug));
				$setting_table 	= $this->master_model->getRecords("setting", array("id" => '1'));
				$content = '';
				if(count($contact_mail) > 0){	
					
					$subject 	 	= $encrptopenssl->decrypt($contact_mail[0]['email_title']);
					$description 	= $encrptopenssl->decrypt($contact_mail[0]['email_description']);
					$from_admin 	= $encrptopenssl->decrypt($contact_mail[0]['from_email']);				
					$from_names 	= 'Admin';
					$sendername  	= $encrptopenssl->decrypt($setting_table[0]['field_1']);
					
					$arr_words = ['[USERNAME]', '[SIGNATURE]'];
					$rep_array = [$contactName,  $sendername];
					
					$content = str_replace($arr_words, $rep_array, $description);
					
				}			
				
				//email admin sending code 
				$info_arr=array(
						'to'		=>	$contactEmail,					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject,
						'view'		=>  'common-file'
						);
				
				$other_info=array('content'=>$content); 
				
				$emailsend=$this->emailsending->sendmail($info_arr,$other_info);
				
				/********************************/
				
				// Admin Email Notification
				$slug_1 			= $encrptopenssl->encrypt('admin_alert_contact');
				$contact_admin_mail = $this->master_model->getRecords("email_template", array("slug" => $slug_1));
				$setting_table_1 	= $this->master_model->getRecords("setting", array("id" => '1'));
				$content_1 = '';
				if(count($contact_admin_mail) > 0){	
					
					$subject_1 	 	= $encrptopenssl->decrypt($contact_admin_mail[0]['email_title']);
					$description_1 	= $encrptopenssl->decrypt($contact_admin_mail[0]['email_description']);
					$from_admin_1 	= $encrptopenssl->decrypt($contact_admin_mail[0]['from_email']);
					$from_admin_2 	= $encrptopenssl->decrypt($contact_admin_mail[0]['cc_email_id']);	
					$from_names_1 	= 'Admin';
					$sendername_1  	= $encrptopenssl->decrypt($setting_table_1[0]['field_1']);					
					
					$arr_words_1 = ['[APPLICANTNAME]', '[NAME]', '[EMAIL]', '[SUBJECT]', '[DETAILS]', '[MOBILE]', '[SIGNATURE]'];
					$rep_array_1 = [$contactName,  $contactName, $contactEmail, $contactSubject, $contactDesc, $contactPhone, $sendername_1];
					
					$content_1 = str_replace($arr_words_1, $rep_array_1, $description_1);
					
				}			
				
				//email admin sending code 
				$info_arr_1 =array(
						'to'		=>	$from_admin_1, //$from_admin_1					
						'cc'		=>	'',
						'from'		=>	$from_admin,
						'subject'	=> 	$subject_1,
						'view'		=>  'common-file'
						);
				
				$other_info_1=array('content'=>$content_1); 
				
				$emailsend=$this->emailsending->sendmail($info_arr_1,$other_info_1);
				
			} // Insert Query END			
			
			
			$this->session->set_flashdata('success','Contact form successfully submitted.');
			
			redirect(base_url('home/contactUs'));
		}
		
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];
		
		$data['contact_data']= $dataArr;
		$data['page_title']='Contact';
		$data['middle_content']='contactus';
		$this->load->view('front/front_combo',$data);
	}
	
	// Check Captcha Code
	public function check_captcha_code($code)
	{		
	  // check if captcha is set -
		if($code == '')
		{
			$this->form_validation->set_message('check_captcha_code', '%s is required.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($code == '' || $_SESSION["captchaCode"] != $code)
		{  
			$this->form_validation->set_message('check_captcha_code', 'Invalid %s.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($_SESSION["captchaCode"] == $code)
		{   
			$this->session->unset_userdata("captchaCode");
			$this->session->set_userdata("captchaCode", rand(1,100000));
		
			return true;
		}
		
	}
	
	// FAQ Page
	public function faq($file_name = NULL)
	{	
		$encrptopenssl =  New Opensslencryptdecrypt();

		$this->db->order_by('FIELD(xOrder, "0") ASC, xOrder ASC, updatedAt DESC', FALSE);	
		$faq_data = $this->master_model->getRecords('faq',array('status'=>"Active"));

		$dataArr = array();		
		if(count($faq_data)){
			
				foreach($faq_data as $row_val){
					
				$row_val['faq_title'] 	= $encrptopenssl->decrypt($row_val['faq_title']);
				$row_val['faq_desc'] 	= $encrptopenssl->decrypt($row_val['faq_desc']);
				$row_val['faq_img'] 	= $encrptopenssl->decrypt($row_val['faq_img']);
				$dataArr[] = $row_val;

				}	
		}
		
		if(isset($_POST['faq_btn']) && !empty($_POST['faq_btn'])){			
			$file = $this->input->post('faq_file');
			$file_name = base_url('assets/faq/'.$file);			
			// Live Envirnment	
			$root = 'assets/faq/';					
			$file_name = $root.$file;		
			if($file_name){ 
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_name));
				flush(); // Flush system output buffer
				readfile($file_name);
				die();
			} else { 
				http_response_code(404);
			}
		}
		
		
		
		$data['faq_data']= $dataArr;
		$data['page_title']='FAQ';
		$data['middle_content']='faq';
		$this->load->view('front/front_combo',$data);
	}
	
	// Create Captcha
	public function create_captcha_code($value='')
	{
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Send captcha image to view
		return $captcha['image'];
	}

	// Unique Email Address
	public function unique_email($email) 
	{		
		$encrptopenssl =  New Opensslencryptdecrypt();
		$email=$encrptopenssl->encrypt($email);
			$user  = $this->master_model->getRecords('newsletter',array('email'=>$email));
			if (count($user)>0) {
				$this->form_validation->set_message('unique_email', '%s already exist.'); 
				return false;
			}else{
				return true;
			}
		
	}

	// Check Captch
	public function check_captcha($code) 
	{
		if($code == '' || $_SESSION["mycaptcha"] != $code    )
		{
			$this->session->unset_userdata("mycaptcha");
			$this->form_validation->set_message('check_captcha', 'Invalid %s.'); 
			return false;
		}
		if($_SESSION["mycaptcha"] == $code && $this->session->userdata("used")==0)
		{
			$this->session->unset_userdata("mycaptcha");
			return true;
		}		
	}

	public function refresh(){
        // Captcha configuration
        $config = array(
      		'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 6,
			'font_size'     => 17,
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
        );
        $captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('mycaptcha');
        $this->session->set_userdata('mycaptcha',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
	}	
		
	
	
   // Test Email
    public function testMail(){
	   $fromemail = "vicky.kuwar@esds.co.in";
		$fromadmin = "abc@gmail.com";
		$sub_content =" Testin Mail";	
			$info_array=array(
					'to'		=>	$fromemail,					
					'cc'		=>	'',
					'from'		=>	$fromadmin,
					'subject'	=> 	"Tesat Mail",
					'view'		=>  'subscription'
					);
			
			$other_infoarray	=	array('content' => $sub_content); 
			
			$email_send = $this->emailsending->sendmail($info_array,$other_infoarray);
			
			if($email_send)
			{
				echo "Success";die();
			} else {
				echo "Fail";die();
			}
	   
   }
   
   // Get IP Address
   public function get_client_ip() {
		$ipaddress = '';
		if (getenv('HTTP_CLIENT_IP'))
			$ipaddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipaddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipaddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
		   $ipaddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipaddress = getenv('REMOTE_ADDR');
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
} // Class End
