<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 0");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization");

class Api_v3 extends CI_Controller {

	function __construct() {
        parent::__construct();
		
		//error_reporting(0);
		
		// set default time zone -
		//date_default_timezone_set('Asia/Kolkata');
		
  		$this->load->database();		
  		$this->load->helper('url');
		$this->load->helper('file');		
  		$this->load->library('session');		
		
		ini_set('max_execution_time', 0);
		ini_set('max_input_time', 0);
		ini_set('upload_max_filesize', "1000M");
		ini_set('post_max_size', "1000M");
		ini_set('memory_limit','-1');
		ignore_user_abort(true);
		set_time_limit(0);
		
		$this->log_id = 0;	
    }
	
	// Main Function By Default Execute
	public function index()
	{												
		$response = array("status" => 0, "message" => "");
		
		// check request type -
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			//echo "<pre>"; print_r($_SERVER); die();
			
			// get API authentication token from request header
			$token = getBearerToken();
				
			// check if authentication token
			if($token != null)
			{
				// validate API authentication token from request header
				if(validateToken($token))
				{
					$_POST = json_decode(file_get_contents("php://input"), TRUE);
										
					//$postData = $this->sql_clean($this->db->conn_id, $_POST);
					//echo "<pre>";print_r($postData);die();
					
					$res = array();
					$this->log_id = $this->api_log($_POST, $res, FALSE);
					
					if(isset($_POST['action']) && $_POST['action'] != "")
					{  
						// get tag value -
						$action = $_POST['action'];
						
						if($action == "login")
						{
							$this->login();
						}
						else if($action == "forgot_password")
						{
							$this->forgot_password();
						} 	
						else if($action == "verify_otp")
						{
							$this->verify_otp();
						} 
						else if($action == "change_password")
						{
							$this->change_password();
						} 
						else if($action == "logout")
						{
							$this->logout();
						} 
						else if($action == "get_district_list")
						{ 
							$this->get_district_list();
						}
						else if($action == "get_revenue_list")
						{ 
							$this->get_revenue_list(); 
						}
						else if($action == "get_survey_list")
						{ 
							$this->get_survey_list(); 
						}
						else if($action == "get_category_list")
						{ 
							$this->get_category_list(); 
						}
						else if($action == "get_all_block")
						{ 
							$this->get_all_block();
						}	
						else if($action == "get_all_panchayat")
						{ 
							$this->get_all_panchayat();
						}
						else if($action == "get_all_district_list")
						{ 
							$this->get_all_district_list();
						}
						else if($action == "get_all_revenue_list")
						{ 
							$this->get_all_revenue_list();
						}
						else if($action == "get_all_block_list")
						{ 
							$this->get_all_block_list();
						}						
						else if($action == "get_all_village_list")
						{ 
							$this->get_all_village_list();
						}
						else if($action == "survey_district_list")
						{ 
							$this->survey_district_list();
						}
						else if($action == "survey_revenue_circle_list")
						{ 
							$this->survey_revenue_circle_list();
						}
						else if($action == "survey_block_list")
						{ 
							$this->survey_block_list(); 
						}
						else if($action == "survey_panchayat_list")
						{ 
							$this->survey_panchayat_list(); 
						}	
						else if($action == "survey_village_list")
						{ 
							$this->survey_village_list(); 
						}	
						else if($action == "survey_questionire")
						{ 
							$this->survey_questionire();
						}
						else if($action == "survey_questionire_v1")
						{ 
							$this->survey_questionire_v1();
						}	
						else if($action == "save_response")
						{ 
							@$this->save_response();
						}
						else if($action == "survey_statistics")
						{ 
							$this->survey_statistics();
						}	
						else if($action == "send_mail_to_approver")
						{ 
							$this->send_mail_to_approver();
						}
						else if($action == "check_update")
						{ 
							$this->check_update();
						}
						else if($action == "survey_response_list")
						{ 
							$this->survey_response_list();
						} 
						else if($action == "survey_response_details")
						{ 
							$this->survey_response_details();
						}
						else
						{
							$response['status']		= "0";
							$response['message'] 	= "Invalid request method.";
							
							$this->response($response);
						}
					}
					else
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Request can not be null.";
						
						$this->response($response);
					}
				}
				else
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Invalid api key.";
					
					$this->response($response);
				}
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "API authentication missing.";
				
				$this->response($response);
			}
		}
		else
		{
			$response['status'] 	= "0";
			$response['message'] 	= "Invalid request type. Must use Request type as POST.";
			
			$this->response($response);
		}
	}
	
	// function to login user
	public function login()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$username	= isset($_POST['username']) ? $_POST['username'] : '';
		$password 	= isset($_POST['password']) ? $_POST['password'] : '';
	
		if(empty($username) || empty($password))
		{
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Username or password can not be blank.";
		}
		else
		{
			// encrypt password using sha1() -
			$password = md5($password);
			
			//surveyor_id
			$role_id = $this->config->item('surveyor_role_id');
			
			// check login username and password -
			$query = $this->db->query("SELECT first_name, last_name, user_id, session_token, username, email, role_id, district_id, revenue_id, status, is_login FROM survey_users WHERE role_id= '$role_id' AND username = ".$this->db->escape($username)." AND password = '$password' AND is_deleted = '0'");
			$cnt = $query->num_rows();
			//echo $this->db->last_query();
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					if($row['status'] == 'Active')
					{
						if($row['is_login'] == '0')
						{
							if($row['role_id'] == 999){
								$fullname = "Super Admin";
							} else {
								$fullname = ucfirst($row['first_name'])." ".ucfirst($row['last_name']);
							}
							
							$user_id = $row['user_id'];
							$edited_date = $last_login_on = date("Y-m-d H:i:s");
	
							$sql_update = "UPDATE survey_users SET is_login = '1', last_login_on = '".$last_login_on."', updated_on = '".$edited_date."' WHERE user_id = '".$user_id."'";
							$query_update = $this->db->query($sql_update);
							
							$session_token = generateSessionToken();
							setUserSessionToken($session_token, $user_id);
							
							//$data = array('session_token' => $session_token, 'username' => $row['username']);
							$data = array('session_token' => $session_token, 'username' => $row['username'], 'fullname' => $fullname, 'email' => $row['email'], 'role_id' => $row['role_id'], 'district_id' => $row['district_id'], 'revenue_id' => $row['revenue_id'], 'user_id' => $row['user_id']);
							
							$response['status'] 	= "1";
							$response['message']	= "User logged in successfully.";
							$response['data'] 		= $data;
						}
						else
						{
							$response['status'] 	= "0";
							$response['message'] 	= "User is already logged in into another device.";
						}
					}
					else
					{
						$response['status'] 	= "0";
						$response['message'] 	= "User status is Inactive.";
					}
				}			
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "Invalid username or password.";
			}
		}
		
		$this->response($response);
	}
	
	// Check Update
	public function check_update()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$device_token	= isset($_POST['device_token']) ? $_POST['device_token'] : '';
		$apk_version 	= isset($_POST['apk_version']) ? $_POST['apk_version'] : '';
	
		//if(empty($device_token) || empty($apk_version))
		if(empty($apk_version))
		{
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Required fields can not be blank.";
		}
		else
		{
			// get logged in user id
			$user_id = getUserIdForSessionToken();			
						
			// check login username and password -
			$query = $this->db->query("SELECT user_id, status FROM survey_users WHERE user_id = '$user_id' AND is_deleted = '0'");
			$cnt = $query->num_rows();			
			
			$config_apk_version  = $this->config->item('apk_version');			
			$is_notice_available = $this->config->item('is_notice_available');
			$notice_text 		 = $this->config->item('notice_text');
			
			$is_update_available = 0;
			if($apk_version != $config_apk_version) 
			{
				$is_update_available = 1;
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					if($device_token)
					{
						$edited_date = $last_login_on = date("Y-m-d H:i:s");
	
						$sql_update = "UPDATE survey_users SET device_token = '".$device_token."', updated_on = '".$edited_date."' WHERE user_id = '".$user_id."'";
						$query_update = $this->db->query($sql_update);
					}
				
					$data = array('is_update_available' => $is_update_available, 'is_notice_available' => $is_notice_available, 'notice_text' => $notice_text);
					
					$response['status'] 	= "1";
					$response['message'] 	= "Device token updated successfully.";
					$response['data'] 		= $data;
				}			
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "Invalid user.";
			}
		} //Else Part
		
		$this->response($response);
	}
	
	// Get Survey District List
	public function survey_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID can not be blank.";
		} else if(empty($user_id)){
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
		} 
		else 
		{
			$education_survey_id = $this->config->item('education_survey_id');
			
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
					
					$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
					$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE status = 'Active' AND panchayat_id IN('".$panchayatArr."')");
					
					$countRes = $query_1->num_rows();
					
					if($countRes > 0)
					{ 
						$blockList = array();
						foreach($query_1->result_array() as $row)
						{
							array_push($blockList, $row['block_id']);
						}
						
						$blockArr = "" . implode( "','", $blockList ) . "";
					
						$query_2 = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE status = 'Active' AND block_id IN('".$blockArr."')");
						
						$countDistrict = $query_2->num_rows();
						
						if($countDistrict > 0)
						{ 
							$districtList = array();
							foreach($query_2->result_array() as $row)
							{	
								array_push($districtList, $row['district_id']);
							}
						}
					
						// Get District Result 				
						$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
						$disRow = $surveyDistrict->num_rows();
									
						$dList = "";
						if($disRow > 0)
						{ 
							//$dList = array();
							foreach($surveyDistrict->result_array() as $row)
							{
								//array_push($dList, $row['district_id']);
								$dList = $row['district_id'];
							}
							
							$user_district_id = explode(",",$dList);					
							$commonDistrict = array_intersect($user_district_id, $districtList);	
							$outputString = "" . implode( "','", $commonDistrict ) . "";				
							$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND district_id IN('".$outputString."')");
							$cnt = $query->num_rows();
						}
						else 
						{
							$response['status'] 	= "0";
							$response['message'] 	= "Survey district data not found.";
						}
					}
					else 
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Survey block data not found.";
					}
				}				
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Survey Gaon/Panchyat data not found.";
				}
			} 
			else 
			{
				$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $surveyDistrict->num_rows();			
				
				if($cntRow > 0)
				{ 
					$districtList = array();
					foreach($surveyDistrict->result_array() as $row)
					{
						array_push($districtList, $row['district_id']);
					}
	
					$district_result = explode(",",$districtList[0]);				
					$outputString = "" . implode( "','", $district_result ) . "";
					
					$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND district_id IN('".$outputString."')");
					$cnt = $query->num_rows();
				}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Survey district data not found.";
				}
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Surevy district list get successfully.";
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No data found.";
			}
		}
		
		$this->response($response);
	}
	
	// Get Survey Revenue Circle List
	public function survey_revenue_circle_list() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
		} else {
			$surveyRevenue = $this->db->query("SELECT revenue_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0'");
			$cntRow = $surveyRevenue->num_rows();			
			
			if($cntRow > 0)
			{ 
				$revenueList = array();
				foreach($surveyRevenue->result_array() as $row)
				{
					array_push($revenueList, $row['revenue_id']);
				}
				
				$revenue_result = explode(",",$revenueList[0]);			
				$outputString = "" . implode( "','", $revenue_result ) . "";
				
				$query = $this->db->query("SELECT revenue_id, revenue_code, revenue_name  FROM survey_revenue_master WHERE status = 'Active' AND revenue_id IN('".$outputString."')");
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{
						$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'revenue_name' => $row['revenue_name']);
					}
					
					$response['status'] 	= "1";
					$response['message'] 	= "Revenue Circle List Get Successfully.";
				}
				else
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No data Found.";
				}
			}
			else 
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get District List -
	public function get_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$state_id	= isset($_POST['state_id']) ? $_POST['state_id'] : '';
		
		if(empty($state_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State name is required.";
			
		} else {
			
			$query = $this->db->query("SELECT district_id, district_name  FROM survey_district_master WHERE status = 'Active' AND state_id = '".$state_id."'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get All District List -
	public function get_all_district_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if($survey_id == $education_survey_id) 
		{ 
			$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
			$cntRow = $villageMaster->num_rows();
			
			if($cntRow > 0)
			{ 
				$panchyatList = array();
				foreach($villageMaster->result_array() as $row)
				{
					array_push($panchyatList, $row['panchayat_id']);
				}
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";			
				$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
				
				$countRes = $query_1->num_rows();
				
				if($countRes > 0)
				{ 
					$blockList = array();
					foreach($query_1->result_array() as $row)
					{
						array_push($blockList, $row['block_id']);
					}
					
					$blockArr = "" . implode( "','", $blockList ) . "";
					$query_2 = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE block_id IN('".$blockArr."')");
					$countDistrict = $query_2->num_rows();			
					if($countDistrict > 0)
					{ 
						$districtList = array();
						foreach($query_2->result_array() as $row)
						{
							
							array_push($districtList, $row['district_id']);
						}
						
						$outputString = "" . implode( "','", $districtList ) . "";				
						$query = $this->db->query("SELECT district_id, district_name, status, is_deleted  FROM survey_district_master WHERE district_id IN('".$outputString."')");
						$cnt = $query->num_rows();
						
						if($cnt > 0)
						{
							foreach($query->result_array() as $row)
							{
								$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
							}
							
							$response['status'] 	= "1";
							$response['message'] 	= "District List Get Successfully.";
						}
						else
						{
							$response['status'] 	= "0";
							$response['message'] 	= "No data Found.";
						}
					}
					else 
					{
						$response['status'] 	= "0";
						$response['message'] 	= "No District Data Found For This Survey.";
					}
				}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No Block Data Found For This Survey.";
				}
			}				
			else 
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No Gaon/Panchyat Data Found For This Survey.";
			}
		}
		else 
		{
			$query = $this->db->query("SELECT * FROM survey_district_master");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('district_id' => $row['district_id'], 'district_name' => $row['district_name'], 'state_id' => $row['state_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "District List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}		
		
		$this->response($response);
	}
	
	// function to Get Revenue Circle List
	public function get_revenue_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		if(empty($district_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "District name is required.";
			
		} else {
			
			$query = $this->db->query("SELECT revenue_id, revenue_code, revenue_name, district_id FROM survey_revenue_master WHERE status = 'Active' AND district_id = '".$district_id."' AND is_deleted='0'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'revenue_name' => $row['revenue_name'], 'district_id' => $row['district_id']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Revenue List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get All Revenue Circle List
	public function get_all_revenue_list()
	{
		$response = array("status" => 0, "message" => "");		
		$query = $this->db->query("SELECT revenue_id,revenue_code,district_id,revenue_name  FROM survey_revenue_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('revenue_id' => $row['revenue_id'], 'revenue_code' => $row['revenue_code'], 'district_id' => $row['district_id'], 'revenue_name' => $row['revenue_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
			}
			
			$response['status'] 	= "1";
			$response['message'] 	= "Revenue List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	
	// function to Get Development Block List
	public function survey_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id))
		{			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else if(empty($district_id))
		{
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "District name is required.";
			
		} 
		else 
		{
			$education_survey_id = $this->config->item('education_survey_id');
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";				
				$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE status = 'Active' AND panchayat_id IN('".$panchayatArr."')");				
				$countRes = $query_1->num_rows();
				
				if($countRes > 0)
				{ 
					$blockList = array();
					foreach($query_1->result_array() as $row)
					{
						array_push($blockList, $row['block_id']);
					}
					
					$blockArr = "" . implode( "','", $blockList ) . "";
					$query = $this->db->query("SELECT * FROM survey_block_master WHERE status = 'Active' AND district_id = '".$district_id."' AND block_id IN('".$blockArr."')");
					$cnt = $query->num_rows();
				}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No Block List Found For This Survey.";
				}
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_block_master WHERE status = 'Active' AND district_id = '".$district_id."'");
				$cnt = $query->num_rows();
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
				}
				if($survey_id != $education_survey_id) 
				{
					$response['data'][] = array('block_id' => 'Other', 'block_name' => 'Other/select from State List');
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Development block List Get Successfully.";
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	} 

	// function to Get All Development Block List
	public function get_all_development_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT block_id,block_name,district_id FROM survey_block_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
			}
			$response['data'][] = array('block_id' => 'Other', 'block_name' => 'Other');
			
			$response['status'] 	= "1";
			$response['message'] 	= "Development block List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	} 
	
	// Get All Block
	public function get_all_block()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		//$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
				}				
				
				$panchayatArr = "" . implode( "','", $panchyatList ) . "";				
				$query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");				
				$countRes = $query_1->num_rows();
				
				if($countRes > 0)
				{ 
					$blockList = array();
					foreach($query_1->result_array() as $row)
					{
						array_push($blockList, $row['block_id']);
					}
					
					$blockArr = "" . implode( "','", $blockList ) . "";
					$query = $this->db->query("SELECT * FROM survey_block_master WHERE block_id IN('".$blockArr."')");
					$cnt = $query->num_rows();
				}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No Block List Found For This Survey.";
				}
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_block_master");
				$cnt = $query->num_rows();
			}					
				
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
			
				/*if($survey_id != $education_survey_id) 
				{
					$response['data'][] = array('block_id' => 'Other', 'block_name' => 'Other');
				}*/
				
				$response['status'] 	= "1";
				$response['message'] 	= "Development block List Get Successfully.";
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
		
	// Get All Block
	public function get_all_block_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT district_id,block_name,block_id FROM survey_block_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('block_id' => $row['block_id'], 'block_name' => $row['block_name'], 'district_id' => $row['district_id']);
			}
		
			$response['status'] 	= "1";
			$response['message'] 	= "Development block List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	
	// Get All Panchayat
	public function get_all_panchayat()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
					
					$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
					$query = $this->db->query("SELECT * FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
					$cnt = $query->num_rows();
				}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Gaon-Panchayat List Found For This Survey.";
				}
				
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_panchayat_master");
				$cnt = $query->num_rows();
			}			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('panchayat_id' => $row['panchayat_id'], 'block_id' => $row['block_id'], 'panchayat_name' => $row['panchayat_name'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Panchayat-Gaon List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}		
		
		$this->response($response);
	}
	
	// function to Get Panchayat/Gaon List -
	public function survey_panchayat_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$block_id	= isset($_POST['block_id']) ? $_POST['block_id'] : '';		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey name is required.";
			
		} else if(empty($block_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Block name is required.";
			
		} else {
			
			$education_survey_id = $this->config->item('education_survey_id');
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0 AND status = 'Active' AND is_deleted = '0'");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						array_push($panchyatList, $row['panchayat_id']);
					}
					
					$panchayatArr = "" . implode( "','", $panchyatList ) . "";
				
					$query = $this->db->query("SELECT panchayat_id, block_id, panchayat_name  FROM survey_panchayat_master WHERE status = 'Active' AND block_id = '".$block_id."' AND panchayat_id IN('".$panchayatArr."')");
					$cnt = $query->num_rows();
					
				}
				else
				{
					$response['status'] 	= "0";
					$response['message'] 	= "No Gaon/Panchyat Found For This Survey.";
				}	
				
			}
			else 
			{
				$query = $this->db->query("SELECT panchayat_id, block_id, panchayat_name FROM survey_panchayat_master WHERE status = 'Active' AND block_id = '".$block_id."'");
				$cnt = $query->num_rows();
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('panchayat_id' => $row['panchayat_id'], 'block_id' => $row['block_id'], 'panchayat_name' => $row['panchayat_name']);
				}
				
				if($survey_id != $education_survey_id) 
				{
					$response['data'][] = array('panchayat_id' => 'Other',  'block_id' => $row['block_id'], 'panchayat_name' => 'Other/select Gaon-Panchyat List');
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Gaon-Panchayat List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get Village List
	public function survey_village_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$revenue_id	= isset($_POST['revenue_id']) ? $_POST['revenue_id'] : '';
		
		// get POST data -
		$panchayat_id	= isset($_POST['panchayat_id']) ? $_POST['panchayat_id'] : '';
		
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		if(empty($revenue_id) && $survey_id != $education_survey_id){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Revenue circle name is required.";
			
		} else if(empty($panchayat_id) && $survey_id == $education_survey_id){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Gaon/Panchyat name is required.";
			
		}  else {
			
			if($survey_id == $education_survey_id) 
			{ 
				$query = $this->db->query("SELECT village_id, village_code, village_name  FROM survey_village_master WHERE panchayat_id = '".$panchayat_id."' AND status = 'Active' AND is_deleted = '0'");
				$cnt = $query->num_rows();
			}
			else 
			{	
				$query = $this->db->query("SELECT village_id, village_name, village_code FROM survey_village_master WHERE status = 'Active' AND revenue_id = '".$revenue_id."' AND is_deleted = '0'");
				$cnt = $query->num_rows();
			}
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$villageName = $row['village_name']."(".$row['village_code'].")";
					$response['data'][] = array('village_id' => $row['village_id'], 'village_code' => $row['village_code'],  'village_name' => $villageName);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Village List Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get All Village List
	public function get_all_village_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		$education_survey_id = $this->config->item('education_survey_id');
		
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} 
		else 
		{
			
			if($survey_id == $education_survey_id) 
			{
				$villageMaster = $this->db->query("SELECT * FROM survey_village_master WHERE panchayat_id > 0");
				$cntRow = $villageMaster->num_rows();
				
				if($cntRow > 0)
				{ 
					$panchyatList = array();
					foreach($villageMaster->result_array() as $row)
					{
						$villageName = $row['village_name']."(".$row['village_code'].")";
						$response['data'][] = array('village_id' => $row['village_id'], 'village_code' => $row['village_code'],  'village_name' => $villageName, 'panchayat_id' => $row['panchayat_id'], 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
					}
					
					$response['status'] 	= "1";
					$response['message'] 	= "Village List Get Successfully.";
				}	
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
			}
			else 
			{
				$query = $this->db->query("SELECT * FROM survey_village_master");
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{
						$villageName = $row['village_name']."(".$row['village_code'].")";
						$response['data'][] = array('village_id' => $row['village_id'], 'revenue_id' => $row['revenue_id'], 'village_code' => $row['village_code'],  'village_name' => $villageName, 'status' => $row['status'], 'is_deleted' => $row['is_deleted']);
					}
					
					$response['status'] 	= "1";
					$response['message'] 	= "Village List Get Successfully.";
				}
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
				
			}
			
		}		
		
		$this->response($response);
	}

	// function to Get Survey Category List
	public function get_category_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT category_name, category_id FROM survey_category_master WHERE status = 'Active' AND is_deleted = '0'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('category_name' => $row['category_name'], 'category_id' => $row['category_id']);
			}
			
			$response['status'] 	= "1";
			$response['message'] 	= "Survey Category List Get Successfully.";
		}
		else
		{
			$response['status'] 		= "0";
			$response['message'] 	= "No data Found.";
		}
		
		$this->response($response);
	}
	
	// function to Get Survey List
	public function get_survey_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
				
		if(!empty($user_id) && is_user_login($user_id))
		{			
			$surveyData = $this->db->query("SELECT survey_id FROM survey_assign_users WHERE user_id = '".$user_id."' AND is_deleted = '0'");
			$cntRow = $surveyData->num_rows();			
			
			if($cntRow > 0)
			{
				$surveyList = array();
				foreach($surveyData->result_array() as $row)
				{
					array_push($surveyList, $row['survey_id']);
				}
				
				$outputString = "" . implode( "','", $surveyList ) . "";
				$query = $this->db->query("SELECT survey_master.survey_id, survey_master.user_id, survey_master.category_id,  survey_category_master.category_name, survey_master.description, survey_master.title, survey_master.user_id FROM survey_master LEFT JOIN survey_category_master ON survey_master.category_id = survey_category_master.category_id 
				WHERE survey_master.survey_id IN('".$outputString."') AND survey_master.status != 'Draft' AND survey_master.is_deleted = '0'");
				
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					$arrayList = array();
					foreach($query->result_array() as $row)
					{
						
						array_push($arrayList, $row['survey_id']);
						
						$data[] = array('survey_id' => $row['survey_id'], 'category_id' => $row['category_id'],  'category_name' => $row['category_name'],  'survey_title' => $row['title'],  'survey_description' => $row['description']);
					}
					
					//print_r($arrayList);
					$response['status'] 	= "1";
					$response['message'] 	= "Survey List Get Successfully.";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					$response['message'] 	= "No data Found.";
				}
				
			}
			else
			{
				$response['status'] 	= "1";
				$response['message'] 	= "No Survey Assigned To User.";
			}
			
		}
		else
		{
			$response['status']		= "0";
			$response['message']    = "You must be logged in to perform this request.";
		}
		
		$this->response($response);
	}	
	
	// function to Get Survey Details
	public function get_survey_details()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey ID is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT survey_listing.survey_id, survey_listing.user_id, survey_listing.category_id, survey_category_master.category_name, survey_listing.description, survey_listing.title, survey_listing.user_id FROM survey_listing LEFT JOIN survey_category_master ON survey_listing.category_id = survey_category_master.category_id 
			WHERE survey_listing.status = 'Publish' AND survey_listing.survey_id = '".$survey_id."' AND is_deleted = '0'");
			
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('survey_id' => $row['survey_id'], 'user_id' => $row['user_id'], 'category_id' => $row['category_id'],  'category_name' => $row['category_name'],  'title' => $row['title'],  'description' => $row['description']);
				}
				
				$response['status'] 	= "1";
				$response['message'] 	= "Survey Details Get Successfully.";
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
	}
	
	// Forgot Password
	public function forgot_password(){
		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$email_id	= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		
		if(empty($email_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Email Address is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT first_name, last_name, username, contact_no, email, status, is_deleted FROM survey_users WHERE email = '".$email_id."'");
			
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					
					if($row['is_deleted'] == 1){
						
						$response['status'] 	= "0";
						$response['message'] 	= "User is deleted. Please contact to your administrator.";
						
					} else {
							
							$random_number = mt_rand(100000,999999);
							
							$fullname = ucfirst($row['first_name'])." ".ucfirst($row['last_name']);
							$username = $row['username'];
							$email_id = $row['email'];	
							$state_code = md5(time()."+".$email_id);
							
							$check_exist = $this->master_model->getRecords("verify_otp", array("email_id" => $email_id));
							if(count($check_exist) > 0){
								$values = '"'.$email_id.'"';	
								$updateArr = array('random_str_1' => $state_code, 'random_str_2' => '');			
								$updateQuery = $this->master_model->updateRecord('verify_otp',$updateArr,array('email_id' => $values));
							} else {								
								$insertData = array('verify_code' => $random_number, 'email_id' => $email_id, 'random_str_1' => $state_code);
								$this->master_model->insertRecord('verify_otp',$insertData);							
							}	
								
							//$this->send_forgot_password_email($fullname, $username, $random_number, $email_id);
							
							//$data = array('state_code' => $state_code);
							
							$response['status'] 	= "1";
							$response['message'] 	= "Forgot Password Email Sent Successfully.";
							$response['state_code']	= $state_code;						
					}	
				}
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
		}
		
		$this->response($response);
		
	}
		
	// Send an Forgot Email To Register User 
	public function send_forgot_password_email($fullname, $username, $random_number, $email_id)
	{	
		//echo $random_number;die();
		$slug = 'user_forgot_password';
		//survey_verify_otp
		$email_body = $this->master_model->getRecords("email_master", array("slug" => $slug));		
		if(count($email_body) > 0){			
			$from_admin = $email_body[0]['from_email'];
			$email_content = $email_body[0]['email_body'];
			$email_subject = $email_body[0]['email_subject'];
			$url = base_url('xAdmin/admin');
			$arr_words 	= ['[FULLNAME]', '[CODE]', '[EMAIL]'];
			$rep_array 	= [$fullname, $random_number, $email_id];		
			$content 	= str_replace($arr_words, $rep_array, $email_content);
			
			/*$contentArray = array(
									'to'			=>	$email,//$email
									'to_name'		=>	$fullname,	
									'cc'			=>	'',
									'from_email'	=>	$from_admin,
									'from_name'		=>	'UNDP Survey',
									'subject'		=> 	$email_subject,
									'description' 	=> 	$content,
									'view'          => 'common-file'	
								);
			$emailsend 	= 	$this->emailsending->sendmail_phpmailer($contentArray);*/
			
			//email admin sending code 
			$contentArray=array(
				'to'		=>	'vicky.kuwar@esds.co.in', //$email_id,					
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$email_subject,
				'view'		=>  'common-file'
				);
			
			$other_info=array('content'=>$content);
			
			
			$emailsend 	= 	$this->emailsending->sendmail($contentArray,$other_info);
			if($emailsend){
				return true;
			}else {
				return false;
			}
		}
	} // End Function	
	
	// OTP Verification
	public function verify_otp()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$email_id	= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		$code		= isset($_POST['code']) ? $_POST['code'] : '';
		$state_code	= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		//print_r($_POST);
		if(empty($email_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Email address is required.";
			
		} else if(empty($code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Verification code is required.";
			
		} else if(empty($state_code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State code is required.";
			
		} else {			
			
			$query = $this->db->query("SELECT verify_code, status, email_id, random_str_1 FROM survey_verify_otp WHERE random_str_1 = '".$state_code."'");		
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{						
					if($row['email_id'] != $email_id)
					{							
						$response['status'] 	= "0";
						$response['message'] 	= "Enter email id is wrong.";
					}
					else if($row['verify_code'] != $code)
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Enter verification code is wrong.";
					}
					else if($row['random_str_1'] != $state_code)
					{
						$response['status'] 	= "0";
						$response['message'] 	= "State code is wrong.";
					}
					else 
					{
					
						if($row['email_id'] == $email_id && $row['verify_code'] == $code && $row['random_str_1'] == $state_code)
						{
							$state_code2 = md5(time()."+".$email_id);
							//$data = array("is_otp_verified" => 'TRUE', 'state_code' => $state_code2);
							$response['status'] 		= "1";
							$response['message'] 		= "OTP successfully verify.";
							$response['is_otp_verified']= 'TRUE';
							$response['random_str_1'] 	= $state_code2;
							
							$values = '"'.$email_id.'"';
							
							// UPDATE STATUS
							$updateArr = array('random_str_2' => $state_code2);			
							$updateQuery = $this->master_model->updateRecord('verify_otp',$updateArr,array('email_id' => $values));
							
						}
						else
						{
							$response['status'] 	= "0";
							$response['message'] 	= "Somthing wrong. Please try again.";
						}
					
					}					
				}
			}
			else
			{
				$response['status'] 		= "0";
				$response['message'] 	= "No data Found.";
			}
			
		}
		
		$this->response($response);
		
	} // End Of Function Verify OTP
	
	// Update Password 
	public function change_password(){
		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$password		= isset($_POST['password']) ? $_POST['password'] : '';
		$cnfpassword	= isset($_POST['cnfpassword']) ? $_POST['cnfpassword'] : '';
		$email_id		= isset($_POST['email_id']) ? $_POST['email_id'] : '';
		$state_code		= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		
		$uppercase = preg_match('@[A-Z]@', $password);
		$lowercase = preg_match('@[a-z]@', $password);
		$number    = preg_match('@[0-9]@', $password);
		$specialChars = preg_match('@[^\w]@', $password);
		
		if(empty($password)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password is required.";
			
		} else if(empty($cnfpassword)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Confirm Password is required.";
			
		} else if(empty($state_code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "State code is required.";
			
		} else if($password != $cnfpassword){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password & Confirm Password does not match.";
			
		} else if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 6){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Password should be at least 6 characters in length and should include at least one upper case letter, one number, and one special character.";
			
		} else {

			$stateC = $this->db->query("SELECT * FROM survey_verify_otp WHERE random_str_2 = '".$state_code."'");
			$cnt_1 = $stateC->num_rows();
			
			if($cnt_1 > 0)
			{
				//$query = $this->db->query("SELECT * FROM survey_users WHERE email = '".$email_id."'");
				$query = $this->db->query("SELECT * FROM survey_verify_otp WHERE random_str_2 = '".$state_code."' AND email_id = '".$email_id."'");	
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{						
						$values = '"'.$email_id.'"';
						// UPDATE STATUS
						$updateArr = array('password' => md5($password));			
						$updateQuery = $this->master_model->updateRecord('users',$updateArr,array('email' => $values));
						
						$response['status'] 	= "1";
						$response['message'] 	= "Password successfully updated.";							
					}
				}
				else
				{
					$response['status'] 		= "0";
					$response['message'] 	= "No data Found.";
				}
				
			} 
			else 		
			{
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "State code is invalid.";
			}
			
		}
		
		$this->response($response);
		
	}
	
	// Get Option Dependent Question 10thFeb2021
	public function getOptionbaseQuestionMultiple($question_id, $dependant_ques_id, $id, $option)
	{ 
		//ob_start();		
			
		$queryQuestion = $this->db->query("SELECT question_id,question_text,response_type_id,is_mandatory,parent_question_id,salutation,input_more_than_1,if_file_required,file_label, input_file_required,if_summation,help_label FROM survey_question_master WHERE question_id = '".$dependant_ques_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			$in_arr = array();
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			} 
			 // get the first row
			$in_arr['question_id'] 			= $questionData->question_id; 
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
			//$in_arr['salutation'] 			= $questionData->salutation;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['if_file_required']		= 'No';
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_summation']			= 'No';
			
			if($questionData->if_file_required == 'Yes')
			{
				$in_arr['if_file_required'] 	= $questionData->if_file_required;
				$in_arr['file_label'] 			= $questionData->file_label;
			}
			
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}	
			
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT *  FROM survey_questions_options_master WHERE question_id = '".$questionData->question_id."' AND is_deleted='0' ORDER BY display_order ASC");
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="")
					{
						array_push($optArr_1, $opt_1['option']);
					}
					
					
					/*************************** Multiple Option Dependent Question Data Start *********************************/
				
						$quetionOptionMultipleQuestion = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$questionData->question_id."' AND ques_option_id='".$opt_1['ques_option_id']."' AND is_deleted='0'");
						$multipleQuestionCnt = $quetionOptionMultipleQuestion->num_rows();
						if($multipleQuestionCnt > 0)
						{
							foreach($quetionOptionMultipleQuestion->result_array() as $multipleQues)
							{
								$queryMultipleQuestion = $this->db->query("SELECT * FROM survey_questions_options_master WHERE ques_option_id = '".$multipleQues['ques_option_id']."' AND is_deleted = '0' ORDER BY display_order ASC");
								$questionMultipleData = $queryMultipleQuestion->row();
								$getOption = $questionMultipleData->option;
								
								// Below Code for multiple level array questions
								$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);								
								//$in_arr['dependent_question_options'] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);
							}
							
						}
				
					/************************* Multiple Option Dependent Question Data Start ***********************************/
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}
					//$in_arr['validation_id'] 	= $opt_1['min_value'];
					//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}

				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
				//$in_arr['options'] = json_encode($optArr_1);					
			}
			
		}
		
		$dataArr = array("option" => $option, "option_dependent_question" => $in_arr); 		
		//return json_encode($dataArr);
		return $dataArr;
	} // Function End
	
	// Survey Section List
	public function getSurveySectionList($survey_id)
	{
			$result = array();
			
			$surveySectionList = $this->db->query("SELECT DISTINCT `survey_section_questions`.`section_id`, `survey_section_master`.`section_name`, `survey_section_master`.`section_desc` FROM `survey_section_questions` JOIN `survey_section_master` ON `survey_section_master`.`section_id` = `survey_section_questions`.`section_id` WHERE `survey_section_questions`.`survey_id` = '".$survey_id."' AND `survey_section_questions`.`is_deleted` = 0 AND `survey_section_master`.`is_deleted` = 0 ORDER BY `survey_section_questions`.`sort_order` IS NOT NULL DESC, `survey_section_questions`.`sort_order` ASC");					
			//echo $this->db->last_query();
			$surveyCount = $surveySectionList->num_rows();
			
			if($surveyCount > 0)
			{		
				foreach($surveySectionList->result_array()  as $res) 
				{
					$result[] = array("section_id" => $res['section_id'], "section_name" => $res['section_name'], "section_description" => $res['section_desc']);
				}
			}
			
			return $result;
				
	}
	
	// Survey Question Listing 07 March 2021
	public function survey_questionire_v1()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} 
		else 
		{
			// Get Survey ID
			$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
			
			if(empty($survey_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Survey ID is required.";
				
			} 
			else 
			{	
				// Get Survey Details 
				$response['status'] 		= "1";
				$response['message'] 		= "Survey questionire get successfully.";	
				
				$surveyData = $this->db->query("SELECT * FROM survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
				$surveyResult = $surveyData->row();	
					
				$response['survey_id'] 			= $surveyResult->survey_id;
				$response['survey_title'] 		= $surveyResult->title; 
				$response['survey_description'] = $surveyResult->description;
			
				// Ask Section Question Table For Section & Sub Section			
				$surveyQuestionList = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$survey_id."'  AND status = 'Active' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
				$sectionQuesitonCount = $surveyQuestionList->num_rows();
				
				if($sectionQuesitonCount > 0)
				{				
					$section_question = array();
					
					$surveySections = $this->getSurveySectionList($survey_id);
					
					//echo "<pre>";print_r($surveySections);
					foreach($surveyQuestionList->result_array() as $res)
					{	
						$querySection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id = '".$res['section_id']."' AND parent_section_id = 0 AND is_deleted = '0' AND status = 'Active'");
						$sectionData = $querySection->row();	
						//echo $this->db->last_query();
						$sectionArr = array();
						
						if($sectionData->section_id!="") 
						{
							$section_id 			= $sectionData->section_id;	
							$section_name 			= $sectionData->section_name; 
							$section_description 	= $sectionData->section_desc;
						}

						$sub_section_id 			= '';	
						$sub_section_name 			= ''; 
						$sub_section_description 	= '';
						if($res['sub_section_id'] > 0)
						{							
							$querySubSection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id='".$res['sub_section_id']."' AND is_deleted = '0' AND status = 'Active'");
							//echo $this->db->last_query();
							$subSectionData = $querySubSection->row();	
							$sub_section_id 			= @$subSectionData->section_id;	
							$sub_section_name 			= @$subSectionData->section_name; 
							$sub_section_description 	= @$subSectionData->section_desc;
						}
						
						$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
						$if_parents_count = $if_checked_parent->num_rows();
						
						if($if_parents_count == 0)
						{
							$in_arr = array();
							// Global Question 
							$global_question_id 		= $res['global_question_id'];
							if($global_question_id > 0)
							{							
								$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
							}
							
							$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
							$questionData = $queryQuestion->row();	
							$explodeSub =  array();
							if($questionData->salutation!="")
							{
								$explodeSub = explode("|", $questionData->salutation);
							} 

							$in_arr['section_id'] 			= $section_id;
							$in_arr['section_name'] 		= $section_name;
							$in_arr['section_desc'] 		= $section_description;
							
							$in_arr['sub_section_id'] 		= $sub_section_id;
							$in_arr['sub_section_name'] 	= $sub_section_name;
							$in_arr['sub_section_des'] 		= $sub_section_description;
							
							$in_arr['question_id'] 			= $questionData->question_id;
							$in_arr['sort_order'] 			= $res['sort_order'];
							$in_arr['question_label'] 		= $questionData->question_text;
							$in_arr['response_type'] 		= $questionData->response_type_id;
							$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
							$in_arr['salutation'] 			= json_encode($explodeSub);
							$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
							$in_arr['question_help_label'] 	= $questionData->help_label;
							$in_arr['upload_file_required']	= 'No';
							$in_arr['if_summation']			= 'No';
							$in_arr['if_file_required']		= 'No';
							if($questionData->input_file_required == 'Yes')
							{
								$in_arr['upload_file_required'] 	= $questionData->input_file_required;
							}
							
							if($questionData->if_file_required == 'Yes')
							{
								$in_arr['if_file_required'] 	= $questionData->if_file_required;
								$in_arr['file_label'] 			= $questionData->file_label;
							}
							
							if($questionData->if_summation == 'Yes')
							{
								$in_arr['if_summation'] 		= $questionData->if_summation;
							}													
							
							// Get Sub Question List On Main Question 								
							$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
							$sub_questuon_count = $subQuestion->num_rows();
							
							if($sub_questuon_count > 0)
							{									
								foreach($subQuestion->result_array() as $opt)
								{
									$sub_arr = array();
									$sub_arr['question_id'] 		= $opt['question_id'];
									$explodeSub =  array();
									if($opt['salutation']!="")
									{
										$explodeSub = explode("|", $opt['salutation']);

									} 
									else 
									{
										$blank = array();
										$explodeSub = $blank;
									}
									
									$sub_arr['section_id'] 			= $section_id;
									$sub_arr['section_name'] 		= $section_name;
									$sub_arr['section_desc'] 		= $section_description;
									
									$sub_arr['sub_section_id'] 		= $sub_section_id;
									$sub_arr['sub_section_name'] 	= $sub_section_name;
									$sub_arr['sub_section_des'] 	= $sub_section_description;
									
									$sub_arr['question_id'] 		= $opt['question_id'];
									$sub_arr['question_label'] 		= $opt['question_text'];
									$sub_arr['response_type'] 		= $opt['response_type_id'];
									$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
									$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
									$sub_arr['question_help_label'] = $opt['help_label'];
									$sub_arr['salutation'] 			= json_encode($explodeSub);
									$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
									$sub_arr['upload_file_required']	= 'No';
									$sub_arr['if_summation']			= 'No';	
									$sub_arr['if_file_required']		= 'No';	
									/*if($questionData->input_file_required == 'Yes')
									{
										$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
									}*/
									
									// To handle image for sub question, by Bhagwan, on 02-04-2021
									if($opt['input_file_required'] == 'Yes')
									{
										$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
									}
									
									if($opt['if_file_required'] == 'Yes')
									{
										$sub_arr['if_file_required'] 	= $opt['if_file_required'];
										$sub_arr['file_label'] 			= $opt['file_label'];
									}
									
									if($opt['if_summation'] == 'Yes')
									{
										$sub_arr['if_summation'] 		= $opt['if_summation'];
									}
									// EOF To handle image for sub question, by Bhagwan, on 02-04-2021
										
								}	// Sub Array End							
							
								$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
								$option_count = $quetionOptions->num_rows();	
								if($option_count > 0)
								{
									$optArr = array();										
									foreach($quetionOptions->result_array() as $optSub)
									{
										if($optSub['option']!="")
										{	
											array_push($optArr, $optSub['option']);	
										}
												
										$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
											}
										}
										
										if($optSub['validation_id'] > 0)
										{
											$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
											
											if($optSub['sub_validation_id'] > 0)
											{
												$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
											}
										}
										else 
										{
											$in_arr['validation_type'] = '';
											$in_arr['validation_sub_type'] = '';
										}	
								
										$in_arr['min_value'] 		= $optSub['min_value'];
										$in_arr['max_value'] 		= $optSub['max_value'];
										$in_arr['validation_label']	= $optSub['validation_label'];	
									}						

									if(count($optArr) > 0)
									{
										$in_arr['options'] = json_encode($optArr);
									}
									else
									{
										$blank = array("");
										$in_arr['options'] = json_encode($blank);
										//$in_arr['options'] = '[]';
									}	
								}
								
								$in_arr['sub_question'] = $sub_arr;							
								//$in_arr['sub_question'][] = $sub_arr;
								
							} // End Sub Question If
							
							// Get Parent Question Option 
							$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
							$option_count_1 = $quetionOptions_1->num_rows();	
							
							if($option_count_1 > 0)
							{
								$optArr_1 = array();
								foreach($quetionOptions_1->result_array() as $opt_1)
								{
									if($opt_1['option']!="")
									{
										array_push($optArr_1, $opt_1['option']);
									}
																		
									// Multiple Question Loop
									$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
									$multi_option_count = $queryMultipleCall->num_rows();
									if($multi_option_count > 0)
									{ 
										foreach($queryMultipleCall->result_array() as $dpenSub)
										{
											$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
										}
									}
							
									if($opt_1['validation_id'] > 0)
									{
										$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
										
										if($opt_1['sub_validation_id'] > 0)
										{
											$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
										}
									}
									else 
									{
										$in_arr['validation_type'] = '';
										$in_arr['validation_sub_type'] = '';
									}	
									
									$in_arr['min_value'] 		= $opt_1['min_value'];
									$in_arr['max_value'] 		= $opt_1['max_value'];
									$in_arr['validation_label']	= $opt_1['validation_label'];
								}										
								
								if(count($optArr_1) > 0)
								{
									$in_arr['options'] = json_encode($optArr_1);
								}
								else
								{
									$blank = array("");
									$in_arr['options'] = json_encode($blank);
									//$in_arr['options'] = '[]';
								}
								
							} // End Option If End							
							
							$section_question[]	= $in_arr;							
							
						} // Check End Related Option Dependent Has Parent				
						
					} // End Foreach Related Section Question
					
					$sectionArr['section_list']		= $surveySections;
					$sectionArr['section_question']	= $section_question;
					
				}			
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Survey questionire not found.";
				}
					
				$response['data']['sections'][]	= $sectionArr;
			}
		}
		
		$this->response($response);
	}	
	
	// Survey Question Listing New 11Feb2021
	public function survey_questionire1()
	{
		ob_start();
		error_reporting(0);
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		if(empty($survey_id)){
			
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Survey name is required.";
			
		} 
		else 
		{
			$response['status'] 		= "1";
			$response['message'] 		= "Data successfully received.";	
			
			$surveyData = $this->db->query("SELECT * FROM survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
			$surveyResult = $surveyData->row();
			
			
			$response['survey_id'] 			= $surveyResult->survey_id;
			$response['survey_title'] 		= $surveyResult->title; 
			$response['survey_description'] = $surveyResult->description; 
			
			
			//START : QUESTION To Survey			
			$querySurveyQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='0' AND sub_section_id='0' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
			
			$count = $querySurveyQuestion->num_rows();						
			
			if($count > 0)
			{		
				$surveyQuestion = array();
				foreach($querySurveyQuestion->result_array() as $res)
				{	
					$in_arr = array();
					
					$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
					$if_parents_count = $if_checked_parent->num_rows();
					
					if($if_parents_count == 0)
					{									
						$global_question_id 		= $res['global_question_id'];
						if($global_question_id > 0)
						{
							
							$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
						}
						
						$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
						$questionData = $queryQuestion->row();	
						$explodeSub =  array();
						if($questionData->salutation!="")
						{
							$explodeSub = explode("|", $questionData->salutation);
						} 	
						$in_arr['question_id'] 			= $questionData->question_id;
						$in_arr['question_label'] 		= $questionData->question_text;
						$in_arr['response_type'] 		= $questionData->response_type_id;
						$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
						//$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
						//$in_arr['salutation'] 			= $questionData->salutation;
						$in_arr['salutation'] 			= json_encode($explodeSub);
						$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
						$in_arr['question_help_label'] 	= $questionData->help_label;
						$in_arr['upload_file_required']	= 'No';
						$in_arr['if_summation']			= 'No';
						$in_arr['if_file_required']		= 'No';
						if($questionData->input_file_required == 'Yes')
						{
							$in_arr['upload_file_required'] 	= $questionData->input_file_required;
						}
						
						if($questionData->if_file_required == 'Yes')
						{
							$in_arr['if_file_required'] 	= $questionData->if_file_required;
							$in_arr['file_label'] 			= $questionData->file_label;
						}
						
						if($questionData->if_summation == 'Yes')
						{
							$in_arr['if_summation'] 		= $questionData->if_summation;
						}													
						
						// Get Sub Question List On Main Question 								
						$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
						$sub_questuon_count = $subQuestion->num_rows();
						
						if($sub_questuon_count > 0)
						{									
							foreach($subQuestion->result_array() as $opt)
							{
								$sub_arr = array();
								$sub_arr['question_id'] 		= $opt['question_id'];
								$explodeSub =  array();
								if($opt['salutation']!="")
								{
									$explodeSub = explode("|", $opt['salutation']);
								} 
								else 
								{
									$blank = array();
									$explodeSub = $blank;
								}
								$sub_arr['question_id'] 		= $opt['question_id'];
								$sub_arr['question_label'] 		= $opt['question_text'];
								$sub_arr['response_type'] 		= $opt['response_type_id'];
								$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
								$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
								//$sub_arr['salutation'] 			= $opt['salutation'];
								$sub_arr['question_help_label'] = $opt['help_label'];
								$sub_arr['salutation'] 			= json_encode($explodeSub);
								$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
								$sub_arr['upload_file_required']	= 'No';
								$sub_arr['if_summation']			= 'No';	
								$sub_arr['if_file_required']		= 'No';	
								if($questionData->input_file_required == 'Yes')
								{
									$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
								}
							}	// Sub Array End							
						
							$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
							$option_count = $quetionOptions->num_rows();	
							if($option_count > 0)
							{
								$optArr = array();										
								foreach($quetionOptions->result_array() as $optSub)
								{
									if($optSub['option']!="")
									{	
										array_push($optArr, $optSub['option']);	
									}		
										$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
											}
										}
									
									if($optSub['validation_id'] > 0)
									{
										$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
										
										if($optSub['sub_validation_id'] > 0)
										{
											$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
										}
									}
									else 
									{
										$in_arr['validation_type'] = '';
										$in_arr['validation_sub_type'] = '';
									}	
							
									$in_arr['min_value'] 		= $optSub['min_value'];
									$in_arr['max_value'] 		= $optSub['max_value'];
									$in_arr['validation_label']	= $optSub['validation_label'];	
								}						

								if(count($optArr) > 0)
								{
									$in_arr['options'] = json_encode($optArr);
								}
								else
								{
									$blank = array("");
									$in_arr['options'] = json_encode($blank);
									//$in_arr['options'] = '[]';
								}	
																
							}
							
							$in_arr['sub_question'] = $sub_arr;							
							//$in_arr['sub_question'][] = $sub_arr;
							
						} // If SubQuestion Attached End
						
						// Get Option 
						$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
						$option_count_1 = $quetionOptions_1->num_rows();	
						
						if($option_count_1 > 0)
						{
							$optArr_1 = array();
							foreach($quetionOptions_1->result_array() as $opt_1)
							{
								if($opt_1['option']!="")
								{
									array_push($optArr_1, $opt_1['option']);
								}									
								// Multiple Question Loop
								$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
								$multi_option_count = $queryMultipleCall->num_rows();
								if($multi_option_count > 0)
								{ 
									foreach($queryMultipleCall->result_array() as $dpenSub)
									{
										$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
									}
								}
								
						
								if($opt_1['validation_id'] > 0)
								{
									$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
									
									if($opt_1['sub_validation_id'] > 0)
									{
										$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
									}
								}
								else 
								{
									$in_arr['validation_type'] = '';
									$in_arr['validation_sub_type'] = '';
								}	
								//$in_arr['validation_id'] 	= $opt_1['validation_id'];
								//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
								$in_arr['min_value'] 		= $opt_1['min_value'];
								$in_arr['max_value'] 		= $opt_1['max_value'];
								$in_arr['validation_label']	= $opt_1['validation_label'];
							}										
							
							if(count($optArr_1) > 0)
							{
								$in_arr['options'] = json_encode($optArr_1);
							}
							else
							{
								$blank = array("");
								$in_arr['options'] = json_encode($blank);
								//$in_arr['options'] = '[]';
							}
							
							//$in_arr['options'] = json_encode($optArr_1);										
						}									
						
						$surveyQuestion[] = $in_arr;									
					}								
													
				} // Foreach End	
				
				$response['data']['survey_question']	= $surveyQuestion;
			} 
			else 
			{
				$response['data']['survey_question'][]	= array();
			}
						
			//END : QUESTION SECTION
			
			// Start Section Code 
			// If Section Assign To Survey
			$sectionQuery = $this->db->query("SELECT SM.section_id, SM.section_name, SM.section_desc, SM.parent_section_id, SSM.survey_id, SSM.title, SSM.description  
			FROM survey_section_master as SM LEFT JOIN survey_section_mapping as SS_M ON SM.section_id = SS_M.section_id
			LEFT JOIN survey_master as SSM ON SS_M.survey_id = SSM.survey_id			
			WHERE SSM.survey_id = '".$survey_id."' AND SM.parent_section_id= '0' AND SM.status = 'Active' AND SM.is_deleted = '0'");
			$numRowSection = $sectionQuery->num_rows();
			if(count($numRowSection) > 0)
			{
				foreach($sectionQuery->result_array() as $sectionValues )
				{
					$sectionArr = array();
					$sectionArr['section_id'] 			= $sectionValues['section_id'];					
					//$sectionArr['sub_section_id'] 		= $sectionValues['parent_section_id'];	
					$sectionArr['section_name'] 		= $sectionValues['section_name']; 
					$sectionArr['section_description'] 	= $sectionValues['section_desc'];
					
					// If Question Assign To Section
					$querySectionQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='".$sectionValues['section_id']."' AND sub_section_id='0' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
					//echo $this->db->last_query();
					$sectionQuesitoncount = $querySectionQuestion->num_rows();	
					
					if($sectionQuesitoncount > 0)
					{		
						$sectionQuestion = array();
						foreach($querySectionQuestion->result_array() as $res)
						{	
							$in_arr = array();
							//$in_arr['section_id'] 		= $res['section_id'];
							//$in_arr['sub_section_id'] 	= $res['sub_section_id'];
							//$in_arr['question_id'] 		= $res['question_id'];
							
							$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
							$if_parents_count = $if_checked_parent->num_rows();
							
							if($if_parents_count == 0)
							{									
								$global_question_id 		= $res['global_question_id'];
								if($global_question_id > 0)
								{	
									$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
								}
								
								$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
								$questionData = $queryQuestion->row();								
								$explodeSub =  array();
								if($questionData->salutation!="")
								{
									$explodeSub = explode("|", $questionData->salutation);
								} 
								$in_arr['question_id'] 			= $questionData->question_id;
								$in_arr['sort_order'] 			= $res['sort_order'];
								$in_arr['question_label'] 		= $questionData->question_text;
								$in_arr['response_type'] 		= $questionData->response_type_id;
								$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
								$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
								$in_arr['question_help_label'] 	= $questionData->help_label;
								$in_arr['salutation'] 			= json_encode($explodeSub);
								$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
								$in_arr['upload_file_required']	= 'No';
								$in_arr['if_summation']			= 'No';	
								$in_arr['if_file_required']		= 'No';		
								if($questionData->input_file_required == 'Yes')
								{
									$in_arr['upload_file_required'] 	= $questionData->input_file_required;
								}
								
								if($questionData->if_file_required == 'Yes')
								{
									$in_arr['if_file_required'] 	= $questionData->if_file_required;
									$in_arr['file_label'] 			= $questionData->file_label;
								}
								
								if($questionData->if_summation == 'Yes')
								{
									$in_arr['if_summation'] 		= $questionData->if_summation;
								}									
								
								// Get Sub Question List On Main Question 								
								$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
								$sub_questuon_count = $subQuestion->num_rows();
								
								if($sub_questuon_count > 0)
								{									
									foreach($subQuestion->result_array() as $opt)
									{
										$sub_arr = array();
										$explodeSub =  array();
										if($opt['salutation']!="")
										{
											$explodeSub = explode("|", $opt['salutation']);
										} 
										$sub_arr['question_id'] 		= $opt['question_id'];
										$sub_arr['sort_order'] 			= $res['sort_order'];
										$sub_arr['question_label'] 		= $opt['question_text'];
										$sub_arr['response_type'] 		= $opt['response_type_id'];
										$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
										$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
										$sub_arr['question_help_label'] = $opt['help_label'];
										//$sub_arr['salutation'] 			= $opt['salutation'];
										$sub_arr['salutation'] 			= json_encode($explodeSub);
										$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
										$sub_arr['upload_file_required']	= 'No';
										$sub_arr['if_summation']			= 'No';
										$sub_arr['if_file_required']		= 'No';			
										if($opt['input_file_required'] == 'Yes')
										{
											$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
										}
									}								
								
									$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
									$option_count = $quetionOptions->num_rows();	
									if($option_count > 0)
									{
										$optArr = array();										
										foreach($quetionOptions->result_array() as $optSub)
										{
												if($optSub['option']!="")
												{	
													array_push($optArr, $optSub['option']);	
												}	
												$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
												$multi_option_count = $queryMultipleCall->num_rows();
												if($multi_option_count > 0)
												{ 
													foreach($queryMultipleCall->result_array() as $dpenSub)
													{
														$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $dpenSub['option']);	
													}
												}
												
											if($optSub['validation_id'] > 0)
											{
												$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
												
												if($optSub['sub_validation_id'] > 0)
												{
													$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
												}
											}
											else 
											{
												$in_arr['validation_type'] = '';
												$in_arr['validation_sub_type'] = '';
											}												
											
											$in_arr['min_value'] 		= $optSub['min_value'];
											$in_arr['max_value'] 		= $optSub['max_value'];
											$in_arr['validation_label']	= $optSub['validation_label'];		
										}
										
										//$sub_arr['sub_question_options'] = json_encode($optArr);
										if(count($optArr) > 0)
										{
											$sub_arr['sub_question_options'] = json_encode($optArr);
										} 
										else 
										{
											$blank = array('');
											//$blank = array();
											$sub_arr['sub_question_options'] = json_encode($blank);
										}
																		
									}
									//$in_arr['sub_question'] = json_encode($sub_arr);
									$in_arr['sub_question'] = $sub_arr;
									//$in_arr['sub_question'][] = $sub_arr;
									
									
									
								} // If SubQuestion Attached End
								
								// Get Option 
								$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
								$option_count_1 = $quetionOptions_1->num_rows();	
								
								if($option_count_1 > 0)
								{
									$optArr_1 = array();
									foreach($quetionOptions_1->result_array() as $opt_1)
									{
										if($opt_1['option']!="")
										{
											array_push($optArr_1, $opt_1['option']); 
										}
										
										// Multiple Question Loop
										$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
											}
										}
										
										if($opt_1['validation_id'] > 0)
										{
											$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
											
											if($opt_1['sub_validation_id'] > 0)
											{
												$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
											}
										}
										else 
										{
											$in_arr['validation_type'] = '';
											$in_arr['validation_sub_type'] = '';
										}	
										
										$in_arr['min_value'] 		= $opt_1['min_value'];
										$in_arr['max_value'] 		= $opt_1['max_value'];
										$in_arr['validation_label']	= $opt_1['validation_label'];	
									}										
									
									//$in_arr['options'] = json_encode($optArr_1);
									if(count($optArr_1) > 0)
									{
										$in_arr['options'] = json_encode($optArr_1);
									}
									else
									{
										$blank = array("");
										$in_arr['options'] = json_encode($blank);
										//$in_arr['options'] = '[]';
									}	
								}									
								
								$sectionQuestion[] = $in_arr;									
							}								
															
						} // Foreach End	
						
						$sectionArr['section_question']	= $sectionQuestion;
					} 
					else 
					{
						$blank = array();
						$sectionArr['section_question']	= $blank;
					}
					

					// Sub Section Start 
					$subsectionQuery = $this->db->query("SELECT section_id, section_name, section_desc, parent_section_id 
					FROM survey_section_master 			
					WHERE parent_section_id = '".$sectionValues['section_id']."' AND status = 'Active' AND is_deleted = '0'");
					$numRowSubSection = $subsectionQuery->num_rows();
					
					$subSectionResponseArr = array();
					if(count($numRowSubSection) > 0)
					{
						
						foreach($subsectionQuery->result_array() as $subsectionValues )
						{
							$subsectionArr = array();
							$subsectionArr['section_id'] 			= $subsectionValues['parent_section_id'];
							$subsectionArr['sub_section_id'] 		= $subsectionValues['section_id']; 
							$subsectionArr['section_name'] 			= $subsectionValues['section_name']; 
							$subsectionArr['section_description'] 	= $subsectionValues['section_desc'];
							
							
							// Sub Section Question Built Start							
							$querysubSectionQuestion = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$surveyResult->survey_id."'  AND status = 'Active' AND is_deleted = '0' AND section_id='".$sectionValues['section_id']."' AND sub_section_id='".$subsectionValues['section_id']."' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
						
							$subsectionQuesitoncount = $querysubSectionQuestion->num_rows();
						
							if($subsectionQuesitoncount > 0)
							{		
								$subsectionQuestion = array();
								foreach($querysubSectionQuestion->result_array() as $res)
								{	
									$in_arr = array();
									//$in_arr['section_id'] 		= $res['section_id'];
									//$in_arr['sub_section_id'] 	= $res['sub_section_id'];
									//$in_arr['question_id'] 		= $res['question_id'];
									
									$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
									$if_parents_count = $if_checked_parent->num_rows();
									
									if($if_parents_count == 0)
									{									
										$global_question_id 		= $res['global_question_id'];
										if($global_question_id > 0)
										{
											$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
										}
										
										$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
										$questionData = $queryQuestion->row();	

										$explodeSub =  array();
										if($questionData->salutation!="")
										{
											$explodeSub = explode("|", $questionData->salutation);
										} 
										
										$in_arr['question_id'] 			= $questionData->question_id;
										$in_arr['sort_order'] 			= $res['sort_order'];
										$in_arr['question_label'] 		= $questionData->question_text;
										$in_arr['response_type'] 		= $questionData->response_type_id;
										$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
										$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
										$in_arr['question_help_label'] 	= $questionData->help_label;
										//$in_arr['salutation'] 			= $questionData->salutation;
										$in_arr['salutation'] 			= json_encode($explodeSub);
										$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
										$in_arr['upload_file_required']	= 'No';
										$in_arr['if_summation']			= 'No';	
										$in_arr['if_file_required']		= 'No';			
										if($questionData->input_file_required == 'Yes')
										{
											$in_arr['upload_file_required'] 	= $questionData->input_file_required;
										}
										
										if($questionData->if_file_required == 'Yes')
										{
											$in_arr['if_file_required'] 	= $questionData->if_file_required;
											$in_arr['file_label'] 			= $questionData->file_label;
										}
										
										if($questionData->if_summation == 'Yes')
										{
											$in_arr['if_summation'] 		= $questionData->if_summation;
										}									
										
										// Get Sub Question List On Main Question 								
										$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
										$sub_questuon_count = $subQuestion->num_rows();
										
										if($sub_questuon_count > 0)
										{									
											foreach($subQuestion->result_array() as $opt)
											{
												$sub_arr = array();
												$explodeSub =  array();
												if($opt['salutation']!="")
												{
													$explodeSub = explode("|", $opt['salutation']);
												}  
												$sub_arr['question_id'] 		= $opt['question_id'];
												$sub_arr['sort_order'] 			= $res['sort_order'];
												$sub_arr['question_label'] 		= $opt['question_text'];
												$sub_arr['response_type'] 		= $opt['response_type_id'];
												$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
												$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
												$sub_arr['question_help_label'] = $opt['help_label'];
												//$sub_arr['salutation'] 			= $opt['salutation'];
												$sub_arr['salutation'] 			= json_encode($explodeSub);
												$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
												$sub_arr['upload_file_required']	= 'No';
												$sub_arr['if_summation']			= 'No';	
												$sub_arr['if_file_required']		= 'No';			
												if($opt['input_file_required'] == 'Yes')
												{
													$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
												}
												if($opt['if_summation'] == 'Yes')
												{
													$sub_arr['if_summation'] 		= $opt['if_summation'];
												}
											}								
										
											$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0'");
											$option_count = $quetionOptions->num_rows();	
											if($option_count > 0)
											{
												$optArr = array();										
												foreach($quetionOptions->result_array() as $optSub)
												{
													if($optSub['option']!="")
													{	
														array_push($optArr, $optSub['option']);	
													}		
														$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
														$multi_option_count = $queryMultipleCall->num_rows();
														if($multi_option_count > 0)
														{ 
															foreach($queryMultipleCall->result_array() as $dpenSub)
															{
																$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option']);	
															}
														}
														
														if($optSub['validation_id'] > 0)
														{
															$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
															
															if($optSub['sub_validation_id'] > 0)
															{
																$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
															}
														}
														else 
														{
															$in_arr['validation_type'] = '';
															$in_arr['validation_sub_type'] = '';
														}	
														$in_arr['min_value'] 		= $optSub['min_value'];
														$in_arr['max_value'] 		= $optSub['max_value'];
														$in_arr['validation_label']	= $optSub['validation_label'];	
																							
												}
													
												$sub_arr['sub_question_options'] = json_encode($optArr);		
																				
											}
											
											$in_arr['sub_question'] = $sub_arr;
											//$in_arr['sub_question'][] = $sub_arr;
											
										} // If SubQuestion Attached End
										
										// Get Option 
										$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0'");
										$option_count_1 = $quetionOptions_1->num_rows();	
										
										if($option_count_1 > 0)
										{
											$optArr_1 = array();
											foreach($quetionOptions_1->result_array() as $opt_1)
											{
												if($opt_1['option']!="")
												{
													array_push($optArr_1, $opt_1['option']); 
												}
												
												// Multiple Question Loop
												$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
												$multi_option_count = $queryMultipleCall->num_rows();
												if($multi_option_count > 0)
												{ 
													foreach($queryMultipleCall->result_array() as $dpenSub)
													{
														$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option']);	
													}
												}
												
													if($opt_1['validation_id'] > 0)
													{
														$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
														
														if($opt_1['sub_validation_id'] > 0)
														{
															$in_arr['validation_sub_type'] =$this->validation_sub_response_type($opt_1['sub_validation_id']);
														}
													}
													else 
													{
														$in_arr['validation_type'] = '';
														$in_arr['validation_sub_type'] = '';
													}								
													
													$in_arr['min_value'] 		= $opt_1['min_value'];
													$in_arr['max_value'] 		= $opt_1['max_value'];
													$in_arr['validation_label']	= $opt_1['validation_label'];	
												
											}										
											
											//$in_arr['options'] = json_encode($optArr_1);	
											if(count($optArr_1) > 0)
											{
												$in_arr['options'] = json_encode($optArr_1);
											}
											else
											{
												$blank = array("");
												$in_arr['options'] = json_encode($blank);
												//$in_arr['options'] = '[]';
											}	
										}									
										
										$subsectionQuestion[] = $in_arr;									
									}								
																	
								} // Foreach End	
								
								$subsectionArr['sub_section_question']	= $subsectionQuestion;
							} 
							else 
							{
								$blank = array("");
								$subsectionArr['sub_section_question']	= json_encode($blank);
								//$blank = array();
								//$subsectionArr['sub_section_question']	= $blank;
							}
							
							
							// Sub Section Question Built End
							
							$subSectionResponseArr[] = $subsectionArr;
						}
						
						
					}
					$sectionArr['sub_section'] = $subSectionResponseArr;
					
					
					// Sub Section End
					
					$response['data']['sections'][]	= $sectionArr;
				}
			}
			else 
			{
				$blank = array();				
				$response['data']['sections'][]	= $blank;
			}
			
			// End Section Code
		}
		
		$this->response($response);
		
	}	
	
	// gloabal question function 
	public function is_global_question($question_id, $global_question_id)
	{
		$in_arr = array();		
		$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$global_question_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			 // get the first row
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			}  
			$in_arr['question_id'] 			= $questionData->question_id;
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $question_id;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_file_required']		= 'No';	
			$in_arr['if_summation']			= 'No';								
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}

			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$global_question_id."' AND is_deleted='0'");
		
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="") 
					{
						array_push($optArr_1, $opt_1['option']); 
					}
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] =$this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}	
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}
				
				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
												
			}
			
		}
		
		$dataArr = array("question_id" => $question_id, "global_question" => $in_arr);	
		
		return $in_arr;
	}
	
	// Validation Response Type 
	public function validation_response_type($id)
	{
		$responseValiation = $this->db->query("SELECT validation_type FROM survey_response_validation_master WHERE validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$responseData = $responseValiation->row();	
		return $responseData->validation_type;
	}
	
	// Survey Statics
	public function survey_statistics() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(!empty($user_id) && is_user_login($user_id))
        {		
			if(empty($survey_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Survey ID is required.";
				
			} else {

				// Draft Survey Count
				$draftSurvey = $this->db->query("SELECT count(response_id) AS DRAFT_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Draft' AND is_deleted='0' AND surveyer_id = '".$user_id."'");
				$draftData 	= $draftSurvey->row();
				$draftCount = $draftData->DRAFT_SURVEY;	
				
				// Cancel Survey Count
				/*$cancelSurvey = $this->db->query("SELECT count(response_id) AS CANCEL_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Cancel' AND is_deleted='0' AND surveyer_id = '".$user_id."'");
				//$cancelCount = $cancelSurvey->num_rows();
				$cancelData 	= $cancelSurvey->row();
				$cancelCount	= $cancelData->CANCEL_SURVEY;	
				
				// Close Survey Count
				$closeSurvey = $this->db->query("SELECT count(response_id) AS CLOSE_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND status='Close' AND is_deleted='0' AND surveyer_id = '".$user_id."'");
				//$closeCount = $closeSurvey->num_rows();
				$closeData 	= $closeSurvey->row();
				$closeCount	= $closeData->CLOSE_SURVEY;	*/			
				
				// Approved Survey Count
				$approvedSurvey = $this->db->query("SELECT count(response_id) AS APPROVED_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND (status='Level 1 Approved' OR status='Level 2 Approved') AND is_deleted='0' AND surveyer_id = '".$user_id."'");				
				$approvedData 	= $approvedSurvey->row();
				$approvedCount	= $approvedData->APPROVED_SURVEY;
				
				// Return Survey Count
				$returnSurvey = $this->db->query("SELECT count(response_id) AS RETURN_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND (status='Level 1 Returned' OR status='Level 2 Returned') AND is_deleted='0' AND surveyer_id = '".$user_id."'");				
				$returnData 	= $returnSurvey->row();
				$returnCount	= $returnData->RETURN_SURVEY;				
				
				// Publish Survey Count
				$publishSurvey = $this->db->query("SELECT count(response_id) AS TOTAL_SUBMITTED_SURVEY FROM survey_response_headers WHERE survey_id='".$survey_id."' AND (status='Submitted' OR status='Re-Submitted' OR status='Level 1 Returned' OR status='Level 2 Returned' OR status='Level 1 Approved' OR status='Level 2 Approved' OR status='Level 1 In Review' OR status='Level 2 In Review') AND is_deleted='0' AND surveyer_id = '".$user_id."'");
				//$publishCount = $publishSurvey->num_rows();
				$publishData 	= $publishSurvey->row();
				$publishCount	= $publishData->TOTAL_SUBMITTED_SURVEY;
				
				$response['status'] 	= "1";
				$response['message'] 	= "Survey statistic get successfully.";
				//$response['data'] 		= array("Draft" => $draftCount, "Cancel" => $cancelCount, "Close" => $closeCount, "Total Submitted" => $publishCount);
				
				$response['data'] = array("approved" => $approvedCount, "returned" => $returnCount, "submitted" => $publishCount);
				
			}
		}
		else
		{
			$response['status']		= "0";
			$response['message']    = "You must be logged in to perform this request.";
		}
		
		$this->response($response);
	}
	
	// Validation Sub Response Type 
	public function validation_sub_response_type($id)
	{
		$responseSubValiation = $this->db->query("SELECT validation_sb_type FROM survey_response_validation_subtype_master WHERE sub_validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$subresponseData = $responseSubValiation->row();	
		return $subresponseData->validation_sb_type;
	}
	
	// Email Function 
	public function email_to_approver($survey_id, $surveyer_id, $village_id, $district_id, $appDetails)
	{ 		
		
		//Survey Details
		$survey_details = $this->master_model->getRecords("survey_master",array('survey_id'=>$survey_id));
		$survey_name = $survey_details[0]['title'];	
		
		//Surveyor Details
		$surveyorDetails = $this->master_model->getRecords("survey_users",array('user_id'=>$surveyer_id));	
		$surveyor_name  = @ucfirst($surveyorDetails[0]['first_name'])." ".@ucfirst($surveyorDetails[0]['last_name']);	
		$surveyor_email = @$surveyorDetails[0]['email'];	
		//echo ">>>".$surveyor_name;die();
		//Village Details
		$villageDetails = $this->master_model->getRecords("survey_village_master",array('village_id'=>$village_id));
		$village_name   = $villageDetails[0]['village_name'];

		//District Details
		$districtDetails = $this->master_model->getRecords("survey_district_master",array('district_id'=>$district_id));
		$district_name   = $districtDetails[0]['district_name'];		
		
		//$array = array("vicky.kuwar87@esds.co.in","tejas.sonar@esds.co.in");
			//print_r($appDetails);
			//echo "+++".count($appDetails);die();
			if(count($appDetails) > 0)
			{
				foreach($appDetails as $key => $detailsApp)
				{					
					$approver_name  = ucfirst($detailsApp['first_name'])." ".ucfirst($detailsApp['last_name']);	
					$approver_email = $detailsApp['email'];
					$arrayDetails 	= array('approver_name' => $approver_name, 'approver_email' => $approver_email, 'surveyer_id' => $surveyer_id, 'surveyor_name' => $surveyor_name, 'district_name' => $district_name, 'village_name' => $village_name); 
					//$this->send_mail_to_approver($approver_name, $approver_email, $surveyer_id, $surveyor_name, $survey_name, $district_name, $village_name);
					//print_r($arrayDetails);
					$this->send_mail_to_approver($arrayDetails);
			} // Foreach End
		} // Count End		
	} // End Email Function
	
	// Email Sent Functionality
	public function send_mail_to_approver($arrayDetails)
	{
		$slug = 'survey_notification_to_approver';
		$email_body = $this->master_model->getRecords("email_master", array("slug" => $slug));
		if(count($email_body) > 0)
		{			
			//$from_admin 		= $email_body[0]['from_email'];
			$from_admin 		= $this->config->item('from_email');
			$email_content 		= $email_body[0]['email_body'];
			$email_subject 		= $email_body[0]['email_subject'];
			
			$subArr	= ['[SURVEYORNAME]','[VILLAGENAME]'];
			$chaArr = [$arrayDetails['surveyor_name'], $arrayDetails['village_name']];
			$subChange = str_replace($subArr, $chaArr, $email_subject);

			$arr_words 		= ['[APPROVERNAME]', '[ID]', '[SURVEYORNAME]', '[TYPE]', '[DISTRICTNAME]', '[VILLAGENAME]'];
			$rep_array 		= [$arrayDetails['approver_name'], $arrayDetails['surveyer_id'], $arrayDetails['surveyor_name'], $arrayDetails['survey_name'], $arrayDetails['district_name'], $arrayDetails['village_name']];		
			$content 		= str_replace($arr_words, $rep_array, $email_content);
						
			//email admin sending code 
			$contentArray = array(
									'to'		=>	$arrayDetails['approver_email'],//$approver_email					
									'cc'		=>	'',
									'from'		=>	$from_admin,//$from_admin
									'subject'	=> 	$subChange,
									'view'		=>  'common-file'
								);
		
			$other_info	=	array('content'=>$content);
			//print_r($other_info);
			//print_r($contentArray);
			$emailsend 	= 	$this->emailsending->sendmail($contentArray,$other_info);
			
			if($emailsend){							
				return true;
			}else { 
				return false;
			}
		} // Email Slug End
	}
	
	// Post Data Clean function
	public function sql_clean($con, $arr)
	{
		$raw_post = array();
		foreach ($arr as $key => $value)// loop out array
		{
			if(!is_array($value))
			{
				//$str_tmp = html_entity_decode($value);
				//$str_tmp = stripcslashes ($str_tmp);
				//$str_tmp = addslashes ($str_tmp);
				//$str_tmp = htmlentities($str_tmp);
				$raw_post[$key] = @mysqli_real_escape_string($con, $str_tmp);
				//$raw_post[$key] = strip_tags($str_tmp);
			}
			else
				$raw_post[$key] = $value;
		}
		return $raw_post;
	}
	
	// Function To get response 
	public function save_response()
	{ 
		$response = array("status" => 0, "message" => "");
		//$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING); 
		$surveyData = $_POST;
		//$surveyData = $this->clean($post_data);
		//echo "===";print_r($surveyData);die();
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} 		
		else 
		{			
			if(count($surveyData['data']) > 0)
			{
				$education_survey_id = $this->config->item('education_survey_id');
				$surveyor_role_id 	 = $this->config->item('surveyor_role_id');
				$approver1_role_id 	 = $this->config->item('approver1_role_id');
				
				$survey_id 			= $surveyData['data']['survey_id'];
				$response_id 		= $surveyData['data']['responses'][0]['response_id'];
				$ref_id 			= $surveyData['data']['responses'][0]['ref_id'];
				$submitted_date 	= $surveyData['data']['responses'][0]['submitted_date'];
				$submited_time 		= $surveyData['data']['responses'][0]['submited_time'];
				$district_id 		= $surveyData['data']['responses'][0]['district_id'];
				//$header_latitude 	= $surveyData['data']['responses'][0]['latitude'];
				//$header_longitude 	= $surveyData['data']['responses'][0]['longitude'];
				
				$header_latitude 	= isset($surveyData['data']['responses'][0]['latitude'])? $surveyData['data']['responses'][0]['latitude']:"";		
				$header_longitude 	= isset($surveyData['data']['responses'][0]['longitude'])? $surveyData['data']['responses'][0]['longitude']:"";
															
				if($survey_id == $education_survey_id)
				{
					$revenue_id = 0;
				}
				else 
				{
					$revenue_id = $surveyData['data']['responses'][0]['revenue_id'];
				}
				
				$block_id 			= $surveyData['data']['responses'][0]['block_id'];
				$panchayat_id 		= $surveyData['data']['responses'][0]['panchayat_id'];
				$village_id 		= $surveyData['data']['responses'][0]['village_id'];
				$survey_status 		= $surveyData['data']['responses'][0]['status'];
				$submitDate = date('Y-m-d', strtotime($submitted_date));
				
				$start_date_time    = date("Y-m-d H:i:s");
				$end_date_time      = date("Y-m-d H:i:s");	
				if($surveyData['data']['responses'][0]['start_date_time']!="")
				{
					$start_date_time	= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['start_date_time']));
				}
				
				if($surveyData['data']['responses'][0]['end_date_time']!="")
				{
					$end_date_time 		= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['end_date_time']));
				}
				
				// Check ID Exist related response id 
				$query = $this->db->query("SELECT survey_id FROM survey_master WHERE survey_id = '".$survey_id."' AND is_deleted = '0' AND status != 'Draft'");
				//echo $this->db->last_query();
				$surveyCnt = $query->num_rows();				
				
				// Replace when implement token value 				
				if($surveyCnt > 0) 
				{ 
					// Check ID Exist related response id 
					$query = $this->db->query("SELECT * FROM survey_response_headers WHERE response_id = '".$response_id."' AND is_deleted = '0'");
					$cnt = $query->num_rows();
					$ref_res = true;
					if($cnt == '0') 
					{						
						// Check Ref ID Exist related response id 
						$queryReference = $this->db->query("SELECT * FROM survey_response_headers WHERE ref_id = '".$ref_id."' AND is_deleted = '0'");
						$cntRef = $queryReference->num_rows();
					
						if($cntRef == '0')
						{
							$insertArr = array( "ref_id" 	=> $ref_id, 
												"survey_id" => $survey_id,
												"surveyer_id" => $user_id,
												"submitted_date" => $submitDate,
												"submited_time" => $submited_time,
												"district_id" => $district_id,
												"revenue_id" => $revenue_id,
												"block_id" => $block_id,
												"panchayat_id" => $panchayat_id,
												"village_id" => $village_id,
												"start_date" => $start_date_time,
												"end_date" => $end_date_time,
												"latitude" => $header_latitude,
												"longitude" => $header_longitude,
												"status" => $survey_status,
												"created_by_id" => $user_id	
										);
							$surveyQuery = $this->master_model->insertRecord('response_headers',$insertArr);
							$cid = $this->db->insert_id();	
							
							//Email To Approver New Response Received					
							$where = "FIND_IN_SET('".$district_id."', district_id)"; 
							$this->db->where( $where );
							$this->db->where("role_id", $approver1_role_id);
							$this->db->where("is_deleted", '0');
							$approverDetails = $this->master_model->getRecords("survey_users");	
							//echo $this->db->last_query();die();
							
							// Surveyor Related Details
							$where_s = "FIND_IN_SET('".$district_id."', district_id)"; 
							$this->db->where( $where_s );
							$this->db->where("role_id", $surveyor_role_id);
							$this->db->where("is_deleted", '0');
							$surveyorDetails = $this->master_model->getRecords("survey_users");	
							$surveyor_ids = @$surveyorDetails[0]['user_id'];
									
							@$this->email_to_approver($survey_id, $surveyor_ids, $village_id, $district_id, $approverDetails);
							$updateFlag = false;
							//$ref_res = true;
						}
						else 
						{
							//echo "No Sections Found";
							$response['status'] 	= "0";
							$response['message'] 	= "This survey form already exist.";
							$response['data']		= array("ref_id" => $ref_id);
							
							$ref_res = false;
						}						
					}
					else 
					{
						$updateArr = array( /*"survey_id" => $survey_id,
											"submitted_date" => $submitDate,
											"submited_time" => $submited_time,
											"district_id" => $district_id,
											"revenue_id" => $revenue_id,
											"block_id" => $block_id,
											"panchayat_id" => $panchayat_id,
											"village_id" => $village_id,
											"latitude" => $header_latitude,
											"longitude" => $header_longitude,
											"end_date" => $end_date_time,*/
											"status" => $survey_status,
											"updated_on" => date("Y-m-d H:i:s"),
											"updated_by_id" => $user_id		
											);
						$updateQuery = $this->master_model->updateRecord('response_headers',$updateArr,array('survey_id' => $survey_id, 'surveyer_id' => $user_id, "response_id" => $response_id));					
						$cid = $response_id;
						$updateFlag = true;	
					}
					//echo $this->db->last_query();die();
					
					// If Ref Id Condition Is True
					if($ref_res)
					{
						// Section Array Check
						if(count($surveyData['data']['responses'][0]['sections']) > 0)
						{
							foreach($surveyData['data']['responses'][0]['sections'] as $key => $sections)
							{
								$section_id 	= $sections['section_id'];
								$section_name 	= $sections['section_name'];
								$section_desc 	= @$sections['section_description'];	
								
								if(count($sections['section_question']) > 0)
								{					
									foreach($sections['section_question'] as $key_val => $sectionQuestions)
									{							
										$question_id 		= $sectionQuestions['question_id'];
										$option_type	 	= $sectionQuestions['response_type'];
										$inputMoreThanOne 	= $sectionQuestions['input_more_than_1'];
										$input_file_required= $sectionQuestions['upload_file_required'];							
										$salutation 		= $sectionQuestions['salutation'];
										$options			= $sectionQuestions['options'];
										$if_file_required   = @$sectionQuestions['if_file_required'];
										//$answer 			= @$sectionQuestions['answer'];
										$answer 			= (@$sectionQuestions['answer'])? @$sectionQuestions['answer']:"";
										//$global_question 	= @$sectionQuestions['global_question'];
										$global_question 	= (@$sectionQuestions['global_question'])? @$sectionQuestions['global_question']:"";
										$dependent_question = @$sectionQuestions['dependent_question_options'];
										$sub_question 		= @$sectionQuestions['sub_question'];									
										
										//Geo Tag Location 
										$latitude 			= @$sectionQuestions['latitude'];
										$longitude 			= @$sectionQuestions['longitude'];
											
												//if(is_array($global_question['answer']))
												//{										
													if(count(@$global_question['answer']) > 0) 
													{	
														$g_question_id = @$global_question['question_id'];
														$g_parent_question_id = @$global_question['parent_question_id'];
														$g_response_type = @$global_question['response_type'];														
														if($global_question['upload_file_required'] == 'Yes' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
														{	
															$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
															$image_name = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
															$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
															$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
															$img_global_name = '';										
															if($image_name!="") 
															{
																$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
																$img_global_name = $this->generateImage($random_name, $image_name);
															}
														
															foreach($global_question['answer']['value'] as $key => $value) 
															{												
																$img_multiple_name = '';										
																if($value['upload_file']!="") 
																{
																	$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
																	$img_multiple_name = $this->generateImage($random_name, $value['upload_file']);
																}
																
																$insertArr = array("response_id" 		=> $cid,
																					"question_id" 		=> $g_question_id,
																					"key_name" 			=> '',
																					"answer_value" 		=> $value['value'],
																					"file_name" 		=> $img_global_name,
																					"salultation_value" => $salutation_name,
																					"input_file_required" => $img_multiple_name,
																					"parent_question_id" => $g_parent_question_id,
																					"latitude" 			=> $latitude,
																					"longitude" 		=> $longitude);
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
																$cnt = $queryResponse->num_rows();															
																if($cnt > 0){$flag = 1;}else{$flag = 0;}															
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
																}										
															}
														} 

													if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
													{	
													
														$image_name = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
														$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
														$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";

														$img_global_name = '';										
														if($image_name!="") 
														{
															$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
															$img_global_name = $this->generateImage($random_name, $image_name);
														}	
														
														foreach($global_question['answer']['value'] as $key => $value) 
														{										
															$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
																																						
															//Table Name : survey_response_headers
															$insertArr = array("response_id" => $cid,
																				"question_id" => $g_question_id,
																				"key_name" => $key,
																				"answer_value" => $value,
																				"file_name" => $img_global_name,
																				"salultation_value" => $salutation_name,
																				"parent_question_id" => $g_parent_question_id,
																				"latitude" => $latitude,
																				"longitude" => $longitude);
																				
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
															}	
														}
													} 	
													
													if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
													{
														$image_name = isset($global_question['image_url'])? $global_question['image_url']:"";
														$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
														$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
														$img_global_name = '';										
														if($image_name!="") 
														{
															$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
															$img_global_name = $this->generateImage($random_name, $image_name);
														}
															
														foreach($global_question['answer'] as $key => $value) 
														{										
															//Table Name : survey_response_headers
															$image_name = "";
															
															if($key == "image_url") 
															{
																
															}
															else 
															{	
																$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";												
																
																$insertArr = array("response_id" => $cid,
																					"question_id" => $g_question_id,
																					"key_name" => $key,
																					"answer_value" => $value,
																					"file_name" => $img_global_name,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $g_parent_question_id,
																					"latitude" => $latitude,
																					"longitude" => $longitude);
															
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id, "key_name" => $key));
																	//echo ">>>".$this->db->last_query();die();
																}		
															}
														}
													}	
																								
													if($global_question['upload_file_required'] == 'Yes' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'Yes' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
													{												
														foreach($global_question['answer']['value'] as $key => $value) 
														{										
															$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
															$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
															$img_global_name = '';										
															if($value['upload_file']!="") 
															{
																$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
																$img_global_name = $this->generateImage($random_name, $value['upload_file']);
															}
														
															//Table Name : survey_response_headers
															$insertArr = array("response_id" => $cid,
																				"question_id" => $g_question_id,
																				"answer_value" => $value['value'],
																				"salultation_value" => $salutation_name,
																				"input_file_required" => $img_global_name,
																				"parent_question_id" => $g_parent_question_id,
																				"latitude" => $latitude,
																				"longitude" => $longitude);
																		
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
															}	
														}
													}
													
													if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "textbox" || $g_response_type == "textarea"))
													{
															
														$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
														$insertArr = array("response_id" => $cid,
																			"question_id" => $g_question_id,
																			"answer_value" => $global_question['answer']['value'],
																			"salultation_value" => $salutation_name,
																			"parent_question_id" => $g_parent_question_id);
																	
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND answer_value='".$answer['value']."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
															}	
													}	

													// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute													
													if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'Yes' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "radio" || $g_response_type == "checkbox"  || $g_response_type == "single_select" || $g_response_type == "file"))
													{
														
														$imgUrl = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
														$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
														$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
														$img_global_name = '';										
														if($imgUrl!="") 
														{
															$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
															$img_global_name = $this->generateImage($random_name, $imgUrl);
														}

														if($g_response_type == "file") 
														{
															
															$selectedFile = $global_question['answer']['value'];
															$img_global_name = '';										
															if($selectedFile!="") 
															{
																$random_name_2 = $survey_id."_".$g_question_id."_".time()."_".rand();
																$img_global_name_2 = $this->generateImage($random_name_2, $selectedFile);
															}
															$insertArr = array("response_id" => $cid,
																				"question_id" => $g_question_id,
																				"answer_value" => $img_global_name_2,
																				"file_name" => $img_global_name,
																				"salultation_value" => $salutation_name,
																				"parent_question_id" => $g_parent_question_id,
																				"latitude" => $latitude,
																				"longitude" => $longitude);
															
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																			
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $g_parent_question_id));
															}	
															
														}												
														else 
														{
															foreach($global_question['answer'] as $key => $value) 
															{
																$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
																$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
																$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
																if($key == "image_url") 
																{
																	
																}
																else 
																{
																	if(is_array($value))
																	{
																	
																		foreach($value as $names => $namValue) 
																		{
																			
																			if($g_response_type == 'checkbox' || $g_response_type == 'radio' ||  $g_response_type == 'single_select') 
																			{
																				if(is_numeric($names))
																				{
																					$nm = "";
																				}
																				else 
																				{
																					$nm = $names;
																				}
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $g_question_id,
																								"key_name" => $nm,
																								"answer_value" => $namValue['value'],
																								"file_name" => $img_global_name,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $g_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																			}
																			else 
																			{
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $g_question_id,
																								"answer_value" => $namValue['value'],
																								"file_name" => $img_global_name,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $g_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																			}
																			
																		
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																			
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $g_parent_question_id));
																			}														
																		}
																	}
																	else
																	{
																		$insertArr = array("response_id" => $cid,
																							"question_id" => $g_question_id,
																							"answer_value" => $value,
																							"file_name" => $img_global_name,
																							"salultation_value" => $salutation_name,
																							"parent_question_id" => $question_id,
																							"latitude" => $latitude,
																							"longitude" => $longitude);
																	
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$question_id."'" );
																		
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $question_id));
																		}		
																	}	
																}
															}

														}
																
													}
													
													// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute																				
													if($global_question['upload_file_required'] == 'No' && $global_question['if_file_required'] == 'No' && $global_question['input_more_than_1'] == 'No' && ($g_response_type == "radio" || $g_response_type == "checkbox"  || $g_response_type == "single_select" || $g_response_type == "file"))
													{
														$chkArrpush = array();
														$imgUrl = isset($global_question['answer']['image_url'])? $global_question['answer']['image_url']:"";
														$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
														$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
														if($g_response_type == "file") 
														{
															$img_global_name = '';										
															if($global_question['answer']['value']!="") 
															{
																$random_name = $survey_id."_".$g_question_id."_".time()."_".rand();
																$img_global_name = $this->generateImage($random_name, $global_question['answer']['value']);
															}
															
															
															$insertArr = array("response_id" => $cid,
																				"question_id" => $g_question_id,
																				"answer_value" => $img_global_name,
																				"parent_question_id" => $g_parent_question_id,
																				"latitude" => $latitude,
																				"longitude" => $longitude);
																				
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																			
															$cnt = $queryResponse->num_rows();
															
															if($cnt > 0){$flag = 1;}else{$flag = 0;}
																
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id,"parent_question_id" => $g_parent_question_id));
															}					
															
														}
														else 
														{
															
															foreach($global_question['answer'] as $key => $value) 
															{	

																$salutation_name = isset($global_question['answer']['salutation'])? $global_question['answer']['salutation']:"";
																$latitude 	= isset($global_question['latitude'])? $global_question['latitude']:"";		
																$longitude 	= isset($global_question['longitude'])? $global_question['longitude']:"";
																if($key == "image_url") 
																{
																	
																}
																else 
																{
																	
																	if(is_array($value))
																	{
																		
																		foreach($value as $names => $namValue) 
																		{
																			
																			if($g_response_type == 'checkbox' ||  $g_response_type == 'radio' ||  $g_response_type == 'single_select') 
																			{
																				array_push($chkArrpush, $names);
																				if(is_numeric($names))
																				{
																					$nm = "";
																				}
																				else 
																				{
																					$nm = $names;
																				}
																				
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $g_question_id,
																								"key_name" => $nm,
																								"answer_value" => $namValue,
																								"file_name" => $imgUrl,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $g_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																								
																			}
																			else 
																			{
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $g_question_id,
																								"answer_value" => $namValue,
																								"file_name" => $imgUrl,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $g_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																			}
																			
																			
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$g_parent_question_id."'" );
																			
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "key_name" => $names, "parent_question_id" => $g_parent_question_id));
																			}														
																		}												
																		
																	}
																	else 
																	{		
																		if($g_response_type == 'checkbox' ||  $g_response_type == 'radio' ||  $g_response_type == 'single_select') 
																		{	

																			array_push($chkArrpush, $key);
																			$insertArr = array("response_id" => $cid,
																							"question_id" => $g_question_id,
																							"key_name" => $key,
																							"answer_value" => $value,
																							"file_name" => $imgUrl,
																							"salultation_value" => $salutation_name,
																							"parent_question_id" => $question_id,
																							"latitude" => $latitude,
																							"longitude" => $longitude);
																							
																							
																		}
																		else 
																		{	
																			$insertArr = array("response_id" => $cid,
																							"question_id" => $g_question_id,
																							"answer_value" => $value,
																							"key_name" => $key,
																							"file_name" => $imgUrl,
																							"salultation_value" => $salutation_name,
																							"parent_question_id" => $question_id,

																							"latitude" => $latitude,
																							"longitude" => $longitude);
																		}
																			
																		//Table Name : survey_response_headers														
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$g_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$question_id."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $question_id, "key_name" => "'".$key."'"));
																		}
																	}														
																	//print_r($chkArrpush);
																	// Delete Previous Answers regarding Checkbox and Selectbox
																	if($updateFlag)
																	{
																		if(count($chkArrpush) > 0) 
																		{
																			$updatedAt = date('Y-m-d H:i:s');
																			$this->db->where_not_in('key_name', $chkArrpush);
																			$updateArr = array("is_deleted" => '1');
																			$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $g_question_id, "parent_question_id" => $question_id));
																			//echo $this->db->last_query();
																		}
																																
																	}			
																}
																
															}
															
																												
														}
													}												
													
													}
													else 
													{
														if($updateFlag)
														{
															$this->removeAnswers($g_question_id, $g_parent_question_id, $response_id);
														}										
													}
												//} // Globle Question End
										
									
										if(is_array($answer))
										{									
											if(count($answer) > 0) 
											{								
												if($input_file_required == 'Yes' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
												{
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													$image_name = isset($answer['image_url'])? $answer['image_url']:"";
													$img_latitude 	= isset($latitude)? $latitude:"";		
													$img_longitude 	= isset($longitude)? $longitude:"";
													
													$img_origional_name = '';										
													if($image_name!="") 
													{
														$random_name = $survey_id."_".$question_id."_".time()."_".rand();
														$img_origional_name = $this->generateImage($random_name, $image_name);
													}												
													foreach($answer['value'] as $key => $value) 
													{												
														$img_multiple_name = '';
														if($value['upload_file']!="") 
														{
															$random_name = $survey_id."_".$question_id."_".time()."_".rand();
															$img_multiple_name = $this->generateImage($random_name, $value['upload_file']);
														}													
														//if($value['value']!="")
														//{
															$insertArr = array("response_id" 		=> $cid,
																				"question_id" 		=> $question_id,
																				"answer_value" 		=> $value['value'],
																				"file_name" 		=> $img_origional_name,
																				"salultation_value" => $salutation_name,
																				"input_file_required" => $img_multiple_name,
																				"latitude" 			=> $img_latitude,
																				"longitude" 		=> $img_longitude);
																	
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();														
															if($cnt > 0){$flag = 1;}else{$flag = 0;}														
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $question_id, "parent_question_id" => $question_id));
															}
														//}										
													}
												} 
												
												if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
												{	
													$image_name = isset($answer['image_url'])? $answer['image_url']:"";	
													$img_origional_name = '';										
													if($image_name!="") 
													{
														$random_name = $survey_id."_".$question_id."_".time()."_".rand();
														$img_origional_name = $this->generateImage($random_name, $image_name);
													}	
													$img_latitude 	= isset($latitude)? $latitude:"0.00";		
													$img_longitude 	= isset($longitude)? $longitude:"0.00";
													if(!is_array($answer['value']))
													{  
														foreach($answer as $key => $value) 
														{	
															$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
															//if($value!="")
															//{
																$insertArr = array("response_id" 		=> $cid,
																					"question_id" 		=> $question_id,
																					"key_name" 			=> $key,
																					"answer_value" 		=> $value,
																					"file_name" 		=> $img_origional_name,
																					"salultation_value" => $salutation_name,
																					"latitude" 			=> $img_latitude,
																					"longitude" 		=> $img_longitude);
																		
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();														
																if($cnt > 0){$flag = 1;}else{$flag = 0;}																
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																}
															//}	
														}
													}
													else 
													{	
														foreach($answer['value'] as $key => $value) 
														{	
															$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
															$img_latitude 	= isset($latitude)? $latitude:"";		
															$img_longitude 	= isset($longitude)? $longitude:"";	
															//if($value!="")
															//{
																$insertArr = array("response_id" 		=> $cid,
																					"question_id" 		=> $question_id,
																					"key_name" 			=> $key,
																					"answer_value" 		=> $value,
																					"file_name" 		=> $img_origional_name,
																					"salultation_value" => $salutation_name,
																					"latitude" 			=> $img_latitude,
																					"longitude" 		=> $img_longitude);
																			
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();														
																if($cnt > 0){$flag = 1;}else{$flag = 0;}																
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																}
															//}	
														}
													}
												} 	

												if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
												{
													$image_name 		= isset($answer['image_url'])? $answer['image_url']:"";
													$salutation_name 	= isset($answer['salutation'])? $answer['salutation']:"";												
													$chkArrpush = array();
													foreach($answer as $key => $value) 
													{										
														$image_name = "";
														if($key == "image_url") 
														{
															
														}
														else 
														{	
															$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
															$img_latitude 	= isset($latitude)? $latitude:"";		
															$img_longitude 	= isset($longitude)? $longitude:"";
															//if($value!="")
															//{
																array_push($chkArrpush, $key);
																$insertArr = array("response_id" 		=> $cid,
																					"question_id" 		=> $question_id,
																					"key_name" 			=> $key,
																					"answer_value"		=> $value,
																					"file_name" 		=> $image_name,
																					"salultation_value" => $salutation_name,
																					"latitude" 			=> $img_latitude,
																					"longitude" 		=> $img_longitude);
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = ".$cid." AND question_id = ".$question_id." AND key_name='".$key."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{ 
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																}	
															//}
														}
													}
													if($updateFlag)
													{
														if(count($chkArrpush) > 0) 
														{
															$updatedAt = date('Y-m-d H:i:s');
															$this->db->where_not_in('key_name', $chkArrpush);
															$updateArr = array("is_deleted" => '1');
															$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
															
														}														
													}
												}	
												
												if($input_file_required == 'Yes' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($option_type == "textbox" || $option_type == "textarea"))
												{
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													foreach($answer['value'] as $key => $value) 
													{										
														$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
														$image_name 	= isset($answer['image_url'])? $answer['image_url']:"";
														$img_latitude 	= isset($latitude)? $latitude:"";		
														$img_longitude 	= isset($longitude)? $longitude:"";
														$img_origional_name = '';										
														if($image_name!="") 
														{
															$random_name = $survey_id."_".$question_id."_".time()."_".rand();
															$img_origional_name = $this->generateImage($random_name, $image_name);
														}	
														//if($value!="")
														//{
															//Table Name : survey_response_headers
															$insertArr = array("response_id" 		=> $cid,
																				"question_id" 		=> $question_id,
																				"answer_value" 		=> $value['value'],
																				"salultation_value" => $salutation_name,
																				"input_file_required" => $img_origional_name,
																				"latitude" 			=> $img_latitude,
																				"longitude" 		=> $img_longitude);
																		
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
															$cnt = $queryResponse->num_rows();														
															if($cnt > 0){$flag = 1;}else{$flag = 0;}															
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
															}
														//}	
													}
												}

												if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($option_type == "textbox" || $option_type == "textarea"))
												{												
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";	
													//if($answer['value'])
													//{
														$insertArr = array("response_id" => $cid,
																			"question_id" => $question_id,
																			"answer_value" => $answer['value'],
																			"salultation_value" => $salutation_name);
														
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
														$cnt = $queryResponse->num_rows();													
														if($cnt > 0){$flag = 1;}else{$flag = 0;}														
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
														}
													//}													
												}	

												// New Case
												if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($option_type == "textbox" || $option_type == "textarea"))
												{
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													$image_name 	 = isset($answer['image_url'])? $answer['image_url']:"";
													$img_latitude 	 = isset($latitude)? $latitude:"";		
													$img_longitude 	 = isset($longitude)? $longitude:"";
													$img_origional_name = '';										
													if($image_name!="") 
													{
														$random_name = $survey_id."_".$question_id."_".time()."_".rand();
														$img_origional_name = $this->generateImage($random_name, $image_name);
													}
													
													//if($answer['value']!="")
													//{
														$answer_value = isset($answer['value'])? $answer['value']:"";
														$insertArr = array( "response_id" 		=> $cid,
																			"question_id" 		=> $question_id,
																			"answer_value" 		=> $answer_value,
																			"salultation_value" => $salutation_name,
																			"file_name" 		=> $img_origional_name,
																			"latitude" 			=> $img_latitude,
																			"longitude" 		=> $img_longitude);
															
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}													
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
														
														}
													//}		
												}
												
												// For Checkbox|Radio|Select Box|File If File Is Required Yes 
												if($input_file_required == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($option_type == "radio" || $option_type == "checkbox"  || $option_type == "single_select" || $option_type == "file"))
												{	
													$imgUrl = isset($answer['image_url'])? $answer['image_url']:"";
													$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
													$img_latitude 	= isset($latitude)? $latitude:"";		
													$img_longitude 	= isset($longitude)? $longitude:"";
													$img_multiple_name = '';
													if($imgUrl!="") 
													{
														$random_name = $survey_id."_".$question_id."_".time()."_".rand();
														$img_multiple_name = $this->generateImage($random_name, $imgUrl);
													}
													
													if($option_type == 'file') 
													{	
														$insertArr = array("response_id" => $cid,
																			"question_id" => $question_id,
																			"answer_value" => $img_multiple_name,
																			"latitude" => $img_latitude,
																			"longitude" => $img_longitude);
														//Table Name : survey_response_headers	
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
														$cnt = $queryResponse->num_rows();												
														if($cnt > 0){$flag = 1;}else{$flag = 0;}												
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
														}
													}
													else 
													{
														$chkArrpush = array();													
														foreach($answer as $key => $value) 
														{
															if($key ==  "image_url")
															{
																
															}
															else 
															{
																//if($value!="")
																//{
																	$img_latitude 	= isset($latitude)? $latitude:"";		
																	$img_longitude 	= isset($longitude)? $longitude:"";
																	$insertArr = array("response_id" => $cid,
																				"question_id" => $question_id,
																				"key_name" => $key,
																				"answer_value" => $value,
																				"file_name" => $img_multiple_name,
																				"salultation_value" => $salutation_name,
																				"latitude" => $img_latitude,
																				"longitude" => $img_longitude);
																	
																	if($option_type == "checkbox" || $option_type == "radio" ||  $option_type == 'single_select') 
																	{
																		
																		array_push($chkArrpush, $key);
																	}	
																	
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name =  '".$key."' AND is_deleted = '0'");
																	
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																		
																	}
																//}
															}
														}
														
														// Delete Previous Answers regarding Checkbox and Selectbox
														if($updateFlag)
														{
															if(count($chkArrpush) > 0) 
															{
																$updatedAt = date('Y-m-d H:i:s');
																$this->db->where_not_in('key_name', $chkArrpush);
																$updateArr = array("is_deleted" => '1');
																$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
																//echo $this->db->last_query();
															}													
														}
													}									
												}
												
												// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute
												if($input_file_required == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($option_type == "radio" || $option_type == "checkbox"  || $option_type == "single_select" || $option_type == "file"))
												{
													
													if($option_type == 'file') 
													{
														$image_name = isset($answer['image_url'])? $answer['image_url']:"";						
														$img_latitude 	= isset($latitude)? $latitude:"";		
														$img_longitude 	= isset($longitude)? $longitude:"";		
														$img_origional_name = '';										
														if($image_name!="") 
														{
															$random_name = $survey_id."_".$question_id."_".time()."_".rand();
															$img_origional_name = $this->generateImage($random_name, $image_name);
														}
														
														$insertArr = array("response_id" => $cid,
																			"question_id" => $question_id,
																			"answer_value" => $img_origional_name,
																			"latitude" => $img_latitude,
																			"longitude" => $img_longitude);
														//Table Name : survey_response_headers
														$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND is_deleted = '0'");
														$cnt = $queryResponse->num_rows();
														
														if($cnt > 0){$flag = 1;}else{$flag = 0;}
														
														if($flag == 0)
														{
															$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
														}	
														else 
														{
															$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id));
														}
														
													}
													else 
													{
														$chkArrpush = array();
														$img_latitude 	= isset($latitude)? $latitude:"";		
														$img_longitude 	= isset($longitude)? $longitude:"";
														foreach($answer as $key => $value) 
														{	

															$salutation_name = isset($answer['salutation'])? $answer['salutation']:"";
															
															if($key == "image_url") 
															{
																
															}
															else 
															{
																
																//if($value!="")
																//{
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $question_id,
																					"key_name" => $key,
																					"answer_value" => $value,
																					"salultation_value" => $salutation_name,
																					"latitude" => $img_latitude,
																					"longitude" => $img_longitude);
																	//Table Name : survey_response_headers										
																	
																	if($option_type == "checkbox" || $option_type == "radio" ||  $option_type == 'single_select') 
																	{																	
																		array_push($chkArrpush, $key);
																	}	
																	
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $question_id, "key_name" => "'".$key."'"));
																	}
																//}	
																		
															}
															
														}
														
														if($updateFlag)
														{
															if(count($chkArrpush) > 0) 
															{
																$updatedAt = date('Y-m-d H:i:s');
																$this->db->where_not_in('key_name', $chkArrpush);
																$updateArr = array("is_deleted" => '1');
																$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $question_id));
															}														
														}
													}
													
												}											
												
											}									
											
										}	// Answer Node End
										else 
										{ 
											if($updateFlag)
											{	//echo "++++++>>>>>>>>>>>++++++++++++";	
												$this->removeAnswers($question_id, $parentQid = 0, $response_id);
											}										
										}

										//echo "<pre>";print_r($dependent_question);
										if(is_array(@$dependent_question))
										{	
											if(count(@$dependent_question) > 0) 
											{
												foreach(@$dependent_question as $opt => $optionDependent) 
												{	
													$selectedOptionValues 	= $dependent_question[$opt]['option'];$answerNodeValues 	= $dependent_question[$opt]['answer'];												
													$parent_question_qid 	= $question_id;
													$option_response_type 	= $optionDependent['option_dependent_question']['response_type'];
													$option_dependent_file 	= $optionDependent['option_dependent_question']['if_file_required'];
													$option_dependent_input = $optionDependent['option_dependent_question']['input_more_than_1'];
													$option_input_file_required = $optionDependent['option_dependent_question']['upload_file_required'];
													$q_id 					= $optionDependent['option_dependent_question']['question_id'];
													$option_dependent_salutation = $optionDependent['option_dependent_question']['salutation'];
													//$option_dependent_answer = @$optionDependent['option_dependent_question']['answer'];
													$option_dependent_answer = (@$optionDependent['option_dependent_question']['answer'])? @$optionDependent['option_dependent_question']['answer']: array();	
													$section_option_dependent_recursive = @$optionDependent['option_dependent_question']['dependent_question_options'];
									
													$option_latitude  = isset($optionDependent['option_dependent_question']['latitude'])? $optionDependent['option_dependent_question']['latitude']:"";
													$option_longitude = isset($optionDependent['option_dependent_question']['longitude'])? $optionDependent['option_dependent_question']['longitude']:"";
													//echo "ParentQ==".$parent_question_qid."==Child Q==".$q_id;
													//echo "Option<pre>";print_r($section_option_dependent_recursive);
													//if(is_array(@$option_dependent_answer))
													//{
														if(count($option_dependent_answer) > 0)
														{
															if($option_input_file_required == 'Yes' && $option_dependent_file == 'Yes' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{
																
																
																$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																
																$img_old_name = '';										
																if($image_name!="") 
																{
																	$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $image_name);
																}	
																
																foreach($option_dependent_answer['value'] as $key => $value) 
																{		
																	
																	$img_old_name = '';										
																	if($value['upload_file']!="") 
																	{
																		$random_name = $survey_id."_".$q_id."_".time()."_".rand();
																		$img_old_name2 = $this->generateImage($random_name, $value['upload_file']);
																	}
																	
																	//Table Name : survey_response_headers
																	
																	//if($value['value']!="")
																	//{
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $q_id,
																						"answer_value" => $value['value'],
																						"file_name" => $img_old_name,
																						"salultation_value" => $salutation_name,
																						"input_file_required" => $img_old_name2,
																						"parent_question_id" => $parent_question_qid,
																						"latitude" => $option_latitude,
																						"longitude" => $option_longitude);
																				
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
																		}
																	//}													
																												
																}											
																
															} 
															
															if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{	
																
																foreach($option_dependent_answer['value'] as $key => $value) 
																{										
																
																	$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																	$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																	
																	$img_old_name = '';										
																	if($image_name!="") 
																	{
																		$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																		$img_old_name = $this->generateImage($random_name2, $image_name);
																	}	
																		
																	//Table Name : survey_response_headers
																	//if($value!="")
																	//{
																		$insertArr = array("response_id" => $cid,
																						"question_id" => $q_id,
																						"key_name" => $key,
																						"answer_value" => $value,
																						"file_name" => $img_old_name,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_question_qid,
																						"latitude" => $option_latitude,
																						"longitude" => $option_longitude);
																						
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
																		}
																	//}														
																}
															} 	
													
															if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{	
																$image_name = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																
																$img_old_name = '';										
																if($image_name!="") 
																{
																	$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $image_name);
																}	
																
																foreach($option_dependent_answer as $key => $value) 
																{										
																	//Table Name : survey_response_headers
																	$image_name = "";
																	
																	if($key == "image_url") 
																	{
																		
																	}
																	else 
																	{											
																		//if($value!="")
																		//{
																			$insertArr = array("response_id" => $cid,
																				"question_id" => $q_id,
																				"key_name" => $key,
																				"answer_value" => $value,
																				"file_name" => $img_old_name,
																				"salultation_value" => $salutation_name,
																				"parent_question_id" => $parent_question_qid,
																				"latitude" => $option_latitude,
																				"longitude" => $option_longitude);
																			//print_r($insertArr);
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
																			}
																		//}																
																	}
																}
																
															}	
														
															if($option_input_file_required == 'Yes' && $option_dependent_file == 'No' && $option_dependent_input == 'Yes' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{
																foreach($option_dependent_answer['value'] as $key => $value) 
																{										
																	$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																	$img_old_name 	 = '';										
																	if($value['upload_file']!="") 
																	{
																		$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																		$img_old_name = $this->generateImage($random_name2, $value['upload_file']);
																	}											
																	
																	//if($value['value']!="")
																	//{
																		$insertArr = array("response_id" 		=> $cid,
																							"question_id" 		=> $q_id,
																							"answer_value" 		=> $value['value'],
																							"salultation_value" => $salutation_name,
																							"input_file_required" => $img_old_name,
																							"parent_question_id" => $parent_question_qid,
																							"latitude" 			=> $option_latitude,
																							"longitude" 		=> $option_longitude);
																				
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																		$cnt = $queryResponse->num_rows();
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}																	
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
																		}
																	//}														
																}
															}
														
															//done
															if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'No' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{
																
																$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																//if($option_dependent_answer['value']!="")
																//{
																	$insertArr = array("response_id" 		=> $cid,
																						"question_id" 		=> $q_id, 
																						"option_value" 		=> $selectedOptionValues,
																						"answer_value" 		=> $option_dependent_answer['value'],
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_question_qid);													
																		
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND option_value='".$selectedOptionValues."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																		$cnt = $queryResponse->num_rows();
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}																
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "option_value" => "'".$selectedOptionValues."'"));
																		}
																		
																		
																		if(count($option_dependent_recursive) > 0)
																		{	
																			foreach($option_dependent_recursive as $nestedValue) 
																			{	
																				$nestedTextQuestionId = $nestedValue['question_id'];
																				$nestedTextQuestionLabel = $nestedValue['question_label'];
																				$nestedTextQuestionResponseType = $nestedValue['response_type'];
																				$nestedTextQuestionSalutation = $nestedValue['salutation'];
																				$nestedTextQuestionInputMoreThanOne = $nestedValue['input_more_than_1'];
																				$nestedTextQuestionFileRequired = $nestedValue['if_file_required'];
																				$nestedTextQuestionFileUploadR = $nestedValue['upload_file_required'];
																				$nestedTextQuestionLabel = $nestedValue['question_label'];
																				//$nestedTextQuestionAnswer = $nestedValue['answer'];
																				$nestedTextQuestionAnswer = ($nestedValue['answer'])? $nestedValue['answer']:"";	
																				
																				$nested_latitude  = @$nestedValue['latitude'];
																				$nested_longitude = @$nestedValue['longitude'];
																				//echo ">>==".$nestedTextQuestionId.">>>".$q_id;
																				
																				if(count($nestedTextQuestionAnswer) > 0)
																				{
																					if($nestedTextQuestionFileUploadR == 'No' && $nestedTextQuestionFileRequired == 'No' && $nestedTextQuestionInputMoreThanOne == 'No' && ($nestedTextQuestionResponseType == "textbox"  || $nestedTextQuestionResponseType == "textarea"))
																					{
																																					
																						$image_name = isset($nestedTextQuestionId['image_url'])? $nestedTextQuestionId['image_url']:"";
																						foreach($nestedTextQuestionAnswer as $key => $value) 
																						{	
																							$insertArr = array("response_id" 	=> $cid,
																												"question_id" 	=> $nestedTextQuestionId,
																												"key_name" 		=> $key,
																												"answer_value" 	=> $value);
																										
																							$queryResponse 	= $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedTextQuestionId."' AND key_name = '".$key."'  AND is_deleted = '0' AND parent_question_id = '".$q_id."'");
																							$cnt 			= $queryResponse->num_rows();																				
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}																				
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $nestedTextQuestionId, "parent_question_id" => $q_id, "key_name" => "'".$key."'"));
																							}										
																						}	
																					}
																				}
																				else 
																				{
																					if($updateFlag)
																					{
																						$this->removeAnswers($nestedTextQuestionId, $q_id, $response_id);
																					}	
																				}													
																			} // Foreach End
																		} // Count End
																		
																		
																		if(is_array($section_option_dependent_recursive))
																		{													
																			if(count($section_option_dependent_recursive) > 0)
																			{														
																				foreach($section_option_dependent_recursive as $key => $val)
																				{
																					$parent_id 					= $q_id;
																					$child_question_id 			= $val['option_dependent_question']['question_id'];
																					$child_response_type 		= $val['option_dependent_question']['response_type'];
																					$child_input_more_than_1 	= $val['option_dependent_question']['input_more_than_1'];
																					$child_if_file_required 	= $val['option_dependent_question']['if_file_required'];
																					$child_upload_file_required = $val['option_dependent_question']['upload_file_required'];
																					$child_answer_value 		= $val['option_dependent_question']['answer']['value'];
																					if(count($child_answer_value) > 0)
																					{
																						if($child_upload_file_required == 'No' && $child_if_file_required == 'No' && $child_input_more_than_1 == 'No' && ($child_response_type == "textbox" || $child_response_type == "textarea"))
																						{  
																							//if($child_answer_value!="")
																							//{
																								$insertArr = array("response_id" 		=> $cid,
																													"question_id" 		=> $child_question_id,
																													"answer_value" 		=> $child_answer_value,
																													"parent_question_id" => $parent_id);
																									
																								$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$child_question_id."' AND is_deleted = '0' AND parent_question_id='".$parent_id."'");
																								$cnt = $queryResponse->num_rows();
																								
																								if($cnt > 0){$flag = 1;}else{$flag = 0;}
																									
																								if($flag == 0)
																								{
																									$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																								}	
																								else 
																								{
																									$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $child_question_id, "parent_question_id" => $parent_id));
																								}
																							//}																				
																						}
																						
																						if($child_upload_file_required == 'No' && $child_if_file_required == 'No' && $child_input_more_than_1 == 'Yes' && ($child_response_type == "textbox" || $child_response_type == "textarea"))
																						{ 
																							if(is_array($val['option_dependent_question']['answer']))
																							{
																								foreach($val['option_dependent_question']['answer'] as $key => $val)
																								{
																									//if($val!="")
																									//{
																										$insertArr = array("response_id" 		=> $cid,
																															"question_id" 		=> $child_question_id,
																															"key_name" 			=> $key,
																															"answer_value" 		=> $val,
																															"parent_question_id" => $parent_id);
																									
																										$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$child_question_id."' AND is_deleted = '0' AND parent_question_id='".$parent_id."' AND key_name = '".$key."'");
																										$cnt = $queryResponse->num_rows();
																										if($cnt > 0){$flag = 1;}else{$flag = 0;}
																										if($flag == 0)
																										{
																											$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																										}	
																										else 
																										{
																											$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $child_question_id, "parent_question_id" => $parent_id, "key_name" => "'".$key."'"));
																										}
																									//}		
																								}
																							}
																						}
																					}
																					else 
																					{
																						if($updateFlag)
																						{
																							$this->removeAnswers($child_question_id, $parent_id, $response_id);
																						}	
																					}
																				}
																			}
																		}
																//}	
																	
															}	
															
															//done-4964
															if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'No' && ($option_response_type == "textbox" || $option_response_type == "textarea"))
															{
																$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
															
																$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																$img_old_name = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $imgUrl);
																}
															
																$answer_value = isset($option_dependent_answer['value'])? $option_dependent_answer['value']:"";
																//if($option_dependent_answer['value']!="")
																//{
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $q_id,
																					"answer_value" => $answer_value,
																					"file_name"	=> $img_old_name,
																					"salultation_value" => $salutation_name,
																					"parent_question_id" => $parent_question_qid,
																					"latitude" => $option_latitude,
																					"longitude" => $option_longitude);
																	
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																	$cnt = $queryResponse->num_rows();															
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}																
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
																	}	
																//}
																	
																if(count($option_dependent_recursive) > 0)
																{	
																	foreach($option_dependent_recursive as $nestedValue) 
																	{
																		$nestedTextQuestionId = $nestedValue['question_id'];
																		$nestedTextQuestionLabel = $nestedValue['question_label'];
																		$nestedTextQuestionResponseType = $nestedValue['response_type'];
																		$nestedTextQuestionSalutation = $nestedValue['salutation'];
																		$nestedTextQuestionInputMoreThanOne = $nestedValue['input_more_than_1'];
																		$nestedTextQuestionFileRequired = $nestedValue['if_file_required'];
																		$nestedTextQuestionFileUploadR = $nestedValue['upload_file_required'];
																		$nestedTextQuestionLabel = $nestedValue['question_label'];
																		$nestedTextQuestionAnswer = $nestedValue['answer'];
																		//$nestedTextQuestionAnswer = isset($nestedValue['answer'])? $nestedValue['answer']:"";
																		$nestedLogitude = @$nestedValue['longitude'];
																		$nestedLatitude = @$nestedValue['latitude'];
																		
																		if(count($nestedTextQuestionAnswer) > 0)
																		{
																			if($nestedTextQuestionFileUploadR == 'No' && $nestedTextQuestionFileRequired == 'No' && $nestedTextQuestionInputMoreThanOne == 'No' && ($nestedTextQuestionResponseType == "textbox"  || $nestedTextQuestionResponseType == "textarea"))
																			{															
																				$image_name = isset($nestedTextQuestionId['image_url'])? $nestedTextQuestionId['image_url']:"";
																				foreach($nestedTextQuestionAnswer as $key => $value) 
																				{								
																					//Table Name : survey_response_headers
																					//if($value!="")
																					//{
																						$insertArr = array("response_id" 	=> $cid,
																										"question_id" 		=> $nestedTextQuestionId,
																										"key_name" 			=> $key,
																										"answer_value" 		=> $value,
																										"parent_question_id" => $q_id);
																									
																						$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedTextQuestionId."' AND key_name = '".$key."'  AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																						$cnt = $queryResponse->num_rows();																				
																						if($cnt > 0){$flag = 1;}else{$flag = 0;}																				
																						if($flag == 0)
																						{
																							$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																						}	
																						else 
																						{
																							$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $nestedTextQuestionId, "parent_question_id" => $q_id, "key_name" => "'".$key."'"));
																						}	
																					//}										
																				}	
																			}
																		}
																		else 
																		{
																			if($updateFlag)
																			{
																				$this->removeAnswers($nestedTextQuestionId, $q_id, $response_id);
																			}
																		}																	
																	}
																}	
															}

															// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute											
															if($option_input_file_required == 'No' && $option_dependent_file == 'Yes' && $option_dependent_input == 'No' && ($option_response_type == "radio" || $option_response_type == "checkbox"  || $option_response_type == "single_select" || $option_response_type == "file"))
															{
																$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																$img_old_name = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $imgUrl);
																}
															
																if($option_response_type == "file") 
																{
																	$depFile = $option_dependent_answer['value'];
																	$img_origional_name = '';										
																	if($depFile!="") 
																	{
																		$random_name = $survey_id."_".$q_id."_".time()."_".rand();
																		$img_origional_name = $this->generateImage($random_name, $depFile);
																	}
																	
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $q_id,
																						"answer_value" => $img_origional_name,
																						"file_name" => $img_old_name,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_question_qid,
																						"latitude" => $option_latitude,
																						"longitude" => $option_longitude);
																
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																	
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "key_name" => "'".$names."'", "parent_question_id" => $parent_question_qid));
																	}
																}
																else 
																{
																	
																	foreach($option_dependent_answer as $key => $value) 
																	{
																		$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																		
																		$lat = isset($option_dependent_answer['latitude'])? $option_dependent_answer['latitude']:"";
																		$long = isset($option_dependent_answer['longitude'])? $option_dependent_answer['longitude']:"";
																		
																		if($key == "image_url") 
																		{
																			
																		}
																		else 
																		{
																			
																			$i = 0;
																			$chkArrpush = array();
																			foreach($value as $names => $namValue) 
																			{
																				
																				//if($namValue!="")
																				//{
																					$insertArr = array("response_id" => $cid,
																									"question_id" => $q_id,
																									"key_name" => $names,
																									"answer_value" => $namValue,
																									"file_name" => $img_old_name,
																									"salultation_value" => $salutation_name,
																									"parent_question_id" => $parent_question_qid,
																									"latitude" => $lat,
																									"longitude" => $long);
																			
																						if($option_response_type == "checkbox" || $option_response_type == "radio" ||  $option_response_type == "single_select") 
																						{
																							array_push($chkArrpush, $names);
																						}
																						
																						
																						$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id='".$parent_question_qid."'");
																						
																						$cnt = $queryResponse->num_rows();
																						
																						if($cnt > 0){$flag = 1;}else{$flag = 0;}
																							
																						if($flag == 0)
																						{
																							$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																						}	
																						else 
																						{
																							$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$names."'"));
																						}	
																				//}
																																	
																			}
																			
																			// Delete Previous Answers regarding Checkbox and Selectbox
																			if($updateFlag)
																			{
																				if(count($chkArrpush) > 0) 
																				{
																							$updatedAt = date('Y-m-d H:i:s');
																							$this->db->where_not_in('key_name', $chkArrpush);
																							$updateArr = array("is_deleted" => '1');
																							$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));
																				}														
																			}															
																				
																		}
																	}
																}	
															}
															
															// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute												
															if($option_input_file_required == 'No' && $option_dependent_file == 'No' && $option_dependent_input == 'No' && ($option_response_type == "radio" || $option_response_type == "checkbox"  || $option_response_type == "single_select" || $option_response_type == "file"))
															{													
																$imgUrl = isset($option_dependent_answer['image_url'])? $option_dependent_answer['image_url']:"";
																$img_old_name = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$q_id."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $imgUrl);
																}
																
																if($option_response_type == "file") 
																{
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $q_id,
																						"key_name" => $key,
																						"answer_value" => $img_old_name,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $parent_question_qid,
																						"latitude" => $option_latitude,
																						"longitude" => $option_longitude);
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND parent_question_id='".$parent_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
																	}					
																}
																else 
																{
																	
																	$chkArrpush = array();
																	foreach($option_dependent_answer as $key => $value) 
																	{	
																		
																		$salutation_name = isset($option_dependent_answer['salutation'])? $option_dependent_answer['salutation']:"";
																		
																		$lat_titude = isset($option_dependent_answer['latitude'])? $option_dependent_answer['latitude']:"";
																		$long_itude = isset($option_dependent_answer['longitude'])? $option_dependent_answer['longitude']:"";
																		if($key == "image_url") 
																		{
																			
																		}
																		else 
																		{
																			
																			//if($value!="")
																			//{
																				$insertArr = array("response_id" => $cid,
																								"question_id" => $q_id,
																								"key_name" => $key,
																								"answer_value" => $value,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $parent_question_qid,
																								"latitude" => $lat_titude,
																								"longitude" => $long_itude);
																				//Table Name : survey_response_headers
																				//print_r($insertArr);
																				if($option_response_type == "checkbox" || $option_response_type == "radio" ||  $option_response_type == "single_select") 
																				{
																					
																					array_push($chkArrpush, $key);
																				}	
																					
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$q_id."' AND parent_question_id='".$parent_question_qid."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$parent_question_qid."'");
																					$cnt = $queryResponse->num_rows();
																					
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid, "key_name" => "'".$key."'"));
																				}
																			//}
																				
																		}
																	}
																	
																	// Delete Previous Answers regarding Checkbox and Selectbox
																	if($updateFlag)
																	{	//echo "===".print_r($chkArrpush);
																		if(count($chkArrpush) > 0) 
																		{
																			$updatedAt = date('Y-m-d H:i:s');
																			$this->db->where_not_in('key_name', $chkArrpush);
																			$updateArr = array("is_deleted" => '1');
																			$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id, "parent_question_id" => $parent_question_qid));	
																			//echo $this->db->last_query();
																		}														
																	}
																	//die();	
																}
																
																if(is_array($section_option_dependent_recursive))
																{	
																		
																		
																	if(count($section_option_dependent_recursive) > 0)
																	{ 
																		//print_r($section_option_dependent_recursive);
																		foreach($section_option_dependent_recursive as $optionDependentQuestionlist) 
																		{
																			
																			$nestedDependentQuestionID 			= $optionDependentQuestionlist['question_id'];
																			$nestedDependentQuesResponseType 	= $optionDependentQuestionlist['response_type'];
																			$nestedDependentQues_upload_file 	= $optionDependentQuestionlist['upload_file_required'];
																			$nestedDependentQues_if_file_required 	= $optionDependentQuestionlist['if_file_required'];
																			$nestedDependentQues_inputMoreThanOne 	= $optionDependentQuestionlist['input_more_than_1'];
																			$nestedDependentQues_question_id 	= $optionDependentQuestionlist['parent_question_id'];
																			$nestedDependentQues_salutation 	= $optionDependentQuestionlist['salutation'];
																			//$nestedDependentQues_answer = $optionDependentQuestionlist['answer'];																
																			$nestedDependentQues_answer = ($optionDependentQuestionlist['option_dependent_question']['answer'])? $optionDependentQuestionlist['option_dependent_question']['answer']:array();
																			
																			$nestedDependentOptionDependent = $optionDependentQuestionlist['option_dependent_question'];
																			$nestedDependentQuesLatitude 		= isset($optionDependentQuestionlist['latitude'])? $optionDependentQuestionlist['latitude']:"";;
																			$nestedDependentQuesLongitude 		= isset($optionDependentQuestionlist['longitude'])? $optionDependentQuestionlist['longitude']:"";
																			
																			$mainQuestionId = $optionDependentQuestionlist['option_dependent_question']['question_id'];
																			//echo "++".$mainQuestionId;print_r($nestedDependentQues_answer);
																			if(count($nestedDependentQues_answer) > 0)
																			{
																				if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "textbox" || $nestedDependentQuesResponseType == "textarea"))
																				{  
																					$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																					
																					//if($nestedDependentQues_answer['value']!="")
																					//{
																						$insertArr = array("response_id" => $cid,
																										"question_id" => $nestedDependentQuestionID,
																										"answer_value" => $nestedDependentQues_answer['value'],
																										"salultation_value" => $salutation_name,
																										"parent_question_id" => $q_id);
																							
																						$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																						$cnt = $queryResponse->num_rows();
																						
																						if($cnt > 0){$flag = 1;}else{$flag = 0;}
																							
																						if($flag == 0)
																						{
																							$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																						}	
																						else 
																						{
																							$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																						}
																					//}
																					
																				}
																				
																				if($nestedDependentQues_upload_file == 'No' && $nestedDependentQues_if_file_required == 'No' && $nestedDependentQues_inputMoreThanOne == 'No' && ($nestedDependentQuesResponseType == "radio" || $nestedDependentQuesResponseType == "checkbox"  || $nestedDependentQuesResponseType == "single_select" || $nestedDependentQuesResponseType == "file"))
																				{	//echo "===".$nestedDependentQuestionID;																				
																					$salutation_name = isset($nestedDependentQues_salutation['salutation'])? $nestedDependentQues_salutation['salutation']:"";
																					if($nestedDependentQuesResponseType == "file") 
																					{
																						$img_old_name = '';										
																						if($nestedDependentQues_answer['image_url']!="") 
																						{
																							$random_name2 = $survey_id."_".$nestedDependentQuestionID."_".$q_id."_".time()."_".rand();
																							$img_old_name = $this->generateImage($random_name2, $nestedDependentQues_answer['image_url']);
																							$insertArr = array("response_id" => $cid,
																											"question_id" => $nestedDependentQuestionID,
																											"answer_value" => $img_old_name,
																											"parent_question_id" => $q_id,
																											"latitude" => $nestedDependentQuesLatitude,
																											"longitude" => $nestedDependentQuesLongitude);
																							//echo "==Parent ID===".$q_id."==QuestionID==".$nestedDependentQuestionID.">>>>>IMG==".$img_old_name;		
																							$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																							$cnt = $queryResponse->num_rows();
																							//echo $nestedDependentQuestionID.">>>>".$this->db->last_query();
																							
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}
																								
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																							}
																						}
																					}
																					else 
																					{
																						if(count($nestedDependentQues_answer)> 0)
																						{
																							$chkArrpush = array();
																							
																							foreach($nestedDependentQues_answer as $kCh => $value)
																							{
																								
																								//if($value!="")
																								//{
																									$insertArr = array("response_id" => $cid,
																													"question_id" => $nestedDependentQuestionID,
																													"key_name" => $kCh,
																													"answer_value" => $value,
																													"salultation_value" => $salutation_name,
																													"parent_question_id" => $q_id,
																													"latitude" => $nestedDependentQuesLatitude,
																													"longitude" => $nestedDependentQuesLongitude);
																								//echo "<pre>";print_r($insertArr);		
																									if($nestedDependentQuesResponseType == "checkbox" || $nestedDependentQuesResponseType == "radio" ||  $nestedDependentQuesResponseType == "single_select") 
																									{
																										
																										array_push($chkArrpush, $kCh);
																									}	
																								
																									$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$nestedDependentQuestionID."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name = '".$kCh."'");
																									$cnt = $queryResponse->num_rows();
																									
																									if($cnt > 0){$flag = 1;}else{$flag = 0;}
																										
																									if($flag == 0)
																									{
																										$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																									}	
																									else 
																									{
																										$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id, "key_name" => "'".$kCh."'"));
																									}
																								//}
																								
																							}
																							
																							// Delete Previous Answers regarding Checkbox and Selectbox
																							if($updateFlag)
																							{
																								if(count($chkArrpush) > 0) 
																								{
																									$updatedAt = date('Y-m-d H:i:s');
																									$this->db->where_not_in('key_name', $chkArrpush);
																									$updateArr = array("is_deleted" => '1');
																									$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $nestedDependentQuestionID, "parent_question_id" => $q_id));
																								}																			
																							}
																						}
																					}
																															
																				}
																				
																				if(is_array($nestedDependentOptionDependent))
																				{  
																					if(count($nestedDependentOptionDependent) > 0)
																					{
																						$p_qid = $nestedDependentOptionDependent['question_id'];
																						$p_response_type = $nestedDependentOptionDependent['response_type'];
																						$p_file_required = $nestedDependentOptionDependent['if_file_required'];
																						$p_input_more = $nestedDependentOptionDependent['input_more_than_1'];
																						$p_upload_file_required = $nestedDependentOptionDependent['upload_file_required'];
																						$p_salutation = $nestedDependentOptionDependent['salutation'];
																						//$p_answer = $nestedDependentOptionDependent['answer'];
																						$p_answer = ($nestedDependentOptionDependent['answer'])? $nestedDependentOptionDependent['answer']:"";
																						$p_lat = isset($nestedDependentOptionDependent['latitude'])? $nestedDependentOptionDependent['latitude']:"";
																						$p_long = isset($nestedDependentOptionDependent['longitude'])? $nestedDependentOptionDependent['longitude']:"";	
																						
																						if(count($p_answer) > 0)
																						{
																							if($p_upload_file_required == 'No' && $p_file_required == 'No' && $p_input_more == 'No' && ($p_response_type == "radio" || $p_response_type == "checkbox"  || $p_response_type == "single_select" || $p_response_type == "file"))
																							{ 
																								$salutation_name = isset($p_salutation['salutation'])? $p_salutation['salutation']:"";
																								
																								if($p_response_type == "file") 
																								{
																									$img_old_name = '';										
																									if($p_answer['image_url']!="") 
																									{
																										
																										$random_name2 = $survey_id."_".$p_qid."_".$q_id."_".time()."_".rand();
																										$img_old_name = $this->generateImage($random_name2, $p_answer['image_url']);
																										$insertArr = array("response_id" => $cid,
																														"question_id" => $p_qid,
																														"answer_value" => $img_old_name,
																														"parent_question_id" => $q_id,
																														"latitude" => $p_lat,
																														"longitude" => $p_long);
																										//print_r($insertArr);		
																										$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																										$cnt = $queryResponse->num_rows();
																										
																										if($cnt > 0){$flag = 1;}else{$flag = 0;}
																											
																										if($flag == 0)
																										{
																											$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																										}	
																										else 
																										{
																											$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																										}
																									}
																								}
																								else 
																								{
																									if(count($p_answer)> 0)
																									{	$chkArrpush = array();
																										foreach($p_answer as $kCh => $value)
																										{
																											//if($value!="")
																											//{
																												$insertArr = array("response_id" => $cid,
																																	"question_id" => $p_qid,
																																	"key_name" => $kCh,
																																	"answer_value" => $value,
																																	"salultation_value" => $salutation_name,
																																	"parent_question_id" => $q_id,
																																	"latitude" => $p_lat,
																																	"longitude" => $p_long);
																											
																												if($p_response_type == "checkbox" || $p_response_type == "radio" ||  $p_response_type == "single_select") 
																												{
																													array_push($chkArrpush, $kCh);
																												}																								
																												$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name = '".$kCh."'");
																												//echo $this->db->last_query();
																												$cnt = $queryResponse->num_rows();																										
																												if($cnt > 0){$flag = 1;}else{$flag = 0;}																											
																												if($flag == 0)
																												{
																													$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																												}	
																												else 
																												{
																													$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id, "key_name" => "'".$kCh."'"));
																												}
																											//}
																											
																										}
																										// Delete Previous Answers regarding Checkbox and Selectbox
																										if($updateFlag)
																										{
																											if(count($chkArrpush) > 0) 
																											{
																											$updatedAt = date('Y-m-d H:i:s');
																											$this->db->where_not_in('key_name', $chkArrpush);
																											$updateArr = array("is_deleted" => '1');
																											$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																											//echo $this->db->last_query();
																											}														
																										}
																									}
																								}
																																		
																							}
																						
																							if($p_upload_file_required == 'No' && $p_file_required == 'No' && $p_input_more == 'No' && ($p_response_type == "textbox" || $p_response_type == "textarea"))
																							{  
																								$salutation_name = isset($p_salutation['salutation'])? $p_salutation['salutation']:"";
																								
																								foreach($p_answer as $k => $val) 
																								{
																									
																									//if($val!="")
																									//{
																										$insertArr = array("response_id" => $cid,
																													"question_id" => $p_qid,
																													"key_name" => $k,
																													"answer_value" => $val,
																													"salultation_value" => $salutation_name,
																													"parent_question_id" => $q_id);
																									
																										if($k == "") 
																										{
																											$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																										}
																										else 
																										{
																											$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name='".$k."'");
																										}																					
																										
																										$cnt = $queryResponse->num_rows();
																										
																										if($cnt > 0){$flag = 1;}else{$flag = 0;}
																											
																										if($flag == 0)
																										{
																											$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																										}	
																										else 
																										{
																											if($k != "") 
																											{
																												$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id, "key_name" => "'".$k."'"));
																											}
																											else
																											{
																												$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																											}
																											
																										}
																									//}
																									
																								}
																								
																							}
																							
																							if($p_upload_file_required == 'No' && $p_file_required == 'No' && $p_input_more == 'Yes' && ($p_response_type == "textbox" || $p_response_type == "textarea"))
																							{  
																								$salutation_name = isset($p_salutation['salutation'])? $p_salutation['salutation']:"";
																								
																								foreach($p_answer as $k => $val) 
																								{
																									
																									//if($val!="")
																									//{
																										$insertArr = array("response_id" => $cid,
																													"question_id" => $p_qid,
																													"key_name" => $k,
																													"answer_value" => $val,
																													"salultation_value" => $salutation_name,
																													"parent_question_id" => $q_id);
																									
																										if($k == "") 
																										{
																											$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."'");
																										}
																										else 
																										{
																											$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$p_qid."' AND is_deleted = '0' AND parent_question_id='".$q_id."' AND key_name='".$k."'");
																										}																					
																										
																										$cnt = $queryResponse->num_rows();
																										
																										if($cnt > 0){$flag = 1;}else{$flag = 0;}
																											
																										if($flag == 0)
																										{
																											$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																										}	
																										else 
																										{
																											if($k != "") 
																											{
																												$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id, "key_name" => "'".$k."'"));
																											}
																											else
																											{
																												$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $p_qid, "parent_question_id" => $q_id));
																											}
																											
																										}
																									//}
																									
																								}
																								
																							}
																						}
																						else 
																						{
																							if($updateFlag)
																							{
																								$this->removeAnswers($p_qid, $q_id, $response_id);
																							}
																						}
																					}
																				}
																			}
																			else 
																			{
																				if($updateFlag)
																				{   
																					$this->removeAnswers($mainQuestionId, $q_id, $response_id);
																				}
																			}																	
																		} // End Foreach
																	}
																	else 
																	{	//echo "==".$q_id."===".$parent_question_qid;
																		if($updateFlag)
																		{ //echo 	"==".$q_id."===".$parent_question_qid;			
																			$this->removeAnswers($q_id, $parent_question_qid, $response_id);
																		}
																	}
																}
															}											
														}
														else 
														{	
															if($updateFlag)
															{
	
																$this->removeAnswers($q_id, $parent_question_qid, $response_id);
															}	
														} // End Answer node not exist
													/*}
													else 
													{														
														$this->removeAnswers($q_id, $parent_question_qid, $response_id);
													}*/													
													
												}	// End Foreach								
											}
										} // End Dependent Question
										
										
										// Sub Question Node
										if(is_array($sub_question))
										{		
											$sub_question_qid 		= $sub_question['question_id'];
											$sub_parent_question_id = $sub_question['parent_question_id'];
											if(count($sub_question) > 0) 
											{	
												//foreach($sub_question as $subDependent) 
												//{
													/*$sub_question_qid = $subDependent['question_id'];
													$sub_option_response_type = $subDependent['response_type'];
													$sub_option_input_file = $subDependent['upload_file_required'];
													$sub_option_dependent_file = $subDependent['if_file_required'];
													$sub_option_dependent_input = $subDependent['input_more_than_1'];
													$sub_parent_question_id = $subDependent['parent_question_id'];
													$sub_option_dependent_salutation = $subDependent['salutation'];
													$sub_option_dependent_answer = $subDependent['answer'];	*/	


													$sub_question_qid 				= $sub_question['question_id'];
													$sub_option_response_type 		= $sub_question['response_type'];
													$sub_option_input_file 			= $sub_question['upload_file_required'];
													$sub_option_dependent_file 		= $sub_question['if_file_required'];
													$sub_option_dependent_input 	= $sub_question['input_more_than_1'];
													$sub_parent_question_id 		= $sub_question['parent_question_id'];
													$sub_option_dependent_salutation = $sub_question['salutation'];
													$sub_option_dependent_answer 	= $sub_question['answer'];	
													//$sub_option_dependent_answer = isset($sub_question['answer'])? $sub_question['answer']:"";
													
													$sub_latitude = isset($sub_question['latitude'])? $sub_question['latitude']:"";
													$sub_longitude = isset($sub_question['longitude'])? $sub_question['longitude']:"";		
													
													if(count($sub_option_dependent_answer) > 0)
													{
														if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'No' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "textbox" || $sub_option_response_type == "textarea"))
														{ 
															$salutation_name = isset($sub_option_dependent_salutation['salutation'])? $sub_option_dependent_salutation['salutation']:"";
															$insertArr = array("response_id" 		=> $cid,
																				"question_id" 		=> $sub_question_qid,
																				"answer_value" 		=> $sub_option_dependent_answer['value'],
																				"salultation_value" => $salutation_name);
																				
															$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND  is_deleted = '0'");
															$cnt = $queryResponse->num_rows();
															if($cnt > 0){$flag = 1;}else{$flag = 0;}															
															if($flag == 0)
															{
																$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
															}	
															else 
															{
																$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
															}	
														}	

														// For Checkbox | Radio | Select Box | File  If File Is Required Yes following condition execute
														if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'Yes' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "radio" || $sub_option_response_type == "checkbox"  || $sub_option_response_type == "single_select" || $sub_option_response_type == "file"))
														{
															$imgUrl = isset($sub_option_dependent_answer['image_url'])? $sub_option_dependent_answer['image_url']:"";
															
															$img_old_name1 = '';										
															if($imgUrl!="") 
															{
																$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
															}
															
															$salutation_name = isset($sub_option_dependent_answer['salutation'])? $sub_option_dependent_answer['salutation']:"";
															
															if($sub_option_response_type == "file") 
															{
																$img_old_name = '';										
																if($sub_option_dependent_salutation['value']!="") 
																{
																	$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																	$img_old_name = $this->generateImage($random_name2, $sub_option_dependent_salutation['value']);
																}
																
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_question_qid,
																					"answer_value" => $img_old_name,
																					"file_name" => $img_old_name1,
																					"salultation_value" => $salutation_name,
																					"latitude"=>$sub_latitude,
																					"longitude"=>$sub_longitude);
															
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'");
																
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id));
																}
																
															}
															else 
															{
																foreach($sub_option_dependent_answer as $key => $value) 
																{
																														
																	if($key == "image_url") 
																	{
																		
																	}
																	else 
																	{
																		
																		$i = 0;
																		
																		$chkArrpush = array();
																		foreach($value as $names => $namValue) 
																		{
																			
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $sub_question_qid,
																								"key_name" => $names,
																								"answer_value" => $namValue,
																								"file_name" => $img_old_name1,
																								"salultation_value" => $salutation_name,
																								"latitude"=>$sub_latitude,
																								"longitude"=>$sub_longitude);
																								
																								
																			if($sub_option_response_type == "checkbox" || $sub_option_response_type == "radio" ||  $sub_option_response_type == "single_select") 
																			{
																				array_push($chkArrpush, $names);
																			}															
																		
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'");
																			
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $q_id, "key_name" => "'".$names."'"));
																			}														
																		}
																		// Delete Previous Answers regarding Checkbox and Selectbox
																		if($updateFlag)
																		{
																			if(count($chkArrpush) > 0) 
																			{
																				$updatedAt = date('Y-m-d H:i:s');
																				$this->db->where_not_in('key_name', $chkArrpush);
																				$updateArr = array("is_deleted" => '1');
																				$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $q_id));
																			}														
																		}																
																	}
																}
															}
															
														}
														
														// For Checkbox | Radio | Select Box | File  If File Is Required No following condition execute										
														if($sub_option_input_file == 'No' && $sub_option_dependent_file == 'No' && $sub_option_dependent_input == 'No' && ($sub_option_response_type == "radio" || $sub_option_response_type == "checkbox"  || $sub_option_response_type == "single_select" || $sub_option_response_type == "file"))
														{
															$salutation_name = isset($sub_option_dependent_answer['salutation'])? $sub_option_dependent_answer['salutation']:"";
															if($sub_option_response_type == "file") 
															{
																$img_old_name1 = '';										
																if($imgUrl!="") 
																{
																	$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																	$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
																}
																
																$insertArr = array("response_id" 		=> 	$cid,
																					"question_id" 		=> 	$sub_question_qid,
																					"answer_value" 		=> 	$img_old_name1,
																					"salultation_value" => 	$salutation_name,
																					"latitude"			=>	$sub_latitude,
																					"longitude"			=>	$sub_longitude);
																														
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();															
																if($cnt > 0){$flag = 1;}else{$flag = 0;}															
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																}
															} 
															else 
															{
																foreach($sub_option_dependent_answer as $key => $value) 
																{
																	if($key == "image_url") 
																	{
																		
																	}
																	else 
																	{
																		$chkArrpush = array();
																		$insertArr = array("response_id" 		=> 	$cid,
																							"question_id" 		=> 	$sub_question_qid,
																							"key_name" 			=> 	$key,
																							"answer_value" 		=> 	$value,
																							"salultation_value" => 	$salutation_name,
																							"latitude"			=>	$sub_latitude,
																							"longitude"			=>	$sub_longitude);
																			
																		if($sub_option_response_type == "checkbox"  || $sub_option_response_type == "radio" ||  $sub_option_response_type == "single_select") 
																		{
																			array_push($chkArrpush, $key);
																		}		
																			
																		$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");
																		$cnt = $queryResponse->num_rows();																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}																	
																		if($flag == 0)
																		{
																			$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																		}	
																		else 
																		{
																			$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																		}	
																	}												
																}
																
																// Delete Previous Answers regarding Checkbox and Selectbox
																if($updateFlag)
																{
																	if(count($chkArrpush) > 0) 
																	{
																		$updatedAt = date('Y-m-d H:i:s');
																		$this->db->where_not_in('key_name', $chkArrpush);
																		$updateArr = array("is_deleted" => '1');
																		$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																	}														
																}														
															}																								
														}										
												
													}
													else 
													{
														if($updateFlag)
														{
															$this->removeAnswers($sub_question_qid, $sub_parent_question_id, $response_id);
														}	
													} // End Answer node not exist											
												//}
											}
											else 
											{
												if($updateFlag)
												{
													$this->removeAnswers($sub_question_qid, $sub_parent_question_id, $response_id);
												}										
											}
										} // Sub Question End
										
									}	// Section Question List
								}	// Section Question Count Check
									
									// Sub Section List Start 
										//echo "<pre>";print_r($sections['sub_section']);	die();
										if(is_array(@$sections['sub_section']))
										{						
											if(count(@$sections['sub_section']) > 0)
											{						
												foreach(@$sections['sub_section'] as $node => $subSectionNode) 
												{		
													//echo "<pre>";print_r($subSectionNode);	
													$sub_section_id 	= $subSectionNode['sub_section_id'];
													$sub_section_name 	= $subSectionNode['section_name'];
													$sb_section_desc 	= $subSectionNode['section_description'];	
													foreach($subSectionNode['sub_section_question'] as $subKey => $subSectionQuestionire) 
													{
														$sub_question_qid 				= $subSectionQuestionire['question_id'];
														$sub_sec_response_type 			= $subSectionQuestionire['response_type'];
														$sub_upload_file_required 		= $subSectionQuestionire['upload_file_required'];
														$sub_if_file_required			= $subSectionQuestionire['if_file_required'];
														$sub_input_more_than_1 			= $subSectionQuestionire['input_more_than_1'];
														$parent_question_id 			= $subSectionQuestionire['parent_question_id'];
														$SubSecoption_dependent_salutation 	= $subSectionQuestionire['salutation'];
														//$suboption_dependent_answer 		= $subSectionQuestionire['answer'];													
														$suboption_dependent_answer = ($subSectionQuestionire['answer'])? $subSectionQuestionire['answer']:"";
														$dependent_question_node 		= $subSectionQuestionire['dependent_question_options'];
														$sub_global_question 			= @$subSectionQuestionire['global_question'];
														
														$latitude = isset($suboption_dependent_answer['latitude'])? $suboption_dependent_answer['latitude']:"";													
														$longitude = isset($suboption_dependent_answer['longitude'])? $suboption_dependent_answer['longitude']:"";
														
													if(count($suboption_dependent_answer) > 0)
													{													
														if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'Yes' && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
														{
															//echo "==".$sub_question_qid."==".$sub_sec_response_type;
															$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
															
															$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
															
															foreach($suboption_dependent_answer as $ks => $val)
															{
																//if($val!="")
																//{
																	$insertArr = array("response_id" => $cid,
																				"question_id" => $sub_question_qid,
																				"key_name" => $ks,
																				"answer_value" => $val,
																				"salultation_value" => $salutation_name);
																		
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name = '".$ks."' AND is_deleted = '0'");
																	//echo ">>>>".$this->db->last_query();
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$ks."'"));
																	}
																//}
																
															}
																
														}
														
														if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No' && ($sub_sec_response_type == "radio" || $sub_sec_response_type == "checkbox"  || $sub_sec_response_type == "single_select" || $sub_sec_response_type == "file"))
														{
															//echo "==".$sub_question_qid."==".$sub_sec_response_type;
															$imgUrl = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
															
															$img_old_name1 = '';										
															if($imgUrl!="") 
															{
																$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																$img_old_name1 = $this->generateImage($random_name2, $imgUrl);
															}
															
															
															if($sub_sec_response_type == "file") 
															{
																$upload_old_name1 = '';										
																if($suboption_dependent_answer['value']!="") 
																{
																	$random_name = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																	$upload_old_name1 = $this->generateImage($random_name, $suboption_dependent_answer['value']);
																}
																
																if($upload_old_name1!="")
																{
																	$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_question_qid,
																					"answer_value" => $upload_old_name1,
																					"file_name" => $img_old_name1,
																					"latitude"=>$latitude,
																					"longitude"=>$longitude);
																					
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'" );
																	
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																	}
																}
																	
															}
															else 
															{
																foreach($suboption_dependent_answer as $key => $value) 
																{	

																	$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
																	
																	if($key == "image_url") 
																	{
																		
																	}
																	else 
																	{
																		if(is_array($value))
																		{
																			$chkArrpush = array();
																			foreach($value as $names => $namValue) 
																			{
																				//if($namValue['value']!="")
																				//{
																					if($sub_sec_response_type == 'checkbox' || $sub_sec_response_type == 'radio' ||  $sub_sec_response_type == 'single_select') 
																					{
																						if(is_numeric($names))
																						{
																							$nm = "";
																						}
																						else 
																						{
																							$nm = $names;
																						}
																						
																						array_push($chkArrpush, $key);
																						$insertArr = array("response_id" => $cid,
																										"question_id" => $sub_question_qid,
																										"key_name" => $nm,
																										"answer_value" => $namValue['value'],
																										"file_name" => $img_old_name1,
																										"salultation_value" => $salutation_name,
																										"latitude"=>$latitude,
																										"longitude"=>$longitude);
																										
																					}
																					else 
																					{
																						$insertArr = array("response_id" => $cid,
																										"question_id" => $sub_question_qid,
																										"answer_value" => $namValue['value'],
																										"file_name" => $img_old_name1,
																										"salultation_value" => $salutation_name,
																										"latitude"=>$latitude,
																										"longitude"=>$longitude);
																					}
																					
																				
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$names."' AND is_deleted = '0'" );
																					
																					$cnt = $queryResponse->num_rows();
																					
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}
																						
																					if($flag == 0)
																					{
																						$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																					}	
																					else 
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$names."'"));
																					}
																				//}
																																		
																			}
																				
																			if($updateFlag)
																			{
																				if(count($chkArrpush) > 0) 
																				{
																					$updatedAt = date('Y-m-d H:i:s');
																					$this->db->where_not_in('key_name', $chkArrpush);
																					$updateArr = array("is_deleted" => '1');
																					$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																				}														
																			}	
																		}
																		else 
																		{
																			//if($value!="")
																			//{
																				if($sub_sec_response_type == 'checkbox' || $sub_sec_response_type == 'radio'  ||  $sub_sec_response_type == 'single_select') 
																				{
																					
																					$insertArr = array("response_id" => $cid,
																									"question_id" => $sub_question_qid,
																									"key_name" => $key,
																									"answer_value" => $value,
																									"file_name" => $img_old_name1,
																									"salultation_value" => $salutation_name,
																									"latitude"=>$latitude,
																									"longitude"=>$longitude);
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND key_name='".$key."' AND is_deleted = '0'");	
																					
																						
																				}
																				else 
																				{
																					$insertArr = array("response_id" => $cid,
																										"question_id" => $sub_question_qid,
																										"answer_value" => $value,
																										"file_name" => $img_old_name1,
																										"salultation_value" => $salutation_name,
																										"latitude"=>$latitude,
																										"longitude"=>$longitude);
																									
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");				
																				}
																					
																				$cnt = $queryResponse->num_rows();
																				//die();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																				}
																			//}
																			
																		}														
																			
																	}
																	
																}
															}
															
															
															
														}
														
														if($sub_if_file_required == 'No' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No' && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
														{
															
															$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
															
															//if($suboption_dependent_answer['value']!="")
															//{
																$insertArr = array("response_id" => $cid,
																				"question_id" => $sub_question_qid,
																				"answer_value" => $suboption_dependent_answer['value'],
																				"salultation_value" => $salutation_name);
																//print_r($insertArr);		
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																}
															//}
																
														}
														
														// HERE
														if($sub_if_file_required == 'Yes' && $sub_upload_file_required == 'No' && $sub_input_more_than_1 == 'No'  && ($sub_sec_response_type == "textbox" || $sub_sec_response_type == "textarea"))
														{
														
															$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
															
															$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
															
															$img_old_name1 = '';										
															if($image_name!="") 
															{
																$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																$img_old_name1 = $this->generateImage($random_name2, $image_name);
															}
															
															
															$answer_value = isset($suboption_dependent_answer['value'])? $suboption_dependent_answer['value']:"";
															
															//if($suboption_dependent_answer['value']!="")
															//{
																$insertArr = array("response_id" => $cid,
																				"question_id" => $sub_question_qid,
																				"answer_value" => $answer_value,
																				"salultation_value" => $salutation_name,
																				"file_name" => $img_old_name1,
																				"latitude"=>$latitude,
																				"longitude"=>$longitude);
																		
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
																	$cnt = $queryResponse->num_rows();
																	
																	if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																}
															//}
																
														}
														
														if($sub_if_file_required == 'No' && $sub_upload_file_required == 'Yes' && $sub_input_more_than_1 == 'No'  && ($sub_sec_response_type == "radio" || $sub_sec_response_type == "checkbox"  || $sub_sec_response_type == "single_select" || $sub_sec_response_type == "file"))
														{	
													
															if($sub_sec_response_type == "file") 
															{
																$image_name = isset($suboption_dependent_answer['image_url'])? $suboption_dependent_answer['image_url']:"";
																
																$img_old_name1 = '';										
																if($image_name!="") 
																{
																	$random_name2 = $survey_id."_".$sub_question_qid."_".time()."_".rand();
																	$img_old_name1 = $this->generateImage($random_name2, $image_name);
																}
																
																$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
																$insertArr = array("response_id" => $cid,
																					"question_id" => $sub_question_qid,
																					"answer_value" => $img_old_name1,
																					"latitude"=>$latitude,
																					"longitude"=>$longitude);
																		
																$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
																$cnt = $queryResponse->num_rows();
																
																if($cnt > 0){$flag = 1;}else{$flag = 0;}
																	
																if($flag == 0)
																{
																	$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																}	
																else 
																{
																	$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																}
															}
															else 
															{
																
																//if($suboption_dependent_answer['value']!="")
																//{
																	$salutation_name = isset($suboption_dependent_answer['salutation'])? $suboption_dependent_answer['salutation']:"";
																	$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_question_qid,
																						"answer_value" => $suboption_dependent_answer['value'],
																						"salultation_value" => $salutation_name,
																						"latitude"=>$latitude,
																						"longitude"=>$longitude);
																			
																	$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_question_qid."' AND is_deleted = '0'");
																		$cnt = $queryResponse->num_rows();
																		
																		if($cnt > 0){$flag = 1;}else{$flag = 0;}
																		
																	if($flag == 0)
																	{
																		$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																	}	
																	else 
																	{
																		$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_question_qid));
																	}
																//}
																
															}
														
																									
														} 
									
														// Sub Section Global Questions 
														if(is_array($sub_global_question))
														{
															$sub_global_question_id 		= $sub_global_question['question_id'];
															$sub_global_parent_question_id 	= $sub_global_question['parent_question_id'];
															$sub_global_response_type 		= $sub_global_question['response_type'];
															
															$latitude 		= isset($sub_global_question['latitude'])? $sub_global_question['latitude']:"";
															$longitude 		= isset($sub_global_question['longitude'])? $sub_global_question['longitude']:"";														
															
															//if(is_array($sub_global_question['answer']))
															//{										
																if(count($sub_global_question['answer']) > 0) 
																{
																	if($sub_global_question['upload_file_required'] == 'Yes' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
																	{
																		
																		$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																		$image_name = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																
																		$img_old_name1 = '';										
																		if($image_name!="") 
																		{
																			$random_name2 = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																			$img_old_name1 = $this->generateImage($random_name2, $image_name);
																		}
																	
																		foreach($sub_global_question['answer']['value'] as $key => $value) 
																		{			

																			$img_old_name = '';										
																			if($key['upload_file']!="") 
																			{
																				$random_name = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																				$img_old_name = $this->generateImage($random_name, $value);
																			}
																			//Table Name : survey_response_headers
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $sub_global_question_id,
																								"answer_value" => $value['value'],
																								"file_name" => $img_old_name1,
																								"salultation_value" => $salutation_name,
																								"input_file_required" => $img_old_name,
																								"parent_question_id" => $sub_global_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																						
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value = '".$value['value']."'  AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																			$cnt = $queryResponse->num_rows();
																			
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																			
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr, array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																			}													
																														
																		}
																	}
																	
																	if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
																	{	
																	
																		$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																		$image_name = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																		
																		$img_old_name1 = '';										
																		if($image_name!="") 
																		{
																			$random_name2 = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																			$img_old_name1 = $this->generateImage($random_name2, $image_name);
																		}
																		
																		foreach($sub_global_question['answer']['value'] as $key => $value) 
																		{										
																			$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																													
																			//Table Name : survey_response_headers
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $sub_global_question_id,
																								"key_name" => $key,
																								"answer_value" => $value,
																								"file_name" => $img_old_name1,
																								"salultation_value" => $salutation_name,
																								"parent_question_id" => $sub_global_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																								
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id, "key_name" => "'".$key."'"));
																			}	
																		}
																	} 
																	
																	if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
																	{
																		$image_name = isset($sub_global_question['image_url'])? $sub_global_question['image_url']:"";
																			
																		$img_old_name1 = '';										
																		if($image_name!="") 
																		{
																			$random_name2 = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																			$img_old_name1 = $this->generateImage($random_name2, $image_name);
																		}	
																			
																		foreach($sub_global_question['answer'] as $key => $value) 
																		{										
																			//Table Name : survey_response_headers
																			$image_name = "";
																			
																			if($key == "image_url") 
																			{
																				
																			}
																			else 
																			{	
																				$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";												
																				
																				$insertArr = array("response_id" => $cid,
																						"question_id" => $sub_global_question_id,
																						"key_name" => $key,
																						"answer_value" => $value,
																						"file_name" => $img_old_name1,
																						"salultation_value" => $salutation_name,
																						"parent_question_id" => $sub_global_parent_question_id,
																						"latitude" => $latitude,
																						"longitude" => $longitude);
																			
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id, "key_name" => "'".$key."'"));
																				}		
																			}
																		}
																	}
																	
																	if($sub_global_question['upload_file_required'] == 'Yes' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'Yes' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
																	{	
																		foreach($sub_global_question['answer']['value'] as $key => $value) 
																		{										
																			$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																			
																			$upload_file = '';										
																			if($key['upload_file']!="") 
																			{
																				$random_name2 = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																				$upload_file = $this->generateImage($random_name2, $value);
																			}
																														
																			//Table Name : survey_response_headers
																			$insertArr = array("response_id" => $cid,
																								"question_id" => $sub_global_question_id,
																								"answer_value" => $value['value'],
																								"salultation_value" => $salutation_name,
																								"input_file_required" => $upload_file,
																								"parent_question_id" => $sub_global_parent_question_id,
																								"latitude" => $latitude,
																								"longitude" => $longitude);
																						
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value='".$value['value']."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																			}	
																		}
																	}
																	
																	if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "textbox" || $sub_global_response_type == "textarea"))
																	{
																			
																		$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																		$insertArr = array("response_id" => $cid,
																							"question_id" => $sub_global_question_id,
																							"answer_value" => $sub_global_question['answer']['value'],
																							"salultation_value" => $salutation_name,
																							"parent_question_id" => $sub_global_parent_question_id);
																					
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																				$cnt = $queryResponse->num_rows();
																				
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																			}	
																	}
																	
																	if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'Yes' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "radio" || $sub_global_response_type == "checkbox"  || $sub_global_response_type == "single_select" || $sub_global_response_type == "file"))
																	{
																		$upload_file = '';
																		$imgUrl = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																			
																			foreach($sub_global_question['answer'] as $key => $value) 
																			{
																				$salutation_name = isset($sub_global_question['answer']['salutation'])? $sub_global_question['answer']['salutation']:"";
																				
																				if($key == "image_url") 
																				{
																					
																				}
																				else 
																				{
																					
																					
																					if(is_array($value))
																					{
																						$chkArrpush = array();
																						foreach($value as $names => $namValue) 
																						{
																							
																							if($sub_global_question == 'checkbox' || $sub_global_question == 'radio' || $sub_global_question == 'single_select') 
																							{
																								if(is_numeric($names))
																								{
																									$nm = "";
																								}
																								else 
																								{
																									$nm = $names;
																								}
																								$insertArr = array("response_id" => $cid,
																												"question_id" => $sub_global_question_id,
																												"key_name" => $nm,
																												"answer_value" => $namValue['value'],
																												"file_name" => $upload_file,
																												"salultation_value" => $salutation_name,
																												"parent_question_id" => $sub_global_parent_question_id,
																												"latitude" => $latitude,
																												"longitude" => $longitude);
																												
																								if($sub_global_response_type == "checkbox" || $sub_global_response_type == "radio") 
																								{
																									
																									array_push($chkArrpush, $key);
																								}				
																							}
																							else 
																							{
																								$insertArr = array("response_id" => $cid,
																												"question_id" => $sub_global_question_id,
																												"answer_value" => $namValue['value'],
																												"file_name" => $upload_file,
																												"salultation_value" => $salutation_name,
																												"parent_question_id" => $sub_global_parent_question_id,
																												"latitude" => $latitude,
																												"longitude" => $longitude);
																							}
																							
																						
																							$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND key_name='".$names."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'" );
																							
																							$cnt = $queryResponse->num_rows();
																							
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}
																								
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "key_name" => "'".$names."'", "parent_question_id" => $sub_global_parent_question_id));
																							}														
																						}
																						// Delete Previous Answers regarding Checkbox and Selectbox
																						if($updateFlag)
																						{
																							if(count($chkArrpush) > 0) 
																							{						
																								$updatedAt = date('Y-m-d H:i:s');
																								$this->db->where_not_in('key_name', $chkArrpush);
																								$updateArr = array("is_deleted" => '1');
																								$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));	
																							}																					
																						}
																					}
																					else
																					{
																						$insertArr = array("response_id" => $cid,
																											"question_id" => $sub_global_question_id,
																											"answer_value" => $value,
																											"file_name" => $upload_file,
																											"salultation_value" => $salutation_name,
																											"parent_question_id" => $sub_global_parent_question_id,
																											"latitude" => $latitude,
																											"longitude" => $longitude);
																					
																						$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND answer_value='".$value."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'" );
																						
																						$cnt = $queryResponse->num_rows();
																						
																						if($cnt > 0){$flag = 1;}else{$flag = 0;}
																							
																						if($flag == 0)
																						{
																							$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																						}	
																						else 
																						{
																							$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "key_name" => $names, "parent_question_id" => $sub_global_parent_question_id));
																						}		
																					}	
																				}
																			}
																					
																	}
																	
																	if($sub_global_question['upload_file_required'] == 'No' && $sub_global_question['if_file_required'] == 'No' && $sub_global_question['input_more_than_1'] == 'No' && ($sub_global_response_type == "radio" || $sub_global_response_type == "checkbox"  || $sub_global_response_type == "single_select" || $sub_global_response_type == "file"))
																	{
																		$upload_file = '';
																		$imgUrl = isset($sub_global_question['answer']['image_url'])? $sub_global_question['answer']['image_url']:"";
																			
																		$insertArr = array("response_id" => $cid,
																							"question_id" => $sub_global_question_id,
																							"answer_value" => $sub_global_question['answer']['value'],
																							"parent_question_id" => $sub_global_parent_question_id,
																							"latitude" => $latitude,
																							"longitude" => $longitude);
																					
																			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_global_question_id."' AND is_deleted = '0' AND parent_question_id = '".$sub_global_parent_question_id."'");
																			
																			$cnt = $queryResponse->num_rows();
																				
																			if($cnt > 0){$flag = 1;}else{$flag = 0;}
																				
																			if($flag == 0)
																			{
																				$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				
																			}	
																			else 
																			{
																				$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_global_question_id, "parent_question_id" => $sub_global_parent_question_id));
																				
																			}	
																	}
																	
																} 
																else 
																{
																	if($updateFlag)
																	{
																		$this->removeAnswers($sub_global_question_id, $sub_global_parent_question_id, $response_id);
																	}
																}
															//}	
															
														}  // Sub Section Global Section End													
														
														if(is_array($dependent_question_node))
														{									
															if(count($dependent_question_node) > 0)
															{										
																foreach($dependent_question_node as $dptKey => $optionDependentQuestionlist) 
																{											
																	$sub_option_question_qid 				= $optionDependentQuestionlist['option_dependent_question']['question_id'];
																	$sub_sec_option_response_type 			= $optionDependentQuestionlist['option_dependent_question']['response_type'];
																	$upload_option_file_required 			= $optionDependentQuestionlist['option_dependent_question']['upload_file_required'];
																	$subsec_if_file_required 			= $optionDependentQuestionlist['option_dependent_question']['if_file_required'];
																	$subsec_option_inputMoreThanOne 			= $optionDependentQuestionlist['option_dependent_question']['input_more_than_1'];
																	//$parent_option_question_id 				= $optionDependentQuestionlist['option_dependent_question']['parent_question_id'];
																	$parent_option_question_id 				= $sub_question_qid;
																	$selectedOptionName = $dependent_question_node[$dptKey]['option'];
																	$SubSecoption_option_dependent_salutation 	= $optionDependentQuestionlist['option_dependent_question']['salutation'];
																	$suboption_option_dependent_answer 		= $optionDependentQuestionlist['option_dependent_question']['answer'];
																	$dependent_option_question_node 		= $optionDependentQuestionlist['option_dependent_question'];
																	
																	$suboption_latitude 		= isset($optionDependentQuestionlist['option_dependent_question']['latitude'])? $optionDependentQuestionlist['option_dependent_question']['latitude']:"";
																	$suboption_longitude 		= isset($optionDependentQuestionlist['option_dependent_question']['longitude'])? $optionDependentQuestionlist['option_dependent_question']['longitude']:"";
																	
																	if(count($suboption_option_dependent_answer) > 0)
																	{																
																		if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "textbox" || $sub_sec_option_response_type == "textarea"))
																		{ 																	
																			$salutation_name = isset($SubSecoption_option_dependent_salutation['answer']['salutation'])? $SubSecoption_option_dependent_salutation['answer']['salutation']:"";
																			//if($suboption_option_dependent_answer['value']!="")
																			//{
																				$insertArr = array("response_id"		=> $cid,
																									"question_id" 		=> $sub_option_question_qid,
																									"answer_value" 		=> $suboption_option_dependent_answer['value'],
																									"salultation_value" => $salutation_name,
																									"parent_question_id" => $sub_question_qid);
																					
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND is_deleted = '0' AND parent_question_id='".$sub_question_qid."'");
																				$cnt = $queryResponse->num_rows();																			
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}																				
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																				}
																			//}
																		}
																			
																		// Latest Code Updated
																		if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'Yes' && ($sub_sec_option_response_type == "textbox" || $sub_sec_option_response_type == "textarea"))
																		{																	
																			foreach($suboption_option_dependent_answer as $key => $value) 
																			{
																				//if($value!="")
																				//{
																					$insertArr = array("response_id" 		=> $cid,
																										"question_id" 		=> $sub_option_question_qid,
																										"key_name" 			=> $key,
																										"answer_value" 		=> $value,
																										"parent_question_id" => $sub_question_qid,
																										"latitude" 			=> $suboption_latitude,
																										"longitude" 		=> $suboption_longitude);
																				
																					$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."'  AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id='".$sub_question_qid."'");
																					$cnt = $queryResponse->num_rows();																				
																					if($cnt > 0){$flag = 1;}else{$flag = 0;}																					
																					if($flag == 0)
																					{
																						$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																					}	
																					else 
																					{
																						$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid, "key_name" => "'".$key."'"));
																					}
																				//}																			
																			}																		
																		}
																		
																		if($upload_option_file_required == 'No' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
																		{ 
																			$salutation_name = isset($SubSecoption_option_dependent_salutation['answer']['salutation'])? $SubSecoption_option_dependent_salutation['answer']['salutation']:"";
																			if($sub_sec_option_response_type == "file") 
																			{
																				$upload_file = '';										
																				if($suboption_option_dependent_answer['value']!="") 
																				{
																					$random_name2 = $survey_id."_".$sub_global_question_id."_".time()."_".rand();
																					$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																				}

																				$insertArr = array("response_id" 		=> $cid,
																									"question_id" 		=> $sub_option_question_qid,
																									"answer_value" 		=> $upload_file,
																									"salultation_value" => $salutation_name,
																									"parent_question_id" => $parent_option_question_id,
																									"latitude" 			=> $suboption_latitude,
																									"longitude" 		=> $suboption_longitude);
																							
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND parent_question_id='".$parent_option_question_id."' AND is_deleted = '0'");
																				$cnt = $queryResponse->num_rows();
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}
																					
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id));
																				}	
																			}
																			else
																			{	
																				if(count($suboption_option_dependent_answer)> 0)
																				{
																					$chkArrpush = array();
																					foreach($suboption_option_dependent_answer as $kCh => $value)
																					{	
																						//if($value!="")
																						//{
																							$insertArr = array("response_id" 		=> $cid,
																												"question_id" 		=> $sub_option_question_qid,
																												"key_name" 			=> $kCh,
																												"answer_value" 		=> $value,
																												"salultation_value" => $salutation_name,
																												"parent_question_id"=> $parent_option_question_id,
																												"latitude" 			=> $suboption_latitude,
																												"longitude" 		=> $suboption_longitude);
																						
																							if($sub_sec_option_response_type == "checkbox" || $sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == 'single_select') 
																							{
																								array_push($chkArrpush, $kCh);
																							}	
																							
																							$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND parent_question_id='".$parent_option_question_id."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																							$cnt = $queryResponse->num_rows();
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}
																								
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id, "key_name" =>  "'".$kCh."'"));
																							}
																						//}
																						
																					}
																					// Delete Previous Answers regarding Checkbox and Selectbox
																					if($updateFlag)
																					{
																						if(count($chkArrpush) > 0) 
																						{
																							$updatedAt = date('Y-m-d H:i:s');
																							$this->db->where_not_in('key_name', $chkArrpush);
																							$updateArr = array("is_deleted" => '1');
																							$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $parent_option_question_id));
																						}														
																					}
																				}
																			}	
																															
																		}
																
																		if($upload_option_file_required == 'Yes' && $subsec_if_file_required == 'No' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
																		{ 
																			$salutation_name = isset($suboption_option_dependent_answer['answer']['salutation'])? $suboption_option_dependent_answer['answer']['salutation']:"";
																			$imgUrl = isset($suboption_option_dependent_answer['image_url'])? $suboption_option_dependent_answer['image_url']:"";
																			
																			$file_file = '';										
																			if($imgUrl!="") 
																			{
																				$random_name2 = $survey_id."_".$sub_option_question_qid."_".time()."_".rand();
																				$file_file = $this->generateImage($random_name2, $imgUrl);
																			}
																			
																			if($sub_sec_option_response_type == "file") 
																			{
																				$upload_file = '';										
																				if($suboption_option_dependent_answer['value']!="") 
																				{
																					$random_name2 = $survey_id."_".$sub_option_question_qid."_".time()."_".rand();
																					$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																				}
																				
																				$insertArr = array("response_id" 		=> $cid,
																									"question_id" 		=> $sub_option_question_qid,
																									"answer_value" 		=> $upload_file,
																									"file_name"			=> $file_file,
																									"parent_question_id" => $parent_option_question_id,
																									"latitude" 			=> $suboption_latitude,
																									"longitude" 		=> $suboption_longitude);
																						
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND is_deleted = '0'");
																				$cnt = $queryResponse->num_rows();																						
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}																				
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid,"parent_question_id" => $parent_option_question_id));
																				}																		
																			}
																			else 
																			{																			
																				if(is_array($suboption_option_dependent_answer['value']))
																				{	
																					$chkArrpush = array();
																					foreach($suboption_option_dependent_answer['value'] as $kCh => $value)
																					{
																						//if($value!="")
																						//{
																							$insertArr = array("response_id" 		=> $cid,
																												"question_id" 		=> $sub_option_question_qid,
																												"key_name" 			=> $kCh,
																												"answer_value" 		=> $value,
																												"file_name"			=> $file_file,
																												"salultation_value" => $salutation_name,
																												"parent_question_id" => $sub_question_qid,
																												"latitude" 			=> $suboption_latitude,
																												"longitude" 		=> $suboption_longitude);
																						
																							if($sub_sec_option_response_type == "checkbox" || $sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == 'single_select') 
																							{
																								array_push($chkArrpush, $key);
																							}																						
																							$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																							$cnt = $queryResponse->num_rows();																						
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}																							
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "key_name" => "'".$kCh."'", "parent_question_id" => $parent_option_question_id));
																							}
																						//}																					
																					}																				
																					// Delete Previous Answers regarding Checkbox and Selectbox
																					if($updateFlag)
																					{
																						if(count($chkArrpush) > 0) 
																						{
																							$updatedAt = date('Y-m-d H:i:s');
																							$this->db->where_not_in('key_name', $chkArrpush);
																							$updateArr = array("is_deleted" => '1');
																							$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																						}														
																					}
																				}
																			}						
																		}
																	
																		if($upload_option_file_required == 'No' && $subsec_if_file_required == 'Yes' && $subsec_option_inputMoreThanOne == 'No' && ($sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == "checkbox"  || $sub_sec_option_response_type == "single_select" || $sub_sec_option_response_type == "file"))
																		{ 																
																			$salutation_name = isset($suboption_option_dependent_answer['answer']['salutation'])? $suboption_option_dependent_answer['answer']['salutation']:"";
																			$imgUrl = isset($suboption_option_dependent_answer['image_url'])? $suboption_option_dependent_answer['image_url']:"";
																			
																			$file_file = '';										
																			if($imgUrl!="") 
																			{
																				$random_name2 = $survey_id."_".$sub_option_question_qid."_".time()."_".rand();
																				$file_file = $this->generateImage($random_name2, $imgUrl);
																			}
																			
																			if($sub_sec_option_response_type == "file") 
																			{
																				$upload_file = '';										
																				if($suboption_option_dependent_answer['value']!="") 
																				{
																					$random_name2 = $survey_id."_".$sub_option_question_qid."_".time()."_".rand();
																					$upload_file = $this->generateImage($random_name2, $suboption_option_dependent_answer['value']);
																				}
																				
																				$insertArr = array("response_id" 	=> $cid,
																								"question_id" 		=> $sub_option_question_qid,
																								"answer_value" 		=> $upload_file,
																								"file_name"			=> $file_file,
																								"parent_question_id" => $parent_option_question_id,
																								"latitude" 			=> $suboption_latitude,
																								"longitude" 		=> $suboption_longitude);
																						
																				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND key_name = '".$kCh."' AND parent_question_id = '".$parent_option_question_id."' AND is_deleted = '0'");
																				$cnt = $queryResponse->num_rows();																						
																				if($cnt > 0){$flag = 1;}else{$flag = 0;}																							
																				if($flag == 0)
																				{
																					$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																				}	
																				else 
																				{
																					$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid,"parent_question_id" => $parent_option_question_id));
																				}
																			
																			}
																			else 
																			{
																				if(is_array($suboption_option_dependent_answer['value']))
																				{
																					$chkArrpush = array();
																					foreach($suboption_option_dependent_answer['value'] as $kCh => $value)
																					{																					
																						if($value!="")
																						{
																							$insertArr = array("response_id" 		=> $cid,
																												"question_id" 		=> $sub_option_question_qid,
																												"key_name" 			=> $kCh,
																												"answer_value" 		=> $value,
																												"file_name"			=> $file_file,
																												"salultation_value" => $salutation_name,
																												"parent_question_id" => $sub_question_qid,
																												"latitude" 			=> $suboption_latitude,
																												"longitude" 		=> $suboption_longitude);
																						
																						
																							if($sub_sec_option_response_type == "checkbox" || $sub_sec_option_response_type == "radio" || $sub_sec_option_response_type == 'single_select') 
																							{
																								array_push($chkArrpush, $kCh);
																							}
																							
																							$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id = '".$cid."' AND question_id = '".$sub_option_question_qid."' AND key_name = '".$kCh."' AND parent_question_id = '".$sub_question_qid."' AND key_name = '".$kCh."' AND is_deleted = '0'");
																							$cnt = $queryResponse->num_rows();
																							
																							if($cnt > 0){$flag = 1;}else{$flag = 0;}
																								
																							if($flag == 0)
																							{
																								$insertQuery = $this->master_model->insertRecord('responses',$insertArr);
																							}	
																							else 
																							{
																								$updateQuery = $this->master_model->updateRecord('responses',$insertArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "key_name" => "'".$kCh."'", "parent_question_id" => $sub_question_qid));
																							}
																						}																					
																					}
																					
																					// Delete Previous Answers regarding Checkbox and Selectbox
																					if($updateFlag)
																					{
																						if(count($chkArrpush) > 0) 
																						{
																							$updatedAt = date('Y-m-d H:i:s');
																							$this->db->where_not_in('key_name', $chkArrpush);
																							$updateArr = array("is_deleted" => '1');
																							$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $cid, "question_id" => $sub_option_question_qid, "parent_question_id" => $sub_question_qid));
																						}														
																					}
																				}
																			}						
																		}
																																
																	} 
																	else 
																	{
																		if($updateFlag)
																		{
																			$this->removeAnswers($sub_option_question_qid, $parent_option_question_id, $response_id);
																		}
																	}
																	
																} // End Foreach
															}
														}   // Sub Section Dependent Question
													
													}
													else 
													{
														if($updateFlag)
														{
															$this->removeAnswers($sub_question_qid, $parent_question_id, $response_id);
														}
													}
												}										
											}
										}
											
									} // End Sub Section End
										
									// Log Data Added
									$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
									$ipAddr		= $this->master_model->get_client_ip();						
									$postArr			= $_POST;	
									$json_encode_data 	= json_encode($postArr);
									$logData = array('user_id' => $surveyer_id,'action_name'=> "Add",'module_name'=> 'API CALL', 'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
									//$this->master_model->insertRecord('api_log',$logData);
									
									//$res = array();
									//$this->api_log($postArr, $res);
									
									$response['status'] 	= "1";
									$response['message'] 	= "Survey Response Saved Successfully.";
									$response['data']		= array("response_id" => $cid, "ref_id" =>$ref_id);
								
								/*else
								{						
									$response['status'] 	= "0";
									$response['message'] 	= "No Section Questions Found";
								}*/
									
							}	// Section Foreach End
						}
						else
						{
							//echo "No Sections Found";
							$response['status'] 	= "0";
							$response['message'] 	= "No Sections Found.";
						}
					
					} // Ref ID Check For Duplication
				}
				else 
				{
					//echo "No Data Found";
					$response['status'] 	= "0";
					$response['message'] 	= "Survey ID does not exist.";
				}
			}
			else
			{
				//echo "No Data Found";
				$response['status'] 	= "0";
				$response['message'] 	= "No Data Found.";
			}			
			
		} // End User Check
		
		$this->response($response);
		
	} // Function End
	
	// Remove Answer When Resubmit 
	public function removeAnswers($question_id, $parent_id, $response_id)
	{
		//echo "==QuestionID==".$question_id."==Parent ID==".$parent_id."==Response ID==".$response_id;
		$updateArr = array('is_deleted' => 1);
		$updateNotQuery = $this->master_model->updateRecord('responses',$updateArr,array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_id));
		//echo $this->db->last_query();
	}
	
	// Survey Response List
	public function survey_response_list() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		$status		= isset($_POST['status']) ? $_POST['status'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} 
		else 
		{
			/*if(empty($status))
			{
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "Status can not be blank.";
			
			} 
			else */
			if(empty($survey_id))
			{
				// request not be null -
				$response['status']		= "0";
				$response['message']    = "Survey ID can not be blank.";
			}
			else 
			{			
				$statusWhr = '';
				if($status == "returned")
				{
					$statusWhr = " AND (`survey_response_headers`.`status` = 'Level 1 Returned' OR `survey_response_headers`.`status` = 'Level 2 Returned')";
				}
				else if($status == "approved") 
				{
					$statusWhr = " AND (`survey_response_headers`.`status` = 'Level 1 Approved' OR `survey_response_headers`.`status` = 'Level 2 Approved')";
				}
				else if($status == "submitted") 
				{
					$statusWhr = '';
					//$statusWhr = " AND (`survey_response_headers`.`status` = 'Submitted' OR `survey_response_headers`.`status` = 'Re-Submitted')";
				}
				
				//if($statusWhr!= "")
				//{
					$surveyResponseList = $this->db->query("SELECT survey_response_headers.response_id, survey_response_headers.ref_id, survey_response_headers.submitted_date, survey_response_headers.submited_time, survey_response_headers.start_date, survey_response_headers.end_date, survey_village_master.village_id, survey_village_master.village_name, survey_response_headers.status FROM survey_response_headers LEFT JOIN `survey_village_master` ON `survey_response_headers`.`village_id` = `survey_village_master`.`village_id` WHERE `survey_response_headers`.`survey_id` = '".$survey_id."' ".$statusWhr." AND `survey_response_headers`.`is_deleted` = '0' AND `survey_response_headers`.`surveyer_id` = '".$user_id."' ORDER BY survey_response_headers.response_id DESC");
					//echo $this->db->last_query();
					$cntRow = $surveyResponseList->num_rows();			
					$response_data = array();
					if($cntRow > 0)
					{ 				
						foreach($surveyResponseList->result_array() as $row)
						{
							$response_data[] = array('response_id' => $row['response_id'], 'ref_id' => $row['ref_id'], 'submitted_date' => $row['submitted_date'], 'submited_time' => $row['submited_time'], 'start_date' => $row['start_date'], 'end_date' => $row['end_date'], 'village_id' => $row['village_id'], 'village_name' => $row['village_name'], 'status' => $row['status']);
						}
						
						$response['status']		= "1";
						$response['message'] 	= "Data sucessfully received.";
						$response['data'] 		= $response_data;
					}
					else
					{
						$response['status'] 		= "0";
						$response['message'] 	= "No data Found.";
					}
				/*}
				else
				{
					$response['status'] 	= "0";
					$response['message'] 	= "Invalid Status.";
				}*/
			
			}
		}
		
		$this->response($response);
	}
	
	
	// gloabal question function for return survey
	public function return_global_question($question_id, $global_question_id, $response_id)
	{
		$in_arr = array();		
		$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$global_question_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			 // get the first row
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			}  
			$in_arr['question_id'] 			= $questionData->question_id;
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $question_id;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_file_required']		= 'No';	
			$in_arr['if_summation']			= 'No';								
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}
			
			// Add Answer Node 
			$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE response_id='".$response_id."' AND question_id = '".$global_question_id."' AND parent_question_id = '".$question_id."' AND is_deleted = '0' ORDER BY id DESC LIMIT 1");
			$responseData = $queryResponse->row();
			//echo $this->db->last_query();
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$global_question_id."' AND is_deleted='0'");
		
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="") 
					{
						array_push($optArr_1, $opt_1['option']); 
					}
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] =$this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}	
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}
				
				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
												
			}
			
			if(count($responseData) > 0)
			{
				// Add Answer Node
				//echo $global_question_id."=>>>=".$question_id."=>>>=".$questionData->response_type_id."=>>>=".$questionData->input_more_than_1."=>>>=".$questionData->if_file_required."=>>>=".$questionData->input_file_required;	
				$answerNode = $this->addAnswer($response_id, $global_question_id, $questionData->response_type_id, $questionData->input_more_than_1, $questionData->if_file_required, $questionData->input_file_required, $salutationArr, $question_id);	
				//print_r($answerNode);die();
				$in_arr['answer']   = $answerNode;
			}
			
		}
		
		$dataArr = array("question_id" => $question_id, "global_question" => $in_arr);	
		
		return $in_arr;
	}
	
	// response recursive function
	public function getOptionbaseQuestionMultipleResponse($question_id, $dependant_ques_id, $id, $option, $survey_id, $response_id)
	{ 
		//ob_start();		
		$queryQuestion = $this->db->query("SELECT question_id,question_text,response_type_id,is_mandatory,parent_question_id,salutation,input_more_than_1,if_file_required,file_label, input_file_required,if_summation,help_label FROM survey_question_master WHERE question_id = '".$dependant_ques_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			$in_arr = array();
			$explodeSub =  array();
			if($questionData->salutation!="")
			{
				$explodeSub = explode("|", $questionData->salutation);
			} 
			 // get the first row
			$in_arr['question_id'] 			= $questionData->question_id; 
			$in_arr['question_label'] 		= $questionData->question_text;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
			$in_arr['question_help_label'] 	= $questionData->help_label;
			$in_arr['salutation'] 			= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['if_file_required']		= 'No';
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_summation']			= 'No';
			$in_arr['is_correction_required']	= 'Yes';
			$arrRemark = array("survey_id" => $survey_id, "response_id" =>  $response_id, "question_id" =>  $questionData->question_id);
			$remarkContent = $this->remarkContent($arrRemark);
			$in_arr['remark'] = $remarkContent;
			if($questionData->if_file_required == 'Yes')
			{
				$in_arr['if_file_required'] 	= $questionData->if_file_required;
				$in_arr['file_label'] 			= $questionData->file_label;
			}
			
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}	
			
			
			
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT *  FROM survey_questions_options_master WHERE question_id = '".$questionData->question_id."' AND is_deleted='0' ORDER BY display_order ASC");
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option']!="")
					{
						array_push($optArr_1, $opt_1['option']);
					}
					
					
					/*************************** Multiple Option Dependent Question Data Start *********************************/
				
						$quetionOptionMultipleQuestion = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$questionData->question_id."' AND ques_option_id='".$opt_1['ques_option_id']."' AND is_deleted='0'");
						$multipleQuestionCnt = $quetionOptionMultipleQuestion->num_rows();
						if($multipleQuestionCnt > 0)
						{
							foreach($quetionOptionMultipleQuestion->result_array() as $multipleQues)
							{
								$queryMultipleQuestion = $this->db->query("SELECT * FROM survey_questions_options_master WHERE ques_option_id = '".$multipleQues['ques_option_id']."' AND is_deleted = '0' ORDER BY display_order ASC");
								$questionMultipleData = $queryMultipleQuestion->row();
								$getOption = $questionMultipleData->option;
								
								// Below Code for multiple level array questions
								$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultipleResponse($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption, $survey_id, $response_id);								
								//$in_arr['dependent_question_options'] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);
							}
							
						}
				
					/************************* Multiple Option Dependent Question Data Start ***********************************/
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}
					//$in_arr['validation_id'] 	= $opt_1['min_value'];
					//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}

				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
				//$in_arr['options'] = json_encode($optArr_1);

				$queryResponse = $this->db->query("SELECT * FROM survey_responses WHERE question_id = '".$dependant_ques_id."' AND parent_question_id = '".$question_id."' AND response_id='".$response_id."' AND is_deleted = '0' ORDER BY id DESC");
				//echo $this->db->last_query();
				$responseData = $queryResponse->row();
				//print_r($responseData);	
				if(count($responseData) > 0)
				{
					// Add Answer Node 	
					$answerNode 	= $this->addAnswer($response_id, $responseData->question_id, $questionData->response_type_id, $questionData->input_more_than_1, $questionData->if_file_required, $questionData->input_file_required, $salutationArr, $responseData->parent_question_id);	
					$in_arr['answer']  =  $answerNode;
				}
			}
		}
		
		$dataArr = array("option" => $option, "option_dependent_question" => $in_arr); 		
		//return json_encode($dataArr);
		return $dataArr;
	} // Function End
	
	//Remark Showing In Questionaire 
	public function remarkContent($arr)
	{
		$question_id 	= $arr['question_id']; 
		$survey_id 	 	= $arr['survey_id']; 
		$response_id 	= $arr['response_id'];	
		$this->db->select('remark');	
		$this->db->order_by('status_id', 'desc');
		$this->db->limit(1);
		$returnContent = $this->master_model->getRecords("response_remark",array('survey_id' => $survey_id, 'response_id' => $response_id, 'question_id' => $question_id));
		$return_text = "";
		if(count($returnContent) > 0)
		{
			$return_text = $returnContent[0]['remark'];
		}
		return $return_text;	
	}
	
	// Return Survey Section List
	public function getReturnSurveySectionList($survey_id, $returnQuestionID)
	{
			$result = array();
			
			$surveySectionList = $this->db->query("SELECT DISTINCT `survey_section_questions`.`section_id`, `survey_section_master`.`section_name`, `survey_section_master`.`section_desc` FROM `survey_section_questions` JOIN `survey_section_master` ON `survey_section_master`.`section_id` = `survey_section_questions`.`section_id` WHERE `survey_section_questions`.`survey_id` = '".$survey_id."' AND `survey_section_questions`.`is_deleted` = 0 AND `survey_section_master`.`is_deleted` = 0 AND `survey_section_questions`.`question_id` IN('".$returnQuestionID."') ORDER BY `survey_section_questions`.`sort_order` IS NOT NULL DESC, `survey_section_questions`.`sort_order` ASC");					
			//echo $this->db->last_query();
			$surveyCount = $surveySectionList->num_rows();
			
			if($surveyCount > 0)
			{		
				foreach($surveySectionList->result_array()  as $res) 
				{
					$result[] = array("section_id" => $res['section_id'], "section_name" => $res['section_name'], "section_description" => $res['section_desc']);
				}
			}
			
			return $result;
				
	}
	
	
	// Survey Questionaire For Survey Responses
	public function survey_response_details()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// Get Survey ID
		//$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		$response_id	= isset($_POST['response_id']) ? $_POST['response_id'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "User ID can not be blank.";
			
		} 
		else 
		{
			/*if(empty($survey_id)){
			
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Survey name is required.";
				
			} 
			else */

			if(empty($response_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Response id is required.";
				
			} 
			else 
			{
				// Check Surveyor Is Match
				$check_record = $this->master_model->getRecords("response_headers",array('surveyer_id'=>$user_id, "response_id" => $response_id));
				$survey_id = $check_record[0]['survey_id'];
				$ref_id = $check_record[0]['ref_id'];
				
				if(count($check_record) > 0)
				{
					// Get Survey Details 
					$response['status'] 		= "1";
					$response['message'] 		= "Survey response details get successfully.";	
					
					$responseQuery = $this->db->query("SELECT user_id, status_id FROM survey_response_status WHERE survey_id = '".$survey_id."' AND response_id='".$response_id."' ORDER BY status_id DESC LIMIT 1");
					//echo "==>".$this->db->last_query();	
					$responseCount = $responseQuery->num_rows();
					$returnQuesionaire = array();
					if($responseCount > 0)
					{
						foreach($responseQuery->result_array() as $res)
						{
							$remarkQuery = $this->db->query("SELECT question_id FROM survey_response_remark WHERE survey_id = '".$survey_id."' AND response_id='".$response_id."' AND status_id = '".$res['status_id']."' ORDER BY remark_id DESC");
							//echo "==>>>".$this->db->last_query();
							foreach($remarkQuery->result_array() as $result)
							{
								$questionID = $result['question_id'];
								array_push($returnQuesionaire, $questionID);
							}
						}
					}
					
					if(count($returnQuesionaire) > 0)
					{
						$surveyData = $this->db->query("SELECT * FROM survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
						$surveyResult = $surveyData->row();			
						$response['survey_id'] 			= $surveyResult->survey_id;
						$response['survey_title'] 		= $surveyResult->title; 
						$response['survey_description'] = $surveyResult->description;
						
						// response details required by Vishal, added on Bhagwan on 27-03-2021
						$response['response_id'] = $response_id;
						$response['ref_id'] = $ref_id;
						// eof response details required by Vishal, added on Bhagwan on 27-03-2021
						
						$returnQuestionID = "" . implode( "','", $returnQuesionaire ) . "";
						// Ask Section Question Table For Section & Sub Section			
						$surveyQuestionList = $this->db->query("SELECT * FROM survey_section_questions WHERE survey_id = '".$survey_id."'  AND status = 'Active' AND is_deleted = '0' AND question_id IN('".$returnQuestionID."') ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");					
						//echo $this->db->last_query();die();
						$sectionQuesitonCount = $surveyQuestionList->num_rows();
						
						if($sectionQuesitonCount > 0)
						{				
							$section_question = array();
							
							$surveySections = $this->getReturnSurveySectionList($survey_id, $returnQuestionID);
							
							foreach($surveyQuestionList->result_array() as $res)
							{	
								
								$querySection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id = '".$res['section_id']."' AND parent_section_id = 0 AND is_deleted = '0' AND status = 'Active'");
								$sectionData = $querySection->row();	
								//echo $this->db->last_query();
								$sectionArr = array();
								
								if($sectionData->section_id!="") 
								{
									$section_id 			= $sectionData->section_id;	
									$section_name 			= $sectionData->section_name; 
									$section_description 	= $sectionData->section_desc;
								}

								$sub_section_id 			= '';	
								$sub_section_name 			= ''; 
								$sub_section_description 	= '';
								if($res['sub_section_id'] > 0)
								{							
									$querySubSection = $this->db->query("SELECT section_id, parent_section_id, section_name, section_desc   FROM survey_section_master WHERE section_id='".$res['sub_section_id']."' AND is_deleted = '0' AND status = 'Active'");
									//echo $this->db->last_query();
									$subSectionData = $querySubSection->row();	
									$sub_section_id 			= @$subSectionData->section_id;	
									$sub_section_name 			= @$subSectionData->section_name; 
									$sub_section_description 	= @$subSectionData->section_desc;
								}
								
								$if_checked_parent = $this->db->query("SELECT * FROM survey_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
								$if_parents_count = $if_checked_parent->num_rows();
								
								if($if_parents_count == 0)
								{
									$in_arr = array();
									// Global Question 
									$global_question_id 		= $res['global_question_id'];
									if($global_question_id > 0)
									{							
										$in_arr['global_question'] = $this->return_global_question($res['question_id'], $global_question_id, $response_id);
									}
									
									$queryQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
									$questionData = $queryQuestion->row();	
									$explodeSub =  array();
									if($questionData->salutation!="")
									{
										$explodeSub = explode("|", $questionData->salutation);
									} 

									$in_arr['section_id'] 			= $section_id;
									$in_arr['section_name'] 		= $section_name;
									$in_arr['section_desc'] 		= $section_description;
									
									$in_arr['sub_section_id'] 		= $sub_section_id;
									$in_arr['sub_section_name'] 	= $sub_section_name;
									$in_arr['sub_section_des'] 		= $sub_section_description;
									
									$in_arr['question_id'] 			= $questionData->question_id;
									$in_arr['sort_order'] 			= $res['sort_order'];
									$in_arr['question_label'] 		= $questionData->question_text;
									$in_arr['response_type'] 		= $questionData->response_type_id;
									$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
									$in_arr['salutation'] 			= json_encode($explodeSub);
									$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
									$in_arr['question_help_label'] 	= $questionData->help_label;
									$in_arr['upload_file_required']	= 'No';
									$in_arr['if_summation']			= 'No';
									$in_arr['if_file_required']		= 'No';
									$in_arr['is_correction_required']	= 'Yes';
									$parentQuestionID 				= '0';
									$arrRemark = array("survey_id" => $survey_id, "response_id" =>  $response_id, "question_id" =>  $questionData->question_id);								
									$remarkContent = $this->remarkContent($arrRemark);
									$in_arr['remark'] = $remarkContent;
									if($questionData->input_file_required == 'Yes')
									{
										$in_arr['upload_file_required'] 	= $questionData->input_file_required;
									}
									
									if($questionData->if_file_required == 'Yes')
									{
										$in_arr['if_file_required'] 	= $questionData->if_file_required;
										$in_arr['file_label'] 			= $questionData->file_label;
									}
									
									if($questionData->if_summation == 'Yes')
									{
										$in_arr['if_summation'] 		= $questionData->if_summation;
									}
									
									$getResponse = $this->master_model->getRecords("responses", array("response_id" => $response_id, "question_id" => $questionData->question_id, "parent_question_id" => $parentQuestionID, "is_deleted" => 0),'',array("id" => 'DESC'));
									//echo $this->db->last_query();
									if(count($getResponse) > 0)
									{
										// Add Answer Node 
										$inputMore 			= $questionData->input_more_than_1;								
										$fileRequired 		= $questionData->if_file_required;
										$uploadRequired 	= $questionData->input_file_required;
										$responseTypeChek 	= $questionData->response_type_id;
										$salutationArr		= $explodeSub;
										$answerNode 		= $this->addAnswer($response_id, $questionData->question_id, $responseTypeChek, $inputMore, $fileRequired, $uploadRequired, $salutationArr, $parentQuestionID);	
										$in_arr['answer']  =  $answerNode;
									}
									
									// Get Sub Question List On Main Question 								
									$subQuestion = $this->db->query("SELECT * FROM survey_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
									$sub_questuon_count = $subQuestion->num_rows();
									
									if($sub_questuon_count > 0)
									{									
										foreach($subQuestion->result_array() as $opt)
										{
											$sub_arr = array();
											$sub_arr['question_id'] 		= $opt['question_id'];
											$explodeSub =  array();
											if($opt['salutation']!="")
											{
												$explodeSub = explode("|", $opt['salutation']);
											} 
											else 
											{
												$blank = array();
												$explodeSub = $blank;
											}
											
											$sub_arr['section_id'] 			= $section_id;
											$sub_arr['section_name'] 		= $section_name;
											$sub_arr['section_desc'] 		= $section_description;
											
											$sub_arr['sub_section_id'] 		= $sub_section_id;
											$sub_arr['sub_section_name'] 	= $sub_section_name;
											$sub_arr['sub_section_des'] 	= $sub_section_description;
											
											$sub_arr['question_id'] 		= $opt['question_id'];
											$sub_arr['question_label'] 		= $opt['question_text'];
											$sub_arr['response_type'] 		= $opt['response_type_id'];
											$sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
											$sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
											$sub_arr['question_help_label'] = $opt['help_label'];
											$sub_arr['salutation'] 			= json_encode($explodeSub);
											$sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
											$sub_arr['upload_file_required']	= 'No';
											$sub_arr['if_summation']			= 'No';	
											$sub_arr['if_file_required']		= 'No';	
											$sub_arr['is_correction_required']	= 'Yes';
											
											$subRemark = array("survey_id" => $survey_id, "response_id" =>  $response_id, "question_id" =>  $opt['question_id']);								
											$subremarkContent = $this->remarkContent($subRemark);
											$sub_arr['remark'] = $subremarkContent;											
											if($opt['input_file_required'] == 'Yes')
											{
												$sub_arr['upload_file_required'] 	= $opt['input_file_required'];
											}
											
											$getResponseSub = $this->master_model->getRecords("responses", array("response_id" => $response_id, "question_id" => $opt['question_id'], "parent_question_id" => '0', "is_deleted" => 0),'',array("id" => 'DESC'));
											//echo $this->db->last_query();
											if(count($getResponseSub) > 0)
											{
												
												// Add Answer Node 											
												$salutationArr		= $explodeSub;
												$fileRequired 		= $opt['if_file_required'];
												$uploadRequired		= $opt['input_file_required'];
												$subParent 		= 0;
												$answerNode 		= $this->addAnswer($response_id, $opt['question_id'], $opt['response_type_id'], $opt['input_more_than_1'], $fileRequired, $uploadRequired, $salutationArr, $subParent);	
												$sub_arr['answer']  =  $answerNode;
											}
											
										}	// Sub Array End							
									
										$quetionOptions = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
										$option_count = $quetionOptions->num_rows();	
										if($option_count > 0)
										{
											$optArr = array();										
											foreach($quetionOptions->result_array() as $optSub)
											{
												if($optSub['option']!="")
												{	
													array_push($optArr, $optSub['option']);	
												}		
													$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
													$multi_option_count = $queryMultipleCall->num_rows();
													if($multi_option_count > 0)
													{ 
														foreach($queryMultipleCall->result_array() as $dpenSub)
														{
															$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultipleResponse($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option'],$survey_id, $response_id);	
														}
													}
												
												if($optSub['validation_id'] > 0)
												{
													$in_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
													
													if($optSub['sub_validation_id'] > 0)
													{
														$in_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
													}
												}
												else 
												{
													$in_arr['validation_type'] = '';
													$in_arr['validation_sub_type'] = '';
												}	
										
												$in_arr['min_value'] 		= $optSub['min_value'];
												$in_arr['max_value'] 		= $optSub['max_value'];
												$in_arr['validation_label']	= $optSub['validation_label'];	
											}						

											if(count($optArr) > 0)
											{
												$in_arr['options'] = json_encode($optArr);
											}
											else
											{
												$blank = array("");
												$in_arr['options'] = json_encode($blank);
												//$in_arr['options'] = '[]';
											}	
										}
										
										$in_arr['sub_question'] = $sub_arr;							
										//$in_arr['sub_question'][] = $sub_arr;
										
									} // End Sub Question If
									
									// Get Parent Question Option 
									$quetionOptions_1 = $this->db->query("SELECT * FROM survey_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
									$option_count_1 = $quetionOptions_1->num_rows();	
									
									if($option_count_1 > 0)
									{
										$optArr_1 = array();
										foreach($quetionOptions_1->result_array() as $opt_1)
										{
											if($opt_1['option']!="")
											{
												array_push($optArr_1, $opt_1['option']);
											}									
											// Multiple Question Loop
											$queryMultipleCall = $this->db->query("SELECT * FROM survey_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
											$multi_option_count = $queryMultipleCall->num_rows();
											if($multi_option_count > 0)
											{ 
												foreach($queryMultipleCall->result_array() as $dpenSub)
												{
													$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultipleResponse($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $opt_1['option'],$survey_id, $response_id);	
												}
											}
									
											if($opt_1['validation_id'] > 0)
											{
												$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
												
												if($opt_1['sub_validation_id'] > 0)
												{
													$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
												}
											}
											else 
											{
												$in_arr['validation_type'] = '';
												$in_arr['validation_sub_type'] = '';
											}	
											
											$in_arr['min_value'] 		= $opt_1['min_value'];
											$in_arr['max_value'] 		= $opt_1['max_value'];
											$in_arr['validation_label']	= $opt_1['validation_label'];
										}										
										
										if(count($optArr_1) > 0)
										{
											$in_arr['options'] = json_encode($optArr_1);
										}
										else
										{
											$blank = array("");
											$in_arr['options'] = json_encode($blank);
											//$in_arr['options'] = '[]';
										}
										
									} // End Option If End							
									
									$section_question[]	= $in_arr;							
									
								} // Check End Related Option Dependent Has Parent				
								
							} // End Foreach Related Section Question
							
							$sectionArr['section_list']		= $surveySections;
							$sectionArr['section_question']	= $section_question;
						}			
						else 
						{
							$response['status'] 	= "0";
							$response['message'] 	= "No Question Found.";
						}
						
						$response['data']['sections'][]	= $sectionArr;
					}
					else 
					{
						$response['status'] 	= "0";
						$response['message'] 	= "No Return Survey Found With Remark(s).";
					}
				}
				else 
				{
					$response['status'] 	= "1";
					$response['message'] 	= "Invalid Response ID.";
				}
			}
		}
		
		$this->response($response);
	}	
	
	// Enccode Image with Base64 Encode Format
	public function convertImageToBase64encode($image_name)
	{
		
		$img = file_get_contents('uploads/'.$image_name);
		  
		// Encode the image string data into base64
		$data = base64_encode($img);
		  
		// Display the output
		return $data;
	}
	
	//Add Answer Node To Responses 
	public function addAnswer($response_id, $question_id, $type, $inputMore, $fileRequired, $uploadRequired, $salutation, $parentQuestionID)
	{
		$getResponse = $this->master_model->getRecords("responses", array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parentQuestionID, "is_deleted" => 0),'',array("id" => 'DESC'));
		//echo $this->db->last_query();
		
		$response = array();
		if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="Yes" && $uploadRequired=="Yes")
		{
			if(count($salutation) > 0)
			{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}				
			}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
				
				if($getResponse[0]['input_file_required']!="")
				{
					$response['value']['upload_file'] = $this->convertImageToBase64encode($getResponse[0]['input_file_required']);
				}
				else 
				{
					$response['value']['upload_file'] = "";
				}
			}
			$response['value'] = $str;			
		}
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			if(count($salutation) > 0)
			{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
			}
			$response['value'] = $str;		
			
		}
		// Condition added to handle single textfield + image, by Bhagwan, on 02-04-2021
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="No" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			if(count($salutation) > 0)
			{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$response['value'] = $getResponse[0]['answer_value'];		
			
		} // EOF condition added to handle single textfield + image, by Bhagwan, on 02-04-2021
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="No" && $uploadRequired=="No")
		{
			if(count($salutation) > 0)
			{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
				$response = $str;
			}
		}
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="No" && $fileRequired=="No" && $uploadRequired=="No")
		{
			if(count($salutation) > 0)
			{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			}
			
			$response["value"] = $getResponse[0]['answer_value'];
		}
		else if(($type=="checkbox" || $type=="radio" || $type=="single_select" || $type=="file") && $inputMore=="No" && $fileRequired=="No" && $uploadRequired=="No")
		{
			$str = array();
			
			if($type=="file")
			{	
				if($getResponse[0]['answer_value']!="")
				{
					//$response['value'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
					$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
				}
				else 
				{
					//$response['value'] = "";
					$response['image_url'] = "";
				}				
			}
			else if($type=="radio" || $type=="single_select")
			{
				$response["value"] = $getResponse[0]['answer_value'];
			}
			else if($type=="checkbox")
			{	
				//echo "RESULT<pre>";print_r($getResponse);
				if(count($getResponse) > 0)
				{	
					foreach($getResponse as $key => $values)
					{	//echo ">>>>".$values['answer_value'];
						$str[$values['key_name']] = $values['answer_value'];
					}
				}
				
				$response = $str;
			}
			//$response['value'] = $str;		
		}
		else if(($type=="checkbox" || $type=="radio" || $type=="single_select" || $type=="file") && $inputMore=="No" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}	
			
			$str = array();
			
			if($type=="file")
			{	
				if($getResponse[0]['answer_value']!="")
				{
					//$response['value'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
					$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
				}
				else 
				{
					//$response['value'] = "";
					$response['image_url'] = "";
				}				
			}
			else if($type=="radio" || $type=="single_select")
			{
				$response["value"] = $getResponse[0]['answer_value'];
			}
			else if($type=="checkbox")
			{
				if(count($getResponse) > 0)
				{
					foreach($getResponse as $key => $values)
					{
						$str[$values['key_name']] = $values['answer_value'];
					}
				}
				
				$response = $str;
			}	
		}
		
		return $response;
	}
	
	// function to user logout
	public function logout()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		/*if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "All fields are mandatory.";
			
		} else {*/			
			if(!empty($user_id) && is_user_login($user_id))
            {
				$updated_date = date("Y-m-d H:i:s");

				$sql = "UPDATE survey_users SET is_login = '0', updated_on = '".$updated_date."' WHERE user_id = '".$user_id."'";
				if($result = $this->db->query($sql))
				{
					$response['status']		= "1";
					$response['message'] 	= "User logout successfully.";
				}
				else
				{
					$response['status'] 	= "1";
					$response['message'] 	= "No data Found.";
				}
			}
			else
			{
				$response['status']		= "0";
				$response['message']    = "You must be logged in to perform this request.";
			}
		//}
		
		$this->response($response);
	}
	
	// Get Image Name
	public function generateImage($random_name, $image_name)
	{
		define('UPLOAD_DIR', 'uploads/');
		
		//Survey_id_response_id_question_id
		$imgName = $random_name.'.jpg';
		$file_name = UPLOAD_DIR.$imgName;
		
		if (base64_encode(base64_decode($image_name, true)) === $image_name) {
			$image_base64 = base64_decode($image_name);
			//$file_name = UPLOAD_DIR . 'new.jpg';
			file_put_contents($file_name, $image_base64);	
			//echo "++".$imgName;die();
			return $imgName;
		}
		else 
		{
			$imgName = '';
			return $imgName;
		}	
		
		/*$image_base64 = base64_decode($image_name);
		//$file_name = UPLOAD_DIR . 'new.jpg';
		file_put_contents($file_name, $image_base64);		
		return $imgName;		
		return 1;*/
	}
	
	// function to send JSON response -
	public function response($response)
	{
		// log api access -
		$req = array();
		$this->api_log($req, $response, TRUE, $this->log_id);
		
		@header('Content-type: application/json');
		echo json_encode($response);
		
		die();
	}
	
	// function to log api access
	public function api_log($req, $res, $is_update = FALSE, $log_id = NULL)
	{
		// get api user agent -
		$user_agent = @$_SERVER['HTTP_USER_AGENT'];
		
		$req = json_encode($req);
		$res = json_encode($res);
		
		$access_date = date("Y-m-d H:i:s");
		
		//$query = $this->db->query("INSERT INTO api_log(user_agent, req, res, access_date) VALUES('$user_agent', '$req', '$res', '$access_date')");
		
		$headers = "";
		foreach (@getallheaders() as $name => $value) { 
			//echo "$name: $value \n";
			$headers .= "$name: $value \n";
		}
		
		// $_SERVER
		$server = print_r($_SERVER, TRUE);
		
		/*$myfile = @fopen("logs_".date("dmY").".txt", "a") or die("Unable to open file!");
		$txt = "
		=================== ".$access_date." ====================
					
		# Headers:
		".$headers."
		
		# Server:
		".$server."
		
		# Request:
		".$req."
		
		# Response:
		".$res."
		";
		@fwrite($myfile, $txt);
		@fclose($myfile);*/
		
		// =====
		
		$request_headers = array();
		$request_headers["Headers"] = $headers;
		$request_headers["Server"] = $server;
		
		//$request_headers_json_str = json_encode($request_headers);
		$request_headers_str = print_r($request_headers, TRUE);
		
		$ip = @isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $this->input->ip_address();
		
		// If update
		if($is_update == TRUE)
		{
			$query = $this->db->query("UPDATE `app_api_logs` SET response = '$res', response_timestamp = '$access_date', edited_date = '$access_date' WHERE id = " . $log_id);
		}
		else
		{
			$query = $this->db->query("INSERT INTO `app_api_logs` (`request`, `request_timestamp`, `request_headers`, `ip`, `user_agent`, `added_date`) VALUES ('$req', '$access_date', '$request_headers_str', '$ip', '$user_agent', '$access_date')");
			
			$log_id = $this->db->insert_id();
		}
		
		return $log_id;
	}
	
} // Class End
