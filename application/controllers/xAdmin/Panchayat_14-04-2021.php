<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Panchayat
Author : Vicky K
*/

class Panchayat extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {	
		// Permission Set Up
		$this->master_model->permission_access('12', 'view');	
	
    	$this->db->where('survey_panchayat_master.is_deleted','0');
		$this->db->where('survey_block_master.status','Active'); 
		$this->db->where('survey_block_master.is_deleted','0'); 
			
		$this->db->select('survey_block_master.block_name, survey_panchayat_master.block_id, survey_panchayat_master.panchayat_id, survey_panchayat_master.status,  survey_panchayat_master.panchayat_name');
		$this->db->join('survey_block_master','survey_panchayat_master.block_id=survey_block_master.block_id','left',FALSE);
		$response_data = $this->master_model->getRecords("panchayat_master",'','',array('panchayat_id'=>'DESC'));	
		//echo $this->db->last_query();die();
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Panchayat';	
    	$data['middle_content']='panchayat/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add state
    public function add()
    {
		// Permission Set Up
		$this->master_model->permission_access('12', 'add');
		$this->db->where('survey_block_master.status','Active'); 
		$this->db->where('survey_block_master.is_deleted','0'); 
		$response_data = $this->master_model->getRecords("block_master",'','',array('block_name'=>'ASC'));	
        // Check Validation
		$this->form_validation->set_rules('block_id', 'Block Name', 'required');	
		$this->form_validation->set_rules('name', 'Gaon/Panchayat Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			$block_id 			= $this->input->post('block_id');			
			$panchayat_name 	= trim($this->input->post('name'));	
			$insertArr = array( 'block_id' => $block_id, 'panchayat_name' => $panchayat_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('panchayat_master',$insertArr);
			
			if($insertQuery > 0){				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Panchayat Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Panchayat successfully created');
				redirect(base_url('xAdmin/panchayat'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/panchayat/add'));
			}
		}
		$data['blockList'] = $response_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Panchayat';	
    	$data['middle_content']='panchayat/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit state 
	 public function edit($id)
    {
		 // Permission Set Up
		$this->master_model->permission_access('12', 'edit');
		
		$panchayat_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("panchayat_master",array('panchayat_id'=>$panchayat_id));
		
		$this->form_validation->set_rules('block_id', 'Block Name', 'required');	
		$this->form_validation->set_rules('name', 'Gaon/Panchayat Name', 'required|min_length[3]|xss_clean');
		
		$this->db->where('survey_block_master.status','Active'); 
		$this->db->where('survey_block_master.is_deleted','0'); 
		$response_data = $this->master_model->getRecords("block_master",'','',array('block_name'=>'ASC'));	
		if($this->form_validation->run())
		{	
		
			$block_id 	= $this->input->post('block_id');
			$panchayat_name 	= trim($this->input->post('name'));
			
			$updateArr = array( 'block_id' => $block_id, 'panchayat_name' => $panchayat_name, 'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('panchayat_master',$updateArr,array('panchayat_id' => $panchayat_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Panchayat Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}

				$this->session->set_flashdata('success','Panchayat successfully updated');
				redirect(base_url('xAdmin/panchayat'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/panchayat/edit/'.$panchayat_id));
			}
		}
		
		$data['blockList'] = $response_data;
		$data['panchayat_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Panchayat';
        $data['middle_content']	=	'panchayat/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update state Status
	 public function changeStatus(){
		 
		  // Permission Set Up
		$this->master_model->permission_access('12', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			$updateQuery = $this->master_model->updateRecord('panchayat_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('panchayat_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Panchayat Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/panchayat'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('panchayat_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('panchayat_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Panchayat Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/panchayat'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 
		// Permission Set Up
		$this->master_model->permission_access('12', 'delete');
		$id 	= base64_decode($id);	
		//echo $id ; die;	 
		$updateQuery = $this->master_model->updateRecord('panchayat_master', array('is_deleted'=>'1', 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')), array('panchayat_id' => $id));
		//echo $updateQuery; die;
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Panchayat Master', 'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Panchayat successfully deleted');
		 redirect(base_url('xAdmin/panchayat'));	
		 
	 }


}