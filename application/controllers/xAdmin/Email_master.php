<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class  : Email Template
Author : Vicky K
*/

class Email_master extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');		
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

    public function index()
    {
		$response_data = $this->master_model->getRecords("email_master");
		
    	$data['records'] = $response_data; 
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Email';	
    	$data['middle_content']='email-template/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

    public function add()
    {
		 // Check Validation
		$this->form_validation->set_rules('email_subject', 'Email Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('from_email', 'Email ID', 'required|xss_clean');
		//$this->form_validation->set_rules('cc_email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('email_body', 'Email Description', 'required|xss_clean');
		$this->form_validation->set_rules('slug', 'Slug', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$email_title 	= trim($this->input->post('email_title'));
			$from_email		= trim($this->input->post('from_email'));
			$slug 			= trim($this->input->post('slug'));
			$email_desc  	= trim($this->input->post('email_description'));
			
			$insertArr = array( 'slug' => $slug, 'email_subject' => $email_title, 'email_body' => $email_desc,  'from_email' => $from_email);			
			$insertQuery = $this->master_model->insertRecord('email_master',$insertArr);
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Email Template',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Email content successfully created');
				redirect(base_url('xAdmin/email_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/email_master/add'));
			}
		}
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Email';
        $data['middle_content']='email-template/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function edit($id)
    {
		$email_data = $this->master_model->getRecords("email_master", array('id' => $id));
		
        // Check Validation
		$this->form_validation->set_rules('email_title', 'Email Title', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('from_email', 'Email ID', 'required|xss_clean');		
		$this->form_validation->set_rules('email_description', 'Email Description', 'required|xss_clean');
		
		if($this->form_validation->run())
		{	
			$email_title 	= trim($this->input->post('email_title'));
			$from_email		= trim($this->input->post('from_email'));
			$slug 			= trim($this->input->post('slug'));
			$email_desc  	= trim($this->input->post('email_description'));
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array( 'email_subject' => $email_title, 'email_body' => $email_desc,  'from_email' => $from_email);	
			
			$updateQuery = $this->master_model->updateRecord('email_master',$updateArr,array('id' => $id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Email Template',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Email content successfully updated');
				redirect(base_url('xAdmin/email_master'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/email_master/edit/'.$id));
			}
		}
		
		$data['email_data'] = $email_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Email';
        $data['middle_content']='email-template/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('email_template',array('status'=>$value),array('id' => $id));
			 
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Email Template',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/email_master'));	
			 
		 } else if($value == 'Block'){
			 
			$updateQuery = $this->master_model->updateRecord('email_template',array('status'=>$value),array('id' => $id)); 
			
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Email Template',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/email_master'));		
		 }
		 
	 }
	 
	 /*public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('cms',array('is_deleted'=>1),array('id' => $id));
		 $this->session->set_flashdata('success','CMS page successfully deleted');
		 redirect(base_url('xAdmin/cms'));	
		 
	 }*/


}