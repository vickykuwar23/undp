<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Survey Category
Author : Vicky K
*/

class Survey_category extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
    	
		// Permission Set Up
		$this->master_model->permission_access('2', 'view');
		$this->db->where('survey_category_master.is_deleted','0');
		$response_data = $this->master_model->getRecords("survey_category_master",'','',array('category_id'=>'DESC'));		
		$data['records'] 		= $response_data;		
		$data['module_name'] 	= 'Survey';
		$data['submodule_name'] = 'Category';	
    	$data['middle_content']	= 'survey-categories/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add Role
    public function add()
    {
		// Permission Set Up
		$this->master_model->permission_access('2', 'add');
        // Check Validation
		$this->form_validation->set_rules('name', 'Category Name', 'required|min_length[4]|xss_clean');	
		$this->form_validation->set_rules('restricted_draft_count', 'Restricted Draft Count', 'required|xss_clean');	
		if($this->form_validation->run())
		{	
			$category_name = trim($this->input->post('name'));	
			$restricted_draft_count = trim($this->input->post('restricted_draft_count'));
			$insertArr = array( 'category_name' => $category_name, 'restricted_draft_count' => $restricted_draft_count, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('survey_category_master',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
					             'action_name'=> "Add",
					             'module_name'=> 'Survey Category Master',
					             'store_data'=> $json_encode_data,
					             'ip_address'=> $ipAddr,
							 	 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Survey Category Name successfully created');
				redirect(base_url('xAdmin/survey_category'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/survey_category/add'));
			}
		}
		
		$data['module_name'] 	= 'Survey';
		$data['submodule_name'] = 'Category';	
    	$data['middle_content']	= 'survey-categories/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit Role 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('2', 'edit');
		$catid = base64_decode($id);	
		$rec = $this->master_model->getRecords("survey_category_master",array('category_id'=>$catid));
		$this->form_validation->set_rules('name', 'Category Name', 'required|xss_clean');
		$this->form_validation->set_rules('restricted_draft_count', 'Restricted Draft Count', 'required|xss_clean');
		if($this->form_validation->run())
		{	
		
			$category_name = trim($this->input->post('name'));	
			$restricted_draft_count = trim($this->input->post('restricted_draft_count'));
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('category_name' => $category_name, 'restricted_draft_count' => $restricted_draft_count, 'updated_by_id' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('survey_category_master',$updateArr,array('category_id' => $catid));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
					             'action_name'=> "Edit",
					             'module_name'=> 'Survey Category Master',
					             'store_data'=> $json_encode_data,
					             'ip_address'=> $ipAddr,
							 	 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Survey Category Name successfully updated');
				redirect(base_url('xAdmin/survey_category'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/survey_category/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Survey';
		$data['submodule_name'] = 	'Category';
        $data['middle_content']	=	'survey-categories/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update Role Status
	 public function changeStatus(){
		 
		 // Permission Set Up
		$this->master_model->permission_access('2', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('survey_category_master',array('status'=>$value),array('category_id' => $id));
			 
			  // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
				             'action_name'=> "Status",
				             'module_name'=> 'Survey Category Master',
				             'store_data'=> $json_encode_data,
				             'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/survey_category'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_category_master',array('status'=>$value),array('category_id' => $id)); 
			
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
							 'action_name'=> "Status",
							 'module_name'=> 'Survey Category Master',
							 'store_data'=> $json_encode_data,
							 'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey_category'));		
		 }
		 
	 }
	 
	 public function check_status(){

   		$where = array('category_id' => $_POST['category_id']);
   		$survey_data = $this->master_model->getRecords("survey_master", $where);
   		$str='';
   		if(sizeof(@$survey_data)>0){
   			$str.='Child records are present in Survey Master';
   		}

   		echo $str;

   	}

	 // Soft Delete Role
	 public function delete($id){
		 
		 // Permission Set Up
		$this->master_model->permission_access('2', 'delete');
		$id 	= base64_decode($id);		 
		$updateQuery = $this->master_model->updateRecord('survey_category_master',array('is_deleted'=>1),array('category_id' => $id));

		if($updateQuery > 0)
		{
		 	$where = array('category_id' => $id);
		 	$survey_data = $this->master_model->getRecords("survey_master",$where);
		 	if(sizeof(@$survey_data) > 0){
		 		$deleteArr = array("is_deleted" => 1);
				$updateQuery2 = $this->master_model->updateRecord('survey_master', $deleteArr, $where);
		 	}
			
		}

		 // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),
						 'action_name'=> "Delete",
						 'module_name'=> 'Survey Category Master',
						 'store_data'=> $json_encode_data,
						 'ip_address'=> $ipAddr,
						 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Survey Category successfully deleted');
		 redirect(base_url('xAdmin/survey_category'));	
		 
	 }


}