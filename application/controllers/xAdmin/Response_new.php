<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Response
Author : Priyanka Wadnere
*/
class Response extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'Question';
        $this->module_title       =  "Question";
        $this->module_view_folder =  "questions/";    
    }

	// Csv Listing
    /*public function index()
    {
		$this->db->select("*");
        $this->db->from('survey_master');       
        $query = $this->db->get();
        $result = $query->result();
        print_r($result);
		echo "innnnnnnnnnnnnnnnnnnnnnnnn";die();
   	 }*/
	 
	public function response_preview($survey_id)
	{

		// Permission Set Up
        //$this->master_model->permission_access('16', 'view');

     	//$survey_id = base64_decode($survey_id);

		$question_query = $this->db->query("SELECT q1.survey_id, (SELECT svy.title FROM survey_master svy WHERE svy.survey_id = q1.survey_id) survey_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.section_id = sec.section_id) section_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.sub_section_id = sec.section_id) sub_section_name, q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt,  q1.is_mandatory, sm.global_question_id, q1.if_summation, q1.if_file_required, q1.file_label
		    FROM survey_question_master q1 LEFT JOIN survey_section_questions sm ON q1.survey_id = sm.survey_id 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.question_id = sm.question_id
			AND q1.survey_id = ".$survey_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			GROUP BY q1.survey_id, sm.section_id, sm.sub_section_id, q1.question_id
			ORDER BY q1.survey_id, sm.section_id, sm.sub_section_id, q1.question_id ASC");

		$question_data = $question_query->result_array();

     	//$tree = $this->buildTree($question_data[0]['question_id']);
		$tree = array();
		$last_section_name = '';
		$last_sub_section_name = '';
		$last_question_id = '';
		$label_mandatory = ''; 

		$key_name = array();
	    $answer_value = array();


		$html = '<span style="color: blue; font: bold;"> Survey : &nbsp;&nbsp;'.@$question_data[0]['survey_name'].'</span>
          <hr>  ';

     	//$tree = $this->buildTree($question_data);
     	if(sizeof($question_data) >0){
     		foreach ($question_data as $key => $value) {
     			$sr = $key+1;

     			$html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';

     			if($last_section_name <> $value['section_name']) {
     				$html.= '<hr>
	     				<span style="color: green; font: bold;"> Section : &nbsp;&nbsp;'.$value['section_name'].'</span>
	                  	<hr>';

                    
	                $last_section_name = $value['section_name'];   	
	     			$last_sub_section_name = '';
     			}

     			if($last_sub_section_name <> $value['sub_section_name']) {
                	$html.= '<hr>
                		<span style="color: #aa42f5; font: bold;"> Sub Section : &nbsp;&nbsp;'.$value['sub_section_name'].'</span>
     				<hr>';

     				if($value['sub_section_name']!=''){
	     			$last_sub_section_name = $value['sub_section_name']; 
	     			}
     			}

     		
     			if($value['is_mandatory'] == 1) { $label_mandatory = '<font color="red">*</font>'; }

     			$html.='<label class="w-100" for= description">'.$value['question_id'].'----'.'Q.'.$sr.'&nbsp;&nbsp;'.ucfirst($value['question_text']).$label_mandatory.'</label>';

     			$response_query = $this->db->query("SELECT sr.key_name, sr.answer_value, sr.salultation_value, sr.file_name 
				FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
				WHERE srh.survey_id = ".$survey_id." 
				AND sr.question_id = ".$value['question_id']);

				$response_data = $response_query->result_array();

	 			if($value['option_cnt'] >0 ){
	                $where = array('question_id' => $value['question_id']);
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, op.validation_id, op.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt FROM survey_questions_options_master op WHERE op.question_id=".$value['question_id']);

	                $option_data = $option_query->result_array();

	             
	            }
	            else{
	            	$option_data = [];
	            }
	            //echo $value['global_question_id'];

	            $html_input = $this->get_html_tags($value['response_type_id'], $value['question_id'], $option_data, $value['salutation'], $value['if_summation'], $value['if_file_required'], $value['file_label'], $response_data);

	            $html.= $html_input;

	            foreach ($option_data as $option_key => $option_value) {
                	if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
                		if(@$response_data[0]['answer_value'] == $option_value['option']){
                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option']);

	                		if($option_dependant){
			     				$html.= $option_dependant;
			     			}
			     		}
                	}
                }

     			$child = $this->buildTree($survey_id, $value['question_id'], $sr);
     			//echo '<pre>';
     			if($child){
     				//$value['sub_question'] = $child;
     				$html.= $child;
     				//echo $html;
     			}

     		
     			$html.="</div>
                  	</div>
                </div>";
            }	

     	}

     
     	$this->data['html']       		= $html;
		$this->data['page_title']       = 'View '.$this->module_title;
		$this->data['module_name']     	= $this->module_title; 
		$this->data['mode']     		= 'survey_view'; 
        $this->data['middle_content']   = $this->module_view_folder.'preview_response';
	
		$this->load->view('admin/admin_combo',$this->data);
	}

	public function get_dependant_question($survey_id, $option_id, $label_option){

		//error_reporting(0);
		$option_dependant_html = '';

		$question_query = $this->db->query("SELECT q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory, q1.salutation, q1.if_summation, q1.if_file_required, q1.file_label, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE optn.validation_id = v1.validation_id) validation_title, optn.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE optn.sub_validation_id = v2.sub_validation_id) sub_validation_title, optn.min_value, optn.max_value, optn.validation_label, optn.display_order, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt
		FROM   survey_question_master q1  LEFT JOIN survey_option_dependent od ON od.dependant_ques_id = q1.question_id LEFT JOIN survey_questions_options_master optn ON optn.question_id = q1.question_id
		WHERE  od.ques_option_id = ".$option_id."
		AND   q1.is_deleted = 0 
		AND   q1.status = 'Active'
		GROUP BY q1.question_id 
		ORDER BY q1.question_id ASC");
		 //echo $this->db->last_query();
		$question_data = $question_query->result_array();
		$label_mandatory = ''; 
		if(isset($question_data) && sizeof($question_data) >0){

			foreach ($question_data as $question) {
				$option_dependant_html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';

                if($question['is_mandatory'] == 1) { $label_mandatory = '<font color="red">*</font>'; }

     				$option_dependant_html.='<label class="w-100" for= description" style="color:#868b8f;">'.$question['question_id'].'Question for '.$label_option.' : &nbsp;'.'&nbsp;&nbsp;'.ucfirst($question['question_text']).$label_mandatory.'</label>';

     				$response_query = $this->db->query("SELECT sr.key_name, sr.answer_value, sr.salultation_value, sr.file_name 
					FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
					WHERE srh.survey_id = ".$survey_id." 
					AND sr.question_id = ".$question['question_id']);

					$response_data = $response_query->result_array();

					if($question['option_cnt'] >0 ){
						$option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title, 
							CASE WHEN op.sub_validation_id = 1 THEN '>' 
							     WHEN op.sub_validation_id = 2 THEN '>=' 
							     WHEN op.sub_validation_id = 3 THEN '<'
							     WHEN op.sub_validation_id = 4 THEN '<='
							     WHEN op.sub_validation_id = 5 THEN '=='
							     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition, 
							op.min_value, op.max_value, op.validation_label, op.dependant_ques_id, op.option, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$question['question_id']);

				        $option_data = $option_query->result_array();
				    }
				    else{
		            	$option_data = [];
		            }

	            $html_tag = $this->get_html_tags($question['response_type_id'], $question['question_id'], $option_data, $question['salutation'], $question['if_summation'], $question['if_file_required'], $question['file_label'], $response_data);

	 			$option_dependant_html.= $html_tag;

	 			foreach ($option_data as $option_key => $option_value) {
                	if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
	                	foreach (@$response_data as $resp_key => $resp_value) {
	                		if(@$resp_value['answer_value'] == $option_value['option']){
		                		$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option']);

		                		if($option_dependant){
				     				$option_dependant_html.= $option_dependant;
				     			}
				     		}
				     	}
                	}
                }

                $option_dependant_html.="</div>
                  	</div>
                </div>";

 			}
 			
 			//echo json_encode($question_array);

     	}
		return $option_dependant_html;
	}

	public function get_html_tags($tag_name, $question_id, $option_array, $salutation, $if_summation, $if_file_required, $file_label, $response_data){
		//echo $question_id.'--------'.$dependant_cnt.'</br>';
		$str = '';
		$length = '';
		$salutation_flag =0;

		/*$str.='@@@@@'.$question_id;
		$str.='*****'.json_encode($key_name);
		$str.='+++++'.json_encode($response_data);*/

		if($salutation!='' && $salutation!='0'){
			//$str.='-----'.$response_data[0]["salultation_value"];
			$str.= '</div> </div>';
			$salutation_flag =1;
			$salutation_array = explode('|', $salutation);
			if(sizeof($salutation_array)>0){
				$str.= '<div class="col-3"> <div class="form-group">';
				$str.= '<select class="form-control salutation_class">
		                    <option value="">Select Salutation</option>';
				foreach ($salutation_array as $id) {
					$selected = "";
					if(@$response_data[0]["salultation_value"] == $id) 
					{  
						$selected =  "selected='selected'"; 
					}
					
					$str.= '<option value="'.$id.'"  '.$selected.'>'.$id.'</option>';	
				}
				$str.= '</select>';
				$str.= '</div> </div>';
			}
			$str.='<div class="col-12"> <div class="form-group">';
		}

		if($tag_name == 'radio'){
			if(sizeof($option_array) >0){
				//$str.='</br>';
				//$str.='---'.$response_data[0]['answer_value'];
				foreach ($option_array as $key => $value) {
					if($value['option'] == @$response_data[0]['answer_value']){
						$checked =  "checked='checked'"; 
					}
					else{
						$checked='';
					}
					//$str.='</br>';
					$str.='<div class="form-check form-check-inline">';
					$str.= '<input class="form-check-input dependantcls" type="radio" id="'.$question_id.'" data-id="'.$value['ques_option_id'].'" name="radio_'.$question_id.'" value="'.$value['option'].'" '.$checked.'> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div>';	
				}
			}	

			foreach ($option_array as $key => $value) {
				$str.= '</div>
		                </div>
		                  <div class="col-6 radio_dependant_div_'.$question_id.'_'.$value['ques_option_id'].'" style="display:none;">';
				if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

					for ($i=0; $i < $value['dependant_cnt']; $i++) { 
						$j = $i+1;    
		                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
		               
		           }
		          
	           }
	            $str.='</div>';
		           $str.=' <div class="col-12">
		               <div class="form-group">';
           }
           $str.='<input type="hidden" id="prev_radio_'.$question_id.'" value="">';
		}
		elseif($tag_name == 'checkbox'){
			$str1 = '';
			if(sizeof($option_array) >0){
				//$str.='</br>';
				foreach ($option_array as $key => $value) {

					$checked ='';
					foreach (@$response_data as $resp_key => $resp_value) {
						if($value['option'] == $resp_value['answer_value']){
							$checked =  "checked='checked'"; 
						}
					}
					
					$str.='<div class="form-check" style="clear:left;">';
					$str.= '<input class="form-check-input dependantcls " type="checkbox" id="'.$value['ques_option_id'].'" data-id="'.$question_id.'" name="chk[]" value="'.$value['option'].'" '.$checked.'> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div>';


					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){
						$str.='<span class="checkbox_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
							$str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
						}
						$str.='</span>';
					}
						
				}
			}
		}
		elseif($tag_name == 'single_select'){

			$str.= '<select class="form-control dependantcls" id="'.$question_id.'">
	                    <option value="">Select Option Type</option>';

	        if(sizeof($option_array) >0){
		        foreach ($option_array as $key => $value) {
		        	$selected = "";
		        	if($value['option'] == @$response_data[0]['answer_value']){
						$selected =  "selected='selected'"; 
					}

					$str.= '<option value="'.$value['option'].'" id="'.$value['question_id'].'" data-id="'.$value['ques_option_id'].'" '.$selected.'>'.$value['option'].'</option>';
				}
			}
			$str.= '</select>';

			if(sizeof($option_array) >0){
				foreach ($option_array as $key => $value) {
					$str.= '</div>
			                </div>
			                  <div class="col-6 dropdown_dependant_div_'.$question_id.'_'.$value['ques_option_id'].'" style="display:none;">';
					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			          
		           }
		            $str.='</div>';
			           $str.=' <div class="col-12">
			               <div class="form-group">';
	           }
			}

			$str.='<input type="hidden" id="prev_select_'.$question_id.'" value="">';
		}
		elseif($tag_name == 'textbox'){
			//$str.=$salutation_flag;
			if(sizeof($option_array) >1){

				//if($salutation_flag = 0){
					$str.= '</div> </div>';
				//}
				$str.= '<div class="row mx-0">';
				foreach ($option_array as $key => $value) {
					//if($salutation_flag = 0){
					 $str.= '<div class="col-6"> <div class="form-group">';
					//}

					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{

						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}

					if($value['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					//$str.='a';

					//$str.='*****'.print_r($response_data);
					
					$textfield_value ='';
					$sum = 0;
					foreach (@$response_data as $resp_key => $resp_value) {
						if($value['option'] == $resp_value['key_name']){
							$textfield_value = $resp_value['answer_value'];
						}
						if($if_summation == 'Yes'){
							$sum = $sum + $resp_value['answer_value'];
						}
					}

					$str.= "<input type='text' ".$length." id='".$question_id."' data-optn-id='".$value['ques_option_id']."'" ;
					$str.="data-id='[".json_encode($option_array[$key])."]'";
					$str.="placeholder='".$value['option']."' value='".$textfield_value."' " ;
					if($if_summation == 'Yes'){
						$str.="class='form-control dependantcls calculate_total textfields_".$question_id."' ".$call_function.">";
					}
					else{
						$str.="class='form-control dependantcls' ".$call_function.">";
					}
					//if($salutation_flag = 0){
						$str.= '</div> </div>';
					//}
				}
				//$str.='--'.$if_summation;
				if($if_summation == 'Yes'){
			    	$str.='<div class="col-6"> <div class="form-group">';
			    	$str.= "<input type='text' class='form-control total_cnt_class' id='total_cnt_class_".$question_id."'";
					$str.="placeholder='Total' readonly='readonly' value='".$sum."'>";
					$str.='</div></div>';
				}
				//if($salutation_flag = 0){
				    $str.= '</div>';
					$str.='<div class="col-12"> <div class="form-group">';
				//}

			}
			else{
				$textbox_text = '';
				if(@$response_data[0]['answer_value'] != ''){
					$textbox_text = @$response_data[0]['answer_value'];
				}
				
				//$str.='<div class="col-12"> <div class="form-group">';
				if(isset($option_array) && sizeof($option_array)>0){
					if($option_array[0]['validation_title'] == "Length" && $option_array[0]['min_value'] > 0)
					{

						if($option_array[0]['sub_validation_id'] == 18){
							$length =  'maxlength='.$option_array[0]['min_value'];
						}
						else if($option_array[0]['sub_validation_id'] == 23){
							$length =  'minlength='.$option_array[0]['min_value'];
						
						}	
					}

					if($option_array[0]['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					$str.= "<input type='text' class='form-control dependantcls' ".$length." id='".$question_id."'  data-optn-id='".$option_array[0]['ques_option_id']."' data-id='".json_encode($option_array)."' placeholder='Enter Answer' ".$call_function." value='".$textbox_text."'>";

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textbox_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			           $str.='</span>';
			          
		           }
				}
				
			}
			
			$str.='<span id="text_error_'.$question_id.'" style="color: red;"></span>';
			$str.='</br>';
			$str.='<span id="text_error_numonly_'.$question_id.'" style="color: red;"></span>';

			
		}
		elseif($tag_name == 'file'){
			$str.= '<input type="file" class="form-control" id="'.$question_id.'">';
		}
		elseif($tag_name == 'textarea'){
			//print_r($option_array);
			if(@$response_data[0]['answer_value'] != ''){
				$textarea_text = @$response_data[0]['answer_value'];
			}
			else{
				$textarea_text = '';
			}
			if(sizeof($option_array) > 0)
			{

				$str.= '<textarea class="form-control form-group textarea_dependantcls" id="'.$question_id.'" data-optn-id="'.$option_array[0]['ques_option_id'].'" 
					data-id="'.$option_array[0]['if_sub_question_checked'].'" rows="2" "'.$length.'" >'.$textarea_text.'</textarea>';
				foreach ($option_array as $key => $value) {
					
				
					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{
						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}
			         
					

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textarea_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';

			                $arr['if_sub_question_checked'] = $option_array[0]['if_sub_question_checked']; 
			               
			           }
			           $str.='</span>';
					
					}
				} 
			}
			else
			{
				$str.= '<textarea class="form-control form-group" id="'.$question_id.'" placeholder="Enter Answer" rows="2" /></textarea>';
			}
			
			//$str.= '<textarea class="form-control dependantcls" id="'.$question_id.'" placeholder="Enter Answer" rows="2"  /></textarea>';
		}

		if($if_file_required == 'Yes'){
			$str.= '<label>'.$file_label.'</label>';
			$str.= '<input type="file" class="form-control">';
		}

		//echo $str.'</br>';

		return $str;
	}

	public function buildTree($survey_id, $question_id, $sr){
		//error_reporting(E_ERROR | E_PARSE);
		//$tree = array();
		$branch = array();
		$sub_html = '';
		$html_input = '';
		$label_mandatory = '';

		$key_name = array();
	    $answer_value = array();

    	$sub_question_query = $this->db->query("SELECT  q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt, q1.is_mandatory, q1.if_summation, q1.if_file_required, q1.file_label FROM survey_question_master q1 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.parent_question_id = ".$question_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			ORDER BY q1.question_id ASC");
    	
    	//echo '<pre>';

    	$sub_question_data = $sub_question_query->result_array();
    	
		if (sizeof($sub_question_data)>0) {
			//echo 'if'.$question_id;
		   foreach ($sub_question_data as $id => $sub_question) { 

		   	$key_sr = $id+1;
		   	$sub_sr = 'Q.'.$sr.'.'.$key_sr;

		   	$sub_html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';

                if($sub_question['is_mandatory'] == 1) { $label_mandatory = '<font color="red">*</font>'; }

		   	    $sub_html.='<label for="description" class="w-100">'.$sub_sr.'&nbsp;&nbsp;'.ucfirst($sub_question['question_text']).$label_mandatory.'</label>';

		   	    $response_query = $this->db->query("SELECT sr.key_name, sr.answer_value, sr.salultation_value, sr.file_name 
				FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
				WHERE srh.survey_id = ".$survey_id." 
				AND sr.question_id = ".$sub_question['question_id']);

				$response_data = $response_query->result_array();
				//$response_data['answer_value'] = '';

	 			if($sub_question['option_cnt'] >0 ){
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$sub_question['question_id']);

	                $option_data = $option_query->result_array();
	                
	                //$option_data = $option_query->result_array();
	            }
	            else{
	            	$option_data = [];
	            }

	            $html_input = $this->get_html_tags($sub_question['response_type_id'], $sub_question['question_id'], $option_data, $sub_question['salutation'], $sub_question['if_summation'], $sub_question['if_file_required'], $sub_question['file_label'], $response_data);

	            $sub_html.= $html_input;

	            $children = $this->buildTree($survey_id, $sub_question['question_id'],$key_sr);

	          	if($children){
	          		//$sub_question['sub_question'] = $children;
	          		$sub_html.= $children;

	            }



	            //$branch[] = $sub_question;

	            $sub_html.="</div>
                  	</div>
                </div>";
	        }
	       
	    }
		   
		//print_r($branch);
	    //return $branch;
	    //echo $sub_html;
	    return $sub_html;
		
	}


} // End Controller 