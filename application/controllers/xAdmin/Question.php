<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Question
Author : Priyanka Wadnere
*/
class Question extends CI_Controller 
{
	function __construct() {
			
        parent::__construct();

		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		
		$this->user_id            	=  $this->session->userdata('user_id');
        $this->ip                 	=  $this->input->ip_address();
        $this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'Question';
        $this->module_title       =  "Question";
        $this->module_view_folder =  "questions/";    
    }

    public function index()
    {	

    	// Permission Set Up
        $this->master_model->permission_access('16', 'view');

    	$where = array('survey_question_master.is_deleted' => 0);
    	$this->db->order_by('question_id','DESC');
        $this->db->join('survey_input_category_master','survey_input_category_master.input_type  = survey_question_master.response_type_id');

    	$question_list = $this->master_model->getRecords("survey_question_master", $where, "survey_question_master.survey_id, survey_question_master.question_id, parent_question_id, question_text, 
    		survey_input_category_master.input_category response_type_id, CASE is_mandatory when 1 then 'Yes' else 'No' END AS mandatory, survey_question_master.display_order, (SELECT COUNT(1) FROM survey_questions_options_master where survey_questions_options_master.question_id = survey_question_master.question_id) cnt");	 

		$this->db->where('is_deleted',0);	
    	$survey_list = $this->master_model->getRecords("survey_master");

    	$this->data['survey_list']		= $survey_list;
    	$this->data['question_list']	= $question_list;
    	$this->data['page_title']       = $this->module_title.' List';
		$this->data['module_title']     = $this->module_title;
		$this->data['middle_content']   = $this->module_view_folder."question_list";

		$this->load->view('admin/admin_combo',$this->data);
   	}


   	public function get_datatable()
	{	
		$survey_id = $_POST['survey_id'];
		
		if($survey_id != ''){
			$where = array('survey_question_master.is_deleted' => 0, 'survey_question_master.survey_id' => $survey_id);
			//$this->db->where('survey_question_master.survey_id', $survey_id);
		}
		else{
			$where = array('survey_question_master.is_deleted' => 0);
		}

    	$this->db->order_by('question_id','DESC');
        $this->db->join('survey_input_category_master','survey_input_category_master.input_type  = survey_question_master.response_type_id');

    	$question_list = $this->master_model->getRecords("survey_question_master", $where, "survey_question_master.survey_id, survey_question_master.question_id, parent_question_id, question_text, 
    		survey_input_category_master.input_category response_type_id, CASE is_mandatory when 1 then 'Yes' else 'No' END AS mandatory, survey_question_master.display_order, (SELECT COUNT(1) FROM survey_questions_options_master where survey_questions_options_master.question_id = survey_question_master.question_id) cnt");	

    	if(sizeof($question_list) > 0){
			$resstr = json_encode($question_list);
		}
		else{
			$resstr = '';
		}
		echo $resstr; 

	}

   	public function add(){

        //error_reporting(0);

        // Permission Set Up
        $this->master_model->permission_access('16', 'add');
        $this->session->set_userdata('prev_survey_id', '');
        if(isset($_POST['submit']) || isset($_POST['save']) || isset($_POST['save_preview'])){
			
			//echo "<pre>";print_r($_POST);die();

			$if_requiredParent = $this->input->post('if_parent_required');
			if($if_requiredParent == 1)
			{
				$p_question_id = $this->input->post('parent_question');
			}
			else 
			{
				$p_question_id = 0;
			}	
			
			$if_file_required = $this->input->post('if_file_required');
			if($if_file_required == 1)
			{
				$file_required = 'Yes';
				$file_label = $_POST['file_label'];
			}
			else 
			{
				$file_required = 'No';
				$file_label = '';
			}

			if(isset($_POST['if_summation'])){
				$if_summation = $_POST['if_summation'];
			}
            else 
			{
				$if_summation = 'No';
			}	
			
			if(isset($_POST['question_labels'])){
				$moreThanone = $_POST['question_labels'];
			}
            else 
			{
				$moreThanone = 'No';
			}



			if(isset($_POST['salutation_box'])){
				$salutation = trim($this->input->post('salutation_input'));
				$salutation_arr = explode('|', $salutation);
				$salutation_str='';
				foreach ($salutation_arr as $key => $value) {
					if($value != ''){
						$salutation_str.= $value.'|';
					}
				}
				$salutation_add = substr($salutation_str,0,-1);
			}
			else{
				$salutation_add = '';
			}

			if(isset($_POST['input_file_required'])){
				$input_file_required = $_POST['input_file_required'];
			}
            else 
			{
				$input_file_required = 'No';
			}	
			
			
			
        	$option_type = trim($this->input->post('option_type'));			
            $dataArr = array( 'question_text'	    => trim($this->input->post('question_text')),
							  'parent_question_id'  => $p_question_id,
							  'survey_id'  			=> trim($this->input->post('survey')),
							  'response_type_id'	=> trim($this->input->post('option_type')),
							  'salutation'			=> $salutation_add,
							  'if_file_required'	=> $file_required,
							  'file_label'			=> $file_label,
							  'help_label'			=> trim($this->input->post('help_label')),
							  'if_summation'		=> $if_summation,
							  'input_more_than_1'	=> $moreThanone,
							  'input_file_required'	=> $input_file_required,
							  'is_mandatory'	    => trim($this->input->post('is_mandatory')),
							  'created_by_id'       => $this->session->userdata('user_id')
							);

			$insertQuery = $this->master_model->insertRecord('survey_question_master',$dataArr);

			if($insertQuery > 0){
				
				$last_insert_id_question_tbl =  $this->db->insert_id();

				if($option_type == 'radio' || $option_type == 'checkbox' || $option_type == 'single_select'){
					
						if(isset($_POST['title']))
						{
							$titleArr = array_filter($_POST['title']);
						}						
						if(isset($_POST['option_display_order']))
						{
							$display_orderArr = $_POST['option_display_order'];
						}						
						if(isset($_POST['parent_qus_chk']))
						{
							$sub_question_id_Arr = $_POST['parent_qus_chk'];
						}	
					
				}
				else
				{
					
					if(isset($_POST['text_type'])){
						$text_type = $_POST['text_type'];
					}
					if(isset($_POST['condition_type'])){
						$condition_type = $_POST['condition_type'];
					}
					if(isset($_POST['min'])){
						$min = $_POST['min'];
					}
					
					if(isset($_POST['custom_error'])){
						$custom_error = $_POST['custom_error'];
					}
					
					if(isset($_POST['if_logical'])){
						$if_logical = $_POST['if_logical'];
						$if_sub_question_checked = 'Yes';
						
						if(isset($_POST['parent_qus'])){
							$sub_question_id = $_POST['parent_qus'];
						}
					}
					else{
						$if_sub_question_checked = 'No';
					}
					if(isset($_POST['max'])){
						$max = $_POST['max'];
					}
					else{
						$max = '';
					}
					
					if(isset($_POST['question_labels']))
					{
						
						$question_labels = $_POST['question_labels'];
						
						
						if(isset($_POST['text_label'])){
							$labelArr = array_filter($_POST['text_label']);
						}
						
						if(isset($_POST['label_display_order'])){
							$label_display_order = $_POST['label_display_order'];
						}
						
						if(isset($_POST['min_val'])){
							$min_val = $_POST['min_val'];
						}
						
						if(isset($_POST['max_val'])){
							$max_val = $_POST['max_val'];
						}
						
						if(isset($_POST['condition_type_val'])){
							$condition_type_val = $_POST['condition_type_val'];
						}
						
						if(isset($_POST['text_type_val'])){
							$text_type = $_POST['text_type_val'];
						}
						
						if(isset($_POST['custom_error_val'])){
							$custom_error_val = $_POST['custom_error_val'];
						}
						
						
						if(sizeof($labelArr) > 0)
						{
							
							$i = 0;
							foreach ($labelArr as $key => $value) 
							{		
								$clean_string = str_replace("'","",$labelArr[$key]);
								$insertdtl_data = array(
									'question_id'    	=> $last_insert_id_question_tbl,
									'option'         	=> $clean_string,
									'validation_id'  	=> $text_type,
									'sub_validation_id '=> $condition_type_val,
									'min_value'			=> $min_val,
									'max_value' 		=> $max_val,
									'validation_label'  => $custom_error_val,
									'display_order'  	=> $label_display_order[$key],
									'created_by_id'  	=> $this->session->userdata('user_id'),
								);
								
								$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);
								$last_insert_id_option_table =  $this->db->insert_id();
								
								
							}//parent Question option foreach End 
							
						}// check sizeof
						
					} // End Label 
					else{

					
						$insertdtl_data = array(
							'question_id'    	=> $last_insert_id_question_tbl,
							'validation_id'  	=> $text_type,
							'sub_validation_id '=> $condition_type,
							'min_value'			=> $min,
							'max_value' 		=> $max,
							'validation_label'  => $custom_error,
							'if_sub_question_checked' => $if_sub_question_checked,
							'created_by_id'     => $this->session->userdata('user_id'),
						);
						
						$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);
						$last_insert_id_option_table =  $this->db->insert_id();
						
						
								
						// ShortText|Paragraph
						if(isset($_POST['parent_qus']))
						{
							$parent_qus = $_POST['parent_qus'];
							if(sizeof($parent_qus) > 0)
							{
								foreach ($parent_qus as $optionkey => $optvalue) 
								{
									$option_dependent_data = array(
												'question_id'			=> $last_insert_id_question_tbl,
												'ques_option_id'        => $last_insert_id_option_table,
												'dependant_ques_id'  	=> $parent_qus[$optionkey],
												'created_by_id'			=> $this->session->userdata('user_id')
											);
										$optionDependentSQL = $this->master_model->insertRecord("survey_option_dependent",$option_dependent_data);
										$optionDependentInsertID =  $this->db->insert_id();
										
									// Update Array For Sub Question To Add Parent											
									$updateParentArr 	= array("parent_question_id" => $last_insert_id_question_tbl);
									$updateWhere 	 	= array('question_id' => $parent_qus[$optionkey]);								
									//$upd_subQues_Query 	= $this->master_model->updateRecord('survey_question_master',$updateParentArr,$updateWhere);
							
								}
							}
						}
													
					}	
					
				} // Else End
				
				if(isset($titleArr) && sizeof($titleArr) > 0){
					
					$i = 0;
					foreach ($titleArr as $key => $value) {
						$clean_string_1 = str_replace("'","",$titleArr[$key]);
						$insertdtl_data = array(
							'question_id'    => $last_insert_id_question_tbl,
							'option'         => $clean_string_1,
							'display_order'  => $display_orderArr[$key],
							'created_by_id'  => $this->session->userdata('user_id')
						);
						
						$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);
						$last_insert_id_option_table =  $this->db->insert_id();
						
						if($_POST['sub_q'][$key] == 1)
						{							
							$dependentQuesion = array('if_sub_question_checked' => 'Yes');				
							// Update SQL Fire as per option dependent Question															
							$where1 = array('ques_option_id' => $last_insert_id_option_table);								
							$upd_subQues_Query = $this->master_model->updateRecord('survey_questions_options_master',$dependentQuesion,$where1);
							
							// Radio|Checkbox|SelectBox
							if(isset($_POST['parent_qus_chk'])){
								$option_dependent_selected_question = $_POST['parent_qus_chk'][$key];
								if(sizeof($option_dependent_selected_question) > 0)
								{
									foreach ($option_dependent_selected_question as $optionkey => $optvalue) 
									{
										$option_dependent_data = array(
												'question_id'			=> $last_insert_id_question_tbl,
												'ques_option_id'        => $last_insert_id_option_table,
												'dependant_ques_id'  	=> $option_dependent_selected_question[$optionkey],
												'created_by_id'			=> $this->session->userdata('user_id')
											);
											$optionDependentSQL = $this->master_model->insertRecord("survey_option_dependent",$option_dependent_data);
											$optionDependentInsertID =  $this->db->insert_id();
											
											// Update Array For Sub Question To Add Parent											
											$updateParentArr 	= array("parent_question_id" => $last_insert_id_question_tbl);
											$updateWhere 	 	= array('question_id' => $option_dependent_selected_question[$optionkey]);					
											//$upd_subQues_Query 	= $this->master_model->updateRecord('survey_question_master',$updateParentArr,$updateWhere);
									
									}
								}									
							}							
						}
						
					}//parent Question option foreach End 
					
				}// check sizeof 

				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id'   => $this->session->userdata('user_id'),
								'action_name'=> "Add",
								'module_name'=> 'Question Master',
								'store_data' => $json_encode_data,
								'ip_address' => $ipAddr,
							 	'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}

				//echo "<pre>";print_r($_POST);die();
				$this->session->set_flashdata('success','Question successfully created');
				if(isset($_POST['save'])){
					$this->session->set_userdata('prev_survey_id', $_POST['survey']);
					redirect(base_url('xAdmin/question/add'));
				}
				else if(isset($_POST['save_preview'])){
					$this->session->set_userdata('prev_survey_id', $_POST['survey']);
					redirect(base_url('xAdmin/question/preview/'.$last_insert_id_question_tbl.'/add'));
				}
				else{
					$this->session->set_userdata('prev_survey_id', '');
					redirect(base_url('xAdmin/question'));
				}
				
			}//check parent question inserted or not
			
			
        }

        $where = array('status' => 'Active', 'is_deleted' => 0);

		$survey_data = $this->master_model->getRecords("survey_master", array('is_deleted' => 0),'', array('survey_id' => 'DESC'));	
		$option_type = $this->master_model->getRecords("survey_input_category_master");	

		$text_type = $this->master_model->getRecords("survey_response_validation_master", $where);	
		$condition_type = $this->master_model->getRecords("survey_response_validation_subtype_master",$where);	
        $data['action']      	    = 'add';

        $data['survey_data']    	= $survey_data;
        $data['option_type']    	= $option_type;
        $data['question_id']    	= '';
        $data['text_type']        	= $text_type;
        $data['condition_type']   	= $condition_type;
        $data['module_name']     	= 'Survey';
        $data['submodule_name'] 	= 'Question';   
        $data['middle_content']     = 'questions/add';
        $this->load->view('admin/admin_combo',$data);
    }
	
	public function edit($question_id){
		//error_reporting(0);
    	// Permission Set Up
        $this->master_model->permission_access('16', 'edit');

    	$question_id = base64_decode($question_id);
       
        if(isset($_POST['submit']) || isset($_POST['save_preview']))
		{
			//echo "<pre>";print_r($this->input->post());die();
        	$option_type = trim($this->input->post('option_type'));
			$if_requiredParent = $this->input->post('if_parent_required');
			if($if_requiredParent == 1)
			{
				$p_question_id = $this->input->post('parent_question');
			}
			else 
			{
				$p_question_id = 0;
			}	
			
			$if_file_required = $this->input->post('if_file_required');
			if($if_file_required == 1)
			{
				$file_required = 'Yes';
				$file_label = $_POST['file_label'];
			}
			else 
			{
				$file_required = 'No';
				$file_label = '';
			}

			if(isset($_POST['if_summation'])){
				$if_summation = $_POST['if_summation'];
			}
            else 
			{
				$if_summation = 'No';
			}	
			
			if(isset($_POST['question_labels'])){
				$moreThanone = $_POST['question_labels'];
			}
            else 
			{
				$moreThanone = 'No';
			}	
			
			if(isset($_POST['salutation_box'])){
				$salutation = trim($this->input->post('salutation_input'));
				$salutation_arr = explode('|', $salutation);
				$salutation_str='';
				foreach ($salutation_arr as $key => $value) {
					if($value != ''){
						$salutation_str.= $value.'|';
					}
				}
				$salutation_add = substr($salutation_str,0,-1);
			}
			else{
				$salutation_add = '';
			}
			
			if(isset($_POST['input_file_required'])){
				$input_file_required = $_POST['input_file_required'];
			}
            else 
			{
				$input_file_required = 'No';
			}	

			$survey_id = trim($this->input->post('survey_id_hidden'));
			if($survey_id == '' || $survey_id == 0){
				$survey_id = trim($this->input->post('survey'));
			}

            $dataArr = array( 'question_text'	    => trim($this->input->post('question_text')),
							  'parent_question_id'  => $p_question_id,
							  //'survey_id'  			=> trim($this->input->post('survey')),
							  'survey_id'  			=> $survey_id,
							  'response_type_id'	=> trim($this->input->post('option_type')),
							  'salutation'			=> $salutation_add,
							  'if_file_required'	=> $file_required,
							  'file_label'			=> $file_label,
							  'help_label'			=> trim($this->input->post('help_label')),
							  'if_summation'		=> $if_summation,
							  'input_more_than_1'	=> $moreThanone,
							  'input_file_required'	=> $input_file_required,
							  'is_mandatory'	    => trim($this->input->post('is_mandatory')),
							  'updated_by_id'       => $this->session->userdata('user_id')
							);

			
			$where  = array('question_id' => $question_id);
			$updateQuery = $this->master_model->updateRecord('survey_question_master', $dataArr, $where);
			
			if($updateQuery > 0){
				
				if($option_type == 'radio' || $option_type == 'checkbox' || $option_type == 'single_select'){
					
					if(isset($_POST['title']))
					{
						$titleArr = array_filter($_POST['title']);
					}						
					if(isset($_POST['option_display_order']))
					{
						$display_orderArr = $_POST['option_display_order'];
					}					
				}
				else  //Textbox or Textarea
				{	
						
					if(isset($_POST['text_type'])){
						$text_type = $_POST['text_type'];    
					}
					if(isset($_POST['condition_type'])){
						$condition_type = $_POST['condition_type'];
					}
					if(isset($_POST['min'])){
						$min = $_POST['min'];
					}
					
					if(isset($_POST['custom_error'])){
						$custom_error = $_POST['custom_error'];
					}

					if(isset($_POST['if_logical'])){
						$if_logical = $_POST['if_logical'];
						$if_sub_question_checked = 'Yes';
						
						if(isset($_POST['parent_qus'])){
							$sub_question_id = $_POST['parent_qus'];
						}
					}
					else{
						$if_sub_question_checked = 'No';
					}
					if(isset($_POST['max'])){
						$max = $_POST['max'];
					}
					else{
						$max = '';
					}
					
					
					if(isset($_POST['question_labels']))
					{
						
						$question_labels = $_POST['question_labels'];
						
						
						if(isset($_POST['text_label'])){
							$labelArr = array_filter($_POST['text_label']);
						}
						
						if(isset($_POST['label_display_order'])){
							$label_display_order = $_POST['label_display_order'];
						}
						
						if(isset($_POST['min_val'])){
							$min_val = $_POST['min_val'];
						}
						
						if(isset($_POST['max_val'])){
							$max_val = $_POST['max_val'];
						}
						
						if(isset($_POST['condition_type_val'])){
							$condition_type_val = $_POST['condition_type_val'];
						}
						
						if(isset($_POST['text_type_val'])){
							$text_type = $_POST['text_type_val'];
						}
						
						if(isset($_POST['custom_error_val'])){
							$custom_error_val = $_POST['custom_error_val'];
						}
						
						
						if(sizeof(@$labelArr) > 0)
						{
							$option_record = $this->master_model->getRecords('survey_questions_options_master', array('question_id' => $question_id));
							if(sizeof(@$option_record) >0 ){
								$delete_record = $this->master_model->deleteRecord('survey_questions_options_master', 'question_id', $question_id);
							}
							$i = 0;
							foreach ($labelArr as $key => $value) 
							{		
								$clean_string = str_replace("'","",$labelArr[$key]);
								$insertdtl_data = array(
									'question_id'    	=> $question_id,
									'option'         	=> $clean_string,
									'validation_id'  	=> $text_type,
									'sub_validation_id '=> $condition_type_val,
									'min_value'			=> $min_val,
									'max_value'			=> $max_val,
									'validation_label'  => $custom_error_val,
									'display_order'  	=> $label_display_order[$key],
									'created_by_id'  	=> $this->session->userdata('user_id'),
								);
								
						
								$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);
								$last_insert_id_option_table =  $this->db->insert_id();
								//echo ">>>".$this->db->last_query();
							}//parent Question option foreach End 
							
						}// check sizeof
						
					} // End Label 
					else{
				//die();
						//echo 'else'; 
						$insertdtl_data = array(
							'question_id'    	=> $question_id,
							'validation_id'  	=> $text_type,
							'sub_validation_id '=> $condition_type,
							'min_value'			=> $min,
							'max_value'			=> $max,
							'if_sub_question_checked' => $if_sub_question_checked,
							'validation_label'  => $custom_error,
							'created_by_id'     => $this->session->userdata('user_id'),
						);

						$option_record = $this->master_model->getRecords('survey_questions_options_master', array('question_id' => $question_id));
							if(sizeof(@$option_record) >0 ){
								$delete_record = $this->master_model->deleteRecord('survey_questions_options_master', 'question_id', $question_id);
							}
						
						if(($text_type!="" && $condition_type!="") || isset($_POST['if_logical']))
						{
							
							$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);

							$last_insert_id_option_table =  $this->db->insert_id();
								
							// ShortText|Paragraph
							if(isset($_POST['parent_qus']))
							{
								$parent_qus = $_POST['parent_qus'];
								if(sizeof($parent_qus) > 0)
								{
									$delete_record = $this->master_model->deleteRecord('survey_option_dependent', 'question_id', $question_id);
									foreach ($parent_qus as $optionkey => $optvalue) 
									{
										$option_dependent_data = array(
													'question_id'			=> $question_id,
													'ques_option_id'        => $last_insert_id_option_table,
													'dependant_ques_id'  	=> $parent_qus[$optionkey],
													'created_by_id'			=> $this->session->userdata('user_id')
												);
											$optionDependentSQL = $this->master_model->insertRecord("survey_option_dependent",$option_dependent_data);
											$optionDependentInsertID =  $this->db->insert_id();
											
											// Update Array For Sub Question To Add Parent											
											$updateParentArr 	= array("parent_question_id" => $question_id);
											$updateWhere 	 	= array('question_id' => $parent_qus[$optionkey]);								
											//$upd_subQues_Query 	= $this->master_model->updateRecord('survey_question_master',$updateParentArr,$updateWhere);
									
									}
								}
							}
													
						}
					}
					
				}
				
				if(isset($titleArr) && sizeof($titleArr) > 0)
				{
					
					$option_record = $this->master_model->getRecords('survey_questions_options_master', array('question_id' => $question_id));
					if(sizeof(@$option_record) >0 ){
						$delete_record = $this->master_model->deleteRecord('survey_questions_options_master', 'question_id', $question_id);
						$option_dependant_record = $this->master_model->getRecords('survey_option_dependent', array('question_id' => $question_id));
						if(sizeof(@$option_dependant_record) >0 ){
							$delete_record2 = $this->master_model->deleteRecord('survey_option_dependent', 'question_id', $question_id);
						}
					}
					
					$i = 0;
					foreach ($titleArr as $key => $value) 
					{
						$clean_string_1 = str_replace("'","",$titleArr[$key]);
						$insertdtl_data = array(
							'question_id'    => $question_id,
							'option'         => $clean_string_1,
							'display_order'  => $display_orderArr[$key],
							'updated_by_id'  => $this->session->userdata('user_id'),
						);
						
						$insertDtlQuery = $this->master_model->insertRecord("survey_questions_options_master",$insertdtl_data);
						$last_insert_id_option_table =  $this->db->insert_id();
						
						if($_POST['sub_q'][$key] == 1)
						{
							
							$dependentQuesion = array('if_sub_question_checked' => 'Yes');				
							// Update SQL Fire as per option dependent Question															
							$where1 = array('ques_option_id' => $last_insert_id_option_table);								
							$upd_subQues_Query = $this->master_model->updateRecord('survey_questions_options_master',$dependentQuesion,$where1);
							
							// Radio|Checkbox|SelectBox
							if(isset($_POST['parent_qus_chk'][$key]))
							{
								
								$option_dependent_selected_question = $_POST['parent_qus_chk'][$key];
								//echo "<pre>";print_r($option_dependent_selected_question);
								if(sizeof($option_dependent_selected_question) > 0)
								{
									//$delete_record = $this->master_model->deleteRecord('survey_option_dependent', 'question_id', $question_id);
									foreach ($option_dependent_selected_question as $optionkey => $optvalue) 
									{
											$option_dependent_data = array(
												'question_id'			=> $question_id,
												'ques_option_id'        => $last_insert_id_option_table,
												'dependant_ques_id'  	=> $option_dependent_selected_question[$optionkey],
												'created_by_id'			=> $this->session->userdata('user_id')
											);
											$optionDependentSQL = $this->master_model->insertRecord("survey_option_dependent",$option_dependent_data);
											$optionDependentInsertID =  $this->db->insert_id();
											
											// Update Array For Sub Question To Add Parent											
											$updateParentArr 	= array("parent_question_id" => $question_id);
											$updateWhere 	 	= array('question_id' => $option_dependent_selected_question[$optionkey]);					
											//$upd_subQues_Query 	= $this->master_model->updateRecord('survey_question_master',$updateParentArr,$updateWhere);
									
									}
								}									
							}						
						}
						
					}//parent Question option foreach End 
					
				}// check sizeof 

				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id'   => $this->session->userdata('user_id'),
								'action_name'=> "Edit",
								'module_name'=> 'Question Master',
								'store_data' => $json_encode_data,
								'ip_address' => $ipAddr,
							 	'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));
				
				$this->session->set_flashdata('success','Question successfully Updated');
				if(isset($_POST['save_preview'])){
					redirect(base_url('xAdmin/question/preview/'.$question_id.'/edit'));
				}
				else{
					redirect(base_url('xAdmin/question'));
				}
				
			}
        }



		$this->db->where('question_id',$question_id);
		$question_data = $this->master_model->getRecords("survey_question_master");	

		$option_query = $this->db->query("SELECT op.*, (SELECT COUNT(1) FROM survey_option_dependent od WHERE od.ques_option_id = op.ques_option_id AND od.is_deleted=0) dependant_cnt 
			FROM survey_questions_options_master op
			WHERE op.question_id = ".$question_id."
			AND   op.is_deleted = 0");

		//echo $this->db->last_query();
		
		$option_data = $option_query->result_array();	

		//print_r($option_data); die;

		/*$last_display_order = $this->master_model->getRecords("survey_questions_options_master op", $where, 'max(op.display_order) last_display_order');*/

		$where2 = array('question_id'=>$question_id, 'is_deleted'=>0);
		$last_display_order = $this->master_model->getRecords("survey_questions_options_master", $where2);	

		$where3 = array('survey_id'=>$question_data[0]['survey_id'],'question_id' => $question_id);
   		$question_mapping_data = $this->master_model->getRecords("survey_section_questions", $where3);
		
		if(count($last_display_order) > 0)
		{
			$lastOrderNo = $last_display_order[0]['display_order'];
			//$lastOrderNo = $last_disp_ord+1;
		}
		else
		{
			$lastOrderNo = 0;
		}
			 
		$where = array('is_deleted' => 0); //'status' => 'Active', 
		$survey_data = $this->master_model->getRecords("survey_master", $where);	
		$option_type = $this->master_model->getRecords("survey_input_category_master");	

		$text_type = $this->master_model->getRecords("survey_response_validation_master", $where);	
		$condition_type = $this->master_model->getRecords("survey_response_validation_subtype_master",$where);

		$where = array('status' => 'Active', 'is_deleted' => 0, 'parent_question_id !=' => 0 );
		$sub_question = $this->master_model->getRecords("survey_question_master", $where);

		$data['question_id']	  = $question_id;
		$data['question_data']    = $question_data;
		$data['option_data']      = $option_data;	
		$data['sub_question']     = $sub_question;
		$data['question_mapping_data'] = $question_mapping_data;
		$data['action']      	  = 'edit';	
		
        $data['survey_data']    	= $survey_data;
        $data['option_type']    	= $option_type;
        //$data['last_display_order'] = $last_display_order[0]['last_display_order'];
		$data['last_display_order'] = $lastOrderNo;
        $data['text_type']        	= $text_type;
        $data['condition_type']   	= $condition_type;
        $data['module_name']     	= 'Survey';
        $data['submodule_name'] 	= 'Question';   
        $data['middle_content']     = 'questions/edit';
        
        $this->load->view('admin/admin_combo',$data);
        
    }

    public function remove_option(){
    	if($_POST['question_id']!='' && $_POST['option_id']!='')
    	$this->db->where('question_id', $_POST['question_id']);
        $this->db->where('ques_option_id', $_POST['option_id']);

        $res = $this->db->delete('survey_questions_options_master');

        echo $res;
    }

    public function preview($question_id, $mode){

    	//$question_id = base64_decode($question_id);

    	$where = array('q1.question_id' => $question_id, 'q1.status' => 'Active', 'q1.is_deleted' => 0);
		$question_data = $this->master_model->getRecords("survey_question_master q1", $where, "q1.survey_id, (SELECT svy.title FROM survey_master svy WHERE svy.survey_id = q1.survey_id AND svy.is_deleted=0) survey_name, q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation,  (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id AND o1.is_deleted=0) option_cnt,  q1.is_mandatory, q1.if_summation, q1.if_file_required, q1.file_label, q1.input_file_required");	
		//echo $this->last_query();

		$label_mandatory = ''; 
		
		$html = '<span style="color: blue; font: bold;"> Survey : &nbsp;&nbsp;'.@$question_data[0]['survey_name'].'</span>
          <hr>  ';

		if(sizeof($question_data) >0){
     		foreach ($question_data as $key => $value) {

     			$sr= $key+1;

     			$html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';

                if($value['is_mandatory'] == 1) { 
     				$label_mandatory = '<span style="color: red">*</span>'; 
     			}
     			else{
     				$label_mandatory = '';
     			}    			
     			
     			$html.='<label for="description">Q.'.$sr.'&nbsp;&nbsp;'.ucfirst($value['question_text']).$label_mandatory.'</label>';

	 			if($value['option_cnt'] >0 ){
	                $where = array('question_id' => $value['question_id']);
	                $this->db->order_by('op.display_order','ASC');
	                //$option_data = $this->master_model->getRecords("survey_questions_options_master",$where);
	                $option_data = $this->master_model->getRecords("survey_questions_options_master op",$where,"op.ques_option_id, op.question_id, op.dependant_ques_id, op.option, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id AND v1.is_deleted=0) validation_title, op.validation_id, op.sub_validation_id,
						(SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title,
						op.min_value, op.max_value, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != '' AND od.is_deleted=0) dependant_cnt ");
	            }
	            else{
	            	$option_data = [];
	            }

	            $html_input = $this->get_html_tags($value['response_type_id'], $value['question_id'], $option_data, $value['salutation'], $value['if_summation'], $value['if_file_required'], $value['file_label'], $value['input_file_required'],'', '');

	            $html.= $html_input;

     			$child = $this->buildTree($value['question_id'], $sr);
     			//echo '<pre>';
     			if($child){
     				//$value['sub_question'] = $child;
     				$html.= $child;
     				//echo $html;
     			}

     			
     			$html.="</div>
                  	</div>
                </div>";
            }	
			$data['edit_id']     		= base64_encode($question_id);
            $data['module_name']     	= 'Survey';
	        $data['submodule_name'] 	= 'Question';   
	        $data['middle_content']     = 'questions/preview';
	        $data['page_title']   		= 'Preview '.$this->module_title;
	        $data['mode']  				= $mode;
	        $data['html']     			= $html;
	        $this->load->view('admin/admin_combo',$data);
     	}
    }

    public function check_status(){

   		$where = array('parent_question_id' => $_POST['question_id'], 'is_deleted' => 0);
   		$question_data = $this->master_model->getRecords("survey_question_master", $where);
   		$str='';
   		//$str.='**'.sizeof(@$question_data);
   		/*if(sizeof(@$question_data)>0){
   			//if($str != ''){
   				$str.=' Child records Found.';
   			//}
   		}*/

   		$where1 = array('question_id' => $_POST['question_id'], 'is_deleted' => 0);
   		$option_dependant_data = $this->master_model->getRecords("survey_option_dependent", $where1);
   		//$str.='--'.sizeof(@$option_dependant_data);
   		if(sizeof(@$question_data)>0 || sizeof(@$option_dependant_data)>0){
   			//if($str != ''){
   				$str.=' Child records Found.';
   			//}
   		}

   		$where2 = array('survey_id' => $_POST['survey_id'], 'question_id' => $_POST['question_id']);
   		$question_mapping_data = $this->master_model->getRecords("survey_section_questions", $where2);
   		//$str.='///'.sizeof(@$question_mapping_data);
   		if(sizeof(@$question_mapping_data)>0){
   			$str.=' This Record is Mapped to Survey.';
   		}

   		echo $str;

   	}

    // Soft Delete Role
	public function delete($question_id){

	 	// Permission Set Up
        $this->master_model->permission_access('16', 'delete');
		 
		 $question_id = base64_decode($question_id);
		 $delete_array = array('is_deleted' =>1,
		                       'deleted_by_id' =>$this->user_id,
		                       'deleted_on' => date('Y-m-d H:i:s'));

		 $updateQuery = $this->master_model->updateRecord('survey_question_master', $delete_array, array('question_id' => $question_id)); 

		 $where = array('dependant_ques_id' => $question_id);
		 $dependant_ques_data = $this->master_model->getRecords("survey_option_dependent",$where);
		 if(sizeof(@$dependant_ques_data) > 0){
		 	$deleteArr = array("is_deleted" => 1);
		 	$where1 = array('dependant_ques_id' => $question_id);
			$updateQuery1 = $this->master_model->updateRecord('survey_option_dependent', $deleteArr, $where);
		 }
		 
		 if($updateQuery > 0)
		 {
		 	$where = array('question_id' => $question_id);
		 	$question_data = $this->master_model->getRecords("survey_section_questions",$where);
		 	if(sizeof(@$question_data) > 0){
		 		$deleteArr = array("is_deleted" => 1);
				$updateQuery2 = $this->master_model->updateRecord('survey_section_questions', $deleteArr, $where);
		 	}
			
		 }

		 // Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		// Log Data Added
		$postArr			= array('delete_id' => $question_id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id'   => $this->session->userdata('user_id'),
						'action_name'=> "Delete",
						'module_name'=> 'Question Master',
						'store_data' => $json_encode_data,
						'ip_address' => $ipAddr,
					 	'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

		 $this->session->set_flashdata('success','Question deleted successfully');
		 redirect(base_url('xAdmin/question/'));	
		 
	}

	public function edit_option(){
		$dataArr = array( 'option'	   		 => trim($_POST['label']),
						  'display_order'    => trim($_POST['display_order']),
						  'dependant_ques_id'=> trim($_POST['subquestion_id']),
						  'updated_by_id'	 => $this->session->userdata('user_id'),
						);
		$where = array('ques_option_id' => trim($_POST['option_id']));
		$updateQuery = $this->master_model->updateRecord('survey_questions_options_master',$dataArr,$where);
		echo $updateQuery;
	}

	public function delete_option($option_id, $question_id){
		$dataArr = array( 'is_deleted'	   	 => 1,
						  'deleted_by_id'	 => $this->session->userdata('user_id'),
						  'deleted_on'       => date('Y-m-d H:i:s')
						);
		$where = array('ques_option_id' => base64_decode($option_id));
		$updateQuery = $this->master_model->updateRecord('survey_questions_options_master',$dataArr,$where);
		if($updateQuery == 1){
			$this->session->set_flashdata('success','data Deleted successfully');
			redirect(base_url('xAdmin/question/edit/'.$question_id));
		} else {
			$this->session->set_flashdata('error','Something went wrong! Please try again.');
			redirect(base_url('xAdmin/question/edit/'.$question_id));
		}
	}
   	 
    public function list($survey_id){

    	$survey_id = base64_decode($survey_id);

    	$where = array('survey_question_master.is_deleted' => 0, 'survey_question_master.survey_id' => $survey_id);
    	$this->db->order_by('survey_question_master.question_id','ASC');
        $this->db->join('survey_input_category_master cat','cat.input_type  = survey_question_master.response_type_id');

    	$question_list = $this->master_model->getRecords("survey_question_master", $where, "survey_question_master.question_id, survey_question_master.parent_question_id, survey_question_master.question_text, cat.input_category response_type_id, CASE is_mandatory when 1 then 'Yes' else 'No' END AS mandatory, survey_question_master.display_order, (SELECT COUNT(1) FROM survey_questions_options_master optn where optn.question_id = survey_question_master.question_id AND optn.is_deleted=0) cnt");	 

		$this->data['question_list']     =  $question_list;
		//print_r($question_list); die;
		$this->data['page_title']       = $this->module_title.' List';
		$this->data['module_title']     = $this->module_title;
		$this->data['middle_content']   = $this->module_view_folder."question_list";
		
		$this->load->view('admin/admin_combo',$this->data);
	}
	 
    public function open_question_list(){

		$sub_section_id = $_POST['sub_section_id'];
		$survey_id 		= $_POST['survey_id'];
		$section_id 	= $_POST['section_id'];
		
		
		$where = array('survey_question_master.survey_id'=>$_POST['survey_id'], 'survey_section_mapping.section_id'=>$section_id, 'survey_question_master.status' => 'Active', 'survey_question_master.is_deleted' => 0);
		$this->db->group_by('`survey_question_master`.`question_id`');		
		$this->db->join('survey_section_mapping','survey_question_master.survey_id = survey_section_mapping.survey_id');
	 	$question_list = $this->master_model->getRecords("question_master", $where);
	
		if(sizeof($question_list)>0){
     		echo json_encode($question_list);
     	}
	 }
	 

	public function view($survey_id){

		//error_reporting();

		// Permission Set Up
        $this->master_model->permission_access('16', 'view');

     	$survey_id = base64_decode($survey_id);

		$question_query = $this->db->query("SELECT  q1.survey_id, (SELECT svy.title FROM survey_master svy WHERE svy.survey_id = q1.survey_id AND svy.is_deleted=0) survey_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.section_id = sec.section_id AND sec.is_deleted=0) section_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.sub_section_id = sec.section_id AND sec.is_deleted=0) sub_section_name, q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id AND o1.is_deleted=0) option_cnt,  q1.is_mandatory, sm.global_question_id, q1.if_summation, q1.if_file_required, q1.file_label, q1.input_file_required
		    FROM survey_question_master q1 LEFT JOIN survey_section_questions sm ON q1.survey_id = sm.survey_id
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.question_id = sm.question_id
			AND q1.survey_id = ".$survey_id."
			AND q1.is_deleted = 0 
			AND sm.is_deleted = 0 
			AND q1.status = 'Active' 
			GROUP BY q1.survey_id, sm.section_id, sm.sub_section_id, q1.question_id
			ORDER BY q1.survey_id, sm.sort_order IS NOT NULL DESC, sm.sort_order ASC, sm.section_id, sm.sub_section_id, q1.question_id ASC");


		$question_data = $question_query->result_array();

		//echo $this->db->last_query(); die;

     	//$tree = $this->buildTree($question_data[0]['question_id']);
		$tree = array();
		$last_section_name = '';
		$last_sub_section_name = '';
		$label_mandatory = ''; 
		$global_label_mandatory = '';

		$html = '<span style="color: blue; font: bold;"> Survey : &nbsp;&nbsp;'.@$question_data[0]['survey_name'].'</span>
          <hr>  ';

     	//$tree = $this->buildTree($question_data);
     	if(sizeof($question_data) >0){
     		foreach ($question_data as $key => $value) {
     			$sr = $key+1;

     			if($last_section_name <> $value['section_name']) {
     				$html.= '<hr>
	     				<span style="color: green; font: bold;"> Section : &nbsp;&nbsp;'.$value['section_name'].'</span>
	                  	<hr>';

                    
	                $last_section_name = $value['section_name'];   	
	     			$last_sub_section_name = '';
     			}

     			if($last_sub_section_name <> $value['sub_section_name'] && trim($value['sub_section_name']) != '') {
                	$html.= '<hr>
                		<span style="color: #aa42f5; font: bold;"> Sub Section : &nbsp;&nbsp;'.$value['sub_section_name'].'</span>
     				<hr>';

     				if(trim($value['sub_section_name']) != ''){
	     				$last_sub_section_name = $value['sub_section_name']; 
	     			}
     			}

     			$html.='<div class="row border border-info px-3 pt-3 mb-3">

                  			<div class="col-12">
        
                    			<div class="form-group" >';

     			

     			//$html.='***'.$value['is_mandatory'];
     			if($value['is_mandatory'] == 1) { 
     				$label_mandatory = '<span style="color: red">*</span>'; 
     			}
     			else{
     				$label_mandatory = '';
     			}

     			$html.='<label class="w-100" for= description">Q.'.$sr.'&nbsp;&nbsp;'.ucfirst($value['question_text']).$label_mandatory.'</label>';

	 			if($value['option_cnt'] >0 ){
	                $where = array('question_id' => $value['question_id']);
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id AND v1.is_deleted=0) validation_title, op.validation_id, op.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  AND od.is_deleted=0) dependant_cnt FROM survey_questions_options_master op WHERE op.question_id=".$value['question_id']."
	                	    AND op.is_deleted=0
	                	ORDER BY op.display_order ASC");

	                $option_data = $option_query->result_array();
	            }
	            else{
	            	$option_data = [];
	            }
	            //echo $value['global_question_id'];

	            $html_input = $this->get_html_tags($value['response_type_id'], $value['question_id'], $option_data, $value['salutation'], $value['if_summation'], $value['if_file_required'], $value['file_label'], $value['input_file_required'], '', $sr);

	            $html.= $html_input;


     			$child = $this->buildTree($value['question_id'], $sr);
     			//echo '<pre>';
     			if($child){
     				//$value['sub_question'] = $child;
     				$html.= $child;
     				//echo $html;
     			}

     			if($value['global_question_id'] > 0){
	            	/*$html.="</div>
                  	</div>
                </div>";

                $html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';*/


	            	$where = array('q1.question_id' => $value['global_question_id']);
	                $global_question_data = $this->master_model->getRecords("survey_question_master q1",$where, "q1.*");

	            	if(isset($global_question_data)){

	            		if($global_question_data[0]['is_mandatory'] == 1) { 
		     				$global_label_mandatory = '<span style="color: red">*</span>'; 
		     			}
		     			else{
		     				$global_label_mandatory = '';
		     			}

	            		$html.='<label for="description">'.ucfirst($global_question_data[0]['question_text']).$global_label_mandatory.'</label>';

		                $where1 = array('question_id' => $global_question_data[0]['question_id'], 'op.is_deleted'=>0);
		                $this->db->order_by('op.display_order', 'ASC');
		                $global_option_data = $this->master_model->getRecords("survey_questions_options_master op",$where1,"op.ques_option_id, op.question_id, op.dependant_ques_id, op.option, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id AND v1.is_deleted=0) validation_title, 
							(SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title,
							op.min_value, op.max_value, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != '' AND od.is_deleted=0) dependant_cnt");

		                $html_input = $this->get_html_tags($global_question_data[0]['response_type_id'], $global_question_data[0]['question_id'], $global_option_data, $global_question_data[0]['salutation'], $global_question_data[0]['if_summation'], $global_question_data[0]['if_file_required'], $global_question_data[0]['file_label'], $global_question_data[0]['input_file_required'],'', '');

		            	//echo 'Global'.$global_question_data[0]['dependant_cnt'];
		            }

		            $html.= $html_input;
	            }
	            

     			
     			
     			//print_r($value);
     			//$tree[] = $value;
     			//$html.= $html;
     			$html.="</div>
                  	</div>
                </div>";
            }	


     	}

     	
     	/*echo '<pre>';
     	print_r($tree) ; 
     	die;*/

     	//echo $html; 
     	//die;
     	
     	/*if(sizeof($tree) >0){
     		$this->data['question_data']  =  $tree;
     	}
     	else{
     		$this->data['question_data']  =  '';
     	}*/
     	$this->data['html']       		= $html;
		$this->data['page_title']       = 'View '.$this->module_title;
		$this->data['module_name']     	= 'Survey'; 
		$this->data['submodule_name']   = 'Survey_list'; 
		$this->data['mode']     		= 'survey_view'; 
        $this->data['middle_content']   = $this->module_view_folder.'question_view';
	
		$this->load->view('admin/admin_combo',$this->data);
	}

	public function buildTree($question_id, $sr){
		//error_reporting(E_ERROR | E_PARSE);
		//$tree = array();
		$branch = array();
		$sub_html = '';
		$html_input = '';
		$label_mandatory = '';

    	$sub_question_query = $this->db->query("SELECT  q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id AND o1.is_deleted=0) option_cnt, q1.is_mandatory, q1.if_summation, q1.if_file_required, q1.file_label, q1.input_file_required FROM survey_question_master q1 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id AND od.is_deleted=0)
            AND q1.parent_question_id = ".$question_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			ORDER BY q1.question_id ASC");
    	
    	//echo '<pre>';

    	$sub_question_data = $sub_question_query->result_array();
    	
		if (sizeof($sub_question_data)>0) {
			//echo 'if'.$question_id;
		   foreach ($sub_question_data as $id => $sub_question) { 

		   	$key_sr = $id+1;
		   	if($sr != ''){
		   		$sub_sr = 'Q.'.$sr.'.'.$key_sr;
		   	}
		   	else{
		   		$sub_sr = '';
		   	}

		   	$sub_html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';

                //$sub_html.=$sub_question['is_mandatory'];    			
                if($sub_question['is_mandatory'] == 1) { 
                	$label_mandatory = '<span style="color: red">*</span>'; 
            	}
            	else{
            		$label_mandatory = '';
            	}

		   	    $sub_html.='<label for="description" class="w-100">'.$sub_sr.'&nbsp;&nbsp;'.ucfirst($sub_question['question_text']).$label_mandatory.'</label>';

	 			if($sub_question['option_cnt'] >0 ){
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id AND v1.is_deleted=0) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != '' AND od.is_deleted=0 ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$sub_question['question_id']."
	                	ORDER BY op.display_order ASC");

	                $option_data = $option_query->result_array();
	                
	                //$option_data = $option_query->result_array();
	            }
	            else{
	            	$option_data = [];
	            }

	            $html_input = $this->get_html_tags($sub_question['response_type_id'], $sub_question['question_id'], $option_data, $sub_question['salutation'], $sub_question['if_summation'], $sub_question['if_file_required'], $sub_question['file_label'], $sub_question['input_file_required'],'', $sub_sr);

	            $sub_html.= $html_input;

	            $children = $this->buildTree($sub_question['question_id'],$key_sr);

	          	if($children){
	          		//$sub_question['sub_question'] = $children;
	          		$sub_html.= $children;

	            }

	            //$branch[] = $sub_question;

	            $sub_html.="</div>
                  	</div>
                </div>";
	        }
	       
	    }
		   
		//print_r($branch);
	    //return $branch;
	    //echo $sub_html;
	    return $sub_html;
		
	}

	public function get_html_tags($tag_name, $question_id, $option_array, $salutation, $if_summation, $if_file_required, $file_label, $input_file_required, $p_ques_id, $srno){
		//echo $question_id.'--------'.$dependant_cnt.'</br>';
		$str = '';
		$length = '';
		$salutation_flag =0;

		if($salutation!='' && $salutation!='0'){
			$str.= '</div> </div>';
			$salutation_flag =1;
			$salutation_array = explode('|', $salutation);
			if(sizeof($salutation_array)>0){
				$str.= '<div class="col-3"> <div class="form-group">';
				$str.= '<select class="form-control">
		                    <option value="">Select Salutation</option>';
				foreach ($salutation_array as $id) {
					$str.= '<option value="">'.$id.'</option>';	
				}
				$str.= '</select>';
				$str.= '</div> </div>';
			}
			$str.='<div class="col-12"> <div class="form-group">';
		}

		if($tag_name == 'radio'){
			if(sizeof($option_array) >0){
				//$str.='</br>';
				foreach ($option_array as $key => $value) {
					//$str.='</br>';

					if($p_ques_id != ''){
						$radio_name = $p_ques_id.'_'.$question_id;
					}
					else{
						$radio_name = $question_id;
					}

					$str.='<div class="form-check form-check-inline">';
					$str.= '<input class="form-check-input dependantcls" type="radio" id="'.$question_id.'" data-id="'.$value['ques_option_id'].'" data-parent="'.$p_ques_id.'" data-srno="'.$srno.'" name="radio_'.$radio_name.'" value=""> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'">'.$value['option'].'</span>';
					$str.='</div>';	
				}
			}	


			foreach ($option_array as $key => $value) {
				$str.= '</div>
		                </div>';

                if($p_ques_id != ''){
					$ques_id = $p_ques_id.'_'.$question_id.'_'.$value['ques_option_id'];
				}
				else{
					$ques_id = $question_id.'_'.$value['ques_option_id'];
				}
		        $str.= '<div class="col-6 radio_dependant_div_'.$ques_id.'" style="display:none;">';
				if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

					for ($i=0; $i < $value['dependant_cnt']; $i++) { 
						$j = $i+1;   

						if($p_ques_id != ''){
							$repeat_section_id = $p_ques_id.'_'.$value['ques_option_id'].'_'.$j;
						}
						else{
							$repeat_section_id = $value['ques_option_id'].'_'.$j;
						}

		                $str.= '<span id="repeat_section_'.$repeat_section_id.'"></span>';
		               
		           }
		            if($p_ques_id != ''){
						$ques_id = $p_ques_id.'_'.$question_id.'_'.$value['ques_option_id'];
						$repeat_id = $p_ques_id.'_'.$value['ques_option_id'];
					}
					else{
						$ques_id = $question_id.'_'.$value['ques_option_id'];
						$repeat_id = $value['ques_option_id'];
					}
		            $str.= '<div class="col-6 dependant_radio_subquestion_div_'.$ques_id.'" style="display:none;">';
		            $str.= '<span id="subquestion_section_'.$repeat_id.'"></span>';
		            $str.='</div>';
		          
	           }
	            $str.='</div>';

		        $str.=' <div class="col-12">
		               <div class="form-group">';
           }
           $str.='<input type="hidden" id="prev_radio_'.$radio_name.'" value="">';
		}
		elseif($tag_name == 'checkbox'){
			$str1 = '';
			if(sizeof($option_array) >0){
				//$str.='</br>';
				foreach ($option_array as $key => $value) {
					$str.='<div class="form-check" style="clear:left;">';
					$str.= '<input class="form-check-input dependantcls " type="checkbox" id="'.$value['ques_option_id'].'" data-id="'.$question_id.'" data-srno="'.$srno.'" name="chk[]" value="'.$question_id.'"> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div>';


					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){
						$str.='<span class=" checkbox_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
							$str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
						}
						$str.='</span>';

						$str.= '<div class="col-6 dependant_checkbox_subquestion_div_'.$question_id.'" style="display:none;">';
			         	$str.= '<span id="subquestion_section_'.$value['ques_option_id'].'"></span>';
			            $str.='</div>';

					}
						
				}
			}
			$str.='</div>';
			$str.='</div>';
			  $str.=' <div class="col-12">
		               <div class="form-group">';
		}
		elseif($tag_name == 'single_select'){

			$str.= '<select class="form-control dependantcls" id="'.$question_id.'">
	                    <option value="">Select Option Type</option>';
	        if(sizeof($option_array) >0){
		        foreach ($option_array as $key => $value) {
					$str.= '<option value="" id="'.$value['question_id'].'" data-id="'.$value['ques_option_id'].'" data-parent="'.$p_ques_id.'" data-srno="'.$srno.'">'.$value['option'].'</option>';
				}
			}
			$str.= '</select>';

			if(sizeof($option_array) >0){
				foreach ($option_array as $key => $value) {
					$str.= '</div>
			                </div>';

			        if($p_ques_id != ''){
						$ques_id = $p_ques_id.'_'.$question_id.'_'.$value['ques_option_id'];
					}
					else{
						$ques_id = $question_id.'_'.$value['ques_option_id'];
					}

			       $str.= '<div class="col-6 dropdown_dependant_div_'.$ques_id.'" style="display:none;">';

					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    

							if($p_ques_id != ''){
								$repeat_section_id = $p_ques_id.'_'.$value['ques_option_id'].'_'.$j;
							}
							else{
								$repeat_section_id = $value['ques_option_id'].'_'.$j;
							}
			                $str.= '<span id="repeat_section_'.$repeat_section_id.'"></span>';
			               
			           }
			          
		           }
		            $str.='</div>';

		            if($p_ques_id != ''){
		            	$ques_id = $p_ques_id.'_'.$question_id.'_'.$value['ques_option_id'];
						$repeat_id = $p_ques_id.'_'.$value['ques_option_id'];
					}
					else{
						$repeat_id = $value['ques_option_id'];
						$ques_id = $question_id.'_'.$value['ques_option_id'];
					}

		            $str.= '<div class="col-6 dependant_dropdown_subquestion_div_'.$ques_id.'" style="display:none;">';
		            $str.= '<span id="subquestion_section_'.$repeat_id.'"></span>';
		            $str.='</div>';	
		            
			        $str.=' <div class="col-12">
			               <div class="form-group">';
	           }

	           
			}
			if($p_ques_id != ''){
				$single_select_id = $p_ques_id.'_'.$question_id;
			}
			else{
				$single_select_id = $question_id;
			}


			//$str.='<input type="hidden" id="prev_select_'.$single_select_id.'" value="">';

			$str.='<input type="hidden" id="prev_select_'.$single_select_id.'" value="">';

			//$str.='<input type="hidden" id="prev_subq_dropdown_'.$single_select_id.'" value="">';
		}
		elseif($tag_name == 'textbox'){
			//$str.=$salutation_flag;
			if(sizeof($option_array) >1){

				//if($salutation_flag = 0){
					$str.= '</div> </div>';
				//}
				$str.= '<div class="row w-100">';
				foreach ($option_array as $key => $value) {
					//if($salutation_flag = 0){
					 $str.= '<div class="col-md-4"> <div class="form-group">';
					//}

					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{

						if($value['sub_validation_id'] == 14){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 15){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}

					if($value['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					//$str.='a';
					$str.='<label>'.$value['option'].'</label>';
					$str.= "<input type='text' ".$length." id='".$question_id."' data-optn-id='".$value['ques_option_id']."'" ;
					$str.="data-id='[".json_encode($option_array[$key])."]'";
					$str.="placeholder='".$value['option']."'";
					if($if_summation == 'Yes'){
						$str.="class='form-control dependantcls calculate_total textfields_".$question_id."' value='' ".$call_function.">";
					}
					else{
						$str.="class='form-control dependantcls' value='' ".$call_function.">";
					}
					$str.='<span id="text_error_'.$question_id.'_'.$value['ques_option_id'].'" class="error-msg" style="color: red;"></span>';
					//if($salutation_flag = 0){
						//$str.= '</div> </div>';
					//}
					if($input_file_required == 'Yes'){
						$str.= '<input type="file" class="form-control mt-3" id="'.$question_id.'">';
					}

					$str.= '</div> </div>';
				}
				//$str.='--'.$if_summation;
				if($if_summation == 'Yes'){
			    	$str.='<div class="col-6"> <div class="form-group">';
			    	$str.= "<input type='text' class='form-control total_cnt_class' id='total_cnt_class_".$question_id."'";
					$str.="placeholder='Total' readonly='readonly' value=''>";
					$str.='</div></div>';
					//$str.='<div class="col-6"> <div class="form-group">';
					//$str.='<button id="calculate_total">Total</button>';
					//$str.='</div></div>';
				}
				//if($salutation_flag = 0){
				    $str.= '</div>';
					$str.='<div class="col-12"> <div class="form-group">';
				//}

			}
			else{
				//$str.='<div class="col-12"> <div class="form-group">';
				if(isset($option_array) && sizeof($option_array)>0){
					if($option_array[0]['validation_title'] == "Length" && $option_array[0]['min_value'] > 0)
					{

						if($option_array[0]['sub_validation_id'] == 14){
							$length =  'maxlength='.$option_array[0]['min_value'];
						}
						else if($option_array[0]['sub_validation_id'] == 15){
							$length =  'minlength='.$option_array[0]['min_value'];
						
						}	
					}

					if($option_array[0]['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					$str.= "<input type='text' class='form-control dependantcls' ".$length." id='".$question_id."'  data-optn-id='".$option_array[0]['ques_option_id']."' data-id='".json_encode($option_array)."' data-srno='".$srno."' placeholder='Enter Answer' ".$call_function.">";
					
					$str.='<span id="text_error_'.$question_id.'_'.$option_array[0]['ques_option_id'].'" class="error-msg" style="color: red;"></span>';

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textbox_dependant_div_'.$question_id.'_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			           $str.='</span>';

			          	$str.= '<div class="col-6 dependant_textbox_subquestion_div" style="display:none;">';
			            $str.= '<span id="subquestion_section_'.$option_array[0]['ques_option_id'].'"></span>';
			            $str.='</div>';
		           }
				}
				
			}
			
			
			//$str.='</br>';
			$str.='<span id="text_error_numonly_'.$question_id.'" style="color: red;"></span>';

			
		}
		elseif($tag_name == 'file'){
			$str.= '<input type="file" class="form-control" id="'.$question_id.'">';
		}
		elseif($tag_name == 'textarea'){
			//print_r($option_array);
			if(sizeof($option_array) >1){

				//if($salutation_flag = 0){
					$str.= '</div> </div>';
				//}
				$str.= '<div class="row mx-0">';
				foreach ($option_array as $key => $value) {
					
					$str.= '<div class="col-6"> <div class="form-group">';
					
					if($value['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}

					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{
						if($value['sub_validation_id'] == 14){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 15){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}

					$str.='<label>'.$value['option'].'</label>';
					$str.= '<textarea class="form-control form-group textarea_dependantcls" id="'.$question_id.'" data-optn-id="'.$option_array[$key]['ques_option_id'].'" 
					data-id="'.$option_array[$key]['if_sub_question_checked'].'" data-srno="'.$srno.'" rows="2" '.$length.' "'.$call_function.'"></textarea>';

					if($option_array[$key]['if_sub_question_checked'] == 'Yes' && $option_array[$key]['dependant_cnt'] > 0){

						$str.='<span class="textarea_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';

			                $arr['if_sub_question_checked'] = $option_array[0]['if_sub_question_checked']; 
			               
			           }
			           $str.='</span>';
					
					}
				
					if($input_file_required == 'Yes'){
						$str.= '<input type="file" class="form-control mt-3" id="'.$question_id.'">';
					}

					$str.= '</div> </div>';
				}
				
				$str.= '</div>';
			    $str.='<div class="col-12"> <div class="form-group">';
				

			}
			else{
			if(isset($option_array) && sizeof($option_array)>0){

				
				foreach ($option_array as $key => $value) {
					
				
					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{
						if($value['sub_validation_id'] == 14){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 15){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}
			         
					

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textarea_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';

			                $arr['if_sub_question_checked'] = $option_array[0]['if_sub_question_checked']; 
			               
			           }
			           $str.='</span>';
					
					}
				} 

				$str.= '<textarea '.$length.' class="form-control form-group textarea_dependantcls" id="'.$question_id.'" data-optn-id="'.$option_array[0]['ques_option_id'].'" 
					data-id="'.$option_array[0]['if_sub_question_checked'].'" rows="2"  ></textarea>';


				$str.= '<div class="col-6 dependant_textarea_subquestion_div" style="display:none;">';
			            $str.= '<span id="subquestion_section_'.$option_array[0]['ques_option_id'].'"></span>';
			            $str.='</div>';
			 }
		    }
			/*else
			{
				$str.= '<textarea class="form-control form-group" id="'.$question_id.'" placeholder="Enter Answer" rows="2" /></textarea>';
			}
			*/
			//$str.= '<textarea class="form-control dependantcls" id="'.$question_id.'" placeholder="Enter Answer" rows="2"  /></textarea>';
		}

		if($if_file_required == 'Yes'){
			$str.= '<label>'.$file_label.'</label>';
			$str.= '<input type="file" class="form-control mb-4">';
		}

		//echo $str.'</br>';

		return $str;
	}

	public function get_dependant_question(){

		//error_reporting(0);
		$question_id = $_POST['question_id'];
		$option_id = $_POST['option_id'];

		$srno = $_POST['srno'];

		$subquestion_html = '';

		$question_query = $this->db->query("SELECT q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory, q1.salutation, q1.if_summation, q1.if_file_required, q1.file_label, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE optn.validation_id = v1.validation_id AND v1.is_deleted=0)  validation_title, optn.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE optn.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title, optn.min_value, optn.max_value, optn.validation_label, optn.display_order, q1.input_file_required, q1.input_file_required
		FROM   survey_question_master q1  LEFT JOIN survey_option_dependent od ON od.dependant_ques_id = q1.question_id LEFT JOIN survey_questions_options_master optn ON optn.question_id = q1.question_id
		WHERE  od.ques_option_id = ".$option_id."
		AND   q1.is_deleted = 0 
		AND   q1.status = 'Active'
		GROUP BY q1.question_id 
		ORDER BY q1.question_id ASC");
		 //echo $this->db->last_query();
		$question_data = $question_query->result_array();

		$question_array = array();
		if(isset($question_data) && sizeof($question_data) >0){
			$j=0;
			foreach ($question_data as $ques_key => $question) {

				$key_srno = $ques_key+1;
		   		$dependant_sr = $srno.'.'.$key_srno;

				$option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id AND v1.is_deleted=0) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id AND v2.is_deleted=0) sub_validation_title, 
					CASE WHEN op.sub_validation_id = 1 THEN '>' 
					     WHEN op.sub_validation_id = 2 THEN '>=' 
					     WHEN op.sub_validation_id = 3 THEN '<'
					     WHEN op.sub_validation_id = 4 THEN '<='
					     WHEN op.sub_validation_id = 5 THEN '=='
					     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition, 
					op.min_value, op.max_value, op.validation_label, op.dependant_ques_id, op.option, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != '' AND od.is_deleted=0 ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$question['question_id']."
					AND op.is_deleted=0
					ORDER BY op.display_order ASC");

		        $option_data = $option_query->result_array();

	 			$html_tag = $this->get_html_tags($question['response_type_id'], $question['question_id'], $option_data, $question['salutation'], $question['if_summation'], $question['if_file_required'], $question['file_label'], $question['input_file_required'], $question_id, $dependant_sr);

	 			for ($i=0; $i < sizeof($question_data) ; $i++) { 
	 				$html_tag.= '</div>
			                </div>
			                  <div class="col-6">
			                    <div class="form-group">
			                      <label id="label_'.$question_id.'"></label>
			                      <div id="dependant_'.$question_id.'"></div>
			                    </div>
			                  </div>

		                <div class="col-12">
		               <div class="form-group">';

		            //$str = $question['question_text'].'$$$'.$html_tag.'$$$'.sizeof($question_data);

		 			
		 			//array_push($question_array, $str);
	 			}

	 			$child_question = $this->buildTree($question['question_id'], $dependant_sr);

	 			if($child_question){
	 				$subquestion_html.= $child_question;
	 				
	 			}

				//$question_array[] = $str;	
	 			$question_array[$j]['question_text'] = $question['question_text'];
	 			$question_array[$j]['is_mandatory'] = $question['is_mandatory'];		
		 		$question_array[$j]['html_tag'] = $html_tag;	
	 			//$question_array[$j]['subquestion_html'] = $subquestion_html;	

	 			//echo $question['question_text'].'$$$'.$html_tag.'$$$'.$question['response_type_id'].'$$$'.sizeof($question_data);
	 			$j++;
 			}
 			
 			echo json_encode($question_array).'$$$'.$subquestion_html;

     	}
     	else{
     		echo '';
     	}
	
	}

	public function remove_global_question($survey_id){

		$survey_id = base64_decode($survey_id);

		$where = array('survey_id'=>$survey_id);
		$survey_data = $this->master_model->getRecords("survey_master", $where);

		$this->db->join('survey_section_master', 'survey_section_mapping.section_id = survey_section_master.section_id');
     	$section_data = $this->master_model->getRecords("survey_section_mapping", $where, "survey_section_mapping.*, survey_section_master.section_name");
     
     	$data['survey_id']     		= $survey_id;
     	$data['survey']     		= $survey_data[0]['title'];
     	$data['section_data']     	= $section_data;
     	$data['module_name']     	= 'Survey';
        $data['submodule_name'] 	= 'Remove Global Question';   
        $data['middle_content']     = 'questions/remove_global_question';
        $this->load->view('admin/admin_combo',$data);
	}

	public function update_global_question_status(){
		
		if(isset($_POST['mode']) == 'submit'){
        	if(isset($_POST['question_array'])){
        		$question_array = $_POST['question_array'];
        	}

        	if(sizeof($question_array)>0){
                $survey_id = $_POST['survey'];
                $section_id = $_POST['section'];
                
            	foreach ($question_array as $key => $value) {
            		
            		$where = array('survey_id' => $survey_id, 'section_id' => $section_id, 'question_id' => $value);

                    $dataArr = array( 'global_question_id'  => '' );

    				$updateQuery = $this->master_model->updateRecord('survey_section_questions',$dataArr, $where); 

            	}
                if ($updateQuery>0) {
                    echo 1;
                }
                else{
                    echo 0;
                }
            }
         	else{
         		echo 2;
         	}
        }
	}

	public function get_questions(){
		
		if($_POST["section_id"]!=''){

            $question_query = $this->db->query("SELECT q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory, s2.global_question_id, (SELECT q2.question_text FROM survey_question_master q2 WHERE q2.question_id = s2.global_question_id) global_question_text 
            FROM survey_question_master q1 LEFT JOIN survey_section_questions s2 ON s2.question_id = q1.question_id
            WHERE q1.survey_id = ".$_POST["survey_id"]."
            AND s2.section_id = ".$_POST["section_id"]."
            AND q1.parent_question_id = 0
            AND q1.is_deleted = 0
            AND s2.is_deleted = 0
            GROUP BY q1.question_id  
            ORDER BY q1.question_id  ASC");

            $question_data = $question_query->result_array();
        }
     
     	if(sizeof($question_data)>0){
     		echo json_encode($question_data);
     	}

	}

	public function get_question_list_choice(){
            
        $where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0);//, 'parent_question_id !=' => 0
        if(isset($_POST['question_id'])){
        	$this->db->where('question_id !=', $_POST['question_id']);
        }
        $question_data = $this->master_model->getRecords("question_master", $where);
       // echo $this->db->last_query();
        if(sizeof($question_data)>0){
            echo json_encode($question_data);
        }
    
    }

	public function get_dependant_question_options(){

		$where = array('question_id' => $_POST['question_id'], 'is_deleted' => 0);
     	$question_option_data = $this->master_model->getRecords("survey_questions_options_master", $where);

     	if(sizeof($question_option_data) >0){
     		echo json_encode($question_option_data);
     	}
     	else{
     		echo '';
     	}
	}

	// Update Role Status
	public function changeStatus(){
		 // Permission Set Up
        $this->master_model->permission_access('16', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_list',array('status'=>'Inactive'),array('survey_id' => $id));
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_list',array('status'=>'Active'),array('survey_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey'));		
		 }
		 
	}

	public function get_section(){

     	$where = array('survey_id'=>$_POST['survey_id']);

     	$this->db->join('survey_section_master', 'survey_section_mapping.section_id = survey_section_master.section_id');

     	$section_data = $this->master_model->getRecords("survey_section_mapping", $where, "survey_section_mapping.*, survey_section_master.section_name");
     
     	if(sizeof($section_data)>0){
     		echo json_encode($section_data);
     	}
	}
	
	public function get_sub_section(){
		
		$where = array('parent_section_id'=>$_POST['section_id'], 'is_deleted' => 0);

     	//$this->db->join('survey_section_master', 'survey_section_mapping.section_id = survey_section_master.section_id');

     	$section_data = $this->master_model->getRecords("section_master", $where);
		//echo $this->db->last_query();die();
     
     	if(sizeof($section_data)>0){
     		echo json_encode($section_data);
     	}
		
	}

	public function get_validation_list(){
		
		if($_POST['mode'] == 'add')
		{
			if($_POST['option_type'] == 'textarea'){
			//$where = array('validation_id'=>'4', 'validation_id' => '9', 'status' => 'Active', 'is_deleted' => 0);
			$this->db->where('validation_id', 4);
			//$this->db->where('validation_id', 5);
			
		}
			$this->db->where('status', 'Active');
			$this->db->where('is_deleted', 0);

			$condition_data = $this->master_model->getRecords("survey_response_validation_master");
		 
			if(sizeof($condition_data)>0){
				echo json_encode($condition_data);
			}
		}
		
	}

	public function get_condition(){

		$where = array('validation_id'=>$_POST['validation_id'], 'status' => 'Active', 'is_deleted' => 0);

     	$condition_data = $this->master_model->getRecords("survey_response_validation_subtype_master", $where);
     
     	if(sizeof($condition_data)>0){
     		echo json_encode($condition_data);
     	}
	}
	
	// Section wise question list 
	public function get_question_list(){
		//error_reporting(0);
		
		if($_POST['survey_id'] > 0)
		{

			if($_POST['question_id'] == ''){
				$question_query = $this->db->query("SELECT Q1.* FROM survey_question_master Q1 WHERE Q1.survey_id = '".$_POST['survey_id']."' AND Q1.status = 'Active' AND Q1.is_deleted = '0' AND Q1.parent_question_id = '0' AND NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE Q1.question_id = od.dependant_ques_id
                  			  AND od.is_deleted=0)
                  			  AND NOT EXISTS (SELECT * FROM  survey_section_questions ssq
                  			  WHERE ssq.survey_id='".$_POST['survey_id']."' AND Q1.question_id = ssq.question_id AND ssq.is_deleted=0) ORDER BY Q1.question_id DESC");
			}
			else{
				$question_query = $this->db->query("SELECT Q1.* FROM survey_question_master Q1 WHERE Q1.survey_id = '".$_POST['survey_id']."' AND Q1.status = 'Active' AND Q1.is_deleted = '0' AND Q1.parent_question_id = '0' AND NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE Q1.question_id = od.dependant_ques_id AND od.is_deleted=0)
                  			  AND NOT EXISTS (SELECT * FROM  survey_section_questions ssq
                  			  WHERE ssq.survey_id='".$_POST['survey_id']."' AND Q1.question_id = ssq.question_id AND ssq.is_deleted=0) 
                  			  AND Q1.question_id != ".$_POST['question_id']."
                  			  ORDER BY Q1.question_id DESC");
				//echo $question_query;
			}
			
			$question_data = $question_query->result_array();

			$res ='';
			if(sizeof($question_data)>0){
				$res = json_encode($question_data);			
			}

			if(@$_POST['option_type'] == 'radio' || @$_POST['option_type'] == 'checkbox' || @$_POST['option_type'] == 'single_select'){
				if(@$_POST['if_subquestion'] == 'Yes'){
					$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0);
			    	$sub_question_data = $this->master_model->getRecords("question_master", $where);
			    	if(sizeof($sub_question_data) > 0){
						$res.= '$$'.json_encode($sub_question_data);	
					}
				}
			}
			else{
				if(@$_POST['if_logical'] == 'Yes'){
					$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0);
			    	$sub_question_data = $this->master_model->getRecords("question_master", $where);
			    	if(sizeof($sub_question_data) > 0){
						$res.= '$$'.json_encode($sub_question_data);	
					}
				}
			}

			echo $res;
		}		
	
	}	


	public function get_question_list_selected(){
		//error_reporting(0);
		if($_POST['survey_id'] > 0)
		{
			$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0, 'parent_question_id' => 0);

			if(isset($_POST['question_id'])){
	        	$this->db->where('question_id !=', $_POST['question_id']);
	        }

			$question_data = $this->master_model->getRecords("question_master", $where);

			$where1 = array('ques_option_id'=> $_POST['option_id'], 'is_deleted'=>0, 'status'=>'Active');

			$dependant_question_data = $this->master_model->getRecords("survey_option_dependent", 
				$where1);

			//print_r($dependant_question_data); 

			$check_array = array();
			if(sizeof($dependant_question_data)> 0){

				foreach($dependant_question_data as $value)
				{
					$check_array[] = $value['dependant_ques_id'];
					//echo $value['dependant_ques_id'] .'---'. $key['question_id'];	
					//if($value['dependant_ques_id'] == $key['question_id']){$chk = "selected='selected'";}else{$chk = "";}
				}
			}

			print_r($check_array);
		 
			//$html = '<option value="">-- Select Question --</option>';
			if(count($question_data) > 0)
			{	
				foreach($question_data as $key)
				{
					if(in_array($key['question_id'], $check_array)) { $chk = "selected='selected'";}
						else{$chk = "";}	
					$html .= '<option value="'.$key['question_id'].'" '.$chk.'>'.$key['question_text'].'</option>';
				}
			}
			echo $html;
		}		
	
	}	
	
	
	// Section wise question list 
	public function get_question_list_edit(){
		//error_reporting(0);
		if($_POST['survey_id'] > 0)
		{
			//$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0, 'parent_question_id' => 0);
			//$question_data = $this->master_model->getRecords("question_master", $where);
			$question_query = $this->db->query("SELECT Q1.* FROM survey_question_master Q1 WHERE Q1.survey_id = '".$_POST['survey_id']."' AND Q1.status = 'Active' AND Q1.is_deleted = '0' AND Q1.parent_question_id = '0' AND NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE Q1.question_id = od.dependant_ques_id AND od.is_deleted=0) 
                  			  AND NOT EXISTS (SELECT * FROM  survey_section_questions ssq
                  			  WHERE ssq.survey_id='".$_POST['survey_id']."' AND Q1.question_id = ssq.question_id AND ssq.is_deleted=0) ORDER BY Q1.question_id DESC");
			$question_data = $question_query->result_array();
			
			//$parent_id = $_POST['parent_id'];
			$get_condition_data = $this->master_model->getRecords("question_master", array("question_id" => $_POST['parent_id']));
		 
			//$html = '<option value="">-- Select Question --</option>';
			if(count($question_data) > 0)
			{	
				foreach($question_data as $key)
				{
					if($get_condition_data[0]['question_id'] == $key['question_id']){$chk = "selected='selected'";}else{$chk = "";}
					$html .= '<option value="'.$key['question_id'].'" '.$chk.'>'.$key['question_text'].'</option>';
				}
			}
			echo $html;
		}		
	
	}	
	
	// Edit Selected Values 
	public function get_selected_validation_list()
	{

		
		if($_POST['option_type'] == 'textarea'){
			//$where = array('validation_id'=>'4', 'validation_id' => '9', 'status' => 'Active', 'is_deleted' => 0);
			$this->db->where('validation_id', 4);
			//$this->db->where('validation_id', 5);
			
		}
		$this->db->where('status', 'Active');
		$this->db->where('is_deleted', 0);

     	$condition_data = $this->master_model->getRecords("survey_response_validation_master");
		
		$validType = $_POST['validtype'];
		$get_condition_data = $this->master_model->getRecords("survey_response_validation_master", array("validation_id" => $validType));
     
     	$html = '';
		if(count($condition_data) > 0)
		{
			$html .="<option value='' disabled>--Select--</option>";
			foreach($condition_data as $key)
			{
				if($get_condition_data[0]['validation_id'] == $key['validation_id']){$chk = "selected='selected'";}else{$chk = "";}
				$html .= '<option value="'.$key['validation_id'].'" '.$chk.'>'.$key['validation_type'].'</option>';
			}
		}
		echo $html;
	}
	
	// Edit Selected Values 
	public function get_selected_sub_validation_list()
	{		
		$this->db->where('status', 'Active');
		$this->db->where('is_deleted', 0);
	
     	$condition_data = $this->master_model->getRecords("survey_response_validation_subtype_master", array('validation_id' => $_POST['validtype']));
		
		$sub_id = $_POST['sub_id'];
		$get_condition_data = $this->master_model->getRecords("survey_response_validation_subtype_master", array("sub_validation_id" => $sub_id));
     
     	$html = '';
		if(count($condition_data) > 0)
		{
			$html .="<option value='' disabled>--Select--</option>";
			foreach($condition_data as $key)
			{
				if($get_condition_data[0]['sub_validation_id'] == $key['sub_validation_id']){$chk = "selected='selected'";}else{$chk = "";}
				$html .= '<option value="'.$key['sub_validation_id'].'" '.$chk.'>'.$key['validation_sb_type'].'</option>';
			}
		}
		echo $html;
	}
	
	// Edit Selected Values 
	public function get_selected_question_list()
	{

		$survey_id = $_POST['survey_id'];
		$question_id = $_POST['question_id'];
		
		$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0, 'parent_question_id' => 0);

		$question_data = $this->master_model->getRecords("question_master", $where);		
     	
		$get_selected_data = $this->master_model->getRecords("questions_options_master", array("question_id" => $question_id));
     
     	$html = '';
		if(count($question_data) > 0)
		{
			$html .= '<option value="" disabled>---Select---</option>';
			foreach($question_data as $key)
			{
				if($get_selected_data[0]['dependant_ques_id'] == $key['question_id']){$chk = "selected='selected'";}else{$chk = "";}
				$html .= '<option value="'.$key['question_id'].'" '.$chk.'>'.$key['question_text'].'</option>';
			}
		}
		echo $html;
	}
	
	/* 
	// Edit Selected Values 
	public function get_selected_question_list()
	{

		$survey_id = $_POST['survey_id'];
		$question_id = $_POST['question_id'];
		$ques_option_id = $_POST['ques_option_id'];	
		$where = array('survey_id'=>$_POST['survey_id'], 'status' => 'Active', 'is_deleted' => 0, 'parent_question_id' => 0);

		$question_data = $this->master_model->getRecords("question_master", $where);		
     	
		$get_selected_data = $this->master_model->getRecords("survey_option_dependent", array("question_id" => $question_id, "ques_option_id" => $ques_option_id));
		if(count($get_selected_data) > 0)
		{
			$arrQuestion = array();
			foreach($get_selected_data as $quest)
			{
				array_push($arrQuestion,$quest);
			}
		}
		//print_r($arrQuestion);
     	$html = '';
		if(count($question_data) > 0)
		{
			$html .= '<option value="">---Select---</option>';
			foreach($question_data as $key)
			{
				if($get_selected_data[0]['dependant_ques_id'] == $key['question_id']){$chk = "selected='selected'";}else{$chk = "";}
				$html .= '<option value="'.$key['question_id'].'" '.$chk.'>'.$key['question_text'].'</option>';
			}
		}
		echo $html;
	}
	
	*/

}