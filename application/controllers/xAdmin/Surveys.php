<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Survey Master
Author : Priyanka Wadnere
*/

class Surveys extends CI_Controller 
{

	function __construct() {
	
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	

		$this->db->query("SET global sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
		//echo $this->db->last_query();
		$this->user_id            	=  $this->session->userdata('user_id');
        //$this->name               =  $this->session->userdata('nabcons_username');
        $this->ip                 	=  $this->input->ip_address();
        $this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'survey';
        $this->module_title       =  "Survey Listing";
        $this->module_view_folder =  "survey/";  

    }
   	 
	public function assign_users($survey_id)
	{
		//error_reporting(0);		
		//print_r($_POST);
		if(isset($_POST['submit']))
		{
			// Users Array
			$users 	= $this->input->post('users');
			$s_id = $this->input->post('s_id');
			//echo ">>>".$s_id;
			//echo "<pre>";print_r($users);
			//echo "==";count($users);die();
			if(count($users) > 0){
				
				$this->db->where('survey_id', $s_id);				
				$this->db->delete('survey_assign_users_2');
				
				foreach($users as $users_id)
				{
					$dataArr = array( 	
										'survey_id' 	=> $s_id,
										'user_id' 		=> $users_id,
										'created_by_id' => $this->session->userdata('user_id')
									);	
					
					$insertQuery = $this->master_model->insertRecord('assign_users_2',$dataArr);
				}
				
				if(@$insertQuery >0 ){
					$this->session->set_flashdata('success','User Assigned successfully');
					redirect(base_url('xAdmin/surveys/assign_users/'.base64_encode($s_id)));	
				} 
					
			}
			
			//$update_array = array('users' => $usersStr);
			//$updateQuery = $this->master_model->updateRecord('survey_master', $update_array, array('survey_id' => $survey_id));
			
			
		}
		$survey_id = base64_decode($survey_id);

		$config_survey_id = $this->config->item('education_survey_id');
		$approver2_role_id = $this->config->item('approver2_role_id');

		if($config_survey_id == $survey_id){
			$this->db->where('role_id !=', $approver2_role_id);
		}

		//$this->db->where('is_deletedd', 0);
		$user_data = $this->master_model->getRecords("survey_users",array('user_id!=' => '1','is_deleted' => '0', 'status' => 'Active'));

		//echo $this->db->last_query();


    	$survey_data = $this->master_model->getRecords("survey_assign_users_2",array('survey_id' => $survey_id, 'status' => 'Active', 'is_deleted' => '0'));
		$this->data['page_title']      = "Assign Users";
		$this->data['module_title']    = $this->module_title;
		$this->data['module_name']     = 'Assign Users';
		$this->data['submodule_name']  = '';
		$this->data['user_data']       = $user_data;
		$this->data['survey_data']     = $survey_data;
		$this->data['survey_id']       = $survey_id;
		$this->data['middle_content']  = $this->module_view_folder."assign_users_2";

		$this->load->view('admin/admin_combo',$this->data);
	}
}