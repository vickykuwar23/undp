<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Response
Author : Priyanka Wadnere
*/
class Response extends CI_Controller 
{	
	function __construct() {
		
        parent::__construct();
		$this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'Responses';
        $this->module_title       =  "Responses";
        $this->module_view_folder =  "responses/";    
    }

    public function index($survey_id = null)
	{
		//error_reporting();
		//$survey_id = base64_decode($survey_id);
		$response_headers_query = $this->db->query("SELECT shr.*, (SELECT COUNT(1) FROM survey_responses sr where shr.response_id = sr.response_id AND sr.is_deleted = '0') ques_cnt,  (SELECT title FROM survey_master s1 WHERE shr.survey_id = s1.survey_id) survey_name, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
				FROM survey_response_headers shr 
				ORDER BY shr.response_id DESC");
		//echo $this->db->last_query();
		$response_header_data = $response_headers_query->result_array();

		
		$system_admin_id = $this->config->item('system_admin_id');
		$admin_role_id = $this->config->item('admin_role_id');
		
		if($this->session->userdata('role_id') == $admin_role_id || $this->session->userdata('role_id') == 2 || $this->session->userdata('role_id') == $system_admin_id) {
			$where = array('is_deleted' => 0);
			$survey_data = $this->master_model->getRecords("survey_master",$where);

		}
		else{
			$survey_query = $this->db->query("SELECT s1.* 
					FROM survey_master s1  
					WHERE s1.survey_id IN (select su.survey_id from survey_assign_users su where su.user_id = ".@$this->session->userdata('user_id').")");

			$survey_data = $survey_query->result_array();
		}
		$config_survey_id = $this->config->item('education_survey_id');
		$approver2_role_id = $this->config->item('approver2_role_id');
		//echo $this->session->userdata('role_id');
		//echo $approver2_role_id;
		//print_r($survey_data);
		if($this->session->userdata('role_id') == $approver2_role_id){
			foreach ($survey_data as $key => $value) {
				if($config_survey_id == $value['survey_id']){
					unset($survey_data[$key]);
				}
			}
		}
		
		//die;
		$where2 = array('is_deleted' => 0, 'status' => 'Active', 'role_id' => $this->config->item( 'surveyor_role_id' ));
		$surveyer_data = $this->master_model->getRecords("survey_users",$where2);
		//echo $this->db->last_query();
		$this->data['page_title']      = 'Response Listing';//$this->module_title." List";
		$this->data['module_title']    = $this->module_title;
		$this->data['module_name']     = 'Survey';
		$this->data['submodule_name']  = 'Response_list';
		$this->data['survey_id']  	   = base64_decode(@$survey_id);
		$this->data['survey_data']     = $survey_data;//$this->module_title." List";
		//$this->data['district_data']   = $district_data;
		$this->data['surveyer_data']   = $surveyer_data;
		$this->data['response_header_data']     = $response_header_data;
		$this->data['redirect_from']      = 'index';

		$this->data['middle_content']  = $this->module_view_folder."response_list";

		$this->load->view('admin/admin_combo',$this->data);
	}

	public function show_responses($survey_id)
	{
		//error_reporting();
		$survey_id = base64_decode($survey_id);
		
		$this->db->select('district_id');
		$where_con = array('is_deleted' => 0, 'status' => 'Active', 'user_id' => $this->session->userdata('user_id'));
		$approver_data = $this->master_model->getRecords("survey_users",$where_con);
		//echo $this->db->last_query();
		//print_r($approver_data);
		
		$distData = $approver_data[0]['district_id'];
		
		$explode = explode(",", $distData);
		$where_con1 = '';
		if(count((array)$explode) > 0){
				
			$where_con1 .= " AND (";
			$i = 0;
			foreach($explode as $t){
			
				if($i!=(count($explode) - 1)){
					$where_con1 .= " FIND_IN_SET('".$t."',`district_id`) OR ";
					} else {
					$where_con1 .= " FIND_IN_SET('".$t."',`district_id`))";
				}				
				$i++;
			}
			
		} 
		
		$system_admin_id = $this->config->item('system_admin_id');
		$admin_role_id = $this->config->item('admin_role_id');
		$config_survey_id = $this->config->item('education_survey_id');

		if($this->session->userdata('role_id') == $system_admin_id || $this->session->userdata('role_id') == $admin_role_id)
		{
			$response_headers_query = $this->db->query("SELECT shr.*, (SELECT COUNT(1) FROM survey_responses sr where shr.response_id = sr.response_id AND sr.is_deleted = '0') ques_cnt,  (SELECT title FROM survey_master s1 WHERE shr.survey_id = s1.survey_id) survey_name, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
					FROM survey_response_headers shr 
					WHERE shr.survey_id = ".$survey_id."
					ORDER BY shr.response_id DESC");
					
					
			$district_query = $this->db->query("SELECT district_id, district_name FROM survey_district_master WHERE is_deleted = '0' AND status = 'Active' "); 		
		}
		else{

			$approver1_role_id = $this->config->item('approver1_role_id');
            $approver2_role_id = $this->config->item('approver2_role_id');
			
			$district_query = $this->db->query("SELECT district_id, district_name FROM survey_district_master WHERE district_id IN(".$distData.") AND is_deleted = '0' AND status = 'Active' "); 

           /* if($this->session->userdata('role_id') == $approver1_role_id){
            	$where_con1.= ' AND (shr.status = "Submitted" OR shr.status = "Level 1 In Review" OR shr.status = "Level 1 Returned" OR shr.status =  "Level 1 Approved")';
            }*/
       
			$response_headers_query = $this->db->query("SELECT shr.*, (SELECT COUNT(1) FROM survey_responses sr where shr.response_id = sr.response_id AND sr.is_deleted = '0') ques_cnt,  (SELECT title FROM survey_master s1 WHERE shr.survey_id = s1.survey_id) survey_name, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
					FROM survey_response_headers shr 
					WHERE shr.survey_id = ".$survey_id."
					".$where_con1." ORDER BY shr.response_id DESC");
			//AND   u1.user_id 	  = ".$this->session->userdata('user_id')."
		}
		
		$response_header_data = $response_headers_query->result_array();

		if(@$_POST['open_response_page'] == 'No'){
			//echo $this->db->last_query();
			//echo $this->session->userdata('role_id');
			$config_survey_id = $this->config->item('education_survey_id');
				/*if($survey_id != '')
				{
			       	if($config_survey_id == @$survey_id){

			        	$village_query = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
			            
			            $village_data = $village_query->result_array();

			            if(count(@$village_data) > 0)
			            {
			                $panchyatList = array();
			                foreach($village_data as $row)
			                {
			                    array_push($panchyatList, $row['panchayat_id']);
			                }
			            }               
			           
			            $panchayatArr = "" . implode( "','", $panchyatList ) . "";
			           
			            $block_query = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");

			            $block_data = $block_query->result_array();
			           
			            if(count($block_data) > 0)
			            {
			            	$blockList = array();
			                foreach($block_data as $row)
			                {
			                    array_push($blockList, $row['block_id']);
			                }
			            }
			                   
			            $blockArr = "" . implode( "','", $blockList ) . "";

			            $dist_query = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE block_id IN('".$blockArr."')");
			           
			            $dist_data = $dist_query->result_array(); 

			            if(count($dist_data) > 0)
			            {
			                $districtList = array();
			                foreach($dist_data as $row)
			                {
			                    array_push($districtList, $row['district_id']);
			                }
			            }
			           
			            $outputString = "" . implode( "','", $districtList ) . "";  

			            $district_query = $this->db->query("SELECT district_id, district_name FROM survey_district_master WHERE district_id IN('".$outputString."')");
			            $district_data = $district_query->result_array(); 
			            
			        }
			        else{
			        	//echo $distData;
			        	$district_query = $this->db->query("SELECT district_id, district_name FROM survey_district_master WHERE district_id IN(".$distData.")
			        		AND is_deleted = '0'
			        		AND status = 'Active' ");
			        	//echo $this->db->last_query();
			            $district_data = $district_query->result_array(); 

			        
					}
				}*/
				
				//echo $this->db->last_query();
			    $district_data = $district_query->result_array(); 

				$resstr = '';
				if(sizeof($response_header_data) > 0){
					$resstr.= json_encode($response_header_data);
				}
				if(sizeof($district_data) > 0){
					$resstr.= '$$$'.json_encode($district_data);
				}
				echo $resstr;

			
		}
		else{
			//echo 'else'; die;
			$this->data['page_title']      = 'Response Listing';//$this->module_title." List";
			$this->data['module_title']    = $this->module_title;
			$this->data['module_name']     = 'Survey';
			$this->data['submodule_name']  = 'Survey_list';
			$this->data['redirect_from']      = 'show_responses';
			$this->data['response_header_data']     = $response_header_data;
			$this->data['middle_content']  = $this->module_view_folder."response_list";

			$this->load->view('admin/admin_combo',$this->data);
		}
	}


	public function get_village()
	{
		//error_reporting();
		$survey_id = $_POST['survey_id'];
		$district_id = $_POST['district_id'];
		$config_survey_id = $this->config->item('education_survey_id');
		
		if($district_id != ''){
			$where_claus = 'AND  d1.district_id = '.$district_id;
		}
		else{
			$where_claus = '';
		}
		
		if($survey_id == $config_survey_id)
		{
			//echo "++++++++++++++++";
			//$village_query = $this->db->query("SELECT village_id, village_name FROM survey_village_master WHERE panchayat_id > 0");
			
			$villageMaster = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_village_master WHERE panchayat_id > 0");
            $cntRow = $villageMaster->num_rows();
           
            if($cntRow > 0)
            {
                $panchyatList = array();
                foreach($villageMaster->result_array() as $row)
                {
                    array_push($panchyatList, $row['panchayat_id']);
                }
            }               
           
            $panchayatArr = "" . implode( "','", $panchyatList ) . "";
			
			//print_r($panchyatList);
           
            $query_1 = $this->db->query("SELECT DISTINCT block_id FROM survey_panchayat_master WHERE panchayat_id IN('".$panchayatArr."')");
           
            $countRes = $query_1->num_rows();
           
            if($countRes > 0)
            {
                $blockList = array();
                foreach($query_1->result_array() as $row)
                {
                    array_push($blockList, $row['block_id']);
                }
            }
                   
            $blockArr = "" . implode( "','", $blockList ) . "";			
            $query_2 = $this->db->query("SELECT DISTINCT district_id FROM survey_block_master WHERE block_id IN('".$blockArr."')");
           
            $countDistrict = $query_2->num_rows();           
            if($countDistrict > 0)
            {
                $districtList = array();
                foreach($query_2->result_array() as $row)
                {
                   
                    array_push($districtList, $row['district_id']);
                }
            }
           
            $outputString = "" . implode( "','", $districtList ) . "";               
         			
			$get_result = $this->db->query("SELECT block_id FROM survey_block_master WHERE district_id = '".$district_id."'");
			$countDistrict = $get_result->num_rows();
			
            if($countDistrict > 0)
            {
                $block_List = array();
                foreach($get_result->result_array() as $row)
                {
                    array_push($block_List, $row['block_id']);
                }
            }
			
			$block_ids = "" . implode( "','", $block_List ) . ""; 
			
			
			$panchayatQuery = $this->db->query("SELECT DISTINCT panchayat_id FROM survey_panchayat_master WHERE block_id IN('".$block_ids."')");
          
            $countPanchayat = $panchayatQuery->num_rows();
           
            if($countPanchayat > 0)
            {
                $panchList = array();
                foreach($panchayatQuery->result_array() as $row)
                {
                    array_push($panchList, $row['panchayat_id']);
                }
            }
			
			$panch_ids = "" . implode( "','", $panchList ) . ""; 
			
			$village_query = $this->db->query("SELECT village_id, village_name FROM survey_village_master WHERE panchayat_id IN('".$panch_ids."')");
          
			
		}
		else
		{
			$village_query = $this->db->query("SELECT v1.* 
				FROM survey_village_master v1 LEFT JOIN survey_revenue_master r1 ON v1.revenue_id = r1.revenue_id LEFT JOIN survey_district_master d1 ON d1.district_id = r1.district_id 
				WHERE v1.is_deleted = 0
				AND   v1.status = 'Active'
				".$where_claus."
				ORDER BY v1.village_name ASC");
				//echo "=======";
		}
		//echo $this->db->last_query();

		$village_data = $village_query->result_array();
		//echo @$district_id;
		//print_r($village_data);die();
		$wherestr = '';
		if(@$survey_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.survey_id = '.@$survey_id;
			}
			else{
				$wherestr.= ' WHERE shr.survey_id = '.@$survey_id;
			}
		}
		if(@$district_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.district_id = '.@$district_id;
			}
			else{
				$wherestr.= ' WHERE shr.district_id = '.@$district_id;
			}
		}

		$approver1_role_id = $this->config->item('approver1_role_id');
        $approver2_role_id = $this->config->item('approver2_role_id');
			
       /* if($this->session->userdata('role_id') == $approver1_role_id){
          	$wherestr.= ' AND (shr.status = "Submitted" OR shr.status = "Level 1 In Review" OR shr.status = "Level 1 Returned" OR shr.status =  "Level 1 Approved")';
        }*/

		//echo $wherestr;
		
		$response_headers_query = $this->db->query("SELECT shr.*, (SELECT COUNT(1) FROM survey_responses sr where shr.response_id = sr.response_id AND sr.is_deleted = '0') ques_cnt,  (SELECT title FROM survey_master s1 WHERE shr.survey_id = s1.survey_id) survey_name, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
				FROM survey_response_headers shr 
				".$wherestr." 
				ORDER BY shr.response_id DESC");

		//echo $this->db->last_query();
	
		$response_header_data = $response_headers_query->result_array();

		$resstr = '';
		if(sizeof($village_data) > 0){
			$resstr.= json_encode($village_data);
		}
		if(sizeof($response_header_data) > 0){
			$resstr.= '$$$'.json_encode($response_header_data);
		}
		echo $resstr;
	}

	public function get_datatable()
	{	
		//error_reporting();
		$survey_id 		= $_POST['survey_id'];
		$district_id 	= $_POST['district_id'];
		$village_id 	= $_POST['village_id'];
		$surveyer_id 	= $_POST['surveyer_id'];
		$status 		= $_POST['status'];
		
		
		/**************** Get User Assign District List ************************/
		
		$system_admin_id = $this->config->item('system_admin_id');
		$admin_role_id = $this->config->item('admin_role_id');
		$config_survey_id = $this->config->item('education_survey_id');
		
		$approver1_role_id = $this->config->item('approver1_role_id');
		$approver2_role_id = $this->config->item('approver2_role_id');		
		
	
		/*****************End User Assign District List *********************/


		//print_r($_POST);

		$wherestr = '';
		if(@$survey_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.survey_id = '.@$survey_id;
			}
			else{
				$wherestr.= ' WHERE shr.survey_id = '.@$survey_id;
			}
		}
		
		if(@$district_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.district_id = '.@$district_id;
			}
			else{
				$wherestr.= ' WHERE shr.district_id = '.@$district_id;
			}
		}
		else 
		{
			
			if($this->session->userdata('role_id') == $approver1_role_id || $this->session->userdata('role_id') == $approver2_role_id)
			{	
				
				$this->db->select('district_id');
				$where_con = array('is_deleted' => 0, 'status' => 'Active', 'user_id' => $this->session->userdata('user_id'));
				$approver_data = $this->master_model->getRecords("survey_users",$where_con);
						
				$distData = $approver_data[0]['district_id'];
				
				$explode = explode(",", $distData);
		
				if(count((array)$explode) > 0){
						
					$wherestr .= " AND (";
					$i = 0;
					foreach($explode as $t){
					
						if($i!=(count($explode) - 1)){
							$wherestr .= " FIND_IN_SET('".$t."',`district_id`) OR ";
							} else {
							$wherestr .= " FIND_IN_SET('".$t."',`district_id`))";
						}				
						$i++;
					}					
				} 
			}
		}
		
		if(@$village_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.village_id = '.@$village_id;
			}
			else{
				$wherestr.= ' WHERE shr.village_id = '.@$village_id;
			}
		}
		
		if(@$surveyer_id != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.surveyer_id = '.@$surveyer_id;
			}
			else{
				$wherestr.= ' WHERE shr.surveyer_id = '.@$surveyer_id;
			}
			
		}
		if(@$status != ''){
			if($wherestr != ''){
				$wherestr.= ' AND shr.status LIKE "%'.@$status.'%"';
			}
			else{
				$wherestr.= ' WHERE shr.status LIKE "%'.@$status.'%"';
			}
			
		}

		//echo $wherestr;
		$response_headers_query = $this->db->query("SELECT shr.*, (SELECT COUNT(1) FROM survey_responses sr where shr.response_id = sr.response_id AND sr.is_deleted = '0') ques_cnt,  (SELECT title FROM survey_master s1 WHERE shr.survey_id = s1.survey_id) survey_name, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
			FROM survey_response_headers shr ".$wherestr."
			ORDER BY shr.response_id DESC");

		//echo $this->db->last_query();
	
		$response_header_data = $response_headers_query->result_array();

		
		if(sizeof($response_header_data) > 0){
			$resstr = json_encode($response_header_data);
		}
		else{
			$resstr = '';
		}
		echo $resstr;
	}

    public function response_preview($response_id, $submodule_name)
	{

		// Permission Set Up
        //$this->master_model->permission_access('16', 'view');
        //error_reporting();

     	$response_id = base64_decode($response_id);
     	$submodule_name = base64_decode($submodule_name);
     	//echo $submodule_name; die;
     	//DATE_FORMAT(shr.start_date, '%d/%m/%Y') start_date, DATE_FORMAT(shr.end_date, '%d/%m/%Y') end_date

     	$response_headers_query = $this->db->query("SELECT shr.*,shr.start_date, shr.end_date, (SELECT concat(first_name,' ',last_name)fullname FROM survey_users u1 WHERE shr.surveyer_id = u1.user_id) surveyer_name, (SELECT district_name FROM survey_district_master d1 WHERE shr.district_id = d1.district_id) district_name, (SELECT revenue_name FROM survey_revenue_master r1 WHERE shr.revenue_id = r1.revenue_id) revenue_name, (SELECT block_name FROM survey_block_master b1 WHERE shr.block_id = b1.block_id) block_name, (SELECT panchayat_name FROM survey_panchayat_master p1 WHERE shr.panchayat_id = p1.panchayat_id) panchayat_name, (SELECT village_name FROM survey_village_master v1 WHERE shr.village_id = v1.village_id) village_name
				FROM survey_response_headers shr 
				WHERE shr.response_id = ".$response_id);

		$response_header_data = $response_headers_query->result_array();

		$survey_id = $response_header_data[0]['survey_id'];
		$status = $response_header_data[0]['status'];

		$question_query = $this->db->query("SELECT q1.survey_id, (SELECT svy.title FROM survey_master svy WHERE svy.survey_id = q1.survey_id) survey_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.section_id = sec.section_id) section_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.sub_section_id = sec.section_id) sub_section_name, q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt,  q1.is_mandatory, sm.global_question_id, q1.if_summation, q1.if_file_required, q1.file_label, q1.input_file_required, q1.help_label
		    FROM survey_question_master q1 LEFT JOIN survey_section_questions sm ON q1.survey_id = sm.survey_id 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od WHERE q1.question_id = od.dependant_ques_id)
            AND q1.question_id = sm.question_id
			AND q1.survey_id = ".$survey_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			GROUP BY q1.survey_id, sm.section_id, sm.sub_section_id, sm.sort_order
			ORDER BY q1.survey_id, sm.sort_order IS NOT NULL DESC, sm.sort_order ASC, sm.section_id, sm.sub_section_id, q1.question_id ASC");

		$question_data = $question_query->result_array();


     	//$tree = $this->buildTree($question_data[0]['question_id']);
		$tree = array();
		$last_section_name = '';
		$last_sub_section_name = '';
		$last_question_id = '';
		$label_mandatory = ''; 

		$key_name = array();
	    $answer_value = array();

	    $html='<input type="hidden" name="response_id" id="response_id" value="'.$response_id.'">';
	    $html.='<input type="hidden" name="survey_id" id="survey_id" value="'.$survey_id.'">';

	    //<div class="form-group col-md-6 col-sm-6 px-0 mb-1"><label class="control-label col-md-4">Surveyer ID: </label><span class="col-md-8">'.@$response_header_data[0]['surveyer_id'].'</span></div>

	    $caption = $this->config->item('education_survey_id');
	    if($caption == $survey_id){
	    	$response_label = 'Household Serial ID';
	    }
	    else{
	    	$response_label = 'Response ID';	
	    }

		$html.='<div class="row">
					<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">'.$response_label.'</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['response_id'].'</span>
                    </div>
                  	<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Surveyor Name</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['surveyer_name'].'</span>
                    </div>
                </div>

                <div class="row">
					<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Submitted Date</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['submitted_date'].'</span>
                    </div>
                  	<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Start Date/Time </label>
                    	 <span class="col-md-8">'.@$response_header_data[0]['start_date'].'</span>
                    </div>
                </div>

                <div class="row">

					<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">End Date/Time</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['end_date'].'</span>
                    </div>
                  	<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">District </label>
                    	 <span class="col-md-8">'.@$response_header_data[0]['district_name'].'</span>
                    </div>
                </div>';

				if($caption != $survey_id)
				{
					$html.= '<div class="row">
						<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
							<label class="control-label col-md-4">Revenue circle</label>
							<span class="col-md-8">'.@$response_header_data[0]['revenue_name'].'</span>
						</div>
						<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
							<label class="control-label col-md-4">Block </label>
							 <span class="col-md-8">'.@$response_header_data[0]['block_name'].'</span>
						</div>
					</div>';
				}	
				else 
				{
					$html.= '<div class="row">						
								<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
									<label class="control-label col-md-4">Block </label>
									 <span class="col-md-8">'.@$response_header_data[0]['block_name'].'</span>
								</div>
							</div>';
				}					
               

               $html.= '<div class="row">
					<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Goan Panchayat</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['panchayat_name'].'</span>
                    </div>
                  	<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Village </label>
                    	 <span class="col-md-8">'.@$response_header_data[0]['village_name'].'</span>
                    </div>
                </div>

                <div class="row">
					<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Latitude</label>
                    	<span class="col-md-8">'.@$response_header_data[0]['latitude'].'</span>
                    </div>
                  	<div class="form-group col-md-6 col-sm-6 px-0 mb-1">
                    	<label class="control-label col-md-4">Longitude</label>
                    	 <span class="col-md-8">'.@$response_header_data[0]['longitude'].'</span>
                    </div>
                </div>
                <hr>';

        //$html.= '<div id="show_preview" style="display:none;">';   

        $status_query = $this->db->query("SELECT (select concat(u.first_name,' ',u.last_name)user_name from survey_users u where u.user_id = r.user_id) user_name, status, date_time FROM survey_response_status r
	        	WHERE  r.response_id = ".$response_id."
                AND    r.survey_id = ".$survey_id);        

	    $status_data = $status_query->result_array();

	    $html.='<div class="box-header with-border">
		            <div class="pull-right box-tools">
		                <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="collapse" data-target="#collapseExample">
		                 <i class="fa fa-plus"></i></button>
		            </div>
		            <h3 class="box-title">Status History</h3>
		        </div>';
	    $html.='<div class="col-md-12 collapse" id="collapseExample">';
	    $html.='<table id="example1"  class="table table-bordered dt-responsive table-hover" width="100%">
	   			<thead> 
	   				<tr>
	           			<th width="1%">Sr.</th>          
	                    <th width="15%">User</th>
	                    <th width="20%">Status</th>
	                    <th width="20%">Date/Time</th>
	                </tr>
	            </thead>
	            <tbody>';
	            foreach ($status_data as $key => $status) {
	            	$index = $key+1;
	            	$html.= '<tr>
	            				<td>'.$index.'</td>
	              				<td>'.$status['user_name'].'</td>
	              				<td>'.$status['status'].'</td>
	              				<td>'.$status['date_time'].'</td>
	              			</tr>';
	           	}
	    $html.= 	'</tbody>
	    		</table>';
	    $html.='</div>';
           
		$html.= '<span style="color: blue; font: bold;"> Survey : &nbsp;&nbsp;'.@$question_data[0]['survey_name'].'</span>
          <hr>  ';

     	//$tree = $this->buildTree($question_data);
     	if(sizeof($question_data) >0){
     		foreach ($question_data as $key => $value) {
     			$sr = $key+1;

     			if($last_section_name <> $value['section_name']) {
     				$html.= '<hr>
	     				<span style="color: green; font: bold;"> Section : &nbsp;&nbsp;'.$value['section_name'].'</span>
	                  	<hr>';	

                    
	                $last_section_name = $value['section_name'];   	
	     			$last_sub_section_name = '';
     			}

     			if($last_sub_section_name <> $value['sub_section_name']) {
     				if($value['sub_section_name'] !=''){
	                	$html.= '<hr>
	                		<span style="color: #aa42f5; font: bold;"> Sub Section : &nbsp;&nbsp;'.$value['sub_section_name'].'</span>
	     				<hr>';
	     			}

     				if($value['sub_section_name']!=''){
	     				$last_sub_section_name = $value['sub_section_name']; 
	     			}
     			}

     			//Parent
     			$html.='<div class="row border border-info px-3 pt-3 mb-3"> 

                  			<div class="col-12">
        
                    			<div class="form-group">';
     			
     			if($value['is_mandatory'] == 1) { 
                	$label_mandatory = '<span style="color: red">*</span>'; 
            	}
            	else{
            		$label_mandatory = '';
            	}

            	
	    		$html.='<input type="hidden" name="parent_question_id" id="question_id" value="'.$value['question_id'].'">';
	    
     			$html.='<label class="w-100" for= description">'.'Q.'.$sr.'&nbsp;&nbsp;'.ucfirst($value['question_text']).$label_mandatory.'&nbsp;&nbsp;';
     			if($value['help_label'] != '') { $html.='<i class="fas fa-info-circle"><span class="tooltiptext">'.$value['help_label'].'</span></i>'; }
     			$html.='</label>';
     			
     			$html.='<input type="hidden" id="help_label" value="'.$value['help_label'].'">';
				// SQL Modified By Vicky
     			$response_query = $this->db->query("SELECT sr.*
				FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
				WHERE sr.is_deleted = '0'
				AND srh.response_id = ".$response_id."
				AND srh.survey_id = ".$survey_id." 
				AND sr.question_id = ".$value['question_id']." ORDER BY sr.id DESC"); 

				$response_data = $response_query->result_array();
				//echo "<pre>";print_r($response_data); 

	 			if($value['option_cnt'] >0 ){
	                $where = array('question_id' => $value['question_id']);
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, op.validation_id, op.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt FROM survey_questions_options_master op WHERE op.question_id=".$value['question_id']."
	                	ORDER BY op.display_order ASC");

	                $option_data = $option_query->result_array();

	             
	            }
	            else{
	            	$option_data = [];
	            }
	            //echo $value['global_question_id'];

	            $html_input = $this->get_html_tags($value['response_type_id'], $value['question_id'], $option_data, $value['salutation'], $value['if_summation'], $value['if_file_required'], $value['file_label'], $response_data, $value['input_file_required']);

	            $html.= $html_input;

	            foreach ($option_data as $option_key => $option_value) {
                	if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
                		if($value['response_type_id'] == 'radio' ||  $value['response_type_id'] == 'single_select'){
                			if(@$response_data[0]['answer_value'] == $option_value['option']){
	                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $value['question_id'], $response_id, $sr);

		                		if($option_dependant){
				     				$html.= $option_dependant;
				     			}
				     		}
				     	}
				     	else if($value['response_type_id'] == 'checkbox'){
				     		foreach ($response_data as $responsekey => $responsevalue) {
				     			if(trim(@$responsevalue['key_name']) == 'value'){
							
									$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $value['question_id'], $response_id, $sr);

			                		if($option_dependant){
					     				$html.= $option_dependant;
					     			}
								}
								else if(trim(@$responsevalue['key_name']) == trim($option_value['option'])){
		                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $value['question_id'], $response_id, $sr);

			                		if($option_dependant){
					     				$html.= $option_dependant;
					     			}
						     	}
					     	}
				     	}
				     	else{
				     		if(@$response_data[0]['answer_value']){
	                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $value['question_id'], $response_id, $sr);

		                		if($option_dependant){
				     				$html.= $option_dependant;
				     			}
				     		}
				     	}
                	}
                }

     			$child = $this->buildTree($survey_id, $value['question_id'], $sr, $response_id);
     			//echo '<pre>';
     			if($child){
     				//$value['sub_question'] = $child;
     				$html.= $child;
     				//echo $html;
     			}

     			if($value['global_question_id'] > 0){
	            	/*$html.="</div>
                  	</div>
                </div>";

                $html.='<div class="row">

                  			<div class="col-12">
        
                    			<div class="form-group">';*/


	            	$where = array('q1.question_id' => $value['global_question_id']);
	                $global_question_data = $this->master_model->getRecords("survey_question_master q1",$where, "q1.*");

	            	if(isset($global_question_data)){

	            		$html.='<input type="hidden" name="global_question_id" id="question_id" value="'.$global_question_data[0]['question_id'].'">';

	            		$html.='<label for="description">'.ucfirst($global_question_data[0]['question_text']).'</label>';

		                $where1 = array('question_id' => $global_question_data[0]['question_id']);

		                $response_query = $this->db->query("SELECT sr.*
							FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
							WHERE sr.is_deleted = '0'
							AND srh.response_id = ".$response_id."
							AND srh.survey_id = ".$survey_id." 
							AND sr.question_id = ".$global_question_data[0]['question_id']."
							AND sr.parent_question_id = ".$value['question_id']); //
		                //$html.=$this->db->last_query();

						$response_data = $response_query->result_array();
						$this->db->order_by('op.display_order','ASC');
		                $global_option_data = $this->master_model->getRecords("survey_questions_options_master op",$where1,"op.ques_option_id, op.question_id, op.dependant_ques_id, op.option, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, 
							(SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
							op.min_value, op.max_value, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt");

		                $html_input = $this->get_html_tags($global_question_data[0]['response_type_id'], $global_question_data[0]['question_id'], $global_option_data, $global_question_data[0]['salutation'], $global_question_data[0]['if_summation'], $global_question_data[0]['if_file_required'], $global_question_data[0]['file_label'], $response_data, $global_question_data[0]['input_file_required']);

		            	//echo 'Global'.$global_question_data[0]['dependant_cnt'];
		            }

		            $html.= $html_input;
	            }

	            $remark_query = $this->db->query("SELECT r1.*, (SELECT concat(u1.first_name,' ',u1.last_name)fullname FROM survey_users u1 WHERE u1.user_id = r1.user_id) user_name 
				FROM survey_response_remark r1
				WHERE r1.survey_id   = ".$survey_id."
				AND   r1.response_id = ".$response_id."
				AND   r1.question_id = ".$value['question_id']." 
				ORDER BY r1.remark_id ASC");

			//AND   r1.question_id = ".$question_data[0]['question_id']." 
			$remark_data = $remark_query->result_array();
			//print_r($remark_data); die;
			if(count($remark_data) >0 ){
				/* <th width="10%">Response ID</th>
				                    <th width="10%">Survey ID</th>*/
	            $html.='<label>Remark History</label>';
	            $html.='<table id="example1"  class="table table-bordered dt-responsive table-hover" width="100%">
	               			<thead> 
	               				<tr>
			               			<th width="1%">Sr.</th>          
				                    <th width="15%">User</th>
				                    <th width="20%">Remark</th>
				                    <th width="20%">Date/Time</th>
			                    </tr>
			                </thead>
			                <tbody>';
			            foreach ($remark_data as $remark_key => $remark_value) {
			            	$index = $remark_key+1;
			            	$html.= '<tr>
			            				<td>'.$index.'</td>
	                      				<td>'.$remark_value['user_name'].'</td>
	                      				<td>'.$remark_value['remark'].'</td>
	                      				<td>'.$remark_value['date_time'].'</td>
	                      			</tr>';
			                
		               	}
		        $html.= 	'</tbody>
		        		</table>';
		        }

	            $html.='<div class="form-check notok_span_class"><input class="form-check-input notok_class" type="checkbox" value="'.$value['question_id'].'"> <span class="form-check-label">&nbsp;<b>Flag</b></span> </div>';
	           	$html.='<div class="show_remark_'.$value['question_id'].' rmrk_cls" style="display:none;">';
	            $html.= '<label>Remark</label>';
				$html.= '<textarea class="form-control form-group remark_cls" id="'.$value['question_id'].'" name="remark_'.$value['question_id'].'" placeholder="Enter Remark" rows="2"></textarea>';
     			$html.="</div> ";
				$html.= '<span id="show_remark_error_'.$value['question_id'].'" style="color:red;"></span>';
     			$html.="</div>
                	</div>
                </div>";

            }	

     	}

     	/*$last_status_resubmitted = $this->db->query("SELECT status FROM survey_response_headers WHERE  response_id = ".$response_id."
                    AND survey_id = ".$survey_id." AND (status!='Submitted' OR status!='Re-Submitted')
                    AND is_deleted = '0'");        
        $resResubmitted = $last_status_resubmitted->num_rows();
         //echo $this->db->last_query();
        if($resResubmitted > 0)
        {
            $this->data['last_status']  = '';
        }
        else 
        {*/
            $last_status_query = $this->db->query("SELECT rs.*, concat(u1.first_name,' ',u1.last_name) fullname FROM  survey_response_status rs  LEFT JOIN survey_users u1 ON u1.user_id = rs.user_id
            WHERE  rs.response_id = ".$response_id."
            AND   rs.survey_id = ".$survey_id." 
            ORDER BY rs.status_id DESC LIMIT 1");
             //echo $this->db->last_query();

            $last_status = $last_status_query->result_array();

            if(sizeof(@$last_status) >0 ){
                $this->data['last_status']  = @$last_status;
            }
            else{
                $this->data['last_status']  = '';
            }


            $last_header_status_query = $this->db->query("SELECT status FROM survey_response_headers WHERE  response_id = ".$response_id."
                    AND survey_id = ".$survey_id."
                    AND is_deleted = '0'");        

	        $last_header_status = $last_header_status_query->result_array();
	         //echo $this->db->last_query();
	        if(count($last_header_status) > 0)
	        {
	            $this->data['last_header_status']  = $last_header_status;
	        }

	        
        //}

   		/*$last_status_query = $this->db->query("SELECT rs.*, concat(u1.first_name,' ',u1.last_name) fullname FROM  survey_response_status rs  LEFT JOIN survey_users u1 ON u1.user_id = rs.user_id
			WHERE  rs.response_id = ".$response_id."
			AND   rs.survey_id = ".$survey_id." 
			ORDER BY rs.status_id DESC LIMIT 1");
		 //echo $this->db->last_query();


		$last_status = $last_status_query->result_array();

		if(sizeof(@$last_status) >0 ){
			$this->data['last_status']  = @$last_status;
		}
		else{
			$this->data['last_status']  = '';
		}*/
     	//$html.= '</div>';
     	$this->data['survey_id']       	= $survey_id;
     	$this->data['html']       		= $html;
     	$this->data['status']           = $status;
     	$this->data['page_title']       = 'Survey Response';
		$this->data['module_name']      = 'Survey';
		$this->data['submodule_name']   = $submodule_name;
		$this->data['mode']     		= 'survey_view'; 
        $this->data['middle_content']   = $this->module_view_folder.'preview_response';
		//echo $submodule_name; die;
		$this->load->view('admin/admin_combo',$this->data);
	}

	public function get_dependant_question($survey_id, $option_id, $label_option, $question_id, $response_id, $srno){

		//error_reporting();
		$option_dependant_html = '';

		$question_query = $this->db->query("SELECT q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory, q1.salutation, q1.if_summation, q1.if_file_required, q1.file_label, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE optn.validation_id = v1.validation_id) validation_title, optn.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE optn.sub_validation_id = v2.sub_validation_id) sub_validation_title, optn.min_value, optn.max_value, optn.validation_label, optn.display_order, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt, q1.input_file_required
		FROM   survey_question_master q1  LEFT JOIN survey_option_dependent od ON od.dependant_ques_id = q1.question_id LEFT JOIN survey_questions_options_master optn ON optn.question_id = q1.question_id
		WHERE  od.ques_option_id = ".$option_id."
		AND   q1.is_deleted = 0 
		AND   q1.status = 'Active'
		GROUP BY q1.question_id 
		ORDER BY q1.question_id ASC");
		 //echo $this->db->last_query();
		$question_data = $question_query->result_array();
		$label_mandatory = ''; 
		if(isset($question_data) && sizeof($question_data) >0){

			foreach ($question_data as $ques_key => $question) {

				$key_srno = $ques_key+1;
		   		$dependant_sr = $srno.'.'.$key_srno;

		   		/*$option_dependant_html.="</div>
                </div>";

				$option_dependant_html.='<div class="col-12">
        
                    			<div class="form-group">';*/

	                if($question['is_mandatory'] == 1) { 
	                	$label_mandatory = '<span style="color: red">*</span>'; 
	            	}
	            	else{
	            		$label_mandatory = '';
	            	}

	            	$option_dependant_html.='<input type="hidden" name="option_dependant_question_id" id="question_id" value="'.$question['question_id'].'">';
               
     				$option_dependant_html.='<label class="w-100" for= description">Q. '.$dependant_sr.'&nbsp;&nbsp;'.ucfirst($question['question_text']).$label_mandatory.'</label>';


     				//$condition = ';

     				$response_query = $this->db->query("SELECT sr.*
					FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
					WHERE sr.is_deleted = '0'
					AND srh.response_id = ".$response_id."
					AND srh.survey_id = ".$survey_id." 
					AND sr.question_id = ".$question['question_id']."
					AND sr.parent_question_id =".$question_id);

					//$option_dependant_html.='----------'.$question_id; 

					$response_data = $response_query->result_array();

					if($question['option_cnt'] >0 ){
						$option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title, 
							CASE WHEN op.sub_validation_id = 1 THEN '>' 
							     WHEN op.sub_validation_id = 2 THEN '>=' 
							     WHEN op.sub_validation_id = 3 THEN '<'
							     WHEN op.sub_validation_id = 4 THEN '<='
							     WHEN op.sub_validation_id = 5 THEN '=='
							     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition, 
							op.min_value, op.max_value, op.validation_label, op.dependant_ques_id, op.option, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$question['question_id']."
							ORDER BY op.display_order ASC");

				        $option_data = $option_query->result_array();
				    }
				    else{
		            	$option_data = [];
		            }

	            $html_tag = $this->get_html_tags($question['response_type_id'], $question['question_id'], $option_data, $question['salutation'], $question['if_summation'], $question['if_file_required'], $question['file_label'], $response_data, $question['input_file_required']);

	 			$option_dependant_html.= $html_tag;

	 			/*foreach ($option_data as $option_key => $option_value) {
                	if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
	                	foreach (@$response_data as $resp_key => $resp_value) {
	                		if(@$question['response_type_id'] == 'radio' || @$question['response_type_id'] == 'single_select'){
			  					if(@$resp_value['answer_value'] == $option_value['option']){
		                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'], $response_id, $dependant_sr);

			                		if($option_dependant){
					     				$option_dependant_html.= $option_dependant;
					     			}
					     		}
					     	}
					     	else if($question['response_type_id'] == 'checkbox'){
					     		foreach ($response_data as $responsekey => $responsevalue) {
					     			if(trim(@$responsevalue['key_name']) == 'value'){
								
										$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $question['question_id'], $response_id, $dependant_sr);

				                		if($option_dependant){
						     				$option_dependant_html.= $option_dependant;
						     			}
									}
									else if(trim(@$responsevalue['key_name']) == trim($option_value['option'])){
			                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $question['question_id'], $response_id, $dependant_sr);

				                		if($option_dependant){
						     				$option_dependant_html.= $option_dependant;
						     			}
							     	}
						     	}
					     	}
					     	else{
					     		if(@$resp_value['answer_value']!=''){
		                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'],$response_id, $dependant_sr);

			                		if($option_dependant){
					     				$option_dependant_html.= $option_dependant;
					     			}
					     		}
					     	}
			     		}
			     	}
            	}*/
               
			   foreach ($option_data as $option_key => $option_value) {
                	
            		if(@$question['response_type_id'] == 'radio' || @$question['response_type_id'] == 'single_select'){
                		foreach (@$response_data as $resp_key => $resp_value) {	
			  				if(@$resp_value['answer_value'] == $option_value['option']){
			  					if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
		                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'], $response_id, $dependant_sr);

			                		if($option_dependant){
					     				$option_dependant_html.= $option_dependant;
					     			}
					     		}
					     	}
				     	}
				    }
				    else if($question['response_type_id'] == 'checkbox'){
			     		foreach ($response_data as $responsekey => $responsevalue) {
			     			if(trim(@$responsevalue['key_name']) == 'value'){
			     				if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
							
									$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $question['question_id'], $response_id, $dependant_sr);

			                		if($option_dependant){
					     				$option_dependant_html.= $option_dependant;
					     			}
								}
							}
							else if(trim(@$responsevalue['key_name']) == trim($option_value['option'])){
								if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
									$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $question['question_id'], $response_id, $dependant_sr);

			                		if($option_dependant){
					     				$option_dependant_html.= $option_dependant;
					     			}
					     		}
						    }
						}
					}
			     	else{
			     		if(@$resp_value['answer_value']!=''){
			     			if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
	                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'],$response_id, $dependant_sr);

		                		if($option_dependant){
				     				$option_dependant_html.= $option_dependant;
				     			}
				     		}
			     		}
			     	}//else
			     		
			  	}

                /*$option_dependant_html.="</div>
                </div>";*/

 			}
 			
 			//echo json_encode($question_array);

     	}
		return $option_dependant_html;
	}

	public function get_html_tags($tag_name, $question_id, $option_array, $salutation, $if_summation, $if_file_required, $file_label, $response_data, $input_file_required){
		//echo $question_id.'--------'.$dependant_cnt.'</br>';
		$str = '';
		$length = '';
		$salutation_flag =0;
		//print_r($response_data); die;

		if($salutation!='' && $salutation!='0'){
			//$str.='-----'.$response_data[0]["salultation_value"];
			$str.= '</div> </div>';
			$salutation_flag =1;
			$salutation_array = explode('|', $salutation);
			if(sizeof($salutation_array)>0){
				$str.= '<div class="col-3"> <div class="form-group">';
				$str.= '<select class="form-control salutation_class">
		                    <option value="">Select Salutation</option>';
				foreach ($salutation_array as $id) {
					$selected = "";
					if(@$response_data[0]["salultation_value"] == $id) 
					{  
						$selected =  "selected='selected'"; 
					}
					
					$str.= '<option value="'.$id.'"  '.$selected.'>'.$id.'</option>';	
				}
				$str.= '</select>';
				$str.= '</div> </div>';
			}
			$str.='<div class="col-12"> <div class="form-group">';
		}

		if($tag_name == 'radio'){
			$checked = '';
			
			if(sizeof($option_array) >0){
				//echo "<pre>";print_r($response_data);
				//$str.= $question_id."===".$response_data[0]['answer_value'];
				foreach ($option_array as $key => $value) {
					//$str.= $value['option']."====".$response_data[0]['answer_value'];
					//$str.= "====".$response_data[0]['answer_value'];
					$str.='<form name="" class="radio_form"><div class="form-check form-check-inline">';
					$str.= '<input class="form-check-input dependantcls" type="radio" id="'.$question_id.'" data-id="'.$value['ques_option_id'].'" name="radio_'.$question_id.'" value="'.$value['option'].'" ';
					if(trim($value['option']) == trim(@$response_data[0]['answer_value'])){
						//$str.= 'hello';
						$str.= $checked = "checked='checked'"; 
					}
					else{
						$str.= $checked ='';
					}

					$str.='> &nbsp;';

					//$str.='</br>';
					
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div></form>';	
				}
			}	

			$str.='</div> </div>';
		           $str.=' <div class="col-12">
		               <div class="form-group mx-0">';

			/*foreach ($option_array as $key => $value) {
				$str.= '</div>
		                </div>
		                  <div class="col-6 radio_dependant_div_'.$question_id.'_'.$value['ques_option_id'].'" style="display:none;">';
				if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

					for ($i=0; $i < $value['dependant_cnt']; $i++) { 
						$j = $i+1;    
		                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
		               
		           }
		          
	           }
	            
           }
           $str.='<input type="hidden" id="prev_radio_'.$question_id.'" value="">';*/
		}
		elseif($tag_name == 'checkbox'){
			$response_arr = array();
			foreach ($response_data as $resp_key => $resp_value) {
				$response_arr[] = $resp_value['key_name'];
			}

			if(sizeof($option_array) >0){
				$flag_checked ='';
				$chk = false;
				foreach ($option_array as $key => $value) {
					$str.='<form method=""><div class="" id="disable_check" style="clear:left;">';
					$str.= '<input class=" dependantcls" type="checkbox" id="'.$value['ques_option_id'].'" data-id="'.$question_id.'" name="chk[]" value="'.$value['option'].'"';
					//echo "<pre>";print_r($response_data);
					foreach ($response_data as $resp_key => $resp_value) 
					{
						
						//if(trim(@$resp_value['key_name']) == 'value'){
						// && trim(@$resp_value['answer_value']) == 'Yes' Condition Added Below By Vicky On 25March2021 To Resolved Global Question Issue
						if(trim(@$resp_value['key_name']) == 'value' && trim(@$resp_value['answer_value']) == 'Yes'){	
							
							$str.= $flag_checked =  "checked='checked'"; 
							$chk = true; 
						}
						else if(trim(@$value['option']) == trim(@$resp_value['key_name']))
						{
							$chk = true; 
							$str.= $flag_checked =  "checked='checked'"; 
						}
						/*else{
							$str.= $flag_checked = '';
						}*/
					}
					
					if($chk == true)
					{	
						$chkChk = $this->db->query("SELECT parent_question_id, question_id FROM survey_responses WHERE is_deleted = '0' AND question_id = '".$question_id."' AND parent_question_id='".$response_data[0]['parent_question_id']."'");
						$flag_checked =  "checked='checked'"; 
					}
					$str.= ' > &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div> </form>';
					
					
					/*if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){
						$str.='<span class="checkbox_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
							$str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
						}
						$str.='</span></form>';
					}*/
					
				}
			}
			$str.='</div>';
			$str.='</div>';
			  $str.=' <div class="col-12">
		               <div class="form-group mx-0">';
	
		}
		elseif($tag_name == 'single_select'){

			$str.= '<select class="form-control dependantcls" id="'.$question_id.'">
	                    <option value="">Select Option Type</option>';

	        if(sizeof($option_array) >0){
		        foreach ($option_array as $key => $value) {
		        	$selected = "";
		        	if($value['option'] == @$response_data[0]['answer_value']){
						$selected =  "selected='selected'"; 
					}

					$str.= '<option value="'.$value['option'].'" id="'.$value['question_id'].'" data-id="'.$value['ques_option_id'].'" '.$selected.'>'.$value['option'].'</option>';
				}
			}
			$str.= '</select>';

			$str.='</div> </div>';
		           $str.=' <div class="col-12">
		               <div class="form-group mx-0">';

			/*if(sizeof($option_array) >0){
				foreach ($option_array as $key => $value) {
					$str.= '</div>
			                </div>
			                  <div class="col-6 dropdown_dependant_div_'.$question_id.'_'.$value['ques_option_id'].'" style="display:none;">';
					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			          
		           }
		            $str.='</div>';
			           $str.=' <div class="col-12">
			               <div class="form-group m-0">';
	           }
			}



			$str.='<input type="hidden" id="prev_select_'.$question_id.'" value="">';*/
		}
		elseif($tag_name == 'textbox'){
			//$str.=$salutation_flag;
			
			if(sizeof($option_array) >1){

				//if($salutation_flag = 0){
					//$str.= '</div> </div>';
				//}
				$str.= '<div class="row w-100">';
				foreach ($option_array as $key => $value) {
					//if($salutation_flag = 0){
					 $str.= '<div class="col-4"> <div class="form-group">';
					//}

					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{

						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}

					if($value['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					//$str.='a';

					//$str.='*****'.print_r($response_data);
					
					$textfield_value ='';
					$sum = 0;
					foreach (@$response_data as $resp_key => $resp_value) {
					
						if(trim($value['option']) == trim($resp_value['key_name'])){
							$textfield_value = $resp_value['answer_value'];
						}
						if($if_summation == 'Yes'){
							$sum = $sum + $resp_value['answer_value'];
						}
					}
				
					$str.='<label>'.$value['option'].'</label>';
					$str.= "<input type='text' ".$length." id='".$question_id."' data-optn-id='".$value['ques_option_id']."'" ;
					$str.="data-id='[".json_encode($option_array[$key])."]'";
					$str.="placeholder='".$value['option']."' value='".$textfield_value."' " ;
					if($if_summation == 'Yes'){
						$str.="class='form-control dependantcls calculate_total textfields_".$question_id."' ".$call_function.">";
					}
					else{
						$str.="class='form-control dependantcls' ".$call_function.">";
					}
					//if($salutation_flag = 0){
						//$str.= '</div> </div>';
					//}
					
					if(@$input_file_required == 'Yes'){
						foreach ($response_data as $resp_key => $resp_value) {
						
							if (preg_match('/(\.jpg|\.png|\.bmp|\.jpeg|\.gif)$/', @$resp_value['input_file_required'])) {
				   				$input_image_file = @$resp_value['input_file_required'];

				   				$str.= '<a href="'.base_url().'uploads/'.$input_image_file.'" target="_blank">Click Here</a>';

				   				$str.= '<div class="row">
					   						<div class="col-4">
					   							<label>Latitude: '.@$resp_value['latitude'].'</label>
					   						</div>
					   						<div class="col-4">
					   							<label>Longitude: '.@$resp_value['longitude'].'</label>
					   						</div>
				   						</div>';
							} 
							
						}
						
						//$str.= '<img src="'.base_url().'uploads/'.$input_image_file.'" width="100" height="100"> ';
					}
					$str.= '</div> </div>';
				}
				//$str.='--'.$if_summation;
				if($if_summation == 'Yes'){
			    	$str.='<div class="col-6"> <div class="form-group">';
			    	$str.='<label>Total</label>';
			    	$str.= "<input type='text' class='form-control total_cnt_class' id='total_cnt_class_".$question_id."'";
					$str.="placeholder='Total' readonly='readonly' value='".$sum."'>";
					$str.='</div></div>';
				}
				//if($salutation_flag = 0){
				    $str.= '</div>';
  
				//}

			}
			else{
				$textbox_text = '';
				if(@$response_data[0]['answer_value'] != ''){
					$textbox_text = @$response_data[0]['answer_value'];
				}
				
				//$str.='<div class="col-12"> <div class="form-group">';
				if(isset($option_array) && sizeof($option_array)>0){
					if($option_array[0]['validation_title'] == "Length" && $option_array[0]['min_value'] > 0)
					{

						if($option_array[0]['sub_validation_id'] == 18){
							$length =  'maxlength='.$option_array[0]['min_value'];
						}
						else if($option_array[0]['sub_validation_id'] == 23){
							$length =  'minlength='.$option_array[0]['min_value'];
						
						}	
					}

					if($option_array[0]['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					$str.= "<input type='text' class='form-control dependantcls' ".$length." id='".$question_id."'  data-optn-id='".$option_array[0]['ques_option_id']."' data-id='".json_encode($option_array)."' placeholder='Enter Answer' ".$call_function." value='".$textbox_text."'>";

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textbox_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			           $str.='</span>';
			          
		           }
				}
				
			}
			
			/*$str.='<span id="text_error_'.$question_id.'" style="color: red;"></span>';
			$str.='</br>';
			$str.='<span id="text_error_numonly_'.$question_id.'" style="color: red;"></span>';*/

			$str.= '</div></div>';
			$str.='<div class="col-12"> <div class="form-group mx-0">';
		}
		elseif($tag_name == 'file'){
			//$str.= '<input type="file" class="form-control" id="'.$question_id.'">';
			foreach ($response_data as $resp_key => $resp_value) {
			
				if (preg_match('/(\.jpg|\.png|\.bmp|\.jpeg|\.gif)$/', @$resp_value['answer_value'])) {
	   				$image = @$resp_value['answer_value'];

	   				$str.= '<a href="'.base_url().'uploads/'.$image.'" target="_blank">Click Here</a>';

	   				$str.= '<div class="row">
	   						<div class="col-4">
	   							<label>Latitude: '.@$resp_value['latitude'].'</label>
	   						</div>
	   						<div class="col-4">
	   							<label>Longitude: '.@$resp_value['longitude'].'</label>
	   						</div>
   						</div>';
				} 
				
			}

			$str.= '</div></div>';
			$str.='<div class="col-12"> <div class="form-group mx-0">';
			//$str.= '<img src="'.base_url().'uploads/'.$image.'" width="100" height="100"> ';
		}
		elseif($tag_name == 'textarea'){
			//print_r($option_array);
			if(@$response_data[0]['answer_value'] != ''){
				$textarea_text = @$response_data[0]['answer_value'];
			}
			else{
				$textarea_text = '';
			}
			if(sizeof($option_array) > 0)
			{

				$str.= '<textarea class="form-control form-group textarea_dependantcls" id="'.$question_id.'" data-optn-id="'.$option_array[0]['ques_option_id'].'" 
					data-id="'.$option_array[0]['if_sub_question_checked'].'" rows="2" "'.$length.'" >'.$textarea_text.'</textarea>';
				foreach ($option_array as $key => $value) {
					
				
					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{
						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}
					

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textarea_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';

			                $arr['if_sub_question_checked'] = $option_array[0]['if_sub_question_checked']; 
			               
			           }
			           $str.='</span>';
					
					}
				} 
			}
			else
			{
				$str.= '<textarea class="form-control form-group" id="'.$question_id.'" placeholder="Enter Answer" rows="2" /></textarea>';
			}
			
			//$str.= '<textarea class="form-control dependantcls" id="'.$question_id.'" placeholder="Enter Answer" rows="2"  /></textarea>';

			$str.= '</div></div>';
			$str.='<div class="col-12"> <div class="form-group mx-0">';
		}

		if($if_file_required == 'Yes'){
			$str.= '<label>'.$file_label.'</label>';
			if (preg_match('/(\.jpg|\.png|\.bmp|\.jpeg|\.gif)$/', @$response_data[0]['file_name'])) {
   				$image_file_required = @$response_data[0]['file_name'];

   				$str.= '<a href="'.base_url().'uploads/'.$image_file_required.'" target="_blank">Click Here</a>';

   				$str.= '<div class="row">
	   						<div class="col-4">
	   							<label>Latitude: '.@$response_data[0]['latitude'].'</label>
	   						</div>
	   						<div class="col-4">
	   							<label>Longitude: '.@$response_data[0]['longitude'].'</label>
	   						</div>
   						</div>';
			} 
			

			$str.= '</div></div>';
			$str.='<div class="col-12"> <div class="form-group">';
			
			//$str.= '<img src="'.base_url().'uploads/'.$image_file_required.'" width="100" height="100"> ';
		}
		/*$str.= '<label>Remark</label>';
		$str.= '<textarea class="form-control form-group" id="remark" name="remark_'.$question_id.'" placeholder="Enter Remark" rows="2"></textarea>';*/

		//echo $str.'</br>';

		return $str;
	}

	public function buildTree($survey_id, $question_id, $sr, $response_id){
		//error_reporting(E_ERROR | E_PARSE);
		//$tree = array();
		$branch = array();
		$sub_html = '';
		$html_input = '';
		$label_mandatory = '';

		$key_name = array();
	    $answer_value = array();

    	$sub_question_query = $this->db->query("SELECT  q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt, q1.is_mandatory, q1.if_summation, q1.if_file_required, q1.file_label, q1.input_file_required FROM survey_question_master q1 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.parent_question_id = ".$question_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			ORDER BY q1.question_id ASC");
    	
    	//echo '<pre>';

    	$sub_question_data = $sub_question_query->result_array();
    	
		if (sizeof($sub_question_data)>0) {
			//echo 'if'.$question_id;
		   foreach ($sub_question_data as $id => $sub_question) { 

		   	$key_sr = $id+1;
		   	$sub_sr = 'Q.'.$sr.'.'.$key_sr;

		   	/*$sub_html.="</div>
                </div>";

		   	$sub_html.='<div class="col-12">
        
                    			<div class="form-group">';*/

                if($sub_question['is_mandatory'] == 1) { 
                	$label_mandatory = '<span style="color: red">*</span>'; 
            	}
            	else{
            		$label_mandatory = '';
            	}

            	$sub_html.='<input type="hidden" name="sub_question_id" id="question_id" value="'.$sub_question['question_id'].'">';

		   	    $sub_html.='<label for="description" class="w-100">'.$sub_sr.'&nbsp;&nbsp;'.ucfirst($sub_question['question_text']).$label_mandatory.'</label>';

		   	    $response_query = $this->db->query("SELECT sr.*
				FROM survey_response_headers srh LEFT JOIN survey_responses sr ON srh.response_id = sr.response_id 
				WHERE sr.is_deleted = '0'
				AND srh.response_id = ".$response_id."
				AND srh.survey_id = ".$survey_id." 
				AND sr.question_id = ".$sub_question['question_id']); //

				$response_data = $response_query->result_array();
				//$response_data['answer_value'] = '';

	 			if($sub_question['option_cnt'] >0 ){
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$sub_question['question_id']."
	                	ORDER BY op.display_order ASC");

	                $option_data = $option_query->result_array();
	                
	                //$option_data = $option_query->result_array();
	            }
	            else{
	            	$option_data = [];
	            }

	            $html_input = $this->get_html_tags($sub_question['response_type_id'], $sub_question['question_id'], $option_data, $sub_question['salutation'], $sub_question['if_summation'], $sub_question['if_file_required'], $sub_question['file_label'], $response_data, $sub_question['input_file_required']);

	            $sub_html.= $html_input;

	            foreach ($option_data as $option_key => $option_value) {
                	if($option_value['if_sub_question_checked'] == 'Yes' && $option_value['dependant_cnt'] > 0){
                		if($sub_question['response_type_id'] == 'radio' || $sub_question['response_type_id'] == 'single_select'){
	                		if(@$response_data[0]['answer_value']!='' || @$response_data[0]['answer_value'] == $option_value['option']){
	                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'], $sub_sr);

		                		if($option_dependant){
				     				$sub_html.= $option_dependant;
				     			}
				     		}
				     	}
				     	else if($sub_question['response_type_id'] == 'checkbox'){
				     		foreach ($response_data as $responsekey => $responsevalue) {
				     			if(trim(@$responsevalue['key_name']) == 'value'){
							
									$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $sub_question['question_id'], $response_id, $sub_sr);

			                		if($option_dependant){
					     				$sub_html.= $option_dependant;
					     			}
								}
								else if(trim(@$responsevalue['key_name']) == trim($option_value['option'])){
		                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $sub_question['question_id'], $response_id, $sub_sr);

			                		if($option_dependant){
					     				$sub_html.= $option_dependant;
					     			}
						     	}
					     	}
				     	}
				     	else{
				     		if(@$response_data[0]['answer_value']!=''){
	                			$option_dependant = $this->get_dependant_question($survey_id, $option_value['ques_option_id'], $option_value['option'], $option_value['question_id'], $sub_sr);

		                		if($option_dependant){
				     				$sub_html.= $option_dependant;
				     			}
				     		}
				     	}
                	}
                }

	            $children = $this->buildTree($survey_id, $sub_question['question_id'],$key_sr, $response_id);

	          	if($children){
	          		//$sub_question['sub_question'] = $children;
	          		$sub_html.= $children;

	            }



	            //$branch[] = $sub_question;

	           /* $sub_html.="</div>
                </div>";*/
	        }
	       
	    }
		   
		//print_r($branch);
	    //return $branch;
	    //echo $sub_html;
	    return $sub_html;
		
	}

	public function change_review_status(){

		//$this->master_model->permission_access('8', 'status');
		
		$response_id 	= $_POST['response_id'];
		$status 		= $_POST['status'];
		
		if($status != ''){
			$insertArr = array('survey_id'		=> trim($this->input->post('survey_id')),
				               'response_id'	=> trim($this->input->post('response_id')),
				               'user_id'		=> $this->session->userdata('user_id'),
							   'status'			=> $status);

			$insertQuery = $this->master_model->insertRecord('survey_response_status', $insertArr);
			// Get Log Setting

			$updateQuery = $this->master_model->updateRecord('survey_response_headers',array('status'=> $status, 'updated_by_id' => $this->session->userdata('user_id'), 'updated_on' => date('Y-m-d H:i:s')),array('response_id' => $response_id));

			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $response_id, 'status' => $status);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			echo $insertQuery;
			 
		} 
		 
	}

	public function change_response_status(){

		error_reporting();
		//$this->master_model->permission_access('8', 'status');
		$response_id 	= trim($this->input->post('response_id'));
		$status 		= $_POST['status'];
		$survey_id 		= trim($this->input->post('survey_id'));
		
		//print_r($_POST); die;
		
		if($status != ''){
			 
			$insertArr = array('survey_id'		=> $survey_id,
				               'response_id'	=> $response_id,
				               'user_id'		=> $this->session->userdata('user_id'),
							   'status'			=> $status);

			$insertQuery = $this->master_model->insertRecord('survey_response_status', $insertArr);
			
			//if($status == 'Level 1 Returned' || $status == 'Level 2 Returned'){			
			// Added By Vicky 1st April 2021	
			if($status == 'Level 1 Returned' || $status == 'Level 2 Returned' || $status == 'Level 1 Approved' || $status == 'Level 2 Approved'){	
				if($insertQuery)
				{
					if(@$_POST['remarkArr'])
					{
						$remarkArr 		= $_POST['remarkArr'];
						foreach ($remarkArr as $key => $value) {

							$insertArr = array('survey_id'		=> $survey_id,
											   'response_id'	=> $response_id,
											   'status_id'		=> $insertQuery,	
											   'user_id'		=> $this->session->userdata('user_id'),
											   'question_id'	=> $value['question_id'],
											   'remark'			=> $value['remark'],
											   'date_time'		=> date('Y-m-d H:i:s'));

							$this->master_model->insertRecord('survey_response_remark',$insertArr);
						}
					}
				}
			}

			$updateQuery = $this->master_model->updateRecord('survey_response_headers',array('status'=> $status, 'updated_by_id' => $this->session->userdata('user_id'), 'updated_on' => date('Y-m-d H:i:s')),array('response_id' => $response_id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $response_id, 'status' => $status);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			if($insertQuery){
				if($status == 'Level 1 Approved' || $status == 'Level 2 Approved' || $status == 'Level 1 Returned' || $status == 'Level 2 Returned'){

					$status 		= $_POST['status'];

					$config_survey_id = $this->config->item('education_survey_id');

					if($config_survey_id == $survey_id){
						$survey_type = 'Education';
					}
					else{
						$survey_type = 'Village';
					}

					//echo $this->db->last_query();

					$user_query = $this->db->query("SELECT u1.email
			            	FROM survey_users u1
			            	WHERE u1.user_id = ".$this->session->userdata('user_id'));

					$user_data = $user_query->result_array();

					// Get Surveyor details for response
					$surveyor_query = $this->db->query("SELECT srh.*, (SELECT u1.email from survey_users u1 WHERE srh.surveyer_id = u1.user_id) email_id, (SELECT concat(u1.first_name,' ',u1.last_name) fullname from survey_users u1 WHERE srh.surveyer_id = u1.user_id) fullname, (SELECT device_token from survey_users u1 WHERE srh.surveyer_id = u1.user_id) device_token, (SELECT v1.village_id from survey_village_master v1 WHERE srh.village_id = v1.village_id) village_name
				            	FROM survey_response_headers srh WHERE srh.response_id =".$response_id." AND  srh.survey_id =".$survey_id);
		            $surveyor_data = $surveyor_query->result_array();	
					
					// Get Survey Details for response
					$survey_data = $this->master_model->getRecords('survey_master', array('survey_id'=>$survey_id));
					
			        if($status == 'Level 1 Returned' || $status == 'Level 2 Returned'){
			        
			        	if(strpos($status, '1') !== false){
						    $level = 1;
						} else{
						    $level = 2;
						}

		            	$slug = 'status_notification_to_surveyor';
    					$email_data = $this->master_model->getRecords("email_master", array("slug" => $slug));
						
		                $header_query = $this->db->query("SELECT shr.*, (SELECT v1.village_name from survey_village_master v1 where shr.village_id=v1.village_id) village_name FROM survey_response_headers shr
				    			WHERE  shr.response_id = ".$response_id."
			                    AND    shr.survey_id = ".$survey_id."
			                    AND    shr.is_deleted = '0'");   

				        $header_data = $header_query->result_array();

		               // $notification_body = 'Survey Response ID "'.$response_id.'" Rejected Please Correct and Resend.';
					   
		                $notification_body = @$survey_data[0]['title'].', Survey Response ID - "'.$response_id.'" Rejected... Please Correct and Resend.';

		                $push_notification = array('msg_title'=>'UNDP Survey', 'msg_body'=>$notification_body);

		                $responseArr = array();

		                $push_response = push_notification($surveyor_data[0]['device_token'], $push_notification);

		                $responseArr['push_response'] = $push_response;
		                $responseArr['survey_id'] = $survey_id;
		                $responseArr['response_id'] = $response_id;
		                $responseArr['approver_id'] = $this->session->userdata('user_id');


		                $pushArray = array(
	                        'user_agent'   		=>    $_SERVER,      
	                        'req'   		    =>    $status,
	                        'res'  				=>    json_encode($responseArr),
	                        'access_date'   	=>    date('Y-m-d H:i:s')
	                    );

		               
		                $this->db->query("INSERT INTO api_log(user_agent, req, res, access_date) VALUES('$_SERVER', '$status', '".json_encode($responseArr)."', '".date('Y-m-d H:i:s')."')");
		                

    					if(count($email_data) > 0){
	        				$from        		= $from_email;
	        				$email_subject      = $email_data[0]['email_subject'];
	        				$sub_words         = ['[LEVEL]'];
				            $sub_array         = [$level];      
				            $subject           = str_replace($sub_words, $sub_array, $email_subject); 
				            $email_body     	= $email_data[0]['email_body'];
				            $arr_words         = ['[SURVEYOR]', '[LEVEL]', '[SUVRYE_TYPE]', '[VILLAGENAME]', '[SURVEY_ID]'];
				            $rep_array         = [$surveyor_data[0]['fullname'], $level, $survey_type, $header_data[0]['village_name'], $survey_id];       
				             
				            $content           = str_replace($arr_words, $rep_array, $email_body); 
				        }

		                //print_r($surveyor_data);

		                $to= $surveyor_data[0]['email_id'];

		                $contentArray = array(
	                        'to'   		=>    $to,      
	                        'cc'   		=>    '',
	                        'from'  	=>    $from_email,
	                        'subject'   =>    $subject,
	                        'view'      =>    'common-file'
	                    );

	                    $other_info    =    array('content'=>$content);

	                    //print_r($contentArray);
			
			            $emailsend     =     $this->emailsending->sendmail($contentArray,$other_info);
			            //echo $emailsend;	
			            if($emailsend){
			            	echo $emailsend;
			            }
			            else{
			            	echo $insertQuery;
			            }
					}
					else if($status == 'Level 1 Approved'){

					
						// Push Notification
						$notification_body = @$survey_data[0]['title'].', Survey Response ID - "'.$response_id.'" Approved... Thank You.';
						$push_notification = array('msg_title'=>'UNDP Survey', 'msg_body'=>$notification_body);
						$push_response = push_notification($surveyor_data[0]['device_token'], $push_notification);

						$responseArr = array();
						$responseArr['push_response'] = $push_response;
						$responseArr['survey_id'] = $survey_id;
						$responseArr['response_id'] = $response_id;
						$responseArr['approver_id'] = $this->session->userdata('user_id');
						
						$pushArray = array(
											'user_agent'   	=>  $_SERVER,      
											'req'   		=>  $status,
											'res'  			=>  json_encode($responseArr),
											'access_date'   =>  date('Y-m-d H:i:s')
										);					   
						$this->db->query("INSERT INTO api_log(user_agent, req, res, access_date) VALUES('$_SERVER', '$status', '".json_encode($responseArr)."', '".date('Y-m-d H:i:s')."')");
						
					
    					if($config_survey_id != @$survey_id){

    						/*if(strpos($status, '1') !== false){
							    $level = 1;
							} else{
							    $level = 2;
							}*/

							//echo $level;
							
							
    						// Email Functionality Start
							$slug = 'status_notification_to_approver';
    						$email_data = $this->master_model->getRecords("email_master", array("slug" => $slug));

    						$approver2_role_id = $this->config->item('approver2_role_id');

				    		$approver2_query = $this->db->query("SELECT u1.*, concat(u1.first_name,' ', u1.last_name) fullname, (SELECT v1.village_id from survey_village_master v1 WHERE u1.village_id = v1.village_id) village_name
				            	FROM survey_users u1
				            	WHERE u1.role_id = ".$approver2_role_id);

				    		$approver2_data = $approver2_query->result_array();

				    	
				    		$header_query = $this->db->query("SELECT shr.*, (SELECT v1.village_name from survey_village_master v1 where shr.village_id=v1.village_id) village_name FROM survey_response_headers shr
				    			WHERE  shr.response_id = ".$response_id."
			                    AND    shr.survey_id = ".$survey_id."
			                    AND    shr.is_deleted = '0'");   

				    		$header_data = $header_query->result_array();

							$from_email = $config_survey_id = $this->config->item('from_email');

							foreach ($approver2_data as $key => $value) {
								if(count($email_data) > 0){
									//echo $value['fullname'];
			        				$from        	   = $from_email;
			        				$email_subject     = $email_data[0]['email_subject'];
			        				$sub_words         = ['[LEVEL]'];
						            $sub_array         = [1];    
						            $subject           = str_replace($sub_words, $sub_array, $email_subject);   
						            $email_body        = $email_data[0]['email_body'];

						            $arr_words         = ['[APPROVER]', '[SUVRYE_TYPE]', '[VILLAGENAME]', '[SURVEY_ID]'];
						            $rep_array         = [$value['fullname'], $survey_type, $header_data[0]['village_name'], $survey_id];       
						             
						            $content           = str_replace($arr_words, $rep_array, $email_body); 
						        }


								$contentArray = array(
			                        'to'   		=>    $value['email'],      
			                        'cc'   		=>    '',
			                        'from'  	=>    $from_email,
			                        'subject'   =>    $subject,
			                        'view'      =>    'common-file'
			                    );

			                    $other_info    =    array('content'=>$content);

			                    //print_r($contentArray);

			                    $res = $this->sendemail_function($contentArray,$other_info);
			                    //die;

			                   /* if($res){
			                    	return true;
			                    }
			                    else{
			                    	return false;
			                    }*/

			                    echo $insertQuery;
					
					            /*$emailsend     =     $this->emailsending->sendmail($contentArray,$other_info);

					            echo $emailsend;*/
							}

							
				    	}// village survey

				    	else{
					    	echo $insertQuery;
						    //echo $to;
					    }

    				
				    }// level 1 approve
				    else{
				    	echo $insertQuery;
				    }
				   
      
				}
			} // insertquery

			//echo $insertQuery;
			 
		} 
		 
	}

	public function sendemail_function($contentArray,$other_info){
		//print_r($contentArray);
		$emailsend     =     $this->emailsending->sendmail($contentArray,$other_info);
		if($emailsend){
			return true;
		}
		else{
			return false;
		}
	}

	public function changeStatus(){
		$this->master_model->permission_access('8', 'status');
		$response_id 	= $_POST['response_id'];
		$status 	= $_POST['status'];
		
		if($status != ''){
			 
			$updateQuery = $this->master_model->updateRecord('survey_response_headers',array('status'=> $status),array('response_id' => $response_id));
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $response_id, 'status' => $status);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			if($updateQuery == 1){
				echo 'Status Changed Successfully.';
			}	
			 
		} 
		 
	}

}	