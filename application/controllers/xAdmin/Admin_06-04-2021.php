<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin for admin basic function
Author : Vicky K
*/

class Admin extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->library("session");
        $this->load->helper('captcha');
		$this->load->helper('url');
		$this->load->helper('security');
		//$this->load->library('Opensslencryptdecrypt');
		
		//session_start();	
    }
	
	public function index()
	{   
		if ($this->session->userdata('user_id')!='')  {
			redirect(base_url('xAdmin/admin/dashboard'));
		}
		$data['msg'] = '';
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('captcha', 'Capcha', 'trim|required|callback_check_captcha_code|xss_clean');
		//$encrptopenssl =  New Opensslencryptdecrypt();
		
		if ($this->form_validation->run() != FALSE)
		{		//echo "+++++++++++";die();

			//print_r($_POST); die;
			$user_name 		= mysqli_real_escape_string($this->db->conn_id,$this->input->post('username'));
			$user_password 	= mysqli_real_escape_string($this->db->conn_id,$this->input->post('password')); 
			$inputCaptcha 	= $this->input->post('captcha');			
			
			$u_name = $user_name;
			$p_name = md5($user_password);
			
			// Log Data Added
			$postArr			= $this->input->post();	
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->master_model->get_client_ip();			
			
				
			$where = array("username" =>$u_name, "password" => $p_name, "is_deleted" => 0, "role_id!=" => $this->config->item( 'surveyor_role_id' ));
			$user_exist = $this->master_model->getRecordCount('users',$where);
			//echo $this->db->last_query();
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				if($user_exist)
				{ 
					$where = array("username" => $u_name,"password" => $p_name );
					$user_all_details = $this->master_model->getRecords("users",$where);
					//echo "<pre>";print_r($user_all_details);die();
							if($user_all_details[0]['status']=="Active" && $user_all_details[0]['role_id'] !=  $this->config->item( 'surveyor_role_id' ))
							{
								$session_data['user_id'] 	= $user_all_details[0]['user_id'];
								$session_data['username'] 	= $user_all_details[0]['username'];
								$session_data['fullname'] 	= ucfirst($user_all_details[0]['first_name'])." ".ucfirst($user_all_details[0]['last_name']);
								$session_data['email'] 		= $user_all_details[0]['email'];
								$session_data['role_id'] 	= $user_all_details[0]['role_id'];	
								$this->session->set_userdata($session_data);
								// Log Capture
								$logData = array(
													'user_id' 		=> $session_data['user_id'],
													'action_name' 	=> "Login Success",
													'module_name'	=> 'Login',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr
												);
								
									
								if($logCapture[0]['log_detect'] == 'Yes'){
									$this->master_model->insertRecord('logs',$logData);
								}
								redirect(base_url('xAdmin/admin/dashboard'));
							}
							else
							{ 
								// Log Capture
								$logData = array(
													'user_id' 		=> '',
													'action_name' 	=> "Access Block This User",
													'module_name'	=> 'Login',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr
												);
								if($logCapture[0]['log_detect'] == 'Yes'){
									$this->master_model->insertRecord('logs',$logData);
								}
								
								$data['msg'] = 'Sorry, your adminstrator has blocked access to this User';
							}
				}
				else
				{
					// Log Capture
					$logData = array(
										'user_id' 		=> '',
										'action_name' 	=> "Invalid Credential",
										'module_name'	=> 'Login',
										'store_data'	=> $json_encode_data,
										'ip_address'	=> $ipAddr
									);
					if($logCapture[0]['log_detect'] == 'Yes'){
						$this->master_model->insertRecord('logs',$logData);
					}
					$data['msg'] = 'Invalid Username or Password!';
					
				}
		
		}
			
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        //$this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];

		$data["page_title"] ="Admin Login" ; 
		$this->load->view('admin/login', $data);
	}

	public function refresh(){
        // Captcha configuration
		$this->session->unset_userdata('captchaCode');
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word        
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }
	
	public function check_captcha_code($code)
	{
		
	  // check if captcha is set -
		if($code == '')
		{
			$this->form_validation->set_message('check_captcha_code', '%s is required.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		
		if($code == '' || $_SESSION["captchaCode"] != $code)
		{  
			$this->form_validation->set_message('check_captcha_code', 'Invalid %s.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($_SESSION["captchaCode"] == $code)
		{   
			$this->session->unset_userdata("captchaCode");
			$this->session->set_userdata("captchaCode", rand(1,100000));
		
			return true;
		}
		
	}
	
	// Return 'Submitted','Re-Submitted','Level 1 In Review','Level 1 Approved','Level 1 Returned','Level 2 In Review','Level 2 Approved','Level 2 Returned'
	public function getCountRecords($status, $dist_id)
	{
		$returnCnt = 0;	
		$approvedCnt = 0;
		$submittedCnt = 0;
		
		$this->db->select('response_id, status');
		$sqlDashboard = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id));
		
		/*$sqlDashboardSubmitted = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id, "status" => 'Submitted'));
		$totalDistrictCount = count($sqlDashboardSubmitted);*/
		
		//$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."' AND (status = 'Submitted' OR status = 'Re-Submitted' OR status = 'Level 1 In Review' OR status = 'Level 2 In Review')");
		$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."'");
		$totalDistrictCount = $sqlDashboardSubmitted->num_rows();
		//echo $this->db->last_query();
		if(count($sqlDashboard) > 0)
		{				
			foreach($sqlDashboard as $res)
			{
				$responseID  = @$res['response_id'];
				$this->db->select('status');
				$this->db->order_by('status_id', 'desc');
				$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
				//echo $this->db->last_query();
				//Submittted Record Count 
				if(@$trackResult[0]['status'] == 'Submitted' || @$trackResult[0]['status'] == 'Re-Submitted')
				{
					$submittedCnt++;
				}
				
				// Approved Record Count
				if(@$trackResult[0]['status'] == 'Level 1 Approved' || @$trackResult[0]['status'] == 'Level 2 Approved')
				{
					$approvedCnt++;
				}
				
				// Return Record Count
				if(@$trackResult[0]['status'] == 'Level 1 Returned' || @$trackResult[0]['status'] == 'Level 2 Returned')
				{
					$returnCnt++;
				}
			}
		}
		
		return array("Submitted" => $totalDistrictCount, "Approved" => $approvedCnt, "Return" => $returnCnt);
	}
	
	
	public function getAdminCountRecords($status, $dist_id)
	{
		$returnCnt = 0;	
		$approvedCnt = 0;
		$submittedCnt = 0;	
		
		$this->db->select('response_id, status');
		$sqlDashboard = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id));
		
		/*$sqlDashboardSubmitted = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id, "status" => 'Submitted'));
		$submittedCnt = count($sqlDashboardSubmitted);*/
		
		//$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."' AND (status = 'Submitted' OR status = 'Re-Submitted' OR status = 'Level 1 In Review' OR status = 'Level 2 In Review')");
		$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."'");
		$submittedCnt = $sqlDashboardSubmitted->num_rows();
		
		if(count($sqlDashboard) > 0)
		{				
			foreach($sqlDashboard as $res)
			{
				$responseID  = @$res['response_id'];
				$this->db->select('status');
				$this->db->order_by('status_id', 'desc');
				$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
				
				//Submittted Record Count 
				/*if(@$trackResult[0]['status'] == 'Submitted')
				{
					$submittedCnt++;
				}*/
				
				// Approved Record Count
				if(@$trackResult[0]['status'] == 'Level 1 Approved' || @$trackResult[0]['status'] == 'Level 2 Approved')
				{
					$approvedCnt++;
				}
				
				// Return Record Count
				if(@$trackResult[0]['status'] == 'Level 1 Returned' || @$trackResult[0]['status'] == 'Level 2 Returned')
				{
					$returnCnt++;
				}
			}
		}
		
		return array("Submitted" => $submittedCnt, "Approved" => $approvedCnt, "Return" => $returnCnt);
	}

	
	//showing dashboard
	public function dashboard()
	{   
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}
		$calculateSubmitted = 0;
		$calculateApproved = 0;
		$calculateReturn = 0;
		$totalDistrictCount = 0;
		$sub = 0;
		$app = 0;
		$ret = 0;	
		if($this->session->userdata('role_id') == $this->config->item('system_admin_id') || $this->session->userdata('role_id') == $this->config->item('admin_role_id'))
		{			
			
			
			$this->db->select('response_id, status');
			$sqlDashboard = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0'));
			//echo $this->db->last_query();
			if(count($sqlDashboard) > 0)
			{ 
				/*$sqlDashboardSubmitted = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "status" => 'Submitted'));
				$totalDistrictCount = count($sqlDashboardSubmitted);*/
				//echo $this->db->last_query();
				
				$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND (status = 'Submitted' OR status = 'Re-Submitted' OR status = 'Level 1 In Review' OR status = 'Level 2 In Review')");
				$totalDistrictCount = $sqlDashboardSubmitted->num_rows();
				
				foreach($sqlDashboard as $res)
				{
					$responseID  = @$res['response_id'];
					$this->db->select('status');
					$this->db->order_by('status_id', 'desc');
					$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
					//echo $this->db->last_query();
					//Submittted Record Count 
					/*if(@$trackResult[0]['status'] == 'Submitted')
					{
						$sub++;
					}*/
					
					// Approved Record Count
					if(@$trackResult[0]['status'] == 'Level 1 Approved' || @$trackResult[0]['status'] == 'Level 2 Approved')
					{
						$app++;
					}
					
					// Return Record Count
					if(@$trackResult[0]['status'] == 'Level 1 Returned' || @$trackResult[0]['status'] == 'Level 2 Returned')
					{
						$ret++;
					}
				}
			}
			
			$resultRecords = $this->master_model->getRecords("district_master", array("is_deleted" => '0', "status" => 'Active'));
			//echo $this->db->last_query();
			
			$html = '<tr>
						<th style="width: 75px">Sr No.</th>
						<th>District</th>
						<th>Submitted</th>
						<th>Approved</th>
						<th>Return</th>
						<th>Total</th>
					</tr>';
					
			
			if(count($resultRecords) > 0)
			{
				$i = 1; //65
				$totalStatus = '';
				$approvedStatus = '';
				$returnStatus = '';
				foreach($resultRecords as $district_results)
				{
					$dist_id = $district_results['district_id'];
					$submittedCnt 	= $this->getAdminCountRecords(@$totalStatus, $dist_id);
					$approvedCnts 	= $this->getAdminCountRecords($approvedStatus, $dist_id);
					$returnCnts 	= $this->getAdminCountRecords($returnStatus, $dist_id);
					$totalCnt		= ($submittedCnt['Submitted'])+($approvedCnts['Approved'])+($returnCnts['Return']);
					//print_r($submittedCnt);
					$html .= '<tr>
								<td>'.$i++.'</td>
								<td>'.$district_results['district_name'].' </td>
								<td>'.$submittedCnt['Submitted'].'</td>
								<td>'.$approvedCnts['Approved'].'</td>
								<td>'.$returnCnts['Return'].'</td>
								<td>'.$totalCnt.'</td>
							</tr>';
					
					
				}
			}
			
			
			// Need Survey ID if required
			$allRecordsTotla = $this->master_model->getRecords("response_headers", array("is_deleted" => 0));			
			$allRecords = count($allRecordsTotla);
			
			// All Submitted
			if($totalDistrictCount > 0){
				$calculateSubmitted = ($totalDistrictCount / $allRecords)*100;
			}	
			
			if($app > 0){
				$calculateApproved = ($app / $allRecords)*100;	
			}

			if($ret > 0){
				// All Return			
				$calculateReturn = ($ret / $allRecords)*100;
			}	
			
			
					
			
			
			
		}
		else 
		{
			if($this->session->userdata('role_id') ==  $this->config->item('approver1_role_id'))
			{
				$totalStatus 	= 'Submitted';
				$approvedStatus = 'Level 1 Approved';
				$returnStatus 	= 'Level 1 Returned';
			}
			else if($this->session->userdata('role_id') == $this->config->item('approver2_role_id'))
			{
				$totalStatus 	= 'Submitted';
				$approvedStatus = 'Level 2 Approved';
				$returnStatus 	= 'Level 2 Returned';
			}
			
			$sub = 0;
			$app = 0;
			$ret = 0;
			$totalDistrictCount = 0;	
			$this->db->select('district_id');
			$sqlUserDistrict = $totalReturn = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
			$userDistricts = $sqlUserDistrict[0]['district_id'];
			$explode_district = explode(",", $userDistricts);
			//$totalDistrictCount = 0;
			foreach($explode_district as $key => $disID)
			{
				//'Submitted','Re-Submitted','Level 1 In Review','Level 1 Approved','Level 1 Returned','Level 2 In Review','Level 2 Approved','Level 2 Returned'
				/*$sqlDashboardSubmitted = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $disID));
				$arrWhr = array("status" => 'Submitted', "status" => 'Re-Submitted', "status" => 'Level 1 In Review', "status" => 'Level 1 Returned');
				$totalDistrictCount += count($sqlDashboardSubmitted);*/
				
				$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND (status = 'Submitted' OR status = 'Re-Submitted' OR status = 'Level 1 In Review' OR status = 'Level 2 In Review')");
				//echo $this->db->last_query();
				$totalDistrictCount += $sqlDashboardSubmitted->num_rows();
				
				$this->db->select('response_id, status');
				$sqlDashboard = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $disID));
				
				if(count($sqlDashboard) > 0)
				{				
					foreach($sqlDashboard as $res)
					{
						$responseID  = @$res['response_id'];
						$this->db->select('status');
						$this->db->order_by('status_id', 'desc');
						$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
					
						//Submittted Record Count 
						if(@$trackResult[0]['status'] == 'Submitted' || @$trackResult[0]['status'] == 'Re-Submitted')
						{
							$sub++;
						}
						
						// Approved Record Count
						if(@$trackResult[0]['status'] == 'Level 1 Approved' || @$trackResult[0]['status'] == 'Level 2 Approved')
						{
							$app++;
						}
						
						// Return Record Count
						if(@$trackResult[0]['status'] == 'Level 1 Returned' || @$trackResult[0]['status'] == 'Level 2 Returned')
						{
							$ret++;
						}
					}
				}
			}
			
			$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$this->session->userdata('user_id')."' AND status = 'Active' AND is_deleted = '0'");
			$cntRow = $surveyDistrict->num_rows();			
			
			if($cntRow > 0)
			{ 
				$districtList = array();
				foreach($surveyDistrict->result_array() as $row)
				{
					array_push($districtList, $row['district_id']);
				}

				
			}
			
			$district_result = explode(",",$districtList[0]);
			
			$outputString = "" . implode( "','", $district_result ) . "";
			
			$this->db->select('district_id, district_name');
			$this->db->where_in('district_id', $district_result);
			$resultRecords = $this->master_model->getRecords("survey_district_master", array("is_deleted" => '0', "status" => 'Active'));
			$numRows = count($resultRecords);
			//echo ">>".$this->session->userdata('role_id');
			
			$html = '<tr>
						<th style="width: 75px">Sr No.</th>
						<th>District</th>
						<th>Submitted</th>
						<th>Approved</th>
						<th>Return</th>
						<th>Total</th>
					</tr>';
			if($numRows > 0)
			{
				$i = 1; //65
			
				foreach($resultRecords as $district_results)
				{
					$dist_id = $district_results['district_id'];
					$submittedCnt 	= $this->getCountRecords($totalStatus, $dist_id);
					$approvedCnts 	= $this->getCountRecords($approvedStatus, $dist_id);
					$returnCnts 	= $this->getCountRecords($returnStatus, $dist_id);
					$totalCnt		= ($submittedCnt['Submitted'])+($approvedCnts['Approved'])+($returnCnts['Return']);
					//print_r($submittedCnt);
					$html .= '<tr>
								<td>'.$i++.'</td>
								<td>'.$district_results['district_name'].' </td>
								<td>'.$submittedCnt['Submitted'].'</td>
								<td>'.$approvedCnts['Approved'].'</td>
								<td>'.$returnCnts['Return'].'</td>
								<td>'.$totalCnt.'</td>
							</tr>';
					
					
				}
			}
			
			
			
			
			// Need Survey ID if required
			$allRecordsTotla = $this->master_model->getRecords("response_headers", array("is_deleted" => 0));			
			$allRecords = count($allRecordsTotla);			
			
			// All Submitted
			$calculateSubmitted = ($totalDistrictCount / $allRecords)*100;
			
			// All Approved				
			$calculateApproved = ($app / $allRecords)*100;
			
			// All Return				
			$calculateReturn = ($ret / $allRecords)*100;
			
		} // Else End
		
		
		$data=array('middle_content' => 'dashboard/index','page_title'=>"Dashboard", 'submitted' => @$totalDistrictCount, 'approved' => @$app, 'return' => @$ret, "response_html" => $html, "total_survey" => number_format($calculateSubmitted, 2), "total_approved" => number_format($calculateApproved,2), "total_return" => number_format($calculateReturn,2));
		
		$data['submitted'] 	= @$totalDistrictCount;
		$data['approved'] 	= @$app;
		$data['return'] 	= @$ret;
		$data['module_name'] = 'Dashboard';
		$data['submodule_name'] = '';		
		$this->load->view('admin/admin_combo',$data);
	}
	
	public function change_password()
	{
		//$encrptopenssl =  New Opensslencryptdecrypt();
		if($this->session->userdata('role_id') == $this->config->item('system_admin_id')){
			redirect('xAdmin');
		}
	 	$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password','trim|required|min_length[8]|matches[crm_password]');
		$this->form_validation->set_rules('crm_password', 'Confirm Password', 'trim|required|min_length[3]');
		
		if ($this->form_validation->run())
		{
			
			$old_password 	= $this->input->post('old_password');
			$user_password 	= $this->input->post('new_password');	
			$con_password 	= md5($this->input->post('crm_password'));
			$where = array("username" => $this->session->userdata('username'), "password" => md5($old_password));
			
			$user_all_details = $this->master_model->getRecords("users",$where);
			//echo $this->db->last_query();die();
			if(md5($user_password) == md5($old_password))
			{	
				$this->session->set_flashdata('error','No change in password.');
				redirect('xAdmin/admin/change_password');		
			}
			else
			{  
			
				if(!empty($user_all_details) && md5($old_password) == $user_all_details[0]['password'])
				{
					if(md5($user_password) == $con_password)
					{
							$update_array= array("password"=> md5($user_password));							
							if($admin_details = $this->master_model->updateRecord('users',$update_array,
							array('user_id'=>"'".$this->session->userdata('user_id')."'")))
							{ 
								
								$this->session->set_flashdata('success','Password has been reset successfully');
							  	redirect('xAdmin/admin/change_password');	
							}
							else
							{ 
								$this->session->set_flashdata('error','Error while resetting password.');
								redirect('xAdmin/admin');	
							}
					}
					else
					{	
						$this->session->set_flashdata('error','Wrong Password Confirmation');
						redirect('xAdmin/admin/change_password');	
					}
				}
				else
				{  
					$this->session->set_flashdata('error','Incorrect old password');
					redirect('xAdmin/admin/change_password');	
				}
			}
		}
		
		$data=array('middle_content' => 'change_password');
		$data['module_name'] = 'Change_Password';
		$data['submodule_name'] = '';
		$this->load->view('admin/admin_combo',$data);
	
		//$this->load->view('admin/change_password');
	}
	
	//contact enquiries added from front panel show and details
	public function contact_enq()
	{
		$contact_info=$this->master_model->getRecords("tbl_contact_enq");
		$data = array('middle_content'=>'contact_view','contact_info'=>$contact_info);
		$this->load->view('admin/admin_combo',$data);
	}
	
	//admin logout
	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		redirect(base_url('xAdmin/admin'));	 
	}
}
