<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : District
Author : Vicky K
*/

class District extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		$this->master_model->permission_access('4', 'view');
		 // Permission Set Up
		$this->db->where('survey_states_master.status','Active'); 
		$this->db->where('survey_states_master.is_deleted','0'); 
    	$this->db->where('survey_district_master.is_deleted','0');
		
		$this->db->select('survey_states_master.state_name, survey_district_master.district_name, survey_district_master.district_id, survey_district_master.status, survey_district_master.state_id');
		$this->db->join('survey_states_master','survey_district_master.state_id=survey_states_master.state_id','left',FALSE);
		$response_data = $this->master_model->getRecords("district_master",'','',array('district_id'=>'DESC'));	
		//echo $this->db->last_query();die();

		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'District';	
    	$data['middle_content']='district/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add state
    public function add()
    {
		 // Permission Set Up
		$this->master_model->permission_access('4', 'add');
		
		$this->db->where('survey_states_master.is_deleted','0');
		$this->db->where('survey_states_master.status','Active');
		$response_data = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC'));	
        // Check Validation
		$this->form_validation->set_rules('state_id', 'State Name', 'required');	
		$this->form_validation->set_rules('name', 'District Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			$state_id = $this->input->post('state_id');
			$district_name = trim($this->input->post('name'));			
			$insertArr = array( 'state_id' => $state_id, 'district_name' => $district_name,'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('district_master',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'District Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','District successfully created');
				redirect(base_url('xAdmin/district'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/district/add'));
			}
		}
		$data['stateList'] = $response_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'District';	
    	$data['middle_content']='district/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit state 
	 public function edit($id)
    {
		 // Permission Set Up
		$this->master_model->permission_access('4', 'edit');
		
		$district_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("district_master",array('district_id'=>$district_id));
		$this->form_validation->set_rules('name', 'District Name', 'required|min_length[3]|xss_clean');
		
		$this->db->where('survey_states_master.status','Active');
		$this->db->where('survey_states_master.is_deleted','0');
		$response_data = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC'));
		if($this->form_validation->run())
		{	
		
			$state_id = $this->input->post('state_id');
			$district_name = trim($this->input->post('name'));
			
			$updateArr = array('state_id' => $state_id, 'district_name' => $district_name, 'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('district_master',$updateArr,array('district_id' => $district_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'District Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}

				$this->session->set_flashdata('success','District successfully updated');
				redirect(base_url('xAdmin/district'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/district/edit/'.$id));
			}
		}
		
		$data['stateList'] = $response_data;
		$data['district_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'District';
        $data['middle_content']	=	'district/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update state Status
	 public function changeStatus(){
		 
		  // Permission Set Up
		$this->master_model->permission_access('4', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			$updateQuery = $this->master_model->updateRecord('district_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('district_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'District Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/district'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('district_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('district_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'District Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/district'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 
		  // Permission Set Up
		$this->master_model->permission_access('4', 'delete');
		 $id 	= base64_decode($id);		 
		 $updateQuery = $this->master_model->updateRecord('district_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('district_id' => $id));
		  // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'District Master',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','District successfully deleted');
		 redirect(base_url('xAdmin/district'));	
		 
	 }


}