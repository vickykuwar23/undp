<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Role
Author : Vicky K
*/

class Role extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		// Permission Set Up
		$this->master_model->permission_access('10', 'view');
		
    	$this->db->where('role_master.is_deleted','0');
		$response_data = $this->master_model->getRecords("role_master",'','',array('role_id'=>'DESC'));		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Role';	
    	$data['middle_content']='role-master/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add Role
    public function add()
    {
		 // Permission Set Up
		$this->master_model->permission_access('10', 'add');
		
        // Check Validation
		$this->form_validation->set_rules('name', 'Role Name', 'required|min_length[4]|xss_clean');	
		if($this->form_validation->run())
		{	
			$role_name = trim($this->input->post('name'));			
			$insertArr = array( 'role_name' => $role_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('role_master',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Role Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Role successfully created');
				redirect(base_url('xAdmin/role'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/role/add'));
			}
		}
		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Role';	
    	$data['middle_content']='role-master/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit Role 
	 public function edit($id)
    {
		 // Permission Set Up
		$this->master_model->permission_access('10', 'edit');
		
		$roleid = base64_decode($id);	
		$rec = $this->master_model->getRecords("role_master",array('role_id'=>$roleid));
		$this->form_validation->set_rules('name', 'Type Master Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
		
			$role_name = trim($this->input->post('name'));	
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('role_name' => $role_name, 'updated_by_id' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('role_master',$updateArr,array('role_id' => $roleid));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Role Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Role successfully updated');
				redirect(base_url('xAdmin/role'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/role/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Role';
        $data['middle_content']	=	'role-master/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update Role Status
	 public function changeStatus(){
		 
		  // Permission Set Up
		$this->master_model->permission_access('10', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('role_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('role_id' => $id));
			 
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Role Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/role'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('role_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('role_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Role Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/role'));		
		 }
		 
	 }
	 
	 // Soft Delete Role
	 public function delete($id){
			
		// Permission Set Up
		$this->master_model->permission_access('10', 'delete');
		 
		 $id 	= base64_decode($id);	
		 $updateQuery = $this->master_model->updateRecord('role_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('role_id' => $id));
		 
		  // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Role Master',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 
		 $this->session->set_flashdata('success','Type master successfully deleted');
		 redirect(base_url('xAdmin/role'));	
		 
	 }
	 
	// Set Permission
	public function permission($id){
		error_reporting(0);
		
		//print_r($this->session->userdata());die();
		/*if($this->session->userdata('role_id')!= '1' || $this->session->userdata('role_id')!= '999'){
			redirect(base_url('xAdmin'));	
		}*/

		if(isset($_POST['btn_save']))
		{
			//echo "<pre>";print_r($_POST);
			$role_id = $_POST['role_id'];
			$this->master_model->deleteRecord('permission','role_id', $role_id);	
			foreach($_POST['module_id'] as $key => $value) 
			{
				//echo ">>>".$key;
				foreach($value as $val) 
				{
					//echo ">>Key>>".$key .">>Value>>". $val;
					$insertArr = array("role_id" => $role_id, "module_id" => $key, "permission_name" => $val, "created_by_id" => $this->session->userdata('user_id'));
					$this->master_model->insertRecord('permission',$insertArr);
				}
			}
			
			 $this->session->set_flashdata('success','Permission successfully updated.');
			redirect(base_url('xAdmin/role/permission/'.$id));	
		}
		
		$role_id = base64_decode($id);	
		$moduleList = $this->master_model->getRecords("module",array('status'=>'Active', 'is_deleted' => 0));

		$role_data = $this->master_model->getRecords("role_master",array('role_id'=>$role_id));
		
		$data['role_id'] 		= 	$role_id;
		$data['role_name'] 		= 	$role_data[0]['role_name'];
		$data['module_list'] 	= 	$moduleList;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Role';
        $data['middle_content']	=	'role-master/permission';
        $this->load->view('admin/admin_combo',$data);
		 
	} 
	
	// Update Permission 
	public function updatePermission(){
				
		$module_id 		= $this->input->post('module_id');
		$role_id 		= $this->input->post('role_id');
		$action_name 	= $this->input->post('action_name');
		
		$check_exist = $this->master_model->getRecords("permission",array('module_id'=>$module_id, 'role_id'=>$role_id, 'permission_name'=>$action_name));
		if(count($check_exist) > 0){
			$return = "Skip";
			$permissionId = $check_exist[0]['permission_id'];
			if($this->master_model->deleteRecord('permission','permission_id',$permissionId)){
				
				 // Log Data Added
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				$postArr			= $this->input->post();		
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Permission Removed",'module_name'=> 'Role Master','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$return = "Delete";
			}	
		} else {			
			$insertArr = array('role_id' => $role_id, 'module_id' => $module_id, 'permission_name' => $action_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('permission',$insertArr);	
			
			 // Log Data Added
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			$postArr			= $this->input->post();		
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Permission Add",'module_name'=> 'Role Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$return = "Created";
		}
		$responseArr = array("response" => $return);
		echo json_encode($responseArr);
	}


}