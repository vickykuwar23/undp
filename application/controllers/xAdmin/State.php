<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : State
Author : Vicky K
*/

class State extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		 // Permission Set Up
    	
		$this->master_model->permission_access('3', 'view');
		$this->db->where('is_deleted','0');
		$response_data = $this->master_model->getRecords("states_master",'','',array('state_id'=>'DESC'));		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'State';	
    	$data['middle_content']='state/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add state
    public function add()
    {
		 // Permission Set Up
		$this->master_model->permission_access('3', 'add');
		
        // Check Validation
		$this->form_validation->set_rules('name', 'State Name', 'required|min_length[4]|xss_clean');	
		if($this->form_validation->run())
		{	
			$state_name = trim($this->input->post('name'));			
			$insertArr = array( 'state_name' => $state_name,'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('states_master',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
								 'action_name'=> "Add",
								 'module_name'=> 'State Master',
								 'store_data'=> $json_encode_data,
								 'ip_address'=> $ipAddr,
							 	 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','State successfully created');
				redirect(base_url('xAdmin/state'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/state/add'));
			}
		}
		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'State';	
    	$data['middle_content']='state/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit state 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('3', 'edit');
		$state_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("states_master",array('state_id'=>$state_id));
		$this->form_validation->set_rules('name', 'State Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
		
			$state_name = trim($this->input->post('name'));			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('state_name' => $state_name, 'updated_by_id' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('states_master',$updateArr,array('state_id' => $state_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
					 		     'action_name'=> "Edit",
					 		     'module_name'=> 'State Master',
								 'store_data'=> $json_encode_data,
								 'ip_address'=> $ipAddr,
							 	 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','State successfully updated');
				redirect(base_url('xAdmin/state'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/state/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'State';
        $data['middle_content']	=	'state/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update state Status
	 public function changeStatus(){
		 
		 // Permission Set Up
		$this->master_model->permission_access('3', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('states_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('state_id' => $id));
			 
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
						     'action_name'=> "Status",
						     'module_name'=> 'State Master',
							 'store_data'=> $json_encode_data,
							 'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/state'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('states_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('state_id' => $id)); 
			
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
				             'action_name'=> "Status",
				             'module_name'=> 'State Master',
							 'store_data'=> $json_encode_data,
							 'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/state'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 // Permission Set Up
		$this->master_model->permission_access('3', 'delete');
		
		 $id 	= base64_decode($this->uri->segment(4));
		 $updateQuery = $this->master_model->updateRecord('states_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('state_id' => $id));
		 
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData 			= array('user_id' => $this->session->userdata('user_id'),
						 			'action_name'=> "Delete",
						 			'module_name'=> 'State Master',
									'store_data'=> $json_encode_data,
									'ip_address'=> $ipAddr,
							 	    'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		
		 $this->session->set_flashdata('success','State successfully deleted');
		 redirect(base_url('xAdmin/state'));	
		 
	 }


}