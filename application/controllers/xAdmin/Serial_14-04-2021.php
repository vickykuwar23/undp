<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Serial
Author : Vicky K
*/

class Serial extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {		
	
		if(isset($_POST['submit']))
		{
			$survey_id = $_POST['survey_id'];
			//echo "<pre>";print_r($_POST['order']);
			if(count($_POST['order']) > 0)
			{

				foreach($_POST['order'] as $key => $val)
				{
					//echo $key."===".$val."<br />";
					$updateArr = array("sort_order" => $val);
					$updateQuery = $this->master_model->updateRecord('section_questions',$updateArr,array('survey_id'=>$survey_id, 'question_id' => $key));
				}
				
				$this->session->set_flashdata('success','Order Updated successfully');
				redirect(base_url('xAdmin/serial'));
			}
		//die();
		}
		$this->db->where('survey_section_questions.is_deleted','0');
		$this->db->where('survey_section_questions.survey_id','1');
		$this->db->select("survey_question_master.question_id, survey_question_master.question_text, survey_section_questions.section_id, survey_section_questions.sort_order");
		$this->db->join("survey_question_master",'survey_section_questions.question_id=survey_question_master.question_id','LEFT', FALSE);
		$response_data = $this->master_model->getRecords("section_questions",'','',array('survey_section_questions.sq_id'=>'ASC'));		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Serial';	
    	$data['middle_content']='serial/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

}