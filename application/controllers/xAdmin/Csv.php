<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Csv
Author : Vicky K
*/
class Csv extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		$this->load->library('excel');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Csv Listing
    public function index()
    {
		// Permission Set Up
		$this->master_model->permission_access('6', 'view');
		$this->db->join('survey_block','survey_village.block_id=survey_block.block_id','left',FALSE);
		$response_data = $this->master_model->getRecords("village",'','',array('village_id'=>'DESC'));	
		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Csv';	
    	$data['middle_content']='csv/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	// Export data in CSV format 
	  public function exportCSV(){ 
	   // file name 
	   $filename = 'users_'.date('Ymd').'.csv'; 
	   header("Content-Description: File Transfer"); 
	   header("Content-Disposition: attachment; filename=$filename"); 
	   header("Content-Type: application/csv; ");	   
	   // get data 
	   $usersData = $this->master_model->getRecords("village",'','',array('village_id'=>'DESC'));

	   // file creation 
	   $file = fopen('php://output', 'w');
	 
	   $header = array("Username","Name","Gender","Email"); 
	   fputcsv($file, $header);
	   foreach ($usersData as $key=>$line){ 
		 fputcsv($file,$line); 
	   }
	   
	    /*foreach ($myData as $line){
            fputcsv($file,array($line->field1,$line->field2,$line->field3,$line->field4));
        }*/
	   
	   fclose($file); 
	   exit; 
	  }
	  
	  public function test_excel()
	{
		require(APPPATH."third_party/PHPExcel/Classes/PHPExcel.php");
		require(APPPATH."third_party/PHPExcel/Classes/PHPExcel/Writer/Excel5.php");			
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
		->setCreator("Temporaris")
		->setLastModifiedBy("Temporaris")
		->setTitle("Template Relevé des heures intérimaires")
		->setSubject("Template excel")
		->setDescription("Template excel permettant la création d'un ou plusieurs relevés d'heures")
		->setKeywords("Template excel");
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', "12");

		// $writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);  
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="excel.xls"');
		header('Cache-Control: max-age=0');
		
		$objWriter->save('some_excel_file.xlsx');
		// $writer->save('php://output');
	}
	
	public function setPDF(){
		
		//$headImg  = base_url('img/emblemofindia.png');
		//$headImg2 = base_url('img/IGRlogo.png');		
		
		//$img1 = '<img src="img/emblemofindia.png" height="80" width="80" />';
		//$img2 = '<img src="img/IGRlogo.png" height="80" width="80" />';
		$img1 = '';$img2 = '';
		
		$header = '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
					<td width="23%">'.$img1.'</td>
					<td width="23%" align="center" style="font-size:17px; color:#000;">UNDP Survey
					</td>
					<td width="23%" style="text-align: right;">'.$img2.'</td>
					</tr></table>';
		$headerE = '<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
					<td width="23%">'.$img1.'</td>
					<td width="23%" align="center" style="font-size:17px; color:#000;">&nbsp;
					</td>
					<td width="23%" style="text-align: right;">'.$img2.'</td>
					</tr></table>';

		$footer = '<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
					<tr>
						<td width="33%"></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;"></td>
					</tr>
				</table>';
		$footerE = '<table width="100%" style="border-top: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;">
					<tr>
						<td width="33%"></td>
						<td width="33%" align="center">{PAGENO}/{nbpg}</td>
						<td width="33%" style="text-align: right;"></td>
					</tr>
				</table>';
				
				
		$arrHeader = array('header_1' => $header, 'header_2' => $headerE, 'footer_1' => $footer, 'footer_2' =>$footerE);
		
		return $arrHeader;
	}
	
	public function downloadpdf(){		
		// Download PDF 
		error_reporting(E_ERROR | E_PARSE);		
		include(APPPATH."third_party/mpdf/mpdf.php");
		//$mpdf=new mPDF('c');
		$mpdf = new mPDF('c','A4','','',20,20,30,30,5,5);
		$mpdf->mirrorMargins = 1;
		
		// Set PDF Header Footer HTML
		$pdfHeader = $this->setPDF();		
				
		$mpdf->SetHTMLHeader($pdfHeader['header_1']);
		$mpdf->SetHTMLHeader($pdfHeader['header_2'],'E');
		$mpdf->SetHTMLFooter($pdfHeader['footer_1']);
		$mpdf->SetHTMLFooter($pdfHeader['footer_2'],'E');

		 $table = '<h3>Sample Form Report</h3>
					<table border="1" class="table table-striped table-bordered dt-responsive nowrap">						
						<tr>
							 <th>Sr.No.</th>
							 <th>Name</th>
						</tr>';			
		$table .='<tr>
					<td align="center">1</td>
					<td>Test</td>
				</tr>';									
		$table .='</table>';			
		$mpdf->WriteHTML($table);
		$filename = "test-".date("Y-m-d-H-i-s").".pdf";
		$mpdf->Output($filename,'D');
		//$filename = "complaint_pdf/pendency-level3-report-on-".date("Y-m-d-H-i-s").".pdf";
		//$mpdf->Output();
		//$data['pdf_file_name'] = $filename;
		//$mpdf->Output($filename,'D');
	}
	
	
	public function downloadExcel(){
		ob_start();	
		$this->load->library('excel');		
		$this->db->select('village_id, village_name');	
		$records = $this->master_model->getRecords("village",'','',array('village_id' => 'DESC'));	
		$i = 0;					
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);			
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'ID');
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Name');			
		// set Row
		$rowCount = 2;			
		foreach($records as $res){ $i++;	
			$village_id 	= $res['village_id'];	
			$village_name 	= $res['village_name'];	
			$objPHPExcel->getActiveSheet()->SetCellValue('A'. $rowCount, $res['village_id']);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'. $rowCount, $res['village_name']);				
			$rowCount++;
		} // Foreach End		
		// create file name		
		header("Content-Type: application/vnd.ms-excel");	
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		//uncomment below line on Server
		//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
		header('Content-Disposition: attachment;filename="village_'.time().'.xlsx"');
		header('Cache-Control: max-age=0');			
		$objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
		ob_end_clean();
		$objWriter->save('php://output');
		//$objWriter->save($fileName);
		exit; 
	}



} // End Controller 