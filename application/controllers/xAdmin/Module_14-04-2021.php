<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Module
Author : Vicky K
*/

class Module extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
    	
		// Permission Set Up

		$this->master_model->permission_access('7', 'view');

		$this->db->where('survey_module.is_deleted','0');
		$response_data = $this->master_model->getRecords("survey_module",'','',array('module_id'=>'ASC'));		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Module';	
    	$data['middle_content']='module/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add module
    public function add()
    {
		 // Permission Set Up
		$this->master_model->permission_access('7', 'add');
        // Check Validation
		$this->form_validation->set_rules('name', 'Module Name', 'required|min_length[4]|xss_clean');	
		if($this->form_validation->run())
		{	
			$module_name = trim($this->input->post('name'));			
			$insertArr = array( 'module_name' => $module_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('module',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Module',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				
				$this->session->set_flashdata('success','Module successfully created');
				redirect(base_url('xAdmin/module'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/module/add'));
			}
		}
		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Module';	
    	$data['middle_content']='module/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit module 
	 public function edit($id)
    {
		 // Permission Set Up
		$this->master_model->permission_access('7', 'edit');
		$module_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("module",array('module_id'=>$module_id));
		$this->form_validation->set_rules('name', 'Module Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
		
			$module_name = trim($this->input->post('name'));			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('module_name' => $module_name, 'updated_by_id' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('module',$updateArr,array('module_id' => $module_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Module',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Module successfully updated');
				redirect(base_url('xAdmin/module'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/module/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Module';
        $data['middle_content']	=	'module/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update module Status
	 public function changeStatus(){
		 
		  // Permission Set Up
		$this->master_model->permission_access('7', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('module',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('module_id' => $id));
			 
			  // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Module',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/module'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('module',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('module_id' => $id)); 
			
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Module',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/module'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 
		  // Permission Set Up
		$this->master_model->permission_access('7', 'delete');
		 $id 	= base64_decode($id);		
		 $updateQuery = $this->master_model->updateRecord('module',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('module_id' => $id));
		 
		 // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Module',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 
		 $this->session->set_flashdata('success','Module successfully deleted');
		 redirect(base_url('xAdmin/module'));	
		 
	 }


}