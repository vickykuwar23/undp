<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Serial Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Serial Master</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">	  
         <div class="card ">
			<div class="card-header">
				
				          
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
		 <form name="frm" method="post">
		 <input type="submit" name="submit" id="submit" value="Submit" />
		 <input type="hidden" name="survey_id" id="survey_id" value="1" />
		 <div class="table-responsive">
            <table id="example21" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>Name</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;					
                     foreach($records as $qest) 
                     { 					 
					 ?>
					<tr>
						<td><?php echo $qest['question_id']; ?></td>
						<td><?php echo $qest['question_text'] ; ?></td> 
						<td><input type="text" name="order[<?php echo $qest['question_id']; ?>]" id="ord" value="" /></td>
					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
		 </form>
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});
});
</script>