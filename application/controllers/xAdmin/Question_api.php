<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Question
Author : Priyanka Wadnere
*/

class Question_api extends CI_Controller 
{
	
	function __construct() {
			
        parent::__construct();

		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		
		$this->user_id            	=  $this->session->userdata('user_id');
        $this->ip                 	=  $this->input->ip_address();
        $this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'Question';
        $this->module_title       =  "Question";
        $this->module_view_folder =  "questions/";    
    }

    

	public function view($survey_id){

		// Permission Set Up
        $this->master_model->permission_access('16', 'view');

     	//$survey_id = base64_decode($survey_id);

		$question_query = $this->db->query("SELECT  q1.survey_id, (SELECT svy.title FROM survey_master svy WHERE svy.survey_id = q1.survey_id) survey_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.section_id = sec.section_id) section_name, (SELECT sec.section_name FROM survey_section_master sec WHERE sm.sub_section_id = sec.section_id) sub_section_name, q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt,  q1.is_mandatory, sm.global_question_id, q1.if_summation, q1.if_file_required, q1.file_label
		    FROM survey_question_master q1 LEFT JOIN survey_section_questions sm ON q1.survey_id = sm.survey_id
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.question_id = sm.question_id
			AND q1.survey_id = ".$survey_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			GROUP BY q1.survey_id, sm.section_id, sm.sub_section_id, q1.question_id
			ORDER BY q1.survey_id, sm.section_id, sm.sub_section_id, q1.question_id ASC");


		$question_data = $question_query->result_array();

     	//$tree = $this->buildTree($question_data[0]['question_id']);

		$tree = array();
	

     	//$tree = $this->buildTree($question_data);

     	if(sizeof($question_data) >0){
     		foreach ($question_data as $key => $value) {

	 			if($value['option_cnt'] >0 ){
	                $where = array('question_id' => $value['question_id']);
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, op.validation_id, op.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt FROM survey_questions_options_master op WHERE op.question_id=".$value['question_id']);

	                $option_data = $option_query->result_array();

	                $value['options'] = json_encode($option_data);

	                foreach ($option_data as $key => $value) {

						if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

							$condition_subquestion = $this->get_dependant_question($value['ques_option_id']);

							if($condition_subquestion){
				          		$value['condition_sub_question'] = $condition_subquestion;
				            }
				          
			            }
			          
		            }
	                
	            }
	            else{
	            	$option_data = [];
	            }
	            //echo $value['global_question_id'];

	            
     			$child = $this->buildTree($value['question_id']);

     			echo '<pre>';
     			if($child){
     				$value['sub_question'] = $child;
     				//echo $html;
     			}

     		
     			//print_r($value);
     			//$tree[] = $value;
     			//$html.= $html;
     		
            }	


     	}

     	
     	echo '<pre>';
     	//print_r($tree); 
     	print_r($question_data); 
     	die;

     	//echo $html; 
     	//die;
     	
     	/*if(sizeof($tree) >0){
     		$this->data['question_data']  =  $tree;
     	}
     	else{
     		$this->data['question_data']  =  '';
     	}*/
     	$this->data['html']       		= $html;
		$this->data['page_title']       = 'View '.$this->module_title;
		$this->data['module_name']     	= $this->module_title; 
		$this->data['mode']     		= 'survey_view'; 
        $this->data['middle_content']   = $this->module_view_folder.'question_view';
	
		//$this->load->view('admin/admin_combo',$this->data);
	}

	public function buildTree($question_id){
		//error_reporting(E_ERROR | E_PARSE);
		//$tree = array();
		$branch = array();
		

    	$sub_question_query = $this->db->query("SELECT  q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.salutation, (SELECT COUNT(1) FROM survey_questions_options_master o1 WHERE o1.question_id = q1.question_id) option_cnt, q1.is_mandatory, q1.if_summation, q1.if_file_required, q1.file_label FROM survey_question_master q1 
			WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent od
                  			  WHERE q1.question_id = od.dependant_ques_id)
            AND q1.parent_question_id = ".$question_id."
			AND q1.is_deleted = 0 
			AND q1.status = 'Active' 
			ORDER BY q1.question_id ASC");
    	
    	//echo '<pre>';

    	$sub_question_data = $sub_question_query->result_array();
    	
		if (sizeof($sub_question_data)>0) {
			//echo 'if'.$question_id;
		   foreach ($sub_question_data as $sub_question) { 

		   
	 			if($sub_question['option_cnt'] >0 ){
	              
	                $option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title,
	                	CASE WHEN op.sub_validation_id = 1 THEN '>' 
						     WHEN op.sub_validation_id = 2 THEN '>=' 
						     WHEN op.sub_validation_id = 3 THEN '<'
						     WHEN op.sub_validation_id = 4 THEN '<='
						     WHEN op.sub_validation_id = 5 THEN '=='
						     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition,
	                	op.min_value, op.max_value, op.dependant_ques_id, op.option, op.validation_label, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$sub_question['question_id']);

	               	$option_data = $option_query->result_array();

	               	$sub_question['options'] = json_encode($option_data);

	               	foreach ($option_data as $key => $value) {
	               		
	               		
						if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

							$condition_subquestion = $this->get_dependant_question($value['ques_option_id']);

							if($condition_subquestion){
				          		$value['condition_sub_question'] = $condition_subquestion;
				            }
				          
			            }
			          
		            }
	                
	                //$option_data = $option_query->result_array();
	            }
	            else{
	            	$option_data = [];
	            }

	          
	            $children = $this->buildTree($sub_question['question_id']);

	          	if($children){
	          		$sub_question['sub_question'] = $children;
	            }

	            $branch[] = $sub_question;

	            
	        }
	       
	    }
		   
		//print_r($branch);
	    return $branch;
	    //echo $sub_html;
	    //return $sub_html;
		
	}

	public function get_dependant_question($option_id){

		$question_query = $this->db->query("SELECT q1.question_id, q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory, q1.salutation, q1.if_summation, q1.if_file_required, q1.file_label, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE optn.validation_id = v1.validation_id) validation_title, optn.sub_validation_id, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE optn.sub_validation_id = v2.sub_validation_id) sub_validation_title, optn.min_value, optn.max_value, optn.validation_label, optn.display_order
		FROM   survey_question_master q1  LEFT JOIN survey_option_dependent od ON od.dependant_ques_id = q1.question_id LEFT JOIN survey_questions_options_master optn ON optn.question_id = q1.question_id
		WHERE  od.ques_option_id = ".$option_id."
		AND   q1.is_deleted = 0 
		AND   q1.status = 'Active'
		GROUP BY q1.question_id 
		ORDER BY q1.question_id ASC");
		 //echo $this->db->last_query();
		$question_data = $question_query->result_array();

		$question_array = array();
		if(isset($question_data) && sizeof($question_data) >0){
			$j=0;
			foreach ($question_data as $question) {
				
				$option_query = $this->db->query("SELECT op.ques_option_id, op.question_id, op.validation_id, op.sub_validation_id, (SELECT v1.validation_type from survey_response_validation_master v1 WHERE op.validation_id = v1.validation_id) validation_title, (SELECT v2.validation_sb_type from survey_response_validation_subtype_master v2 WHERE op.sub_validation_id = v2.sub_validation_id) sub_validation_title, 
					CASE WHEN op.sub_validation_id = 1 THEN '>' 
					     WHEN op.sub_validation_id = 2 THEN '>=' 
					     WHEN op.sub_validation_id = 3 THEN '<'
					     WHEN op.sub_validation_id = 4 THEN '<='
					     WHEN op.sub_validation_id = 5 THEN '=='
					     WHEN op.sub_validation_id = 6 THEN '!=' ELSE '||' END logical_condition, 
					op.min_value, op.max_value, op.validation_label, op.dependant_ques_id, op.option, op.if_sub_question_checked, (SELECT COUNT(1) FROM survey_option_dependent od  WHERE op.if_sub_question_checked='Yes' AND od.ques_option_id = op.ques_option_id AND od.dependant_ques_id != ''  ) dependant_cnt  FROM survey_questions_options_master op WHERE op.question_id=".$question['question_id']);

		        $option_data = $option_query->result_array();

		        foreach ($option_data as $key => $value) {
					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){
		       		 	$child = $this->get_dependant_question($question['question_id']);
     				
		     			if($child){
		     				$question['conditional_sub_question'] = $child;
		     				
		     			}
 					}
 				}
 			}
     	}
     	
	
	}

	

	public function get_html_tags($tag_name, $question_id, $option_array, $salutation, $if_summation, $if_file_required, $file_label){
		//echo $question_id.'--------'.$dependant_cnt.'</br>';
		$str = '';
		$length = '';
		$salutation_flag =0;

		if($salutation!='' && $salutation!='0'){
			$str.= '</div> </div>';
			$salutation_flag =1;
			$salutation_array = explode('|', $salutation);
			if(sizeof($salutation_array)>0){
				$str.= '<div class="col-3"> <div class="form-group">';
				$str.= '<select class="form-control">
		                    <option value="">Select Salutation</option>';
				foreach ($salutation_array as $id) {
					$str.= '<option value="">'.$id.'</option>';	
				}
				$str.= '</select>';
				$str.= '</div> </div>';
			}
			$str.='<div class="col-12"> <div class="form-group">';
		}

		if($tag_name == 'radio'){
			if(sizeof($option_array) >0){
				//$str.='</br>';
				foreach ($option_array as $key => $value) {
					//$str.='</br>';
					$str.='<div class="form-check form-check-inline">';
					$str.= '<input class="form-check-input dependantcls" type="radio" id="'.$question_id.'" data-id="'.$value['ques_option_id'].'" name="radio" value=""> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'">'.$value['option'].'</span>';
					$str.='</div>';	
				}
			}	

			foreach ($option_array as $key => $value) {
				$str.= '</div>
		                </div>
		                  <div class="col-6 radio_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
				if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

					for ($i=0; $i < $value['dependant_cnt']; $i++) { 
						$j = $i+1;    
		                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
		               
		           }
		          
	           }
	            $str.='</div>';
		           $str.=' <div class="col-12">
		               <div class="form-group">';
           }

		}
		elseif($tag_name == 'checkbox'){
			$str1 = '';
			if(sizeof($option_array) >0){
				//$str.='</br>';
				foreach ($option_array as $key => $value) {
					$str.='<div class="form-check" style="clear:left;">';
					$str.= '<input class="form-check-input dependantcls " type="checkbox" id="'.$value['ques_option_id'].'" data-id="'.$question_id.'" name="chk[]" value="'.$question_id.'"> &nbsp;';
					$str.= '<span class="form-check-label" for="'.$question_id.'" >'.$value['option'].'</span>';
					$str.='</div>';


					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){
						$str.='<span class="checkbox_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
							$str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
						}
						$str.='</span>';
					}
						
				}
			}
		}
		elseif($tag_name == 'single_select'){

			$str.= '<select class="form-control dependantcls" id="'.$question_id.'">
	                    <option value="">Select Option Type</option>';
	        if(sizeof($option_array) >0){
		        foreach ($option_array as $key => $value) {
					$str.= '<option value="" id="'.$value['question_id'].'" data-id="'.$value['ques_option_id'].'">'.$value['option'].'</option>';
				}
			}
			$str.= '</select>';

			if(sizeof($option_array) >0){
				foreach ($option_array as $key => $value) {
					$str.= '</div>
			                </div>
			                  <div class="col-6 dropdown_dependant_div_'.$value['ques_option_id'].'" style="display:none;">';
					if($value['if_sub_question_checked'] == 'Yes' && $value['dependant_cnt'] > 0){

						for ($i=0; $i < $value['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$value['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			          
		           }
		            $str.='</div>';
			           $str.=' <div class="col-12">
			               <div class="form-group">';
	           }
			}

		}
		elseif($tag_name == 'textbox'){
			//$str.=$salutation_flag;
			if(sizeof($option_array) >1){

				//if($salutation_flag = 0){
					$str.= '</div> </div>';
				//}
				$str.= '<div class="row mx-0">';
				foreach ($option_array as $key => $value) {
					//if($salutation_flag = 0){
					 $str.= '<div class="col-6"> <div class="form-group">';
					//}

					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{

						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}

					if($value['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}

					$str.= "<input type='text' ".$length." id='".$question_id."' data-optn-id='".$value['ques_option_id']."'" ;
					$str.="data-id='[".json_encode($option_array[$key])."]'";
					$str.="placeholder='".$value['option']."'";
					if($if_summation == 'Yes'){
						$str.="class='form-control dependantcls calculate_total' value='' ".$call_function.">";
					}
					else{
						$str.="class='form-control dependantcls' value='' ".$call_function.">";
					}
					//if($salutation_flag = 0){
						$str.= '</div> </div>';
					//}
				}
				//$str.='--'.$if_summation;
				if($if_summation == 'Yes'){
			    	$str.='<div class="col-6"> <div class="form-group">';
			    	$str.='<label>Total</label>';
					$str.= "<input type='text' class='form-control total_cnt_class' id='".$question_id."'";
					$str.="placeholder='Total' readonly='readonly' value=''>";
					$str.='</div></div>';
					//$str.='<div class="col-6"> <div class="form-group">';
					//$str.='<button id="calculate_total">Total</button>';
					//$str.='</div></div>';
				}
				//if($salutation_flag = 0){
				    $str.= '</div>';
					$str.='<div class="col-12"> <div class="form-group">';
				//}

			}
			else{
				//$str.='<div class="col-12"> <div class="form-group">';
				if(isset($option_array) && sizeof($option_array)>0){
					if($option_array[0]['validation_title'] == "Length" && $option_array[0]['min_value'] > 0)
					{

						if($option_array[0]['sub_validation_id'] == 18){
							$length =  'maxlength='.$option_array[0]['min_value'];
						}
						else if($option_array[0]['sub_validation_id'] == 23){
							$length =  'minlength='.$option_array[0]['min_value'];
						
						}	
					}

					if($option_array[0]['validation_id'] == 1){
						$call_function = 'onkeypress="return isNumber(event)"';
					}
					else{
						$call_function = '';
					}
					$str.= "<input type='text' class='form-control dependantcls' ".$length." id='".$question_id."'  data-optn-id='".$option_array[0]['ques_option_id']."' data-id='".json_encode($option_array)."' placeholder='Enter Answer' ".$call_function.">";

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textbox_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';
			               
			           }
			           $str.='</span>';
			          
		           }
				}
				
			}
			
			$str.='<span id="text_error_'.$question_id.'" style="color: red;"></span>';
			$str.='</br>';
			$str.='<span id="text_error_numonly_'.$question_id.'" style="color: red;"></span>';

			
		}
		elseif($tag_name == 'file'){
			$str.= '<input type="file" class="form-control" id="'.$question_id.'">';
		}
		elseif($tag_name == 'textarea'){
			//print_r($option_array);
			if(sizeof($option_array) > 0)
			{

				$str.= '<textarea class="form-control form-group textarea_dependantcls" id="'.$question_id.'" data-optn-id="'.$option_array[0]['ques_option_id'].'" 
					data-id="'.$option_array[0]['if_sub_question_checked'].'" rows="2" "'.$length.'" ></textarea>';
				foreach ($option_array as $key => $value) {
					
				
					if($value['validation_title'] == "Length" && $value['min_value'] > 0)
					{
						if($value['sub_validation_id'] == 18){
							$length =  'maxlength='.$value['min_value'];
						}
						else if($value['sub_validation_id'] == 23){
							$length =  'minlength='.$value['min_value'];
						
						}	
					}
			         
					

					if($option_array[0]['if_sub_question_checked'] == 'Yes' && $option_array[0]['dependant_cnt'] > 0){

						$str.='<span class="textarea_dependant_div_'.$option_array[0]['ques_option_id'].'" style="display:none;">';
						for ($i=0; $i < $option_array[0]['dependant_cnt']; $i++) { 
							$j = $i+1;    
			                $str.= '<span id="repeat_section_'.$option_array[0]['ques_option_id'].'_'.$j.'"></span>';

			                $arr['if_sub_question_checked'] = $option_array[0]['if_sub_question_checked']; 
			               
			           }
			           $str.='</span>';
					
					}
				} 
			}
			else
			{
				$str.= '<textarea class="form-control form-group" id="'.$question_id.'" placeholder="Enter Answer" rows="2" /></textarea>';
			}
			
			//$str.= '<textarea class="form-control dependantcls" id="'.$question_id.'" placeholder="Enter Answer" rows="2"  /></textarea>';
		}

		if($if_file_required == 'Yes'){
			$str.= '<label>'.$file_label.'</label>';
			$str.= '<input type="file" class="form-control">';
		}

		//echo $str.'</br>';

		return $str;
	}

	
}