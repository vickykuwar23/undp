<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Section
Author : Vicky K
*/

class Section extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Section Listing
    public function index()
    {
		
		
		//$this->db->where('survey_section_master.parent_section_id','0');
		// Permission Set Up
		$this->master_model->permission_access('15', 'view');
		$this->db->where('survey_section_master.is_deleted','0');
		$this->db->select("survey_section_master.parent_section_id,survey_section_master.section_name,survey_section_master.section_desc,survey_section_master.section_id,survey_section_master.status");
		//$this->db->join('survey_district_master','survey_block_master.district_id=survey_district_master.district_id','left',FALSE);
		$response_data = $this->master_model->getRecords("section_master",'','',array('section_id'=>'DESC'));
		
		$survey_data = $this->master_model->getRecords("survey_master",array('is_deleted'=>'0'),'',array('survey_id'=>'DESC'));

		$data['records'] = $response_data;		
		$data['survey_data'] = $survey_data;		
		$data['module_name'] = 'Survey';
		$data['submodule_name'] = 'Section';	
    	$data['middle_content']='section/index';
		$this->load->view('admin/admin_combo',$data);
   	}

   	public function get_datatable(){
   		if(!empty($_POST['survey_id'])){


   			$response_headers_query = $this->db->query("SELECT s1.*
				FROM survey_section_master s1  LEFT JOIN survey_section_mapping s3 
				ON s3.section_id = s1.section_id 
				WHERE s3.survey_id = ".$_POST['survey_id']."
				AND   s1.is_deleted = '0'
				ORDER BY s1.section_id DESC");

   			$response_data =  $response_headers_query->result_array();

	   	 	/*$this->db->where('s1.is_deleted','0');
			$this->db->where('survey_section_mapping.survey_id',$_POST['survey_id']);
			$this->db->select("s1.parent_section_id,s1.section_name,s1.section_desc,survey_section_master.section_id,s1.status,s2.section_name");
			$this->db->join('survey_section_mapping','survey_section_mapping.section_id=s1.section_id','left',FALSE);
			$this->db->join('survey_section_master s2','s2.parent_section_id=s1.section_id','left',FALSE);
			$response_data = $this->master_model->getRecords("section_master s1",'','',array('section_id'=>'DESC'));*/
			echo json_encode($response_data);
		}
		/*else{
			$this->db->where('survey_section_master.is_deleted','0');
			$this->db->select("survey_section_master.parent_section_id,survey_section_master.section_name,survey_section_master.section_desc,survey_section_master.section_id,survey_section_master.status");
			$response_data = $this->master_model->getRecords("section_master",'','',array('section_id'=>'DESC'));
   			//$response_data =  $response_headers_query->result_array();
   			echo json_encode($response_data);
		}*/
   	}

	 // Add Section
    public function add()
    {
		$parent_section_data = $this->master_model->getRecords("section_master",array('is_deleted'=>'0', 'parent_section_id' => '0', 'status' => 'Active'),'',array('section_name'=>'ASC'));
		//echo $this->db->last_query();die();
		// Permission Set Up
		$this->master_model->permission_access('15', 'add');
		
		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		
		//$section_data = $this->master_model->getRecords("section_master",'','',array('section_name'=>'ASC'));
        // Check Validation
		//$this->form_validation->set_rules('section_id', 'Parent Section Name', 'required');	
		$this->form_validation->set_rules('name', 'Section Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			
			$parent_section_id = $this->input->post('section_id');
			if($parent_section_id > 0){$pid = $parent_section_id;}else{$pid = 0;}
			$section_name = trim($this->input->post('name'));	
			$section_desc = trim($this->input->post('section_desc'));		
			$insertArr = array( 'parent_section_id' => $pid, 
				                'section_name' => $section_name, 
				                'section_desc' => $section_desc, 
				                'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('section_master',$insertArr);
			
			if($insertQuery > 0){
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
								 'action_name'=> "Add",
								 'module_name'=> 'Section Master',
								 'store_data'=> $json_encode_data,
								 'ip_address'=> $ipAddr,
							 	 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Section successfully created');
				redirect(base_url('xAdmin/section'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/section/add'));
			}
		}
		//$data['parentsectionList'] = $parent_section_data;
		$data['sectionList']    = $parent_section_data;
		$data['module_name']  = 'Survey';
		$data['submodule_name'] = 'Section';	
    	$data['middle_content']='section/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit Section 
	 public function edit($section_id)
    {

    	//$section_id = base64_decode($id);	

		// Permission Set Up
		$this->master_model->permission_access('15', 'edit');

		$parent_section_query = $this->db->query("SELECT s1.* FROM survey_section_master s1 
			WHERE s1.parent_section_id = 0
			AND s1.is_deleted = 0
			AND s1.status = 'Active'
			AND s1.section_id !=".$section_id);

		$parent_section_data = $parent_section_query->result_array();

		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		
		$rec = $this->master_model->getRecords("section_master",array('section_id'=>$section_id));
		//echo $this->db->last_query();
		//$this->form_validation->set_rules('section_id', 'Parent Section Name', 'required');	
		$this->form_validation->set_rules('name', 'Section Name', 'required|min_length[3]|xss_clean');	
		
		if($this->form_validation->run())
		{	
		
			$parent_section_id = $this->input->post('section_id');
			$section_name = trim($this->input->post('name'));
			$section_desc = trim($this->input->post('section_desc'));
			//if($parent_section_id > 0){$pid = $parent_section_id;}else{$pid = 0;}
			
			//print_r($this->input->post());
			$updateArr = array('parent_section_id' => $parent_section_id, 
				               'section_name' => $section_name, 
				               'section_desc' => $section_desc, 
				               'updated_by_id' => $this->session->userdata('user_id'));		
			$updateQuery = $this->master_model->updateRecord('section_master',$updateArr,array('section_id' => $section_id));
			//echo $this->db->last_query();die();
			if($updateQuery > 0){
				
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),
								'action_name'=> "Edit",
								'module_name'=> 'Section Master',
								'store_data'=> $json_encode_data,
								'ip_address'=> $ipAddr,
							 	'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Section successfully updated');
				redirect(base_url('xAdmin/section'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/section/edit/'.$id));
			}
		}
		
		$data['sectionList'] 	=   $parent_section_data;
		$data['section_record']	= 	$rec;
		$data['module_name'] 	= 	'Survey';
		$data['submodule_name'] = 	'Section';
        $data['middle_content']	=	'section/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update section Status
	 public function changeStatus(){
		
		// Permission Set Up
		$this->master_model->permission_access('15', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			
			$updateQuery = $this->master_model->updateRecord('section_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('section_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $id, 'status' => $value);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
							 'action_name'=> "Status",
							 'module_name'=> 'Section Master',
							 'store_data'=> $json_encode_data,
							 'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/section'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('section_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('section_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),
							 'action_name'=> "Status",
							 'module_name'=> 'Section Master',
							 'store_data'=> $json_encode_data,
							 'ip_address'=> $ipAddr,
							 'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/section'));		
		 }
		 
	 }

	 public function check_status(){

   		$where = array('section_id' => $_POST['section_id']);
   		$survey_data = $this->master_model->getRecords("survey_section_mapping", $where);
   		$str='';
   		if(sizeof(@$survey_data)>0){
   			$str.='Child records are present in Survey Master';
   		}

   		echo $str;

   	}
	 
	 // Soft Delete Section
	 public function delete($id){
		 
		// Permission Set Up
		$this->master_model->permission_access('15', 'delete');		
		 $id 	= base64_decode($id);		 
		 $updateQuery = $this->master_model->updateRecord('section_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('section_id' => $id));
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),
						'action_name'=> "Delete",
						'module_name'=> 'Section Master',
						'store_data'=> $json_encode_data,
						'ip_address'=> $ipAddr,
					    'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Section successfully deleted');
		 redirect(base_url('xAdmin/section'));	
		 
	 }


}