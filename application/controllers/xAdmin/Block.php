<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Block
Author : Vicky K
*/

class Block extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		// Permission Set Up
		$this->master_model->permission_access('5', 'view');
		$this->db->where('survey_block_master.is_deleted','0');
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		
		
		$this->db->select("survey_district_master.district_name,survey_block_master.block_name,survey_block_master.block_id,survey_block_master.status");
		$this->db->join('survey_district_master','survey_block_master.district_id=survey_district_master.district_id','left',FALSE);
		$response_data = $this->master_model->getRecords("block_master",'','',array('block_id'=>'DESC'));	
		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Block';	
    	$data['middle_content']='block/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add block
    public function add()
    {
		
		// Permission Set Up
		$this->master_model->permission_access('5', 'add');
		
		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		$district_data = $this->master_model->getRecords("district_master",'','',array('district_name'=>'ASC'));
		$state_data = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC'));
        // Check Validation
		$this->form_validation->set_rules('district_id', 'District Name', 'required');	
		$this->form_validation->set_rules('name', 'Block Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			
			$district_id = $this->input->post('district_id');
			$block_name = trim($this->input->post('name'));			
			$insertArr = array( 'district_id' => $district_id, 'block_name' => $block_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('block_master',$insertArr);
			
			if($insertQuery > 0){
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Block Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Block successfully created');
				redirect(base_url('xAdmin/block'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/block/add'));
			}
		}
		$data['districtList'] = $district_data;
		$data['stateList']    = $state_data;
		$data['module_name']  = 'Master';
		$data['submodule_name'] = 'Block';	
    	$data['middle_content']='block/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit block 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('5', 'edit');
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		$response_data = $this->master_model->getRecords("district_master",'','',array('district_name'=>'ASC'));	
		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		$block_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("block_master",array('block_id'=>$block_id));
		$this->form_validation->set_rules('district_id', 'District Name', 'required');	
		$this->form_validation->set_rules('name', 'Block Name', 'required|min_length[3]|xss_clean');	
		
		if($this->form_validation->run())
		{	
		
			$district_id = $this->input->post('district_id');
			$block_name = trim($this->input->post('name'));
			
			$updateArr = array('district_id' => $district_id, 'block_name' => $block_name, 'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('block_master',$updateArr,array('block_id' => $block_id));
			if($updateQuery > 0){
				
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Block Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Block successfully updated');
				redirect(base_url('xAdmin/block'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/block/edit/'.$id));
			}
		}
		
		$data['districtList'] 	= $response_data;
		$data['block_record']= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Block';
        $data['middle_content']	=	'block/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update block Status
	 public function changeStatus(){
		
		// Permission Set Up
		$this->master_model->permission_access('5', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			
			$updateQuery = $this->master_model->updateRecord('block_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('block_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $id, 'status' => $value);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Block Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/block'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('block_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('block_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Block Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/block'));		
		 }
		 
	 }
	 
	 // Soft Delete block
	 public function delete($id){
		 
		// Permission Set Up
		$this->master_model->permission_access('5', 'delete');		
		 $id 	= base64_decode($id);		 
		 $updateQuery = $this->master_model->updateRecord('block_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('block_id' => $id));
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Block Master',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT']));
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Block successfully deleted');
		 redirect(base_url('xAdmin/block'));	
		 
	 }


}