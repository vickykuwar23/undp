<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Test
Author : Vicky K
*/

class Test extends CI_Controller 
{
	
	var $table = 'survey_users';
	var $column_order = array(null, 'username','first_name','last_name','contact_no','email','role_name','status'); //set column field database for datatable orderable
	var $column_search = array('username','first_name','last_name','contact_no','email','role_name'); //set column field database for datatable searchable 
	var $order = array('user_id' => 'asc'); // default order 
	
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {	 
		$data['module_name'] 	= 'Test';
		$data['submodule_name'] = '';	
    	$data['middle_content']	= 'test/index';
		$this->load->view('admin/admin_combo',$data);
   	 }
	 
	
	public function ajax_list()
	{ 
		$list = $this->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $customers) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $customers->username;
			$row[] = $customers->first_name;
			$row[] = $customers->last_name;
			$row[] = $customers->contact_no;
			$row[] = $customers->email;
			$row[] = $customers->role_name;
			$row[] = $customers->status;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->count_all(),
						"recordsFiltered" => $this->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	} 
	 
	public function _get_datatables_query()
	{
		
		//add custom filter here
		if($this->input->post('username'))
		{
			$this->db->where('survey_users.username', $this->input->post('username'));
		}
		if($this->input->post('first_name'))
		{
			$this->db->like('survey_users.first_name', $this->input->post('first_name'));
		}
		if($this->input->post('last_name'))
		{
			$this->db->like('survey_users.last_name', $this->input->post('last_name'));
		}
		if($this->input->post('email'))
		{
			$this->db->like('survey_users.email', $this->input->post('email'));
		}		

		$this->db->select("survey_role_master.role_id,survey_role_master.role_name,survey_users.user_id,survey_users.contact_no,survey_users.username,survey_users.first_name,survey_users.last_name,survey_users.email,survey_users.status, survey_users.is_deleted");
		$this->db->join("survey_role_master",'survey_users.role_id=survey_role_master.role_id','INNER', FALSE);
		//$this->master_model->getRecords("users",array('users.user_id !='=>'1', 'users.is_deleted' => '0'));
		$this->db->from($this->table);
		$where = "survey_users.user_id != '1' AND survey_users.is_deleted = '0'";
		$this->db->where($where);
		//echo $this->db->last_query();die();
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		//$this->db->from($this->table);
		$this->db->select("survey_role_master.role_id,survey_role_master.role_name,survey_users.user_id,survey_users.contact_no,survey_users.username,survey_users.first_name,survey_users.last_name,survey_users.email,survey_users.status");
		$this->db->join("survey_role_master",'survey_users.role_id=survey_role_master.role_id','INNER', FALSE);
		$response_data = $this->master_model->getRecords("users",array('users.user_id !='=>'1', 'users.is_deleted' => '0'),'',array('users.user_id'=>'DESC'));
		return count($response_data);
		//return $this->db->count_all_results();
	} 

}