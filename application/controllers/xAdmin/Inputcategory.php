<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Inputcategory
Author : Vicky K
*/

class Inputcategory extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		 // Permission Set Up
    	$this->db->where('is_deleted','0');
		$this->master_model->permission_access('13', 'view');
		
		$response_data = $this->master_model->getRecords("response_validation_master",'','',array('validation_id'=>'DESC'));		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Survey';
		$data['submodule_name'] = 'Response_Validation';	
    	$data['middle_content']='response-validation/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add state
    public function add()
    {
		 // Permission Set Up
		$this->master_model->permission_access('13', 'add');
		
        // Check Validation
		$this->form_validation->set_rules('name', 'Input Category', 'required|min_length[4]|xss_clean');	
		if($this->form_validation->run())
		{	
			$validation_type = trim($this->input->post('name'));			
			$insertArr = array( 'validation_type' => $validation_type, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('response_validation_master',$insertArr);
			
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Input Category  Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Input Category successfully created');
				redirect(base_url('xAdmin/inputcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/inputcategory/add'));
			}
		}
		
		$data['module_name'] = 'Survey';
		$data['submodule_name'] = 'Response_Validation';	
    	$data['middle_content']='response-validation/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit state 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('13', 'edit');
		$id = base64_decode($id);	
		$rec = $this->master_model->getRecords("response_validation_master",array('validation_id'=>$id));
		$this->form_validation->set_rules('name', 'Input Category', 'required|min_length[3]|xss_clean');
		
		if($this->form_validation->run())
		{	
		
			$validation_type = trim($this->input->post('name'));			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('validation_type' => $validation_type, 'updated_by_id' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('response_validation_master',$updateArr,array('validation_id' => $id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Input Category Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Input Category successfully updated');
				redirect(base_url('xAdmin/inputcategory'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/inputcategory/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Survey';
		$data['submodule_name'] = 	'Response_Validation';
        $data['middle_content']	=	'response-validation/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update state Status
	 public function changeStatus(){
		 
		 // Permission Set Up
		$this->master_model->permission_access('13', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('response_validation_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('validation_id' => $id));
			 
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Input Category Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/inputcategory'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('response_validation_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('validation_id' => $id)); 
			
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Input Category Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/inputcategory'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 // Permission Set Up
		$this->master_model->permission_access('13', 'delete');
		
		 $id 	= base64_decode($this->uri->segment(4));
		 $updateQuery = $this->master_model->updateRecord('response_validation_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('validation_id' => $id));
		 
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData 			= array('user_id' => $this->session->userdata('user_id'),
						 			'action_name'=> "Delete",
						 			'module_name'=> 'Input Category Master',
									'store_data'=> $json_encode_data,
									'ip_address'=> $ipAddr);

		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		
		 $this->session->set_flashdata('success','Input Category successfully deleted');
		 redirect(base_url('xAdmin/inputcategory'));	
		 
	 }


}