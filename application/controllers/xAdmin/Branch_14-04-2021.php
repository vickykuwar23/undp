<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Branch
Author : Vicky K
*/

class Branch extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		$response_data = $this->master_model->getRecords("branch_office",'','',array('branch_id'=>'DESC'));		
		$data['records'] 		= $response_data;		
		$data['module_name'] 	= 'Master';
		$data['submodule_name'] = 'Branch';	
    	$data['middle_content']	= 'offices-branch/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add Role
    public function add()
    {
        // Check Validation
		$this->form_validation->set_rules('name', 'Branch/Office Name', 'required|min_length[4]|xss_clean');	
		if($this->form_validation->run())
		{	
			$branch_name = trim($this->input->post('name'));			
			$insertArr = array( 'branch_name' => $branch_name, 'createdByUserid' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('branch_office',$insertArr);
			
			if($insertQuery > 0){
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Branch Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Branch/Office Name successfully created');
				redirect(base_url('xAdmin/branch'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/branch/add'));
			}
		}
		
		$data['module_name'] 	= 'Master';
		$data['submodule_name'] = 'Branch';	
    	$data['middle_content']	= 'offices-branch/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit Role 
	 public function edit($id)
    {
		$branch_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("branch_office",array('branch_id'=>$branch_id));
		$this->form_validation->set_rules('name', 'Branch/Office Name', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{	
		
			$branch_name = trim($this->input->post('name'));	
			
			$updateAt = date('Y-m-d H:i:s');
			$updateArr = array('branch_name' => $branch_name, 'updatedByUserid' => $this->session->userdata('user_id'));			
			$updateQuery = $this->master_model->updateRecord('branch_office',$updateArr,array('branch_id' => $branch_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Branch Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Branch/Office Name successfully updated');
				redirect(base_url('xAdmin/branch'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/branch/edit/'.$id));
			}
		}
		
		$data['role_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Branch';
        $data['middle_content']	=	'offices-branch/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update Role Status
	 public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('branch_office',array('status'=>$value),array('branch_id' => $id));
			 
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();

			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Branch Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/branch'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('branch_office',array('status'=>$value),array('branch_id' => $id)); 
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Branch Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/branch'));		
		 }
		 
	 }
	 
	 // Soft Delete Role
	 public function delete($id){
		 
		 $id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('branch_office',array('is_deleted'=>1),array('branch_id' => $id));
		 // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Branch Master',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Branch/Office successfully deleted');
		 redirect(base_url('xAdmin/branch'));	
		 
	 }


}