<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Survey Master
Author : Priyanka Wadnere
*/

class Survey extends CI_Controller 
{

	function __construct() {
	
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	

		$this->user_id            	=  $this->session->userdata('user_id');
        //$this->name               =  $this->session->userdata('nabcons_username');
        $this->ip                 	=  $this->input->ip_address();
        $this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'survey';
        $this->module_title       =  "Survey Listing";
        $this->module_view_folder =  "survey/";  

    }

	// Role Listing
    public function index()
    {

    	// Permission Set Up
		$this->master_model->permission_access('8', 'view');

    	$where = array('survey_master.is_deleted' => 0);
    	$this->db->join('survey_category_master','survey_category_master.category_id = survey_master.category_id');

		$survey_data = $this->master_model->getRecords("survey_master",$where,'survey_master.*, survey_category_master.category_name, (SELECT COUNT(1) FROM survey_question_master where survey_question_master.survey_id = survey_master.survey_id AND survey_question_master.parent_question_id = 0 and survey_question_master.is_deleted = 0) total_cnt, (SELECT COUNT(1) FROM survey_assign_users usr where survey_master.survey_id = usr.survey_id and usr.is_deleted=0) surveyer_cnt, (SELECT COUNT(1) FROM survey_question_master where survey_question_master.survey_id = survey_master.survey_id) cnt, (SELECT COUNT(1) FROM survey_question_master q1 WHERE survey_master.survey_id = q1.survey_id) ques_mapp, (SELECT COUNT(1) FROM survey_section_questions ssq where ssq.survey_id = survey_master.survey_id) ques_cnt, (SELECT COUNT(1) FROM survey_response_headers srh where survey_master.survey_id = srh.survey_id) response_cnt, (SELECT COUNT(1) FROM survey_section_questions ssq where survey_master.survey_id = ssq.survey_id AND ssq.global_question_id !=0) global_ques_cnt',array('survey_id'=>'DESC'));	

		//print_r($survey_data); die;

		$this->data['page_title']      = 'Survey Listing';//$this->module_title." List";
		$this->data['module_title']    = $this->module_title;
		$this->data['module_name']     = 'Survey';
		$this->data['submodule_name']  = 'Survey_list';
		$this->data['survey_data']     = $survey_data;
		$this->data['middle_content']  = $this->module_view_folder."survey_list";

		$this->load->view('admin/admin_combo',$this->data);
   	 }

   	public function add(){

		$this->master_model->permission_access('8', 'add');
        if(isset($_POST['submit']))
		{
			
			$this->form_validation->set_rules('title', 'Title', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('category', 'Category', 'required');	
			//$this->form_validation->set_rules('section', 'Section', 'required');

			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			
			$ipAddr		= $this->master_model->get_client_ip();

			// Check Validation
			if($this->form_validation->run())
			{	
				$dataArr = array( 	'title' 		=> trim($this->input->post('title')),
									'description' 	=> $this->input->post('description'),
									'user_id' 		=> $this->user_id,
									'category_id' 	=> trim($this->input->post('category')),
									'created_by_id' => $this->session->userdata('user_id')
								);	
					
				$insertQuery = $this->master_model->insertRecord('survey_master',$dataArr);

				if($insertQuery > 0){						
					
					if(count($this->input->post('section')) > 0){
						foreach($this->input->post('section') as $sec){
							$dataSection = array(
													'survey_id' 	=> $insertQuery,
													'section_id' 	=> $sec
												);
							$dataSectionRes = $this->master_model->insertRecord('section_mapping',$dataSection);					
						}
					}

					$postArr			= $this->input->post();	
					$json_encode_data 	= json_encode($postArr);
					$logData = array('user_id' => $this->session->userdata('user_id'),
									 'action_name'=> "Add",
									 'module_name'=> 'Survey',
									 'store_data'=> $json_encode_data,
									 'ip_address'=> $ipAddr);

					if($logCapture[0]['log_detect'] == 'Yes'){
						$this->master_model->insertRecord('logs',$logData);
					}

					if($insertQuery > 0){
						$this->session->set_flashdata('success','Survey Created successfully');
						redirect(base_url('xAdmin/survey'));
					} else {
						$this->session->set_flashdata('error','Something went wrong! Please try again.');
						redirect(base_url('xAdmin/survey/add'));
					}
				}
			}
		}//submit	
			
		$sectionList = $this->master_model->getRecords("section_master",array('is_deleted' => 0, 'parent_section_id' => '0', 'status' => 'Active'),'',array('section_name'=>'ASC')); //, 'parent_section_id' => 0

		$this->data['action']  = 'add';
		$this->data['survey_section']     = '';
		$this->data['survey_id']    = 0;

		$category = $this->master_model->getRecords("survey_category_master", array('is_deleted' => 0, 'status' => 'Active'));	

		$this->data['sectionList']    	= $sectionList;
		$this->data['category']    		= $category;
		$this->data['page_title']       = $this->module_title;
		$this->data['module_title']     = $this->module_title;
		$this->data['module_name']      = 'Survey';
		$this->data['page_sub_title']   = 'Add Survey';
		$this->data['submodule_name']   = 'Survey_list';
		$this->data['middle_content']   = $this->module_view_folder."survey_add";
        $this->load->view('admin/admin_combo',$this->data);
   	}

   	public function edit($id){
   		$this->master_model->permission_access('8', 'edit');
   		if(isset($_POST['submit'])){

			
			$this->form_validation->set_rules('title', 'Title', 'required');
			//$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('category', 'Category', 'required');	
			//$this->form_validation->set_rules('section', 'Section', 'required');

			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			
			$ipAddr		= $this->master_model->get_client_ip();

			// Check Validation
			if($this->form_validation->run())
			{	
				$dataArr = array( 	'title' 		=> trim($this->input->post('title')),
									'description' 	=> trim($this->input->post('description')),
									'user_id' 		=> $this->user_id,
									'category_id' 	=> trim($this->input->post('category')),
									'updated_by_id' => $this->session->userdata('user_id')
								);

					
				$dataArr['updated_by_id'] = $this->session->userdata('user_id')	;		
				//echo $id; print_r($updateArr); die;
				$updateQuery = $this->master_model->updateRecord('survey_master',$dataArr,array('survey_id'=>$id));
					//echo $this->db->last_query();
					
				if($updateQuery > 0){
					//echo ">>>".$id;
					//die();
					// Delete Previous record related survey id

					//print_r($_POST['section']);
					if(count($this->input->post('section')) == 0){
						$this->master_model->deleteRecord('section_mapping','survey_id',$id);
					}

					//die();
					
					// Add New Record
					if(count($this->input->post('section')) > 0){

						
						//echo $this->db->last_query();
					
						foreach($this->input->post('section') as $sec){
							$dataSection = array(
													'survey_id' 	=> $id,
													'section_id' 	=> $sec
												);
							$dataSectionRes = $this->master_model->insertRecord('section_mapping',$dataSection);		//echo $this->db->last_query();			
						}
				
					}						
					//die();
					$postArr			= $this->input->post();	
					$json_encode_data 	= json_encode($postArr);
					$logData = array('user_id' 		=> $this->session->userdata('user_id'),
						             'action_name'	=> "Edit",
						             'module_name'	=> 'Survey',
									 'store_data'	=> $json_encode_data,
									 'ip_address'=> $ipAddr);

					if($logCapture[0]['log_detect'] == 'Yes'){
						$this->master_model->insertRecord('logs',$logData);
					}
					if($updateQuery > 0){
						$this->session->set_flashdata('success','Survey Updated successfully');
						redirect(base_url('xAdmin/survey'));
					} else {
						$this->session->set_flashdata('error','Something went wrong! Please try again.');
						redirect(base_url('xAdmin/survey/edit/'.$id));
					}
				}
					
			}
		}//submit

		$sectionList = $this->master_model->getRecords("section_master",array('is_deleted' => 0, 'parent_section_id' => '0'),'',array('section_name'=>'ASC')); //, 'parent_section_id' => 0
		$survey_id = base64_decode($id);
		$this->db->where('survey_id',$survey_id);
		$survey_data = $this->master_model->getRecords("survey_master",array('is_deleted' => 0));
		$getSection = $this->master_model->getRecords("section_mapping",array('survey_id' => $survey_id));
		$this->data['survey_data']     = $survey_data;
		$this->data['survey_section']  = $getSection;
		$this->data['survey_id']       = $survey_id;
		$this->data['action']          = 'edit';
	
		$category = $this->master_model->getRecords("survey_category_master", array('is_deleted' => 0, 'status' => 'Active'));	

		$this->data['sectionList']    	= $sectionList;
		$this->data['category']    		= $category;
		$this->data['page_title']       = 'Survey Listing'; //.$this->module_title;
		$this->data['module_title']     = $this->module_title;
		$this->data['page_sub_title']   = 'Update Survey';
		$this->data['module_name']      = 'Survey';
		$this->data['submodule_name']   = 'Survey_list';
		$this->data['middle_content']   = $this->module_view_folder."survey_add";
        $this->load->view('admin/admin_combo',$this->data);
   	}

   	public function check_status(){
		
   		$where = array('survey_id' => $_POST['survey_id']);
   		$question_data = $this->master_model->getRecords("survey_question_master", $where);
   		$str='';
   		if(sizeof(@$question_data)>0){
   			$str.='Child records are present in Question Master';

   			$question_mapping_data = $this->master_model->getRecords("survey_section_questions", $where);

   			if(sizeof(@$question_mapping_data)>0){
   				$str.=' And Question Mapping';
   			}
   		}

   		echo $str;

   	}
	
	// Soft Delete Role
	public function delete($id){
		
		$this->master_model->permission_access('8', 'delete');	
		$id 	= $this->uri->segment(4);		 
		$survey_id = base64_decode($id);
		$delete_array = array('is_deleted' =>1,
		                       'deleted_by_id' =>$this->user_id,
		                       'deleted_on' => date('Y-m-d H:i:s'));

		$updateQuery = $this->master_model->updateRecord('survey_master', $delete_array, array('survey_id' => $survey_id));



		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);		
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' 		=> $this->session->userdata('user_id'),
						 'action_name'	=> "Delete",
						 'module_name'	=> 'Survey',
						 'store_data'	=> $json_encode_data,
						 'ip_address'	=> $ipAddr);

		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}

		$this->session->set_flashdata('success','Survey successfully deleted');
		redirect(base_url('xAdmin/survey'));	
		 
	}

	public function changeStatus(){
		$this->master_model->permission_access('8', 'status');
		$survey_id 	= $_POST['survey_id'];
		$status 	= $_POST['status'];
		
		if($status != ''){
			 
			$updateQuery = $this->master_model->updateRecord('survey_master',array('status'=> $status),array('survey_id' => $survey_id));
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $survey_id, 'status' => $status);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			if($updateQuery == 1){
				echo 'Status Changed Successfully.';
			}	
			 
		} 
		 
	}
	 
	// Update Role Status
	/*public function changeStatus(){
		$this->master_model->permission_access('8', 'status');
		$id 	= $this->uri->segment(4);
		$value = ucfirst($this->uri->segment(5));
		 
		if($value == 'Active'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_master',array('status'=>'Inactive'),array('survey_id' => $id));

			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $id, 'status' => 'Inactive');
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey'));	
			 
		} else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_master',array('status'=>'Active'),array('survey_id' => $id)); 

			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => 'Active');	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id'		=> $this->session->userdata('user_id'),
				             'action_name'	=> "Status",
				             'module_name'	=> 'Survey',
							 'store_data'	=> $json_encode_data,
							 'ip_address'	=> $ipAddr);

			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}

			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey'));		
		}
		 
	}*/
	 
	public function assign_users($survey_id){
		//error_reporting(0);
		//echo 'assign_users'; die;
		//$this->db->where('is_deleted', 0);
		//$this->db->order_by('user_id','DESC');
		//print_r($_POST);
		if(isset($_POST['submit']))
		{
			// Users Array
			$users 	= $this->input->post('users');
			
			if(count($users) > 0){
				$dateTime = date("Y-m-d H:i:s");
				$updateQuery = $this->master_model->updateRecord('assign_users',array('is_deleted'=>'1', 'deleted_on' => $dateTime),array('survey_id' => $survey_id)); 	

				foreach($users as $users_id)
				{
					$dataArr = array( 	'survey_id' 	=> $survey_id,
										'user_id' 		=> $this->input->post('description'),
										'user_id' 		=> $users_id,
										'created_by_id' => $this->session->userdata('user_id')
								);	
					
					$insertQuery = $this->master_model->insertRecord('assign_users',$dataArr);
				}
			}

			//$update_array = array('users' => $usersStr);
			//$updateQuery = $this->master_model->updateRecord('survey_master', $update_array, array('survey_id' => $survey_id));
			
			if(@$insertQuery >0 ){
				$this->session->set_flashdata('success','User Assigned successfully');
				redirect(base_url('xAdmin/survey/assign_users/'.base64_encode($survey_id)));	
			} 
		}
		$survey_id = base64_decode($survey_id);

		$config_survey_id = $this->config->item('education_survey_id');
		$approver2_role_id = $this->config->item('approver2_role_id');

		if($config_survey_id == $survey_id){
			$this->db->where('role_id !=', $approver2_role_id);
		}

		//$this->db->where('is_deletedd', 0);
		$user_data = $this->master_model->getRecords("survey_users",array('is_deleted' => '0', 'status' => 'Active'));

		//echo $this->db->last_query();


    	$survey_data = $this->master_model->getRecords("survey_assign_users",array('survey_id' => $survey_id, 'status' => 'Active', 'is_deleted' => '0'));
		$this->data['page_title']      = "Assign Users";
		$this->data['module_title']    = $this->module_title;
		$this->data['module_name']     = 'Assign Users';
		$this->data['submodule_name']  = '';
		$this->data['user_data']       = $user_data;
		$this->data['survey_data']     = $survey_data;
		$this->data['survey_id']       = $survey_id;
		$this->data['middle_content']  = $this->module_view_folder."assign_users";

		$this->load->view('admin/admin_combo',$this->data);
	}

	public function showdata()
    {
    	$where = array('survey_master.is_deleted' => 0);

    	$this->db->join('survey_category','survey_category.category_id = survey_master.category_id');

		$survey_data = $this->master_model->getRecords("survey_master",$where,'survey_master.survey_id, survey_category.category_name, survey_master.title, survey_master.description,""',array('survey_id'=>'DESC'));		

		//print_r($survey_data); die;
		
		/*$jsonArr = array();
		foreach ($survey_data as $key => $value) {
			array_push($jsonArr, $value);
		}*/
		/*echo '{ "draw": 1,
  			   "recordsTotal": '.count($survey_data).',
  			   "recordsFiltered": '.count($survey_data).',
			"data":'.json_encode($jsonArr).'}';*/

		for($i=0;$i<sizeof($survey_data);$i++)
		{
			$temp[]=array_values($survey_data[$i]);
		}
		
		$temp=json_encode($temp,JSON_PRETTY_PRINT);

		echo '{"draw": 1,
  			   "recordsTotal": '.count($survey_data).',
  			   "recordsFiltered": '.count($survey_data).',
			   "data":'.$temp.'}';

		
		//$this->load->view('admin/admin_combo',$this->data);
   	 }

    public function datatable()
    {
    	//$where = array('is_deleted' => 0);

    	/*$this->db->join('survey_category','survey_category.category_id = survey_master.category_id');
		$survey_data = $this->master_model->getRecords("survey_master",$where,'survey_master.*, survey_category.category_name',array('survey_id'=>'DESC'));	
*/
		$this->data['page_title']      = $this->module_title." test";
		$this->data['module_title']    = $this->module_title;
		$this->data['module_name']     = 'Test';
		$this->data['submodule_name']  = '';
		//$this->data['survey_data']     = $survey_data;
		$this->data['middle_content']  = $this->module_view_folder."test";

		$this->load->view('admin/admin_combo',$this->data);
   	}


   	// Add Role
    /*public function submit($action, $id)
    {
       $sectionList = $this->master_model->getRecords("section_master",array('is_deleted' => 0, 'parent_section_id' => 0),'',array('section_name'=>'ASC'));
        if(isset($_POST['submit']))
		{
			//$this->master_model->permission_access('8', 'add');
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('description', 'Description', 'required');
			$this->form_validation->set_rules('category', 'Category', 'required');	
			//$this->form_validation->set_rules('section', 'Section', 'required');

			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			
			$ipAddr		= $this->master_model->get_client_ip();

			// Check Validation
			if($this->form_validation->run())
			{	
				$dataArr = array( 	'title' 		=> trim($this->input->post('title')),
									'description' 	=> trim($this->input->post('description')),
									'user_id' 		=> $this->user_id,
									'category_id' 	=> trim($this->input->post('category'))
								);
				if($action == 'add'){
					
					
					$dataArr['created_by_id'] = $this->session->userdata('user_id')	;		
					$insertQuery = $this->master_model->insertRecord('survey_master',$dataArr);
					if($insertQuery > 0){						
						
						if(count($this->input->post('section')) > 0){
							foreach($this->input->post('section') as $sec){
								$dataSection = array(
														'survey_id' 	=> $insertQuery,
														'section_id' 	=> $sec
													);
								$this->master_model->insertRecord('section_mapping',$dataSection);					
							}
						}

						$postArr			= $this->input->post();	
						$json_encode_data 	= json_encode($postArr);
						$logData = array('user_id' => $this->session->userdata('user_id'),
										 'action_name'=> "Add",
										 'module_name'=> 'Survey',
										 'store_data'=> $json_encode_data,
										 'ip_address'=> $ipAddr);

						if($logCapture[0]['log_detect'] == 'Yes'){
							$this->master_model->insertRecord('logs',$logData);
						}

						$flag = 1;
					}
					$msg = 'Created';
				}

				if($action == 'edit'){
					
					$dataArr['updated_by_id'] = $this->session->userdata('user_id')	;		
					//echo $id; print_r($updateArr); die;
					$updateQuery = $this->master_model->updateRecord('survey_master',$dataArr,array('survey_id'=>$id));
						
					if($updateQuery > 0){
						//echo ">>>".$id;die();
						// Delete Previous record related survey id
						$this->master_model->deleteRecord('section_mapping','survey_id',$id);
						
						// Add New Record
						if(count($this->input->post('section')) > 0){
							foreach($this->input->post('section') as $sec){
								$dataSection = array(
														'survey_id' 	=> $id,
														'section_id' 	=> $sec
													);
								$this->master_model->insertRecord('section_mapping',$dataSection);					
							}
							
							$flag = 1;
						}						
						
						$postArr			= $this->input->post();	
						$json_encode_data 	= json_encode($postArr);
						$logData = array('user_id' 		=> $this->session->userdata('user_id'),
							             'action_name'	=> "Edit",
							             'module_name'	=> 'Survey',
										 'store_data'	=> $json_encode_data,
										 'ip_address'=> $ipAddr);

						if($logCapture[0]['log_detect'] == 'Yes'){
							$this->master_model->insertRecord('logs',$logData);
						}

						$flag = 1;
					}
					$msg = 'Updated';
				}
				
				
				if($flag == 1){
					$this->session->set_flashdata('success','Survey '.$msg.' successfully');
					redirect(base_url('xAdmin/survey'));
				} else {
					$this->session->set_flashdata('error','Something went wrong! Please try again.');
					redirect(base_url('xAdmin/survey/submit/'.$action.'/'.$id));
				}
			}
		}

		if($action == 'edit'){
			$survey_id = base64_decode($id);
			$this->db->where('survey_id',$survey_id);
			$survey_data = $this->master_model->getRecords("survey_master",array('is_deleted' => 0));
			$getSection = $this->master_model->getRecords("section_mapping",array('survey_id' => $survey_id));
			$this->data['survey_data']     = $survey_data;
			$this->data['survey_section']     = $getSection;
			$this->data['survey_id']     = $survey_id;
			$this->data['action']  = 'edit';
		}
		else{
			$this->data['action']  = 'add';
			$this->data['survey_section']     = '';
			$this->data['survey_id']    = 0;
		}

		$category = $this->master_model->getRecords("survey_category_master");	

		$this->data['sectionList']    	= $sectionList;
		$this->data['category']    		= $category;
		$this->data['page_title']       = 'Add '.$this->module_title;
		$this->data['module_title']     = $this->module_title;
		$this->data['module_name']      = 'Survey';
		$this->data['submodule_name']   = '';
		$this->data['middle_content']   = $this->module_view_folder."survey_add";
        $this->load->view('admin/admin_combo',$this->data);
    }*/
	

}