<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Admin for admin basic function
Author : Vicky K
*/

class Admin_1 extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->library("session");
        $this->load->helper('captcha');
		$this->load->helper('url');
		$this->load->helper('security');
		//$this->load->library('Opensslencryptdecrypt');
		
		//session_start();	
    }
	
	public function index()
	{   
		if ($this->session->userdata('user_id')!='')  {
			redirect(base_url('xAdmin/admin/dashboard'));
		}
		$data['msg'] = '';
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
		$this->form_validation->set_rules('captcha', 'Capcha', 'trim|required|callback_check_captcha_code|xss_clean');
		//$encrptopenssl =  New Opensslencryptdecrypt();
		
		if ($this->form_validation->run() != FALSE)
		{		//echo "+++++++++++";die();

			//print_r($_POST); die;
			$user_name 		= mysqli_real_escape_string($this->db->conn_id,$this->input->post('username'));
			$user_password 	= mysqli_real_escape_string($this->db->conn_id,$this->input->post('password')); 
			$inputCaptcha 	= $this->input->post('captcha');			
			
			$u_name = $user_name;
			$p_name = md5($user_password);
			
			// Log Data Added
			$postArr			= $this->input->post();	
			$json_encode_data 	= json_encode($postArr);
			$ipAddr			  	= $this->master_model->get_client_ip();			
			
				
			$where = array("username" =>$u_name, "password" => $p_name, "is_deleted" => 0, "role_id!=" => $this->config->item( 'surveyor_role_id' ));
			$user_exist = $this->master_model->getRecordCount('users',$where);
			//echo $this->db->last_query();
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				if($user_exist)
				{ 
					$where = array("username" => $u_name,"password" => $p_name );
					$user_all_details = $this->master_model->getRecords("users",$where);
					//echo "<pre>";print_r($user_all_details);die();
							if($user_all_details[0]['status']=="Active" && $user_all_details[0]['role_id'] !=  $this->config->item( 'surveyor_role_id' ))
							{
								$session_data['user_id'] 	= $user_all_details[0]['user_id'];
								$session_data['username'] 	= $user_all_details[0]['username'];
								$session_data['fullname'] 	= ucfirst($user_all_details[0]['first_name'])." ".ucfirst($user_all_details[0]['last_name']);
								$session_data['email'] 		= $user_all_details[0]['email'];
								$session_data['role_id'] 	= $user_all_details[0]['role_id'];	
								$this->session->set_userdata($session_data);
								// Log Capture
								$logData = array(
													'user_id' 		=> $session_data['user_id'],
													'action_name' 	=> "Login Success",
													'module_name'	=> 'Login',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr,
							 						'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT'])
												);
								
									
								if($logCapture[0]['log_detect'] == 'Yes'){
									$this->master_model->insertRecord('logs',$logData);
								}
								redirect(base_url('xAdmin/admin/dashboard'));
							}
							else
							{ 
								// Log Capture
								$logData = array(
													'user_id' 		=> '',
													'action_name' 	=> "Access Block This User",
													'module_name'	=> 'Login',
													'store_data'	=> $json_encode_data,
													'ip_address'	=> $ipAddr,
							 						'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT'])
												);
								if($logCapture[0]['log_detect'] == 'Yes'){
									$this->master_model->insertRecord('logs',$logData);
								}
								
								$data['msg'] = 'Sorry, your adminstrator has blocked access to this User';
							}
				}
				else
				{
					// Log Capture
					$logData = array(
										'user_id' 		=> '',
										'action_name' 	=> "Invalid Credential",
										'module_name'	=> 'Login',
										'store_data'	=> $json_encode_data,
										'ip_address'	=> $ipAddr,
							 			'user_agent'	=> json_encode($_SERVER['HTTP_USER_AGENT'])
									);
					if($logCapture[0]['log_detect'] == 'Yes'){
						$this->master_model->insertRecord('logs',$logData);
					}
					$data['msg'] = 'Invalid Username or Password!';
					
				}
		
		}
			
		// Captcha configuration
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
		
        // Unset previous captcha and store new captcha word
        //$this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Send captcha image to view
		$data['captchaImg'] = $captcha['image'];

		$data["page_title"] ="Admin Login" ; 
		$this->load->view('admin/login', $data);
	}

	public function refresh(){
        // Captcha configuration
		$this->session->unset_userdata('captchaCode');
        $config = array(
            'img_path'      => 'assets/captcha_images/',
            'img_url'       => base_url().'assets/captcha_images/',
            'img_width'     => '170',
            'img_height'    => 30,
            'word_length'   => 4,
            'font_size'     => 18,
			'pool'          => 'ABCDEFGHIJKLMNPQRSTUVWXYZ',
            'colors' => array(
			    'background' => array(186, 212, 237),
			    'border' => array(162, 171, 186),
			    'text' => array(5, 32, 77),
			    'grid' => array(151, 173, 196)
			  )
		);
		$captcha = create_captcha($config);
        
        // Unset previous captcha and store new captcha word        
        $this->session->set_userdata('captchaCode',$captcha['word']);
        
        // Display captcha image
        echo $captcha['image'];
    }
	
	public function check_captcha_code($code)
	{
		
	  // check if captcha is set -
		if($code == '')
		{
			$this->form_validation->set_message('check_captcha_code', '%s is required.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		
		if($code == '' || $_SESSION["captchaCode"] != $code)
		{  
			$this->form_validation->set_message('check_captcha_code', 'Invalid %s.'); 
			$this->session->set_userdata("captchaCode", rand(1,100000));
			return false;
		}
		if($_SESSION["captchaCode"] == $code)
		{   
			$this->session->unset_userdata("captchaCode");
			$this->session->set_userdata("captchaCode", rand(1,100000));
		
			return true;
		}
		
	}
	
	public function getCountRecords($status, $dist_id, $survey_id)
	{
		$returnCnt 		= 0;	
		$approvedCnt 	= 0;
		$inreviewCnt	= 0;
		$resubmittedCnt	= 0;
		$submittedCnt 	= 0;
		$totalResponse 	= 0;
		
		$allids = array();
		// Assign Survey To Users 
		$this->db->select('survey_id');
		$sqlUserSurvey = $this->master_model->getRecords("assign_users", array("user_id" => $this->session->userdata('user_id'), "is_deleted" => 0));
		
		if(count($sqlUserSurvey) > 0)
		{
			foreach($sqlUserSurvey as $s_ids)
			{
				$getSurveyId = $s_ids['survey_id'];
				array_push($allids, $getSurveyId);
				
			}
		}
		
		$joinSQL = '';
		$survey_id_assign = '';
		if(count($allids) == 1)
		{	
			$survey_id_assign = $allids[0];
			//$joinSQL = "AND survey_id IN('".$allids[0]."')";
			$joinSQL = "AND survey_id = '".$survey_id."'";
		}
		else if(count($allids) > 1)
		{		
			$survey_id_assign = $allids;
			$joinParameter = "" . implode( "','", $allids ) . "";
			//$joinSQL = "AND survey_id IN('".$joinParameter."')";
			$joinSQL = "AND survey_id = '".$survey_id."'";
		}
		
		// Get logged in user assigned revenue circles
		$this->db->select('revenue_id');			
		$sqlUserRevenueCircles = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
		$userRevenueCircles = $sqlUserRevenueCircles[0]['revenue_id'];
		if($userRevenueCircles)
		{	
			$userRevenueCirclesArr = explode(",", $userRevenueCircles);
			$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
			$joinSQL .= " AND (revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
		}
	
		if(count($allids) == 0)
		{
			$totalDistrictCount = 0;
			$submittedCnt = 0;
			$resubmittedCnt = 0;
			$approvedCnt = 0;
			$returnCnt = 0;
			$inreviewCnt = 0;
		}
		else 
		{
		
			$this->db->select('response_id, status');
			//$this->db->where_in('survey_id', $allids);
			//$this->db->where('survey_id', $survey_id);
			if($userRevenueCircles)
			{	
				$userRevenueCirclesArr = explode(",", $userRevenueCircles);
				$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
				
				$userRevenueCirclesSQL = "(revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
				$this->db->where($userRevenueCirclesSQL);
			}	
			$sqlDashboard = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id, "survey_id" => $survey_id));
			//echo $this->db->last_query(); 
			
			$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."' ".$joinSQL."");
			$totalDistrictCount = $sqlDashboardSubmitted->num_rows();
			//echo $this->db->last_query(); die();
		
			if(count($sqlDashboard) > 0)
			{				
				foreach($sqlDashboard as $res)
				{
					$responseID  = @$res['response_id'];
					$this->db->select('status');
					//$this->db->where_in('survey_id', $survey_id_assign);
					$this->db->order_by('status_id', 'desc');
					$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID, 'user_id' => $this->session->userdata('user_id')),'',array('status_id' => "ASC"));
					
					
					//$this->db->where_in('survey_id', $survey_id_assign);
					$submitResult = $this->master_model->getRecords("response_headers", array("response_id" => $responseID, "survey_id" => $survey_id));
					
					//Submittted Record Count 
					if(@$submitResult[0]['status'] == 'Submitted')
					{
						$submittedCnt++;
					}
					
					//ReSubmittted Record Count 
					if(@$submitResult[0]['status'] == 'Re-Submitted')
					{
						$resubmittedCnt++;
					}
					
					if($this->session->userdata('role_id') ==  $this->config->item('approver1_role_id'))
					{
						// Level 1 Approved Record Count
						if(@$trackResult[0]['status'] == 'Level 1 Approved')
						{
							$approvedCnt++;
						}
						
						// Level 1 Return Record Count
						if(@$trackResult[0]['status'] == 'Level 1 Returned')
						{
							$returnCnt++;
						}
						
						// Level 1 In Review Record Count
						if(@$trackResult[0]['status'] == 'Level 1 In Review')
						{
							$inreviewCnt++;
						}
					}
					else if($this->session->userdata('role_id') == $this->config->item('approver2_role_id'))
					{
						// Level 2 Approved Record Count
						if(@$trackResult[0]['status'] == 'Level 2 Approved')
						{
							$approvedCnt++;
						}
						
						// Level 2 Return Record Count
						if(@$trackResult[0]['status'] == 'Level 2 Returned')
						{
							$returnCnt++;
						}
						
						// Level 2  In Review Record Count
						if(@$trackResult[0]['status'] == 'Level 2 In Review')
						{
							$inreviewCnt++;
						}
					}
					
				}
			}
		}
		return array("total_response" => $totalDistrictCount, "Submitted" => $submittedCnt, "Resubmitted" => $resubmittedCnt, "Approved" => $approvedCnt, "Return" => $returnCnt, "Review" => $inreviewCnt);
	}
	
	
	public function getAdminCountRecords($status, $dist_id)
	{
		$returnCnt = 0;	
		$approvedCnt = 0;
		$submittedCnt = 0;	
		$inreviewCnt = 0;
		$this->db->select('response_id, status');
		$sqlDashboard = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id));
		
		/*$sqlDashboardSubmitted = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $dist_id, "status" => 'Submitted'));
		$submittedCnt = count($sqlDashboardSubmitted);*/
		
		//$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."' AND (status = 'Submitted' OR status = 'Re-Submitted' OR status = 'Level 1 In Review' OR status = 'Level 2 In Review')");
		$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$dist_id."'");
		$submittedCnt = $sqlDashboardSubmitted->num_rows();
		
		if(count($sqlDashboard) > 0)
		{				
			foreach($sqlDashboard as $res)
			{
				$responseID  = @$res['response_id'];
				//$this->db->select('status');
				//$this->db->order_by('status_id', 'desc');
				//$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
				
				//Submittted Record Count 
				/*if(@$trackResult[0]['status'] == 'Submitted')
				{
					$submittedCnt++;
				}*/
				
				// Approved Record Count
				if(@$res['status'] == 'Level 1 Approved' || @$res['status'] == 'Level 2 Approved')
				{
					$approvedCnt++;
				}
				
				// Return Record Count
				if(@$res['status'] == 'Level 1 Returned' || @$res['status'] == 'Level 2 Returned')
				{
					$returnCnt++;
				}
				
				if(@$res['status'] == 'Level 1 In Review' || @$res['status'] == 'Level 2 In Review')
				{
					$inreviewCnt++;
				}
			}
		}
		
		return array("Submitted" => $submittedCnt, "Approved" => $approvedCnt, "Return" => $returnCnt, "Review" => $inreviewCnt);
	}

	
	//showing dashboard
	public function dashboard()
	{   error_reporting(0);
		if($this->session->userdata('user_id') == "")
		{			
			redirect(base_url('xAdmin/admin'));
		}
		$calculateSubmitted = 0;
		$calculateApproved = 0;
		$calculateReturn = 0;
		$totalDistrictCount = 0;
		$totalResponsesCount = 0;
		$totalResubmittedCount = 0;
		$calculateReview=0;
		$sub = 0;
		$review = 0;
		$app = 0;
		$ret = 0;	
		if($this->session->userdata('role_id') == $this->config->item('system_admin_id') || $this->session->userdata('role_id') == $this->config->item('admin_role_id'))
		{			
			
			$this->db->select('response_id, status');
			$sqlDashboard = $totalReturn = $this->master_model->getRecords("response_headers", array("is_deleted" => '0'));
			//echo $this->db->last_query();
			if(count($sqlDashboard) > 0)
			{ 
					
				/*$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0'");
				$totalDistrictCount = $sqlDashboardSubmitted->num_rows();*/
				
				//Total Response Received Count 	
				$sqlDashboardResponse =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0'");
				$totalResponsesCount += $sqlDashboardResponse->num_rows();
				
				// Total Submitted Count
				$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND status='Submitted'");
				$totalDistrictCount += $sqlDashboardSubmitted->num_rows();
				
				// Total ReSubmitted Count
				$sqlDashboardReSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND status='Re-Submitted'");
				$totalResubmittedCount += $sqlDashboardReSubmitted->num_rows();
				
				foreach($sqlDashboard as $res)
				{
					$responseID  = @$res['response_id'];
					//$this->db->select('status');
					//$this->db->order_by('status_id', 'desc');
					//$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID));
					
					
					
					// In Review Record Count
					if(@$res['status'] == 'Level 1 In Review' || @$res['status'] == 'Level 2 In Review')
					{
						$review++;
					}

					// Approved Record Count
					if(@$res['status'] == 'Level 1 Approved' || @$res['status'] == 'Level 2 Approved')
					{
						$app++;
					}
					
					// Return Record Count
					if(@$res['status'] == 'Level 1 Returned' || @$res['status'] == 'Level 2 Returned')
					{
						$ret++;
					}
				}
			}
			
			$resultRecords = $this->master_model->getRecords("district_master", array("is_deleted" => '0', "status" => 'Active'));
			//echo $this->db->last_query();
			
			$html = '<tr>
						<th style="width: 75px">Sr No.</th>
						<th>District</th>
						<th>Submitted</th>
						<th>In Review</th>
						<th>Approved</th>
						<th>Returned</th>
					</tr>';
					
			
			if(count($resultRecords) > 0)
			{
				$i = 1; //65
				$totalStatus = '';
				$approvedStatus = '';
				$returnStatus = '';
				$returnReview = '';
				foreach($resultRecords as $district_results)
				{
					$dist_id 		= $district_results['district_id'];
					$submittedCnt 	= $this->getAdminCountRecords(@$totalStatus, $dist_id);
					$totalReview 	= $this->getAdminCountRecords($returnReview, $dist_id);
					$approvedCnts 	= $this->getAdminCountRecords($approvedStatus, $dist_id);
					$returnCnts 	= $this->getAdminCountRecords($returnStatus, $dist_id);
					
					
					$html .= '<tr>
								<td>'.$i++.'</td>
								<td>'.$district_results['district_name'].' </td>
								<td>'.$submittedCnt['Submitted'].'</td>
								<td>'.$totalReview['Review'].'</td>
								<td>'.$approvedCnts['Approved'].'</td>
								<td>'.$returnCnts['Return'].'</td>
							</tr>';
					
				}
			}
			
			
			// Need Survey ID if required
			$allRecordsTotla = $this->master_model->getRecords("response_headers", array("is_deleted" => 0));			
			$allRecords = count($allRecordsTotla);
			
			// All Submitted
			/*if($totalDistrictCount > 0){
				$calculateSubmitted = ($totalDistrictCount / $allRecords)*100;
			}*/	
			
			// Total Responses Recevied
			if($totalResponsesCount > 0){
				$calculateSubmitted = ($totalResponsesCount / $allRecords)*100;
			}
			
			if($app > 0){
				$calculateApproved = ($app / $allRecords)*100;	
			}

			if($ret > 0){
				// All Return			
				$calculateReturn = ($ret / $allRecords)*100;
			}

			if($review > 0){
				// All Return			
				$calculateReview = ($review / $allRecords)*100;
			}	
		 
			
		} // end if superadmin
		else 
		{
			if($this->session->userdata('role_id') ==  $this->config->item('approver1_role_id'))
			{
				$totalStatus 	= 'Submitted';
				$approvedStatus = 'Level 1 Approved';
				$returnStatus 	= 'Level 1 Returned';
				$reviewStatus 	= 'Level 1 In Review';
				$reSubmittedStatus 	= 'Re-Submitted';
			}
			else if($this->session->userdata('role_id') == $this->config->item('approver2_role_id'))
			{
				$totalStatus 	= 'Submitted';
				$approvedStatus = 'Level 2 Approved';
				$returnStatus 	= 'Level 2 Returned';
				$reviewStatus 	= 'Level 2 In Review';
				$reSubmittedStatus 	= 'Re-Submitted';
			}
			
			
			
			$allids = array();
			
			// Assign Survey To Users 
			$this->db->select('survey_id');
			$sqlUserSurvey = $this->master_model->getRecords("assign_users", array("user_id" => $this->session->userdata('user_id'), "is_deleted" => 0));
			
			/***************** New Code Start ********************/
			
			
			if(count($sqlUserSurvey) > 0)					
			{	
				foreach($sqlUserSurvey as $s_ids)
				{ 
					$getSurveyId = $s_ids['survey_id'];	

					$surveyDetails = $this->master_model->getRecords("survey_master", array("is_deleted" => '0', "survey_id" => $getSurveyId));

					
					$this->db->select('district_id');			
					$sqlUserDistrict = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
					$userDistricts = $sqlUserDistrict[0]['district_id'];
					$explode_district = explode(",", $userDistricts);
					$joinSQL = '';					
					
					// Get logged in user assigned revenue circles
					$this->db->select('revenue_id');			
					$sqlUserRevenueCircles = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
					$userRevenueCircles = $sqlUserRevenueCircles[0]['revenue_id'];
					if($userRevenueCircles)
					{	
						$userRevenueCirclesArr = explode(",", $userRevenueCircles);
						$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
						$joinSQL .= " AND (revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
					}
					
					$sub = 0;
					$review = 0;
					$app = 0;
					$ret = 0;
					$totalDistrictCount = 0;
					$totalResponsesCount = 0;
					$totalResubmittedCount = 0;
					
					foreach($explode_district as $key => $disID)
					{
						//Total Response Received Count					
						$sqlDashboardResponse =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND survey_id = '".$getSurveyId."' "); // ".$joinSQL."
						//echo "<br />".$this->db->last_query();						
						$totalResponsesCount += $sqlDashboardResponse->num_rows();
						
						// Total Submitted Count
						$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND status='Submitted' AND survey_id = '".$getSurveyId."'");
						//echo $this->db->last_query(); die();
						$totalDistrictCount += $sqlDashboardSubmitted->num_rows();
						
						// Total ReSubmitted Count
						$sqlDashboardReSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND status='Re-Submitted' AND survey_id = '".$getSurveyId."'");
						//echo $this->db->last_query(); die();
						$totalResubmittedCount += $sqlDashboardReSubmitted->num_rows();
						
						$this->db->select('response_id, status');
						$this->db->where('survey_id', $getSurveyId);
						if($userRevenueCircles)
						{	
							$userRevenueCirclesArr = explode(",", $userRevenueCircles);
							$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
							
							$userRevenueCirclesSQL = "(revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
							$this->db->where($userRevenueCirclesSQL);
						}				
						$sqlDashboard = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $disID));
						//echo "+++".$this->db->last_query();die();
						if(count($sqlDashboard) > 0)
						{				
							foreach($sqlDashboard as $res)
							{
								$responseID  = @$res['response_id'];
								$this->db->select('status');
								$this->db->order_by('status_id', 'desc');
								$this->db->where('survey_id', $getSurveyId);
								$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID, "user_id" => $this->session->userdata('user_id')));
								
								// Approved Record Count
								if($this->session->userdata('role_id') == $this->config->item('approver1_role_id'))
								{
									
									if(@$trackResult[0]['status'] == 'Level 1 In Review')
									{
										$review++;
									}

									if(@$trackResult[0]['status'] == 'Level 1 Approved')
									{
										$app++;
									}
									
									// Return Record Count
									if(@$trackResult[0]['status'] == 'Level 1 Returned')
									{
										$ret++;
									}

								}
							
								if($this->session->userdata('role_id') == $this->config->item('approver2_role_id')){
									
									if(@$trackResult[0]['status'] == 'Level 2 In Review')
									{
										$review++;
									}

									if(@$trackResult[0]['status'] == 'Level 2 Approved')
									{
										$app++;
									}

									// Return Record Count
									if(@$trackResult[0]['status'] == 'Level 2 Returned')
									{
										$ret++;
									}

								}
							}
						}
					}
					
					
					$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$this->session->userdata('user_id')."' AND status = 'Active' AND is_deleted = '0'");
					$cntRow = $surveyDistrict->num_rows();			
					
					if($cntRow > 0)
					{ 
						$districtList = array();
						foreach($surveyDistrict->result_array() as $row)
						{
							array_push($districtList, $row['district_id']);
						}
						
					}
					
					$district_result = explode(",",$districtList[0]);
					
					$outputString = "" . implode( "','", $district_result ) . "";
					
					$this->db->select('district_id, district_name');
					$this->db->where_in('district_id', $district_result);
					$resultRecords = $this->master_model->getRecords("survey_district_master", array("is_deleted" => '0', "status" => 'Active'));
					$numRows = count($resultRecords);
					
					$html = '<tr>
								<th style="width: 75px">Sr No.</th>
								<th>District</th>
								<th>Submitted</th>
								<th>In Review</th>
								<th>Approved</th>
								<th>Returned</th>
							</tr>';
							
					if($numRows > 0)
					{
						$i = 1; //65
					
						foreach($resultRecords as $district_results)
						{
							$dist_id 			= $district_results['district_id'];
							$totalResReceived 	= $this->getCountRecords($totalStatus = 0, $dist_id, $getSurveyId);
							$submittedCnt 		= $this->getCountRecords($totalStatus, $dist_id, $getSurveyId);
							$resubmittedCnt 	= $this->getCountRecords($reSubmittedStatus, $dist_id, $getSurveyId);
							$totalReview 		= $this->getCountRecords($reviewStatus, $dist_id, $getSurveyId);
							$approvedCnts 		= $this->getCountRecords($approvedStatus, $dist_id, $getSurveyId);
							$returnCnts 		= $this->getCountRecords($returnStatus, $dist_id, $getSurveyId);
							
							$html .= '<tr>
										<td>'.$i++.'</td>
										<td>'.$district_results['district_name'].' </td>
										<td>'.$totalResReceived['total_response'].'</td>
										<td>'.$totalReview['Review'].'</td>
										<td>'.$approvedCnts['Approved'].'</td>
										<td>'.$returnCnts['Return'].'</td>
									</tr>';
						}
					}

					//echo $html;
					if($getSurveyId > 0)
					{
						// All Submitted				
						$calculateSubmitted = ($totalResponsesCount / $totalResponsesCount)*100;
						
						// All ReSubmitted
						$calculateReSubmitted = ($totalResubmittedCount / $totalResponsesCount)*100;
						
						// All Approved	
						$calculateApproved = ($app / $totalResponsesCount)*100;
						
						// All Return
						$calculateReturn = ($ret / $totalResponsesCount)*100;
						
						// All Review
						$calculateReview = ($review / $totalResponsesCount)*100;
					}
					else 
					{
						$totalResponsesCount = 0;
						$totalDistrictCount = 0;
						$totalResubmittedCount = 0;
						$app = 0;
						$ret = 0;
						$ret = 0;
						$review = 0;
						$calculateReturn = 0;
						$calculateApproved = 0;
					}
					
					$boxes .= '
						<div class="row">
							<div class="col-sm-12">
							<h3 class="title" style="text-align:center;font-size: 20px;">'.ucfirst($surveyDetails[0]['title']).'</h3>
						</div>
						</div>
						<div class="row">		  
						  <div class="col-md-3 col-6">
							<div class="small-box info-gradient">
							  <div class="inner">
								<h3>'.$totalResponsesCount.'</h3>

								<p>Total Submitted</p>
							  </div>
							  <div class="icon">
								<i class="ion ion-bag"></i>
							  </div>
							 </div>
						  </div>
						  
						  <div class="col-md-3 col-6">
							<!-- small box -->
							<div class="small-box bg-warning">
							  <div class="inner">
								<h3>'.$review.'</h3>
								<p>Total In Review</p>
							  </div>
							  <div class="icon">
								<i class="ion ion-bag"></i>
							  </div>
							  </div>
						  </div>
						  <div class="col-md-3 col-6">
						  
							<div class="small-box bg-success">
							  <div class="inner">
								<h3>'.$app.'<sup style="font-size: 20px"></sup></h3>

								<p>Total Approved</p>
							  </div>
							  <div class="icon">
								<i class="ion ion-stats-bars"></i>
							  </div>
							  </div>
						  </div>          
						  <div class="col-md-3 col-6">            
							<div class="small-box bg-danger">
							  <div class="inner">
								<h3>'.$ret.'</h3>
								<p>Total Returned</p>
							  </div>
							  <div class="icon">
								<i class="ion ion-pie-graph"></i>
							  </div>
							  </div>
						  </div>						 
						</div>
						
						<div class="row">
							<div class="col-md-6 mb-3">
							<div class="card h-100">
								  <div class="card-header border-transparent">
									
									<h3 class="card-title">Statistics</h3>

									<div class="card-tools">
									  <button type="button" class="btn btn-tool" data-card-widget="collapse">
										<i class="fas fa-minus"></i>
									  </button>
									  <button type="button" class="btn btn-tool" data-card-widget="remove">
										<i class="fas fa-times"></i>
									  </button>
									</div>
								  </div>
								  <!-- /.card-header -->
								  <div class="card-body px-3 py-0">
								  
									<div class="progress-group mb-4">
									  Total Submitted
									  <!--<span class="float-right"><b>40%</b>/100% </span>-->
									  <span class="float-right"><b>'.number_format($calculateSubmitted, 2).'%</b> </span>
									  <div class="progress progress-sm">
										<div class="progress-bar info-gradient" style="width: '.number_format($calculateSubmitted, 2).'%"></div>
									  </div>
									</div>
									<div class="progress-group mb-4">
									 Total Review
									  <span class="float-right"><b>'.number_format($calculateReview,2).'%</b></span>
									  <div class="progress progress-sm">
										<div class="progress-bar bg-warning" style="width: '.number_format($calculateReview,2).'%"></div>
									  </div>
									</div>
									<div class="progress-group mb-4">
									  Total Approved
									  <span class="float-right"><b>'.number_format($calculateApproved,2).'%</b></span>
									  <div class="progress progress-sm">
										<div class="progress-bar bg-success" style="width: '.number_format($calculateApproved,2).'%"></div>
									  </div>
									</div>
									<div class="progress-group mb-4">
									 Total Returned
									  <span class="float-right"><b>'.number_format($calculateReturn,2).'%</b></span>
									  <div class="progress progress-sm">
										<div class="progress-bar bg-danger" style="width: '.number_format($calculateReturn,2).'%"></div>
									  </div>
									</div>
									
								  </div>
								 
								</div>
							</div>
							<div class="col-md-6 mb-3">
							<div class="card h-100">
								  <div class="card-header border-transparent">
									<h3 class="card-title">District wise Statistics</h3>

									<div class="card-tools">
									  <button type="button" class="btn btn-tool" data-card-widget="collapse">
										<i class="fas fa-minus"></i>
									  </button>
									  <button type="button" class="btn btn-tool" data-card-widget="remove">
										<i class="fas fa-times"></i>
									  </button>
									</div>
								  </div>
								 
								  <div class="card-body px-3 py-0">
									<div class="table-responsive">
									  
										<table class="table table-striped">
											<tbody>							
												'.$html.'
											</tbody>
											
										</table>
									</div>
								   
								  </div>
								 
								</div>
								</div>
								</div>';
						
					
				}
				
			}
			//die();
			
			
			/***************** New Code End ********************/
			
			/*if(count($sqlUserSurvey) > 0)
			{
				foreach($sqlUserSurvey as $s_ids)
				{
					$getSurveyId = $s_ids['survey_id'];
					array_push($allids, $getSurveyId);
					
				}
			}
			
			$this->db->select('district_id');			
			$sqlUserDistrict = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
			$userDistricts = $sqlUserDistrict[0]['district_id'];
			$explode_district = explode(",", $userDistricts);
			
			
			$joinSQL = '';
			if(count($allids) == 1)
			{	
				$joinSQL = " AND survey_id IN('".$allids[0]."')";
			}
			else if(count($allids) > 1)
			{		
				$joinParameter = "" . implode( "','", $allids ) . "";
				$joinSQL = " AND survey_id IN('".$joinParameter."')";
			}
			
			// Get logged in user assigned revenue circles
			$this->db->select('revenue_id');			
			$sqlUserRevenueCircles = $this->master_model->getRecords("users", array("is_deleted" => '0', "user_id" => $this->session->userdata('user_id')));
			$userRevenueCircles = $sqlUserRevenueCircles[0]['revenue_id'];
			if($userRevenueCircles)
			{	
				$userRevenueCirclesArr = explode(",", $userRevenueCircles);
				$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
				$joinSQL .= " AND (revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
			}
			
			foreach($explode_district as $key => $disID)
			{
				//Total Response Received Count					
				$sqlDashboardResponse =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' ".$joinSQL.""); // ".$joinSQL."
				//echo $this->db->last_query(); die();
				$totalResponsesCount += $sqlDashboardResponse->num_rows();
				
				// Total Submitted Count
				$sqlDashboardSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND status='Submitted' ".$joinSQL."");
				//echo $this->db->last_query(); die();
				$totalDistrictCount += $sqlDashboardSubmitted->num_rows();
				
				// Total ReSubmitted Count
				$sqlDashboardReSubmitted =  $this->db->query("SELECT status FROM survey_response_headers WHERE is_deleted = '0' AND district_id = '".$disID."' AND status='Re-Submitted' ".$joinSQL."");
				//echo $this->db->last_query(); die();
				$totalResubmittedCount += $sqlDashboardReSubmitted->num_rows();
				
				$this->db->select('response_id, status');
				if(count($allids) > 0)
				{
					$this->db->where_in('survey_id', $allids);
				}
				if($userRevenueCircles)
				{	
					$userRevenueCirclesArr = explode(",", $userRevenueCircles);
					$userRevenueCirclesList = implode("','", $userRevenueCirclesArr);
					
					$userRevenueCirclesSQL = "(revenue_id IN('".$userRevenueCirclesList."') OR revenue_id = 0)";
					$this->db->where($userRevenueCirclesSQL);
				}				
				$sqlDashboard = $this->master_model->getRecords("response_headers", array("is_deleted" => '0', "district_id" => $disID));
				//echo "+++".$this->db->last_query();die();
				if(count($sqlDashboard) > 0)
				{				
					foreach($sqlDashboard as $res)
					{
						$responseID  = @$res['response_id'];
						$this->db->select('status');
						$this->db->order_by('status_id', 'desc');
						if(count($allids) > 0)
						{
							$this->db->where_in('survey_id', $allids);
						}
						$trackResult = $this->master_model->getRecords("response_status", array("response_id" => $responseID, "user_id" => $this->session->userdata('user_id')));
						
						// Approved Record Count
						if($this->session->userdata('role_id') == $this->config->item('approver1_role_id'))
						{
							
							if(@$trackResult[0]['status'] == 'Level 1 In Review')
							{
								$review++;
							}

							if(@$trackResult[0]['status'] == 'Level 1 Approved')
							{
								$app++;
							}
							
							// Return Record Count
							if(@$trackResult[0]['status'] == 'Level 1 Returned')
							{
								$ret++;
							}

						}
					
						if($this->session->userdata('role_id') == $this->config->item('approver2_role_id')){
							
							if(@$trackResult[0]['status'] == 'Level 2 In Review')
							{
								$review++;
							}

							if(@$trackResult[0]['status'] == 'Level 2 Approved')
							{
								$app++;
							}

							// Return Record Count
							if(@$trackResult[0]['status'] == 'Level 2 Returned')
							{
								$ret++;
							}

						}
					}
				}
			}

			
			$surveyDistrict = $this->db->query("SELECT district_id FROM survey_users WHERE user_id = '".$this->session->userdata('user_id')."' AND status = 'Active' AND is_deleted = '0'");
			$cntRow = $surveyDistrict->num_rows();			
			
			if($cntRow > 0)
			{ 
				$districtList = array();
				foreach($surveyDistrict->result_array() as $row)
				{
					array_push($districtList, $row['district_id']);
				}
				
			}
			
			$district_result = explode(",",$districtList[0]);
			
			$outputString = "" . implode( "','", $district_result ) . "";
			
			$this->db->select('district_id, district_name');
			$this->db->where_in('district_id', $district_result);
			$resultRecords = $this->master_model->getRecords("survey_district_master", array("is_deleted" => '0', "status" => 'Active'));
			$numRows = count($resultRecords);
			
			$html = '<tr>
						<th style="width: 75px">Sr No.</th>
						<th>District</th>
						<th>Submitted</th>
						<th>In Review</th>
						<th>Approved</th>
						<th>Returned</th>
					</tr>';
					
			if($numRows > 0)
			{
				$i = 1; //65
			
				foreach($resultRecords as $district_results)
				{
					$dist_id 			= $district_results['district_id'];
					$totalResReceived 	= $this->getCountRecords($totalStatus = 0, $dist_id);
					$submittedCnt 		= $this->getCountRecords($totalStatus, $dist_id);
					$resubmittedCnt 	= $this->getCountRecords($reSubmittedStatus, $dist_id);
					$totalReview 		= $this->getCountRecords($reviewStatus, $dist_id);
					$approvedCnts 		= $this->getCountRecords($approvedStatus, $dist_id);
					$returnCnts 		= $this->getCountRecords($returnStatus, $dist_id);
					
					//<td>'.$submittedCnt['Submitted'].'</td><td>'.$resubmittedCnt['Resubmitted'].'</td>
					$html .= '<tr>
								<td>'.$i++.'</td>
								<td>'.$district_results['district_name'].' </td>
								<td>'.$totalResReceived['total_response'].'</td>
								<td>'.$totalReview['Review'].'</td>
								<td>'.$approvedCnts['Approved'].'</td>
								<td>'.$returnCnts['Return'].'</td>
							</tr>';
				}
			}
			
			
			// Need Survey ID if required
			$allRecordsTotla = $this->master_model->getRecords("response_headers", array("is_deleted" => 0));			
			$allRecords = count($allRecordsTotla);

			if(count($allids) > 0)
			{
				// All Submitted
				//$calculateSubmitted = ($totalDistrictCount / $totalResponsesCount)*100;
				$calculateSubmitted = ($totalResponsesCount / $totalResponsesCount)*100;
				
				// All ReSubmitted
				$calculateReSubmitted = ($totalResubmittedCount / $totalResponsesCount)*100;
				
				// All Approved	
				$calculateApproved = ($app / $totalResponsesCount)*100;
				
				// All Return
				$calculateReturn = ($ret / $totalResponsesCount)*100;
				
				// All Review
				$calculateReview = ($review / $totalResponsesCount)*100;
			}
			else 
			{
				$totalResponsesCount = 0;
				$totalDistrictCount = 0;
				$totalResubmittedCount = 0;
				$app = 0;
				$ret = 0;
				$ret = 0;
				$review = 0;
				$calculateReturn = 0;
				$calculateApproved = 0;
			}*/
						
		} // Else End
		
		$html = '';
		$data=array('middle_content' => 'dashboard/index1','page_title'=>"Dashboard", 'total_response' => @$totalResponsesCount, 'submitted' => @$totalDistrictCount, 'resubmitted' => @$totalResubmittedCount, 'approved' => @$app, 'return' => @$ret, "response_html" => $html, "total_survey" => number_format($calculateSubmitted, 2), "total_approved" => number_format($calculateApproved,2), "total_return" => number_format($calculateReturn,2), "total_review" => number_format($calculateReview,2));
		
		$data['total_response'] = @$totalResponsesCount;
		$data['submitted'] 		= @$totalDistrictCount;
		$data['resubmitted'] 	= @$totalResubmittedCount;
		$data['module_name'] 	= 'Dashboard';
		$data['review'] 		= @$review;
		$data['approved'] 		= @$app;
		$data['return'] 		= @$ret;
		$data['box']			= @$boxes;
		$data['submodule_name'] = '';		
		$this->load->view('admin/admin_combo',$data);
	}
	
	public function change_password()
	{
		//$encrptopenssl =  New Opensslencryptdecrypt();
		if($this->session->userdata('role_id') == $this->config->item('system_admin_id')){
			redirect('xAdmin');
		}
	 	$this->form_validation->set_rules('old_password', 'Old Password', 'trim|required');
		$this->form_validation->set_rules('new_password', 'New Password','trim|required|min_length[8]|matches[crm_password]');
		$this->form_validation->set_rules('crm_password', 'Confirm Password', 'trim|required|min_length[3]');
		
		if ($this->form_validation->run())
		{
			
			$old_password 	= $this->input->post('old_password');
			$user_password 	= $this->input->post('new_password');	
			$con_password 	= md5($this->input->post('crm_password'));
			$where = array("username" => $this->session->userdata('username'), "password" => md5($old_password));
			
			$user_all_details = $this->master_model->getRecords("users",$where);
			//echo $this->db->last_query();die();
			if(md5($user_password) == md5($old_password))
			{	
				$this->session->set_flashdata('error','No change in password.');
				redirect('xAdmin/admin/change_password');		
			}
			else
			{  
			
				if(!empty($user_all_details) && md5($old_password) == $user_all_details[0]['password'])
				{
					if(md5($user_password) == $con_password)
					{
							$update_array= array("password"=> md5($user_password));							
							if($admin_details = $this->master_model->updateRecord('users',$update_array,
							array('user_id'=>"'".$this->session->userdata('user_id')."'")))
							{ 
								
								$this->session->set_flashdata('success','Password has been reset successfully');
							  	redirect('xAdmin/admin/change_password');	
							}
							else
							{ 
								$this->session->set_flashdata('error','Error while resetting password.');
								redirect('xAdmin/admin');	
							}
					}
					else
					{	
						$this->session->set_flashdata('error','Wrong Password Confirmation');
						redirect('xAdmin/admin/change_password');	
					}
				}
				else
				{  
					$this->session->set_flashdata('error','Incorrect old password');
					redirect('xAdmin/admin/change_password');	
				}
			}
		}
		
		$data=array('middle_content' => 'change_password');
		$data['module_name'] = 'Change_Password';
		$data['submodule_name'] = '';
		$this->load->view('admin/admin_combo',$data);
	
		//$this->load->view('admin/change_password');
	}
	
	//contact enquiries added from front panel show and details
	public function contact_enq()
	{
		$contact_info=$this->master_model->getRecords("tbl_contact_enq");
		$data = array('middle_content'=>'contact_view','contact_info'=>$contact_info);
		$this->load->view('admin/admin_combo',$data);
	}
	
	//admin logout
	public function logout()
	{
		unset($_SESSION);
		session_destroy();
		redirect(base_url('xAdmin/admin'));	 
	}
}
