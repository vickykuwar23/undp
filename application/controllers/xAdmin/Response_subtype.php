<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Response_subtype
Author : Vicky K
*/

class Response_subtype extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		
		//$this->db->where('survey_response_validation_subtype_master.is_deleted','0');
		// Permission Set Up
		$this->master_model->permission_access('14', 'view');
		
		$this->db->select("survey_response_validation_subtype_master.validation_sb_type,survey_response_validation_master.validation_type,survey_response_validation_subtype_master.sub_validation_id,survey_response_validation_subtype_master.status,survey_response_validation_subtype_master.is_deleted");
		$this->db->join('survey_response_validation_master','survey_response_validation_subtype_master.validation_id=survey_response_validation_master.validation_id','left',FALSE);
		$response_data = $this->master_model->getRecords("response_validation_subtype_master",array('survey_response_validation_subtype_master.is_deleted'=>'0'),'',array('sub_validation_id'=>'DESC'));	
		
		$data['records'] = $response_data;		
		$data['module_name'] = 'Survey';
		$data['submodule_name'] = 'Response_subtype';	
    	$data['middle_content']='response-sub-type/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add block
    public function add()
    {
		
		// Permission Set Up
		$this->master_model->permission_access('14', 'add');
		
		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		$responseType_data = $this->master_model->getRecords("response_validation_master",array('is_deleted'=>'0'),'',array('display_order'=>'ASC'));
		$state_data = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC'));
        // Check Validation
		$this->form_validation->set_rules('validation_id', 'Input Category', 'required');	
		$this->form_validation->set_rules('name', 'Respone Sub Type Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			
			$validation_id = $this->input->post('validation_id');
			$validation_sb_type = trim($this->input->post('name'));			
			$insertArr = array( 'validation_id' => $validation_id, 'validation_sb_type' => $validation_sb_type, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('response_validation_subtype_master',$insertArr);
			
			if($insertQuery > 0){
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Response Sub Type Validation',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				$this->session->set_flashdata('success','Record successfully created');
				redirect(base_url('xAdmin/response_subtype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/response_subtype/add'));
			}
		}
		$data['responseList'] = $responseType_data;
		$data['stateList']    = $state_data;
		$data['module_name']  = 'Survey';
		$data['submodule_name'] = 'Response_subtype';	
    	$data['middle_content']='response-sub-type/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit block 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('14', 'edit');
		
		$response_data = $this->master_model->getRecords("response_validation_master",array('is_deleted'=>'0'),'',array('display_order'=>'ASC'));	
		// Get Log Setting
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		
		$sub_validation_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("response_validation_subtype_master",array('sub_validation_id'=>$sub_validation_id));
		$this->form_validation->set_rules('validation_id', 'Input Category', 'required');	
		$this->form_validation->set_rules('name', 'Response Sub Type Name', 'required|min_length[3]|xss_clean');	
		
		if($this->form_validation->run())
		{	
		
			$validation_id = $this->input->post('validation_id');
			$validation_sb_type = trim($this->input->post('name'));
			
			$updateArr = array('validation_id' => $validation_id, 'validation_sb_type' => $validation_sb_type, 'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('response_validation_subtype_master',$updateArr,array('sub_validation_id' => $sub_validation_id));
			if($updateQuery > 0){
				
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Response Sub Type Validation',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Record successfully updated');
				redirect(base_url('xAdmin/response_subtype'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/response_subtype/edit/'.$id));
			}
		}
		
		$data['responseList'] 	= $response_data;
		$data['res_record']= 	$rec;
		$data['module_name'] 	= 	'Survey';
		$data['submodule_name'] = 	'Response_subtype';
        $data['middle_content']	=	'response-sub-type/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update block Status
	 public function changeStatus(){
		
		// Permission Set Up
		$this->master_model->permission_access('14', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			
			$updateQuery = $this->master_model->updateRecord('response_validation_subtype_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('sub_validation_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added
			$postArr			= array('id' => $id, 'status' => $value);
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Response Sub Type Validation',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/response_subtype'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('response_validation_subtype_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('sub_validation_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Response Sub Type Validation',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/response_subtype'));		
		 }
		 
	 }
	 
	 // Soft Delete block
	 public function delete($id){
		 
		// Permission Set Up
		$this->master_model->permission_access('14', 'delete');		
		 $id 	= base64_decode($id);		 
		 $updateQuery = $this->master_model->updateRecord('response_validation_subtype_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('sub_validation_id' => $id));
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Response Sub Type Validation',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Response Sub Type successfully deleted');
		 redirect(base_url('xAdmin/response_subtype'));	
		 
	 }


}