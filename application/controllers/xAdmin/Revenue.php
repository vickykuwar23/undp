<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Revenue
Author : Vicky K
*/

class Revenue extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {
		 // Permission Set Up
		 $this->master_model->permission_access('11', 'view');
    	$this->db->where('survey_revenue_master.is_deleted','0');
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		
		
		$this->db->select('survey_district_master.district_name, survey_revenue_master.revenue_name, survey_district_master.district_id, survey_revenue_master.status,  survey_revenue_master.revenue_code, survey_revenue_master.revenue_id');
		$this->db->join('survey_district_master','survey_revenue_master.district_id=survey_district_master.district_id','left',FALSE);
		$response_data = $this->master_model->getRecords("revenue_master",'','',array('revenue_id'=>'DESC'));	
		//echo $this->db->last_query();die();
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Revenue';	
    	$data['middle_content']='revenue/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add state
    public function add()
    {
		// Permission Set Up
		$this->master_model->permission_access('11', 'add');
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		$response_data = $this->master_model->getRecords("district_master",'','',array('district_name'=>'ASC'));	
        // Check Validation
		$this->form_validation->set_rules('district_id', 'District Name', 'required');
		$this->form_validation->set_rules('revenue_code', 'Revenue Code', 'required');		
		$this->form_validation->set_rules('name', 'Revenue Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			$district_id 	= $this->input->post('district_id');
			$revenue_code 	= trim($this->input->post('revenue_code'));
			$revenue_name 	= trim($this->input->post('name'));	

			$insertArr = array( 'district_id' => $district_id, 
								'revenue_code' => $revenue_code, 
								'revenue_name' => $revenue_name,
								'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('revenue_master',$insertArr);
			
			if($insertQuery > 0){
				//echo ">>>>>";die();
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Revenue Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Revenue successfully created');
				redirect(base_url('xAdmin/revenue'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/revenue/add'));
			}
		}
		$data['districtList'] = $response_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Revenue';	
    	$data['middle_content']='revenue/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit state 
	 public function edit($id)
    {
		 // Permission Set Up
		$this->master_model->permission_access('11', 'edit');
		
		$revenue_id = base64_decode($id);	
		$rec = $this->master_model->getRecords("revenue_master",array('revenue_id'=>$revenue_id));
		
		$this->form_validation->set_rules('district_id', 'District Name', 'required');
		$this->form_validation->set_rules('revenue_code', 'Revenue Code', 'required');		
		$this->form_validation->set_rules('name', 'Revenue Name', 'required|min_length[3]|xss_clean');
		$this->db->where('survey_district_master.status','Active'); 
		$this->db->where('survey_district_master.is_deleted','0'); 
		$response_data = $this->master_model->getRecords("district_master",'','',array('district_name'=>'ASC'));
		if($this->form_validation->run())
		{	
		
			$district_id 	= $this->input->post('district_id');
			$revenue_code 	= trim($this->input->post('revenue_code'));
			$revenue_name 	= trim($this->input->post('name'));
			
			$updateArr = array( 'district_id' => $district_id, 
								'revenue_code' => $revenue_code, 
								'revenue_name' => $revenue_name,
								'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('revenue_master',$updateArr,array('revenue_id' => $revenue_id));
			//echo $this->db->last_query();die();
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Revenue Master',
								'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}

				$this->session->set_flashdata('success','Revenue successfully updated');
				redirect(base_url('xAdmin/revenue'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/revenue/edit/'.$id));
			}
		}
		
		$data['districtList'] = $response_data;
		$data['revenue_record'] 	= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Revenue';
        $data['middle_content']	=	'revenue/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update state Status
	 public function changeStatus(){
		 
		  // Permission Set Up
		$this->master_model->permission_access('11', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			$updateQuery = $this->master_model->updateRecord('revenue_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('revenue_id' => $id));
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Revenue Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/revenue'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('revenue_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('revenue_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Revenue Master',
							'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/revenue'));		
		 }
		 
	 }
	 
	 // Soft Delete state
	 public function delete($id){
		 
		// Permission Set Up
		$this->master_model->permission_access('11', 'delete');
		$id 	= base64_decode($id);		 
		$updateQuery = $this->master_model->updateRecord('revenue_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('revenue_id' => $id));
		// Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Revenue Master',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Revenue successfully deleted');
		 redirect(base_url('xAdmin/revenue'));	
		 
	 }


}