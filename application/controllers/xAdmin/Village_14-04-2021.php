<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Village
Author : Vicky K
*/

class Village extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		ini_set('memory_limit', '-1');
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// village Listing
    public function index()
    {	
		// Permission Set Up
		$this->master_model->permission_access('6', 'view');
		$this->db->where('survey_village_master.is_deleted','0');
		$this->db->where('survey_revenue_master.status','Active'); 
		$this->db->where('survey_revenue_master.is_deleted','0');
		
		$this->db->select('survey_revenue_master.revenue_name, survey_village_master.village_code, survey_village_master.village_name, survey_village_master.status, survey_village_master.village_id');
		$this->db->join('survey_revenue_master','survey_village_master.revenue_id=survey_revenue_master.revenue_id','left',FALSE);
		$response_data = $this->master_model->getRecords("village_master",'','',array('village_id'=>'DESC'));	
		//echo $this->db->last_query();die();
		//echo "<pre>";print_r($response_data);die();
		$data['records'] = $response_data;		
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Village';	
    	$data['middle_content']='village/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add village
    public function add()
    {
		// Permission Set Up
		$this->master_model->permission_access('6', 'add');
		$this->db->where('survey_revenue_master.is_deleted','0');
		$this->db->where('survey_revenue_master.status','Active');
		$response_data = $this->master_model->getRecords("revenue_master",'','',array('revenue_name'=>'ASC'));	
        // Check Validation
		$this->form_validation->set_rules('revenue_id', 'Revenue Name', 'required');
		$this->form_validation->set_rules('village_code', 'Village Code', 'required');	
		$this->form_validation->set_rules('name', 'Village Name', 'required|min_length[3]|xss_clean');	
		if($this->form_validation->run())
		{	
			$revenue_id = $this->input->post('revenue_id');
			$village_code = $this->input->post('village_code');
			$village_name = trim($this->input->post('name'));			
			$insertArr = array( 'revenue_id' => $revenue_id, 'village_code' => $village_code, 'village_name' => $village_name, 'created_by_id' => $this->session->userdata('user_id'));			
			$insertQuery = $this->master_model->insertRecord('village_master',$insertArr);
			//echo $this->db->last_query();
			if($insertQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'Village','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','Village successfully created');
				redirect(base_url('xAdmin/village'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/village/add'));
			}
		}
		$data['revenueList'] = $response_data;
		$data['module_name'] = 'Master';
		$data['submodule_name'] = 'Village';	
    	$data['middle_content']='village/add';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 // Edit block 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('6', 'edit');
		$this->db->where('survey_revenue_master.is_deleted','0');
		$this->db->where('survey_revenue_master.status','Active');
		$response_data = $this->master_model->getRecords("revenue_master",'','',array('revenue_name'=>'ASC'));			
		$village_id = base64_decode($id);
		$data['edit_id'] = base64_decode($id);	
		$rec = $this->master_model->getRecords("village_master",array('village_id'=>$village_id));
		//echo $this->db->last_query();
		$this->form_validation->set_rules('revenue_id', 'Revenue Name', 'required');
		$this->form_validation->set_rules('village_code', 'Village Code', 'required');	
		$this->form_validation->set_rules('name', 'Village Name', 'required|min_length[3]|xss_clean');	
		
		if($this->form_validation->run())
		{	
		
			$revenue_id = $this->input->post('revenue_id');
			$village_name = trim($this->input->post('name'));
			$village_code = $this->input->post('village_code');
			
			$updateArr = array('revenue_id' => $revenue_id, 'village_code' => $village_code, 'village_name' => $village_name, 'updated_by_id' => $this->session->userdata('user_id'));	
			
			$updateQuery = $this->master_model->updateRecord('village_master',$updateArr,array('village_id' => $village_id));
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'Village','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				//echo ">>>";die();
				$this->session->set_flashdata('success','Village successfully updated');
				redirect(base_url('xAdmin/village'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/village/edit/'.$id));
			}
		}
		
		$data['revenueList'] 	= $response_data;
		$data['village_record']= 	$rec;
		$data['module_name'] 	= 	'Master';
		$data['submodule_name'] = 	'Village';
        $data['middle_content']	=	'village/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	  // Update village Status
	 public function changeStatus(){
		 
		 // Permission Set Up
		$this->master_model->permission_access('6', 'status');
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		
		 if($value == 'Active'){
			
			 $updateQuery = $this->master_model->updateRecord('village_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('village_id' => $id));
			 // Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Village','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/village'));	
			 
		 } else if($value == 'Inactive'){
			
			$updateQuery = $this->master_model->updateRecord('village_master',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('village_id' => $id)); 
			
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'Village','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/village'));		
		 }
		 
	 }
	 
	 // Soft Delete block
	 public function delete($id){
		 
		 // Permission Set Up
		$this->master_model->permission_access('6', 'delete');
		 $id 	= base64_decode($this->uri->segment(4));		 
		 $updateQuery = $this->master_model->updateRecord('village_master',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('village_id' => $id));
		   // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'Village',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','Village Successfully deleted');
		 redirect(base_url('xAdmin/village'));	
		 
	 }
	 
	 
	public function unique_name_ajax() 
    {        
		
		$name			=	$_POST['name'];
		$village_code	=	$_POST['village_code'];
		$revenue_id		=	$_POST['revenue_id'];
		$edit_id		=	$_POST['edit_id'];
		if($edit_id > 0)
		{
			$village_data   = $this->master_model->getRecords('village_master',array('village_code'=>$village_code,'revenue_id'=>$revenue_id,'village_name'=>$name,'is_deleted'=>'0', 'village_id !=' => $edit_id));
		}
		else
		{
			$village_data   = $this->master_model->getRecords('village_master',array('village_code'=>$village_code,'revenue_id'=>$revenue_id,'village_name'=>$name,'is_deleted'=>'0'));
		}
		
		if (count($village_data)>0) {
			echo "false";
		}else{
			echo "true";
		}        
    }
	
	public function unique_village_ajax() 
    { 
		$village_code	=	$_POST['village_code'];
		$revenue_id		=	$_POST['revenue_id'];
		$edit_id		=	$_POST['edit_id'];
		$village_data   = $this->master_model->getRecords('village_master',array('village_code'=>$village_code,'revenue_id'=>$revenue_id,'is_deleted'=>'0'));
		
		if($edit_id > 0)
		{			
			$village_data   = $this->master_model->getRecords('village_master',array('village_code'=>$village_code,'revenue_id'=>$revenue_id,'is_deleted'=>'0', 'village_id !=' => $edit_id));
		}
		else
		{
			$village_data   = $this->master_model->getRecords('village_master',array('village_code'=>$village_code,'revenue_id'=>$revenue_id,'is_deleted'=>'0'));
		}
		
		if (count($village_data)>0) {
			echo "false";
		}else{
			echo "true";
		}        
    }
	
}