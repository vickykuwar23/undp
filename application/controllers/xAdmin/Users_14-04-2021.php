<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Users
Author : Vicky K
*/

class Users extends CI_Controller 
{
	function __construct() {
        parent::__construct();
		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
    }

	// Role Listing
    public function index()
    {	
    	
		// Permission Set Up
		$this->master_model->permission_access('1', 'view');
		$this->db->where('survey_users.is_deleted','0');
		$this->db->select("survey_role_master.role_id,survey_role_master.role_name,survey_users.user_id,survey_users.username,survey_users.first_name,survey_users.last_name,survey_users.email,survey_users.status, survey_users.is_login");
		$this->db->join("survey_role_master",'survey_users.role_id=survey_role_master.role_id','INNER', FALSE);
		$response_data = $this->master_model->getRecords("users",array('survey_users.user_id !='=>'1', 'survey_users.is_deleted' => '0'),'',array('survey_users.user_id'=>'DESC'));		
		$data['records'] 		= $response_data;		
		$data['module_name'] 	= 'User';
		$data['submodule_name'] = '';	
    	$data['middle_content']	= 'users/index';
		$this->load->view('admin/admin_combo',$data);
   	 }

	 // Add Role
    public function add()
    {
		// Permission Set Up
		$this->master_model->permission_access('1', 'add');
		
		// Get Master Data 
		$data['role_list'] = $this->master_model->getRecords("role_master",'','',array('role_id'=>'ASC'));
		$data['state_list'] = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC')); //array('country_id'=>'101')
		//$data['brance_list'] = $this->master_model->getRecords("branch_office",'','',array('branch_id'=>'ASC'));
		$data['district_list'] = $this->master_model->getRecords("district_master",'','',array('district_id'=>'ASC'));
		//$data['survey_list'] = $this->master_model->getRecords("survey_list",'','',array('survey_id'=>'ASC'));
		
        // Check Validation
		$this->form_validation->set_rules('role_id', 'Role Name', 'required|xss_clean');		
		$this->form_validation->set_rules('first_name', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|xss_clean');
		$this->form_validation->set_rules('email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('contact_no', 'Contact No', 'required|min_length[10]|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|xss_clean');
		$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[4]|xss_clean');
		
		
		if($this->form_validation->run())
		{	
			
			// District Array
			$district_id 	= $this->input->post('district_id');
			if(count($district_id) > 0){
				$districtArr		= implode(",",$district_id);
			}

			// District Array
			$revenue_id 	= $this->input->post('revenue_id');
			if(count($revenue_id) > 0){
				$revenueArr		= implode(",",$revenue_id);
			}

			/*// Survey Array
			$survey_id 	= $this->input->post('survey_id');
			if(count($survey_id) > 0){
				$surveyArr		= implode(",",$survey_id);
			}

						
			// State Array
			$state_id 		= $this->input->post('state_id');
			if(count($state_id) > 0){
				$stateArr		= implode(",",$state_id);
			}
			
			
			// Block Array
			$block_id 		= $this->input->post('block_id');
			if(count($block_id) > 0){
				$blockArr		= implode(",",$block_id);
			}
			
			// Village Array
			$village_id 	= $this->input->post('village_id');
			if(count($village_id) > 0){
				$villageArr		= implode(",",$village_id);
			}*/
			
			
			$role_id 		= $this->input->post('role_id');			
			/*if($role_id == 1 || $role_id == 2){				
				//$stateArr 		= "";
				$districtArr 	= "";
				$revenueArr 	= "";
				//$blockArr 		= "";
				//$villageArr 	= "";
			} else if($role_id == 3){
				//$blockArr 	= "";
				//$villageArr 	= "";
				$revenueArr 	= "";
			} else if($role_id == 4){				
				//$villageArr 	= "";
				$revenueArr 	= "";
			}*/
			
			$first_name 	= trim($this->input->post('first_name'));
			$last_name 		= trim($this->input->post('last_name'));
			$email_id 		= $this->input->post('email_id');
			$contact_no 	= trim($this->input->post('contact_no'));
			$username 		= $this->input->post('username');
			$pass_word 		= trim($this->input->post('pwd'));
			$pwd 			= md5($this->input->post('pwd'));
			$fullname		= ucwords($first_name)." ".ucwords($last_name);
			$insertArr = array( 'state_id' 		=> '1',								
								'district_id' 	=> $districtArr, 
								'revenue_id' 	=> $revenueArr, 
								//'survey_id' 	=> $surveyArr, 
								//'block_id' 	=> $blockArr, 
								//'village_id' 	=> $villageArr, 
								'branch_id' 	=> '0', 
								'username' 		=> $username, 
								'password' 		=> $pwd, 
								'first_name'	=> $first_name, 
								'last_name'		=> $last_name,
								'contact_no'	=> $contact_no,
								'email' 		=> $email_id, 
								'role_id' 		=> $role_id, 
								'created_by_id' => $this->session->userdata('user_id'));
								
			$insertQuery = $this->master_model->insertRecord('users',$insertArr);

			//echo $this->db->last_query();	
			if($insertQuery > 0)
			{
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Add",'module_name'=> 'User','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes')
				{
					$this->master_model->insertRecord('logs',$logData);
				}
				$arrayDetails = array("fullname" => $fullname, "username" => $username, "password" => $pass_word, "email_id" => $email_id);
				$this->send_email_to_register_user($arrayDetails);
				//$this->send_email_to_register_user($fullname, $username, $pass_word, $email_id);
				$this->session->set_flashdata('success','User successfully created');
				redirect(base_url('xAdmin/users'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/users/add'));
			}
		}
		
		$data['module_name'] 	= 'User';
		$data['submodule_name'] = '';	
    	$data['middle_content']	= 'users/add';
        $this->load->view('admin/admin_combo',$data);
    }
	 
	 // Edit Role 
	 public function edit($id)
    {
		// Permission Set Up
		$this->master_model->permission_access('1', 'edit');
		
		$user_id = base64_decode($id);
		
		// Master Data
		$data['role_list'] = $this->master_model->getRecords("role_master",'','',array('role_id'=>'ASC'));
		$data['state_list'] = $this->master_model->getRecords("states_master",'','',array('state_name'=>'ASC')); //array('country_id'=>'101')
		//$data['brance_list'] = $this->master_model->getRecords("branch_office",'','',array('branch_id'=>'ASC'));
		$data['district_list'] = $this->master_model->getRecords("district_master",'','',array('district_id'=>'ASC'));
		//$data['survey_list'] = $this->master_model->getRecords("survey_list",'','',array('survey_id'=>'ASC'));
		
		// Particular Record
		$this->db->join("survey_role_master",'survey_users.role_id=survey_role_master.role_id','INNER', FALSE);	
		$rec = $this->master_model->getRecords("users",array('user_id'=>$user_id));
		
		$this->form_validation->set_rules('role_id', 'Role Name', 'required|xss_clean');		
		$this->form_validation->set_rules('first_name', 'First Name', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required|min_length[3]|xss_clean');
		$this->form_validation->set_rules('email_id', 'Email ID', 'required|xss_clean');
		$this->form_validation->set_rules('contact_no', 'Contact No', 'required|xss_clean');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[3]|xss_clean');
		//$this->form_validation->set_rules('pwd', 'Password', 'required|min_length[4]|xss_clean');
		
		if($this->form_validation->run())
		{		

			// District Array
			$district_id 	= $this->input->post('district_id');
			if(count($district_id) > 0){
				$districtArr		= implode(",",$district_id);
			}


			// Revenue Array
			$revenue_id 	= $this->input->post('revenue_id');
			if(count($revenue_id) > 0){
				$revenueArr		= implode(",",$revenue_id);
			}

			// District Array
			/*$survey_id 	= $this->input->post('survey_id');
			if(count($survey_id) > 0){
				$surveyArr		= implode(",",$survey_id);
			}

			// State Array
			$state_id 		= $this->input->post('state_id');
			if(count($state_id) > 0){
				$stateArr		= implode(",",$state_id);
			}

			// Block Array
			$block_id 		= $this->input->post('block_id');
			if(count($block_id) > 0){
				$blockArr		= implode(",",$block_id);
			}
			
			// Village Array
			$village_id 	= $this->input->post('village_id');
			if(count($village_id) > 0){
				$villageArr		= implode(",",$village_id);
			}*/
			//print_r($revenueArr);die();
			
			$role_id 		= $this->input->post('role_id');			
			if($role_id == 1 || $role_id == 2){				
				//$stateArr 		= "";
				$districtArr 	= "";
				$revenueArr 	= "";
				//$blockArr 		= "";
				//$villageArr 	= "";
			} else if($role_id == 3){
				//$blockArr 	= "";
				//$villageArr 	= "";
				//$revenueArr 	= "";
			} 
			
			
			$first_name 	= trim($this->input->post('first_name'));
			$last_name 		= trim($this->input->post('last_name'));
			$email_id 		= $this->input->post('email_id');
			$contact_no 	= trim($this->input->post('contact_no'));
			$username 		= $this->input->post('username');
			
			if($this->input->post('pwd')!=""){
				
				$pwd 	= md5($this->input->post('pwd'));
				
				$updateArr = array( 'state_id' 		=> '1', 
									'district_id' 	=> $districtArr, 
									'revenue_id' 	=> $revenueArr, 
									//'block_id' 	=> $blockArr, 
									//'village_id' 	=> $villageArr, 
									//'survey_id' 	=> $surveyArr, 
									'branch_id' 	=> '0', 
									'username' 		=> $username, 
									'password' 		=> $pwd, 
									'first_name'	=> $first_name, 
									'last_name'		=> $last_name,
									'contact_no'	=> $contact_no,
									'email' 		=> $email_id, 
									'role_id' 		=> $role_id,
									'updated_by_id' => $this->session->userdata('user_id'));	
				
			} else {
				
				$updateArr = array( //'state_id' 		=> $stateArr, 
									'district_id' 	=> $districtArr, 
									'revenue_id' 	=> $revenueArr, 
									//'block_id' 	=> $blockArr, 
									//'village_id' 	=> $villageArr, 
									//'survey_id' 	=> $surveyArr, 
									'branch_id' 	=> '0', 
									'username' 		=> $username, 
									'first_name'	=> $first_name, 
									'last_name'		=> $last_name,
									'contact_no'	=> $contact_no,
									'email' 		=> $email_id, 
									'role_id' 		=> $role_id,
									'updated_by_id' => $this->session->userdata('user_id'));	
			}
						
			$updateQuery = $this->master_model->updateRecord('users',$updateArr,array('user_id' => $user_id));
			
			if($updateQuery > 0){
				
				// Get Log Setting
				$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
				$ipAddr		= $this->master_model->get_client_ip();
				// Log Data Added
				$postArr			= $this->input->post();	
				$json_encode_data 	= json_encode($postArr);
				$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Edit",'module_name'=> 'User','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
				if($logCapture[0]['log_detect'] == 'Yes'){
					$this->master_model->insertRecord('logs',$logData);
				}
				
				$this->session->set_flashdata('success','User successfully updated');
				redirect(base_url('xAdmin/users'));
			} else {
				$this->session->set_flashdata('error','Something went wrong! Please try again.');
				redirect(base_url('xAdmin/users/edit/'.$id));
			}
		}
		
		$data['edit_record'] 	= 	$rec;
		$data['module_name'] 	= 	'User';
		$data['submodule_name'] = 	'';
        $data['middle_content']	=	'users/edit';
        $this->load->view('admin/admin_combo',$data);
     }
	 
	 
	// Check Username Already Exist
	public function exists_username(){
		$username = $this->input->post('username');
        $user = $this->master_model->getRecords('users',array('username'=>$username));			
		if(count($user)>0) {
			echo "false";
		}else{
			echo "true";
		}
	} 

	// Check Email Already Exist
	public function exists_email(){		
		$email = $this->input->post('email_id');
        $user = $this->master_model->getRecords('users',array('email' => $email));		
		if(count($user)>0) {
			echo "false";
		}else{
			echo "true";
		}
	} 	
	 
	// Edit Existing Email 
	public function edit_exists_email(){		
		$email = $this->input->post('email_id');
		$user_id = $this->input->post('id');
		//print_r($this->input->post());die();
        $user = $this->master_model->getRecords('users',array('email' => $email, 'user_id !=' => $user_id));
		//echo $this->db->last_query();	
		if(count($user)>0) {
			echo "false";
		}else{
			echo "true";
		}
	}

	public function get_revenue_list(){
		
		$csrf_test_name = $this->security->get_csrf_hash();
		$mode 			= $this->input->post('mode');
		$postArr 		= $this->input->post('district_id');
		$explode 		= explode(",", $postArr);
		$district_id 	= "'" . implode("','", $explode) . "'";	
		$sql 			= "SELECT * FROM survey_revenue_master WHERE district_id IN(".$district_id.")";
		$revenue_data 	= $this->db->query($sql)->result_array();

		//print_r($revenue_data); die;
		
		if($mode == 'edit'){
			$revenueArr = array();
			$edit_id = $this->input->post('edit_id');
			
			$getlist = $this->master_model->getRecords("users",array('user_id'=>$edit_id));			
			//echo ">>>".$getlist[0]['revenue_id'];
			$revenue_explode = explode(",",$getlist[0]['revenue_id']);			
			foreach($revenue_explode as $vid){
				array_push($revenueArr, $vid);
			}
		} else {
			
			$revenueArr = array();
		} 
		
		$options = "";
		//print_r($revenueArr);
		if(count($revenue_data) > 0){
			//$options .= '<option value="">-- Select Revenue --</option>';
			foreach($revenue_data as $revenue){
				if(in_array($revenue['revenue_id'], $revenueArr)){ $selectedDest = 'selected="selected"'; }else{ $selectedDest = '';}
				$options .= '<option value="'.$revenue['revenue_id'].'" '.$selectedDest.'>'.$revenue['revenue_name'].'</option>';
			}
		} 
		echo $options;
	}  	
	 
	 
	public function get_revenue_list_selected()
	{
		$csrf_test_name = $this->security->get_csrf_hash();
		$postArr 		= $this->input->post('district_id');
		
		$explode 		= explode(",", $postArr);
		$district_id 	= "'" . implode("','", $explode) . "'";	
		$sql 			= "SELECT * FROM survey_revenue_master WHERE district_id IN(".$district_id.")";
		$revenue_data 	= $this->db->query($sql)->result_array();
		
		$revenueArr = array();
		$edit_id = $this->input->post('edit_id');
		
		$getlist = $this->master_model->getRecords("users",array('user_id'=>$edit_id));			
		//echo ">>>".$getlist[0]['revenue_id'];
		$revenue_explode = explode(",",$getlist[0]['revenue_id']);			
		foreach($revenue_explode as $vid){
			array_push($revenueArr, $vid);
		}
		
		$options = "";
		//print_r($revenueArr);
		if(count($revenue_data) > 0){
			//$options .= '<option value="">-- Select Revenue --</option>';
			foreach($revenue_data as $revenue){
				if(in_array($revenue['revenue_id'], $revenueArr)){ $selectedDest = 'selected="selected"'; }else{ $selectedDest = '';}
				$options .= '<option value="'.$revenue['revenue_id'].'" '.$selectedDest.'>'.$revenue['revenue_name'].'</option>';
			}
		} 
		echo $options;
	}
	
	public function get_district_list(){
		
		$csrf_test_name = $this->security->get_csrf_hash();
		$postArr 		= $this->input->post('state_id');
		$mode 			= $this->input->post('mode');
		
		if($mode == 'edit'){
			$districtArr = array();
			$edit_id = $this->input->post('edit_id');
			$getlist = $this->master_model->getRecords("users",array('user_id'=>$edit_id));			
			$getlist[0]['district_id'];
			$district_explode = explode(",",$getlist[0]['district_id']);			
			foreach($district_explode as $did){
				array_push($districtArr, $did);
			}
		} else {			
			$districtArr = array();
		} 
		
		$explode = explode(",", $postArr);
		$state_id = "'" . implode("','", $explode) . "'";	
		$sql = "SELECT * FROM survey_district_master WHERE state_id IN(".$state_id.")";
		$district_data 	= $this->db->query($sql)->result_array();
		$options = "";
		if(count($district_data) > 0){
			$options .= '<option value="">-- Select District --</option>';
			foreach($district_data as $districts){			
				if(in_array($districts['district_id'], $districtArr)){ $selectedDest = 'selected="selected"'; }else{ $selectedDest = '';}				
				$options .= '<option value="'.$districts['district_id'].'" '.$selectedDest.'>'.$districts['district_name'].'</option>';
			}
		} 
		echo $options;
	} 
	
	public function get_block_list(){
		
		$csrf_test_name = $this->security->get_csrf_hash();
		$postArr = $this->input->post('district_id');
		$mode = $this->input->post('mode');
		$explode = explode(",", $postArr);
		$district_id = "'" . implode("','", $explode) . "'";	
		$sql = "SELECT * FROM survey_block_master WHERE district_id IN(".$district_id.")";
		$block_data 	= $this->db->query($sql)->result_array();
		
		if($mode == 'edit'){
			$blockArr = array();
			$edit_id = $this->input->post('edit_id');
			$getlist = $this->master_model->getRecords("users",array('user_id'=>$edit_id));			
			$getlist[0]['district_id'];
			$block_explode = explode(",",$getlist[0]['block_id']);			
			foreach($block_explode as $bid){
				array_push($blockArr, $bid);
			}
		} else {
			
			$blockArr = array();
		} 		
		
		$options = "";
		if(count($block_data) > 0){
			$options .= '<option value="">-- Select Block --</option>';
			foreach($block_data as $blockdetails){
				if(in_array($blockdetails['block_id'], $blockArr)){ $selectedDest = 'selected="selected"'; }else{ $selectedDest = '';}
				$options .= '<option value="'.$blockdetails['block_id'].'" '.$selectedDest.'>'.$blockdetails['block_name'].'</option>';
			}
		} 
		echo $options;
	}  
	
	public function get_village_list(){
		
		$csrf_test_name = $this->security->get_csrf_hash();
		$mode 			= $this->input->post('mode');
		$postArr 		= $this->input->post('block_id');
		$explode 		= explode(",", $postArr);
		$block_id 		= "'" . implode("','", $explode) . "'";	
		$sql 			= "SELECT * FROM survey_village_master WHERE block_id IN(".$block_id.")";
		$village_data 	= $this->db->query($sql)->result_array();
		
		if($mode == 'edit'){
			$villageArr = array();
			$edit_id = $this->input->post('edit_id');
			$getlist = $this->master_model->getRecords("users",array('user_id'=>$edit_id));			
			$getlist[0]['district_id'];
			$village_explode = explode(",",$getlist[0]['village_id']);			
			foreach($village_explode as $vid){
				array_push($villageArr, $vid);
			}
		} else {
			
			$villageArr = array();
		} 
		
		$options = "";
		if(count($village_data) > 0){
			$options .= '<option value="">-- Select Village --</option>';
			foreach($village_data as $villagedetails){
				if(in_array($villagedetails['village_id'], $villageArr)){ $selectedDest = 'selected="selected"'; }else{ $selectedDest = '';}
				$options .= '<option value="'.$villagedetails['village_id'].'" '.$selectedDest.'>'.$villagedetails['village_name'].'</option>';
			}
		} 
		echo $options;
	} 
	 


	  // Update Role Status
	 public function changeStatus(){
		 
		 // Permission Set Up
		 $this->master_model->permission_access('1', 'status');
		
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('users',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('user_id' => $id));
			 
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'User','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}
			 
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/users'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('users',array('status'=>$value, 'updated_by_id' => $this->session->userdata('user_id')),array('user_id' => $id));
			// Get Log Setting
			$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
			$ipAddr		= $this->master_model->get_client_ip();
			// Log Data Added			
			$postArr			= array('id' => $id, 'status' => $value);	
			$json_encode_data 	= json_encode($postArr);
			$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Status",'module_name'=> 'User','store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
			if($logCapture[0]['log_detect'] == 'Yes'){
				$this->master_model->insertRecord('logs',$logData);
			}	
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/users'));		
		 }
		 
	}
	
	// Send an Email To Register User 
	public function send_email_to_register_user($arrayDetails)
	{	
		$slug = 'user_registration';
		$email_body = $this->master_model->getRecords("email_master", array("slug" => $slug));		
		if(count($email_body) > 0){			
			//$from_admin = $email_body[0]['from_email'];
			$from_admin = $this->config->item('from_email');
			$email_content = $email_body[0]['email_body'];
			$email_subject = $email_body[0]['email_subject'];
			$url = base_url('xAdmin/admin');
			
			$arr_words 	= ['[FULLNAME]', '[USERNAME]', '[PASSWORD]', '[EMAIL]'];
			$rep_array 	= [$arrayDetails['fullname'], $arrayDetails['username'],  $arrayDetails['password'],  $arrayDetails['email_id']];		
			$content 	= str_replace($arr_words, $rep_array, $email_content);
			
					
			//email admin sending code 
			$contentArray=array(
				'to'		=>	$arrayDetails['email_id'],	//$arrayDetails['email_id']				
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$email_subject,
				'view'		=>  'common-file'
				);
			
			$other_info=array('content'=>$content);
			
			
			$emailsend 	= 	$this->emailsending->sendmail($contentArray,$other_info);
			if($emailsend){
				return true;
			}else {
				return false;
			}
		}
	} // End Function
	
	// Send an Email To Register User 
	public function send_email_to_register_user1($fullname, $username, $pass, $email)
	{	
		$slug = 'user_registration';
		$email_body = $this->master_model->getRecords("email_master", array("slug" => $slug));		
		if(count($email_body) > 0){			
			$from_admin = $email_body[0]['from_email'];
			$email_content = $email_body[0]['email_body'];
			$email_subject = $email_body[0]['email_subject'];
			$url = base_url('xAdmin/admin');
			$arr_words 	= ['[FULLNAME]', '[URL]', '[USERNAME]', '[PASSWORD]', '[EMAIL]'];
			$rep_array 	= [$fullname, $url, $username,  $pass,  $email];		
			$content 	= str_replace($arr_words, $rep_array, $email_content);
			
			/*$contentArray = array(
									'to'			=>	$email,//$email
									'to_name'		=>	$fullname,	
									'cc'			=>	'',
									'from_email'	=>	$from_admin,
									'from_name'		=>	'UNDP Survey',
									'subject'		=> 	$email_subject,
									'description' 	=> 	$content,
									'view'          => 'common-file'	
								);
			$emailsend 	= 	$this->emailsending->sendmail_phpmailer($contentArray);*/
			
			//email admin sending code 
			$contentArray=array(
				'to'		=>	$email,	//$email				
				'cc'		=>	'',
				'from'		=>	$from_admin,
				'subject'	=> 	$email_subject,
				'view'		=>  'common-file'
				);
			
			$other_info=array('content'=>$content);
			
			
			$emailsend 	= 	$this->emailsending->sendmail($contentArray,$other_info);
			if($emailsend){
				return true;
			}else {
				return false;
			}
		}
	} // End Function	
	
	public function test_mail()
	{	
		$this->load->library('email'); 
		$config['protocol'] = 'smtp';
	
		$config['smtp_host'] = 'mail.spochub.com';
		$config['smtp_port'] = '587';  //587
		$config['smtp_user'] = 'support@spochub.com';
		$config['smtp_pass'] = 'cdz1D&51';		
		$config['smtp_timeout'] = '10'; 
		$config['wordwrap'] = TRUE; 				
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'html'; 	
		$config['validation'] = TRUE; 
		//$this->load->library('email', $config);
		$this->email->initialize($config); 
					
		$this->email->from('rohit.sharmar7777@gmail.com'); 
		$this->email->to('vicky.kuwar@esds.co.in');
		$this->email->cc('vishal.phadol@esds.co.in'); 	
		$this->email->subject("Test Mail on ".date("Y-m-d H:i:s"));
		$this->email->message("Test mail on ".date("Y-m-d H:i:s"));	

		
		if(@$this->email->send()) { $final_msg = 'success'; }
		else
		{
			$final_msg = 'error. Email not send<br>';
			$final_msg .= $this->email->print_debugger();
		}
			
		echo $final_msg;
		$this->email->clear();		
	}

	
	 
	 // Soft Delete Role
	 public function delete($id){
		 
	 	$id 	= base64_decode($id);		 
		// Permission Set Up
		$this->master_model->permission_access('1', 'delete');
		 
		 //$id 	= $this->uri->segment(4);		 
		 $updateQuery = $this->master_model->updateRecord('users',array('is_deleted'=>1, 'deleted_on' => date('Y-m-d H:i:s'), 'deleted_by_id' => $this->session->userdata('user_id')),array('user_id' => $id));
		  // Log Data Added
		$logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
		$ipAddr		= $this->master_model->get_client_ip();
		$postArr			= array('delete_id' => $id);	
		$json_encode_data 	= json_encode($postArr);
		$logData = array('user_id' => $this->session->userdata('user_id'),'action_name'=> "Delete",'module_name'=> 'User',
						'store_data'=> $json_encode_data,'ip_address'=> $ipAddr);
		if($logCapture[0]['log_detect'] == 'Yes'){
			$this->master_model->insertRecord('logs',$logData);
		}
		 $this->session->set_flashdata('success','User successfully deleted');
		 redirect(base_url('xAdmin/users'));	
		 
	 }

	public function detail($user_id){

		$user_id = base64_decode($user_id);		 

	 	$user_query = $this->db->query("SELECT u1.*, concat(u1.first_name,' ',u1.last_name) full_name, (SELECT s1.state_name FROM survey_states_master s1 WHERE u1.state_id = s1.state_id) state_name, (SELECT d1.district_name FROM survey_district_master d1 WHERE u1.district_id = d1.district_id) district_name, (SELECT r1.revenue_name FROM survey_revenue_master r1 WHERE u1.revenue_id = r1.revenue_id) revenue_name, (SELECT b1.block_name FROM survey_block_master b1 WHERE u1.block_id = b1.block_id) block_name, (SELECT v1.village_name FROM survey_village_master v1 WHERE u1.village_id = v1.village_id) village_name, (SELECT r1.role_name FROM survey_role_master r1 WHERE u1.role_id = r1.role_id) role_name
		    FROM survey_users u1 
            WHERE u1.user_id = ".$user_id."
			AND u1.is_deleted = 0");

		$user_data = $user_query->result_array();


		$data['user_data'] 		= $user_data;		
		$data['module_name'] 	= 'User';
		$data['submodule_name'] = '';	
    	$data['middle_content']	= 'users/user_details';

		$this->load->view('admin/admin_combo',$data);
	}

	public function logout(){
		$user_id = base64_decode($_POST['user_id']);

		$updArr = array('is_login'=>0, 
						'updated_by_id' => $this->session->userdata('user_id'),
						'updated_on' => date('Y-m-d H:i:s')
						);	
		$updateQuery = $this->master_model->updateRecord('users', $updArr, array('user_id' => $user_id));
		
		echo $updateQuery;
	}
}