<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
Class : Question
Author : Priyanka Wadnere
*/

class Question_mapping extends CI_Controller 
{
	
	function __construct() {
			
        parent::__construct();

		$this->load->helper('security');
		if($this->session->userdata('user_id') == ""){			
			redirect(base_url('xAdmin/admin'));
		}	
		
		$this->user_id            	=  $this->session->userdata('user_id');
        $this->ip                 	=  $this->input->ip_address();
        $this->data['module_name'] 	= 	'Survey';
        $this->data['submodule_name'] = 'Question_mapping';
        $this->module_title       =  "Question";
        $this->module_view_folder =  "question_mapping/";    
    }

    public function index()
    {	

      // Permission Set Up
      $this->master_model->permission_access('17', 'view');
     
		  $where = array('is_deleted' => 0); //'status' => 'Active', 
		  $order_by = array('survey_id' => 'DESC');
		  $survey_data = $this->master_model->getRecords("survey_master", $where, '', $order_by);	
		
      $data['survey_data']    	= $survey_data;
     // $data['survey_id']      = $survey_id;
      $data['module_name']     	= 'Survey';
      $data['submodule_name'] 	= 'Question_mapping';   
      $data['middle_content']     = 'question_mapping/add';
      $this->load->view('admin/admin_combo',$data);
 	}


   	public function add(){

      // Permission Set Up
      $this->master_model->permission_access('17', 'add');

        //error_reporting();
       
        if(isset($_POST['mode']) == 'submit'){
        	if(isset($_POST['question_checked_array'])){
        		$question_checked_array = $_POST['question_checked_array'];
            //print_r($question_checked_array); //die;
        	}

      
            if(sizeof($question_checked_array)>0){
                $survey_id = trim($this->input->post('survey'));
                $section_id = trim($this->input->post('section'));
                $sub_section_id = trim($this->input->post('sub_section'));

                if($_POST['survey']!='' && $_POST['section']!='' && $_POST['sub_section']!=''){

                  $this->db->where('survey_id', $survey_id);
                  $this->db->where('section_id', $section_id);
                  $this->db->where('sub_section_id', $sub_section_id);
                }

                else if($_POST['survey']!='' && $_POST['section']!=''){
                  $this->db->where('survey_id', $survey_id);
                  $this->db->where('section_id', $section_id);
                  $this->db->where('sub_section_id', 0);
                }
                   
                else if($_POST['survey']!=''){
                  $this->db->where('survey_id', $survey_id);
                  $this->db->where('section_id', 0);
                  $this->db->where('sub_section_id', 0);
                } 
               
              $this->db->delete('survey_section_questions');

               // echo $this->db->last_query();

            	foreach ($question_checked_array as $key => $value) {
            		
                    $dataArr = array( 'survey_id'           => trim($this->input->post('survey')),
                                      'section_id'          => trim($this->input->post('section')),
                                      'sub_section_id'      => trim($this->input->post('sub_section')),
                                      'question_id'         => $question_checked_array[$key],
                                      'global_question_id'  => trim($this->input->post('global_question')),
                                      'created_by_id'       => $this->session->userdata('user_id')
                                    );

    				        $insertQuery = $this->master_model->insertRecord('survey_section_questions',$dataArr);  
            //echo $this->db->last_query();
            	}

               // Get Log Setting
              $logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
              $ipAddr   = $this->master_model->get_client_ip();
              // Log Data Added
              $postArr      = $this->input->post(); 
              $json_encode_data   = json_encode($postArr);
              $logData = array('user_id'   => $this->session->userdata('user_id'),
                      'action_name'=> "Assign Question",
                      'module_name'=> 'Question Mapping',
                      'store_data' => $json_encode_data,
                      'ip_address' => $ipAddr,
                      'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

                if ($insertQuery>0) {
                    echo 1;
                }
                else{
                    echo 0;
                }
            }
            else{
                echo 2;
            }

        	
        }

       /* $where = array('status' => 'Active', 'is_deleted' => 0);
		$survey_data = $this->master_model->getRecords("survey_master", $where);	
		
        $data['survey_data']    	= $survey_data;
        $data['module_name']     	= 'Master';
        $data['submodule_name'] 	= 'Question';   
        $data['middle_content']     = 'question_mapping/add';
        $this->load->view('admin/admin_combo',$data);*/
    }

    public function delete_mapped_question()
	{
      if(isset($_POST['action']) == 'delete_mapped_question'){


         // Get Log Setting
        $logCapture = $this->master_model->getRecords("setting",array('setting_id' => 1));
        $ipAddr   = $this->master_model->get_client_ip();
        // Log Data Added
        $postArr      = $this->input->post(); 
        $json_encode_data   = json_encode($postArr);
        $logData = array('user_id'   => $this->session->userdata('user_id'),
                'action_name'=> "Delete Assigned Question",
                'module_name'=> 'Question Mapping',
                'store_data' => $json_encode_data,
                'ip_address' => $ipAddr,
                'user_agent' => json_encode($_SERVER['HTTP_USER_AGENT']));

        if($_POST['survey']!=0 && $_POST['section']!=0 && $_POST['sub_section']!=0){

            $where = array('survey_id'=> $_POST['survey'], 'section_id'=> $_POST['section'], 'sub_section_id'=> $_POST['sub_section'], 'question_id'=> $_POST['question_id']);

            $question_data = $this->master_model->getRecords("survey_section_questions", $where);

            if(sizeof($question_data)>0){
              $this->db->where('survey_id', $_POST['survey']);
              $this->db->where('section_id', $_POST['section']);
              $this->db->where('sub_section_id', $_POST['sub_section']);
              $this->db->where('question_id', $_POST['question_id']);

              $res = $this->db->delete('survey_section_questions');
              echo $res;
            }
          }

          else if($_POST['survey']!=0 && $_POST['section']!=0){
          
            $where = array('survey_id'=> $_POST['survey'], 'section_id'=> $_POST['section'], 'question_id'=> $_POST['question_id']);

            $question_data = $this->master_model->getRecords("survey_section_questions", $where);

            if(sizeof($question_data)>0){
              $this->db->where('survey_id', $_POST['survey']);
              $this->db->where('section_id', $_POST['section']);
              $this->db->where('question_id', $_POST['question_id']);

              $res = $this->db->delete('survey_section_questions');
              echo $res;
            }
          }
             
          else if($_POST['survey']!=0){
           

            $where = array('survey_id'=> $_POST['survey'], 'question_id'=> $_POST['question_id']);

            $question_data = $this->master_model->getRecords("survey_section_questions", $where);

            if(sizeof($question_data)>0){
              $this->db->where('survey_id', $_POST['survey']);
              $this->db->where('question_id', $_POST['question_id']);

              $res = $this->db->delete('survey_section_questions');
              echo $res;
            }
            //$question_data = $this->master_model->getRecords("survey_section_questions");
          } 

        
      }
    }
   	

	public function view($question_id){

    // Permission Set Up
    $this->master_model->permission_access('17', 'view');

     	$question_id = base64_decode($question_id);

     	$this->db->where('survey_question_master.question_id',$question_id);
     	$this->db->order_by('survey_questions_options_master.display_order','ASC');
    	$this->db->join('survey_questions_options_master','survey_question_master.question_id = survey_questions_options_master.question_id');

     	$question_data = $this->master_model->getRecords("survey_question_master",$where, "survey_question_master.question_id, survey_question_master.parent_question_id, survey_question_master.question_text, survey_question_master.response_type_id, survey_question_master.is_mandatory, survey_questions_options_master.option, survey_questions_options_master.display_order");
    	
		$this->data['action']     		=  $action;
		$this->data['page_title']       = 'View '.$this->module_title;
		$this->data['module_title']     = $this->module_title;
		$this->data['question_data']    = $question_data;
		$this->data['middle_content']   = $this->module_view_folder."question_view";
		//echo '---------------------------------------------------'.$this->data['middle_content'];

		$this->load->view('admin/admin_combo',$this->data);
	}

	// Update Role Status
	public function changeStatus(){
		 
		 $id 	= $this->uri->segment(4);
		 $value = ucfirst($this->uri->segment(5));
		 if($value == 'Active'){
			 
			 $updateQuery = $this->master_model->updateRecord('survey_list',array('status'=>'Inactive'),array('survey_id' => $id));
			 $this->session->set_flashdata('success','Status successfully changed');
			 redirect(base_url('xAdmin/survey'));	
			 
		 } else if($value == 'Inactive'){
			 
			$updateQuery = $this->master_model->updateRecord('survey_list',array('status'=>'Active'),array('survey_id' => $id)); 
			$this->session->set_flashdata('success','Status successfully changed');
			redirect(base_url('xAdmin/survey'));		
		 }
		 
	}

	public function get_section(){

     	$where = array('survey_id'=>$_POST['survey_id']);
     	$this->db->join('survey_section_master', 'survey_section_mapping.section_id = survey_section_master.section_id');

     	$section_data = $this->master_model->getRecords("survey_section_mapping", $where, "survey_section_mapping.*, survey_section_master.section_name");
      //echo $this->db->last_query();

       $question_query = $this->db->query("SELECT q1.question_id, CASE WHEN (SELECT ssq.question_id FROM survey_section_questions ssq WHERE ssq.survey_id = q1.survey_id AND ssq.question_id = q1.question_id AND ssq.is_deleted=0) THEN 'yes' 
            ELSE 'no' END checked,  q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory 
            FROM survey_question_master q1 LEFT JOIN survey_section_mapping s2 ON s2.survey_id = q1.survey_id
            WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                              WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted=0) 
            AND q1.survey_id = ".$_POST["survey_id"]."
            AND q1.parent_question_id = 0
            AND q1.is_deleted = 0
            AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.section_id > 0 AND s2.is_deleted=0)
            AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.sub_section_id > 0 AND s2.is_deleted=0)
            GROUP BY q1.question_id  
            ORDER BY q1.question_id  ASC");

		    $question_data = $question_query->result_array();


        /*$where2 = array('question_master.survey_id'=>$_POST['survey_id'], 'question_master.is_deleted' => 0,);
        $this->db->join('survey_section_mapping m1', 'm1.survey_id = '.$_POST['survey_id']);
        $this->db->group_by('question_master.question_id');
        $global_question_data = $this->master_model->getRecords("question_master", $where2, 'question_master.*, m1.section_id');*/

        $str = '';
        if(sizeof($section_data)>0){
            $str.=json_encode($section_data);
        }
        if(sizeof($question_data)>0){
            $str.='%%'.json_encode($question_data);
        }
       /* if(sizeof($global_question_data)>0){
            $str.='%%'.json_encode($global_question_data);
        }*/
       
        echo $str;

	}
	
	public function get_sub_section(){
		
		 $str = '';

      if($_POST["section_id"]==''){

        $question_query = $this->db->query("SELECT q1.question_id, CASE WHEN (SELECT ssq.question_id FROM survey_section_questions ssq WHERE ssq.survey_id = q1.survey_id AND ssq.question_id = q1.question_id AND ssq.is_deleted=0) THEN 'yes' 
            ELSE 'no' END checked,  q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory 
            FROM survey_question_master q1 LEFT JOIN survey_section_mapping s2 ON s2.survey_id = q1.survey_id
            WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                              WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted=0) 
            AND q1.survey_id = ".$_POST["survey_id"]."
            AND q1.parent_question_id = 0
            AND q1.is_deleted = 0
            AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.section_id > 0 AND s2.is_deleted=0)
            AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.sub_section_id > 0 AND s2.is_deleted=0)
            GROUP BY q1.question_id  
            ORDER BY q1.question_id  ASC");

          $question_data = $question_query->result_array();

          if(sizeof($question_data)>0){
            $str.='%%'.json_encode($question_data);
          }
      }
      else{

        $where = array('parent_section_id'=>$_POST['section_id'], 'is_deleted' => 0);
        $section_data = $this->master_model->getRecords("section_master", $where);
        //echo $this->db->last_query();die();
      
       
        if(sizeof($section_data)>0){
          $str.=json_encode($section_data);
        }

          $question_query = $this->db->query("SELECT q1.question_id, CASE WHEN (SELECT ssq.question_id FROM survey_section_questions ssq WHERE ssq.survey_id = q1.survey_id AND ssq.question_id = q1.question_id AND ssq.section_id = ".$_POST["section_id"]." AND ssq.is_deleted=0 ) THEN 'yes' ELSE 'no' END checked, 
          q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory
          FROM survey_question_master q1 LEFT JOIN survey_section_questions ssq ON ssq.question_id = q1.question_id
          WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                            WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted=0) 
          AND q1.survey_id = ".$_POST["survey_id"]."
          AND q1.parent_question_id = 0
          AND q1.is_deleted = 0
          AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.section_id != ".$_POST["section_id"]." 
                                       AND s2.is_deleted=0)
          AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.sub_section_id > 0
                                       AND s2.is_deleted=0)
          GROUP BY q1.question_id  
          ORDER BY q1.question_id  ASC");

          $question_data = $question_query->result_array();

          //echo $this->db->last_query();

          if(sizeof($question_data)>0){
            $str.='%%'.json_encode($question_data);
          }
          /* AND q1.question_id NOT IN ( SELECT s2.question_id 
                                        FROM survey_section_questions s2
                                        WHERE s2.survey_id = ".$_POST['survey_id']."
                                        AND   s2.section_id = '')*/
          $global_question_query = $this->db->query("SELECT q1.* 
            FROM survey_question_master q1 LEFT JOIN survey_section_mapping m1 ON m1.survey_id = q1.survey_id
            WHERE  NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                               WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted=0) 
            AND NOT EXISTS (SELECT * FROM survey_question_master q2 
                            WHERE q2.parent_question_id = q1.question_id AND q2.is_deleted=0)
            AND q1.question_id NOT IN ( SELECT s2.question_id 
                                        FROM survey_section_questions s2
                                        WHERE s2.survey_id = ".$_POST['survey_id']."
                                        AND   s2.section_id = ".$_POST['section_id']."
                                        AND   s2.is_deleted=0)
            AND q1.survey_id = ".$_POST['survey_id']."
            AND q1.parent_question_id = 0
            AND q1.is_deleted = 0
            GROUP BY q1.question_id  
            ORDER BY q1.question_id  ASC");

          $global_question_data = $global_question_query->result_array();

          if(sizeof($global_question_data)>0){
            $str.='%%'.json_encode($global_question_data);
          }
          else{
            $str.='%%'.'';
          }

          $selected_global_question_query = $this->db->query("SELECT ssq.global_question_id 
            FROM survey_section_questions ssq 
            WHERE ssq.survey_id = ".$_POST['survey_id']."
            AND ssq.section_id = ".$_POST['section_id']."
            AND ssq.is_deleted = 0 
            AND ssq.global_question_id <> 0");

          $selected_global_question = $selected_global_question_query->result_array();

          if(sizeof($selected_global_question)>0){
            $str.='%%'.$selected_global_question[0]['global_question_id'];
          }
      }
     	
     	echo $str;
		
	}

  public function get_subsection_questions(){
    
    if($_POST["sub_section_id"]!=''){

      $question_query = $this->db->query("SELECT q1.question_id, CASE WHEN (SELECT ssq.question_id FROM survey_section_questions ssq WHERE ssq.survey_id = q1.survey_id AND ssq.question_id = q1.question_id AND ssq.section_id = ".$_POST["section_id"]." AND ssq.is_deleted = 0) THEN 'yes' 
          ELSE 'no' END checked, 
          q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory
          FROM survey_question_master q1 LEFT JOIN survey_section_questions ssq ON ssq.question_id = q1.question_id
          WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                            WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted = 0) 
          AND q1.survey_id = ".$_POST["survey_id"]."
          AND q1.parent_question_id = 0
          AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.sub_section_id != ".$_POST["sub_section_id"]."
                                       AND   s2.is_deleted = 0)
          GROUP BY q1.question_id  
          ORDER BY q1.question_id  ASC");

      //echo $this->db->last_query();

      $question_data = $question_query->result_array();
    }
    else{

      $question_query = $this->db->query("SELECT q1.question_id, CASE WHEN (SELECT ssq.question_id FROM survey_section_questions ssq WHERE ssq.survey_id = q1.survey_id AND ssq.question_id = q1.question_id AND ssq.section_id = ".$_POST["section_id"]." AND ssq.is_deleted = 0) THEN 'yes' ELSE 'no' END checked, 
          q1.parent_question_id, q1.question_text, q1.response_type_id, q1.is_mandatory
          FROM survey_question_master q1 LEFT JOIN survey_section_questions ssq ON ssq.question_id = q1.question_id
          WHERE NOT EXISTS (SELECT * FROM  survey_option_dependent optn
                            WHERE q1.question_id = optn.dependant_ques_id AND optn.is_deleted = 0) 
          AND q1.survey_id = ".$_POST["survey_id"]."
          AND q1.parent_question_id = 0
          AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.section_id != ".$_POST["section_id"]."
                                       AND   s2.is_deleted = 0)
          AND q1.question_id NOT IN (SELECT s2.question_id 
                                       FROM survey_section_questions s2
                                       WHERE s2.survey_id = ".$_POST["survey_id"]."
                                       AND   s2.sub_section_id > 0
                                       AND   s2.is_deleted = 0)
          GROUP BY q1.question_id  
          ORDER BY q1.question_id  ASC");

      
      $question_data = $question_query->result_array();
    }

    //echo $this->db->last_query();die();

    if(sizeof($question_data)>0){
      echo json_encode($question_data);
    }
    else{
      echo '';
    }
     
    
  }



}