<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Upload File</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Upload File</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
           <div class="card-header">
           <h3 class="card-title">Upload File</h3>
              <a href="<?php echo base_url('xAdmin/file_upload') ?>" class="btn btn-info btn-sm pull-right float-right"><i class="fa fa-backward"></i> Back</a>
           </div> 
              <!-- form start -->
              <?php if($this->session->flashdata('error')){ ?>
               <div class="alert alert-danger alert-dismissible">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                 <h5><i class="icon fas fa-ban"></i> Error!</h5>
                <?php echo $this->session->flashdata('error'); ?>
               </div>
               <?php } ?>
            <div class="card-body">
                <form method="post" id="subfrm" name="subfrm" role="form" >
                    <div class="row">
                     <div class="col-6">  
                       
                        <div class="form-group">
                           <label for="exampleInputEmail1">Upload file</label>
                           <input type="file" class="form-control" id="files" placeholder="Upload File" name="files"/>
                        </div>   
                                            
                        <div class="card-footer1">
                           <button type="submit" class="btn btn-primary">Upload</button>                     
                        </div>
                     </div>
                        <!-- /.card-body -->
                    </div>
                     <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
               </form>
            </div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
</script>


