<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Input Category Type</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Input Category Type</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">	   
         <div class="card ">
			<div class="card-header">				
				<a href="<?php echo base_url('xAdmin/inputcategory/add') ?>" class="btn btn-info btn-sm float-right"><i class="fa fa-plus"></i> Add Input Category</a>
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                    <th>No.</th>
					<th>Name</th>
                    <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;					
                     foreach($records as $roledata) 
                     { 					 
					 ?>
					<tr>
						<td style="width:5%"><?php echo $i; ?></td>
						<td style="width:80%"><?php echo $roledata['validation_type'] ; ?></td>         
						<td style="width:10%">						
							<a href="<?php echo base_url();?>xAdmin/inputcategory/edit/<?php echo base64_encode($roledata['validation_id']);  ?>" ><i class="fas fa-edit" title="Edit"></i></a>&nbsp;   
                     <a href="<?php echo base_url();?>xAdmin/inputcategory/delete/<?php echo base64_encode($roledata['validation_id']); ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><i class="far fa-trash-alt" title="Delete"></i></a> &nbsp; 

							<?php      
							if($roledata['status']=="Active"){
								$status = 'Inactive';
							?>
							<a href="<?php echo base_url(); ?>xAdmin/inputcategory/changeStatus/<?php echo $roledata['validation_id'].'/'.$status; ?>"><i class="fas fa-toggle-on" title="Active"></i></a><?php }
							   else if($roledata['status']=="Inactive"){
								$status = 'Active';
							   ?>
							<a href="<?php echo base_url(); ?>xAdmin/inputcategory/changeStatus/<?php echo $roledata['validation_id'].'/'.$status; ?>"><i class="fas fa-toggle-off" title="Inactive"></i> </a><?php }
							   ?>
						</td>
					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});
});
</script>