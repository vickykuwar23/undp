<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Users</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   
         <div class="card ">
			<div class="card-header">
				
				<a href="<?php echo base_url('xAdmin/users/add') ?>" class="btn btn-info btn-sm float-right"><i class="fa fa-plus"></i> Add User</a>            
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="table-responsive">
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
				<thead>
					<tr>
						<th>Sr.</th>
						<th>Username</th>
						<th>Fullname</th>
						<th>Email ID</th>
						<th>Role</th>
                  		<th>Status</th>
						<th class="action_th">Action</th>
					</tr>
				</thead>
               <tbody>
               <?php  
                  $i=1;					
                  foreach($records as $userdata)  { 	?>
      					<tr>
      						<td><?php echo $i; ?></td>
      						<td><?php echo $userdata['username']; //." ".$userdata['user_id']; ?></td> 
      						<td><?php echo ucwords($userdata['first_name'])." ".ucwords($userdata['last_name']); ?></td> 
      						<td><?php echo $userdata['email'] ; ?></td> 
      						<td><?php echo $userdata['role_name'] ; ?></td> 
                        	<td><?php   
	                           if($userdata['status']=="Active"){
	                              $status = 'Inactive';
	                           ?>
	                           <a href="<?php echo base_url(); ?>xAdmin/users/changeStatus/<?php echo $userdata['user_id'].'/'.$status; ?>" onclick="return confirm_action(this,event,'Do you really want to make it Inactive ?')"><span class="badge badge-success">Active</span><?php }
	                              else if($userdata['status']=="Inactive"){
	                              $status = 'Active';
	                              ?>
	                           <a href="<?php echo base_url(); ?>xAdmin/users/changeStatus/<?php echo $userdata['user_id'].'/'.$status; ?>" nclick="return confirm_action(this,event,'Do you really want to make it Active ?')"><span class="badge badge-danger">Inactive</span><?php }
	                              ?>
	                        </td>	
      						<td class="action" nowrap>						
      							<a href="<?php echo base_url();?>xAdmin/users/edit/<?php echo base64_encode($userdata['user_id']);  ?>"><i class="fas fa-edit" title="Edit"></i></a>&nbsp; 
      							<a href="<?php echo base_url();?>xAdmin/users/delete/<?php echo base64_encode($userdata['user_id']);  ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><i class="fas fa-trash-alt" title="Delete"></i></a>&nbsp; 
                           		<a href="<?php echo base_url();?>xAdmin/users/detail/<?php echo base64_encode($userdata['user_id']);  ?>"><i class="fas fa-info" title="Detail"></i></a>

                           		<?php if($userdata['role_id'] == $this->config->item( 'surveyor_role_id' ) && $userdata['is_login'] == 1) { ?>
                           			&nbsp;
                           			<!--<a href="javascript:void(0)" onclick="logout('<?php echo base64_encode($userdata['user_id']); ?>')"><i class="fas fa-sign-out-alt" title="Logout"></i></a>&nbsp;-->
                           		<?php }?>
      						</td>
      					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});
});

function logout(user_id){
 
      swal({
          title: "Are you sure ?",
          text: 'Do you want to Logout ?',
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#0aa89e",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: true
        }).then(result =>{
          if (result.value)
          {  
            $.ajax({
              url: site_path+"xAdmin/users/logout",
              type: 'POST',
              data: {'ci_csrf_token':'',user_id:user_id},
              success: function(response){ 
               if(response != ''){
                  swal('Logged out', "", "success");
                  location.reload();
                }
              }
            }) //ajax
          }
        });
    }// onchange
</script>