<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Users</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
      	<!-- <div class="row"> -->
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
				  <h3 class="card-title">Add Users</h3>
				  <a href="<?php echo base_url('xAdmin/users') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
			  </div> 			
				<div class="card-body">
					<?php echo validation_errors(); ?>
					 <form method="post" id="subfrm" name="subfrm" role="form" >
						 
						<div class="box-body"> 
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="role">Role Name <span style="color: red">*</span></label>
										<select class="form-control" id="role_id" name="role_id">
											<option value="">-- Select Role --</option>
											<?php 										
											if(count($role_list) > 0){
												foreach($role_list as $rolenames){
											?>										
											<option value="<?php echo $rolenames['role_id']; ?>"><?php echo ucfirst($rolenames['role_name']); ?></option>
											<?php 
												} // Foreach End										
											} // If End ?>
										</select>
									</div>
								</div>

								<?php /*?><div class="col-md-6">
									<div class="form-group">
										<label for="survey">Survey <span style="color: red">*</span></label>
										<select class="choose-tech select2" multiple="multiple" id="survey_id" name="survey_id[]" style="width: 100%;">
											<option value="">-- Select Survey --</option>
											<?php foreach($survey_list as $survey){?>						
											<option value="<?php echo $survey['survey_id']; ?>"><?php echo $survey['title']; ?></option>
											<?php 
											} // Foreach End ?>
										</select>
									</div>
								</div><?php */?>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="first_name">First Name <span style="color: red">*</span></label>
										<input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" maxlength="100" />
									<span><?php //echo form_error('first_name'); ?></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="last_name">Last Name <span style="color: red">*</span></label>
										<input type="text" class="form-control" id="last_name" placeholder="Enter Last Name" name="last_name" maxlength="100" />
										<span><?php //echo form_error('last_name'); ?></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="district">District <span style="color: red">*</span></label>
										<select class="choose-tech select2" multiple="multiple" id="district_id" name="district_id[]" style="width: 100%;" onChange="return getRevenue();">
										<option value="" disabled>-- Select District --</option>	
										<?php foreach($district_list as $district) {?>
											<option value="<?php echo $district['district_id']?>"><?php echo $district['district_name']?></option>	
										<?php } ?>									
										</select>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label for="revenue_circle">Revenue Circle <span style="color: red">*</span></label>
										<select class="choose-tech select2" multiple="multiple" id="revenue_id" name="revenue_id[]" style="width: 100%;">
										<option value="" disabled>-- Select Revenue Circle--</option>					
									</select>
									</div>
								</div>
							</div>

							


							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Email ID <span style="color: red">*</span></label>
										<input type="email" class="form-control" id="email_id" placeholder="Enter Email ID" name="email_id"  />
										<span><?php echo form_error('email_id'); ?></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Contact No. <span style="color: red">*</span></label>
										<input type="text" class="form-control" id="contact_no" placeholder="Enter Contact No." name="contact_no"  maxlength="10" onkeypress="return isNumber(event)" />
										<span><?php echo form_error('contact_no'); ?></span>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="exampleInputEmail1">Username <span style="color: red">*</span></label>
										<input type="text" class="form-control" id="username" placeholder="Enter Username" name="username" maxlength="100" />
										<span><?php echo form_error('username'); ?></span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group view_pass">
										<label for="exampleInputEmail1">Password <span style="color: red">*</span></label>
										<input type="password" class="form-control" id="pwd" placeholder="Enter Password" name="pwd" maxlength="100" />
										<div class="input-group-append">
								            <div class="input-group-text">
								              <span class="far fa-eye" onclick="toggle_pwd()"></span> 
								              <!--  <span class="fas fa-lock"></span> -->
								            </div>
								        </div>
										<span><?php echo form_error('pwd'); ?></span>
									</div>
								</div>
							</div>

							<?php /*?> <div class="form-group">
								<label for="exampleInputEmail1">State <em>*</em></label>
								<select class="choose-tech select2" multiple="multiple" id="state_id" name="state_id[]" onChange="return getDistrict();" style="width: 100%;">
									<option value="">-- Select State --</option>
									<?php 										
									if(count($state_list) > 0){
										foreach($state_list as $statenames){
									?>										
									<option value="<?php echo $statenames['state_id']; ?>"><?php echo ucfirst($statenames['state_name']); ?></option>
									<?php 
										} // Foreach End										
									} // If End ?>
								</select>									
							</div><?php */?>
															
							<!-- <div class="form-group">
								<label for="exampleInputEmail1">Block Name <em>*</em></label>
								<select class="choose-tech select2" multiple="multiple" id="block_id" name="block_id[]" style="width: 100%;"  onChange="return getVillage();">
									<option value="">-- Select Block --</option>
								</select>
							</div>		 -->						
							<?php /* ?>	<div class="form-group">
								<label for="exampleInputEmail1">Branch Name <em>*</em></label>
								<select class="form-control" id="branch_id" name="branch_id">
									<option value="">-- Select Branch --</option>
									<?php 										
									if(count($brance_list) > 0){
										foreach($brance_list as $branchnames){
									?>										
									<option value="<?php echo $branchnames['branch_id']; ?>"><?php echo ucfirst($branchnames['branch_name']); ?></option>
									<?php 
										} // Foreach End										
									} // If End ?>
								</select>
							</div>							
							<div class="form-group">
								<label for="exampleInputEmail1">Village Name <em>*</em></label>
								<select class="choose-tech select2" multiple="multiple" id="village_id" name="village_id[]" style="width: 100%;">
									<option value="">-- Select Village --</option>
								</select>
							</div>	<?php */ ?>								
								
							<div class="card-footer1">
								<button type="submit" id="btn_submit" class="btn btn-primary">ADD</button>	 
							</div>
	
						</div>		
						
						<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>

				</div>
            </div>
            <!-- ./col -->
        <!--  </div> -->
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

function getRevenue() {
	var str='';
	var val=document.getElementById('district_id');
	for (i=0;i< val.length;i++) { 
		if(val[i].selected){
			str += val[i].value + ','; 
		}
	}         
	var str=str.slice(0,str.length -1);
	//var selectedType = document.getElementById('role_id').value;

	$.ajax({          
			type: "POST",
			url: "<?php echo base_url('xAdmin/users/get_revenue_list');?>",			
			data:{district_id:str},			
			success: function(res){  
			   //alert(res);              
			   $('#revenue_id').html(res);              
			}			
		});
}

/*function getDistrict() {
	var str='';
	var val=document.getElementById('state_id');
	for (i=0;i< val.length;i++) { 
		if(val[i].selected){
			str += val[i].value + ','; 
		}
	}         
	var str=str.slice(0,str.length -1);	
	$.ajax({          
			type: "POST",
			url: "<?php //echo base_url('xAdmin/users/get_district_list');?>",			
			data:{state_id:str},			
			success: function(res){                
			   $('#district_id').html(res);              
			}			
		});
}*/


/*function getBlock() {
	var str='';
	var val=document.getElementById('district_id');
	for (i=0;i< val.length;i++) { 
		if(val[i].selected){
			str += val[i].value + ','; 
		}
	}         
	var str=str.slice(0,str.length -1);
	//var selectedType = document.getElementById('role_id').value;
	//alert(selectedType);
	$.ajax({          
			type: "POST",
			url: "<?php //echo base_url('xAdmin/users/get_block_list');?>",			
			data:{district_id:str},			
			success: function(res){                
			   $('#block_id').html(res);              
			}			
		});
}*/

/*function getVillage() {
	var str='';
	var val=document.getElementById('block_id');
	for (i=0;i< val.length;i++) { 
		if(val[i].selected){
			str += val[i].value + ','; 
		}
	}         
	var str=str.slice(0,str.length -1);
	
	$.ajax({          
			type: "POST",
			url: "<?php //echo base_url('xAdmin/users/get_village_list');?>",			
			data:{block_id:str},			
			success: function(res){                
			   $('#village_id').html(res);              
			}			
		});
}*/

$(document).ready(function () {	
		
	$.validator.setDefaults({
		submitHandler: function () {
		  //alert( "Form successful submitted!" );
		  $('#btn_submit').prop('disabled',true);
		  form.submit();
		}
	}); 

		
  
	$.validator.addMethod("alphanumeric", function(value, element) {
		return this.optional(element) || /^[a-zA-Z]*$/.test(value);
	}, "Please enter only letters, numbers and dash");
	
	$.validator.addMethod("nospcialchar", function(value, element) {
		return this.optional(element) || /^[a-zA-Z0-9_]*$/.test(value);
	}, "Please enter only letters, digits and underscore");
	
	$.validator.addMethod("password_complex", function(value, element) {
     return this.optional( element ) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$!%*?&])[A-Za-z\d@#$!%*?&]{6,}$/.test( value );
   }, 'Password must be minimum 6 characters, at least one uppercase letter, one lowercase letter, one number and one special character.');
  
	$.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
  
	$.validator.addMethod("valid_email", function(value, element) 
    { 
      var email = value;
      var pattern = new RegExp("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
      var result = pattern.test(email); 
      if(result){return true;}else {return false;}
    });
	
	$.validator.addMethod('NotAllZero', function(value) {
		return value.match(/^(?!0*$).*$/);
    }, 'Invalid Mobile Number');
	
	/*$.validator.addMethod("check_email", function(value, element) 
    { 
		var role_id = $("#role_id").val();
		$.ajax({          
			type: "POST",
			url: "<?php echo base_url('xAdmin/users/exists_email');?>",			
			data:{email:value, role_id: role_id},			
			success: function(res){
				if(res){return true;}else {return false;}            
			}			
		});
      
    });*/
	
	
	/*$.validator.addMethod("isValidMemberType", function(value, element) {
        var order = $(element).attr('data-order');
        if(value == 'PEOPLE_AND_COMPANIES')
        	return true;
        else if(value == 'PEOPLE_ONLY' && $('#company'+order).val() != 0)
        	return false;
        else if(value == 'COMPANIES_ONLY' && $('#user'+order).val() != 0)
        	return false;
        else return true;
    }, function(value,element){
    	if($(element).val() == 'PEOPLE_ONLY')
        	return $L("people.only.setting.error");
        else if($(element).val() == 'COMPANIES_ONLY')
        	return $L("company.only.setting.error");
    });*/
  
  $('#subfrm').validate({
    rules: {
		role_id: {
			required: true
		},
		/*"state_id[]": {
			required: function() {
                return $("#role_id").val() == '' ||  $("#role_id").val() == 3 ||  $("#role_id").val() == 4 ||  $("#role_id").val() == 5; 
			}
		},*/
		"district_id[]": {
			required: function() {
               return $("#role_id").val() == '' ||  $("#role_id").val() == 3 ||  $("#role_id").val() == 4 ||  $("#role_id").val() == 5;
			}	
        },
        "revenue_id[]": {
			required: function() {
               return $("#role_id").val() == '' ||  $("#role_id").val() == 3 ||  $("#role_id").val() == 4 ||  $("#role_id").val() == 5;
			}	
        },
      
		/*"block_id[]": {
			required: function() {
               return $("#role_id").val() == '' ||  $("#role_id").val() == 4 ||  $("#role_id").val() == 5; 
			}	
		},	*/
		/*branch_id: {
			required: true
		},*/
		/*"village_id[]": {
			required: function() {
               return $("#role_id").val() == '' ||  $("#role_id").val() == 5; 
			}
		},*/
		survey_id: {
			required: true,
		},
		first_name: {
			required: true,
			nowhitespace:true,
			alphanumeric: true,
			minlength:3,
			maxlength:50
		},
		last_name: {
			required: true,
			nowhitespace:true,
			alphanumeric: true,
			minlength:3,
			maxlength:50
		},
		email_id: {
			required: true,
			valid_email:true,
			normalizer: function(value) {
				return $.trim($("#email_id").val());
			},
			remote: {
				 url: "<?php echo base_url('xAdmin/users/exists_email');?>",
				 type: "post"
			}
		},
		contact_no: {
			required: true,
			digits:true
		},
		username: {
			required: true,
			nowhitespace:true,
			nospcialchar:true,
			remote: {
				 url: "<?php echo base_url('xAdmin/users/exists_username');?>",
				 type: "post"
			}
		},
		pwd: {
			required: true,
			nowhitespace:true,
			password_complex:true
		}
    },
    messages: {	
		role_id: "This field is required",
		//state_id: "This field is required",
		district_id: "This field is required",
		survey_id: "This field is required",	
		//block_id: "This field is required",	
		/*branch_id: {
			required: true
		},*/
		village_id: "This field is required",
		first_name: {
			required: "This field is required",
			alphanumeric:"Please enter only character."
		},
		last_name: {
			required: "This field is required",
			alphanumeric:"Please enter only character."
		},
		//first_name: "This field is required",
		//last_name: "This field is required",
		email_id: {
			required: "This field is required",
			valid_email:"Please enter valid email address.",
			remote:"Email already exist"
		},
		contact_no: "This field is required",
		username: {
			required: "This field is required",
			minlength: "Enter Username must be at least {0} characters long",
			remote:"Username already exist"
		},
		pwd: { required: "This field is required" },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function toggle_pwd() {
    var x = document.getElementById("pwd");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
} 
</script>


