<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Details</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- <div class="row"> -->
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            <h3 class="card-title">User Details</h3>
            <a href="<?php echo base_url('xAdmin/users') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
          </div>      
          <div class="card-body">
              
            <div class="box-body"> 

            <table id="ex" class="table table-bordered table-striped">
             <tbody>
              <tr>
                <td width="16%"><strong>Name</strong></td>
                <td><?php echo @$user_data[0]['full_name']; ?></td>
              </tr>

              <tr>
                <td><strong>State</strong></td>
                <td><?php echo @$user_data[0]['state_name']; ?></td>
              </tr>
           
              <tr>
               <td><strong>District</strong></td>
                <td><?php echo @$user_data[0]['district_name']; ?></td>
              </tr>

              <tr>
               <td><strong>Revenue Circle</strong></td>
               <td><?php echo @$user_data[0]['revenue_name']; ?></td>
              </tr>
              
             <!-- <tr>
                <td><strong>Block</strong></td>
                <td><?php echo @$user_data[0]['block_name']; ?></td>
              </tr>
              
              <tr>
                <td><strong>Village</strong></td>
                <td><?php echo @$user_data[0]['village_name']; ?></td>
              </tr>--->

              <tr>
                <td><strong>Username</strong></td>
                <td><?php echo @$user_data[0]['username']; ?></td>
              </tr>
              
              <tr>
               <td><strong>Contact No.</strong></td>
                <td><?php echo @$user_data[0]['contact_no']; ?></td>
              </tr>

              <tr>
               <td><strong>Email Id</strong></td>
               <td><?php echo @$user_data[0]['email']; ?></td>
              </tr>
              
              <tr>
                <td><strong>Role</strong></td>
                <td><?php echo @$user_data[0]['role_name']; ?></td>
              </tr>

              <!--<tr>
                <td><strong>Last logged in on </strong></td>
                <td><?php if(@$user_data[0]['updated_on'] == '0000-00-00 00:00:00'){ echo @$user_data[0]['created_on']; } else { echo @$user_data[0]['updated_on']; } ?></td>
              </tr>-->

              <tr>
                <td><strong>Status</strong></td>
                <td><?php echo @$user_data[0]['status']; ?></td>
              </tr>
            
             <!-- <tr>
                <td><strong>Last modified on</strong></td>
                <td><?php if(@$user_data[0]['updated_on'] == '0000-00-00 00:00:00'){ echo @$user_data[0]['created_on']; } else { echo @$user_data[0]['updated_on']; } ?></td>
              </tr>-->

              </tfoot>
            </table>
        
          
          </div>    
      
        </div>
      </div>
            <!-- ./col -->
        <!--  </div> -->
         <!-- /.row -->
    </div>
      <!-- /.container-fluid -->
  </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->




