<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Users</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">User Details</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <?php 
   $arrayDistrict = array();
    $arrayRevenue = array();
   if(@$user_data[0]['district_id']!="")
   {
	   $dis = $user_data[0]['district_id'];
	   $exp = explode(",",$dis);
	   if(count($exp) > 0)
	   {
		   foreach($exp as $disResult)
		   {	
				$this->db->select('district_name');
				$districtResult = $this->master_model->getRecords("district_master",array('district_id' => $disResult));
				array_push($arrayDistrict, $districtResult[0]['district_name']);
		   }		   
	   }
   }
   
   if(count($arrayDistrict) > 0)
   {
	   $implodeDistrict = implode(", ", $arrayDistrict);
   }
   else 
   {
	   $implodeDistrict = '';
   }
   
   if(@$user_data[0]['revenue_id']!="")
   {
	   $rev = $user_data[0]['revenue_id'];
	   $revexp = explode(",",$rev);
	   if(count($revexp) > 0)
	   {
		   foreach($revexp as $revResult)
		   {	
				$this->db->select('revenue_name');
				$revenueResult = $this->master_model->getRecords("revenue_master",array('revenue_id' => $revResult));
				array_push($arrayRevenue, $revenueResult[0]['revenue_name']);
		   }		   
	   }
   }
   
   if(count($arrayRevenue) > 0)
   {
	   $implodeRevenue = implode(", ", $arrayRevenue);
   }
   else 
   {
	   $implodeRevenue = '';
   }
   
   ?>
   <section class="content">
      <div class="container-fluid">
        <!-- <div class="row"> -->
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            <h3 class="card-title">User Details</h3>
            <a href="<?php echo base_url('xAdmin/users') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
          </div>      
          <div class="card-body">
              
            <div class="box-body"> 

            <table id="ex" class="table table-bordered table-striped">
             <tbody>
              <tr>
                <td width="16%"><strong>Name</strong></td>
                <td><?php echo @$user_data[0]['full_name']; ?></td>
              </tr>

              <tr>
                <td><strong>State</strong></td>
                <td>Assam</td>
              </tr>
           
              <tr>
               <td><strong>District</strong></td>
                <td><?php echo @$implodeDistrict; ?></td>
              </tr>

              <tr>
               <td><strong>Revenue Circle</strong></td>
               <td><?php echo @$implodeRevenue; ?></td>
              </tr>
              
             <!-- <tr>
                <td><strong>Block</strong></td>
                <td><?php echo @$user_data[0]['block_name']; ?></td>
              </tr>
              
              <tr>
                <td><strong>Village</strong></td>
                <td><?php echo @$user_data[0]['village_name']; ?></td>
              </tr>--->

              <tr>
                <td><strong>Username</strong></td>
                <td><?php echo @$user_data[0]['username']; ?></td>
              </tr>
              
              <tr>
               <td><strong>Contact No.</strong></td>
                <td><?php echo @$user_data[0]['contact_no']; ?></td>
              </tr>

              <tr>
               <td><strong>Email Id</strong></td>
               <td><?php echo @$user_data[0]['email']; ?></td>
              </tr>
              
              <tr>
                <td><strong>Role</strong></td>
                <td><?php echo @$user_data[0]['role_name']; ?></td>
              </tr>

              <!--<tr>
                <td><strong>Last logged in on </strong></td>
                <td><?php if(@$user_data[0]['updated_on'] == '0000-00-00 00:00:00'){ echo @$user_data[0]['created_on']; } else { echo @$user_data[0]['updated_on']; } ?></td>
              </tr>-->

              <tr>
                <td><strong>Status</strong></td>
                <td><?php echo @$user_data[0]['status']; ?></td>
              </tr>
            
             <!-- <tr>
                <td><strong>Last modified on</strong></td>
                <td><?php if(@$user_data[0]['updated_on'] == '0000-00-00 00:00:00'){ echo @$user_data[0]['created_on']; } else { echo @$user_data[0]['updated_on']; } ?></td>
              </tr>-->

              </tfoot>
            </table>
        
          
          </div>    
      
        </div>
      </div>
            <!-- ./col -->
        <!--  </div> -->
         <!-- /.row -->
    </div>
      <!-- /.container-fluid -->
  </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->




