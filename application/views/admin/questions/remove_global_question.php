<style type="text/css">
  form .error {
    color: red;
  }
</style>

<?php $question_list = $this->master_model->getRecords("survey_question_master"); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Question Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey'); ?>">Survey Listing</a></li>
                  <li class="breadcrumb-item active">Remove Global Question</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">

          <div class="card-header">
          <h3 class="card-title">Remove Global Question</h3>
          <a href="<?php echo base_url('xAdmin/survey') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
          </div>
    				
        <?php $this->load->view('admin/layouts/admin_alert'); ?>
    		  <div class="card-body">
          
            <form action="<?php //echo base_url('xAdmin/ques/add'); ?>" method="post" id="remove_global_question_form" name="remove_global_question_form" role="form" >

              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

              <center>
                <span style="color: blue; font: bold;" > Survey : &nbsp;&nbsp;<?php echo $survey;?></span>
              </center>
               
      			  <div class="row">

                <input type="hidden" name="survey" value="<?php echo $survey_id; ?>" id="survey">

                <div class="col-6">  
                  <div class="form-group">
                    <label for="section">Section</label>
                    <select class="form-control" id="section" name="section">
                      <option value="">Select Section</option>
                      <?php foreach ($section_data as $key => $value) { ?>
                         <option value="<?php echo $value['section_id']?>"><?php echo $value['section_name']?></option>
                      <?php  } ?>
                    </select>
                    <span style="color: red"><?php echo form_error('section'); ?></span>
                  </div>
                </div>

              </div>

              <div id="question_table_div">
                </br>
                  <hr>
                    <label for="question_table">Select Questions</label>
                    <table id="question_table" class="table table-bordered table-hover" width="100%">
                      <thead>
                          <th width="10%">Question id</th>
                          <th width="1%">Sr.</th>
                          <th width="40%">Question Text</th>
                          <th width="40%">Global Question</th>
                          <th width="2%"><input type="checkbox" id="selectall" value="selectall"/></th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    <hr>
                </div>

              
                <div class="card-footer1">                    
        					<!-- <button type="submit" class="btn btn-primary" name="save">Save & Add Next</button> -->
        					<button type="submit" class="btn btn-primary" id="btn_submit" name="submit">Submit</button>
        					<a href="<?php echo base_url('xAdmin/survey') ?>" class="btn btn-primary">Back</a> 
        		    </div>

    		    
    	      </form>

    	    </div><!-- /.card-body -->

        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->

    </section>
</div>

<?php //$this->load->view('client_validation'); ?>

<script type="text/javascript">
 
  $('#dependant_ques_tbldiv').hide();

  $(document).ready(function() {

    var table  = $('#question_table').DataTable({

      "dom": 'Blftirp',
      "stateSave": true,

      "deferRender": true,
      "columnDefs": [   

            { 
              "responsivePriority": -1,
              "targets": -1
            },

            {
              "visible": false,
              "targets":   0,
            },


            {
              "targets": 4,
              "orderable": false,
              "createdCell": function(td, cellData, rowData, row, col) {
                if (rowData[3] != "") {
                   var str = '{ "visible": true,"targets":   0,}'
                }
                else{
                  var str = '{ "visible": false,"targets":   0,}'
                }
                return str;
              }

            },
        
        ],

        "select": {
          "style":    'multi',
          "selector": 'td:nth-child(2)'
          
            },
    
        "responsive":{
            "details" : {
                    "type" : 'column'
                  }
           },
    });

    // Handle click on "Select all" control
    $('#selectall').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
     
      if (this.checked) {
        var rowData = table.data().toArray();

        for(var j = 0; j < rowData.length; j++)
        {
          //var data = rowData[j][2];
          var data = JSON.stringify(rowData[j]);
          if(selectedData.indexOf(data) == -1)
          {
            selectedData.push(data);
          }
        }
      }
      else{
        var rowData1 = table.data().toArray();
        for(var i = 0; i < rowData1.length; i++)
        {
          var data1 = JSON.stringify(rowData1[i]);
          for(var j = selectedData.length-1; j >= 0; j--)
          {
            if(selectedData[j] == data1)
            {
              selectedData.splice(j, 1);
            }
          }
        }
      }
   });

    $('#remove_global_question_form').on('submit', function (e) {

      var section_id = $('#section').val();
      var question_array = [];

      $.each($("input[name='checkbox_data']:checked"), function(){
        question_array.push($(this).val());
      });

      var site_path = '<?php echo base_url(); ?>';
     
      if(question_array.length > 0){
        $.ajax({
          url: site_path+"xAdmin/question/update_global_question_status",
          type: 'POST',
          data: $('#remove_global_question_form').serialize()+'&'+$.param({'ci_csrf_token':'','mode':'submit',question_array:question_array}), 
          success: function(response){
            //alert(response);
           if(response == 1){
            var msg = "Global Question Removed Successfully";
            swal(msg, "", "success");
            location.reload();
           }
          }
        })
      }
      else{
        var msg = "Please Select atleast one question";
        swal(msg, "", "warning");
      }
      return false;
    });

    $('#section').change(function(){

      var section_id = $(this).val();
      var survey_id = '<?php echo $survey_id; ?>';
      var site_path = '<?php echo base_url(); ?>';

      $.ajax({

        url: site_path+"xAdmin/question/get_questions",
        type: 'POST',
        data: {'ci_csrf_token':'',section_id:section_id, survey_id:survey_id},
        success: function(response){
        
          if(response != ''){
            var section_id = $('#section').val();
            table.clear().draw();
              $.each(JSON.parse(response), function(idx, obj) {
                var j = parseInt(idx)+1;
               
                if(obj.global_question_text != '' && obj.global_question_text != null){
                  var str = '<input type="checkbox" style="text-align:right" class="btn_check" name="checkbox_data" id="id_'+obj.question_id+'" value="'+obj.question_id+'">';
                }
                else{
                  var str = '';
                }
                values = [[ obj.question_id, j, obj.question_text, obj.global_question_text, str]];
                
                table.rows.add(values).draw();
                
              });
	        }
	        else{
	            table.clear().draw();
	        }

        }
      })
    });


  }); //ready

 
</script>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>

