<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey'); ?>">Survey List</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
        <div class="container-fluid">

	      	<div class="finnancialYear">
	        	<div class="box-body">
			       
			        <div class="box-header with-border">
			            <!-- tools box -->
			            <div class="pull-right box-tools">
			                <!--<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="collapse" data-target="#collapseExample">
			                 <i class="fa fa-plus"></i></button>-->
			            </div>
			            <!-- /. tools -->
			            <h3 class="box-title">Search Filter</h3>
			        </div>
			        <div class="col-md-12 collapse show" id="collapseExample">
			            <div class="row">    
			                <div class="col-md-4 col-sm-6">
			                    <div class="form-group">
			                        <label>Survey</label>
			                        <select name="survey" id="survey" class="form-control">
			                            <option value="">Select Survey</option>
			                            <?php foreach($survey_list as $survey)  { ?>
			                              <option value="<?php echo $survey['survey_id']?>"><?php echo $survey['title']?></option>
			                            <?php } ?>
			                        </select>
			                    </div>
			                </div>

			           
			            </div>        

			        </div>

         		</div>
            </div>
	  
        	<div class="card ">

				<div class="card-header">
					<a href="<?php echo base_url('xAdmin/question/add') ?>" class="btn btn-info btn-sm float-right" ><i class="fa fa-plus"></i> Add Question</a>                        
				</div>


	         	<div class="card-body">
	         		<center>
		         		<div id="loading" class="divLoading" style="display: none;">
		         			<p>Loading... <img src="<?php echo base_url(); ?>assets/images/loading-4.gif" width="100" height="100" /></p>
		         		</div>
		         	</center>
	    	        

	            	<table id="example1" class="table table-bordered table-hover" width="100%">
			            <thead>
			                <tr>
			                    <th width="5%">Sr.</th>
								<th width="60%">Question Text</th>
			                    <th>Response Type</th>
			                    <th>Action</th>
			                </tr>
			            </thead>
		               <tbody>
	                    <?php 
	                    $i=1;					
	                    foreach($question_list as $info)  { ?>

						   <tr>
	         					<td width="5%"><?php echo $i; ?></td>
	                        	<td width="60%"><?php echo $info['question_text'] ; ?></td>
	         					<td width="10%"><?php echo $info['response_type_id'] ; ?></td>   
	                        <?php /*?><td><?php echo $info['mandatory']; ?></td> 
	                           <td><?php echo $info['cnt']; ?></td><?php */?>
	         					<td width="10%">						
			         				<a href="<?php echo base_url();?>xAdmin/question/edit/<?php echo base64_encode($info['question_id']); ?>" title="edit"><i class="fas fa-edit"></i></a> &nbsp; <!-- <i class="fas fa-pencil-alt"></i> -->  
			                        <a href="<?php echo base_url();?>xAdmin/question/delete/<?php echo base64_encode($info['question_id']); ?>" onclick="return confirm_delete(this,event,'Do you really want to delete this record ?', <?php echo $info['question_id']; ?>, <?php echo $info['survey_id']; ?>)"><i class="far fa-trash-alt" title="Delete"></i></a> &nbsp;  
			                        <a href="<?php echo base_url();?>xAdmin/question/preview/<?php echo $info['question_id']; ?>/view" title="view"><i class="fas fa-eye"></i></a> &nbsp;
			                    </td>
	         				</tr>
	                    <?php $i++; } ?>
	                    </tbody>
	            	</table>
	            	<!-- ./col -->

	        	</div>
         	</div>
         	<!-- /.row -->
       </div>
      <!-- /.container-fluid -->
    </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {

	$("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });

    var survey_id = $('#survey').val();
    if(survey_id != ''){
	    var survey_id = $('#survey').val();
	    get_datatable(survey_id);
	}

	$('#survey').change(function(){
       var survey_id = $('#survey').val();
       //alert(survey_id);
       get_datatable(survey_id);
    });
});

function get_datatable(survey_id){

	var base_url = '<?php echo base_url(); ?>';

	$('.divLoading').css('display','block');

   /* if(survey_id == ''){
        $("#example1").DataTable().clear().draw();   
    }
    else{*/
       
        //alert('else');
        $.ajax({
            url: site_path+"xAdmin/question/get_datatable",
            type: 'POST',
            data: {'ci_csrf_token':'', survey_id:survey_id},
            success: function(response){

            	$('.divLoading').css('display','none');
              //console.log(result);
              //alert(response);
              //var response = result.split('$$$');
              if(response != ''){
                $("#example1").DataTable().clear().draw();
                $.each(JSON.parse(response), function(idx, obj) {
                  	var j = parseInt(idx)+1;

                  	var msg = 'Do you really want to delete this record ?';

                  	var action = '<a href="'+base_url+'xAdmin/question/edit/'+btoa(obj.question_id)+'" title="edit"><i class="fas fa-edit"></i></a> &nbsp;<a href="'+base_url+'xAdmin/question/delete/'+btoa(obj.question_id)+'" onclick="return confirm_delete(this,event,"'+msg+'", '+obj.question_id+', '+survey_id+')"><i class="far fa trash-alt" title="Delete"></i></a> &nbsp;<a href="'+base_url+'xAdmin/question/preview/'+obj.question_id+'/view" title="view"><i class="fas fa-eye"></i></a> &nbsp;';
                    
                    values = [[j, obj.question_text, obj.response_type_id, action]];
                    
                    $("#example1").DataTable().rows.add(values).draw();
                 
                });
              }
              else{
                $("#example1").DataTable().clear().draw();
                swal('No Data Found', "", "info"); 
              }

             

            }
        }) //ajax
      //}
    }// survey change

 function confirm_delete(ref,evt,msg,question_id,survey_id)
  {

    var msg = msg || false;
    evt.preventDefault();  

    swal({
        title: "Are you sure ?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
          //window.location = $(ref).attr('href');
          $.ajax({
            url: site_path+"xAdmin/question/check_status",
            type: 'POST',
            data: {'ci_csrf_token':'',question_id:question_id,survey_id:survey_id},
            success: function(response){    
              if(response != ''){
                 swal(response, "", "warning");
              }
              else{
                window.location = $(ref).attr('href');
              }
            }
          })  
        }
    });

  } 
</script>