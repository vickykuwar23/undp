<style type="text/css">
  .swal2-checkbox
  {
	  display:none!important;
  }

</style>
<?php $question_list = $this->master_model->getRecords("survey_question_master"); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Question</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Question</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">

          <?php $this->load->view('admin/layouts/admin_alert'); ?>

    		  <div class="card-body">          
            <form action="<?php //echo base_url('xAdmin/ques/add'); ?>" method="post" id="question_form" name="question_form" role="form" enctype="multipart/form-data">
      			<?php //echo "<pre>";print_r($option_data);
      				//echo $option_data[0]['min_value'];
      				//echo $action;
      			?>
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				        <div id="main_div">
      				    <div class="">
                    <div class="row">  
                      <div class="col-md-6 col-sm-12 form-group">
                        <label for="survey">Survey <span style="color: red">*</span></label>
                        <select class="form-control" id="survey" name="survey" onchange="get_question(this.value)">
                          <option value="">Select Survey</option>
                          <?php foreach ($survey_data as $key => $value) { ?>

                             <?php 
                             //echo $this->session->userdata('prev_survey_id');
                              if($action == 'add') { 
                                if(!empty($this->session->userdata('prev_survey_id')) && $this->session->userdata('prev_survey_id') == $value['survey_id']) { 
                                  $status = 'selected="selected"'; 
                                } 
                                else{
                                 $status = "";
                                }
                              }
                              else{ 
                                if (!empty($question_data[0]['survey_id']) && $question_data[0]['survey_id'] == $value['survey_id']) { 
                                  $status = 'selected="selected"'; 
                                }
                                else{
                                 $status = "";
                                } 
                              }

                            ?>

                            <option value="<?php echo $value['survey_id']; ?>" <?php echo $status; ?>><?php echo $value['title']; ?></option>
                          <?php } ?>
                        </select>
                        <span style="color: red"><?php echo form_error('survey'); ?></span>
                      </div>
                    </div>

					         <div class="row">
                    <div class="col-md-6 col-sm-12 form-group">
                      <label for="description">Question Text <span style="color: red">*</span></label>
                        <textarea class="form-control" id="question_text" name="question_text" placeholder="Enter Text" maxlength="500" rows="2" /><?php if(!empty($question_data[0]['question_text'])) { echo $question_data[0]['question_text']; } ?></textarea>
                        <span style="color: red"><?php echo form_error('question_text'); ?></span>
                    </div>
                    </div>
					<div class="row">
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="description">Question Help Text </label>
							<input class="form-control" id="help_label" name="help_label" placeholder="Enter Text" value="" />
						</div>
					</div>
  					 <div class="row">
  						<div class="col-md-6 col-sm-12 form-group">
  						  <label for="salutation">Select Parent Question</label>
  						  <input type="checkbox" class="form-control required-parent-box" id="if_parent_required" name="if_parent_required" title="" value="1" style="width: 20%;" <?php if($question_data[0]['parent_question_id'] > 0){ ?> checked="checked" <?php } ?>/>         
  						</div>
  					  </div>
                    <div class="row" id="parent-question-box" style="display:none;">  
                      <div class="col-md-6 col-sm-12 form-group">
                        <select class="form-control" id="parent_question" name="parent_question">                         
						              <option value="">Select Parent Question</option> 
                        </select>
                      </div>
                    </div>

            		
				    <div class="row">
	                    <div class="col-md-6 col-sm-12 form-group">
	                      <label for="salutation">If Salutation?</label>
	                      <input type="checkbox" class="form-control salutation-box" id="salutation_box" name="salutation_box" title="" value="1" style="width: 20%;" <?php if($question_data[0]['salutation']!=""){ ?> checked="checked" <?php } ?>/>         
	                    </div>
	                </div>

					<div class="row" id="salutation-box" style="display:none;">
	                    <div class="col-md-6 col-sm-12 form-group">
	                      <label for="description">Enter Salutation <span style="color: red">*</span></label>
	                      <input type="text" class="form-control " id="salutation_input" name="salutation_input" title="" value="<?php if(!empty($question_data[0]['salutation'])) { echo $question_data[0]['salutation']; } ?>" /> 
	          				<p class="note">Note: Salutation add with pipe (|) separate values.</p>	
	          				<span id="salutation_error" style="color: red;"></span>
	                     </div>
                    </div> 
          			<div class="row">
  						<div class="col-md-2 col-sm-12 form-group">
  						  <label for="salutation">Upload File</label>
  						  <input type="checkbox" class="form-control required-file" id="if_file_required" name="if_file_required" title="" value="1" style="width: 20%;" <?php if(!empty($question_data[0]['if_file_required'])) { ?> checked="checked" <?php } ?> />  &nbsp;
  						  	
  						</div>
  						<div class="col-md-4 col-sm-12 form-group" id="file-upload-label" style="display:none;">
  						  <label for="salutation">File Upload Label Name </label>
  						  &nbsp;<input type="text" class="form-control" id="file_label" name="file_label" title="File Upload Label Name" value="<?php if(!empty($question_data[0]['file_label'])) { echo $question_data[0]['file_label']; } ?>"  /> 
      					</div>
          						
          			</div>
          						
	                <div class="row">  
                    <div class="col-md-6 col-sm-12 form-group">
                      <label for="option_type">Option Type <span style="color: red">*</span></label>
                      <select class="form-control" id="option_type" name="option_type" onchange="change_option(this.value)">
                        <option value="">Select Option Type</option>
                        <?php foreach ($option_type as $key => $value) { ?>
                          <option value="<?php echo $value['input_type']; ?>" <?php if (!empty($question_data[0]['response_type_id']) && $question_data[0]['response_type_id'] == $value['input_type']){ echo 'selected="selected"'; }?>><?php echo $value['input_category']; ?></option>
                        <?php } ?>
                      </select>
                      <span style="color: red"><?php echo form_error('option_type'); ?></span>
                    </div>
                  </div>
					
                <div  id="matrix-row" style="display:none;"> 
    					<div class="row"> 
    						<div class="col-md-6 col-sm-12 form-group">
    							<label for="sub_question">Input Fields more than 1?</label>
    							<input type="checkbox" class="form-control label-add" id="question_labels" name="question_labels" title="" value="1" />
    						</div>
    					</div>	
    				</div>	
    			</div>

				<!--checkbox div start-->
                <div id="radio"  class="border px-3 pt-3 mb-3" style="display: none">
                <div class="row">
                  <div class="col-3">
                    <div class="form-group">
                      <label for="options">Add Options <span style="color: red">*</span></label>
                      <input type="text" class="form-control primary-title title_cls0" id="title0" name="title[]" placeholder="Enter Title"  value="" maxlength="200" />
                    </div>
                  </div>

                  <div class="col-1">
                    <div class="form-group">
                      <label for="option_display_order">Order</label>
                      <input type="text" class="form-control option_display_order_cls" id="option_display_order0" name="option_display_order[]" placeholder="Enter Display Order" value="" maxlength="5" />
                    </div>
                  </div>

                  <div class="col-2">
                    <div class="form-group">
                      <label for="sub_question" class="w-100">If Subquestion?</label>
                      <input type="checkbox" class="form-control check-parent-box" id="sub_question_radio0" name="sub_question_yes[0]" title="Add Sub Question" value="1" data-id="0" />
				              <input type="hidden" name="sub_q[]" id="sub_q_0" value="0" />          
                    </div>
                    <span id="if_subquestion_error0" style="color: red;"></span>
                  </div>
    				<div class="col-5 parent_qus_display0" id="select-0" style="display: none"> 
						<div class="form-group">
							<label for="sub_question">Select Question <span style="color: red"></span></label>
							<select name="parent_qus_chk[0][]" id="parent_qus0" class="form-control select2" multiple="multiple" >
							</select>
						</div>
            			<span id="parent_qus_error0" style="color: red"></span>
					</div>
					 

                </div>

                <div class="box-element" id="repeatable_radio"></div>
                <input type="hidden" name="display_order_count_hidden" id="display_order_count_hidden" value="">
						
                <div class="col-2 mb-3 pl-0">
                  <a href="javascript:void(0);" id="btn_add_radio" title="add option"><i class="btn btn-primary fas fa-plus-square"></i></a>&nbsp;

                  <a href="javascript:void(0);" id="btn_remove_radio_1" class="remove-added-box"><i class="btn btn-primary far fa-minus-square"></i></a>
                </div>					
            </div>  
          <!--checkbox div end-->				
				
	  <!--Textbox div start Parent-->
			<div id="matrix_question" style="display:none;">
			
				<div class="row" >
				
					<div class="col-6">
					<div class="form-group">
						<label for="options">Enter Label <span style="color: red">*</span></label>
						<input type="text" class="form-control primary-title1 title_cls" id="text_label0" name="text_label[]" placeholder="Enter Label"  value="" maxlength="100"/>
					</div>
				</div>
				<div class="col-1">
					<div class="form-group">
						<label for="option_display_order">Order</label>
					<input type="text" class="form-control label_display_order_cls" id="label_display_order0" name="label_display_order[]" placeholder="Enter Display Order" value="1" maxlength="10"/>
					</div>
				</div>
				<div id="repeatable_label_textbox" class="col-12"></div>
					<input type="hidden" name="display_order_label_hidden" id="display_order_label_hidden" value="1">
				</div>

				<div class="col-2 mb-2">
				  <a href="javascript:void(0);" id="btn_add_label" title="add option"><i class="btn btn-primary fas fa-plus-square"></i></a>
				  &nbsp;<a href="javascript:void(0);" id="btn_remove_radio_2" class="remove-added-box"><i class="btn btn-primary far fa-minus-square"></i></a>
				</div>
			
			<div class="row" >
				<div class="col-3">
				  <div class="form-group">
					 <select class="form-control" id="text_type_val" name="text_type_val" onchange="open_condition_list(this.value,0, 'main')">
						<option value="">--Select--</option>                                
					  </select>
				  </div>
				</div>
				<div class="col-3">
				  <div class="form-group">
					 <select class="form-control" id="condition_type_val" name="condition_type_val" onchange="open_textbox(this.value,0)">
						<option value="">--Select--</option>
					  </select>
				  </div>
				</div>
				<div class="col-2" id="custom-min-2">
				  <div class="form-group">
					<input type="text" class="form-control" id="min_val" name="min_val" placeholder="Min" value="" maxlength="100" onkeypress="return isNumber(event)">
				  </div>
				</div>
				<div class="col-2 max_div" id="custom-max-2">
				  <div class="form-group">
					<input type="text" class="form-control" id="max_val" name="max_val" placeholder="Max" value="" maxlength="100" onkeypress="return isNumber(event)">
				  </div>
				</div>	
				<div class="col-3">
				  <div class="form-group" id="custom-label-2">
					<input type="text" class="form-control" id="custom_error_val" name="custom_error_val" placeholder="Custom error text" value="" maxlength="200">
				  </div>
				</div>
				<div class="col-5">
					<div class="form-group">
					  <label for="sub_question">If Summation required?</label>
					  <input type="checkbox" class="form-control short-check" id="if_summation" name="if_summation" value="1" <?php if(isset($question_data[0]['if_summation'])){if($question_data[0]['if_summation']=="Yes"){ ?> checked="checked" <?php }  } ?> />
					</div>
					
					<div class="form-group">
					  <label for="sub_question">If file upload required with Short Answer Or Paragraph?</label>
					  <input type="checkbox" class="form-control short-check" id="input_file_required" name="input_file_required" value="1" <?php if(isset($question_data[0]['input_file_required'])){if($question_data[0]['input_file_required']=="Yes"){ ?> checked="checked" <?php }  } ?> />
					</div>
				 </div>
			</div>
			</div>

            <div id="textbox" class="border px-3 pt-3 mb-3" style="display: none">
              <div class="row">                     
             <!-- logical condition div start -->                     
                <div class="col-3">
                  <div class="form-group">
                     <select class="form-control" id="text_type0" name="text_type" onchange="open_condition_list(this.value,0, 'main')">
                        <option value="">--Select--</option>                                
                      </select>
                  </div>
                </div>
                <div class="col-3">
                  <div class="form-group">
                     <select class="form-control" id="condition_type0" name="condition_type" onchange="open_textbox(this.value,0)">
                        <option value="">--Select--</option>
                      </select>
                  </div>
                </div>
                <div class="col-2" id="custom-min">
                  <div class="form-group">
                    <input type="text" class="form-control" id="min0" name="min" placeholder="Number" value="<?php if(@$option_data[0]['min_value']!="") { echo $option_data[0]['min_value']; } ?>" maxlength="100" onkeypress="return isNumber(event)">
                  </div>
                </div>						
                <div class="col-2 max_div" id="custom-max">
                  <div class="form-group">
                    <input type="text" class="form-control" id="max_div" id="max0" name="max" placeholder="Max" value="<?php if(@$option_data[0]['max_value']!="") { echo $option_data[0]['max_value']; } ?>" maxlength="100" onkeypress="return isNumber(event)">
                  </div>
                </div>

				        <div class="col-3" id="custom-label">
                  <div class="form-group">
                    <input type="text" class="form-control" id="custom_error0" name="custom_error" placeholder="Custom error text" value="<?php if(!empty(@$option_data[0]['validation_label'])) { echo $option_data[0]['validation_label']; } ?>" maxlength="200">
                  </div>
                </div>	

            </div> 

      			<div class="row">
      				<div class="col-3 d-flex" id="apply_logical_question" style="display:none;">
      					<label for="sub_question">If Subquestion?</label>
      					<div class="form-group">								 
      						<input type="checkbox" class="form-control logical-ques" id="if_logical" name="if_logical" value="yes" title=""  <?php if(isset($option_data[0]['if_sub_question_checked'])){if($option_data[0]['if_sub_question_checked']=="Yes"){ ?> checked="checked" <?php }  } ?> />
      					</div>
                 <span id="if_logical_error" style="color: red"></span>
      				</div>
      				<div class="col-8" id="select_logical_question" style="display:none;">
      					<label for="sub_question">Select Subquestion <span style="color: red">*</span></label>
      					<div class="form-group">								 
      						<select id="questionlist2" name="parent_qus[]" class="form-control select2" multiple="multiple">
      						</select>
      					</div>
                 <span id="logical_question_error" style="color: red"></span>
      				</div>
      			</div>

         </div> 
        <!--Textbox div end Parent-->
				
				<!------ File Upload Start ----->
				<div id="upload_system" style="display: none">
          <div class="row">                     
          <!-- logical condition div start -->    
						<?php if(count($file_data) > 0){							
								foreach($file_data as $file_details){ ?>									
								 <div class="col-2">
									<label for="sub_question"><?php echo ucfirst($file_details['file_type_name']) ?></label>
									<div class="form-group">
										<input type="checkbox" class="form-control file-chk" id="img_upload0" name="img_upload[]" value="<?php echo $file_details['file_id'] ?>" style="margin-left: -43px;" />
									</div>
								</div>										
						<?php		}	
						} ?>          
					</div>
				</div>		
				<!------ File Upload End ----->	

          <div class="col-12">
            <div class="row">
              <div class="col-4 pl-0">
                <div class="form-group">
                  <label for="is_mandatory" class="mr-4">Is Mandatory?</label>
                  <input type="radio" id="is_mandatory" name="is_mandatory" value="1" 
                    <?php if(!empty($question_data[0]['is_mandatory']) && $question_data[0]['is_mandatory'] == 1) { echo 'checked'; } ?>>
                  <span for="yes">Yes</span>&nbsp;
                  <input type="radio" id="is_mandatory" name="is_mandatory" value="0" 
                    <?php if(!empty($question_data[0]['is_mandatory']) && $question_data[0]['is_mandatory'] == 0) { echo 'checked'; } else{ echo 'checked'; }?>>
                  <span for="no">No</span>
                </div>
              </div>

			<input type="hidden" id="no_of_input_fields" name="no_of_input_fields" class="form-control" value="0" onkeypress="return isNumber(event)">

            </div>
            <div class="form-group">
              <input type="hidden" class="form-control" id="prev" value="">
            </div>
            

             <?php if(isset($option_data) && count($option_data) >0 ) {?>
                  </br>
                 
                    <label for="options_table">Options Table</label>
                    <table id="option_table" class="table table-bordered table-hover" width="100%">
                      <thead>
                        <tr>
                          <th width="1%">Sr.</th>
                          <th width="10%">Option Id</th>
                          <?php if($question_data[0]['response_type_id'] == 'textbox') { ?>
                            <th width="10%">Labels</th>
                          <?php } else { ?>
                            <th width="10%">Options</th>
                          <?php } ?>
                          
                          <th width="10%">Display Order</th>
                          <th width="10%">Subquestion</th>
                          <th width="10%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($option_data as $key => $value) { ?>
                          <tr>
                            <td><?php echo $key+1;?></td>
                            <td><?php echo $value['ques_option_id'];?></td>
                            <td><?php echo $value['option'];?></td>
                            <td><?php echo $value['display_order'];?></td>
                            <td><?php echo $value['dependant_ques_id'];?></td>
                           <td>            
                              <a href="javascript:void(0);" id="edit"><i class="fas fa-pencil-alt" data-toggle="modal" data-target="#edit_Modal"></i></a>  &nbsp;      
                              <a href="<?php echo base_url();?>xAdmin/question/delete_option/<?php echo base64_encode($value['ques_option_id']).'/'.base64_encode($value['question_id']); ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><i class="far fa-trash-alt" title="Delete"></i></a> &nbsp;  
                           </td>
                          </tr>
                        <?php } ?> 
                      </tbody>
                    </table>
                    <hr>
                  <?php }?>
                

                <!-- Edit Option Modal -->
                <div class="modal fade" id="edit_Modal" role="dialog">
                  <div class="modal-dialog modal-lg">
                  
                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title" style="float: left;"  id="head_title"></h4>
                      </div>
                      
                      <form class="modal-content form-horizontal" method="post" action="#" name="edit_modal">

                        <div class="modal-body">

                          <div class="row">

                            <div class="col-12">
                              <div class="form-group">
                                <label for="label">Option Text</label>
                                <input type="text" class="form-control" id="label" name="label" placeholder="Enter option" value="" maxlength="100"/>
                              </div>
                            </div>
            
                            <div class="col-8">  
                                <div class="form-group">
                                  <label for="display_order">Display Order</label>
                                  <input type="text" class="form-control" id="display_order" name="display_order" placeholder="Enter Display Order" value="" />
                                </div>
                            </div>

                            <div class="col-8">  
                                <div class="form-group">
                                  <label for="subquestion">Subquestion</label>
                                  <select class="form-control" id="subquestion" name="subquestion">
                                    <option value=" ">Select</option>>
                                    <?php foreach ($sub_question as $key => $value) { ?>
                                        <option value="<?php echo $value['question_id']?>"><?php echo $value['question_text']?></option>
                                    <?php } ?>
                                 </select>
                                </div>
                            </div>

                            <div class="col-12">
                              <div class="form-group">
                                <input type="hidden" class="form-control" id="option_id" name="option_id" value="" />
                              </div>
                            </div>

                          </div><!--row-->
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary" name="submit" id="btn_save">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>



                  </form>
                    
                  </div>
                </div>
                <!-- Edit option Modal -->

          	  <div class="card-footer1">
                 <button type="submit" class="btn btn-primary" id="btn_save" name="save">Save & Add Next</button>
                 <button type="submit" class="btn btn-primary" id="btn_save_preview" name="save_preview">Save & Preview</button>
                 <button type="submit" class="btn btn-primary" id="btn_submit" name="submit">Submit</button>
                <a href="<?php echo base_url('xAdmin/question') ?>" class="btn btn-primary">Back</a> 
              </div>

    		      </div> <!-- /.col-12 -->		   
    	     </form>
    	    </div><!-- /.card-body -->
        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->
    </section>
</div>
<script type="text/javascript">
  var save_str = '';
  var action = '<?php //echo $action; ?>';
  var modal_index = 0;
  var option_cnt_index = 1;
  var display_order_count = 1;
  var opt =0;
  var plus_id = 0;
  $('#dependant_ques_tbldiv').hide();
  
  function get_question_listing(parent_id, survey_id)
  {
	var site_path = '<?php echo base_url(); ?>';   
    $.ajax({
      url: site_path+"xAdmin/question/get_question_list_edit",
      type: 'POST',
      data: {'ci_csrf_token':'',survey_id:survey_id, parent_id:parent_id},
      success: function(response){ 		
        $("#parent_question").html(response);
      }
    })  
  }
  
  function get_question(survey_id){

    var site_path = '<?php echo base_url(); ?>';
    var parent_question = $('#parent_question');
    parent_question.empty(); 

    var option_type = $('#option_type').val();
    
    if($("#if_logical").prop('checked') == true){    
     
      var if_logical = 'Yes';
      var mySelect = $('#questionlist2');   
      mySelect.empty();
      var attrValue = '';
    }
    else{
      var if_logical = 'No';
    }

    if($(".check-parent-box").prop('checked') == true){     
      var if_subquestion = 'Yes';
      var attrValue = $('.check-parent-box').attr('data-id');
      var mySelect = $('#parent_qus'+attrValue);    
      mySelect.empty()
    }
    else{
      var if_subquestion = 'No';
    }

  
    $.ajax({

      url: site_path+"xAdmin/question/get_question_list",
      type: 'POST',
      data: {'ci_csrf_token':'',survey_id:survey_id,option_type:option_type,if_logical:if_logical,if_subquestion:if_subquestion, question_id:''},
      success: function(data){
        var response = data.split('$$');

        parent_question.append("<option value=''>-- Select Question --</option>");
        if(response[0]){
          $.each(JSON.parse(response[0]), function(idx, obj) {
            parent_question.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
          });
        }
        if(response[1]){
          if(if_subquestion == 'Yes'){
            $('#if_subquestion_error'+attrValue).text('');
            $('.parent_qus_display'+attrValue).css("display", "block");
          }
          if(if_logical == 'Yes'){
            $('#if_logical_error').text('');
            $('#select_logical_question').css('display','block'); 
          }
          mySelect.append("<option value=''>--Select--</option>");
          $.each(JSON.parse(response[1]), function(idx, obj) {
            mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text +"</option>");
          });
        }
      }
    })
  }
  
  function get_section(survey_id){
    
    var site_path = '<?php echo base_url(); ?>';
    var mySelect = $('#section');
    mySelect.empty(); 

    $.ajax({

      url: site_path+"xAdmin/question/get_section",
      type: 'POST',
      data: {'ci_csrf_token':'',survey_id:survey_id},
      success: function(response){
        
        mySelect.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.section_id+"'>"+obj.section_name+"</option>");
          if(action == 'edit'){
            var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
            $('#section option[value='+section_id+']').attr('selected','selected');
          }
        });
      }
    })
  }
  
  function subsectionpopulate(section_id){  

	populateQuestion(section_id);
    var site_path = '<?php echo base_url(); ?>';
    var mySelect = $('#sub_section');
    mySelect.empty(); 
    $.ajax({
      url: site_path+"xAdmin/question/get_sub_section",
      type: 'POST',
      data: {'ci_csrf_token':'',section_id:section_id},
      success: function(response){
        //console.log(response);
        mySelect.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.section_id+"'>"+obj.section_name+"</option>");
          if(action == 'edit'){
            var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
            $('#section option[value='+section_id+']').attr('selected','selected');
          }
        });
      }
    })
  }
  
  function populateQuestion(section_id){
	  
	var site_path = '<?php echo base_url(); ?>';
    var mySelect = $('#parent_question');
	var survey_id = $("#survey").val();
	var sub_section_id = $("#sub_section").val();
    mySelect.empty(); 
    $.ajax({
      url: site_path+"xAdmin/question/open_question_list",
      type: 'POST',
      data: {'ci_csrf_token':'',section_id:section_id,survey_id:survey_id,sub_section_id:sub_section_id},
      success: function(response){
        //console.log(response);
        mySelect.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
      
        });
      }
    })
  }

  function change_option(option_type)
  {   
	  $("#text_type0 option[selected]").removeAttr("selected");   
	  $("#condition_type0 option[selected]").removeAttr("selected");
	  $('#condition_type0').val(''); 
	  $('#questionlist2').val('');
	  $("#custom-max-2").hide();
	  $('.checkbox').attr('checked', true);
	  $("#min0").val("");
	  $("#max_div").val("");
	  $("#custom_error0").val("");	
	
	  $("#questionlist2 option[selected]").removeAttr("selected");
    $("#custom-min").show();
    
    $("#custom-label").show();
    $("#custom-label-2").show();
	  $('#if_logical').prop('checked', false); 
	  $('#question_labels').prop('checked', false); 
	  
    document.getElementById('matrix_question').style.display = "none";
    
    if(option_type == 'radio' || option_type == 'checkbox' || option_type == 'single_select'){		
      document.getElementById('matrix-row').style.display = "none";
      document.getElementById('textbox').style.display = "none";
	    document.getElementById('radio').style.display = "block";	  
      $('#option_display_order0').val(display_order_count);
      $('#display_order_count_hidden').val(display_order_count);
	    $( "div" ).remove( "#radio-chk" );
    }
    else if(option_type == 'textbox' || option_type == 'textarea'){
	    document.getElementById('matrix-row').style.display = "block";	
      document.getElementById('textbox').style.display = "block";
      document.getElementById('radio').style.display = "none";	  
	    document.getElementById('apply_logical_question').style.display = "block"; 
	    //document.getElementById('select_logical_question').style.display = "block";	  
      open_validation_list(option_type, 'main');
    } 
    else if(option_type == 'upload_file'){
  		$('#question_labels').prop('checked', false); 
  		document.getElementById('matrix-row').style.display = "none";
  		document.getElementById('textbox').style.display = "none";
  		document.getElementById('radio').style.display = "none";
  		document.getElementById('upload_system').style.display = "block";
  	}
  	else if(option_type == "")
  	{
  		document.getElementById('matrix-row').style.display = "none";	
  		document.getElementById('textbox').style.display = "none";
  		document.getElementById('radio').style.display = "none";	  
  		document.getElementById('apply_logical_question').style.display = "none"; 
  		document.getElementById('select_logical_question').style.display = "none";
  	}
    showhidenumber(option_type);
  }

   function open_validation_list(option_type, form_type)
   {    //alert(option_type);
		var site_path = '<?php echo base_url(); ?>';
		if(form_type == 'main'){
		  var mySelect = $('#text_type0');
		  var mySelect_val = $('#text_type_val');
		}
		else{
		  var mySelect = $('#modal_text_type0');
		}
		mySelect.empty(); 
		mySelect_val.empty(); 

		$.ajax({

		  url: site_path+"xAdmin/question/get_validation_list",
		  type: 'POST',
		  data: {'ci_csrf_token':'',option_type:option_type,mode:'add'},
		  success: function(response)
		  {		
			mySelect.append("<option value=''>Select</option>");
			mySelect_val.append("<option value=''>Select</option>");
			$.each(JSON.parse(response), function(idx, obj) {
			  mySelect.append("<option value='"+obj.validation_id+"'>"+obj.validation_type +"</option>");			  
			});			
			$.each(JSON.parse(response), function(idx, obj) {
			  mySelect_val.append("<option value='"+obj.validation_id+"'>"+obj.validation_type +"</option>");			  
			});
		  }
		})
	}

  function open_condition_list(validation_id, id, form_type)
  {

    if(validation_id == 1)
	{ 
      $("#min0").attr("placeholder", "Number");
      $("#modal_min0").attr("placeholder", "Pattern");
	    $("#min_val").attr("placeholder", "Number");
      $("#custom-max").hide();
      $("#custom-label").show();
      $("#custom-label-2").show();
      $("#modal_custom-max").hide();
    } 
	else if(validation_id == 9)
	{
      $("#min0").attr("placeholder", "Pattern");
	    $("#min_val").attr("placeholder", "Pattern");
      $("#modal_min0").attr("placeholder", "Pattern");
      $("#custom-max").hide();
      $("#custom-label").show();
      $("#custom-label-2").show();
      $("#modal_custom-max").hide();
    } 
	else if(validation_id == 2)
	{
      $("#min0").attr("placeholder", "Text");
	    $("#min_val").attr("placeholder", "Text");
      $("#modal_min0").attr("placeholder", "Text");
      $("#custom-label").show();
      $("#custom-label-2").show();
	    $("#custom-max").hide();
      $("#modal_custom-max").hide();
    } 
	else if(validation_id == 4)
	{
      $("#custom-label").hide();
      $("#custom-label-2").hide();
	    $("#custom-max").hide();
      $("#modal_custom-max").hide();
	    $("#min0").attr("placeholder", "Digit");
	    $("#min_val").attr("placeholder", "Digit");
    } 
	else 
	{
      $("#custom-label").show();
      $("#custom-label-2").show();
      $("#min0").attr("placeholder", "Number");
	    $("#min_val").attr("placeholder", "Number");
      $("#modal_min0").attr("placeholder", "Number");
    }
    
    var site_path = '<?php echo base_url(); ?>';

    if(form_type == 'main'){
      var mySelect = $('#condition_type'+id);
	  var mySelect_val = $('#condition_type_val');
	  
    }
    else{
      var mySelect = $('#modal_condition_type'+id);
    }
    
    mySelect.empty(); 
	  mySelect_val.empty();
    $.ajax({

      url: site_path+"xAdmin/question/get_condition",
      type: 'POST',
      data: {'ci_csrf_token':'',validation_id:validation_id},
      success: function(response){

        $("#min_val").val('');
        $("#min0").val('');
        
        $("#custom_error0").val('');
        $("#custom_error_val").val('');

        mySelect.append("<option value=''>Select</option>");
		    mySelect_val.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.sub_validation_id+"'>"+obj.validation_sb_type +"</option>");
        });
		    $.each(JSON.parse(response), function(idx, obj) {
          mySelect_val.append("<option value='"+obj.sub_validation_id+"'>"+obj.validation_sb_type +"</option>");
        });
      }
    })

  }

   function showhidenumber(option_type)
  {
    //alert(option_type);
    if(option_type == 'textbox' || option_type == 'textarea')
	{          
      $("#custom-min").show();
      $("#custom-max").hide();
      $("#modal_custom-max").hide();
    }
  }


  function open_textbox(validation_id, id){ //alert($("#condition_type0").val());

    if(validation_id == 7 || validation_id == 8){ 
      $("#custom-min").show();
      $("#custom-min-2").show();
      $("#custom-max").show();
	    $("#custom-max-2").show();
      $("#modal_custom-max").show();
      $("#custom-label").show();
      $("#custom-label-2").show();    
      $("#min0").attr("placeholder", "Min");
      $("#min_val").attr("placeholder", "Min");
      $("#custom-max").attr("placeholder", "Max");

      $("#modal-min").show();
      $("#modal-max").show();
      $("#modal-label").show();    
      $("#modal_min0").attr("placeholder", "Min");
      $("#modal_max_div").attr("placeholder", "Max");
    } 
    else if($("#condition_type0").val() == 14 || $("#condition_type0").val() == 15)
    {
      $("#custom-min").show();
      $("#custom-min-2").show();
      $("#custom-max").hide();
      $("#custom-max-2").hide();
      $("#custom-label").hide();
      $("#custom-label-2").hide();
      $("#min0").attr("placeholder", "Number"); 
      $("#min_val").attr("placeholder", "Number");
    }
  	else if($("#condition_type0").val() == 16)
  	{
  		$("#custom-min").hide();
  		$("#custom-max").hide();
  		$("#custom-max-2").hide();
      $("#custom-label").hide();
      $("#min0").attr("placeholder", "Number");	
      $("#min_val").attr("placeholder", "Number");
  	}
    else if($("#condition_type_val").val() == 16)
    {
      $("#custom-min-2").hide();
      $("#custom-max-2").hide();
      $("#custom-label-2").hide();

      $("#min0").attr("placeholder", "Number"); 
      $("#min_val").attr("placeholder", "Number");
    } 	
    else	
	  {     
      $("#custom-min").show();
      $("#custom-min-2").show();
      $("#custom-max").hide();
      $("#modal_custom-max").hide();
	    $("#custom-max-2").hide();	

      $("#custom-label").show();
      $("#custom-label-2").show();    

      $("#modal-min").show();
      $("#modal-max").hide();
	    
      $("#min0").attr("placeholder", "Number");
      $("#min_val").attr("placeholder", "Number");
    }

  }
  

  $(document).ready(function() {
    var newid = 1;
    var main_ques = 0;
    var sub_ques = 0;
    var str = '';
    var save_str = '';
    var resultArr = [];	
	 $('.select2').select2();
    var dependent_ques_options = [];
    var dependant_ques_display_order = [];

    if(action == 'edit'){
      var survey_id = $('#survey').val(); 
      get_section(survey_id);
    }

    var error_cnt = 0;
    $('#btn_submit, #btn_save, #btn_save_preview').click(function(){

    	var salutation_check = $(".salutation-box").is(':checked');
        if(salutation_check){
	      if($('#salutation_input').val() == ''){
	        $('#salutation_error').text('Please enter Salutation');
	        return false;
	      }
	      else{
	        $('#salutation_error').text('');
	        return true;
	      }	
	 	}

      var option_type = $('#option_type').val();
      //alert(option_type);
      if(option_type == 'radio' || option_type == 'checkbox' || option_type == 'single_select'){
        //alert('if');
        $.each($(".check-parent-box:checked"), function(){
          var data_id = $(this).attr('data-id');
          //alert(data_id);
          var sub_q_check = $("#sub_question_radio"+data_id).is(':checked');
          //alert(sub_q_check);
          if(sub_q_check){
            if($('#parent_qus'+data_id).val() == ''){
              $('#parent_qus_error'+data_id).text('Please select Subquestion');
              error_cnt++;
        
              //return false;
            }
            else{
              $('#parent_qus_error'+data_id).text('');
              error_cnt = 0;
              
              //return true;
            }
          }

        });
        //alert(error_cnt);
          if(error_cnt == 0){
            
            return true;
          }
          else{
           /* $('#btn_submit').prop('disabled','false'); 
            $('#btn_save').prop('disabled','false');
            $('#btn_save_preview').prop('disabled','false');
            */
            return false;
          }

      }
      else if(option_type == 'textbox' || option_type == 'textarea'){

        var logical_check = $(".logical-ques").is(':checked');
        if(logical_check){
          if($('#questionlist2').val() == ''){
            $('#logical_question_error').text('Please select Subquestion');
            return false;
          }
          else{
            $('#logical_question_error').text('');
            return true;
          }
        }
      }
    });

    $('#btn_add_radio').click(function(){
      var display_order_count_hidden = $("#display_order_count_hidden").val();
      //var option_display_order_radio = $('.option_display_order'+main_ques_radio).val();
      var display_order_index = parseInt(display_order_count_hidden)+1;

      var repeatable_radio = '<div class="row" id="radio-chk"><div class="col-3"><input type="text" class="form-control primary-title title_cls'+newid+'" id="title'+newid+'" name="title[]" placeholder="Enter Title" required /></div><div class="col-1"><input type="text" class="form-control option_display_order_cls" id="option_display_order'+newid+'" name="option_display_order[]" placeholder="Enter Display Order" value="'+display_order_index+'" /></div><div class="col-2"><div class="form-group" style=""><input type="checkbox" class="form-control check-parent-box" id="sub_question_radio'+newid+'" name="sub_question_yes['+newid+']" value="1" data-id="'+newid+'" /><input type="hidden" name="sub_q[]" id="sub_q_'+newid+'" value="0" /></div><span id="if_subquestion_error'+newid+'" style="color: red;"></span></div><div class="col-5 parent_qus_display'+newid+'" id="select-'+newid+'" style="display:none"><div class="form-group"><select name="parent_qus_chk['+newid+'][]" id="parent_qus'+newid+'" class="form-control select2" multiple="multiple"></select></div><span id="parent_qus_error'+newid+'" style="color: red"></span></div></div>';
      $('.select2').select2();

		$('#repeatable_radio').append(repeatable_radio);
        $("#display_order_count_hidden").val(display_order_index);
        main_ques++;
        newid++;      
    });// add radio add more (parent)
	
	
	$('#btn_add_label').click(function(){
      var display_order_label_hidden = $("#display_order_label_hidden").val();
      //var option_display_order_radio = $('.option_display_order'+main_ques_radio).val();
      var display_order_index = parseInt(display_order_label_hidden)+1;

      var repeatable_label_textbox = '<div class="row mb-3" id="textbox-label"><div class="col-6"><input type="text" class="form-control  title_cls'+newid+'" id="text_label'+newid+'" name="text_label[]" placeholder="Enter Label" value="" /></div><div class="col-1"><input type="text" class="form-control label_display_order_cls" id="label_display_order'+newid+'" name="label_display_order[]" placeholder="Enter Display Order" value="'+display_order_index+'" /></div></div>';
    
      var classArr = ["title_cls"+main_ques];
	  
	   $('#repeatable_label_textbox').append(repeatable_label_textbox);

        $("#display_order_label_hidden").val(display_order_index);

        main_ques++;
        newid++;	
     
    });// add radio add more (parent)
	
	$(document).on('click', '.remove-added-box', function() {
    var displayOrder = $("#display_order_count_hidden").val();
    var cnts = displayOrder - 1;
    $("#display_order_count_hidden").val(cnts);
    
		$('div#radio-chk').last().remove();		
  });
	
	$(document).on('click', '.remove-select-box', function() {
		$('div#select-radio-row').last().remove(); 
	});

	$(document).on('click', '#btn_remove_radio_2', function() {
		var displayOrder = $("#display_order_label_hidden").val();
		var cnts = displayOrder - 1;
		$("#display_order_label_hidden").val(cnts);
		
		$('div#textbox-label').last().remove(); 
	});
	
	//$(".check_class").click(function() {
	$(document).on('click', '#if_summation', function() {
	  $("#input_file_required").prop("checked", false); //uncheck all checkboxes
	});
	
	$(document).on('click', '#input_file_required', function() { 
	  $("#if_summation").prop("checked", false); //uncheck all checkboxes
	});
	
	
    var table  = $('#option_table').DataTable({

      "columnDefs": [                 
            {
              "targets": [1],  
              "visible": false
            }
        ],
    });

    $('#option_table').on( 'click', '#edit', function (e) {
      e.preventDefault();     
      var data = table.row( $(this).parents('tr') ).data();
        $('#option_id').val(data[1]);
      $('#label').val(data[2]);
      $('#display_order').val(data[3]);
      if(data[4]!= ''){
        $('select[name^="subquestion"] option[value="'+data[4]+'"]').attr("selected","selected");
      }
      else{
        $('select[name^="subquestion"] option:selected').attr("selected", null);
      }
    });
	
    $('#btn_save').click(function(){
      //alert('**');
      var site_path = '<?php echo base_url(); ?>';
      var label = $('#label').val();
      var display_order = $('#display_order').val();
      var subquestion_id = $('#subquestion').val();
      var option_id = $('#option_id').val();
      $.ajax({
          url: site_path+"xAdmin/question/edit_option",
          type: 'POST',
          data: {'ci_csrf_token':'',label:label,display_order:display_order,option_id:option_id, subquestion_id:subquestion_id},
          success: function(response){
           if(response == 1){
            var msg = "Option Edited Successfully";
            swal(msg, "", "success");
            $('#edit_Modal').modal('toggle'); 
            location.reload();
           }
          }
      })
    });
	
	<?php  if($action != 'edit'){ ?>
		get_question(<?php echo $this->session->userdata('prev_survey_id'); ?>);
	<?php } ?>
	
	$('.primary-title').each(function() {
        $(this).rules("add", 
		{
			required: true,
			messages: {
				required: "This field is required",
			}
		});
    });
	
	
  });//ready
  
  /****************Store Multidimention Values***********************/
	
	$(document).on('click', '.check-parent-box', function() {	

		var surveyid = $("#survey").val();
		//alert(surveyid);
		var section = $("#section").val();
		var sub_section_id = $("#sub_section").val();
		var attrValue = $(this).attr('data-id');
		var site_path = '<?php echo base_url(); ?>';
	
		if($("#sub_question_radio"+attrValue).prop('checked') == true){		
    	

			$("#sub_q_"+attrValue).val(1);
			var mySelect = $('#parent_qus'+attrValue);		
			mySelect.empty(); 
      if(surveyid == ''){
        $('#if_subquestion_error'+attrValue).text('Please select Survey');
      }//if 
      else{
        $('.parent_qus_display'+attrValue).css("display", "block");
        $('#if_subquestion_error'+attrValue).text('');
  			$.ajax({
  			  url: site_path+"xAdmin/question/get_question_list_choice",
  			  type: 'POST',
  			  data: {'ci_csrf_token':'',survey_id:surveyid},
  			  success: function(response){				
  				mySelect.append("<option value=''>--Select--</option>");
  				$.each(JSON.parse(response), function(idx, obj) {
  				  mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text +"</option>");
  				});
  			  }
  			});	
      }//else		
		} 
    else {				
			$("#sub_q_"+attrValue).val(0);
      $('.parent_qus_display'+attrValue).css("display", "none");
			var mySelect = $('#parent_qus'+attrValue);				
			mySelect.empty(); 
			mySelect.append("<option value=''>--Select--</option>");
		}
	}); 
	
	$(document).on('click', '.salutation-box', function() {
		
		if($("#salutation_box").prop('checked') == true){	
			$('#salutation-box').css('display','block');
		} else {
			$('#salutation-box').css('display','none');
		}
		
	});
	
	$(document).on('click', '.label-add', function() 
	{		
		if($("#question_labels").prop('checked') == true){	
			$('#textbox').css('display','none');
			$('#matrix_question').css('display','block');
		} else {
			$('#textbox').css('display','block');
			$('#matrix_question').css('display','none');
		}
		
	});
	
	
	$(document).on('click', '.required-parent-box', function(){
		if($("#if_parent_required").prop('checked') == true){
			$('#parent-question-box').css('display','block');
		} 
		else
		{
			$('#parent-question-box').css('display','none');
		}
	});	
	
	$(document).on('click', '.required-file', function(){
		if($("#if_file_required").prop('checked') == true){
			$('#file-upload-label').css('display','block');
		} 
		else
		{
			$('#file-upload-label').css('display','none');
		}
	});	
	
	$(document).on('click', '.logical-ques', function() {
		var surveyid = $("#survey").val();
		var section = $("#section").val();
		var sub_section_id = $("#sub_section").val();
		var attrValue = $(this).attr('data-id');
		var site_path = '<?php echo base_url(); ?>';
		$('.select2').select2();

		if($("#if_logical").prop('checked') == true){			
      //alert('---');
			//$("#sub_q_"+attrValue).val(1);
			var mySelect = $('#questionlist2');		
			mySelect.empty(); 
      if(surveyid == ''){
        $('#if_logical_error').text('Please select Survey');
      }//if 
      else{
        $('#if_logical_error').text('');
        $('#select_logical_question').css('display','block');
  			$.ajax({

  			  url: site_path+"xAdmin/question/get_question_list_choice",
  			  type: 'POST',
  			  data: {'ci_csrf_token':'',section_id:section,survey_id:surveyid,sub_section_id:sub_section_id},
  			  success: function(response){
  				//alert(response);
  				mySelect.append("<option value=''>--Select--</option>");
  				$.each(JSON.parse(response), function(idx, obj) {
  				  mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text +"</option>");
  				});
  			  }
  			});			
      }
		} 
    else {	
      $('#select_logical_question').css('display','none');
			var mySelect = $('#questionlist2');		
			mySelect.empty(); 
			mySelect.append("<option value=''>--Select--</option>");
		}
	}); 
  
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  
</script>

<script type="text/javascript">
//$("label").removeAttr("style");  
  $(document ).ready( function() 
  {	 
	$("label").removeAttr("style");		
    $("form[name='question_form']").validate({		
	  rules: {
			 survey: {
               required: true,
             },
			 parent_question:{
				required: function() { 
			   if($('#if_parent_required').is(':checked'))
			   { 							
				   return true;
				   
				}  else {
					 return false; 
				 }	
			   } 
			 },
             section: {
               required: true,
             },
			 question_text: {
               required: true,
			   maxlength:500
             },
			 salutation_input:{
				 required: function() { 
				   if($('#salutation_box').is(':checked'))
				   { 
						if($("input#salutation_input").val() == '')
						{ 
						   return true;
						}  	
					  // return true;
					}  else {
						 return false; 
					 }	
				   } 
			 },
			 /*file_label: {
               required: function() { 
               if($('#if_file_required').is(':checked'))
			   { 
				   return true;
				} else 
				{
					return false;
				}					
               }
			},*/
			 option_type: {
               required: true,
             },
			 "text_label[]" : {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#question_labels').is(':checked')){ 
				   return true;
				}  else {
					 return false; 
				 }	
               }
			},
			condition_type_val: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#text_type_val").val() != '')){ 
				   return true;
				}  
               }
			},
			min_val: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#question_labels').is(':checked'))
			   {	
					if($("#text_type_val").val()!="" && $("#condition_type_val").val() !="")
					{
						return true;
					}
					else
					{
						return false;
					}	
				   //return false;
				}
				else 
				{
					return false;
				}	
               }
			},
			max_val: {
               required: function() { 
               if($("#condition_type_val").val() == '7' || $("#condition_type_val").val() == '7'){ 
				   return true;
				}  
               }
			},			
			condition_type: {
               required: function() {
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#text_type0").val() != '')){ 
				   return true;
				}  
               }
			},
			min: {
               required: function() {  
				   if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#condition_type0").val() != ''))
				   {					   
					   if($("#condition_type0").val() == 16)
					   {
						   return false;
					   }
					   else
					   {
						   return true; 
					   }
					  
					}
					else if($("#text_type0").val() == '4')
					{
						return true;
					}
					else
					{ 
						return false;
					}  
			   },
		   digits:function() {
				   if($("#text_type0").val() == 1 || $("#text_type0").val() == 4)
				   {
					   return true;
				   }
				}
			},			
			max: {
               required: function() { 
				   if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#condition_type0").val() == '7' || $("#condition_type0").val() == '8'))
				    { 
					   return true;
					}					
					else
					{ 
						return false;
					} 				
               }
			},
			/*custom_error: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("input[name='min']").val() != '')){ 
				   return true;
				} else if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#condition_type0").val() == '7' || $("#condition_type0").val() == '8')){
					return true;
				}
				else if($("#option_type").val() == 'textbox' && $("#condition_type0").val() == '16')
				{ 
				   return true;
				}
				else
				{ 
				return false;
				}  
				
               }
			},*/			
			parent_qus: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#if_logical').is(':checked')){ 
				   return true;
				}  
               }
			},
			"title[]" : {
               required: function() { 
               if(($("#option_type").val() == 'radio' ||  $("#option_type").val() == 'checkbox' ||  $("#option_type").val() == 'single_select'))
			   { 
				   return true;
				} 
				else
				{
					return false;
				}	
               },
			   minlength: 1
			}
	  },
	  messages: {
			 survey: {
                required: "This field is required"
             },
			 parent_question: {
                required: "This field is required"
             },
             section: {
               required: "This field is required"
             },
			 question_text: {
               required: "This field is required"
             },
			 salutation_input: {
               required: "This field is required"
             },
			 file_label: {
               required: "This field is required"
             },
			 option_type: {
               required: "This field is required"
             },
			"text_label[]": {
               required: "This field is required"
             },
			 condition_type_val: {
               required: "This field is required"
             },
			 min_val: {
               required: "This field is required"
             },
			min_val: {
               required: "This field is required"
             },
			condition_type: {
               required: "This field is required"
             },
			min: {
               required: "This field is required"
             },
			max: {
               required: "This field is required"
             },
			custom_error: {
               required: "This field is required"
             },
			 /*if_logical: {
               required: "This field is required"
             },*/
			 parent_qus: {
               required: "This field is required"
             },
			 "title[]": {
				required: "This field is required"		  
        }
	  },
	  submitHandler: function(form) {
	  	$('#btn_submit').prop('disabled','true'); 
        $('#btn_save').prop('disabled','true');
        $('#btn_save_preview').prop('disabled','true');
        form.submit();
      } 
	});
  
  });


  
</script>

<script>
  function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
  function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>

