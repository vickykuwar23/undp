<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey'); ?>">Survey List</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	  
         <div class="card ">
			<div class="card-header">
				<a href="<?php echo base_url('xAdmin/question/add') ?>" class="btn btn-info btn-sm float-right" ><i class="fa fa-plus"></i> Add Question</a>                        
			</div>
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover" width="100%">
               <thead>
                  <tr>
                    <th width="5%">Sr.</th>
					          <th width="60%">Question Text</th>
                    <th>Response Type</th>
                    <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     $i=1;					
                     foreach($question_list as $info)  { ?>

					         <tr>
         						    <td width="5%"><?php echo $i; ?></td>
                        <td width="60%"><?php echo $info['question_text'] ; ?></td>
         						    <td width="10%"><?php echo $info['response_type_id'] ; ?></td>   
                        <?php /*?><td><?php echo $info['mandatory']; ?></td> 
                           <td><?php echo $info['cnt']; ?></td><?php */?>
         						    <td width="10%">						
         						   	  <a href="<?php echo base_url();?>xAdmin/question/edit/<?php echo base64_encode($info['question_id']); ?>" title="edit"><i class="fas fa-edit"></i></a> &nbsp; <!-- <i class="fas fa-pencil-alt"></i> -->  
                          <a href="<?php echo base_url();?>xAdmin/question/delete/<?php echo base64_encode($info['question_id']); ?>" onclick="return confirm_delete(this,event,'Do you really want to delete this record ?', <?php echo $info['question_id']; ?>, <?php echo $info['survey_id']; ?>)"><i class="far fa-trash-alt" title="Delete"></i></a> &nbsp;  
                          <a href="<?php echo base_url();?>xAdmin/question/preview/<?php echo $info['question_id']; ?>/view" title="view"><i class="fas fa-eye"></i></a> &nbsp;
                        </td>
         					</tr>
                     <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	$("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
});

 function confirm_delete(ref,evt,msg,question_id,survey_id)
  {

    var msg = msg || false;
    evt.preventDefault();  

    swal({
        title: "Are you sure ?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
          //window.location = $(ref).attr('href');
          $.ajax({
            url: site_path+"xAdmin/question/check_status",
            type: 'POST',
            data: {'ci_csrf_token':'',question_id:question_id,survey_id:survey_id},
            success: function(response){    
              if(response != ''){
                 swal(response, "", "warning");
              }
              else{
                window.location = $(ref).attr('href');
              }
            }
          })  
        }
    });

  } 
</script>