<style type="text/css">
  form .error {
    color: red;
	font-size: 14px;
  }
  .form-control error{
	 font-size: 1rem;
	color:#000;	
  }
  .swal2-checkbox
  {
	  display:none!important;
  }

  /*.multiselect_dropdown {
		width: 200px;
	}*/
</style>
<?php $question_list = $this->master_model->getRecords("survey_question_master"); 
	
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Question</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Question</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <?php $this->load->view('admin/layouts/admin_alert'); ?>
    	<div class="card-body">          
            <form method="post" id="question_form" name="question_form" role="form" enctype="multipart/form-data">			
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				   
      				<input type="hidden" id="question_id_hidden" value="<?php echo $question_id;?>">
                    <div class="row">  
                      <div class="col-md-6 col-sm-12 form-group">
                        <label for="survey">Survey <span style="color: red">*</span></label>
                        <select class="form-control" id="survey" name="survey" onchange="get_question(this.value)">
                          <option value="">Select Survey</option>
                          <?php foreach ($survey_data as $key => $value) { 
                              if($action == 'add') { 
                                if(!empty($this->session->userdata('prev_survey_id')) && $this->session->userdata('prev_survey_id') == $value['survey_id']) { 
                                  $status = 'selected="selected"'; 
                                } 
                                else{
                                 $status = "";
                                }
                              }
                              else{ 
                                if (!empty($question_data[0]['survey_id']) && $question_data[0]['survey_id'] == $value['survey_id']) { 
                                  $status = 'selected="selected"'; 
                                }
                                else{
                                 $status = "";
                                } 
                              }
                            ?>
                            <option value="<?php echo $value['survey_id']; ?>" <?php echo $status; ?>><?php echo $value['title']; ?></option>
                          <?php } ?>
                        </select>
                        <span style="color: red"><?php echo form_error('survey'); ?></span>
                      </div>
                    </div>
					<div class="row">
                    <div class="col-md-6 col-sm-12 form-group">
                      <label for="description">Question Text <span style="color: red">*</span></label>
                        <textarea class="form-control" id="question_text" name="question_text" placeholder="Enter Text" maxlength="500" rows="2" /><?php if(!empty($question_data[0]['question_text'])) { echo $question_data[0]['question_text']; } ?></textarea>
                        <span style="color: red"><?php echo form_error('question_text'); ?></span>
                    </div>
                    </div>
					<div class="row">
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="description">Question Help Text </label>
							<!--<input class="form-control" id="help_label" name="help_label" placeholder="Enter Text" value="<?php //if(!empty($question_data[0]['help_label'])) { echo $question_data[0]['help_label']; } ?>" />-->
							<textarea class="form-control" id="help_label" name="help_label" placeholder="Enter Question Help Text"  rows="2" /><?php if(!empty($question_data[0]['help_label'])) { echo $question_data[0]['help_label']; } ?></textarea>
						</div>
					</div>
					 <div class="row">
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="salutation">Select Parent Question</label>
						  <input type="checkbox" class="form-control required-parent-box" id="if_parent_required" name="if_parent_required" title="" value="1" style="width: 20%;" <?php if(@$question_data[0]['parent_question_id'] > 0){ ?> checked="checked" <?php } ?>/>         
						</div>
					  </div>
                    <div class="row" id="parent-question-box" style="display:none;">  
                      <div class="col-md-6 col-sm-12 form-group">
                        <label for="parent_question"></label>
                        <select class="form-control" id="parent_question" name="parent_question" >                         
						    <option value="">Select Parent Question</option> 
                        </select>
                      </div>
                    </div>
					<div class="row">
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="salutation">If Salutation?</label>
						  <input type="checkbox" class="form-control salutation-box" id="salutation_box" name="salutation_box" title="" value="1" style="width: 20%;" <?php if(@$question_data[0]['salutation']!=""){ ?> checked="checked" <?php } ?>/>         
						</div>
					</div>

					<div class="row" id="salutation-box" style="display:none;">
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="description">Enter Salutation <span style="color: red">*</span></label>
						  <input type="text" class="form-control " id="salutation_input" name="salutation_input" title="" value="<?php if(!empty($question_data[0]['salutation'])) { echo $question_data[0]['salutation']; } ?>" /> 
							<p class="note">Note: Salutation add with pipe (|) separate values.</p>
							<span id="salutation_error" style="color: red"></span>	
						</div>
						
					</div> 
					<div class="row">
						<div class="col-md-3 col-sm-12 form-group">
							<label for="salutation">Upload File</label>
							<input type="checkbox" class="form-control required-file" id="if_file_required" name="if_file_required" title="" value="1" style="width: 20%;" <?php if(@$question_data[0]['if_file_required']== 'Yes') { ?> checked="checked" <?php } ?> />  &nbsp;
						</div>
						<div class="col-md-6 col-sm-12 form-group" id="file-upload-label" style="display:none;">
						  <label for="salutation">File Upload Label Name </label>
						  &nbsp;<input type="text" class="form-control" id="file_label" name="file_label" title="File Upload Label Name" value="<?php if(!empty($question_data[0]['file_label'])) { echo $question_data[0]['file_label']; } ?>" style="width: 60%;" /> 
						</div>						
					</div>						
	                <div class="row">  
						<div class="col-md-6 col-sm-12 form-group">
						  <label for="option_type">Option Type <span style="color: red">*</span></label>
						  <select class="form-control" id="option_type" name="option_type" onchange="change_option(this.value)">
							<option value="">Select Option Type</option>
							<?php foreach ($option_type as $key => $value) { ?>
							  <option value="<?php echo $value['input_type']; ?>" <?php if (!empty($question_data[0]['response_type_id']) && $question_data[0]['response_type_id'] == $value['input_type']){ echo 'selected="selected"'; }?>><?php echo $value['input_category']; ?></option>
							  <!-- <option value="<?php //echo $value['input_type']; ?>" ><?php //echo $value['input_category']; ?></option> -->
							<?php } ?>
						  </select>
						  <span style="color: red"><?php echo form_error('option_type'); ?></span>
						</div>
					</div>					
					<div  id="matrix-row" style="display:none;"> 
						<div class="row"> 
							<div class="col-md-6 col-sm-12 form-group">
								<label for="sub_question">Input Fields more than 1?</label>
								<input type="checkbox" class="form-control label-add" id="question_labels" name="question_labels" title="" value="1" />
							</div>
						</div>	
					</div>	
				

				<!-- Checkbox div start -->
				
		        <div id="radio"  class="border px-3 pt-3 mb-3" style="display: none">
		        	<div class="row">
			        	<div class="col-3">
			            	<label for="options">Add Options <span style="color: red">*</span></label>
			            </div>
			            <div class="col-1">
			            	<label for="option_display_order">Order</label>
			            </div>
			            <div class="col-2">
			            	<label for="sub_question">If Subquestion?</label>
			            </div>
			            <div class="col-5" id="select-0">
			            	<label for="sub_question">Select Question <span style="color: red">*</span></label>
			            </div>
			            <div class="col-1">
			            	<label></label>
			            </div>
			        </div>

		        <?php 
				$id_count = '';
				if(sizeof($option_data) > 0 && @$question_data[0]['response_type_id'] != 'textbox' || @$question_data[0]['response_type_id'] != 'textarea') {
					if(@$question_data[0]['response_type_id'] == 'radio' || @$question_data[0]['response_type_id'] == 'checkbox' || @$question_data[0]['response_type_id'] == 'single_select'){ ?>
						

					<?php	 //echo "<pre>";print_r($option_data);
			        	  $id_count = sizeof($option_data);
			        	  //$json_data = array(); 
						  foreach ($option_data as $key => $value) { ?>
			                <div class="row" id="remove-multiple-box-<?php echo @$value['ques_option_id']; ?>">
				                <div class="col-3">
				                    <div class="form-group"> 
			                     	 	<input type="text" class="form-control primary-title title_cls<?php echo $key;?>" id="title<?php echo $key;?>" name="title[]" placeholder="Enter Title"  value="<?php echo $value['option'];?>" maxlength="200" />
			                    	</div>
			                    </div>
			                  	<div class="col-1">
			                    	<div class="form-group">
			                      		<input type="text" class="form-control allow-numeric option_display_order_cls" id="option_display_order<?php echo $key;?>" name="option_display_order[]" placeholder="Enter Display Order" value="<?php echo $value['display_order'];?>" maxlength="5" />
			                   	 	</div>
			                  	</div>
			                  	<div class="col-2">
			                    	<div class="form-group">
				                     	 <?php //echo $value['if_sub_question_checked']; ?>
				                      	<input type="checkbox" class="form-control check-parent-box if_subques_cls" id="sub_question_radio<?php echo $key;?>" name="sub_question_yes[<?php echo $key;?>]" title="Add Sub Question" value=" <?php echo $key;?>" data-id="<?php echo $key;?>" <?php 
				                      	if($value['if_sub_question_checked'] == 'Yes') { echo 'checked=checked'; } ?> />
								      	<input type="hidden" name="sub_q[]" id="sub_q_<?php echo $key;?>" <?php 
				                     	 if($value['if_sub_question_checked'] == 'Yes') {  ?> value="1" <?php } else { ?> value="0" <?php } ?> />
								     	<input type="hidden" name="" id="option_id_<?php echo $key;?>" value="<?php echo $value['ques_option_id'];?>" />          
			                   	    </div>
			                   	    <span id="if_subquestion_error<?php echo $key;?>" style="color: red;"></span>
			                  	</div>

			                <?php
			                	
				                if($value['if_sub_question_checked'] == 'Yes'){
				                	
				                	$where = array('question_id'=>$question_data[0]['question_id'], 'ques_option_id'=> $value['ques_option_id'], 'is_deleted'=>0);

									$dependant_question_data = $this->master_model->getRecords("survey_option_dependent", $where);

									$json_data[$key] = $dependant_question_data;

								}
								//print_r($json_data);
								//echo $json_array;
								
							?>
							
							<div class="col-5 parent_qus_display<?php echo $key;?>" id="select-0" style="display: block;">
								<div class="form-group">
									<select name="parent_qus_chk[<?php echo $key; ?>][]"  id="parent_qus<?php echo $key;?>"  class="form-control multiselect_dropdown parent_ques_class select2" multiple  style="display: block;"> 
										<option value="" ></option>
									</select>
								</div>
								 <span id="parent_qus_error<?php echo $key;?>" style="color: red"></span>
							</div>

							<div class="col-1">
								<div class="form-group">
									<a href="javascript:void(0);" onClick="return delete_kr(<?php echo @$value['ques_option_id']?>,'radio');" id="btn_delete_label" class="class-cnt" data-id="<?php echo @$value['ques_option_id']?>" ><i class="btn btn-primary fas fa-times-circle"></i></a>
								</div>
							</div>

			                </div>
			            <?php } 
			        	} 
		        	} 
		            else{ 
		            	$id_count = 1; ?>
		            	
		            	
			            <div class="row">
		                  	<div class="col-3">
			                    <div class="form-group">
			                      <input type="text" class="form-control primary-title title_cls0" id="title0" name="title[]" placeholder="Enter Title"  value="" maxlength="200" />
			                    </div>
			                </div>

			                <div class="col-1">
			                    <div class="form-group">
			                     	<input type="text" class="form-control allow-numeric option_display_order_cls" id="option_display_order0" name="option_display_order[]" placeholder="Enter Display Order" value="" maxlength="5" />
			                    </div>
			                </div>

			                <div class="col-2">
			                    <div class="form-group">
			                      	<input type="checkbox" class="form-control check-parent-box" id="sub_question_radio0" name="sub_question_yes[0]" title="Add Sub Question" value="1" data-id="0" />
							              <input type="hidden" name="sub_q[]" id="sub_q_0" value="0" />          
			                    </div>
			                    <span id="if_subquestion_error0" style="color: red;"></span>
			                </div>

		    				<div class="col-5 parent_qus_display0" id="select-0" style="display: none"> 
								<div class="form-group">
									<select name="parent_qus_chk[0][]" id="parent_qus0" class="form-control select2" multiple="multiple" >
									</select>
								</div>
		            			<span id="parent_qus_error0" style="color: red"></span>
							</div>
						 
	               		</div>	

	               	<?php } ?>

		                <div class="box-element" id="repeatable_radio"></div>
		                <input type="hidden" name="display_order_count_hidden" id="display_order_count_hidden" value="<?php echo @$last_display_order; ?>">						
		                <div class="col-2 mb-3 pl-0">
		                  <a href="javascript:void(0);" id="btn_add_radio" title="add option"><i class="btn btn-primary fas fa-plus-square"></i></a>&nbsp;
		                  <a href="javascript:void(0);" id="btn_remove_radio_1" class="remove-added-box"><i class="btn btn-primary far fa-minus-square"></i></a>
		                </div>	
		               		
		       </div>  
	        	
	          <!--checkbox div end-->				
				
			 <!-- Matrix Related Div Start -->
				<div id="matrix_question" class="border px-3 pt-3 mb-3" style="display:none;">
					
					<div class="row">
			        	<div class="col-6">
			            	<label for="options">Enter Label <span style="color: red">*</span></label>
			            </div>
			            <div class="col-1">
			            	<label for="option_display_order">Order</label>
			            </div>
			            
					<?php 
					$ord = '';
					if(count($option_data) > 0 && $option_data[0]['option'] != '' && @$question_data[0]['response_type_id'] != 'radio' && @$question_data[0]['response_type_id'] != 'checkbox' && @$question_data[0]['response_type_id'] != 'single_select'){ ?>

						<div class="col-1">
			            	<label>Remove</label>
			            </div>
			        </div>
					
					<?php	if(@$question_data[0]['response_type_id'] == 'textbox' || @$question_data[0]['response_type_id'] == 'textarea'){
							foreach($option_data as $optionProvide => $optVal)
							{
								//$label_count = sizeof($option_data);
								if($optVal['option'] != ''){
								?>
								 <div class="row" id="remove-multiple-box_label-<?php echo @$optVal['ques_option_id']; ?>">						
									<div class="col-6">
										<div class="form-group">
											<input type="text" class="form-control primary-title title_cls" id="text_label0" name="text_label[]" placeholder="Enter Label"  value="<?php echo $optVal['option']; ?>" maxlength="100"/>
										</div>
									</div>
									<div class="col-1">
										<div class="form-group">
											<input type="text" class="form-control allow-numeric label_display_order_cls" id="label_display_order0" name="label_display_order[]" placeholder="Enter Display Order" value="<?php echo $optVal['display_order']; ?>" maxlength="10"/>
										</div>
									</div>
									<div class="col-1">
										<div class="form-group">
											<a href="javascript:void(0);" class="del-id" onClick="return delete_kr(<?php echo @$optVal['ques_option_id']?>, 'label');" id="btn_delete_radio" data-id="<?php echo @$optVal['ques_option_id']?>" ><i class="btn btn-primary fas fa-times-circle"></i></a>
										</div>
									</div>
								</div>
							<?php 
								}
								$ord = count($option_data);
							}
						}
					}
					else 
					{
						$ord = 1; ?>
					</div>

						<div class="row" >
				
							<div class="col-6">
							<div class="form-group">
								<input type="text" class="form-control primary-title title_cls" id="text_label0" name="text_label[]" placeholder="Enter Label"  value="" maxlength="100"/>
							</div>
						</div>
						<div class="col-1">
							<div class="form-group">
							<input type="text" class="form-control allow-numeric label_display_order_cls" id="label_display_order0" name="label_display_order[]" placeholder="Enter Display Order" value="1" maxlength="10"/>
							</div>
						</div>
						<div id="repeatable_label_textbox" class="col-12"></div>
							<input type="hidden" name="display_order_label_hidden" id="display_order_label_hidden" value="1">
						</div>
					<?php } ?>

					<div id="repeatable_label_textbox" class="col-12"></div>
					<input type="hidden" name="display_order_label_hidden" id="display_order_label_hidden" value="<?php echo $ord; ?>">
					<div class="col-2 mb-2">
					  <a href="javascript:void(0);" id="btn_add_label" title="add option"><i class="btn btn-primary fas fa-plus-square"></i></a>
					  &nbsp;<a href="javascript:void(0);" id="btn_remove_radio_2" class="remove-added-box"><i class="btn btn-primary far fa-minus-square"></i></a>
					</div>						
					<div class="row" >
						<div class="col-3">
						  <div class="form-group">
							 <select class="form-control text_type_val" id="text_type_val" name="text_type_val" onchange="open_condition_list(this.value,0, 'main')">
								<option value="">--Select--</option>                                
							  </select>
						  </div>
						</div>
						<div class="col-3">
						  <div class="form-group">
							 <select class="form-control condition_type_val" id="condition_type_val" name="condition_type_val" onchange="open_textbox(this.value,0)">
								<option value="">--Select--</option>
							  </select>
						  </div>
						</div>
						<div class="col-2">
						  <div class="form-group" id="custom-min-2">
							<input type="text" class="form-control" id="min_val" name="min_val" placeholder="Min" value="<?php echo @$option_data[0]['min_value']; ?>" maxlength="100" onkeypress="return isNumber(event)">
						  </div>
						</div>
						<div class="col-2 max_div" id="custom-max-2">
						  <div class="form-group">
							<input type="text" class="form-control" id="max_val" name="max_val" placeholder="Max" value="<?php echo @$option_data[0]['max_value']; ?>" maxlength="100" onkeypress="return isNumber(event)">
						  </div>
						</div>	
						<div class="col-3" id="custom-label-2">
						  <div class="form-group">
							<input type="text" class="form-control" id="custom_error_val" name="custom_error_val" placeholder="Custom error text" value="<?php echo @$option_data[0]['validation_label']; ?>" maxlength="200">
						  </div>
						</div>
						<div class="col-5" id="summession-box-class" >
							<div class="form-group">
							  <label for="sub_question">If Summation required?</label>
							  <input type="checkbox" class="form-control" id="if_summation" name="if_summation" value="1" <?php if(isset($question_data[0]['if_summation'])){if($question_data[0]['if_summation']=="Yes"){ ?> checked="checked" <?php }  } ?> />
							</div>
							
							<div class="form-group">
							  <label for="sub_question">If file upload required with Short Answer Or Paragraph?</label>
							  <input type="checkbox" class="form-control" id="input_file_required" name="input_file_required" value="1" <?php if(isset($question_data[0]['input_file_required'])){if($question_data[0]['input_file_required']=="Yes"){ ?> checked="checked" <?php }  } ?> />
							</div>
							
						 </div>
					</div>
				</div>
				<!--------- Matrix Related Div  End----------->

				<!-- Textbox div end Parent End -->		
	            <div id="textbox" class="border px-3 pt-3 mb-3" style="display: none">
	              <div class="row">  
	                <div class="col-3">
	                  <div class="form-group">
	                     <select class="form-control" id="text_type0" name="text_type" onchange="open_condition_list(this.value,0, 'main')">
	                        <option value="">--Select--</option>                                
	                      </select>
	                  </div>
	                </div>
	                <div class="col-3">
	                  <div class="form-group">
	                     <select class="form-control" id="condition_type0" name="condition_type" onchange="open_textbox(this.value,0)">
	                        <option value="">--Select--</option>
	                     </select>
	                  </div>
	                </div>
	                <div class="col-2" id="custom-min">
	                  <div class="form-group">
	                    <input type="text" class="form-control" id="min0" name="min" placeholder="Number" value="<?php if(@$option_data[0]['min_value']!="") { echo $option_data[0]['min_value']; } ?>" maxlength="100" onkeypress="return isNumber(event)">
	                  </div>
	                </div>						
	                <div class="col-2 max_div" id="custom-max">
	                  <div class="form-group">
	                    <input type="text" class="form-control" id="max_div" id="max0" name="max" placeholder="Max" value="<?php if(@$option_data[0]['max_value']!="") { echo $option_data[0]['max_value']; } ?>" maxlength="100" onkeypress="return isNumber(event)">
	                  </div>
	                </div>
					<div class="col-3" id="custom-label">
	                  <div class="form-group">
	                    <input type="text" class="form-control" id="custom_error0" name="custom_error" placeholder="Custom error text" value="<?php if(!empty(@$option_data[0]['validation_label'])) { echo $option_data[0]['validation_label']; } ?>" maxlength="200">
	                  </div>
	                </div>
	            </div> 
				<div class="row">
					<div class="col-3 d-flex" id="apply_logical_question" style="display:none;">
						<label for="sub_question">If Subquestion?</label>
						<div class="form-group">								 
							<input type="checkbox" class="form-control logical-ques single-logical" id="if_logical" name="if_logical" value="yes" title=""  <?php if(@$option_data[0]['if_sub_question_checked']=="Yes"){ ?> checked="checked" <?php }  ?> />
							<input type="hidden" name="" id="single_option_id" value="<?php echo @$option_data[0]['ques_option_id'];?>" /> 
						</div>

					</div>

					<?php 

						if(@$value['if_sub_question_checked'] == 'Yes'){
			                	
		                	$where = array('question_id'=>$question_data[0]['question_id'], 'ques_option_id'=> $value['ques_option_id'], 'is_deleted'=>0);

							$dependant_question_data = $this->master_model->getRecords("survey_option_dependent", $where);
							//echo ">>>>".$this->db->last_query();
							@$json_data_textbox[$key] = $dependant_question_data;

						}
	                	
					?>
					
					<div class="col-8" id="select_logical_question" style="display:none;">
						<label for="sub_question">Select Subquestion <span style="color: red">*</span></label>
						<div class="form-group">								 
							<select id="questionlist2" name="parent_qus[]" class="form-control select2" multiple="multiple">
							</select>
						</div>
					</div>
				</div>
	         </div> 
	        <!--Textbox div end Parent-->				
				
			<!------ File Upload End ----->	
		        <div class="col-12">
		            <div class="row">
		              <div class="col-4 pl-0">
		                <div class="form-group">
		                  <label for="is_mandatory" class="mr-4">Is Mandatory?</label>
		                  <input type="radio" id="is_mandatory" name="is_mandatory" value="1" 
		                    <?php if(@$question_data[0]['is_mandatory'] == 1) { echo 'checked'; } ?>>
		                  <span for="yes">Yes</span>&nbsp;
		                  <input type="radio" id="is_mandatory" name="is_mandatory" value="0" 
		                    <?php if(@$question_data[0]['is_mandatory'] == 0) { echo 'checked'; } ?>>
		                  <span for="no">No</span>
		                </div>
		              </div>
						<input type="hidden" id="no_of_input_fields" name="no_of_input_fields" class="form-control" value="0" onkeypress="return isNumber(event)">
		             </div>
		            <div class="form-group">
		              <input type="hidden" class="form-control" id="prev" value="">
		            </div>
            
		          	
					</div>
    		    </div> <!-- /.col-12 -->

					<div class="card-footer">
		                <button type="submit" class="btn btn-primary" id="btn_submit" name="submit">Update</button>
		                <button type="submit" class="btn btn-primary" id="btn_save_preview" name="save_preview">Update & Preview</button>

		                <a href="<?php echo base_url('xAdmin/question') ?>" class="btn btn-primary">Back</a> 
		            </div>
    	     </form>
    	    </div><!-- /.card-body -->
        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->
    </section>
</div>

<?php //$this->load->view('client_validation'); 

 ?>



<script type="text/javascript">
$(document).ready(function() 
{
	
	var error_cnt = 0;
    $('#btn_submit, #btn_save, #btn_save_preview').click(function(){

    	var salutation_check = $(".salutation-box").is(':checked');
        if(salutation_check){
	      if($('#salutation_input').val() == ''){
	      	
	        $('#salutation_error').text('Please enter Salutation');
	        return false;
	      }
	      else{
	        $('#salutation_error').text('');
	        return true;
	      }	
	 	}

        var option_type = $('#option_type').val();
        //alert($('#text_type0').val());
        //alert(option_type);
        if(option_type == 'radio' || option_type == 'checkbox' || option_type == 'single_select'){
	        //alert('if');
	        $.each($(".check-parent-box:checked"), function(){
	          var data_id = $(this).attr('data-id');
	          //alert(data_id);
	          var sub_q_check = $("#sub_question_radio"+data_id).is(':checked');
	          //alert(sub_q_check);
	          if(sub_q_check){
	            if($('#parent_qus'+data_id).val() == ''){
	              $('#parent_qus_error'+data_id).text('Please select Subquestion');
	              error_cnt++;
	        
	              //return false;
	            }
	            else{
	              $('#parent_qus_error'+data_id).text('');
	              error_cnt = 0;
	              
	              //return true;
	            }
	          }

	        });
           //alert(error_cnt);
          if(error_cnt == 0){
            //$('#btn_submit').prop('disabled','true'); 
           // $('#btn_save').prop('disabled','true');
           // $('#btn_save_preview').prop('disabled','true');
            return true;
          }
          else{
            //$('#btn_submit').prop('disabled','false'); 
            //$('#btn_save').prop('disabled','false');
            //$('#btn_save_preview').prop('disabled','false');
            return false;
          }
      }
      else if(option_type == 'textbox' || option_type == 'textarea'){

        var logical_check = $(".logical-ques").is(':checked');
        if(logical_check){
          if($('#questionlist2').val() == ''){
            $('#logical_question_error').text('Please select Subquestion');
            return false;
          }
          else{
            $('#logical_question_error').text('');
            return true;
          }
        }
      }
    }); //submit


	get_question(<?php echo $question_data[0]['survey_id'] ?>);
	//alert(<?php //echo $question_data[0]['parent_question_id'] ?>);
		
	<?php 
		if( @$question_data[0]['parent_question_id'] > 0) { ?>
			$('#parent-question-box').css("display","block");
			get_question_listing(<?php echo $question_data[0]['parent_question_id'] ?>, <?php echo $question_data[0]['survey_id'] ?>);
		<?php }
		
		if(@$question_data[0]['salutation']!="" ) { ?>
			$('#salutation-box').css("display","block");	
		<?php } 

		if(@$question_data[0]['if_file_required']== 'Yes' && @$question_data[0]['if_file_required']!="") { ?>
			$('#file-upload-label').css("display","block");	
		<?php } 
		if(@$question_data[0]['response_type_id'] == 'radio' || @$question_data[0]['response_type_id'] == 'checkbox' || @$question_data[0]['response_type_id'] == 'single_select') { ?>
				$('#radio').css("display","block");	
		<?php } 
		if(@$question_data[0]['response_type_id'] == "textbox" || @$question_data[0]['response_type_id'] == "textarea")	
		{
			if(@$question_data[0]['input_more_than_1'] == "Yes" )
			{
				?>		
				
				$('#question_labels').attr('checked', true);
				$('#textbox').css("display","none");
				$('#matrix_question').css("display","block");
				$('#matrix-row').css("display","block");

				//open_validation_list(@$question_data[0]['response_type_id'], 'main');

				<?php
				if(@$option_data[0]['max_value'] != "") 
				{
				?>	
					$('#custom-max-2').css("display","block");
				<?php
				}
				else 
				{
					?>
					$('#custom-max-2').css("display","none");
					<?php				
				}			
				
				if(@$option_data[0]['validation_id'] > 0 ) 
				{			
				 if(@$option_data[0]['validation_id'] == 4 ) { ?>
				 	$('#custom-label').hide();
				 	$("#custom-label-2").hide();
				 <?php } 

				 if(@$option_data[0]['sub_validation_id'] == 16 ) { ?>
				 		$('#custom-min').hide();
				 		$("#custom-min-2").hide();

				 		$('#custom-label').hide();
				 		$("#custom-label-2").hide();
				 	<?php }		
				?>
					var newValidId = '<?php echo @$option_data[0]['validation_id'] ?>';
					var option_type = '<?php echo @$question_data[0]['response_type_id'] ?>';
					var site_path = '<?php echo base_url(); ?>';					
					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',option_type:option_type,validtype:newValidId},
						success: function(data){
							$("#text_type_val").html(data);
						}		
					});
			
					var sub_id = '<?php echo $option_data[0]['sub_validation_id'] ?>';
					
					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_sub_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',sub_id:sub_id,validtype:newValidId,mode:'edit'},
						success: function(data){
							$("#condition_type_val").html(data);
						}		
					});
			
				<?php } 
					else 
					{
					?>
					var option_type = '<?php echo $question_data[0]['response_type_id'] ?>';
					var site_path = '<?php echo base_url(); ?>';
					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',option_type:option_type},
						success: function(data){
							$("#text_type_val").html(data);
						}		
					});	
					<?php 
					
					}
				
				
			} 
			else
			{
				?>
				$('#textbox').css("display","block");
				$('#matrix-row').css("display","block");
				$('#apply_logical_question').css("display","block");
				$('#select_logical_question').css("display","block");	

				<?php if(@$option_data[0]['max_value'] != "") {?>
					
					$('#custom-max').css("display","block");
				<?php }
				else { ?>
					$('#custom-max').css("display","none");
				<?php } 
				 if(@$option_data[0]['validation_id'] > 0 ) {  
				 	if(@$option_data[0]['validation_id'] == 4 ) { ?>
				 		$('#custom-label').hide();
				 		$("#custom-label-2").hide();
				 	<?php } 
				 	if(@$option_data[0]['sub_validation_id'] == 16 ) { ?>
				 		$('#custom-min').hide();
				 		$("#custom-min-2").hide();

				 		$('#custom-label').hide();
				 		$("#custom-label-2").hide();
				 	<?php }
				 	?>
				 	
				
					//$('#textbox').css("display","block");
					var newValidId = '<?php echo $option_data[0]['validation_id'] ?>';
					var option_type = '<?php echo $question_data[0]['response_type_id'] ?>';
					var site_path = '<?php echo base_url(); ?>';
					//alert(newValidId);
					//$('#text_type0 option[value='+newValidId+']').attr('selected','selected');

					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',option_type:option_type,validtype:newValidId},
						success: function(data){
							$("#text_type0").html(data);
						}		
					});
			
					var sub_id = '<?php echo $option_data[0]['sub_validation_id'] ?>';
					
					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_sub_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',sub_id:sub_id,validtype:newValidId,mode:'edit'},
						success: function(data){
							$("#condition_type0").html(data);
						}		
					});
			
				<?php }
				else { ?>	
					var option_type = '<?php echo $question_data[0]['response_type_id'] ?>';
					var site_path = '<?php echo base_url(); ?>';
					$.ajax({
					 url: site_path+"xAdmin/question/get_selected_validation_list",
					 type: 'POST',
					 data: {'ci_csrf_token':'',option_type:option_type},
						success: function(data){
							$("#text_type0").html(data);
						}		
					});
				<?php } ?>

	//matrix_question	
	<?php  } 
		}
	?>


	/********************* check if subquestion checked in logical question*******************/
		if($("#if_logical").prop('checked') == true)
		{	
			var survey_id = '<?php echo @$question_data[0]['survey_id']; ?>';
			var option_id = $('#single_option_id').val();
			var question_id = "<?php echo $question_id; ?>";
			$.ajax({
			  url: site_path+"xAdmin/question/get_question_list_selected",
			  type: 'POST',			 
			  data: {'ci_csrf_token':'',survey_id:survey_id,option_id:option_id,question_id:question_id},
			  success: function(response){
					$("#questionlist2").html(response);
				  }
				  
			  });
			$('.select2').select2();			  
		}

		jQuery.each($(".if_subques_cls"), function(index){
			var subquestion_checked_id = $(this).attr('id');
			var option_id = $('#option_id_'+index).val();
			var question_id = "<?php echo $question_id; ?>";		
			if($("#"+subquestion_checked_id).prop('checked') == true)
			{
				var attrValue = $(this).attr('data-id');
				$('.parent_qus_display'+attrValue).css("display", "block");
				var survey_id = '<?php echo $question_data[0]['survey_id']; ?>';						
				$.ajax({
				  url: site_path+"xAdmin/question/get_question_list_selected",
				  type: 'POST',				 
				  data: {'ci_csrf_token':'',survey_id:survey_id,option_id:option_id,question_id:question_id},
				  success: function(response){
				  	$("#parent_qus"+index).html(response);
				  }
				});	
			}
		});
			
		/*****************************************************************************************************/
	

	 $('.select2').select2();
    var id_count = '<?php echo $id_count; ?>';
    if(id_count != ''){
    	newid = id_count;
    }
    else{
    	newid = 1;
    }
    
    var main_ques = 0;
    var sub_ques = 0;
    var str = '';
    var save_str = '';
    var resultArr = [];	

    //$('.multiselect_dropdown').multiselect();

    //$(selector).multiselect();


    var dependent_ques_options = [];
    var dependant_ques_display_order = [];


    $('#btn_add_radio').click(function(){
      var display_order_count_hidden = $("#display_order_count_hidden").val();
      //var option_display_order_radio = $('.option_display_order'+main_ques_radio).val();
      var display_order_index = parseInt(display_order_count_hidden)+1;

      var repeatable_radio = '<div class="row" id="radio-chk"><div class="col-3"><div class="form-group"><input type="text" class="form-control primary-title title_cls'+newid+'" id="title'+newid+'" name="title[]" placeholder="Enter Title" required /></div></div><div class="col-1"><div class="form-group"><input type="text" class="form-control allow-numeric option_display_order_cls" id="option_display_order'+newid+'" name="option_display_order[]" placeholder="Enter Display Order" value="'+display_order_index+'" /></div></div><div class="col-2"><div class="form-group" ><input type="checkbox" class="form-control check-parent-box" id="sub_question_radio'+newid+'" name="sub_question_yes['+newid+']" value="1" data-id="'+newid+'" /><input type="hidden" name="sub_q[]" id="sub_q_'+newid+'" value="0" /></div><span id="if_subquestion_error'+newid+'" style="color: red;"></span></div><div class="col-5 parent_qus_display'+newid+'" id="select-'+newid+'" style="display:none"><div class="form-group"><select name="parent_qus_chk['+newid+'][]" id="parent_qus'+newid+'" class="form-control multiselect_dropdown select2" multiple="multiple"  style="display: none"></select></div> <span id="parent_qus_error'+newid+'" style="color: red"></span></div></div>';
    
		$('#repeatable_radio').append(repeatable_radio);
        $("#display_order_count_hidden").val(display_order_index);
        main_ques++;
        newid++;
		
		$(".allow-numeric").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                $(".error").css("display", "inline");
                event.preventDefault();
            }else{
            	$(".error").css("display", "none");
            }
        });
       $('.select2').select2();
    });// add radio add more (parent)
	
	
	$('#btn_add_label').click(function(){
      var display_order_label_hidden = $("#display_order_label_hidden").val();
      //var option_display_order_radio = $('.option_display_order'+main_ques_radio).val();
      var display_order_index = parseInt(display_order_label_hidden)+1;

      var repeatable_label_textbox = '<div class="row" id="textbox-label"><div class="col-6"><div class="form-group"><input type="text" class="form-control  primary-title title_cls'+newid+'" id="text_label'+newid+'" name="text_label[]" placeholder="Enter Label" value="" /></div></div><div class="col-1"><div class="form-group"><input type="text" class="form-control allow-numeric label_display_order_cls" id="label_display_order'+newid+'" name="label_display_order[]" placeholder="Enter Display Order" value="'+display_order_index+'" /></div></div></div>';
    
      var classArr = ["title_cls"+main_ques];
	  
	   $('#repeatable_label_textbox').append(repeatable_label_textbox);

        $("#display_order_label_hidden").val(display_order_index);

        main_ques++;
        newid++;
		
		$(".allow-numeric").on("keypress keyup blur",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                $(".error").css("display", "inline");
                event.preventDefault();
            }else{
            	$(".error").css("display", "none");
            }
        });
     
    });// add radio add more (parent)
	
	 $(document).on('click', '.remove-added-box', function() {
	 	var displayOrder = $("#display_order_count_hidden").val();
		var cnts = displayOrder - 1;
		$("#display_order_count_hidden").val(cnts);

		$('div#radio-chk').last().remove();		
    });
	
	$(document).on('click', '.remove-select-box', function() {
		$('div#select-radio-row').last().remove(); 
	});

	$(document).on('click', '#btn_remove_radio_2', function() {
		var displayOrder = $("#display_order_label_hidden").val();
		var cnts = displayOrder - 1;
		$("#display_order_label_hidden").val(cnts);
		
		$('div#textbox-label').last().remove(); 
	});
	
    
  
	$(".allow-numeric").on("keypress keyup blur",function (event) {    
	   $(this).val($(this).val().replace(/[^\d].+/, ""));
		if ((event.which < 48 || event.which > 57)) {
			$(".error").css("display", "inline");
			event.preventDefault();
		}else{
			$(".error").css("display", "none");
		}
	});

}); //ready
</script>


<script type="text/javascript">
  var save_str = '';
  var action = '<?php //echo $action; ?>';
  var modal_index = 0;
  var option_cnt_index = 1;
  var display_order_count = 1;
  var opt =0;
  var plus_id = 0;
  $('#dependant_ques_tbldiv').hide(); 
  
  function get_question_listing(parent_id, survey_id)
  { 
	var site_path = '<?php echo base_url(); ?>';   
    $.ajax({
      url: site_path+"xAdmin/question/get_question_list_edit",
      type: 'POST',
      data: {'ci_csrf_token':'',survey_id:survey_id, parent_id:parent_id},
      success: function(response){ 		
        $("#parent_question").html(response);
       // $('#parent_question option[value='+parent_id+']').attr('selected','selected');
      }
    })  
  }
  
   function get_question(survey_id){
   
    var site_path = '<?php echo base_url(); ?>';
    var parent_question = $('#parent_question');
    parent_question.empty(); 

    var question_id_hidden = $('#question_id_hidden').val();

    $.ajax({

      url: site_path+"xAdmin/question/get_question_list",
      type: 'POST',
      data: {'ci_csrf_token':'',survey_id:survey_id, question_id:question_id_hidden},
      success: function(response){
	       	if(response != ''){
		        parent_question.append("<option value=''>--Select Question--</option>");
		        $.each(JSON.parse(response), function(idx, obj) {
		          parent_question.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
		        });
		    }
      	}
    })
  }
  
  
  function subsectionpopulate(section_id){  

	populateQuestion(section_id);
    var site_path = '<?php echo base_url(); ?>';
    var mySelect = $('#sub_section');
    mySelect.empty(); 
    $.ajax({
      url: site_path+"xAdmin/question/get_sub_section",
      type: 'POST',
      data: {'ci_csrf_token':'',section_id:section_id},
      success: function(response){
        mySelect.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.section_id+"'>"+obj.section_name+"</option>");
          var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
          $('#section option[value='+section_id+']').attr('selected','selected');

        });
      }
    })
  }
  
  function populateQuestion(section_id){
	  
	var site_path = '<?php echo base_url(); ?>';
    var mySelect = $('#parent_question');
	var survey_id = $("#survey").val();
	var sub_section_id = $("#sub_section").val();
    mySelect.empty(); 
    $.ajax({
      url: site_path+"xAdmin/question/open_question_list",
      type: 'POST',
      data: {'ci_csrf_token':'',section_id:section_id,survey_id:survey_id,sub_section_id:sub_section_id},
      success: function(response){
        mySelect.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
      
        });
      }
    })
  }

  function change_option(option_type)
  {  
  	//var prev_option_type = '<?php //echo $option_data[0]['response_type_id'];?>';
	$("#text_type0 option[selected]").removeAttr("selected");   
	$("#condition_type0 option[selected]").removeAttr("selected");
	$('#condition_type0').val(''); 
	$('#questionlist2').val('');
	$("#custom-max-2").hide();
	$('.checkbox').attr('checked', true);
	$("#min0").val("");
	$("#max_div").val("");
	$("#custom_error0").val("");	
	// $('#if_logical input[type=checkbox]'). attr('checked',false);
	 
	 
	$("#questionlist2 option[selected]").removeAttr("selected");
    $("#custom-min").show();
    
    $("#custom-label").show();
    $("#custom-label-2").show();
	$('#if_logical').prop('checked', false); 
	$('#question_labels').prop('checked', false); 
	document.getElementById('matrix_question').style.display = "none";

	var edit_option_type = '<?php echo $question_data[0]['response_type_id']; ?>';

    if(option_type == 'radio' || option_type == 'checkbox' || option_type == 'single_select'){
    	if(edit_option_type == option_type){
    		document.getElementById('matrix-row').style.display = "none";	
      		document.getElementById('textbox').style.display = "none";
    	}		
	      document.getElementById('matrix-row').style.display = "none";
	      document.getElementById('textbox').style.display = "none";
		  document.getElementById('radio').style.display = "block";	  
	      $('#option_display_order0').val(display_order_count);
	      $('#display_order_count_hidden').val(display_order_count);
		  $( "div" ).remove( "#radio-chk" );
		  /*if(prev_option_type != option_type){

		  }*/
    }
    else if(option_type == 'textbox' || option_type == 'textarea'){
    	if(edit_option_type == option_type){
    		document.getElementById('matrix-row').style.display = "none";	
      		document.getElementById('textbox').style.display = "none";
    	}
		document.getElementById('matrix-row').style.display = "block";	
	    document.getElementById('textbox').style.display = "block";
	    document.getElementById('radio').style.display = "none";	  
		document.getElementById('apply_logical_question').style.display = "block"; 
		  //document.getElementById('select_logical_question').style.display = "block";	
		  
	      open_validation_list(option_type, 'main');

    } else if(option_type == 'file'){
		$('#question_labels').prop('checked', false); 
		document.getElementById('matrix-row').style.display = "none";
		document.getElementById('textbox').style.display = "none";
		document.getElementById('radio').style.display = "none";
		document.getElementById('upload_system').style.display = "block";
	}
	else if(option_type == "")
	{
		  document.getElementById('matrix-row').style.display = "none";	
		  document.getElementById('textbox').style.display = "none";
		  document.getElementById('radio').style.display = "none";	  
		  document.getElementById('apply_logical_question').style.display = "none"; 
		  document.getElementById('select_logical_question').style.display = "none";
	}
    showhidenumber(option_type);
  }

   function open_validation_list(option_type, form_type)
   {   
		var site_path = '<?php echo base_url(); ?>';
		if(form_type == 'main'){
		  var mySelect = $('#text_type0');
		  var mySelect_val = $('#text_type_val');
		}
		else{
		  var mySelect = $('#modal_text_type0');
		}
		mySelect.empty(); 
		mySelect_val.empty(); 

		$.ajax({

		  url: site_path+"xAdmin/question/get_validation_list",
		  type: 'POST',
		  data: {'ci_csrf_token':'',option_type:option_type,mode:'add'},
		  success: function(response){
			
			mySelect.append("<option value=''>Select</option>");
			$.each(JSON.parse(response), function(idx, obj) {
			  mySelect.append("<option value='"+obj.validation_id+"'>"+obj.validation_type +"</option>");
			  
			});
			
			mySelect_val.append("<option value=''>Select</option>");
			$.each(JSON.parse(response), function(idx, obj) {
			  mySelect_val.append("<option value='"+obj.validation_id+"'>"+obj.validation_type +"</option>");
			  
			});
		  }
		})
	}

  function open_condition_list(validation_id, id, form_type)
  { 
	
	document.getElementById('summession-box-class').style.display = "none";
	//var open_close = $("#").val();
	if($("#question_labels").prop('checked') == true)
	{
		if(validation_id == 1)
		{
			document.getElementById('summession-box-class').style.display = "block";
		}
		else 
		{
			document.getElementById('summession-box-class').style.display = "none";
		}
		
	}
	
	
    if(validation_id == 1)
	{ 
      $("#min0").attr("placeholder", "Number");
      $("#modal_min0").attr("placeholder", "Pattern");
	  $("#min_val").attr("placeholder", "Number");
	  $("#custom-label").show();
	  $("#custom-label-2").show();
      $("#custom-max").hide();
      $("#modal_custom-max").hide();
    } 
	else if(validation_id == 9)
	{
      $("#min0").attr("placeholder", "Pattern");
	  $("#min_val").attr("placeholder", "Pattern");
      $("#modal_min0").attr("placeholder", "Pattern");
      $("#custom-label").show();
      $("#custom-label-2").show();
      $("#custom-max").hide();
      $("#modal_custom-max").hide();
    } 
	else if(validation_id == 2)
	{
       $("#min0").attr("placeholder", "Text");
	   $("#min_val").attr("placeholder", "Text");
       $("#modal_min0").attr("placeholder", "Text");
       $("#custom-label").show();
       $("#custom-label-2").show();
	   $("#custom-max").hide();
       $("#modal_custom-max").hide();
    } 
	else if(validation_id == 4)
	{
	   $("#custom-label").hide();
	   $("#custom-label-2").hide();
	   $("#custom-max").hide();
       $("#modal_custom-max").hide();
	   $("#min0").attr("placeholder", "Digit");
	   $("#min_val").attr("placeholder", "Digit");

    } 
	else 
	{
	   $("#custom-label").show();
	   $("#custom-label-2").show();
       $("#min0").attr("placeholder", "Number");
	   $("#min_val").attr("placeholder", "Number");
       $("#modal_min0").attr("placeholder", "Number");
    }
    
    var site_path = '<?php echo base_url(); ?>';

    if(form_type == 'main'){
      var mySelect = $('#condition_type'+id);
	  var mySelect_val = $('#condition_type_val');
	  
    }
    else{
      var mySelect = $('#modal_condition_type'+id);
    }
    
    mySelect.empty(); 
	mySelect_val.empty();
    $.ajax({

      url: site_path+"xAdmin/question/get_condition",
      type: 'POST',
      data: {'ci_csrf_token':'',validation_id:validation_id},
      success: function(response){
        
        $("#min_val").val('');
        $("#min0").val('');
        
        $("#custom_error0").val('');
        $("#custom_error_val").val('');

         mySelect.append("<option value=''>Select</option>");
		 mySelect_val.append("<option value=''>Select</option>");
        $.each(JSON.parse(response), function(idx, obj) {
          mySelect.append("<option value='"+obj.sub_validation_id+"'>"+obj.validation_sb_type +"</option>");
        });
		$.each(JSON.parse(response), function(idx, obj) {
          mySelect_val.append("<option value='"+obj.sub_validation_id+"'>"+obj.validation_sb_type +"</option>");
        });
      }
    })

  }

   function showhidenumber(option_type)
  {
    //alert(option_type);
    if(option_type == 'textbox' || option_type == 'textarea')
	{          
      $("#custom-min").show();
      $("#custom-max").hide();
      $("#modal_custom-max").hide();
    }
  }


  function open_textbox(validation_id, id){ //alert($("#condition_type0").val());
    if(validation_id == 7 || validation_id == 8){ 
      $("#custom-min").show();
      $("#custom-max").show();

      $("#custom-min-2").show();
	  $("#custom-max-2").show();
      $("#modal_custom-max").show();
      $("#custom-label").show();    
      $("#custom-label-2").show();
      $("#min0").attr("placeholder", "Min");
      $("#min_val").attr("placeholder", "Min");
      $("#custom-max").attr("placeholder", "Max");

      $("#modal-min").show();
      $("#modal-max").show();
      $("#modal-label").show(); 
      $("#modal_min0").attr("placeholder", "Min");
      $("#modal_max_div").attr("placeholder", "Max");
    } 
    else if($("#condition_type0").val() == 14 || $("#condition_type0").val() == 15)
    {
      $("#custom-min").show();
      $("#custom-min-2").show();
      $("#custom-max").hide();
      $("#custom-max-2").hide();
      $("#custom-label").hide();
      $("#custom-label-2").hide();
      $("#min0").attr("placeholder", "Number"); 
      $("#min_val").attr("placeholder", "Number");
    }
	else if($("#condition_type0").val() == 16)
	{
		$("#custom-min").hide();
		$("#custom-max").hide();

		$("#custom-min-2").show();
		$("#custom-max-2").hide();	
		$("#custom-label").hide();
     	
		$("#min0").attr("placeholder", "Number");
		$("#min_val").attr("placeholder", "Number");
	}	
	else if($("#condition_type_val").val() == 16)
    {
      $("#custom-min").show();
      $("#custom-min-2").hide();

      $("#custom-max").hide();
      $("#custom-max-2").hide();
      $("#custom-label-2").hide();    
      $("#min0").attr("placeholder", "Number");
      $("#min_val").attr("placeholder", "Number");
    } 	
    else if($("#condition_type_val").val() == 14 || $("#condition_type_val").val() == 15)
    {
      $("#custom-min-2").show();    
      $("#custom-label-2").hide();    
      
    }
    else if($("#condition_type0").val() == 14 || $("#condition_type0").val() == 15)
    {
      $("#custom-min").show();    
      $("#custom-min-2").show();

      $("#custom-label").hide();     
      $("#custom-label-2").hide();     
    }
    else	
	{     
      $("#custom-min").show();
      $("#custom-min-2").show();

      $("#custom-max").hide();
      $("#custom-max-2").hide();

      $("#custom-label").show();
      $("#custom-label-2").show();  
      	
      $("#modal_custom-max").hide();

      $("#modal-min").show();
      $("#modal-max").hide();
	  
	    
	  $("#min0").attr("placeholder", "Number");
	  $("#min_val").attr("placeholder", "Number");
    }

  }
  

  /**************************Delete**********************************/ 	
 function delete_kr(id,type)
 { 
		var lengthCnt = $(".class-cnt").length;
		if(lengthCnt > 1)
		{
			swal(
			{
				title:"Confirm?",
				text: "Are you sure you want to delete?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes!'
			}).then(function (result)
			{
				if (result.value)
				{
					//var cs_t =     $('.token').val();
					var question_id = '<?php echo $question_data[0]['question_id']; ?>';
					//var option_id = $(this).attr('data-id');
					//alert(question_id+"=="+id);return false;
					$.ajax(
					{
						type:'POST',
						url: '<?php echo site_url("xAdmin/question/remove_option"); ?>',
						//data:{ 'id':id, 'csrf_test_name':cs_t },
						data: {'ci_csrf_token':'',question_id:question_id, option_id:id},                 
						success:function(data)
						{
							//$(".token").val(data.token); 
							//var lengthCount = $(".option_display_order_cls").length;
							//alert(lengthCount);
							if(type == 'radio'){
								$("#remove-multiple-box-"+id).remove();
							}
							else{
								$("#remove-multiple-box_label-"+id).remove();
							}
							swal(
							{
								title: 'Success!',
								text: "Option Removed Successfully",
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							});
						}
					});
				}
			});
		}
		else 
		{
			swal({
					title: 'Error!',
					text: "Atleast one option required for this question.",
					type: 'error',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				});
		}
	
	}
  
  /******************************************************************/

 
  
  /****************Store Multidimention Values***********************/
  
	//$(".check_class").click(function() {
	$(document).on('click', '#if_summation', function() { 
	  $("#input_file_required").prop("checked", false); //uncheck all checkboxes
	});
	
	$(document).on('click', '#input_file_required', function() {  
	  $("#if_summation").prop("checked", false); //uncheck all checkboxes
	});
	
	$(document).on('click', '.check-parent-box', function() {	
		var surveyid = $("#survey").val();
		//alert(surveyid);
		var section = $("#section").val();
		var sub_section_id = $("#sub_section").val();
		var attrValue = $(this).attr('data-id');
		var site_path = '<?php echo base_url(); ?>';
		var question_id = '<?php echo $question_id; ?>';
		
		if($("#sub_question_radio"+attrValue).prop('checked') == true){	
			$('.parent_qus_display'+attrValue).css("display", "block");			
			$("#sub_q_"+attrValue).val(1);
			var mySelect = $('#parent_qus'+attrValue);		
			mySelect.empty(); 
			if(surveyid == ''){
	       	 $('#if_subquestion_error'+attrValue).text('Please select Survey');
	      	}//if 
	      	else{
		        $('#if_subquestion_error'+attrValue).text('');
				$.ajax({
				  url: site_path+"xAdmin/question/get_question_list_choice",
				  type: 'POST',
				  data: {'ci_csrf_token':'',survey_id:surveyid, question_id: question_id},
				  success: function(response){				
					mySelect.append("<option value='' disabled>--Select--</option>");
					$.each(JSON.parse(response), function(idx, obj) {
					  mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text +"</option>");
					});
					
					$('.select2').select2();
				  }
				});		
			}	
		} 
		else {				
			$('.parent_qus_display'+attrValue).css("display", "none");
			$("#sub_q_"+attrValue).val(0);
			var mySelect = $('#parent_qus'+attrValue);				
			mySelect.empty(); 
			mySelect.append("<option value='' disabled>--Select--</option>");
		}
	}); 
	
	$(document).on('click', '.salutation-box', function() {
		
		if($("#salutation_box").prop('checked') == true){	
			$('#salutation-box').css('display','block');
		} else {
			$('#salutation-box').css('display','none');
		}
		
	});
	
	$(document).on('click', '.label-add', function() 
	{		
		if($("#question_labels").prop('checked') == true){	
			$('#textbox').css('display','none');
			$('#matrix_question').css('display','block');
			open_validation_list('textbox', 'main')
		} else {
			$('#textbox').css('display','block');
			$('#matrix_question').css('display','none');
		}
		
	});
	
	
	$(document).on('click', '.required-parent-box', function(){
		if($("#if_parent_required").prop('checked') == true){
			$('#parent-question-box').css('display','block');
		} 
		else
		{
			$('#parent-question-box').css('display','none');
		}
	});	
	
	$(document).on('click', '.required-file', function(){
		if($("#if_file_required").prop('checked') == true){
			$('#file-upload-label').css('display','block');
		} 
		else
		{
			$('#file-upload-label').css('display','none');
		}
	});	
	
	 /*document.multiselect('.multiselect_dropdown')
		.setCheckBoxClick("checkboxAll", function(target, args) {
			console.log("Checkbox 'Select All' was clicked and got value ", args.checked);
		})
		.setCheckBoxClick("1", function(target, args) {
			console.log("Checkbox for item with value '1' was clicked and got value ", args.checked);
		});

	function enable() {
		document.multiselect('.multiselect_dropdown').setIsEnabled(true);
	}

	function disable() {
		document.multiselect('.multiselect_dropdown').setIsEnabled(false);
	}*/
	
	
	$(document).on('click', '.logical-ques', function() {
		var surveyid = $("#survey").val();
		var section = $("#section").val();
		var sub_section_id = $("#sub_section").val();
		var attrValue = $(this).attr('data-id');
		var site_path = '<?php echo base_url(); ?>';
		var question_id = '<?php echo $question_id; ?>';
		if($("#if_logical").prop('checked') == true){				
			//$("#sub_q_"+attrValue).val(1);
			$('#select_logical_question').css('display','block');
			var mySelect = $('#questionlist2');		
			mySelect.empty(); 
			if(surveyid == ''){
	        $('#if_logical_error').text('Please select Survey');
	      }//if 
	      else{
	        $('#if_logical_error').text('');
			$.ajax({
				  url: site_path+"xAdmin/question/get_question_list_choice",
				  type: 'POST',
				  data: {'ci_csrf_token':'',section_id:section,survey_id:surveyid,sub_section_id:sub_section_id,question_id:question_id},
				  success: function(response){
					
					mySelect.append("<option value='' disabled>--Select--</option>");
					$.each(JSON.parse(response), function(idx, obj) {
					  mySelect.append("<option value='"+obj.question_id+"'>"+obj.question_text +"</option>");
					});
					$('.select2').select2();
				  }
			});	
			}		
		} else {	
			$('#select_logical_question').css('display','none');
			var mySelect = $('#questionlist2');		
			mySelect.empty(); 
			mySelect.append("<option value='' disabled>--Select--</option>");
		}
	});
  
  

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  
  
  $( document ).ready(function() {
   $("label").removeAttr("style");  
   
    $("form[name='question_form']").validate({		
	  rules: {
		  survey: {
               required: true,
             },
			 parent_question:{
				required: function() { 
			   if($('#if_parent_required').is(':checked'))
			   { 							
				   return true;
				   
				}  else {
					 return false; 
				 }	
			   } 
			 },
             section: {
               required: true,
             },
			 question_text: {
               required: true,
			   maxlength:500
             },
			
			 file_label: {
               required: function() { 
               if($('#if_file_required').is(':checked'))
			   { 
				   return true;
				} else 
				{
					return false;
				}					
               }
			},
			 option_type: {
               required: true,
             },
			 "text_label[]" : {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#question_labels').is(':checked')){ 
				   return true;
				}  else {
					 return false; 
				 }	
               }
			},
			condition_type_val: { 
				
               required: function() { 
	               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') )
				    {  
				    	if($(".text_type_val").val() == ''){
						  return false;
						} 
						else 
						{
							return true; 
						}					
	                }
           		}
			},
			min_val: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#question_labels').is(':checked'))
			   {	
					if($(".text_type_val").val() =="" && $(".condition_type_val").val() =="")
					{
						return false;
					}
					else
					{
						return true;
					}	
				   //return false;
				}
				
               },
			   digits:function() {
				   if($("#text_type_val").val() == 1 || $("#text_type_val").val() == 4)
				   {
					   return true;
				   }
				}
			},
			max_val: {
               required: function() { 
               if($("#condition_type_val").val() == '7' || $("#condition_type_val").val() == '7'){ 
				   return true;
				}  
               },
			   digits:function() {
				   if(($("#text_type_val").val() == 1 || $("#text_type_val").val() == 4) && $("#condition_type_val").val() == '7' || $("#condition_type_val").val() == '7')
				   {
					   return true;
				   }
				}
			},			
			condition_type: {
               required: function() {

               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea'))
			   { 
					console.log(">>>>"+$("#text_type0").val());
               		if($("#text_type0").val() == "" || $("#text_type0").val() == "--Select--")
					{
					  return false;
					} 
					else 
					{
						return true; 
					}	
				}				
               }
			},
			min: {
               required: function() {  
				   if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#condition_type0").val() != ''))
				   {					   
					   if($("#condition_type0").val() == 16)
					   {
						   return false;
					   }
					   else
					   {
						   return true; 
					   }
					  
					}
					else if($("#text_type0").val() == '4')
					{
						return true;
					}
					else
					{ 
						return false;
					}  
			   },
			   digits:function() {
				   if($("#text_type0").val() == 1 || $("#text_type0").val() == 4)
				   {
					   return true;
				   }
				    else 
				   {
					   return false;
				   }
				}
            },			  
			max: {
               required: function() { 
				   if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && ($("#condition_type0").val() == '7' || $("#condition_type0").val() == '8'))
				    { 
					   return true;
					}					
					else
					{ 
						return false;
					} 				
               },
			   digits:function() {
				   if(($("#text_type_val").val() == 1 || $("#text_type_val").val() == 4) && $("#condition_type0").val() == '7' || $("#condition_type0").val() == '7')
				   {
					   return true;
				   }
				    else 
				   {
					   return false;
				   }
				}
			},
					
			parent_qus: {
               required: function() { 
               if(($("#option_type").val() == 'textbox' ||  $("#option_type").val() == 'textarea') && $('#if_logical').is(':checked')){ 
				   return true;
				}  
               }
			},
			"title[]" : {
               required: function() { 
               if(($("#option_type").val() == 'radio' ||  $("#option_type").val() == 'checkbox' ||  $("#option_type").val() == 'single_select'))
			   { 
				   return true;
				} 
				else
				{
					return false;
				}	
               },
			   minlength: 1
			}
	  },
	  messages: {
			 survey: {
                required: "This field is required"
             },
			 parent_question: {
                required: "This field is required"
             },
             section: {
               required: "This field is required"
             },
			 question_text: {
               required: "This field is required"
             },
			 salutation_input: {
               required: "This field is required"
             },
			 file_label: {
               required: "This field is required"
             },
			 option_type: {
               required: "This field is required"
             },
			"text_label[]": {
               required: "This field is required"
             },
			 condition_type_val: {
               required: "This field is required"
             },
			 min_val: {
               required: "This field is required"
             },
			 min_val: {
               required: "This field is required"
             },
			condition_type: {
               required: "This field is required"
             },
			min: {
               required: "This field is required"
             },
			max: {
               required: "This field is required"
             },
			custom_error: {
               required: "This field is required"
             },			
			 parent_qus: {
               required: "This field is required"
             },
			 "title[]": {
				required: "This field is required"		  
			}
	  },
	  submitHandler: function(form) {
	  	//$('#btn_submit').prop('disabled','true'); 
       // $('#btn_save_preview').prop('disabled','true');
	   //alert("INNNNNN");
        question_form.submit();
      } 
	});
	
});
  
</script>


<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>

