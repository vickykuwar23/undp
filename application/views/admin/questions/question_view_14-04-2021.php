<?php 
 // $logical_conditions = array('Greater than'=>'>', 'Greater than or equal to'=>'>=', 'Less than'=> '<', 'Less than or equal to' => '<=', 'E')
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                   <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey');?>">Survey List</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>  
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            <?php /*?><a href="<?php echo base_url('xAdmin/question/list/'.base64_encode($question_list[0]['survey_id']).'/'.base64_encode($question_list[0]['category_id'])) ?>" class="btn btn-primary btn-sm float-right">Back</a><?php */?>
            <h3 class="card-title">Question Bank</h3>
            <a href="<?php echo base_url('xAdmin/survey'); ?>" class="btn btn-primary btn-sm float-right">Back</a>
          </div>
          <div class="card-body">
            <!-- <form action="<?php //echo base_url(); ?>xAdmin/question/view/<?php //echo $action.'/'.$survey_id; ?>" method="post" id="answer_form" name="answer_form" role="form" > -->
         <!--  <span style="color: blue; font: bold;"><?php //echo $question_data[0]['survey_name'];?></span>
          <hr>   --> 




          <?php
            echo $html; 
          ?>

        
          <center>
            <div id="loading" class="divLoading"><p>Loading... <img src="<?php echo base_url(); ?>assets/images/loader.gif" /></p></div>
          </center>
      
           <!--  <div class="card-footer1">
              <button type="submit" class="btn btn-primary" name="submit">Edit</button>
            </div> -->

          <!-- </form> -->
          
          </div><!-- /.card-body -->

        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->

  </section>

 </div>

<script type="text/javascript">
	$("#loading").hide();



  $(document).on('click, change', '.dependantcls', function() {
	 
   
    var inputType = $(this).attr('type');
	  //alert(inputType);
    if(inputType == '' || inputType == undefined){
      inputType = 'select';
      var question_id = $(this).children(":selected").attr("id");
      var option_id = $(this).children(":selected").data("id");

      var p_ques_id  = $(this).children(":selected").attr('data-parent');
      var ques_sr  = $(this).children(":selected").attr('data-srno');
    }
    else if(inputType == 'checkbox'){ //alert("Radio");
      var option_id =  $(this).attr('id');
      var question_id  = $(this).data('id');
      var ques_sr  = $(this).attr('data-srno');
      //var ques_id = $("#"+option_id).val();

      var is_checked = $("#"+option_id).is(':checked');
      //alert(option_id);
      //alert(is_checked);
    }
    else{ //alert("Radio");
      var question_id =  $(this).attr('id');
      var option_id  = $(this).data('id');
      var p_ques_id  = $(this).attr('data-parent');
      var ques_sr  = $(this).attr('data-parent');
    }    

    //alert(question_id+'---'+option_id);
  
    if(question_id!='' && (inputType == 'select' || inputType == 'radio' || (inputType == 'checkbox' && is_checked == true ))){

      //$("#loading").show();
	      $.ajax({

	        url: site_path+"xAdmin/question/get_dependant_question",
	        type: 'POST',
	        data: {'ci_csrf_token':'', question_id:question_id, option_id:option_id, srno:ques_sr},
	        success: function(result){
	          if(result!='')
	          {   
              //alert(response);
              var response = result.split('$$$');
	            var data = jQuery.parseJSON(response[0]);
              
	            if(inputType == 'select'){
                //alert(p_ques_id);
                if(p_ques_id != ''){
                  var select_name = p_ques_id+'_'+question_id;
                  
                  var prev_select = $('#prev_select_'+select_name).val();

                  var pr_ques_id = p_ques_id+'_'+question_id+'_'+option_id;
                  var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_select;

                  var select_repeat_id = p_ques_id+'_'+option_id;
                }
                else{
                  var select_name = question_id;
                  
                  var prev_select = $('#prev_select_'+select_name).val();

                  var pr_ques_id = question_id+'_'+option_id;
                  var prev_ques_id = question_id+'_'+prev_select;
                  var select_repeat_id = option_id;
                }
                
                $(".dropdown_dependant_div_"+prev_ques_id).css("display", "none");
                $(".dropdown_dependant_div_"+pr_ques_id).css("display", "block");

                $('#prev_select_'+select_name).val(option_id);

                if(response[1] != ''){
                 // $prev_subq_select = $('#prev_subq_select').val();

                  $(".dependant_dropdown_subquestion_div_"+prev_ques_id).css("display", "none");
                  $(".dependant_dropdown_subquestion_div_"+pr_ques_id).css("display", "block");

                  $('#subquestion_section_'+select_repeat_id).show();
                  $('#subquestion_section_'+select_repeat_id).empty();
                  $('#subquestion_section_'+select_repeat_id).append(response[1]);

                }
                else{
                  $(".dependant_dropdown_subquestion_div_"+prev_ques_id).css("display", "none");
                  $('#subquestion_section_'+select_repeat_id).hide();
                  $('#subquestion_section_'+select_repeat_id).empty();
                  $('#subquestion_section_'+select_repeat_id).append('');
                }
                
	            }
	            else if(inputType == 'radio'){

                if(p_ques_id != ''){
                  var radio_name = p_ques_id+'_'+question_id;
                  
                  var prev_radio = $('#prev_radio_'+radio_name).val();

                  var pr_ques_id = p_ques_id+'_'+question_id+'_'+option_id;
                  var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_radio;

                  var radio_repeat_id = p_ques_id+'_'+option_id;
                }
                else{
                  var radio_name = question_id;
                 
                  var prev_radio = $('#prev_radio_'+radio_name).val();

                  var pr_ques_id = question_id+'_'+option_id;
                  var prev_ques_id = question_id+'_'+prev_radio;

                  var radio_repeat_id = option_id;
                }

                $(".radio_dependant_div_"+prev_ques_id).css("display", "none");
                $(".radio_dependant_div_"+pr_ques_id).css("display", "block");
                
                $('#prev_radio_'+radio_name).val(option_id);

                 if(response[1] != ''){
                  $(".dependant_radio_subquestion_div_"+prev_ques_id).css("display", "none");
                  $(".dependant_radio_subquestion_div_"+pr_ques_id).css("display", "block");

                  $('#subquestion_section_'+radio_repeat_id).show();
                  $('#subquestion_section_'+radio_repeat_id).empty();
                  $('#subquestion_section_'+radio_repeat_id).append(response[1]);

                }
                else{
                  $(".dependant_radio_subquestion_div_"+prev_ques_id).css("display", "none");
                  $('#subquestion_section_'+radio_repeat_id).hide();
                  $('#subquestion_section_'+radio_repeat_id).empty();
                  $('#subquestion_section_'+radio_repeat_id).append('');

                }
                
	            }

	            //$("#loading").hide();
	              
	            if(inputType == 'checkbox'){
	              //alert(is_checked);
	              if(is_checked){

                  $(".checkbox_dependant_div_"+option_id).css("display", "block");

                  $.each(data, function(key, val) 
                  {
                    var j = parseInt(key)+1;

                    var srno = 'Q.'+ques_sr+'.'+j;

                    var html= '<div class="form-group form-row mt-2"><label class="mr-3">'+srno+'&nbsp;&nbsp;'+val.question_text+'</label><div>'+val.html_tag+'</div></div>';

                    $('#repeat_section_'+option_id+'_'+j).show();
                    $('#repeat_section_'+option_id+'_'+j).empty();
                    $('#repeat_section_'+option_id+'_'+j).append(html);

                  });

	              }
  	              if(response[1] != ''){

                    $(".dependant_checkbox_subquestion_div").css("display", "block");
                    $('#subquestion_section_'+option_id).show();
                    $('#subquestion_section_'+option_id).append(response[1]);
                  }
                  else{
                    $(".dependant_checkbox_subquestion_div").css("display", "none");
                  }
	            }
	            else{

                $.each(data, function(key, val) 
                {
                  var j = parseInt(key)+1;

                  var srno = 'Q.'+ques_sr+'.'+j;

                  var html= '<div class="form-group form-row mt-2"><label class="mr-3">'+srno+'&nbsp;&nbsp;'+val.question_text+'</label><div>'+val.html_tag+'</div></div>';

                  if(p_ques_id!=''){
                    var pr_ques_id = p_ques_id+'_'+option_id+'_'+j;
                    var prev_ques_id = p_ques_id+'_'+prev_radio+'_'+j;
                  }
                  else{
                    var pr_ques_id = option_id+'_'+j;
                    var prev_ques_id = prev_radio+'_'+j;
                  }


                  $('#repeat_section_'+prev_ques_id).hide();
                  $('#repeat_section_'+prev_ques_id).empty();
                  $('#repeat_section_'+prev_ques_id).append('');

                  //alert(val.html_tag);

                  $('#repeat_section_'+pr_ques_id).show();
                  $('#repeat_section_'+pr_ques_id).empty();
                  $('#repeat_section_'+pr_ques_id).append(html);

                });
	             
	            }


	          }//response
            else{
              if(inputType == 'select'){
                

                if(p_ques_id!=''){
                  var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_select;
                  var select_name = p_ques_id+'_'+question_id;

                  var prev_select = $('#prev_select_'+select_name).val();
                 }
                else{
                  var prev_ques_id = question_id+'_'+prev_select;
                  var select_name = question_id;

                  var prev_select = $('#prev_select_'+select_name).val();
                 }

                $(".dropdown_dependant_div_"+prev_ques_id).css("display", "none");  // if no dependant question
                $('#prev_select_'+select_name).val(option_id);

                $(".dependant_dropdown_subquestion_div_"+prev_ques_id).css("display", "none");
                //$('#subquestion_section_'+option_id).empty();
                
              }

              if(inputType == 'radio'){
               

                if(p_ques_id!=''){
                  var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_radio;
                  var radio_name = p_ques_id+'_'+question_id;

                  var prev_radio = $('#prev_radio_'+radio_name).val();

                }
                else{
                  var radio_name = question_id;
                  var prev_radio = $('#prev_radio_'+radio_name).val();
                  var prev_ques_id = question_id+'_'+prev_radio;
               }

               $(".radio_dependant_div_"+prev_ques_id).css("display", "none");
                
                $('#prev_radio_'+radio_name).val(option_id);

                $(".dependant_radio_subquestion_div_"+prev_ques_id).css("display", "none");
                //$('#subquestion_section_'+option_id).empty();
              }

            }
           

	        }//success
	      }) //ajax

    } //if
    else{
      if(inputType == 'checkbox'){

        $(".checkbox_dependant_div_"+option_id).css("display", "none");  // if no dependant question

        $(".dependant_checkbox_subquestion_div").css("display", "none");
        $('#subquestion_section_'+option_id).empty();
      }
      else{

       if(inputType == 'single_select'){

            if(p_ques_id != ''){
                var select_name = p_ques_id+'_'+question_id;
                
                var prev_select = $('#prev_select_'+select_name).val();

                var pr_ques_id = p_ques_id+'_'+question_id+'_'+option_id;
                var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_select;
              }
              else{
                var select_name = question_id;
                
                var prev_select = $('#prev_select_'+select_name).val();

                var pr_ques_id = question_id+'_'+option_id;
                var prev_ques_id = question_id+'_'+prev_select;

              }
              
              $(".dropdown_dependant_div_"+prev_ques_id).css("display", "none");
              $(".dependant_dropdown_subquestion_div_"+prev_ques_id).css("display", "none");

              $('#prev_select_'+select_name).val(option_id);

            }

            else if(inputType == 'radio'){

                if(p_ques_id != ''){
                  var radio_name = p_ques_id+'_'+question_id;
                  
                  var prev_radio = $('#prev_radio_'+radio_name).val();

                  var pr_ques_id = p_ques_id+'_'+question_id+'_'+option_id;
                  var prev_ques_id = p_ques_id+'_'+question_id+'_'+prev_radio;
                }
                else{
                  var radio_name = question_id;
                 
                  var prev_radio = $('#prev_radio_'+radio_name).val();

                  var pr_ques_id = question_id+'_'+option_id;
                  var prev_ques_id = question_id+'_'+prev_radio;
                }

                $(".radio_dependant_div_"+prev_ques_id).css("display", "none");
                $(".dependant_radio_subquestion_div_"+prev_ques_id).css("display", "none");
                
                $('#prev_radio_'+radio_name).val(option_id);

                // $('#subquestion_section_'+option_id).empty();
              }

       
       
      }
    }
  });

  $(document).on('keyup', '.dependantcls', function(e) {

    var inputType = $(this).attr('type');
   
    var text_value  =  $(this).val();
    var question_id =  $(this).attr('id');
    var option_data = $(this).data('id');
    var option_id = $(this).attr("data-optn-id");
    var ques_sr = $(this).attr("data-srno");
    //alert(question_id+'---'+option_data);
    var flag = 1;
    var error_cnt = 0;

    
    

    if(option_data != '' && text_value != ''){

      if(inputType == 'text'){
        //alert(option_data[0].question_id);
        if(option_data[0].validation_id == '1'){ //Number
          //alert('--');
         if(option_data[0].sub_validation_id == '16'){
              var regex = /^\d*[.]?\d*$/;          //Is Number
              if(regex.test(text_value)){         
                $('#text_error_numonly_'+question_id).text("");
                flag = 1;
              } 
              else{
                //if(option_data[0].validation_label != ''){
                  //$('#text_error_numonly_'+question_id).text(option_data[0].validation_label);
                   //$('#text_error_numonly_'+question_id).text("Please enter only Digits");
                  flag = 0;
                //}
              /*  else{
                  $('#text_error_numonly_'+question_id).text("Please enter only Digits");
                  error_cnt = 1;
                }*/
               
              }
          }   //alert(option_data[0].sub_validation_id);
          else if(option_data[0].sub_validation_id == '1'){  //Greater than
           
              if(parseInt(text_value) > parseInt(option_data[0].min_value) == true){
                flag = 1;
                //alert('true');
              }
              else{
                flag = 0;
                //alert('f');
              }
            }
            else if(option_data[0].sub_validation_id == '2'){ //Greater than or equal to
              if((parseInt(text_value) >= parseInt(option_data[0].min_value)) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '3'){ //Less than
              if((parseInt(text_value) < parseInt(option_data[0].min_value)) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '4'){ //Less than or equal to
              if((parseInt(text_value) <= parseInt(option_data[0].min_value)) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '5'){ //Equal to
              if((parseInt(text_value) == parseInt(option_data[0].min_value)) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }

            else if(option_data[0].sub_validation_id == '6'){ //Not Equal to
              if((parseInt(text_value) != parseInt(option_data[0].min_value)) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '7'){ //Between
              if(((parseInt(text_value) <= parseInt(option_data[0].max_value)) && (parseInt(text_value) >= parseInt(option_data[0].min_value))) == true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '8'){ //not Between
              if((parseInt(text_value) < parseInt(option_data[0].max_value) && parseInt(text_value) > parseInt(option_data[0].min_value)) != true){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }

            if(flag == 0){
              $('#text_error_'+question_id+'_'+option_id).text(option_data[0].validation_label);
              //return 0;
            }
            else{
              $('#text_error_'+question_id+'_'+option_id).text('');
              //return 1;
            }
            
          }
          else if(option_data[0].validation_id == '2'){       //Text 
            if(option_data[0].sub_validation_id == '9'){      //Contains
              if(text_value.indexOf(option_data[0].min_value) != -1){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }
            else if(option_data[0].sub_validation_id == '10'){  //Doesnt Contain
              if(text_value.indexOf(option_data[0].min_value) == -1){
                flag = 1;
              }
              else{
                flag = 0;
              }
            }

            if(flag == 0){
              $('#text_error_'+question_id+'_'+option_id).text(option_data[0].validation_label);
              //return 0;
            }
            else{
              $('#text_error_'+question_id+'_'+option_id).text('');
              //return 1;
            }
          }
          else if(option_data[0].validation_id == '4'){       //Length 
            if(option_data[0].sub_validation_id == '15'){      //Minimum Length
              //alert(text_value.length+'---------'+option_data[0].min_value);
              if(text_value.length < option_data[0].min_value){

                $('#text_error_'+question_id+'_'+option_id).text('Please Enter Length Greater than '+option_data[0].min_value);
              
              }
              else{
                $('#text_error_'+question_id+'_'+option_id).text('');
              }
            }
          }
       
        } //textbox
       
        //alert(option_data[0].if_sub_question_checked+'---'+question_id); 

          if(option_data[0].if_sub_question_checked == 'Yes'){
            
           
            if(flag == 1 && error_cnt == 0){
              
              $.ajax({

                  url: site_path+"xAdmin/question/get_dependant_question",
                  type: 'POST',
                  data: {'ci_csrf_token':'', question_id:question_id, option_id:option_id, srno:ques_sr},
                  success: function(result){
                    if(result!='')
                    {   
                      var response = result.split('$$$');
                      // alert(question_id);
                      var data = jQuery.parseJSON(response[0]);

                      if(inputType == 'text'){
                        $(".textbox_dependant_div_"+question_id+'_'+option_id).css("display", "block");

                      }
                      
                      $.each(data, function(key, val) 
                      {
                        var j = parseInt(key)+1;

                        var html= '<div class="form-group form-row mt-2"><label class="mr-3">'+val.question_text+'</label><div>'+val.html_tag+'</div></div>';

                        $('#repeat_section_'+option_id+'_'+j).show();
                        $('#repeat_section_'+option_id+'_'+j).empty();
                        $('#repeat_section_'+option_id+'_'+j).append(html);

                      });
                          
                      if(response[1] != ''){
                        $(".dependant_textbox_subquestion_div").css("display", "block");
                        $('#subquestion_section_'+option_id).show();
                        $('#subquestion_section_'+option_id).append(response[1]);
                      }
                    }//response
                  
                  }//success
                }) //ajax
            }
            else{
              if(inputType == 'text'){
                $(".textbox_dependant_div_"+question_id+'_'+option_id).css("display", "none");
                $(".dependant_textbox_subquestion_div").css("display", "none");
                $('#subquestion_section_'+option_id).empty();
              }
            }
           
          } 
    }
    else{
      $(".textbox_dependant_div_"+question_id+'_'+option_id).css("display", "none");
      $(".dependant_textbox_subquestion_div").css("display", "none");
      $('#subquestion_section_'+option_id).empty();
    }

    
  });

$(document).on('keyup', '.textarea_dependantcls', function(e) {

    var question_id =  $(this).attr('id');
    var text_value =  $(this).val();
    var if_sub_question_checked = $(this).data('id');
    var option_id = $(this).attr("data-optn-id");
    var ques_sr = $(this).attr("data-srno");
    
    
    if(text_value != ''){
      if(if_sub_question_checked == 'Yes'){
           
      $.ajax({

          url: site_path+"xAdmin/question/get_dependant_question",
          type: 'POST',
          data: {'ci_csrf_token':'', question_id:question_id, option_id:option_id, srno:ques_sr},
          success: function(result){
            if(result!='')
            {   
              var response = result.split('$$$');
              var data = jQuery.parseJSON(response[0]);

              $(".textarea_dependant_div_"+option_id).css("display", "block");
              
              $.each(data, function(key, val) 
              {
                var j = parseInt(key)+1;

                var html= '<div class="form-group form-row mt-2"><label class="mr-3">'+val.question_text+'</label><div>'+val.html_tag+'</div></div>';

                $('#repeat_section_'+option_id+'_'+j).show();
                $('#repeat_section_'+option_id+'_'+j).empty();
                $('#repeat_section_'+option_id+'_'+j).append(html);

              });
              
              if(response[1] != ''){
                $(".dependant_textarea_subquestion_div").css("display", "block");
                $('#subquestion_section_'+option_id).show();
                $('#subquestion_section_'+option_id).append(response[1]);
              }      
              
            }//response
          
          }//success
        }) //ajax
        
      }
    }
    else{
     $(".textarea_dependant_div_"+option_id).css("display", "none");
    }

    
  });

  $(document).on("keyup", ".calculate_total", function() {
      var sum = 0;
      var id = $(this).attr('id');
      $("#total_cnt_class_"+id).val('');
      $(".textfields_"+id).each(function(){
        sum += +$(this).val();
      });
      $("#total_cnt_class_"+id).val(sum);
  });

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

</script>  