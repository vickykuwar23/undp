<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Block Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Block Master</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Block Master
			  </h3>
				  <a href="<?php echo base_url('xAdmin/block') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->				 
				<div class="card-body">
					 <form method="post" id="subfrm" name="subfrm" role="form" >
						
						  <div class="row">
							<div class="col-6">	
								<div class="form-group">
									<label for="exampleInputEmail1">District Name</label>
									<select name="district_id" id="district_id" class="form-control">
										<option value="">-- Select District --</option>
										<?php foreach($districtList as $district_list){ ?>
										<option value="<?php echo $district_list['district_id'] ?>" <?php if($block_record[0]['district_id'] == $district_list['district_id']){ ?> selected="selected" <?php } ?>><?php echo $district_list['district_name'] ?></option>
										<?php } ?>
									</select>
									<span><?php echo form_error('district_id'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Block Name</label>
									<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?php echo $block_record[0]['block_name'] ?>" maxlength="100" required>
									<span><?php echo form_error('name'); ?></span>
								</div>
								<div class="card-footer1">
									<button type="submit" id="btn_submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
      $('#btn_submit').prop('disabled',true);
	    form.submit();
    }
  });
  
  $.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
  
  $('#subfrm').validate({
    rules: {
		state_id: {
			 required: true,
		}, 	
		name: {
			required: true,
			nowhitespace:true,
			minlength:3
		}
    },
    messages: {
		
		state_id: {
			required: "This field is required"
		},
		name: {
			required: "This field is required",
			minlength: "Enter District name must be at least {0} characters long"
		}
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


