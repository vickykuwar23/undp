<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	  
         <div class="card ">
			<div class="card-header">
				<a href="<?php echo base_url('xAdmin/survey/add') ?>" class="btn btn-info btn-sm float-right" ><i class="fa fa-plus"></i> Add Survey</a>           
			</div>
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover" width="100%">
               <thead>
                  <tr>
                    <th width="1%">Sr.</th>
                    <th width="10%">Category</th>
                    <th width="15%">Title</th>
                    <th width="18%">Description</th>
                    <th width="5%">Parent Questions</th>
                    <th width="5%">User Assigned</th>
                    <th width="5%">Responses Count</th>
                    <th width="15%">Status</th>
                    <th width="16%">Action</th>
                  </tr>
               </thead>
               <tbody>
                <?php 
                  $i=1;         
                  foreach($survey_data as $info)  { ?>
                   <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo ucfirst($info['category_name']) ; ?></td>
                      <td><?php echo ucfirst($info['title']) ; ?></td>   
                      <td><?php echo strlen($info['description']) > 50 ? substr($info['description'],0,50)."..." : $info['description']; ?></td>
                      <td><?php echo $info['total_cnt']; ?></td> 
                      <td><?php echo $info['surveyer_cnt']; ?></td>
                      <td><?php echo $info['response_cnt']; ?></td>
                       <td>
                        <select id="status" class="form-control h-auto status_cls" onchange="change_status(<?php echo $info['survey_id']; ?>, this.value)">
                          <option value="">Select</option>
                          <option value="Draft" <?php if($info['status'] == 'Draft') { echo 'selected="selected"'; } ?>>Draft</option>
                          <option value="Publish" <?php if($info['status'] == 'Publish') { echo 'selected="selected"'; } ?>>Published</option>
                          <option value="Close" <?php if($info['status'] == 'Close') { echo 'selected="selected"'; } ?>>Closed</option>
                          <option value="Cancel" <?php if($info['status'] == 'Cancel') { echo 'selected="selected"'; } ?>>Cancelled</option>
                        </select>
                      </td>

                        
                      <?php /*
                       if($info['status']=="Inactive"){ ?> 
                         <a href="<?php echo base_url(); ?>xAdmin/survey/changeStatus/<?php echo $info['survey_id'].'/'.$info['status']; ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><span class="badge badge-danger">Inactive</span></a>
                       <?php }
                       else if($info['status']=="Active"){ ?>
                         <a href="<?php echo base_url(); ?>xAdmin/survey/changeStatus/<?php echo $info['survey_id'].'/'.$info['status']; ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><span class="badge badge-success">Active</span></a>
                      <?php } */ ?>
                      
                      <td>        
                       <a href="<?php echo base_url();?>xAdmin/survey/edit/<?php echo base64_encode($info['survey_id']); ?>"><i class="fas fa-edit" title="Edit"></i></a>     
                       <a href="<?php echo base_url();?>xAdmin/survey/delete/<?php echo base64_encode($info['survey_id']); ?>" onclick="return confirm_delete(this,event,'Do you really want to delete this record ?', <?php echo $info['survey_id']; ?>)"><i class="far fa-trash-alt" title="Delete"></i></a>
                      
                       <a href="<?php echo base_url();?>xAdmin/survey/assign_users/<?php echo base64_encode($info['survey_id']); ?>"><i class="fas fa-user-plus" title="Assign Users"></i></a> 
                      <?php /*if($info['ques_mapp']>0) { ?>
                         &nbsp; | &nbsp;
                         <a href="<?php echo base_url();?>xAdmin/question_mapping/index/<?php echo base64_encode($info['survey_id']); ?>"><i class="fab fa-quora" title="Map Question"></i></a> 
                         &nbsp;
                      <?php } */ 
                      //if($info['ques_cnt']>0) { ?>
                      &nbsp; | &nbsp; <a <?php if($info['ques_cnt'] == 0 ){ ?>  onclick="Swal.fire('No Question Mapped to Survey')" <?php }else{ ?> href="<?php echo base_url();?>xAdmin/question/view/<?php echo base64_encode($info['survey_id']); ?>" <?php } ?>><i class="fab fa-quora" title="View Question"></i></a> 
                         
                        <?php //if($info['response_cnt']>0) {?>
                            <a <?php if($info['response_cnt'] == 0 ){ ?>  onclick="Swal.fire('No Response Given')" <?php }else{ ?>  href="<?php echo base_url();?>xAdmin/Response/show_responses/<?php echo base64_encode($info['survey_id']); ?>" <?php } ?>><i class="fas fa-reply" title="View Response"></i></a>
                        <?php // }  } ?>
                       

                       <?php // if($info['global_ques_cnt']>0) { ?>
                       <a <?php if($info['global_ques_cnt'] == 0 ){ ?>  onclick="Swal.fire('No Global Question Attached')" <?php }else{ ?> href="<?php echo base_url();?>xAdmin/question/remove_global_question/<?php echo base64_encode($info['survey_id']); ?>" <?php } ?> ><i class="fas fa-question" title="Remove Global Question"></i></a> 
                     <?php // } ?>
  
                    </td>
                  </tr>
                     <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
  
	$("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,

      "columnDefs": [ 
        
          <?php if($this->session->userdata('role_id') == $this->config->item('admin_role_id') || $this->session->userdata('role_id') == $this->config->item('system_admin_id')) { ?>
            { 
              "targets":[8],
              "responsivePriority": -1,
              "targets": -1
            },
          <?php } 
          else{ ?>
            {
              "visible": false,
              "targets": 7
            },
            {
              "visible": false,
              "targets": 8
            },
          <?php } ?>
      ]
    });// Datatable

  
});// ready

function change_status(survey_id, status,){
    
  swal({
      title: "Are you sure ?",
      text: 'Do you want to Change the status ?',
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#0aa89e",
      confirmButtonText: "Yes",
      cancelButtonText: "No",
      closeOnConfirm: false,
      closeOnCancel: true
    }).then(result =>{
      if (result.value)
      {
        $.ajax({
          url: site_path+"xAdmin/survey/changeStatus",
          type: 'POST',
          data: {'ci_csrf_token':'',survey_id:survey_id, status:status},
          success: function(response){    
            if(response != ''){
              swal(response, "", "success");
            }
          }
        }) //ajax
      }
    });
  }// onchange

function confirm_delete(ref,evt,msg,survey_id)
  {

    var msg = msg || false;
    evt.preventDefault();  

    swal({
        title: "Are you sure ?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
          //window.location = $(ref).attr('href');
          $.ajax({
            url: site_path+"xAdmin/survey/check_status",
            type: 'POST',
            data: {'ci_csrf_token':'',survey_id:survey_id},
            success: function(response){    
              if(response != ''){
                 swal(response, "", "warning");
              }
              else{
                window.location = $(ref).attr('href');
              }
            }
          })  
        }
    });

  } 
</script>