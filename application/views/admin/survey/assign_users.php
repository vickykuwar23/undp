<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Survey Listing</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            <h3 class="card-title">Assign Users</h3>
    			  <a href="<?php echo base_url('xAdmin/survey') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
          </div>
          <!-- form start -->
		    <?php if($this->session->flashdata('error')){ ?>
				<div class="alert alert-danger alert-dismissible">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				  <h5><i class="icon fas fa-ban"></i> Error!</h5>
				 <?php echo $this->session->flashdata('error'); ?>
				</div>
		    <?php } ?>

          <?php 
          	if(count($survey_data) > 0){
	            //$users_explode = explode(",",$survey_data[0]['users']);
	            $usersArr = array();
	            foreach($survey_data as $sid){
	              array_push($usersArr, $sid['user_id']);
	            }
            	//print_r($usersArr);
	        }
          ?>

    	<div class="card-body">
            
    		<form action="<?php echo base_url(); ?>xAdmin/survey/assign_users/<?php echo $survey_id;?>" method="post" id="assign_users_form" name="assign_users_form" role="form" >

        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
				<div class="row">
					<input type="hidden" name="s_id" value="<?php echo $survey_id; ?>" />
					<div class="col-12">
						<div class="form-group">
							<label for="title">Assign Users <span style="color: red">*</span></label>
							<select id="users" name="users[]" class="form-control select2" multiple="multiple">
	                       <?php 
	      					 if(sizeof($user_data) > 0){
	                       		foreach($user_data as $user) {

									$roles 		= $this->master_model->getRecords("role_master",array('role_id'=>$user['role_id']));
									$rolename 	= $roles[0]['role_name'];
								?>
	                          		<option value="<?php echo $user['user_id']?>" <?php if(@in_array($user['user_id'], $usersArr)): ?> selected="selected" <?php endif; ?>><?php echo $user['first_name'].' '.$user['last_name'].'&nbsp;('.$rolename.')'; ?></option>
	                       <?php }
	                   	    }?>
	                      </select>
	                      <span id="users_error" style="color: red;"></span>
						</div>	

	    				<div class="card-footer1">
	                        <button type="submit" id="btn_submit" class="btn btn-primary" name="submit">Submit</button>	 
	    				</div>

					</div> <!-- /.col-12 -->

    			</div><!-- /.row -->
    						   
    		</form>

    	</div><!-- /.card-body -->

        </div> <!-- /.card -->
    </div> <!-- /.container-fluid -->

    </section>
</div>
<!-- jquery-validation -->


<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<style type="text/css">
  form .error {
    color: red;
  }
</style>
<script type="text/javascript">
	$(document).ready(function() {
    var base_url = '<?php echo base_url('xAdmin/survey'); ?>';
	    $('#btn_submit').click(function(){
	    	if($('#users').val() == ''){
	    		$('#users_error').text('Please select atleast one user');
	    		return false;
	    	}
	    	else{
	    		$('#users_error').text('');

         /* var msg = "Users Assigned/Removed Successfully";
          swal(msg, "", "success");
          location.href = base_url;
	    		//return true;*/
	    	}
         //return false;
  		});

  });
   
</script>

<script>
   var base_url = '<?php echo base_url('xAdmin/survey'); ?>';
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" });  location.href = base_url; }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php }  ?>
