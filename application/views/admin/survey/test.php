<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	  
         <div class="card ">
			<div class="card-header">
				<a href="<?php echo base_url('xAdmin/survey/submit/add/0/') ?>" class="btn btn-info btn-sm float-right" ><i class="fa fa-plus"></i> Add Survey</a>           
			</div>
         <div class="card-body">
            <table id="example" class="table table-bordered table-hover" width="100%">
               <thead>
                  <tr>
                    <th width="1%">Sr.</th>
                    <th width="10%">Category</th>
                    <th width="15%">Title</th>
                    <th width="40%">Description</th>
                    <th width="17%">Action</th>
                  </tr>
               </thead>
              
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready( function () {
    var site_url = '<?php echo base_url();?>';
    //alert(site_url);
    var table  = $('#example').DataTable({
        "processing": true,
        "serverSide": true,

        "dom": 'Blftirp',
        "stateSave": true,

        "columnDefs": [    
              {             
                "orderable": false,
                "targets": [4],
                "data": null,
                "defaultContent":"<a href='' id='edit' class='fas fa-edit' title='Edit'></a>"
              },
              {
                "orderable": false,
                "className": 'select-checkbox',
                "targets": 1
              },
              { 
                "responsivePriority": -1,
                  "targets": -1
              },
        ],

        "ajax": {
            "url": site_url+"xAdmin/survey/showdata",
            "type": "POST"
        },

        "select": {
          "style":    'multi',
          "selector": 'td:nth-child(2)'
          
            },
    
        "responsive":{
            "details" : {
                    "type" : 'column'
                  }
           },
    });// Datatable

    $('#example').on( 'click', '#edit', function (e) {
      e.preventDefault();
      var data = table.row( $(this).parents('tr') ).data();
      var site_path = '<?php echo base_url(); ?>';
      $.ajax({
        url: site_path+"xAdmin/survey/submit/edit/"+data[0],
        method: 'POST',
        data :  {'ci_csrf_token':''},
        /*success: function(response){
          alert(response);
          //$('.stationclass').html(response);
        }*/
      })//ajax
    });
  });
</script>