<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            	<h3 class="card-title"><?php echo $page_sub_title; ?></h3>
    			<a href="<?php echo base_url('xAdmin/survey') ?>" class="btn btn-info btn-sm pull-right float-right">Back</a>
          </div>
          <!-- form start -->
		  <?php if($this->session->flashdata('error')){ ?>
				<div class="alert alert-danger alert-dismissible">
				  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				  <h5><i class="icon fas fa-ban"></i> Error!</h5>
				 <?php echo $this->session->flashdata('error'); ?>
				</div>
		  <?php } 
		  
		  if($action == "edit"){
			  //print_r($survey_section);
			  $sectionArrs = array();
			  if(count($survey_section) > 0){
				  foreach($survey_section as $sectiondetails){
					  //$listName =  $this->master_model->getRecords("section_master",array('section_id' => $sectiondetails['section_id']));
					  //$section_name_get = $listName[0]['section_id'];
					  array_push($sectionArrs, $sectiondetails['section_id']);
				  }
			  }
		  } else {
			  $sectionArrs = array();
		  }
		
		  ?>

    		<div class="card-body">
            
          <?php if($action == 'add') { ?>
            <form action="<?php echo base_url(); ?>xAdmin/survey/add" method="post" id="survey_form" name="survey_form" role="form" >
          <?php } else { ?>  
    			 <form action="<?php echo base_url(); ?>xAdmin/survey/edit/<?php echo $survey_id;?>" method="post" id="survey_form" name="survey_form" role="form" >
          <?php }?>


          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
    				<div class="row">

    					<div class="col-12">

    						 <div class="form-group">
		                      <label for="category">Category <span style="color: red">*</span></label>
		                      <select class="form-control" id="category" name="category" onchange="get_section(this.value)">
		                        <option value="">Select Category</option>
		                        <?php foreach ($category as $key => $value) { ?>
		                          <option value="<?php echo $value['category_id']; ?>" <?php if(!empty($survey_data[0]['category_id']) && $survey_data[0]['category_id'] == $value['category_id']) {  echo 'selected="selected"'; } ?>><?php echo $value['category_name']; ?></option>
		                        <?php } ?>
		                      </select>
		                      <span><?php echo form_error('category'); ?></span>
		                    </div>

		                    <div class="form-group">
		                      <label for="section">Section<span style="color: red"></span></label>
		                      <select class="choose-tech select2" id="section" name="section[]" multiple="multiple" style="width: 100%;">
		                        <option value="" disabled="disabled">Select Section</option>
		                        <?php foreach ($sectionList as $key => $value) { ?>
		                          <option value="<?php echo $value['section_id']; ?>" <?php if($action == "edit"){ if(in_array($value['section_id'],$sectionArrs)) {  echo 'selected="selected"'; }  } ?>><?php echo $value['section_name']; ?></option>
		                        <?php } ?>
		                      </select>
		                      <span><?php echo form_error('section'); ?></span>
		                    </div>
    							 	
          						 	<div class="form-group">
            								<label for="title">Title <span style="color: red">*</span></label>
            								<input type="text" class="form-control" id="title" name="title" placeholder="Enter Title"  value="<?php if(!empty($survey_data[0]['title'])) { echo $survey_data[0]['title']; } ?>" maxlength="200" />
            								<span><?php echo form_error('title'); ?></span>
          							</div>	

		                    <div class="form-group">
		                      <label for="description">Description <span style="color: red"></span></label>
		                      <textarea class="form-control" id="description" name="description" placeholder="Enter Description" maxlength="200" /><?php if(!empty($survey_data[0]['description'])) { echo $survey_data[0]['description']; } ?></textarea>
		                      <span><?php //echo form_error('description'); ?></span>
		                    </div>

      						<div class="card-footer1">
                    <button type="submit" id="btn_submit" class="btn btn-primary" name="submit">Submit</button>			 
      						</div>

    					</div> <!-- /.col-12 -->

    				</div><!-- /.row -->
    						   
    			</form>

    		</div><!-- /.card-body -->

        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->

    </section>
</div>
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<style type="text/css">
  form .error {
    color: red;
  }
</style>
<script type="text/javascript">
  var action = '<?php echo $action; ?>';

  $(document).ready(function() {
    /*$('#btn_submit').click(function(){
      $('#btn_submit').prop('disabled','true'); 
      return true;
    });*/
  
   
    $("form[name='survey_form']").validate({
      
      rules: {
        title: "required",
        //description: "required",
        category: "required",
		//"section[]": "required"
       },
      // Specify validation error messages
      messages: {
        title: "Please enter Title",
        //description: "Please enter Description",
        category: "Please Select Category",
		//section: "Please Select Section",
       },
      submitHandler: function(form) {
      	$('#btn_submit').prop('disabled','true'); 
        
        form.submit();
      }
    });
  
  });
   
</script>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>




