  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="finnancialYear">
            <div class="col-sm-12">
                <h3 class="title">Last Updated on - <?php echo date('d M Y, h:i A'); ?></h3>
                <!-- Select Finacial Year -->
                <!--<div class="row">
                   
                    <div class="col-sm-4">
                        <div class="form-group">
						<input class="form-control" type="text" placeholder="" value="Assam">
  
                        </div>
                    </div>
                   
                    <div class="col-sm-4">
                        <div class="form-group">
                            <select class="form-control select2">
                            <option selected="selected">Select District</option>
                            <option>Bongaigaon</option>
                            <option>Baska</option>
                            <option>Barpeta</option>
                            <option>Biswanath</option>
                            <option>Cachar</option>
                        </select>
                        </div>
                    </div>

                 
                    <div class="col-sm-4">
                        <div class="form-group">
                            <select class="form-control select2">
                            <option selected="selected">Select Block</option>
                            <option>Boitamari</option>
                            <option>Dangtol</option>
                            <option>Manikpur</option>
                        </select>
                        </div>
                    </div>
                </div>--->
            </div>
        </div>
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-4 col-6">
            <!-- small box -->
            <div class="small-box info-gradient">
              <div class="inner">
                <h3><?php echo @$submitted; ?></h3>

                <p>Total Submitted</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          <div class="col-md-4 col-6">
            <!-- small box -->
            <div class="small-box warning-gradient">
              <div class="inner">
                <h3><?php echo @$approved; ?><sup style="font-size: 20px"></sup></h3>

                <p>Total Approved</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
          
          <div class="col-md-4 col-6">
            <!-- small box -->
            <div class="small-box success-gradient">
              <div class="inner">
                <h3><?php echo @$return; ?></h3>

                <p>Total Returned</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <!-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> -->
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->		
		<div class="row">
		<div class="col-md-6 mb-3">
		<div class="card h-100">
              <div class="card-header border-transparent">
				
				<h3 class="card-title">Statistics</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body px-3 py-0">
			  
				<div class="progress-group mb-4">
				  Total Submit
				  <!--<span class="float-right"><b>40%</b>/100% </span>-->
				  <span class="float-right"><b><?php echo $total_survey; ?>%</b> </span>
				  <div class="progress progress-sm">
					<div class="progress-bar bg-primary" style="width: <?php echo $total_survey; ?>%"></div>
				  </div>
				</div>
				<div class="progress-group mb-4">
				  Total Approved
				  <span class="float-right"><b><?php echo $total_approved; ?>%</b></span>
				  <div class="progress progress-sm">
					<div class="progress-bar bg-warning" style="width: <?php echo $total_approved; ?>%"></div>
				  </div>
				</div>
				<div class="progress-group mb-4">
				 Total Returned
				  <span class="float-right"><b><?php echo $total_return; ?>%</b></span>
				  <div class="progress progress-sm">
					<div class="progress-bar bg-success" style="width: <?php echo $total_return; ?>%"></div>
				  </div>
				</div>
				
              </div>
             
            </div>
		</div>
		<div class="col-md-6 mb-3">
		<div class="card h-100">
              <div class="card-header border-transparent">
                <h3 class="card-title">District wise Statistics</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body px-3 py-0">
                <div class="table-responsive">
                  <!--<table class="table table-striped">
						<tbody>
							<tr>
								<th style="width: 75px">Sr No.</th>
								<th>District</th>
								<th>Total</th>
								<th>Under Process</th>
								<th>Approved</th>
							</tr>
							<tr>
								<td>1.</td>
								<td>Baska </td>
								<td>000</td>
								<td>000</td>
								<td>000</td>
							</tr>
							<tr>
								<td>2.</td>
								<td>Barpeta</td>
								<td>000</td>
								<td>000</td>
								<td>000</td>
							</tr>
							 <tr>
								<td>3.</td>
								<td>Biswanath</td>
								<td>000</td>
								<td>000</td>
								<td>000</td>
							</tr>
							 <tr>
								<td>4.</td>
								<td>Cachar</td>
								<td>000</td>
								<td>000</td>
								<td>000</td>
							</tr>
							 <tr>
								<td>5.</td>
								<td>Chirang</td>
								<td>000</td>
								<td>000</td>
								<td>000</td>
							</tr>

						</tbody>
					</table>--->
					<table class="table table-striped">
						<tbody>							
							<?php echo $response_html; ?>
						</tbody>
						
					</table>
                </div>
                <!-- /.table-responsive -->
              </div>
             
            </div>
            </div>
            </div>
		
		
		
		
		
		
		
		
		
		
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  