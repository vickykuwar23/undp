<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey'); ?>">Survey List</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	  
         <div class="card ">
			<div class="card-header">
				<a href="<?php echo base_url('xAdmin/question/submit/add/0/') ?>" class="btn btn-info btn-sm float-right" ><i class="fa fa-plus"></i> Add Question</a>                        
			</div>
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover" width="100%">
               <thead>
                  <tr>
                     <th width="1%">Sr.</th>
					      <th width="25%">Question Text</th>
                     <th width="10%">Response Type</th>
                     <th width="10%">Is Mandatory ?</th>
                     <th width="10%">Option Count</th>
                     <th width="7%">Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php 
                     $i=1;					
                     foreach($question_list as $info)  { ?>

					         <tr>
         						<td><?php echo $i; ?></td>
                           <td><?php echo $info['question_text'] ; ?></td>
         						<td><?php echo $info['response_type_id'] ; ?></td>   
                           <td><?php echo $info['mandatory']; ?></td> 
                           <td><?php echo $info['cnt']; ?></td>
         						<td>						
         							<a href="<?php echo base_url();?>xAdmin/question/submit/edit/<?php echo base64_encode($info['question_id']); ?>"><i class="fas fa-edit"></i></a> &nbsp; <!-- <i class="fas fa-pencil-alt"></i> -->  
                              <a href="<?php echo base_url();?>xAdmin/question/delete/<?php echo base64_encode($info['question_id']); ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><i class="far fa-trash-alt" title="Delete"></i></a> &nbsp;  
                              <a href="<?php echo base_url();?>xAdmin/question/view/<?php echo base64_encode($info['question_id']); ?>"><i class="fas fa-eye"></i></a> &nbsp;
                           </td>
         					</tr>
                     <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	$("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
});
</script>