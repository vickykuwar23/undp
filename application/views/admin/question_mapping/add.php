<style type="text/css">
  form .error {
    color: red;
  }
</style>

<?php $question_list = $this->master_model->getRecords("survey_question_master"); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Question</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Question</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
    				
        <?php $this->load->view('admin/layouts/admin_alert'); ?>
    		  <div class="card-body">
          
            <form action="<?php //echo base_url('xAdmin/ques/add'); ?>" method="post" id="question_map_form" name="question_mapping_form" role="form" >

              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />

               
      			<div class="row">
                <div class="col-4">  
                  <div class="form-group">
                    <label for="survey">Survey</label>
                    <select class="form-control" id="survey" name="survey">
                      <option value="">Select Survey</option>
                      <?php foreach ($survey_data as $key => $value) { ?>
                        <option value="<?php echo $value['survey_id']; ?>" <?php if ($value['survey_id'] == @$survey_id){ echo 'selected="selected"'; }?>><?php echo $value['title']; ?></option>
                      <?php } ?>
                    </select>
                    <span style="color: red"><?php echo form_error('survey'); ?></span>
                  </div>
                </div>

                <div class="col-4">  
                  <div class="form-group">
                    <label for="section">Section</label>
                    <select class="form-control" id="section" name="section">
                      <option value="">Select Section</option>
                    </select>
                    <span style="color: red"><?php echo form_error('section'); ?></span>
                  </div>
                </div>
					
				<div class="col-4">  
                  <div class="form-group">
                    <label for="section">Sub Section</label>
                    <select class="form-control sub-section" id="sub_section" name="sub_section" >
                      <option value="">Select Sub Section</option>
                    </select>
                    <span style="color: red"><?php echo form_error('sub_section'); ?></span>
                  </div>
                </div>

                <div class="col-12" id="global_question_div" style="display: none">  
                  <div class="form-group">
                    <label for="section">Select Global Question</label>
                    <select class="form-control sub-section" id="global_question" name="global_question" >
                      <option value="">Select Sub Section</option>
                    </select>
                    <span style="color: red"><?php echo form_error('sub_section'); ?></span>
                  </div>
                </div>
              </div>

              <div id="question_table_div">
                </br>
                  <hr>
                    <label for="question_table">Select Questions</label>
                    <table id="question_table" class="table table-bordered table-hover" width="100%">
                      <thead>
                        <tr>
                          <th width="15%"><input type="checkbox" id="selectall" value="selectall"/></th>
                          <th width="1%">status</th>
                          <th width="1%">Question id</th>
                          <th width="1%">Sr.</th>
                          <th width="80%">Question Text</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                    <hr>
                </div>

                <input type="hidden" name="prev" id="prev" value="">
						  
                <div class="card-footer1">                    
					<!-- <button type="submit" class="btn btn-primary" name="save">Save & Add Next</button> -->
					<button type="submit" class="btn btn-primary" id="btn_submit" name="submit">Submit</button>
					<a href="<?php echo base_url('xAdmin/question_mapping') ?>" class="btn btn-primary">Back</a> 
        		</div>

    		    
    	      </form>

    	    </div><!-- /.card-body -->

        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->

    </section>
</div>

<?php //$this->load->view('client_validation'); ?>

<script type="text/javascript">
  var save_str = '';
  var action = '<?php //echo $action; ?>';
  var modal_index = 0;
  var option_cnt_index = 1;
  var display_order_count = 1;
  var opt =0;
  var plus_id = 0;
  var selectedData = [];
  //var table;
  //$('#btn_submit').prop('disabled', true);
  $('#dependant_ques_tbldiv').hide();



  $(document).ready(function() {

  	var selected_section = $('#section').val();
  	if(selected_section == ''){
  		//$('#global_question_div').hide();
     // document.getElementById('global_question_div').style.display = "none";
     $('#global_question_div').css("display","none");
  	}
  	

    var table  = $('#question_table').DataTable({

      "dom": 'Blftirp',
      "stateSave": true,

      "paging":   false,

      "deferRender": true,
      "columnDefs": [   

            { 
              "responsivePriority": -1,
              "targets": -1
            },

            {
              "targets": 0,
              "orderable": false,
              "createdCell": function(td, cellData, rowData, row, col) {
                if (rowData[1] === "yes") {
                  $('input[type="checkbox"]', td).prop('checked', true);
                  //$(td).prop('checked', true);
                }else {
                  $('input[type="checkbox"]', td).prop('checked', false);
                  //$(td).prop('checked', false);
                }
              }
            },

            {
              "visible": false,
              "targets":   1,
            },
            {
              "visible": false,
              "targets":   2,
            },

        ],

        "select": {
          "style":    'multi',
          "selector": 'td:nth-child(2)'
          
            },
    
        "responsive":{
            "details" : {
                    "type" : 'column'
                  }
           },
    });

    // Handle click on "Select all" control
    $('#selectall').on('click', function(){
      // Get all rows with search applied
      var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
     
      if (this.checked) {
        var rowData = table.data().toArray();

        for(var j = 0; j < rowData.length; j++)
        {
          //var data = rowData[j][2];
          var data = JSON.stringify(rowData[j]);
          if(selectedData.indexOf(data) == -1)
          {
            selectedData.push(data);
          }
        }
      }
      else{
        var rowData1 = table.data().toArray();
        for(var i = 0; i < rowData1.length; i++)
        {
          var data1 = JSON.stringify(rowData1[i]);
          for(var j = selectedData.length-1; j >= 0; j--)
          {
            if(selectedData[j] == data1)
            {
              selectedData.splice(j, 1);
            }
          }
        }
      }
   });

    /*$('.btn_check').on('click', function (){
      alert('--');
       if($(this).prop(":checked")!=true){ 
        alert('unchecked');
       }
       else{
        alert('yes');
       }
    });*/

    $('#question_map_form').on('submit', function (e) {
      var question_checked_array = [];
      var question_unchecked_array = [];
      var global_question_id = $('#global_question').val();

      $.each($("input[name='checkbox_data']:checked"), function(){
        question_checked_array.push($(this).val());
        //$('#btn_submit').prop('disabled', false);
      });
      
      //alert(question_checked_array);

      var site_path = '<?php echo base_url(); ?>';
     
      if(question_checked_array.length > 0){
        //var index = array.indexOf(item);
      
        for(i=0; i<question_checked_array.length;i++){
          if (question_checked_array[i] == global_question_id) {
            //var index = array.indexOf('item');
            question_checked_array.splice(i, 1);
          }
        }

        //alert(question_array);

        $.ajax({
          url: site_path+"xAdmin/question_mapping/add",
          type: 'POST',
          data: $('#question_map_form').serialize()+'&'+$.param({'ci_csrf_token':'','mode':'submit',question_checked_array:question_checked_array, question_unchecked_array:question_unchecked_array}),
          success: function(response){
           // alert(response);
           if(response == 1){
            var msg = "Question Assigned Successfully";
            swal(msg, "", "success");
            location.reload();
           }
          }
        })
        //$('#btn_submit').prop('disabled', false);
      }
      else{
        var msg = "Please Select atleast one question";
        swal(msg, "", "warning");
        //$('#btn_submit').prop('disabled', true);
      }
      return false;
    });


    //$('#apply_logical_question').hide();
    $('#survey').change(function(){

     $('#global_question_div').css("display","none");

      var survey_id = $('#survey').val();
      var site_path = '<?php echo base_url(); ?>';
      var mySelect = $('#section');
      mySelect.empty(); 

     
      if(survey_id == ''){
        table.clear().draw();
      }

      $.ajax({

        url: site_path+"xAdmin/question_mapping/get_section",
        type: 'POST',
        data: {'ci_csrf_token':'',survey_id:survey_id},
        success: function(response){
          //console.log(response);
          var res = response.split('%%');

          //var res = response;

          if(res[0] != ''){
            mySelect.append("<option value=''>Select</option>");
            $.each(JSON.parse(res[0]), function(idx, obj) {
              mySelect.append("<option value='"+obj.section_id+"'>"+obj.section_name+"</option>");
              if(action == 'edit'){
                var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
                $('#section option[value='+section_id+']').attr('selected','selected');
              }
            });
          }

          if(res[1] != ''){
            table.clear().draw();
            var section_id = $('#section').val();
            $.each(JSON.parse(res[1]), function(idx, obj) {
              var j = parseInt(idx)+1;
              var str = '<input type="checkbox" style="text-align:right" class="btn_check" name="checkbox_data" id="id_'+obj.question_id+'" value="'+obj.question_id+'" >';
              values = [[str, obj.checked, obj.question_id, j, obj.question_text]];
                
              table.rows.add(values).draw();
             
            });
          }
          else{
            table.clear().draw();
          }

          /*if(res[2] != ''){
            var mySelect1 = $('#global_question');
            mySelect1.empty(); 
            mySelect1.append("<option value=''>Select</option>");
            $.each(JSON.parse(res[2]), function(idx, obj) {
              mySelect1.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
              if(action == 'edit'){
                var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
                $('#section option[value='+section_id+']').attr('selected','selected');
              }
            });
          }
          else{
            var mySelect1 = $('#global_question');
            mySelect1.empty(); 
          }*/

        }//response
      })
    })

    $('#section').change(function(){

      var section_id = $(this).val();
      var survey_id = $('#survey').val();
      var site_path = '<?php echo base_url(); ?>';
      var mySelect = $('#sub_section');
      if(section_id == ''){
        mySelect.empty(); 
      }
      mySelect.empty(); 

      $.ajax({

        url: site_path+"xAdmin/question_mapping/get_sub_section",
        type: 'POST',
        data: {'ci_csrf_token':'',section_id:section_id,survey_id:survey_id},
        success: function(response){
          //alert(response);
          var res = response.split('%%');

          if(res[0] != ''){
            mySelect.append("<option value=''>Select</option>");
            $.each(JSON.parse(res[0]), function(idx, obj) {
              mySelect.append("<option value='"+obj.section_id+"'>"+obj.section_name+"</option>");
              if(action == 'edit'){
                var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
                $('#section option[value='+section_id+']').attr('selected','selected');
              }
            });
          }
          //alert(res[1]);
          if(res[1] != ''){
            var section_id = $('#section').val();
            table.clear().draw();
              $.each(JSON.parse(res[1]), function(idx, obj) {
                var j = parseInt(idx)+1;
                
                var str = '<input type="checkbox" style="text-align:right" class="btn_check" name="checkbox_data" id="id_'+obj.question_id+'" value="'+obj.question_id+'">';
                values = [[str, obj.checked, obj.question_id, j, obj.question_text]];
                
                table.rows.add(values).draw();
                
              });
	        }
	        else{
	            table.clear().draw();
	        }

        	if(section_id != ''){
        	  //$('#global_question_div').show();
            $('#global_question_div').css("display","block");
            //document.getElementById('global_question_div').style.display = "block";

	          if(res[2] != ''){
	            var mySelect1 = $('#global_question');
	            mySelect1.empty(); 
	            mySelect1.append("<option value=''>Select</option>");
	            $.each(JSON.parse(res[2]), function(idx, obj) {
	              mySelect1.append("<option value='"+obj.question_id+"'>"+obj.question_text+"</option>");
                if(res[3] != ''){
                  $('#global_question option[value='+res[3]+']').attr('selected','selected');
                }
	              if(action == 'edit'){
	                var section_id = '<?php //echo $question_data[0]['section_id']; ?>';
	                $('#section option[value='+section_id+']').attr('selected','selected');
	              }
	            });
	          }
	          else{
	            var mySelect1 = $('#global_question');
	            mySelect1.empty(); 
	          }
	        }
	        else{
	        	//$('#global_question_div').hide();
            //document.getElementById('global_question_div').style.display = "none";
            $('#global_question_div').css("display","none");
	        }
        }
      })
    });

    $('#sub_section').change(function(){

      var sub_section_id = $(this).val();
      var survey_id = $('#survey').val();
      var section_id = $('#section').val();
      var site_path = '<?php echo base_url(); ?>';

      $.ajax({

        url: site_path+"xAdmin/question_mapping/get_subsection_questions",
        type: 'POST',
        data: {'ci_csrf_token':'', sub_section_id:sub_section_id, section_id:section_id,survey_id:survey_id},
        success: function(response){
          //alert(response);
         
          if(response != ''){
            
            table.clear().draw();
              $.each(JSON.parse(response), function(idx, obj) {
                var j = parseInt(idx)+1;
                
                var str = '<input type="checkbox" style="text-align:right" class="btn_check" name="checkbox_data" id="id_'+obj.question_id+'" value="'+obj.question_id+'">';
                values = [[str, obj.checked, obj.question_id, j, obj.question_text]];
                
                table.rows.add(values).draw();
                
              });
          }
          else{
              table.clear().draw();
          }

          
        }
      })
    });

    $('#global_question').change(function(){

      var question_id = $(this).val();
      
      var tabledata = [];
      table.rows().every(function (rowIdx, tableLoop, rowLoop) {
        new_data = this.data(); 
        //$('#id_'+question_id).prop('disabled', false);
         var prev = $('#prev').val();
        if(question_id!=''){
	        if(question_id == new_data[2]){
	         
	          if(prev != '' && question_id != prev)
	          {
	            $('#id_'+prev).prop('disabled', false);
	          }
	          
	          $('#id_'+question_id).prop('disabled', true);
	          $('#prev').val(question_id);
	        }
        }
        else{
        	$('#id_'+prev).prop('disabled', false);
        }
      });
     
    });

  }); //ready

  $(document).on('click', '.btn_check', function() {
    var survey = $('#survey').val();
    var section = $('#section').val();
    var sub_section = $('#sub_section').val();

    if($(this).is(':checked') == false){
      var question_id = $(this).val();

      $.ajax({
          url: site_path+"xAdmin/question_mapping/delete_mapped_question",
          type: 'POST',
          data: {'ci_csrf_token':'', 'action': 'delete_mapped_question', 'survey': survey, 'section': section, 'sub_section': sub_section, question_id:question_id},
          success: function(response){
           // alert(response);
           if(response == 1){
            var msg = "Question Unmapped Successfully";
            swal(msg, "", "success");
            //location.reload();
           }
          }
        })
    }
  
  });


  // validate field has value
  function validate_field(textArr){  
    var valid = 1;
    if(textArr.length > 0){ 
      for(var i = 0; i<textArr.length; i++){         
        $("."+textArr[i]).each(function(){
          if($(this).val() == ""){
            $(this).css('border','1px solid red');
            valid = 0;
          } 
        });
      }
    }
    
    if(valid == 1){
      return true;
    }else{
      return false;
    } 
  }

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }
  
  
</script>

<script type="text/javascript">

 /* $(function() {
    $("form[name='question_form']").validate({
      
      rules: {
        question_text: "required",
        option_type: "required",
        'title[]': "required", 
      },
      // Specify validation error messages
      messages: {
        question_text: "Please enter Title",
        option_type: "Please enter Description",
        'title[]': "Please enter Title",
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  });*/
</script>

<script>
   function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
   function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>

