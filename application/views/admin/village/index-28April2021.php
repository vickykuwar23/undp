<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Village Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Village Master</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	   
         <div class="card ">
			<div class="card-header">

				<a href="<?php echo base_url('xAdmin/village/add') ?>" class="btn btn-info btn-sm float-right" title="Add Village Master"><i class="fa fa-plus"></i> Add Village</a>         
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
		 <div class="table-responsive">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>Sr.</th>
					      <th>Village Code</th>
					      <th>Village Name</th>
					      <th>Revenue Name</th>
                     <th>Status</th>
                     <th class="action_th">Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;					
                     foreach($records as $roledata) 
                     { 	
					 
					
					 ?>
      					<tr>
      						<td style="width:1%"><?php echo $i; ?></td>
      						<td style="width:15%"><?php echo $roledata['village_code']; ?></td> 						
      						<td style="width:30%"><?php echo $roledata['village_name']; ?></td> 
      						<td style="width:30%"><?php echo $roledata['revenue_name']; ?></td> 
                        <td><?php      
                           if($roledata['status']=="Active"){
                              $status = 'Inactive';
                           ?>
                           <a href="<?php echo base_url(); ?>xAdmin/village/changeStatus/<?php echo $roledata['village_id'].'/'.$status; ?>"><span class="badge badge-success">Active</span></a><?php }
                              else if($roledata['status']=="Inactive"){
                              $status = 'Active';
                              ?>
                           <a href="<?php echo base_url(); ?>xAdmin/village/changeStatus/<?php echo $roledata['village_id'].'/'.$status; ?>"><span class="badge badge-danger">Inactive</span></a><?php }
                              ?>
                        </td>
      					   <td class="action" style="width:10%">						
      							<a href="<?php echo base_url();?>xAdmin/village/edit/<?php echo base64_encode($roledata['village_id']);  ?>" ><i class="fas fa-edit" title="Edit"></i></a>&nbsp;
      							<a href="<?php echo base_url();?>xAdmin/village/delete/<?php echo base64_encode($roledata['village_id']);  ?>" onclick="return confirm_action(this,event,'Do you really want to delete this record ?')"><i class="fas fa-trash-alt" title="Delete"></i></a>&nbsp; 
      						</td>
      					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});
});
</script>