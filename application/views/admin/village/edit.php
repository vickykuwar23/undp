<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Village Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Village Master</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Village Master
			  </h3>
				  <a href="<?php echo base_url('xAdmin/village') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->				 
				<div class="card-body">
					 <form method="post" id="subfrm" name="subfrm" role="form" >
						<?php //echo ">>>".$village_record[0]['revenue_id']; ?>
						  <div class="row">
							<div class="col-6">	
								<div class="form-group">
									<label for="exampleInputEmail1">Revenue Name<span style="color: red">*</span></label>
									<select name="revenue_id" id="revenue_id" class="form-control">
										<option value="">-- Select Revenue --</option>
										<?php foreach($revenueList as $revenue_list){ 
												$districtName = $this->master_model->getRecords('district_master',array('district_id'=>$revenue_list['district_id']));
										?>
										<option value="<?php echo $revenue_list['revenue_id'] ?>" <?php 
										if($village_record[0]['revenue_id'] == $revenue_list['revenue_id']){ ?> selected="selected" <?php } ?>><?php echo $revenue_list['revenue_name']." - (".$districtName[0]['district_name'].")"; ?></option>
										<?php } ?>
									</select>
									<span><?php echo form_error('revenue_id'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Village Code<span style="color: red">*</span></label>
									<input type="text" class="form-control" id="village_code" placeholder="Enter Village Code" name="village_code" maxlength="10" value="<?php echo $village_record[0]['village_code'] ?>"  />
									<span><?php echo form_error('village_code'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Village Name<span style="color: red">*</span></label>
									<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?php echo $village_record[0]['village_name'] ?>" maxlength="100" required>
									<span><?php echo form_error('name'); ?></span>
								</div>
								<div class="card-footer1">
									<button type="submit" id="btn_submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  
  $.validator.addMethod("specialChars", function( value, element ) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    }, "Please use only numeric value");
	
	$('#village_code').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });
	
  $.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
  
  $('#subfrm').validate({
    rules: {
		revenue_id: {
			 required: true,
		},
		village_code: {
			 required: true,
			 number: true,
			 remote: {
				url: "<?php echo base_url();?>/xAdmin/village/unique_village_ajax",
				type: "post",
				data: {
				 village_code: function() {
				 return $( "#village_code" ).val();
				 },
				 revenue_id: function() {
				 return $( "#revenue_id" ).val();
				 },
				 edit_id: function() {
				 return <?php echo $edit_id; ?>;
				 }
			  },
				async:false
			},
		},	
		name: {
			required: true,
			nowhitespace:true,
			minlength:3,
			remote: {
				url: "<?php echo base_url();?>/xAdmin/village/unique_name_ajax",
				type: "post",
				data: {
				 village_code: function() {
				 return $( "#village_code" ).val();
				 },
				 revenue_id: function() {
				 return $( "#revenue_id" ).val();
				 },
				 name: function() {
				 return $( "#name" ).val();
				 },
				 edit_id: function() {
				 return <?php echo $edit_id; ?>;
				 }
			  },
				async:false
			},
		}
    },
    messages: {
		
		revenue_id: {
			required: "This field is required"
		},
		village_code: {
			 required: "This field is required",
			 remote: "Village code already exist."
		},
		name: {
			required: "This field is required",
			minlength: "Enter Village name must be at least {0} characters long",
			remote: "Village name already exist."
		}
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    },	
	submitHandler: function () {
		$('#btn_submit').prop('disabled',true);
	    form.submit();
    }
  });
});
</script>


