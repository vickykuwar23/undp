<?php 
  //$this->load->view("admin/layouts/admin_header");
 // $logical_conditions = array('Greater than'=>'>', 'Greater than or equal to'=>'>=', 'Less than'=> '<', 'Less than or equal to' => '<=', 'E')
?>

<style>
.fa-info-circle {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.fa-info-circle .tooltiptext {
  visibility: hidden;
  width: 250px;
  background-color: black;
  color: #fff;
  text-align: left;
  border-radius: 6px;
  padding: 5px 0;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.fa-info-circle:hover .tooltiptext {
  visibility: visible;
}
</style>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/survey');?>">Survey List</a></li>
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/response/index/'.base64_encode($survey_id));?>">Responses List</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>  
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
        <div class="card ">
          <div class="card-header">
            <?php /*?><a href="<?php echo base_url('xAdmin/question/list/'.base64_encode($question_list[0]['survey_id']).'/'.base64_encode($question_list[0]['category_id'])) ?>" class="btn btn-primary btn-sm float-right">Back</a><?php */?>
            <h3 class="card-title">Question Bank With Response</h3>

            <?php if($submodule_name == 'Survey_list') { ?>
            	<a href="<?php echo base_url('xAdmin/response/show_responses/'.base64_encode($survey_id)); ?>" class="btn btn-info btn-sm float-right" ></i> Back</a>
            <?php } else { ?>
            
            	<a href="<?php echo base_url('xAdmin/response/index/'.base64_encode($survey_id)); ?>" class="btn btn-info btn-sm float-right" ></i> Back</a>
            <?php } ?>
           
          </div>
          <div class="card-body">
          
            <?php 

            $approver1_role_id = $this->config->item('approver1_role_id');
            $approver2_role_id = $this->config->item('approver2_role_id');

            $system_admin_id = $this->config->item('system_admin_id');
            $admin_role_id = $this->config->item('admin_role_id');
   
            if($this->session->userdata('role_id') == $system_admin_id || $this->session->userdata('role_id') == $admin_role_id) {
                $show_status = 'no';
                $show_preview = 'yes';
                $review_status = 'no';
                //echo 'if';
            }
            else{
              //echo 'else';
              if($last_status){
                //echo 'last_status';
                if(@$last_status[0]['user_id'] == $this->session->userdata('user_id')) {
                  //echo 'same';
                  $show_preview = 'yes';
                  $show_status = 'yes';
                  $review_status = 'no';
                  if(@$last_status[0]['status'] != '') {  
                     $show_preview = 'yes';
                     $review_status = 'no';
                     $show_status = 'no';
                    ?>
                    <div class="row">
                      <div class="col-6">
                        <div class="form-group">
                           <label><?php echo @$last_status[0]['status'].' by '. @$last_status[0]['fullname'];?></label>
                        </div>
                      </div>
                    </div>
                  <?php } 
                  
                }
                else{
                  //echo 'not same';
                  if(@$last_status[0]['status'] != '') { 
                    $show_preview = 'yes';
                    $review_status = 'no';
                    $show_status = 'no';
                    
                    if(@$last_status[0]['status'] == 'Level 1 Approved') { 
                     

                      if($this->session->userdata('role_id') == $approver1_role_id){
                        $review_status_arr = array('Level 1 In Review');
                        $show_preview = 'yes';
                        $review_status = 'no';
                        $show_status = 'no';
                      }
                      else{
                        $review_status_arr = array('Level 2 In Review');

                        $show_preview = 'no';
                        $review_status = 'yes';
                        $show_status = 'yes';
                      } 
                    
                   /* $show_preview = 'yes';
                    $show_status = 'yes';
                    $review_status = 'no';*/
                  }
                } ?>
                <label><?php echo @$last_status[0]['status'].' by '. @$last_status[0]['fullname'];?></label>
             <?php  }
            }
            else{  
              //echo 'final else';
              
              if($this->session->userdata('role_id') == $approver1_role_id){
                $review_status_arr = array('Level 1 In Review');
                $show_preview = 'no';
                $show_status = 'yes';
                $review_status = 'yes';
              }
              else{
                $show_preview = 'yes';
                $show_status = 'no';
                $review_status = 'no';
              }

            ?>
                 
            <?php  }
            }
            ?>
            <?php if($review_status == 'yes') { ?>
             <div class="row">
                <div class="col-6">
                  <div class="form-group">
                   <label>Select Status</label>
                    <select class="form-control" id="review_status">
                        <option value="">Select</option>
                        <?php for($i=0; $i<sizeof($review_status_arr); $i++){ ?>
                          <option value="<?php echo $review_status_arr[$i]; ?>"><?php echo $review_status_arr[$i]; ?></option>
                        <?php }?>
                    </select>
                  </div>
                </div>
              </div>
            <?php } ?>

            <hr>

           <form action="#"  id="response_form" name="response_form" role="form"> 
          <!--  <span style="color: blue; font: bold;"><?php //echo $question_data[0]['survey_name'];?></span>
          <hr>   --> 
          <div id="show_preview" style="display:none;">   
	         <?php
            	echo $html; 

              $config_survey_id = $this->config->item('education_survey_id');
              if($config_survey_id == $survey_id){
                $status_arr = array('Level 1 Approved', 'Level 1 Returned');
              }
              else{
                $approver1_role_id = $this->config->item('approver1_role_id');
                $approver2_role_id = $this->config->item('approver2_role_id');
                if($this->session->userdata('role_id') == $approver1_role_id){
                  $status_arr = array('Level 1 Approved', 'Level 1 Returned');
                }
                else{
                  $status_arr = array('Level 2 Approved', 'Level 2 Returned');
                } 
              }
            
            ?>

            <hr>
            <?php if($show_status == 'yes') { ?>
              <div class="row">
        				<div class="col-6">
                			<div class="form-group">
        			          <label>Select Status</label>
        			          	<select class="form-control" id="status" name="satus">
        		  			        <option value="">Select</option>
                            <?php for($i=0; $i<sizeof($status_arr); $i++){ ?>
        		  			         <option value="<?php echo $status_arr[$i]; ?>" <?php if(trim($status_arr[$i]) == trim($status)) { echo "selected='selected'"; } ?>><?php echo $status_arr[$i]; ?></option>
                            <?php }?>
        			          	</select>
                          <span id="status_error" style="color: red;"></span>
        			        </div>
        			    </div>
        			</div>

    	        <!-- <center>
    	          <div id="loading" class="divLoading"><p>Loading... <img src="<?php echo base_url(); ?>assets/images/loader.gif" /></p></div>
    	        </center> -->
        
              <div class="card-footer1">
                <button type="submit" id="btn_submit" class="btn btn-primary" name="submit">Submit</button>
              </div> 
            <?php } ?>
            </div> 

           </form> 
          
          </div><!-- /.card-body -->

        </div> <!-- /.card -->
      </div> <!-- /.container-fluid -->

  </section>

 </div>
<?php //$this->load->view("admin/layouts/admin_footer"); ?>
<script type="text/javascript">
	$("#loading").hide();

  	$('#response_form input[type="text"]').prop("disabled", true);
  	$('.dependantcls').prop("disabled", true);
  	$('.salutation_class').prop("disabled", true);

  	$(document).ready(function() {

      var review_status = '<?php echo @$show_preview; ?>';
      if(review_status == 'yes'){
        $('#show_preview').css('display', 'block');
      }
	
	 var show_remark = '<?php echo @$show_status; ?>';
	 if(show_remark == 'yes'){
        $('.notok_span_class').css('display', 'block');
      }
	  else{
		  $('.notok_span_class').css('display', 'none');
	  }

      $('#review_status').change(function(){
        //alert('review_status');

        var response_id = $('#response_id').val();
        var survey_id = '<?php echo $survey_id; ?>';
        var review_status = $(this).val();

        $.ajax({
            url: site_path+"xAdmin/Response/change_review_status",
            type: 'POST',
            data: {'ci_csrf_token':'', response_id:response_id, survey_id:survey_id, status:review_status},
            success: function(response){
              
                if(response > 0){
                  var msg = "Status Changed Successfully";
                  swal(msg, "", "success");
                  //location.reload();
                  $('#show_preview').css('display', 'block');
                  $('#review_status').prop('disabled', true);
                }
            }
          }) //ajax
      });

  		$('#btn_submit').click(function(){
        
  			var response_id = $('#response_id').val();
        var survey_id = '<?php echo $survey_id; ?>';
  			var status = $('#status').val();
        var remarkArr = [];

        var base_url = '<?php echo base_url('xAdmin/response/index/'.base64_encode($survey_id)); ?>';
        

        $(".remark_cls").each(function(){
          var id = $(this).attr('id');
          var remark = $(this).val();
          
          if(remark != ''){
            remarkArr.push({'question_id': id, 'remark': remark});
          }
          
        });
       //console.log(remarkArr);

        if(status == ''){ //remarkArr != '' && 
          $('#status_error').text('Please select Status');
          return false;
        }
        else{
          $('#status_error').text('');
          if(status == 'Level 1 Returned' || status == 'Level 2 Returned'){
            if(remarkArr == ''){
              $('#status_error').text('Please Enter Remark');
               return false;
            }
            else{
              $('#status_error').text('');
            }
          }
          $('#btn_submit').prop('disabled','true'); 
    			if(response_id != ''){
  		    	$.ajax({
  			        url: site_path+"xAdmin/Response/change_response_status",
  			        type: 'POST',
  			        data: {'ci_csrf_token':'', response_id:response_id, survey_id: survey_id, status:status, remarkArr: remarkArr},
  			        success: function(response){
  			        	//alert(response);
  			            if(response > 0){
  				            var msg = "Status Changed Successfully";
  				            swal(msg, "", "success");
                      //alert(base_url);
                      location.href = base_url;
  				            //location.reload();
  			            }
  			        }
  		      	}) //ajax
  			 }
        }
	      return false;
		});

    $('.notok_class').click(function(){  
      var is_checked = $(this).is(':checked');
      var value = $(this).val();
    
      if(is_checked){
        $('.show_remark_'+value).css('display', 'block');
      }
      else{ //alert(value);
        $('.show_remark_'+value).css('display', 'none');
		$("#remark_"+value).val('');
		//remark_279
      }
    }); 

   /* $('.'+ques_id).text('');

    $('#tooltip').hover(function(){  
      var ques_id = $('#question_id').val();
      var help_text = $('#help_label').val();
      $('.'+ques_id).text(help_text);
    });*/
    
  });// ready

</script>  