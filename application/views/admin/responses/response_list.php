
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark"><?php echo $page_title; ?></h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active"><?php echo $page_title; ?></li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->

   <section class="content">
      <div class="container-fluid">

        <div class="finnancialYear">
        <div class="box-body">
        <div class="box-header">
           <?php if($redirect_from == 'index') {?>
            <h3 class="box-title mt-1"> Select Survey</h3>
            <div class="row">     
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <select name="survey" id="survey" class="form-control">
                          <option value="" data-id="">Select</option>
                          <?php foreach($survey_data as $survey)  { ?>
                            <option value="<?php echo $survey['survey_id']?>" data-id="<?php echo base64_encode($survey['survey_id']); ?>" <?php if(@$survey_id == $survey['survey_id']) { echo "selected='selected'";  } ?> ><?php echo $survey['title']?></option>
                          <?php }?>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-header with-border">
            <!-- tools box -->
            <div class="pull-right box-tools">
                <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="collapse" data-target="#collapseExample">
                 <i class="fa fa-plus"></i></button>
            </div>
            <!-- /. tools -->
            <h3 class="box-title">Search Filter</h3>
        </div>
        <div class="col-md-12 collapse" id="collapseExample">
            <div class="row">    
                <div class="col-md-3 col-sm-6">
                    <div class="form-group">
                        <label>District</label>
                        <select name="district" id="district" class="custom_filter form-control">
                            <option value="">Select District</option>
                            <?php /*foreach($district_data as $district)  { ?>
                              <option value="<?php echo $district['district_id']?>"><?php echo $district['district_name']?></option>
                            <?php }*/ ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                      <label>Village</label>
                      <select name="village" id="village" class="custom_filter form-control">
                        <option value="">Select Village</option>
                      </select>
                  </div>
                </div>

                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                    <label>Surveyor ID / Name</label>
                    <select name="surveyer" id="surveyer" class="custom_filter form-control">
                      <option value="">Select Surveyer</option>
                      <?php foreach($surveyer_data as $surveyer)  { ?>
                        <option value="<?php echo $surveyer['user_id']?>"><?php echo $surveyer['first_name'].' '.$surveyer['last_name']." (".$surveyer['user_id'].")";?></option>
                      <?php }?>
                    </select>
                  </div>
                </div>

              

                <div class="col-md-3 col-sm-6">
                  <div class="form-group">
                    <label>Status</label>
                    <select name="status" id="status" class="custom_filter form-control">
                      <option value="">Select Status</option>
                    </select>
                  </div>
                </div>

              </div>        

            </div>
             <?php }?>
          </div>
      </div>
	  
        <div class="card ">
        	<?php if($redirect_from == 'show_responses') { ?>
	  			<div class="card-header">
	  				<a href="<?php echo base_url('xAdmin/survey'); ?>" class="btn btn-primary btn-sm float-right">Back</a> 
	  			</div> 
	  		<?php } ?>
         <div class="card-body">   

          <center>
            <div id="loading" class="divLoading" style="display: none;">
              <p>Loading... <img src="<?php echo base_url(); ?>assets/images/loading-4.gif" width="100" height="100" /></p>
            </div>
          </center> 
          
          <?php //if($redirect_from == 'show_responses') { ?>
            <table id="response_table" class="table table-bordered dt-responsive table-hover" width="100%">
               <thead>
                  <tr>
                    <!-- <th width="1%">Expand</th>
                    <th width="1%">Sr.</th>
					          <th width="15%">Survey</th>
                    <th width="15%">Surveyer</th>
                    <th width="10%">District</th>
                    <th width="10%">Revenue</th>
                    <th width="10%">Development Block</th>
                    <th width="10%">Gaon Panchayat</th>
                    <th width="10%">Village</th>
                    <th width="15%">Status</th>
                    <th width="10%">Start Date</th>
                    <th width="10%">End Date</th>
                    <th width="2%">Action</th> -->
                    <th width="1%">Sr.No.</th>
                    <th width="10%">Response ID</th>                
                    <th width="20%">Submitted Date/Time</th>
                    <!-- <th width="15%">Submitted Time</th> -->
                    <th width="20%">Surveyor Name (ID)</th>
                    <!--<th width="15%">Surveyer ID</th>-->
                    <th width="15%">District</th>
                    <th width="15%">Village (ID)</th>
                    <th width="15%">Status</th>
                    <th width="10%">Action</th>
                  </tr>
               </thead>
               <tbody>
                <?php /*if($redirect_from == 'show_responses') {
                  $i=1;					
                  foreach($response_header_data as $response)  { ?>
					          <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $response['response_id']; ?></td>   
                      <td><?php echo $response['submitted_date'].'/'.$response['submited_time'];; ?></td>
                      <td><?php echo ucfirst($response['surveyer_name']).' ('.$response['surveyer_id'] .')'; ?></td>   
                     
                      <td><?php echo ucfirst($response['district_name']); ?></td> 	
                      <td><?php echo $response['village_name'] .'('.$response['village_id'].')'; ?></td> 
                      <td>
                        <?php 
                        if($response['status'] == 'Submitted' || $response['status'] == 'Re-Submitted') { 
                            $text = $response['status'];
                            $class = 'badge badge-success';
                        }
						            else if($response['status'] == 'Level 1 In Review' || $response['status'] == 'Level 2 In Review') { 
                            $text = $response['status'];
                            $class = 'badge badge-warning';
                        } 
                        else if($response['status'] == 'Level 1 Approved' || $response['status'] == 'Level 2 Approved') { 
                            $text = $response['status'];
                            $class = 'badge badge-success';
                        } 
                        else if($response['status'] == 'Level 1 Returned' || $response['status'] == 'Level 2 Returned') { 
                            $text = $response['status'];
                            $class = 'badge badge-danger';
                        } 
                        ?>
                        <span class="<?php echo $class; ?>"><?php echo $text; ?></span>

                      </td>
                      <td>  	
                      <?php  //echo $response['ques_cnt']; //if($response['ques_cnt'] > 0) { ?>		
         				<a  href="<?php echo base_url();?>xAdmin/response/response_preview/<?php echo base64_encode($response['response_id']); ?>/<?php echo base64_encode('Survey_list'); ?>"><i class="fas fa-eye" title="View Response"></i></a>&nbsp;     
                      
                     <?php //} ?>

         						</td>
         					</tr>
                    <?php $i++; } 
                   }*/?>
                  </tbody>
            </table>
          
            <!-- ./col -->
         </div>
         </div>

         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {

  var survey_id = $('#survey').val();
  var district_id = $('#district').val();
  var village_id = $('#village').val();
  var surveyer_id = $('#surveyer').val();
  var status = $('#status').val();

  //alert(survey_id);
  <?php if($redirect_from == 'index') { ?>
	  if(survey_id != ''){
	    var option_id = $('#survey').children(":selected").attr('data-id');
	    //get_datatable(option_id);
      get_district(option_id)
	  }
	<?php } ?>  
  //else{
    //alert('else');
  $('#survey').change(function(){
    var survey_id = $(this).children(":selected").attr('data-id');
    //alert(survey_id);
    //get_datatable(survey_id);
    dataTable.draw();
    get_district(survey_id);

    var mySelect1 = $('#status');
    mySelect1.empty(); 

    var config_survey_id = '<?php echo $this->config->item('education_survey_id'); ?>';
    var survey_id_value = $('#survey').val();
     
    if(config_survey_id == survey_id_value){
      var status_arr = new Array('Submitted', 'Re-Submitted', 'Level 1 Approved', 'Level 1 Returned');
    }
    else{
      var status_arr = new Array('Submitted', 'Re-Submitted', 'Level 1 Approved', 'Level 1 Returned', 'Level 2 Approved', 'Level 2 Returned');
    }

    mySelect1.append("<option value=''>Select</option>");
    for(i=0;i<status_arr.length;i++){
      mySelect1.append("<option value='"+status_arr[i]+"'>"+status_arr[i]+"</option>");
    }
  });
  //}

 	<?php if($redirect_from == 'index') { ?>
	  if(district_id != ''){
	    get_village(district_id, survey_id);
	  }
    <?php } ?>

    
    var dataTable = $('#response_table').DataTable({
      'processing': true,
      'serverSide': true,
      'serverMethod': 'post',
      //'searching': false, // Remove default Search Control
      'ajax': {
         "url": site_path+"xAdmin/Response/show_responses",
         'data': function(data){
            // Read values
            var survey_id = $('#survey').val();
            var district_id = $('#district').val();
            var village_id = $('#village').val();
            var surveyer_id = $('#surveyer').val()
            var status = $('#status').val();
            
            // Append to data
            //data.searchByGender = gender;
            data.survey_id   = survey_id;
            data.district_id = district_id;
            data.village_id  = village_id;
            data.surveyer_id = surveyer_id;
            data.status      = status;
         }
      },
      'columns': [
         { data: 'sr' }, 
         { data: 'response_id' }, 
         { data: 'submitted_date' },
         { data: 'surveyer_name' },
         { data: 'district_name' },
         { data: 'village_name' },
         { data: 'status' },
         { data: 'action' },
      ]
    });

    
    $('.custom_filter').change(function(){
      dataTable.draw();
    });

  //else{
    //alert('else');
    $('#district').change(function(){
      var district_id = $('#district').val();
      var survey_id = $('#survey').val();
      get_village(district_id, survey_id);	 	
	   
    });
  //}
  
  //alert(survey_id);
    <?php if($redirect_from == 'index') { ?>
	  if(village_id != ''){
	    get_datatable_vlg(village_id, surveyer_id, survey_id, district_id, status);
	  }
	<?php } ?>  

  var surveyer_id = $('#surveyer').val();
  //alert(survey_id);
    <?php if($redirect_from == 'index') { ?>
	  if(surveyer_id != ''){
	    get_datatable_vlg(village_id, surveyer_id, survey_id, district_id, status);
	  }
	<?php } ?>
 
  //alert(survey_id);
    <?php if($redirect_from == 'index') { ?>
	  if(status != ''){
	    get_datatable_vlg(village_id,surveyer_id, survey_id, district_id, status);
	  }
	<?php }?>
 

});//ready



  function get_district(survey_id){

      $('.divLoading').css('display','block');

      var mySelect = $('#district');
      mySelect.empty(); 

      var mySelect1 = $('#status');
      mySelect1.empty(); 
      //alert('else');
      $.ajax({
          url: site_path+"xAdmin/Response/get_district",
          type: 'POST',
          data: {'ci_csrf_token':'', survey_id:survey_id, open_response_page:'No'},
          success: function(result){
            $('.divLoading').css('display','none');
            
            if(result != ''){
              mySelect.append("<option value=''>Select</option>");
              $.each(JSON.parse(result), function(idx, obj) {
                mySelect.append("<option value='"+obj.district_id+"'>"+obj.district_name+"</option>");
              });
            }

          }
        }) //ajax
      
    }// survey change*/

    function get_village(district_id, survey_id){

      $('.divLoading').css('display','block');

      var mySelect = $('#village');
      mySelect.empty(); 
		
      $.ajax({
          url: site_path+"xAdmin/response/get_village",
          type: 'POST',
          data: {'ci_csrf_token':'',district_id:district_id, survey_id:survey_id},
          success: function(response){

            $('.divLoading').css('display','none');
           //alert(response);
            //var res = response.split('$$$');
           
            if(response != ''){
              mySelect.append("<option value=''>Select</option>");
              $.each(JSON.parse(response), function(idx, obj) {
                mySelect.append("<option value='"+obj.village_id+"'>"+obj.village_name+' ('+obj.village_code+')'+"</option>");
              });
            }
           
          }
        }) //ajax
    }

    function get_datatable_vlg(village_id, surveyer_id, survey_id, district_id, status){
        
        $('.divLoading').css('display','block');

        $.ajax({
          url: site_path+"xAdmin/response/get_datatable",
          type: 'POST',
          data: {'ci_csrf_token':'', village_id:village_id, surveyer_id:surveyer_id, survey_id:survey_id, district_id:district_id, status:status},
          success: function(response){

            $('.divLoading').css('display','none');
           //alert(response);
            //var res = response.split('$$$');
            if(response != ''){
              $("#example1").DataTable().clear().draw();
              $.each(JSON.parse(response), function(idx, obj) {

                var j = parseInt(idx)+1;
               
                var dropdown = '';
                if(obj.status == 'Submitted' || obj.status == 'Re-Submitted') { 
                  var text = obj.status;
                  var span_class = 'badge badge-success';
                }
                else if(obj.status == 'Level 1 In Review' || obj.status == 'Level 2 In Review') { 
                  var text = obj.status;
                  var span_class = 'badge badge-warning';
                }
                else if(obj.status == 'Level 1 Approved' || obj.status == 'Level 2 Approved') { 
                  var text = obj.status;
                  var span_class = 'badge badge-success';
                } 
                else if(obj.status == 'Level 1 Returned' || obj.status == 'Level 2 Returned') { 
                  var text = obj.status;
                  var span_class = 'badge badge-danger';
                } 
              	var dropdown = '<span class="'+span_class+'">'+text+'</span>';

                var action = '<a href="javascript:void(0)" onclick="open_page('+obj.ques_cnt+','+obj.response_id+')"><i class="fas fa-eye" title="View Response"></i></a>';
                

                //var action = '<a '+if(obj.ques_cnt == 0) { + onclick="Swal.fire('No Response to show')"+ else{+ 'href="'+site_path+"xAdmin/response/response_preview/"+btoa(response_id)+'/'+btoa('Response_list')+'"'+}'  onclick="open_page('+obj.ques_cnt+','+obj.response_id+')"><i class="fas fa-eye" title="View Response"></i></a>';

                //values = [['', j, obj.survey_name, obj.surveyer_name, obj.district_name, obj.revenue_name, obj.block_name, obj.panchayat_name, obj.village_name, dropdown, obj.start_date, obj.end_date, action]];

                values = [[j, obj.response_id, obj.submitted_date+' '+obj.submited_time, obj.surveyer_name+'('+obj.surveyer_id+')', obj.district_name, obj.village_name+' ('+obj.village_id+')', dropdown, action]];
   
   
                  $("#example1").DataTable().rows.add(values).draw();
               
              });
            }
            else{
            	$("#example1").DataTable().clear().draw();
              swal('No Data Found', "", "info"); 
              $('.divLoading').css('display','none');
            }
          }
        }) //ajax
    }

    function open_page(ques_cnt, response_id){
      /*if(ques_cnt == 0) {
        Swal.fire('No Response to show');
      }
      else{*/
        window.location.href = site_path+"xAdmin/response/response_preview/"+btoa(response_id)+'/'+btoa('Response_list');
      //}
    }// onchange

    function change_status(response_id, status){
  
      swal({
          title: "Are you sure ?",
          text: 'Do you want to Change the status ?',
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#0aa89e",
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          closeOnConfirm: false,
          closeOnCancel: true
        }).then(result =>{
          if (result.value)
          {  
            $.ajax({
              url: site_path+"xAdmin/response/changeStatus",
              type: 'POST',
              data: {'ci_csrf_token':'',response_id:response_id, status:status},
              success: function(response){    
                if(response != ''){
                  swal(response, "", "success");
                }
              }
            }) //ajax
          }
        });
    }// onchange
</script>