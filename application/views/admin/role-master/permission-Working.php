<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Permission</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Permission</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Add/Update Role Permission
			  </h3>
				 <a href="<?php echo base_url('xAdmin/role') ?>" class="btn btn-info btn-sm pull-right float-right"><i class="fa fa-backward"></i> Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php } ?>
				<div class="card-body">
					 <form method="post" id="subfrm" name="subfrm" role="form" >
						  <div class="row">
							<div class="col-12">
							 <button type="submit" class="btn btn-primary" name="btn_save" id="btn_save">Submit</button><br />
							<div class="table-responsive">
								<table class="table table-bordered table-hover" id="role-check">
									<thead>
										<tr>
										<th class="text-center" ><input type="checkbox" id="selectAll" /></th>
										<th class="text-center">Module ID</th>
										<th>Module Name</th>
										<th class="text-center">Insert/Add</th>
										<th class="text-center">Edit/Update</th>
										<!-- <th class="text-center">Delete/Remove</th> -->
										<th class="text-center">View</th>
										<!-- <th class="text-center">Export</th> -->
										<th class="text-center">Active/Inactive</th>
									</tr>
									</thead>
									<?php $i=1; foreach($module_list as $mList){
										error_reporting(0);	
										$checkAdd = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'add'));
										
										$checkEdit = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'edit'));
										
										$checkDelete = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'delete'));
										
										$checkView = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'view'));
										
										$checkExport = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'export'));
										
										$checkExport = $this->master_model->getRecords("permission",array('module_id'=>$mList['module_id'], 'role_id' => $role_id, 'permission_name' => 'status'));
										
									?>
									<tr>
										<td class="text-center"><input type="checkbox" id="<?php echo $i; ?>" class="checkAll" value="<?php echo $mList['module_id'] ?>" name="m_id[]"/></td>
										<td class="text-center"><?php echo $mList['module_id']; ?></td>
										<td><?php echo ucwords($mList['module_name']); ?></td>
										<td class="text-center"><input type="checkbox" name="module_id[<?php echo $mList['module_id'] ?>][]" id="module_id" class="form_control check" value="add" <?php if($checkAdd[0]['permission_name'] == "add"){ ?> checked="checked" <?php } ?>  /></td>
										<td class="text-center"><input type="checkbox" name="module_id[<?php echo $mList['module_id'] ?>][]" id="module_id" class="form_control check" value="edit" <?php if($checkEdit[0]['permission_name'] == "edit"){ ?> checked="checked" <?php } ?>  /></td>
										
										<td class="text-center"><input type="checkbox" name="module_id[<?php echo $mList['module_id'] ?>][]" id="module_id" class="form_control check" value="view" <?php if($checkView[0]['permission_name'] == "view"){ ?> checked="checked" <?php } ?>  /></td>
										
										<td class="text-center"><input type="checkbox" name="module_id[<?php echo $mList['module_id'] ?>][]" id="module_id" class="form_control check" value="status" <?php if($checkExport[0]['permission_name'] == "status"){ ?> checked="checked" <?php } ?>  /></td>
										
									</tr>
									<?php $i++; } ?>
								</table>
							</div>
							</div>
							
								<!-- /.card-body -->
						  </div>
							<input type="hidden" name="role_id" value="<?php echo $role_id; ?>" />
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">

function addeditPermission(module_id, role_id, action_name)
{
	
	$.ajax({          
		type: "POST",
		url: "<?php echo base_url('xAdmin/role/updatePermission');?>",			
		data:{module_id:module_id, role_id:role_id, action_name:action_name},			
		success: function(res){ 
			//console.log(res);
		   //$('#district_id').html(res);              
		}			
	});
	
}
$(document).ready(function () {
	
	$(".yourButtonClass").on('click', function(event){
		event.stopPropagation();
		event.stopImmediatePropagation();
		//(... rest of your JS code)
	});
	
	// Check All Checkbox
	 $('#selectAll').click(function(e){
		var table= $(e.target).closest('table');
		$('td input:checkbox',table).prop('checked',this.checked);
	});
	
	// Row wise checkboxes
	$(".checkAll").on("change",function(){
	   if($(this).is(":checked")) 
	   $(this).closest("tr").find(".check").prop('checked',true);
	   else
		$(this).closest("tr").find(".check").prop('checked',false);
	});
	
	//
	
	/*$('#btn_save').submit(function() 
	{
		alert("Done");
		if($('.check:checkbox:checked').length == 0)
		{
			var msg = 'Please select at least one checkbox';
			sweet_alert_error(msg);
		}
		else
		{
			$.ajax({          
				type: "POST",
				url: "<?php echo base_url('xAdmin/role/updatePermission');?>",			
				data:{module_id:module_id, role_id:role_id, action_name:action_name},			
				success: function(res){ 
					console.log(res);
				   //$('#district_id').html(res);              
				}			
			});		
		}   
	});*/
	
});

$(document).ready(function() {                                            
    $("#subfrm").validate({
        submitHandler: function(form) {
            var boxes = $('.check:checkbox');
            if(boxes.length > 0) {
                if( $('.check:checkbox:checked').length < 1) {                  
					 swal({
                        title: 'Error!',
                        text: "Please select at least one checkbox",
                        type: 'error',
                        //showCancelButton: false,
                        confirmButtonText: 'Ok'
                    }); 
                    boxes[0].focus();
                    return false;
                }
            }
            form.submit();
        }
    }); 

});

$("body").on("click", "#btn_save", function (e) {
		
		
		

		
        /*var teamStatus = $(this).val();           
        var tid    =    $(this).attr('data-id');       
        var csrf_test_name    = $('.token').val();           
        var base_url = '<?php echo base_url('xAdmin/team/updateTeamStatus'); ?>';
        $('#preloader-loader').css('display', 'block');   
         $.ajax({
            url: base_url,
            type: "post",
            data: {id:tid,csrf_test_name:csrf_test_name,tstatus:teamStatus},               
            success: function (response) {
                $('#preloader-loader').css('display', 'none');   
                var returndata = JSON.parse(response);
                var token = returndata.token;
                $(".token").val(token);    
                swal({
                        title: 'Success!',
                        text: "Team Status updated successfully.",
                        type: 'success',
                        showCancelButton: false,
                        confirmButtonText: 'Ok'
                    });   
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });*/
    });
</script>


