<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Branch/Office</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Branch/Office</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
	  
         <div class="card ">
			<div class="card-header">
				
				<a href="<?php echo base_url('xAdmin/branch/add') ?>" class="btn btn-primary btn-sm float-right">Add Branch/Office</a>              
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                     <th>No.</th>
					 <th>Name</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  <?php  
                     $i=1;					
                     foreach($records as $roledata) 
                     { 					 
					 ?>
					<tr>
						<td style="width:5%"><?php echo $i; ?></td>
						<td style="width:70%"><?php echo $roledata['branch_name'] ; ?></td>         
						 <td style="width:20%">						
							<a href="<?php echo base_url();?>xAdmin/branch/edit/<?php echo base64_encode($roledata['branch_id']);  ?>" ><button class="btn btn-success btn-sm">Edit</button></a>                       
							<?php      
							if($roledata['status']=="Active"){
								$status = 'Inactive';
							?>
							<a href="<?php echo base_url(); ?>xAdmin/branch/changeStatus/<?php echo $roledata['branch_id'].'/'.$status; ?>"><button class="btn btn-info btn-sm">Active </button> </a><?php }
							   else if($roledata['status']=="Inactive"){
								$status = 'Active';
							   ?>
							<a href="<?php echo base_url(); ?>xAdmin/branch/changeStatus/<?php echo $roledata['branch_id'].'/'.$status; ?>"><button class="btn btn-primary btn-sm">Inactive</button> </a><?php }
							   ?>
						</td>
					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});
});
</script>