<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UNDP | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>dist/css/adminlte.css?v=1"> 
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>dist/css/custom.css?v=1"> 

   <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.all.min.js" type="text/javascript"></script>
   <script src="<?php echo base_url('assets/front/') ?>js/sweetalert2.min.js" type="text/javascript"></script>  
   <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/summernote/summernote-bs4.css">  
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css"> 
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!--Datatable checkbox-->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>plugins/datatables-select/css/select.bootstrap4.min.css">

  
  <!-- jQuery -->
	<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url('assets/admin/') ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>

  <!-- iCheck 1.0.1 -->
  <script src="<?php echo base_url('assets/admin/') ?>dist/js/icheck.min.js"></script>

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <!-- <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a> -->
        <a href="javascript: void(0);" class="sidebar-toggle nav-link" data-widget="pushmenu" role="button">
            <span class="bars bar1"></span>
            <span class="bars bar2"></span>
            <span class="bars bar3"></span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link white-text" href="#">Assam Village Survey Information Survey Management</a>
      </li>
       <!--<li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url('xAdmin/setting'); ?>" class="nav-link <?php if($module_name=='Setting') echo 'active' ?>">Setting</a>
      </li>
	  <li class="nav-item d-none d-sm-inline-block">
        <a href="<?php echo base_url('xAdmin/task_setting'); ?>" class="nav-link <?php if($module_name=='Task Setting') echo 'active' ?>">Task Setting</a>
      </li>
     <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li>-->
    </ul>
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
         <img src="http://115.124.127.130/~devteamgrowth/undp_survey/assets/front/img/avatar5.png" class="img-circle elevation-2 user-image" alt="User Image"/> &nbsp;  <?php echo $this->session->userdata('username');  ?> &nbsp;&nbsp;<i class="fas fa-angle-down"></i>
          <?php //if($this->session->userdata('id')) echo $this->session->userdata('username');  ?>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right top_menu">           
             <a href="<?php echo base_url('xAdmin/admin/change_password') ?>" class="dropdown-item">
              <i class="far fa-user text-aqua"></i> Change Password
            </a>   
            <a href="<?php echo base_url('xAdmin/admin/logout') ?>" class="dropdown-item">
              <i class="fas fa-sign-out-alt text-yellow"></i> Logout
            </a> 
          
          <div class="dropdown-divider"></div>
        </div>
      </li>
    </ul>
  </nav>
 <script>
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }

  $(function () {
       //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  })

</script> 
<?php if($this->session->flashdata('error_pemission')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error_pemission'); ?>"); </script><?php } ?>
  <!-- /.navbar -->