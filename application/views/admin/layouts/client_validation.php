
<style type="text/css">
  form .error {
    color: red;
  }
</style>

<script type="text/javascript">
  <?php if($this->router->fetch_class() == 'videoGallery') { ?>
    jQuery.validator.addMethod("link", function(value, element) {
      // allow any non-whitespace characters as the host part
      return this.optional( element ) || /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test( value );
    }, 'Please enter a valid  You Tube URL.');
    
  <?php } if($this->router->fetch_class() == 'career') { ?>
    jQuery.validator.addMethod("url", function(value, element) {
      // allow any non-whitespace characters as the host part
      return this.optional( element ) || /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/.test( value );
    }, 'Please enter a valid URL.');
  <?php } ?>

  $(function() {

    <?php if($this->router->fetch_class() == 'survey') { ?>

    $("form[name='survey_form']").validate({
      
      rules: {
        title: "required",
        description: "required",
        category: "required",
       },
      // Specify validation error messages
      messages: {
        title: "Please enter Title in English",
        description: "Please enter Description",
        category: "Please Select Category",
       },
      submitHandler: function(form) {
        form.submit();
      }
    });

  <?php } if($this->router->fetch_class() == 'question') { ?>

    $("form[name='question_form']").validate({
      
      rules: {
        question_text: "required",
        option_type: "required",
     },

      // Specify validation error messages
      messages: {
        question_text: "Please enter Question",
        option_type: "Please Select Option Type"
      },
      submitHandler: function(form) {
        form.submit();
      }
    });

  <?php }  ?>
  });

<?php if($this->router->fetch_class() == 'videoGallery' || $this->router->fetch_class() == 'testimonials') { ?>
    
  var fl = document.getElementById('files');

  fl.onchange = function(e){ 
    var ext = this.value.match(/\.(.+)$/)[1];
    switch(ext)
    {
        case 'jpg':
        case 'bmp':
        case 'png':
        case 'tif':
          $('#file_error').text('');
          break;
        default:
          $('#file_error').text('Please select only image files');
          this.value='';
    }
  };  
<?php } if($this->router->fetch_class() == 'tender' || $this->router->fetch_class() == 'career') { ?>

  var fl = document.getElementById('document');

  fl.onchange = function(e){ 
    var ext = this.value.match(/\.(.+)$/)[1];
    switch(ext)
    {
      case 'pdf':
        $('#file_error').text('');
        break;
      default:
        $('#file_error').text('Please select only PDF file');
        this.value='';
    }
  };  
<?php } ?>
</script>

<script type="text/javascript">
  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
  }

  // validate field has value
  function validate_field(textArr){  
    var valid = 1;
    if(textArr.length > 0){ 
      for(var i = 0; i<textArr.length; i++){         
        $("."+textArr[i]).each(function(){
          if($(this).val() == ""){
            $(this).css('border','1px solid red');
            valid = 0;
          } 
        });
      }
    }
    
    if(valid == 1){
      return true;
    }else{
      return false;
    } 
  }
</script>
