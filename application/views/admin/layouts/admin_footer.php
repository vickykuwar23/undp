<footer class="main-footer">
    <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="https://esds.co.in" target="_blank">ESDS Software Solution Pvt. Ltd.</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      Version <?php echo $this->config->item('BUILD_VERSION'); ?>
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script type="text/javascript">
 //$.widget.bridge('uibutton', $.ui.button)
  
  $(function () {
  
   //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
	
	});
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url('assets/admin/') ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script>

<script src="<?php echo base_url('assets/admin/') ?>dist/js/sweetalert2.all.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>dist/js/sweetalert2.min.js"></script>
<script src="<?php echo base_url('assets/admin/') ?>plugins/select2/js/select2.full.min.js"></script>
<!--<script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script>-->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/adminlte.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/pages/dashboard.js"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo base_url('assets/admin/') ?>dist/js/demo.js"></script> -->



<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>

<!--datatable checkbox-->
<script>var site_path = '<?php echo base_url('assets/admin/'); ?>plugins/datatables-select/jsdataTables.select.min.js'; </script>

<script>var site_path = '<?php echo base_url(); ?>'; </script>
<script type="text/javascript">
  function confirm_action(ref,evt,msg)
  {

    var msg = msg || false;
    evt.preventDefault();  

    swal({
        title: "Are you sure ?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
          window.location = $(ref).attr('href');
        }
    });

  } 
</script>

</body>
</html>
