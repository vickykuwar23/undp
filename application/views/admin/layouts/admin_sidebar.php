<?php 
	$url = $this->uri->segment(2);
 	$url2 = $this->uri->segment(4); 

 	//$where = array('');
 	$role_data = $this->master_model->getRecords("role_master");
	//print_r($this->session->userdata('role_id'));
 	$role_permission_query = $this->db->query("SELECT  r1.role_id, r1.role_name, sp.module_id, sp.permission_name FROM survey_role_master r1 LEFT JOIN survey_permission sp ON r1.role_id = sp.role_id
		WHERE r1.is_deleted = 0 
		AND   r1.status = 'Active' 
		AND   r1.role_id = ".$this->session->userdata('role_id')."
		GROUP BY r1.role_id, sp.module_id
		ORDER BY r1.role_id, sp.module_id ASC");

 	//echo $this->db->last_query();

	$role_permission_data = $role_permission_query->result_array();

	$permission_cnt = sizeof(@$role_permission_data);

	
	//print_r($role_permission_data);
?>
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
     <a href="javascript:void(0);" class="brand-link">
      <!--<img src="<?php echo base_url('assets/admin/img/admin_logo.png') ?>" alt="AARAI LOGO" class="brand-image img-circle elevation-3"
           style="opacity: .8">-->
      <span class="brand-text font-weight-light">Survey Portal</span>
    </a> 
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!--<div class="user-panel mt-3 pb-3 mb-3 d-flex">
		<div class="image">
          <img src="<?php echo base_url('assets/front/') ?>img/avatar5.png" class="img-circle elevation-2" alt="User Image">
        </div>
          <div class="info">
			  <a href="#" class="d-block"><?php echo $this->session->userdata('admin_name'); ?></a>
			</div>
		  </div>-->
		
		<!-- SidebarSearch Form -->
	      <div class="form-inline px-3 pt-3 sidebar-form">
	        <div class="input-group" data-widget="sidebar-search">
	          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
	          <div class="input-group-append">
	            <button class="btn btn-sidebar">
	              <i class="fas fa-search fa-fw"></i>
	            </button>
	          </div>
	        </div>
	      </div>
		  
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

			<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/admin/dashboard');?>" class="nav-link <?php if($module_name=='Dashboard') echo 'active' ?>">
					<i class="nav-icon fas fa-th"></i>
					<p>Dashboard</p>
				</a>
			</li>
			

			<?php if($this->session->userdata('role_id') == 1 || $this->session->userdata('role_id') == 999){ ?>
				<li class="nav-item">
					<a href="<?php echo base_url('xAdmin/users');?>" class="nav-link <?php if($module_name == 'User') echo 'active' ?>">
						<i class="nav-icon fas fa-users"></i>
						<p>Users</p>
					</a>
				</li>			
				<li class="nav-item has-treeview <?php if($module_name=='Master'): echo 'menu-open'; endif; ?>">
					<a href="javascript:void(0);" class="nav-link">
					  <i class="nav-icon fas fa-book"></i>
					  <p>
						Masters
						<i class="fas fa-angle-left right"></i>
					  </p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">

							<a href="<?php echo base_url('xAdmin/state');?>" class="nav-link <?php if($submodule_name=='State') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>State Master</p>
							</a>
						</li> 
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/district');?>" class="nav-link <?php if($submodule_name=='District') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>District Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/revenue');?>" class="nav-link <?php if($submodule_name=='Revenue') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Revenue Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/village');?>" class="nav-link <?php if($submodule_name=='Village') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Village Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/block');?>" class="nav-link <?php if($submodule_name=='Block') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Block Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/panchayat');?>" class="nav-link <?php if($submodule_name=='Panchayat') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Panchayat Master</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/role');?>" class="nav-link <?php if($submodule_name=='Role') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Role Master</p>
							</a>
						</li> 
						
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/module');?>" class="nav-link <?php if($submodule_name=='Module') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Module Master</p>
							</a>
						</li> 
			
					</ul>

				</li>

				<li class="nav-item has-treeview <?php if($module_name=='Survey'): echo 'menu-open'; endif; ?>">
					<a href="javascript:void(0);" class="nav-link ">
						 <i class="nav-icon fas fa-poll"></i>
						<p>Survey Management</p>
						<i class="fas fa-angle-left right"></i>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/survey_category');?>" class="nav-link <?php if($submodule_name=='Category') echo 'active' ?>">
							  <i class="far fa-circle nav-icon"></i>
							  <p>Survey Category</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/section');?>" class="nav-link <?php if($submodule_name=='Section') echo 'active' ?>">
							  <!-- <i class="nav-icon fas fa-poll"></i> -->
							  <i class="far fa-circle nav-icon"></i>
							  <p>Section Master</p>
							</a>
						</li>	
						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/survey');?>" class="nav-link <?php if($submodule_name=='Survey_list') echo 'active' ?>">
							  <!-- <i class="nav-icon fas fa-poll"></i> -->
							  <i class="far fa-circle nav-icon"></i>
							  <p>Survey Listing</p>
							</a>
						</li>	

						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/question');?>" class="nav-link <?php if($submodule_name=='Question') echo 'active' ?>">
							  <!-- <i class="nav-icon fas fa-poll"></i> -->
							  <i class="far fa-circle nav-icon"></i>
							  <p>Question Master</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/question_mapping');?>" class="nav-link <?php if($submodule_name=='Question_mapping') echo 'active' ?>">
							  <!-- <i class="nav-icon fas fa-poll"></i> -->
							  <i class="far fa-circle nav-icon"></i>
							  <p>Question Mapping</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="<?php echo base_url('xAdmin/Response');?>" class="nav-link <?php if($submodule_name=='Response_list') echo 'active' ?>">
							  <!-- <i class="nav-icon fas fa-poll"></i> -->
							  <i class="far fa-circle nav-icon"></i>
							  <p>Survey Responses</p>
							</a>
						</li>
					</ul>
				</li>
			<?php } 
			
			else if(($this->session->userdata('role_id') != 1 || $this->session->userdata('role_id') != 2 || $this->session->userdata('role_id') != 999) && $permission_cnt > 0) {  ?>

				<?php 
					for ($i=0;$i<sizeof($role_permission_data);$i++) {
						if($role_permission_data[$i]['module_id'] == 1) { 
							$user = 'yes';
						}
						if($role_permission_data[$i]['module_id'] == 3 || $role_permission_data[$i]['module_id'] == 4 || $role_permission_data[$i]['module_id'] == 5 || $role_permission_data[$i]['module_id'] == 6 || $role_permission_data[$i]['module_id'] == 7 || $role_permission_data[$i]['module_id'] == 10 || $role_permission_data[$i]['module_id'] == 11 || $role_permission_data[$i]['module_id'] == 12) {
							$master = 'yes';
							if($role_permission_data[$i]['module_id'] == 3 ) {
								$state = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 4 ) {
								$district = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 5 ) {
								$block = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 6 ) {
								$village = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 7 ) {
								$module = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 10 ) {
								$role = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 11 ) {
								$revenue = 'yes';
							}
							if($role_permission_data[$i]['module_id'] == 12 ) {
								$panchayat = 'yes';
							}
						}

						if($role_permission_data[$i]['module_id'] == 15 || $role_permission_data[$i]['module_id'] == 16 || $role_permission_data[$i]['module_id'] == 17 || $role_permission_data[$i]['module_id'] == 18 || $role_permission_data[$i]['module_id'] == 8 || $role_permission_data[$i]['module_id'] == 20) {
								$survey = 'yes';
								if($role_permission_data[$i]['module_id'] == 18) {
									$category = 'yes';
								}
								if($role_permission_data[$i]['module_id'] == 15) {
									$section = 'yes';
								}
								if($role_permission_data[$i]['module_id'] == 16) {
									$question = 'yes';
								}
								if($role_permission_data[$i]['module_id'] == 17) {
									$question_mapping = 'yes';
								}
								if($role_permission_data[$i]['module_id'] == 8) {
									$survey_list = 'yes';
								}
								if($role_permission_data[$i]['module_id'] == 20) {
									$response = 'yes';
								}
							}
						}
						?>

						<?php if(@$user == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/users');?>" class="nav-link <?php if($module_name=='System_user') echo 'active' ?>">
									<i class="nav-icon fas fa-users"></i>
									<p>Users</p>
								</a>
							</li>
						<?php } ?>

				<?php 
				
				if(@$master == 'yes') { ?>	

				<li class="nav-item has-treeview <?php if($module_name=='Master'): echo 'menu-open'; endif; ?>">
					<a href="javascript:void(0);" class="nav-link">
					  <i class="nav-icon fas fa-book"></i>
					  <p>
						Masters
						<i class="fas fa-angle-left right"></i>
					  </p>
					</a>
					<ul class="nav nav-treeview">
						<?php if(@$state == 3 ) { ?>
							<li class="nav-item">

								<a href="<?php echo base_url('xAdmin/state');?>" class="nav-link <?php if($submodule_name=='State') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>State Master</p>
								</a>
							</li> 
						<?php } ?>

						<?php if(@$district == 'yes' ) { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/district');?>" class="nav-link <?php if($submodule_name=='District') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>District Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$revenue == 'yes' ) { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/revenue');?>" class="nav-link <?php if($submodule_name=='Revenue') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Revenue Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$village == 'yes' ) { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/village');?>" class="nav-link <?php if($submodule_name=='Village') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Village Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$block == 'yes' ) { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/block');?>" class="nav-link <?php if($submodule_name=='Block') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Block Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$panchayat == 'yes') { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/panchayat');?>" class="nav-link <?php if($submodule_name=='Panchayat') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Panchayat Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$role == 'yes') { ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/role');?>" class="nav-link <?php if($submodule_name=='Role') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Role Master</p>
								</a>
							</li> 
						<?php } ?>	
						
						<?php if(@$module == 'yes') {  ?>
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/module');?>" class="nav-link <?php if($submodule_name=='Module') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Module Master</p>
								</a>
							</li> 
						<?php } ?>	
					</ul>

				</li>
			<?php } ?>

			<?php if(@$survey == 'yes')  { ?>	

					<li class="nav-item has-treeview <?php if($module_name=='Survey'): echo 'menu-open'; endif; ?>">
						<a href="javascript:void(0);" class="nav-link ">
							 <i class="nav-icon fas fa-poll"></i>
							<p>Survey Management</p>
							<i class="fas fa-angle-left right"></i>
						</a>
						<ul class="nav nav-treeview">

						<?php if(@$category == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/survey_category');?>" class="nav-link <?php if($submodule_name=='Category') echo 'active' ?>">
								  <i class="far fa-circle nav-icon"></i>
								  <p>Survey Category</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$section == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/section');?>" class="nav-link <?php if($submodule_name=='Section') echo 'active' ?>">
								  <!-- <i class="nav-icon fas fa-poll"></i> -->
								  <i class="far fa-circle nav-icon"></i>
								  <p>Section Master</p>
								</a>
							</li>	
						<?php } ?>

						<?php if(@$survey_list == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/survey');?>" class="nav-link <?php if($submodule_name=='Survey_list') echo 'active' ?>">
								  <!-- <i class="nav-icon fas fa-poll"></i> -->
								  <i class="far fa-circle nav-icon"></i>
								  <p>Survey Listing</p>
								</a>
							</li>	
						<?php } ?>

						<?php if(@$question == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/question');?>" class="nav-link <?php if($submodule_name=='Question') echo 'active' ?>">
								  <!-- <i class="nav-icon fas fa-poll"></i> -->
								  <i class="far fa-circle nav-icon"></i>
								  <p>Question Master</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$question_mapping == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/question_mapping');?>" class="nav-link <?php if($submodule_name=='Question_mapping') echo 'active' ?>">
								  <!-- <i class="nav-icon fas fa-poll"></i> -->
								  <i class="far fa-circle nav-icon"></i>
								  <p>Question Mapping</p>
								</a>
							</li>
						<?php } ?>

						<?php if(@$response == 'yes') { ?>	
							<li class="nav-item">
								<a href="<?php echo base_url('xAdmin/Response');?>" class="nav-link <?php if($submodule_name=='Responses') echo 'active' ?>">
								  <!-- <i class="nav-icon fas fa-poll"></i> -->
								  <i class="far fa-circle nav-icon"></i>
								  <p>Survey Responses</p>
								</a>
							</li>
						<?php } ?>
						</ul>
					</li>	

			
			<?php } 
		} ?>
				
			<!--<li class="nav-item">
				<a href="<?php echo base_url('xAdmin/blog');?>" class="nav-link <?php if($module_name=='Blog') echo 'active' ?>">
				  <i class="nav-icon fas fa-th"></i>
				  <p>Blog</p>
				</a>
			</li>-->			
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>