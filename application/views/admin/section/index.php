<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Section Master</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Section Master</li>
               </ol>
            </div>
               
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">


        <div class="finnancialYear">
            <div class="box-body">
             
              <div class="box-header with-border">
              <!-- tools box -->
             <!--  <div class="pull-right box-tools">
                  <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="collapse" data-target="#collapseExample">
                   <i class="fa fa-plus"></i></button>
              </div> -->
              <!-- /. tools -->
                <h3 class="box-title">Search Filter</h3>
              </div>

              <div class="col-md-12 expand" id="collapseExample">
                  <div class="row">    
                      <div class="col-md-5 col-sm-6">
                          <div class="form-group">
                              <label>Survey</label>
                              <select name="survey" id="survey" class="form-control">
                                  <option value="">Select Survey</option>
                                  <?php foreach($survey_data as $survey)  { ?>
                                    <option value="<?php echo $survey['survey_id']; ?>"><?php echo $survey['title']; ?></option>
                                  <?php } ?>
                              </select>
                          </div>
                      </div>
                    </div>
                  </div>
      	      </div>
            </div>
          </div>

         <div class="card ">
			<div class="card-header">
				
				<a href="<?php echo base_url('xAdmin/section/add') ?>" class="btn btn-info btn-sm float-right"><i class="fa fa-plus"></i> Add Section</a>              
			</div>
         <!-- Small boxes (Stat box) -->
         <div class="card-body">
           <center>
            <div id="loading" class="divLoading" style="display: none;">
              <p>Loading... <img src="<?php echo base_url(); ?>assets/images/loading-4.gif" width="100" height="100" /></p>
            </div>
          </center> 

            <table id="example1" class="table table-bordered table-hover">
               <thead>
                  <tr>
                    <th>Sr.</th>
   					        <th>Section Name</th>
   					        <th>Parent Section Name</th>
                    <th>Section Description</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                <?php  
                  $i=1;					
                  foreach($records as $roledata) 
                  { 	
        						if($roledata['parent_section_id'] != 0){
        							$parentName = $this->master_model->getRecords("section_master",array('section_id' => $roledata['parent_section_id']));
        							//echo $this->db->last_query();
        							$parentfullname = $parentName[0]['section_name'];
        						} else {
        							$parentfullname = '';
        						}
					        ?>
        					<tr>
        						<td style="width:5%"><?php echo $i; ?></td>
        						<td style="width:30%"><?php echo $roledata['section_name']; ?></td> 
        						<td style="width:30%"><?php echo $parentfullname; ?></td> 
        						<td style="width:30%"><?php echo $roledata['section_desc']; ?></td> 
        						<td><?php      
                       if($roledata['status']=="Active"){
                          $status = 'Inactive';
                       ?>
                       <a href="<?php echo base_url(); ?>xAdmin/section/changeStatus/<?php echo $roledata['section_id'].'/'.$status; ?>"><span class="badge badge-success">Active</span></a><?php }
                          else if($roledata['status']=="Inactive"){
                          $status = 'Active';
                          ?>
                       <a href="<?php echo base_url(); ?>xAdmin/section/changeStatus/<?php echo $roledata['section_id'].'/'.$status; ?>"><span class="badge badge-danger">Inactive</span></a><?php }
                          ?>
                    </td>           
        						<td style="width:10%">						
        							<a href="<?php echo base_url();?>xAdmin/section/edit/<?php echo $roledata['section_id'];  ?>" ><i class="fas fa-edit" title="Edit"></i></a>&nbsp;
        							<a href="<?php echo base_url();?>xAdmin/section/delete/<?php echo base64_encode($roledata['section_id']);  ?>" onclick="return confirm_delete(this,event,'Do you really want to delete this record ?', <?php echo $roledata['section_id']; ?>)"><i class="fas fa-trash-alt" title="Delete"></i></a>&nbsp;                       
        						</td>
        					</tr>
                  <?php $i++; } ?>
                  </tbody>
            </table>
            <!-- ./col -->
         </div>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
	function sweet_alert_success(msg) { swal({ title: "Success", text: msg, type: "success" }); }
	function sweet_alert_error(msg) { swal({ title: "Error", text: msg, type: "error" }); }
</script>
<?php if($this->session->flashdata('success')) { ?><script>sweet_alert_success("<?php echo $this->session->flashdata('success'); ?>"); </script><?php } ?>
<?php if($this->session->flashdata('error')) { ?><script>sweet_alert_error("<?php echo $this->session->flashdata('error'); ?>"); </script><?php } ?>
<script>
$(document).ready( function () {
	 $("#example1").DataTable({
	  "responsive": true,
	  "autoWidth": false,
	});

   $('#survey').change(function(){
      var survey_id = $('#survey').val();
         
      //$('.divLoading').css('display','block');
      if(survey_id != ''){
        $.ajax({
          url: site_path+"xAdmin/section/get_datatable",
          type: 'POST',
          data: {'ci_csrf_token':'', survey_id:survey_id},
          success: function(response){

            ////$('.divLoading').css('display','none');
           //alert(response);
            //var res = response.split('$$$');
            
            if(response != ''){
              $("#example1").DataTable().clear().draw();
              $.each(JSON.parse(response), function(idx, obj) {

                var j = parseInt(idx)+1;

                var url = '<?php echo base_url(); ?>';

               if(obj.status == "Active"){
                       
                  var status_href='<a href="'+url+'xAdmin/section/changeStatus/'+obj.section_id+'/'+obj.status+'"><span class="badge badge-success">Active</span></a>';
                
                }
                else if(obj.status == "Inactive"){
                  var status_href='<a href="'+url+'xAdmin/section/changeStatus/'+obj.section_id+'/'+obj.status+'"><span class="badge badge-success">Inactive</span></a>';
                }

                var action = '<a href="'+url+'xAdmin/section/edit/'+obj.section_id+'" ><i class="fas fa-edit" title="Edit"></i></a>&nbsp;<a href="javascript:void(0)" onclick="open_page('+obj.section_id+')"><i class="fas fa-trash-alt" title="Delete"></i></a>&nbsp; ';

                values = [[j, obj.section_name, '', obj.section_desc, status_href, action]];
  
                $("#example1").DataTable().rows.add(values).draw();
               
              });
            }
            else{
              $("#example1").DataTable().clear().draw();
              swal('No Data Found', "", "info"); 
             // $('.divLoading').css('display','none');
            }
          

          }
        }) //ajax
      }
      else{
            location.reload();
          }
    
    });
});

function open_page(section_id){
      swal({
        title: "Are you sure ?",
        text: 'Do you really want to delete this record ?',
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
           var url = '<?php echo base_url(); ?>';
        window.location.href = url+"xAdmin/section/delete/"+btoa(section_id);
      }
    })// onchange
  }

function confirm_delete(ref,evt,msg,section_id)
  {

    var msg = msg || false;
    evt.preventDefault();  

    swal({
        title: "Are you sure ?",
        text: msg,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#0aa89e",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true
      }).then(result =>{
        if (result.value)
        {
          //window.location = $(ref).attr('href');
          $.ajax({
            url: site_path+"xAdmin/section/check_status",
            type: 'POST',
            data: {'ci_csrf_token':'',section_id:section_id},
            success: function(response){    
              if(response != ''){
                 swal(response, "", "warning");
              }
              else{
                window.location = $(ref).attr('href');
              }
            }
          })  
        }
    });

  } 
</script>