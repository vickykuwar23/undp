<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <div class="content-header">
      <div class="container-fluid">
         <div class="row mb-2">
            <div class="col-sm-6">
               <h1 class="m-0 text-dark">Survey Category</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('xAdmin/admin/dashboard'); ?>">Home</a></li>
                  <li class="breadcrumb-item active">Survey Category</li>
               </ol>
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </div>
   <!-- /.content-header -->
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
         <!-- Small boxes (Stat box) -->
           <div class="card ">
			  <div class="card-header">
			  <h3 class="card-title">
				  Edit Survey Category
			  </h3>
				  <a href="<?php echo base_url('xAdmin/survey_category') ?>" class="btn btn-primary btn-sm pull-right float-right">Back</a>
			  </div> 
				  <!-- form start -->
				  <?php if($this->session->flashdata('error')){ ?>
					<div class="alert alert-danger alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  <h5><i class="icon fas fa-ban"></i> Error!</h5>
					 <?php echo $this->session->flashdata('error'); ?>
					</div>
				   <?php }  
				   
				   ?>
				<div class="card-body">
					 <form method="post" id="subfrm" name="subfrm" role="form" >
						
						  <div class="row">
							<div class="col-6">	
								<div class="form-group">
									<label for="exampleInputEmail1">Category Name<span style="color: red">*</span></label>
									<input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" value="<?php echo $role_record[0]['category_name'] ?>" maxlength="100" required>
									<span><?php echo form_error('name'); ?></span>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail1">Restricted Draft Count<span style="color: red">*</span></label>
									<input type="text" class="form-control" id="restricted_draft_count" placeholder="Enter Name" name="restricted_draft_count" value="<?php echo $role_record[0]['restricted_draft_count'] ?>" maxlength="2" required>
									<span><?php echo form_error('restricted_draft_count'); ?></span>
								</div>
								<div class="card-footer1">
									<button type="submit" id="btn_submit" class="btn btn-primary">Update</button>							 
								</div>
							</div>
								<!-- /.card-body -->
						  </div>
						   <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
					</form>
				</div>
            </div>
            <!-- ./col -->
         </div>
         <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jquery-validation -->
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url('assets/admin/'); ?>plugins/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.validator.setDefaults({
    submitHandler: function () {
      //alert( "Form successful submitted!" );
      $('#btn_submit').prop('disabled',true);
	    form.submit();
    }
  });
  
  $.validator.addMethod("nowhitespace", function(value, element) { if($.trim(value).length == 0) { return false; } else { return true; } });
  
  $('#subfrm').validate({
    rules: {	 	
      name: {
        required: true,
		nowhitespace:true,
		minlength:2
      },
	  restricted_draft_count: {
        required: true,
		nowhitespace:true,
		digits:true
      }
    },
    messages: {
	
      name: {
        required: "This field is required",
		minlength: "Enter Category name must be at least {0} characters long"
      },
	  restricted_draft_count: {
        required: "This field is required",
		digit: "Please enter only digit"
      }
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
    }
  });
});
</script>


