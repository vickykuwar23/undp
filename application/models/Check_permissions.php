<?php if(!defined('BASEPATH')){exit('No direct script access.');}
class Check_permissions extends CI_Model
{	
	public function is_logged_in()
	{
		$_SESSION['intended_url']= current_url();
		if($this->session->userdata('user_id')=='' ){
			$this->session->set_flashdata('error_is_logged_in','Access denied !');
			redirect(base_url('login'));

			// access
		}
	
	}

	public function show_link_if_loggedin()
	{
		if($this->session->userdata('user_id')=='' ){
			return false;
		}
		return true;
	
	}

	public function is_admin_approved()
	{
		$user_id=$this->session->userdata('user_id');
		if ($user_id!='') {
			
		$user_info = $this->master_model->getRecords('registration',array('user_id' => $user_id));

		if ($user_info[0]['admin_approval'] == 'no') {
			if(isset($_SERVER['HTTP_REFERER'])) {
			  $rediect_url=$_SERVER['HTTP_REFERER'];
			  }
			else
			{
			   $rediect_url=base_url();
			}

			$this->session->set_flashdata('error_approval','Access denied !');
			redirect($rediect_url);	
		}else{
			return true;
		}

		

		

		}
	
	}

	public function is_authorise($module_id)
	{
		$permissions=$this->session->userdata('permissions');
		$permissions_arr = explode(',', $permissions);
		if (in_array($module_id, $permissions_arr)) {
			return true;
		}else{
			if(isset($_SERVER['HTTP_REFERER'])) {
			  $rediect_url=$_SERVER['HTTP_REFERER'];
			  }
			else
			{
			   $rediect_url=base_url();
			}
	    	
    		$this->session->set_flashdata('error_pemission','Access denied !');
			redirect($rediect_url);	

		}
	}

	public function is_profile_complete()
	{  
		$user_id=$this->session->userdata('user_id');
		if ($user_id!='') {
			
		
		$category=$this->session->userdata('user_category');

		if ($category==2) {
			$table_name='profile_organization';
			
		}else if($category==1){
			$table_name='arai_student_profile';
		}
		$profileInfo = $this->master_model->getRecords($table_name,array('user_id' => $user_id));

		if (count($profileInfo)) {
			$profile_status= $profileInfo[0]['profile_completion_status'];
			if ($profile_status=='incomplete') {
				if ($category==2) {
					$redirect_url=base_url('organization');
				}else if($category==1){

					$redirect_url=base_url('profile');
				}
				$this->session->set_flashdata('error_profile_competion','Access denied !');
				redirect($redirect_url);	
			}else{
				return true;
			}
		}else{
			if ($category==2) {
					$redirect_url=base_url('organization');
			}else if($category==1){

				$redirect_url=base_url('profile');
			}
			$this->session->set_flashdata('error_profile_competion','Access denied !');
			redirect($redirect_url);	
		}

		

		}

	}

	public function is_authorise_link($module_id)
	{
		$permissions=$this->session->userdata('permissions');
		$permissions_arr = explode(',', $permissions);
		if (in_array($module_id, $permissions_arr)) {
			return true;
		}else{
			return false;

		}
	}

	public function is_profile_complete_without_redirect()
	{  
		$user_id=$this->session->userdata('user_id');
		if ($user_id!='') {
			
		
		$category=$this->session->userdata('user_category');

		if($category==1){
			$table_name='arai_student_profile';
		}
		$profileInfo = $this->master_model->getRecords($table_name,array('user_id' => $user_id));

		if (count($profileInfo)) {
			
			$profile_status= $profileInfo[0]['profile_completion_status'];
			if ($profile_status=='incomplete') {

				echo 'false';	
			}else{
				echo 'true';
			}
		}else{
			if($category==1){

				echo 'false';
			}
				
		}

		

		}

	}
}	