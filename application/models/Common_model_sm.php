<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	class Common_model_sm extends CI_Model
	{ 
		public function __construct()
		{
			parent::__construct();
		}
		
		function upload_single_file($input_name,$valid_arr,$new_file_name,$upload_path,$allowed_types,$is_multiple=0,$cnt='')
		{
			$flag = 0;
			if($is_multiple == 0) { $path_img = $_FILES[$input_name]['name']; }
			else { $path_img = $_FILES[$input_name]['name'][$cnt]; }
			
			$ext_img = pathinfo($path_img, PATHINFO_EXTENSION);
			$valid_ext_arr = $valid_arr;
			
			if(!in_array(strtolower($ext_img),$valid_ext_arr))
			{
				$flag=1;
			}
			
			if($flag == 0)
			{
				$chk_upload_dir = './uploads';
				if(!is_dir($chk_upload_dir))
				{
					$dir = mkdir($chk_upload_dir,0755);
					
					$myfile0 = fopen($chk_upload_dir."/index.php", "w") or die("Unable to open file!");
					$txt0 = "";
					fwrite($myfile0, $txt0);				
					fclose($myfile0);
				}
				
				if(is_dir($upload_path)){ }
				else
				{ 
					$dir=mkdir($upload_path,0755);
					
					$myfile = fopen($upload_path."/index.php", "w") or die("Unable to open file!");
					$txt = "";
					fwrite($myfile, $txt);				
					fclose($myfile);
				}	
				
				$file=$_FILES;	
				if($is_multiple == 0) { $_FILES['file_upload']['name'] = $file[$input_name]['name']; }
				else { $_FILES['file_upload']['name'] = $file[$input_name]['name'][$cnt]; }
				
				$filename = $new_file_name;
				$path = $_FILES['file_upload']['name'];
				$ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));							
				$final_img = $filename.".".$ext;					
				
				$config['file_name']     = $filename;
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $allowed_types;
				
				$this->upload->initialize($config);					
				
				if($is_multiple == 0) 
				{
				$_FILES['file_upload']['type']=$file[$input_name]['type'];
				$_FILES['file_upload']['tmp_name']=$file[$input_name]['tmp_name'];
				$_FILES['file_upload']['error']=$file[$input_name]['error'];
				$_FILES['file_upload']['size']=$file[$input_name]['size'];
				}
				else
				{
					$_FILES['file_upload']['type']=$file[$input_name]['type'][$cnt];
					$_FILES['file_upload']['tmp_name']=$file[$input_name]['tmp_name'][$cnt];
					$_FILES['file_upload']['error']=$file[$input_name]['error'][$cnt];
					$_FILES['file_upload']['size']=$file[$input_name]['size'][$cnt];
				}
				
				if($this->upload->do_upload('file_upload'))
				{
					$data=$this->upload->data();
					return array('response'=>'success','message' => $final_img);
				}
				else
				{
					return array('response'=>'error','message' => $this->upload->display_errors());
				}
			}
			else
			{
				return array('response'=>'error','message' => "Please upload valid ".str_replace('|',' | ',$allowed_types)." extension image.");
			}
		}
	}
?>