<?php
/*
File_name 			: my_helper.php
Developer  			: Vicky Kuwar
Created on 			: 15th July 2020
Description 		: to manage frequently used authentication related activities.
Last Modified on 	:
Last Modified By 	:
Description 		:
 */
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

/*function isLogin($return = '') {
	if (!isset($_SESSION['user_id'])) {
		if ($return != '') {
			return true;
		} else {
			redirect(base_url());
		}
	}
}*/
/*function isAccess($module) {
	//modules : PR: Projects
	$CI = &get_instance();
	$user_role = $CI->user_role;
	$role_array = array('AD' => array('PR', 'MR'),
		'SD' => array('PR'),
	);
	$module_array = $role_array[$user_role];
	if (!in_array($module, $module_array)) {
		redirect('dashboard');
	}
}

function dateDiffInDays($date1, $date2 = '') {
	$date2 = $date2 != '' ? $date2 : date('Y-m-d');
	$diff = strtotime($date2) - strtotime($date1);
	return abs(round($diff / 86400));
}


function format_date($date) {
	return date('d-m-Y H:i:s', $date);
}
function createFromFormat($date) {
	$date_obj = DateTime::createFromFormat('d-m-Y', $date);
	$date = $date_obj->format('Y-m-d');
	return $date;
}
function get_years() {
	$year = date('Y');
	for ($i = 2018; $i <= $year; $i++) {
		$year_array[] = $i;
	}
	return $year_array;
}

function getLastInsertID($cid){
	
	$CI = &get_instance();
	$insertArr = array('c_id' => $cid);
	$insertQuery = $CI->master_model->insertRecord('track_challenge',$insertArr);
	$updateArrID = array('challenge_code' => $insertQuery);
	
	// Update Challenge Code
	$CI->master_model->updateRecord('track_challenge',$updateArrID,array('c_id' => $cid));	
	$CI->db->select('challenge_code');
	
	// Get Last Challenge COde
	$getRecord = $CI->master_model->getRecords("track_challenge",'','',array('id' => 'DESC'));
	return $getRecord[0]['challenge_code'];
	
}


function getLastWebinarID($wid){
	
	$CI = &get_instance();
	$insertArr = array('w_id' => $wid);
	$insertQuery = $CI->master_model->insertRecord('track_webinar',$insertArr);
	$updateArrID = array('webinar_code' => $insertQuery);
	
	// Update Challenge Code
	$CI->master_model->updateRecord('track_webinar',$updateArrID,array('w_id' => $wid));	
	$CI->db->select('webinar_code');
	
	// Get Last Challenge COde
	$getRecord = $CI->master_model->getRecords("track_webinar",'','',array('id' => 'DESC'));
	return $getRecord[0]['webinar_code'];
	
}*/

//====

// https://stackoverflow.com/a/40582472
// function to get header Authorization
function getAuthorizationHeader(){
    $headers = null;
    if (isset($_SERVER['Authorization'])) {
        $headers = trim($_SERVER["Authorization"]);
    }
    else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
        $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
    } else if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) { //Nginx or fast CGI	
		$headers = trim($_SERVER["REDIRECT_HTTP_AUTHORIZATION"]);	
	} elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        //print_r($requestHeaders);
        if (isset($requestHeaders['Authorization'])) {
            $headers = trim($requestHeaders['Authorization']);
        }
    }
    return $headers;
}

// function to get access token from header
function getBearerToken() {
    $headers = getAuthorizationHeader();
    // HEADER: Get the access token from the header
    if (!empty($headers)) {
        if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
            return $matches[1];
        }
    }
    return null;
}

// function to validate token
function validateToken($token)
{
    $CI = &get_instance();
	$auth_token = $CI->config->item('AUTH_TOKEN');	// defined in config.php
	
	if($token == $auth_token)
    {
        return TRUE;
    }
    return FALSE;
}

// function to check if user login
function is_user_login($user_id)
{
	$CI = &get_instance();
	$sql = "SELECT user_id FROM survey_users WHERE user_id = '".$user_id."' AND status = 'Active' AND is_deleted = '0' AND is_login = '1'";
    if($result = $CI->db->query($sql))
    {
        // Return the number of rows in result set
        if($result->num_rows() > 0)
        {
            return TRUE;
        }
    }
    return FALSE;
}

// function to create session id
function generateSessionToken()
{
	$token = bin2hex(openssl_random_pseudo_bytes(16));

	# or in php7
	//$token = bin2hex(random_bytes(16));

	return $token;
}

// function to set user session id
function setUserSessionToken($session_token, $user_id)
{
	$CI = &get_instance();
    $sql = "UPDATE survey_users SET session_token = '".$session_token."' WHERE user_id = '".$user_id."'";
    if($result = $CI->db->query($sql))
    {
        // Return the number of rows in result set
        //if(mysqli_num_rows($result) > 0)
        //{
            return TRUE;
        //}
    }
    return FALSE;
}

// function to get user session id
function getUserSessionToken()
{
	$session_token = null;
    if (isset($_SERVER['session_token'])) {
        $session_token = trim($_SERVER["session_token"]);
    }
    else if (isset($_SERVER['HTTP_SESSION_TOKEN'])) { //Nginx or fast CGI
        $session_token = trim($_SERVER["HTTP_SESSION_TOKEN"]);
    } elseif (function_exists('apache_request_headers')) {
        $requestHeaders = apache_request_headers();
        // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
        $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
        //print_r($requestHeaders);
        if (isset($requestHeaders['SESSION_TOKEN'])) {
            $session_token = trim($requestHeaders['SESSION_TOKEN']);
        }
    }
    
    // HEADER: Get the access token from the header
    if (!empty($session_token)) {
        return $session_token;
    }
    return null;
}

// function to get user id for session token
function getUserIdForSessionToken()
{
	$CI = &get_instance();

	$session_token = getUserSessionToken();

	if($session_token)
	{
	    $sql = "SELECT user_id FROM survey_users WHERE session_token = '".$session_token."' AND status = 'Active' AND is_deleted = '0'";
	   if($result = $CI->db->query($sql))
	    {
	        // Return the number of rows in result set
	        if($result->num_rows() > 0)
	        {
	            foreach($result->result_array() as $row)
				{
					$user_id = $row['user_id'];
				}

	            return $user_id;
	        }
	    }
	}
    return FALSE;
}

// Push Notification Start 
function push_notification($registrationIds, $postArr)
{
	
	$CI = &get_instance();
	
	//API_ACCESS_KEY
	$api_access_key = $CI->config->item('API_ACCESS_KEY');
	
	$msg = array
	(
		'body' 	=> $postArr['msg_body'],
		'title'	=> $postArr['msg_title'],
		'icon'	=> 'myIcon',/*Default Icon*/
		'sound' => 'mySound'/*Default sound*/
	);

	$fields = array
	(
		'to'			=> $registrationIds,
		'notification'	=> $msg
	);

	$headers = array
	(
		'Authorization: key=' . $api_access_key,
		'Content-Type: application/json'
	);

	#Send Reponse To FireBase Server	
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );

	#Echo Result Of FireBase Server
	return $result;
	//return true;
}