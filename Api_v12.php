<?php
//defined('BASEPATH') OR exit('No direct script access allowed');
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 0");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization");
//header('Content-type: text/plain; charset=utf-8');
class Api_v12 extends CI_Controller {

	function __construct() {
        parent::__construct();		
		//error_reporting(0);
		
		// set default time zone -
		//date_default_timezone_set('Asia/Kolkata');		
  		$this->load->database();		
  		$this->load->helper('url');
		$this->load->helper('file');		
  		$this->load->library('session');		
		
		ini_set('max_execution_time', 0);
		ini_set('max_input_time', 0);
		ini_set('upload_max_filesize', "1000M");
		ini_set('post_max_size', "1000M");
		ini_set('memory_limit','-1');
		//ignore_user_abort(true);
		set_time_limit(0);
		
		$this->log_id = 0;	
    }
	
	// Main Function By Default Execute
	public function index()
	{												
		$response = array("status" => 0, "message" => "");
		
		// check request type -
		if($_SERVER['REQUEST_METHOD'] == "POST")
		{
			//echo "<pre>"; print_r($_SERVER); die();			
			// get API authentication token from request header
			$token = getBearerToken();
			
			// check if authentication token
			if($token != null)
			{
				// validate API authentication token from request header
				if(validateToken($token))
				{
					$_POST = json_decode(file_get_contents("php://input"), TRUE);
										
					//$postData = $this->sql_clean($this->db->conn_id, $_POST);
					//echo "<pre>";print_r($_POST);die();
					
					$res = array();
					$this->log_id = $this->api_log($_POST, $res, FALSE);
					
					if(isset($_POST['action']) && $_POST['action'] != "")
					{  
						// get tag value -
						$action = $_POST['action'];
						
						if($action == "login")
						{
							$this->login();
						}
						else if($action == "forgot_password")
						{
							$this->forgot_password();
						} 	
						else if($action == "verify_otp")
						{
							$this->verify_otp();
						} 
						else if($action == "change_password")
						{
							$this->change_password();
						}
						else if($action == "get_taluka_listing")
						{
							$this->get_taluka_listing();
						}
						else if($action == "get_taluka")
						{
							$this->get_taluka();
						}
						else if($action == "get_village_listing")
						{
							$this->get_village_listing();
						}
						else if($action == "get_village")
						{
							$this->get_village();
						}
						else if($action == "get_cmrc_user_listing")
						{
							$this->get_cmrc_user_listing();
						}
						else if($action == "get_cmrc_user")
						{
							$this->get_cmrc_user();
						}
						else if($action == "get_cast_listing")
						{
							$this->get_cast_listing();
						}
						else if($action == "get_cast")
						{
							$this->get_cast();
						}
						else if($action == "get_product_category")
						{
							$this->get_product_category();
						}
						else if($action == "get_category")
						{
							$this->get_category();
						}
						else if($action == "get_product")
						{
							$this->get_product();
						}
						else if($action == "get_product_name")
						{
							$this->get_product_name();
						}
						else if($action == "get_product_category_list")
						{
							$this->get_product_category_list();
						}
						else if($action == "get_variety_list")
						{
							$this->get_variety_list();
						}
						else if($action == "get_variety")
						{
							$this->get_variety();
						}
						else if($action == "get_season_list")
						{
							$this->get_season_list();
						}
						else if($action == "get_season")
						{
							$this->get_season();
						}
						else if($action == "get_marital_status_list")
						{
							$this->get_marital_status_list();
						}
						else if($action == "get_marital_status")
						{
							$this->get_marital_status();
						}
						else if($action == "get_disability_type_list")
						{
							$this->get_disability_type_list();
						}
						else if($action == "get_disability_type")
						{
							$this->get_disability_type();
						}
						else if($action == "get_measurement")
						{
							$this->get_measurement();
						}
						else if($action == "get_min_max")
						{
							$this->get_min_max();
						}
						else if($action == "user_profile")
						{
							$this->user_profile();
						}
						else if($action == "shg_registration")
						{
							$this->shg_registration();
						}
						else if($action == "shg_listing")
						{
							$this->shg_listing();
						}
						else if($action == "get_survey_list")
						{
							$this->get_survey_list();
						}
						else if($action == "get_category_survey_listing")
						{
							$this->get_category_survey_listing();
						}
						else if($action == "get_wearhouse_type_list")
						{
							$this->get_wearhouse_type_list();
						}
						else if($action == "get_wearhouse_type")
						{
							$this->get_wearhouse_type();
						}
						else if($action == "get_wearhouse_category_list")
						{
							$this->get_wearhouse_category_list();
						}
						else if($action == "get_wearhouse_category")
						{
							$this->get_wearhouse_category();
						}
						else if($action == "publisedSection")
						{
							$this->publisedSection();
						}
						else if($action == "survey_questionire")
						{
							$this->survey_questionire();
						}
						else if($action == "save_response")
						{	
							$this->save_response();
						}
						else if($action == "response_list")
						{
							$this->response_list();
						}
						else if($action == "survey_response_details")
						{
							$this->survey_response_details();
						}
						else if($action == "logout")
						{
							$this->logout();
						}
						else if($action == "check_update")
						{ 
							$this->check_update();
						} 	// sample function for pagination testing
						else if($action == "response_list_pagination") 
						{ 
							$this->response_list_pagination();
						}	
						else
						{
							$response['status']		= "0";
							//$response['message'] 	= "Invalid request method.";
							$response['message'] 	= "अवैध विनंती पद्धत.";
							
							$this->response($response);
						}
					}
					else
					{
						$response['status'] 	= "0";
						//$response['message'] 	= "Request can not be null.";
						$response['message'] 	= "विनंती शून्य असू शकत नाही.";
						
						$this->response($response);
					}
				}
				else
				{
					$response['status'] 	= "0";
					//$response['message'] 	= "Invalid api key.";
					$response['message'] 	= "अवैध एपीआय की.";
					
					$this->response($response);
				}
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "API authentication missing.";
				$response['message'] 	= "API प्रमाणीकरण गहाळ आहे.";
				
				$this->response($response);
			}
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "Invalid request type. Must use Request type as POST.";
			$response['message'] 	= "अवैध विनंती प्रकार. विनंती प्रकार पीओएसटी म्हणून वापरणे आवश्यक आहे.";
			
			$this->response($response);
		}
	}
	
	// login API
	public function login()
	{
		$response = array("status" => 0, "message" => "");		
		// get POST data -
		$village_code	= isset($_POST['village_code']) ? $_POST['village_code'] : '';
		//$password 	= isset($_POST['password']) ? $_POST['password'] : '';	
		if(empty($village_code))
		{
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Village Code or Password can not be blank.";
			$response['message'] 	= "गाव कोड रिक्त असू शकत नाही.";
		}
		else
		{
			// encrypt password using sha1() -
			//$password = sha1($password);
			
			// check login username and password -		
			
			$get_village_id = $this->master_model->getRecords("mv_village_master", array("village_code" => $village_code));
			
			if(count($get_village_id) > 0) 
			{
				$get_count = $this->master_model->getRecords("mv_login_attempt", array("village_id" => @$get_village_id[0]['village_id']));
					
				if(@$get_count[0]['login_cnt'] == 3)
				{
					$response['status'] 	= "0";
					$response['redirect'] 	= true;
					//$response['message'] 	= "Your login attempt is over. Redirect to forgot password page.";
					$response['message'] 	= "आपला लॉगिन प्रयत्न संपला आहे. विसरलेल्या संकेतशब्द पृष्ठाकडे पुनर्निर्देशित करा.";
				}
				else
				{
				
					$query = $this->db->query("SELECT mv_assign_login_details.aid, mv_assign_login_details.admin_id, mv_assign_login_details.password, mv_admin_user.first_name,mv_admin_user.middle_name, mv_admin_user.last_name,
					mv_assign_login_details.status, mv_assign_login_details.is_login, mv_assign_login_details.session_token FROM mv_assign_login_details LEFT JOIN mv_admin_user ON mv_admin_user.admin_id = mv_assign_login_details.admin_id WHERE mv_assign_login_details.village_id = ".$get_village_id[0]['village_id']." AND mv_assign_login_details.is_deleted = '0' AND mv_assign_login_details.status='Active' AND  mv_admin_user.status='Active' ORDER BY aid DESC LIMIT 1");
					
					$cntR = $query->num_rows();
					//echo $this->db->last_query();
					$i = 0;
					if($cntR > 0)
					{
						
						foreach($query->result_array() as $row)
						{
							$get_count_user = $this->master_model->getRecords("mv_admin_user", array("admin_id" => @$row['admin_id']));
							
							if(count($get_count_user) > 0)
							{
								
								if($row['status'] == 'Active')
								{
									/*if($row['is_login'] == '0')
									{*/
										//$fullname = ucfirst($row['first_name'])." ".ucfirst($row['last_name']);
										
										$user_id 	 = $row['aid'];
										$edited_date = $last_login_on = date("Y-m-d H:i:s");
				
										$sql_update = "UPDATE mv_assign_login_details SET is_login = '1', last_login_on = '".$last_login_on."', updated_on = '".$edited_date."' WHERE aid = '".$user_id."'";
										$query_update = $this->db->query($sql_update);
										
										$session_token = generateSessionToken();
										setUserSessionToken($session_token, $user_id);										
										// CMRC ID
										$cmrc_id	= isset($get_village_id[0]['cmrc_id']) ? $get_village_id[0]['cmrc_id'] : '';
										
										// Taluka Details From CMRC Master
										$get_cmrc_details = $this->master_model->getRecords("mv_cmrc_master", array("cmrc_id" => $cmrc_id));

										$taluka_id	= isset($get_cmrc_details[0]['taluka_id']) ? $get_cmrc_details[0]['taluka_id'] : '';

										$cmrc_name_marathi	= isset($get_cmrc_details[0]['cmrc_name_marathi']) ? $get_cmrc_details[0]['cmrc_name_marathi'] : '';

										$get_taluka_details = $this->master_model->getRecords("mv_taluka_master", array("taluka_id" => $taluka_id));

										$taluka_name_marathi	= isset($get_taluka_details[0]['taluka_name_marathi']) ? $get_taluka_details[0]['taluka_name_marathi'] : '';
										
										// District Details From Taluka ID
										$get_taluka_details = $this->master_model->getRecords("mv_taluka_master", array("taluka_id" => $get_village_id[0]['taluka_id']));
										$district_id	= isset($get_taluka_details[0]['district_id']) ? $get_taluka_details[0]['district_id'] : '';
										
										$data = array('session_token' => $session_token,
														'village_code' => $get_village_id[0]['village_code'],
														'village_id' => $get_village_id[0]['village_id'],
														'village_name_marathi' => $get_village_id[0]['village_name_marathi'],
														'taluka_id' => $taluka_id,
														'taluka_name_marathi' => $taluka_name_marathi,
														'district_id' => $district_id,
														'cmrc_id' => $cmrc_id,
														'cmrc_name_marathi' => $cmrc_name_marathi,
														'login_id' => $row['admin_id'],	
														'user_id' => $row['aid']);
										
										$response['status'] 	= "1";
										//$response['message']	= "User logged in successfully.";
										$response['message']	= "वापरकर्त्याने यशस्वीरित्या लॉग इन केले.";
										$response['data'] 		= $data;
										
										// Delete SQL
										$this->db->delete('mv_login_attempt', array('village_id' => $get_village_id[0]['village_id']));
									
									/*}
									else
									{
										$response['status'] 	= "0";
										$response['message'] 	= "वापरकर्ता आधीच दुसर्या डिव्हाइसमध्ये लॉग इन आहे.";
									}*/
								}
								else
								{
									$response['status'] 	= "0";
									//$response['message'] 	= "User status is Inactive.";
									$response['message'] 	= "वापरकर्त्याची स्थिती निष्क्रिय आहे.";
								}
							
							}
							else
							{
								$response['status'] 	= "0";
								$response['message'] 	= "वापरकर्ता विद्यमान नाही";
							}
							
						}   // Foreach End
							
					}
					else
					{						
						
						//$this->addloginattempt($get_village_id[0]['village_id']);						
						$response['status'] 	= "0";
						//$response['message'] 	= "Invalid username or password.";
						//$response['message'] 	= "अवैध वापरकर्तानाव किंवा संकेतशब्द.";
						$response['message'] 	= "अवैध गाव कोड";
					}				
				
				} // Login Attempt End
				
			}
			else 
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "Enter Village Code is wrong.";
				$response['message'] 	= "कृपया योग्य गाव कोड प्रविष्ट करा.";
			}
			
		}
		
		$this->response($response);
	}
	
	// Add Login Attempt
	public function addloginattempt($villageID)
	{
		$this->db->select('count(*) AS TOTAL, login_cnt');
		$get_count = $this->master_model->getRecords("mv_login_attempt", array("village_id" => $villageID));
		//echo "==0==".$this->db->last_query();
		if($get_count[0]['TOTAL'] == 0)
		{
			$dataArr = array('village_id' => $villageID, 'ip_address' => $_SERVER['REMOTE_ADDR'], 'login_cnt' => 1);
			$this->master_model->insertRecord('mv_login_attempt',$dataArr);
			//echo "==1==".$this->db->last_query();
		}
		else 
		{
			$nextCnt = $get_count[0]['login_cnt']+1;
			$dataArr = array('login_cnt' => $nextCnt);
			$this->master_model->updateRecord('mv_login_attempt',$dataArr,array('village_id' => $villageID));
			//echo "==2==".$this->db->last_query();
		}
		
	}
	
	// Forgot Password
	public function forgot_password()
	{		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$mobile_no	= isset($_POST['mobile_no']) ? $_POST['mobile_no'] : '';
		
		if(empty($mobile_no)){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Mobile Number is required.";
			$response['message'] 	= "मोबाइल नंबर आवश्यक आहे.";
			
		} else {			
			
			$query = $this->db->query("SELECT mobile_no, status, is_deleted FROM mv_assign_login_details WHERE mobile_no = '".$mobile_no."'");
			
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					
					if($row['is_deleted'] == 1){
						
						$response['status'] 	= "0";
						//$response['message'] 	= "User is deleted. Please contact to your administrator.";
						$response['message'] 	= "वापरकर्ता हटविला गेला आहे. कृपया आपल्या प्रशासकाशी संपर्क साधा.";

					} else {
							
							$random_number = mt_rand(1000,9999);
							
							$this->db->select('count(*) AS TOTAL, resend_cnt');
							$check_exist = $this->master_model->getRecords("mv_verify_otp", array("mobile_no" => $mobile_no));
							
							if($check_exist[0]['resend_cnt'] == 3)
							{
								
								$response['status'] 	= "2";
								//$response['message'] 	= "OTP send limit is over. Please contact to Administrator";
								$response['message'] 	= "ओटीपी पाठविण्याची मर्यादा संपली आहे. कृपया प्रशासकाशी संपर्क साधा.";
							}
							else 
							{
								if($check_exist[0]['TOTAL'] > 0){
								
									$totalCnt = $check_exist[0]['resend_cnt']+1;
									$updateArr = array('random_code' => $random_number, 'resend_cnt' => $totalCnt, 'created_on' => date('Y-m-d H:i:s'));			
									$updateQuery = $this->master_model->updateRecord('mv_verify_otp',$updateArr,array('mobile_no' => $mobile_no));
									//echo "==UPDATE==".$this->db->last_query();
								} else {
									
									$insertData = array('random_code' => $random_number, 'mobile_no' => $mobile_no, 'resend_cnt' => "1",'created_on' => date('Y-m-d H:i:s'));
									$this->master_model->insertRecord('mv_verify_otp',$insertData);
									//echo "==INSERT==".$this->db->last_query();							
								}
								
								$response['status'] 	= "1";
								$response['otp'] 		= $random_number;
								//$response['message'] 	= "OTP successfully sent on your registered mobile number.";
								$response['message'] 	= "आपल्या नोंदणीकृत मोबाइल क्रमांकावर ओटीपी यशस्वीरित्या पाठविला.";
								
							}								
							
					}	
				}
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
		
	}
	
	// Check Update
	/*public function check_update()
	{
		$response = array("status" => 0, "message" => "");		
		// get POST data -
		$device_token	= isset($_POST['device_token']) ? $_POST['device_token'] : '';
		$apk_version 	= isset($_POST['apk_version']) ? $_POST['apk_version'] : '';	
		//if(empty($device_token) || empty($apk_version))
		if(empty($apk_version))
		{
			// request not be null -
			$response['status'] 	= "0";
    		$response['message'] 	= "Required fields can not be blank.";
		}
		else
		{
			// get logged in user id
			$user_id = getUserIdForSessionToken();
			// check login username and password -
			$query = $this->db->query("SELECT user_id, status FROM survey_users WHERE user_id = '$user_id' AND is_deleted = '0'");
			$cnt = $query->num_rows();
			$config_apk_version  = $this->config->item('apk_version');			
			$is_notice_available = $this->config->item('is_notice_available');
			$notice_text 		 = $this->config->item('notice_text');
			$is_update_available = 0;
			if($apk_version != $config_apk_version) 
			{
				$is_update_available = 1;
			}			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					if($device_token)
					{
						$edited_date = $last_login_on = date("Y-m-d H:i:s");	
						$sql_update = "UPDATE survey_users SET device_token = '".$device_token."', updated_on = '".$edited_date."' WHERE user_id = '".$user_id."'";
						$query_update = $this->db->query($sql_update);
					}
				
					$data = array('is_update_available' => $is_update_available, 'is_notice_available' => $is_notice_available, 'notice_text' => $notice_text);
					
					$response['status'] 	= "1";
					$response['message'] 	= "Device token updated successfully.";
					$response['data'] 		= $data;
				}			
			}
			else
			{
				$response['status'] 	= "0";
				$response['message'] 	= "Login required to Continue.";
			}
		} //Else Part
		
		$this->response($response);
	}*/
		
	// OTP Verification
	public function verify_otp()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$mobile_no	= isset($_POST['mobile_no']) ? $_POST['mobile_no'] : '';
		$code		= isset($_POST['code']) ? $_POST['code'] : '';
		//$state_code	= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		//print_r($_POST);
		if(empty($code)){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Verification code is required.";
			$response['message'] 	= "सत्यापन कोड आवश्यक आहे.";
			
		} else {			
			
			$query = $this->db->query("SELECT random_code, status, mobile_no FROM mv_verify_otp WHERE random_code = '".$code."' AND mobile_no = '".$mobile_no."'");		
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{						
					/*if($row['random_code'] != $code)
					{
						$response['status'] 	= "0";
						$response['message'] 	= "Enter verification code is wrong.";
					}
					else 
					{*/
					
						if($row['random_code'] == $code)
						{
							//$state_code = md5(time()."+".$mobile_no);
							$response['status'] 		= "1";
							$response['message'] 		= "OTP successfully verify.";
							$response['is_otp_verified']= 'TRUE';
							//$response['random_str_1'] 	= $state_code;
							
							// Delete SQL
							$this->db->delete('mv_verify_otp', array('random_code' => $code, 'mobile_no' => $mobile_no));
						}
						else
						{
							$response['status'] 	= "0";
							//$response['message'] 	= "Somthing wrong. Please try again.";
							$response['message'] 	= "काहीतरी चुकीचे. कृपया पुन्हा प्रयत्न करा.";
						}					
					//}					
				}
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "Enter verification code is wrong.";
				$response['message'] 	= "सत्यापन कोड चुकीचा आहे प्रविष्ट करा.";
			}
			
		}
		
		$this->response($response);
		
	} // End Of Function Verify OTP
	
	// Update Password 
	public function change_password()
	{
		
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$password		= isset($_POST['password']) ? $_POST['password'] : '';
		$cnfpassword	= isset($_POST['cnfpassword']) ? $_POST['cnfpassword'] : '';
		$mobile_no		= isset($_POST['mobile_no']) ? $_POST['mobile_no'] : '';
		//$state_code		= isset($_POST['state_code']) ? $_POST['state_code'] : '';
		
		$uppercase = preg_match('@[A-Z]@', $password);
		$lowercase = preg_match('@[a-z]@', $password);
		$number    = preg_match('@[0-9]@', $password);
		$specialChars = preg_match('@[^\w]@', $password);
		
		if(empty($password)){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Password is required.";
			$response['message'] 	= "संकेतशब्द आवश्यक आहे.";
			
		} else if(empty($cnfpassword)){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Confirm Password is required.";
			$response['message'] 	= "कन्फर्म पासवर्ड आवश्यक आहे.";
			
		} else if($password != $cnfpassword){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Password & Confirm Password does not match.";
			$response['message'] 	= "संकेतशब्द आणि पुष्टीकरण संकेतशब्द जुळत नाही.";
			
		} else if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 6){
			
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Password should be at least 6 characters in length and should include at least one upper case letter, one number, and one special character.";
			$response['message'] 	= "संकेतशब्दाची लांबी कमीतकमी 6 वर्णांची असावी आणि त्यात कमीतकमी एक अप्पर केस अक्षर, एक संख्या आणि एक विशेष वर्ण असावा.";
			
		} else {

			$this->db->select('mobile_no, admin_id, village_id');
			$get_count = $this->master_model->getRecords("mv_assign_login_details", array("mobile_no" => $mobile_no));
			
			if(count($get_count) > 0)
			{
				foreach($get_count as $row)
				{						
					//$values = '"'.$email_id.'"';
					// UPDATE STATUS
					$updateArr = array('password' => sha1($password), 'updated_on' => date("Y-m-d H:i:s"), 'updated_by_id' => $row['admin_id']);			
					$updateQuery = $this->master_model->updateRecord('mv_assign_login_details',$updateArr,array('mobile_no' => $mobile_no));

					// Delete Entry 
					$this->db->delete('mv_login_attempt', array('village_id' => $row['village_id']));
					
					$response['status'] 	= "1";
					//$response['message'] 	= "Password successfully updated.";	
					$response['message'] 	= "संकेतशब्द यशस्वीरित्या अद्यतनित केला.";	
				}
			
			}
			else 
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "Enter details are incorrect.";
				$response['message'] 	= "प्रविष्ट करा तपशील चुकीचे आहेत.";
			}
			
		}
		
		$this->response($response);
		
	}
	
	// Get Taluka Listing
	public function get_taluka_listing()
	{
		$response = array("status" => 0, "message" => "");
		
		//$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			$crp_details 	= $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));	
			$admin_id 		= $crp_details[0]['admin_id'];
			$user_details 	= $this->master_model->getRecords("mv_admin_user", array("admin_id" => $admin_id));
			$district_id 	= $user_details[0]['district_id'];
			$taluka_id 		= $user_details[0]['taluka_id'];
			
			//$query = $this->db->query("SELECT district_id, taluka_id, taluka_name, taluka_name_marathi FROM mv_taluka_master WHERE district_id = '".$district_id."' AND is_deleted = '0' AND status = 'Active'");
			
			$query = $this->db->query("SELECT district_id, taluka_id, taluka_name, taluka_name_marathi FROM mv_taluka_master WHERE taluka_id = '".$taluka_id."' AND is_deleted = '0' AND status = 'Active'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					//$response['data'][] = array('taluka_id' => $row['taluka_id'], 'taluka_name' => $row['taluka_name'], 'taluka_name_marathi' => $row['taluka_name_marathi'], 'district_id' => $row['district_id']);
					$response['data'][] = array('taluka_id' => $row['taluka_id'], 'taluka_name' => $row['taluka_name_marathi'], 'taluka_name_marathi' => $row['taluka_name_marathi'], 'district_id' => $row['district_id']);
				}
			
				$response['status'] 	= "1";
				//$response['message'] 	= "Taluka List Get Successfully.";
				$response['message'] 	= "तालुका यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 		= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
			
		}
		
		$this->response($response);
	}

	// Get Taluka Dump
	public function get_taluka()
	{
		$response = array("status" => 0, "message" => "");
		
		//$district_id	= isset($_POST['district_id']) ? $_POST['district_id'] : '';
		
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			$crp_details 	= $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));	
			$admin_id 		= $crp_details[0]['admin_id'];
			$user_details 	= $this->master_model->getRecords("mv_admin_user", array("admin_id" => $admin_id));
			$district_id 	= $user_details[0]['district_id'];
			$taluka_id 		= $user_details[0]['taluka_id'];
			
			//$query = $this->db->query("SELECT district_id, taluka_id, taluka_name, taluka_name_marathi FROM mv_taluka_master WHERE district_id = '".$district_id."' AND is_deleted = '0' AND status = 'Active'");
			
			$query = $this->db->query("SELECT district_id, taluka_id, taluka_name, taluka_name_marathi FROM mv_taluka_master WHERE taluka_id = '".$taluka_id."' AND is_deleted = '0' AND status = 'Active'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					//$response['data'][] = array('taluka_id' => $row['taluka_id'], 'taluka_name' => $row['taluka_name'], 'taluka_name_marathi' => $row['taluka_name_marathi'], 'district_id' => $row['district_id']);					
					$response['data'][] = array('taluka_id' => $row['taluka_id'], 'taluka_name' => $row['taluka_name_marathi'], 'taluka_name_marathi' => $row['taluka_name_marathi'], 'district_id' => $row['district_id']);
				}
			
				$response['status'] 	= "1";
				//$response['message'] 	= "Taluka List Get Successfully.";
				$response['message'] 	= "तालुका यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 		= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
			
		}
		
		$this->response($response);
	}
	
	// Get Village Listing
	public function get_village_listing()
	{
		$response = array("status" => 0, "message" => "");
		
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			$crp_details 	= $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));
			$village_id 		= $crp_details[0]['village_id'];
			
			$query = $this->db->query("SELECT cmrc_id, village_id, village_name, village_name_marathi, village_code FROM mv_village_master WHERE village_id = '".$village_id."' AND is_deleted = '0' AND status = 'Active'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					//$response['data'][] = array('village_id' => $row['village_id'], 'village_name' => $row['village_name'], 'village_name_marathi' => $row['village_name_marathi'], 'village_code' => $row['village_code'], 'cmrc_id' => $row['cmrc_id']);
					
					$response['data'][] = array('village_id' => $row['village_id'], 'village_name' => $row['village_name_marathi'], 'village_name_marathi' => $row['village_name_marathi'], 'village_code' => $row['village_code'], 'cmrc_id' => $row['cmrc_id']);
				}
				/*$response['data'][] = array('village_id' => '0', 'village_name' => 'Other', 'village_name_marathi' => 'Other', 'village_code' => '', 'taluka_id' => '');*/
				
				$response['status'] 	= "1";
				//$response['message'] 	= "Village List Get Successfully.";
				$response['message'] 	= "गाव यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}	
		}
		
		$this->response($response);
	}
	
	// Get Village Dump
	public function get_village()
	{
		$response = array("status" => 0, "message" => "");
		
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			/*$crp_details 	= $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));
			$village_id 		= $crp_details[0]['village_id'];*/

			$crp_query = $this->db->query("SELECT * FROM mv_admin_user u1 LEFT JOIN mv_assign_login_details u2 ON u1.admin_id = u2.admin_id WHERE u2.aid = '".$user_id."' AND u1.is_deleted = '0' AND u1.status = 'Active'");
			
			$crp_details = $crp_query->result_array();
			$cmrc_id = $crp_details[0]['cmrc_id'];

			$query = $this->db->query("SELECT village_id, village_name, village_name_marathi, village_code, cmrc_id FROM mv_village_master WHERE cmrc_id = '".$cmrc_id."' AND is_deleted = '0' AND status = 'Active'");

			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					//$response['data'][] = array('village_id' => $row['village_id'], 'village_name' => $row['village_name'], 'village_name_marathi' => $row['village_name_marathi'], 'village_code' => $row['village_code'], 'cmrc_id' => $row['cmrc_id']);
					$response['data'][] = array('village_id' => $row['village_id'], 'village_name' => $row['village_name_marathi'], 'village_name_marathi' => $row['village_name_marathi'], 'village_code' => $row['village_code'], 'cmrc_id' => $row['cmrc_id']);
				}
				//$response['data'][] = array('village_id' => '0', 'village_name' => 'Other', 'village_name_marathi' => 'Other', 'village_code' => '', 'taluka_id' => '');
				
				$response['status'] 	= "1";
				//$response['message'] 	= "Village List Get Successfully.";
				$response['message'] 	= "गाव यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}	
		}
		
		$this->response($response);
	}
	
	// Get Cast Listing
	public function get_cast_listing()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT cast_id, cast_english, cast_marathi FROM mv_cast_category WHERE is_deleted = '0' AND status = 'Active'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('cast_id' => $row['cast_id'], 'cast_english' => $row['cast_marathi'], 'cast_marathi' => $row['cast_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "कास्ट यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Cast Dump
	public function get_cast()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT cast_id, cast_english, cast_marathi FROM mv_cast_category");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('cast_id' => $row['cast_id'], 'cast_english' => $row['cast_marathi'], 'cast_marathi' => $row['cast_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "कास्ट यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Variety Listing
	public function get_variety_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$product_id	= isset($_POST['product_id']) ? $_POST['product_id'] : '';
		
		if(empty($product_id))
		{
			// request not be null -
			$response['status'] 	= "0";
    		//$response['message'] 	= "Taluka name is required.";
			$response['message'] 	= "उत्पादन प्रकार नाव निवडा.";
			
		} 
		else 
		{
			$query = $this->db->query("SELECT variety_id, product_id, variety_name_english, variety_name_marathi FROM mv_variety_master WHERE product_id='".$product_id."' AND is_deleted = '0' AND status = 'Active'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('variety_id' => $row['variety_id'], 'product_id' => $row['product_id'], 'variety_name_english' => $row['variety_name_marathi'], 'variety_name_marathi' => $row['variety_name_marathi']);
				}
				$response['data'][] = array('variety_id' => 'Other', 'product_id' => $product_id, 'variety_name_english' => 'इतर', 'variety_name_marathi' => 'इतर');
				
				$response['status'] 	= "1";
				//$response['message'] 	= "Cast List Get Successfully.";
				$response['message'] 	= "उत्पादन प्रकार  यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
	}
	
	// Get Variety Dump
	public function get_variety()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT variety_id, product_id, variety_name_english, variety_name_marathi FROM mv_variety_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('variety_id' => $row['variety_id'], 'product_id' => $row['product_id'], 'variety_name_english' => $row['variety_name_marathi'], 'variety_name_marathi' => $row['variety_name_marathi']);
			}
			$response['data'][] = array('variety_id' => 'Other', 'product_id' => '', 'variety_name_english' => 'इतर', 'variety_name_marathi' => 'इतर');
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "कास्ट यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	public function get_season_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT season_id, season_name_english, season_name_marathi FROM mv_season");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('season_id' => $row['season_id'], 'season_name_english' => $row['season_name_marathi'], 'season_name_marathi' => $row['season_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "हंगाम यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	//Season Dump
	public function get_season()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT season_id, season_name_english, season_name_marathi FROM mv_season");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('season_id' => $row['season_id'], 'season_name_english' => $row['season_name_marathi'], 'season_name_marathi' => $row['season_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "हंगाम यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	public function get_marital_status_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT marital_status_id, marital_status_english, marital_status_marathi FROM mv_marital_status");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('marital_status_id' => $row['marital_status_id'], 'marital_status_english' => $row['marital_status_marathi'], 'marital_status_marathi' => $row['marital_status_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "वैवाहिक स्थिती यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	//Marital Status Dump
	public function get_marital_status()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT marital_status_id, marital_status_english, marital_status_marathi FROM mv_marital_status");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('marital_status_id' => $row['marital_status_id'], 'marital_status_english' => $row['marital_status_marathi'], 'marital_status_marathi' => $row['marital_status_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "वैवाहिक स्थिती यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	public function get_disability_type_list()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT disability_type_id, disability_name_english, disability_name_marathi FROM mv_disability_type_master WHERE is_deleted = 0 AND status='Active'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('disability_type_id' => $row['disability_type_id'], 'disability_name_english' => $row['disability_name_marathi'], 'disability_name_marathi' => $row['disability_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "दिव्यांग यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Disability dump
	public function get_disability_type()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT disability_type_id, disability_name_english, disability_name_marathi FROM mv_disability_type_master WHERE is_deleted = 0 AND status='Active'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('disability_type_id' => $row['disability_type_id'], 'disability_name_english' => $row['disability_name_marathi'], 'disability_name_marathi' => $row['disability_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "दिव्यांग यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Measurement Dump
	public function get_measurement()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT mid, m_name, e_name FROM mv_measurement_master WHERE status = 'Active' AND is_deleted = '0'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				//$response['data'][] = array('mid' => $row['mid'], 'm_name' => $row['m_name']);
				$response['data'][] = array('mid' => $row['mid'], 'm_name' => $row['m_name'], 'e_name' => $row['e_name']);
			}
			//$response['data'][] = array('variety_id' => 'Other', 'block_name' => 'Other', 'product_id' => '', 'variety_name_english' => '', 'variety_name_marathi' => '');
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Cast List Get Successfully.";
			$response['message'] 	= "मोजमाप यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 	= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Product Category from master
	public function get_product_category()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			$query = $this->db->query("SELECT product_cat_id, p_category_name_english, p_category_name_marathi FROM mv_product_category WHERE status = 'Active' AND is_deleted = '0'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('product_cat_id' => $row['product_cat_id'], 'p_category_name_marathi' => $row['p_category_name_marathi'], 'p_category_name_english' => $row['p_category_name_marathi']);
				}

				/*$response['data'][] = array('product_cat_id' => '0', 'p_category_name_marathi' => 'इतर', 'p_category_name_english' => 'Other');*/
			
				$response['status'] 	= "1";
				//$response['message'] 	= "Product Cagegory List Get Successfully.";
				$response['message'] 	= "उत्पादन केजोरी सूची यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
	}
	
	// Get Product Category DUMP
	public function get_category()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT product_cat_id, p_category_name_english, p_category_name_marathi FROM mv_product_category WHERE is_deleted = '0'");
			$cnt = $query->num_rows();
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$response['data'][] = array('product_cat_id' => $row['product_cat_id'], 'p_category_name_marathi' => $row['p_category_name_marathi'], 'p_category_name_english' => $row['p_category_name_marathi']);
				}

				/*$response['data'][] = array('product_cat_id' => '0', 'p_category_name_marathi' => 'इतर', 'p_category_name_english' => 'Other');*/
			
				$response['status'] 	= "1";
				//$response['message'] 	= "Product Cagegory List Get Successfully.";
				$response['message'] 	= "उत्पादन केजोरी सूची यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		
		$this->response($response);
	}
	
	// Get Product Dump
	public function get_product()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT product_id, product_cat_id, product_name_marathi, product_name_english FROM mv_product_master");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('product_id' => $row['product_id'], 'product_cat_id' => $row['product_cat_id'], 'product_name_marathi' => $row['product_name_marathi'], 'product_name_english' => $row['product_name_marathi']);
			}
		
			$response['status'] 	= "1";
			//$response['message'] 	= "Product List Get Successfully.";
			$response['message'] 	= "उत्पादन यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 		= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Product name from master
	public function get_product_name()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT product_id, product_name_marathi, product_name_english FROM mv_product_master WHERE status = 'Active' AND is_deleted = '0'");
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			foreach($query->result_array() as $row)
			{
				$response['data'][] = array('product_id' => $row['product_id'], 'product_name_marathi' => $row['product_name_marathi'], 'product_name_english' => $row['product_name_marathi']);
			}
		
			$response['status'] 	= "1";
			//$response['message'] 	= "Product Name List Get Successfully.";
			$response['message'] 	= "उत्पादनाच्या नावाची यादी यशस्वीरित्या मिळवा.";
		}
		else
		{
			$response['status'] 		= "0";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}
	
	// Get Category Wise Product Listing
	public function get_product_category_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		$product_cat_id	= isset($_POST['product_cat_id']) ? $_POST['product_cat_id'] : '';
		
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else
		{
			if(empty($product_cat_id))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Product Category is required.";
				$response['message'] 	= "उत्पादन श्रेणी आवश्यक आहे.";
				
			} 
			else 
			{
				$query = $this->db->query("SELECT product_id, product_cat_id, product_name_marathi, product_name_english FROM mv_product_master WHERE product_cat_id = '".$product_cat_id."' AND status = 'Active' AND is_deleted = '0'");
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					foreach($query->result_array() as $row)
					{
						$response['data'][] = array('product_id' => $row['product_id'], 'product_cat_id' => $row['product_cat_id'], 'product_name_marathi' => $row['product_name_marathi'], 'product_name_english' => $row['product_name_marathi']);
					}
				
					$response['status'] 	= "1";
					//$response['message'] 	= "Product List Get Successfully.";
					$response['message'] 	= "उत्पादन यादी यशस्वीरित्या मिळवा.";
				}
				else
				{
					$response['status'] 		= "0";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
			}
			
		}
		
		$this->response($response);
	}
	
	// CMRC User Listing
	public function get_cmrc_user_listing()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			
			
			$village_id = $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));	
			
			$query = $this->db->query("SELECT cmrc_id, village_id FROM mv_village_master WHERE village_id = '".$village_id[0]['village_id']."' AND status = 'Active'");
			//echo "+++".$this->db->last_query();
			$cnt = $query->num_rows();
			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$cmrc_details = $this->master_model->getRecords("mv_cmrc_master", array("cmrc_id" => $row['cmrc_id']));	
					//echo $this->db->last_query();
					$response['data'][] = array('cmrc_id' => $row['cmrc_id'], 'cmrc_name' => $cmrc_details[0]['cmrc_name_marathi'], 'cmrc_name_marathi' => $cmrc_details[0]['cmrc_name_marathi']);
				}
			
				$response['status'] 	= "1";
				//$response['message'] 	= "CMRC User list Get Successfully.";
				$response['message'] 	= "सीएमआरसी वापरकर्ता यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
	}
	
	// CMRC User Dump
	public function get_cmrc_user()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			
			$village_id = $this->master_model->getRecords("mv_assign_login_details", array("aid" => $user_id));	
			
			$query = $this->db->query("SELECT cmrc_id, village_id FROM mv_village_master WHERE village_id = '".$village_id[0]['village_id']."' AND status = 'Active'");
			//echo "+++".$this->db->last_query();
			$cnt = $query->num_rows();
			
			
			if($cnt > 0)
			{
				foreach($query->result_array() as $row)
				{
					$cmrc_details = $this->master_model->getRecords("mv_cmrc_master", array("cmrc_id" => $row['cmrc_id']));	
					$response['data'][] = array('cmrc_id' => $row['cmrc_id'], 'cmrc_name' => $cmrc_details[0]['cmrc_name_marathi'], 'cmrc_name_marathi' => $cmrc_details[0]['cmrc_name_marathi']);
				}
			
				$response['status'] 	= "1";
				//$response['message'] 	= "CMRC User list Get Successfully.";
				$response['message'] 	= "सीएमआरसी वापरकर्ता यादी यशस्वीरित्या मिळवा.";
			}
			else
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
	}
	
	// Profile Details 
	public function user_profile()
	{
		$response = array("status" => 0, "message" => "");
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		//echo "===".$user_id;die();
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			$user_info = $this->master_model->getRecords("mv_assign_login_details",array('is_deleted' => 0, 'aid' => $user_id, 'status' => 'Active' ));
			//print_r($user_info);
			//echo $this->db->last_query();die();
			if(count($user_info) > 0)
			{
				
				foreach($user_info as $row)
				{
					$this->db->select('village_id, cmrc_id, village_code, village_name, village_name_marathi');
					$village_info = $this->master_model->getRecords("mv_village_master",array('is_deleted' => 0, 'village_id' => $row['village_id'], 'status' => 'Active' ));
					
					$this->db->select('admin_id, email, role_id, aadhar_no, mobile_no, first_name, middle_name, last_name, status, district_id');
					$user_detail = $this->master_model->getRecords("mv_admin_user",array('is_deleted' => 0, 'admin_id' => $user_info[0]['admin_id'], 'status' => 'Active' ));

					$email_id		= isset($user_detail[0]['email']) ?$user_detail[0]['email'] : '';
					$aadhar_no		= isset($user_detail[0]['aadhar_no']) ? $user_detail[0]['aadhar_no'] : '';
					$mobile_no		= isset($user_detail[0]['mobile_no']) ? $user_detail[0]['mobile_no'] : '';
					$first_name		= isset($user_detail[0]['first_name']) ? $user_detail[0]['first_name'] : '';
					$middle_name	= isset($user_detail[0]['middle_name']) ? $user_detail[0]['middle_name'] : '';
					$last_name		= isset($user_detail[0]['last_name']) ? $user_detail[0]['last_name'] : '';
					$role_id		= isset($user_detail[0]['role_id']) ? $user_detail[0]['role_id'] : '';
					$status			= isset($user_detail[0]['status']) ? $user_detail[0]['status'] : '';
					$village_name_marathi = isset($village_info[0]['village_name_marathi']) ? $village_info[0]['village_name_marathi'] : '';
					$village_code	= isset($village_info[0]['village_code']) ? $village_info[0]['village_code'] : '';
					$district_id	= isset($user_detail[0]['district_id']) ? $user_detail[0]['district_id'] : '';					
					
					$response_data[] = array('admin_id' 		=> $row['admin_id'], 
											'role_id' 			=> $role_id, 
											'district_id' 		=> $district_id, 
											//'village_id' 		=> $row['village_id'], 
											'first_name' 		=> $first_name, 
											'middle_name' 		=> $middle_name, 
											'last_name' 		=> $last_name,
											'email_id' 			=> $email_id,
											'mobile_no' 		=> $mobile_no,
											'aadhar_no' 		=> $aadhar_no,
											'village_code' 		=> $village_code,	
											//'village_name' 	=> $village_name_marathi, 
											'village_name' 		=> $village_name_marathi, 
											'status'			=> $status);
			
				}
				
					$response['status']		= "1";
					//$response['message'] 	= "Profile sucessfully received.";
					$response['message'] 	= "प्रोफाइल यशस्वीरित्या प्राप्त झाले.";
					$response['data'] 		= $response_data;
			}
			else 
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
			
		}
		
		$this->response($response);
	}
	
	// Farmer/SHG Registration
	public function shg_registration()
	{
		$response = array("status" => 0, "message" => "");
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			// get POST data -
			$ref_id			= isset($_POST['ref_id']) ? $_POST['ref_id'] : '';
			$shg_id			= isset($_POST['shg_id']) ? $_POST['shg_id'] : '';
			$added_date		= isset($_POST['added_date']) ? $_POST['added_date'] : '';
			$taluka_id		= isset($_POST['taluka_id']) ? $_POST['taluka_id'] : '';
			$cmrc_id		= isset($_POST['cmrc_id']) ? $_POST['cmrc_id'] : '';
			$village_id		= isset($_POST['village_id']) ? $_POST['village_id'] : '';
			$other			= isset($_POST['other']) ? $_POST['other'] : '';
			$shg_name		= isset($_POST['shg_name']) ? $_POST['shg_name'] : '';
			$shg_father_name= isset($_POST['shg_father_name']) ? $_POST['shg_father_name'] : '';
			$shg_last_name	= isset($_POST['shg_last_name']) ? $_POST['shg_last_name'] : '';
			$mobile_no		= isset($_POST['mobile_no']) ? $_POST['mobile_no'] : '';
			$shg_unique_id	= isset($_POST['shg_unique_id']) ? $_POST['shg_unique_id'] : '';
			$emp_of_mahim	= isset($_POST['emp_of_mahim']) ? $_POST['emp_of_mahim'] : '';
			$shg_marital_status	= isset($_POST['shg_marital_status']) ? $_POST['shg_marital_status'] : '';
			$shg_disability	= isset($_POST['shg_disability']) ? $_POST['shg_disability'] : '';
			$type_of_disability	= isset($_POST['type_of_disability']) ? $_POST['type_of_disability'] : '';
			$shg_location_lat	= isset($_POST['shg_location_lat']) ? $_POST['shg_location_lat'] : '';
			$shg_location_long	= isset($_POST['shg_location_long']) ? $_POST['shg_location_long'] : '';
			$shg_area_of_land	= isset($_POST['shg_area_of_land']) ? $_POST['shg_area_of_land'] : '';
			$land_in_irrigated	= isset($_POST['land_in_irrigated']) ? $_POST['land_in_irrigated'] : '';
			$crop_policy	= isset($_POST['crop_policy']) ? $_POST['crop_policy'] : '';
			$crop_store_in_godown	= isset($_POST['crop_store_in_godown']) ? $_POST['crop_store_in_godown'] : '';
			$godown_village_id	= isset($_POST['godown_village_id']) ? $_POST['godown_village_id'] : '';
			$godown_own_by		= isset($_POST['godown_own_by']) ? $_POST['godown_own_by'] : '';
			$select_item_from_godown	= isset($_POST['select_item_from_godown']) ? $_POST['select_item_from_godown'] : '';
			$other_any			= isset($_POST['other_any']) ? $_POST['other_any'] : '';
			$height_in_foot		= isset($_POST['height_in_foot']) ? $_POST['height_in_foot'] : '';
			$width_in_foot		= isset($_POST['width_in_foot']) ? $_POST['width_in_foot'] : '';
			$yearly_income		= isset($_POST['yearly_income']) ? $_POST['yearly_income'] : '';
			$source_of_income	= isset($_POST['source_of_income']) ? $_POST['source_of_income'] : '';
			$shg_cast_type		= isset($_POST['shg_cast_type']) ? $_POST['shg_cast_type'] : '';
			$is_working_category= isset($_POST['is_working_category']) ? $_POST['is_working_category'] : '';
			$will_work_category	= isset($_POST['will_work_category']) ? $_POST['will_work_category'] : '';
			
			if(empty($shg_name))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Name can not be blank.";
				$response['message'] 	= "नाव रिक्त असू शकत नाही.";
			}
			/*else if(empty($shg_father_name))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Father Name can not be blank.";
				$response['message'] 	= "वडिलांचे नाव रिक्त असू शकत नाही.";
			}*/
			else if(empty($shg_last_name))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Last Name can not be blank.";
				$response['message'] 	= "आडनाव रिक्त असू शकत नाही.";
			}
			else if(empty($mobile_no))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Mobile Number can not be blank.";
				$response['message'] 	= "मोबाइल नंबर रिक्त असू शकत नाही.";
			}
			/*else if(empty($shg_unique_id))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Unique ID can not be blank.";
				$response['message'] 	= "युनिक आयडी रिक्त असू शकत नाही.";
			}*/
			else if(empty($emp_of_mahim))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Employee Of Mahim field can not be blank.";
				$response['message'] 	= "माहीम फील्डचा कर्मचारी रिक्त असू शकत नाही.";
			}
			else if(empty($shg_marital_status))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Marital Status field can not be blank.";
				$response['message'] 	= "वैवाहिक स्थिती फील्ड रिक्त असू शकत नाही.";
			}
			else if(empty($shg_disability))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Disability field can not be blank.";
				$response['message'] 	= "अपंगत्व फील्ड रिक्त असू शकत नाही.";
				if($shg_disability == 'Yes')
				{
					if(empty($type_of_disability))
					{
						// request not be null -
						$response['status'] 	= "0";
						//$response['message'] 	= "Type of Disability field can not be blank.";
						$response['message'] 	= "अपंगत्वाचा प्रकार रिक्त असू शकत नाही.";
					}
				}
				
			}			
			else if(empty($shg_location_lat))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Location latitude field can not be blank.";
				$response['message'] 	= "स्थान अक्षांश फील्ड रिक्त असू शकत नाही.";
			}
			else if(empty($shg_location_long))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Location longitude field can not be blank.";
				$response['message'] 	= "स्थान रेखांश फील्ड रिक्त असू शकत नाही.";
			}
			else if(empty($shg_area_of_land))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Area of land field can not be blank.";
				$response['message'] 	= "भूमीचे क्षेत्र रिक्त असू शकत नाही.";
			}
			else if(empty($land_in_irrigated))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Land in Irrigated field can not be blank.";
				$response['message'] 	= "सिंचनाच्या शेतात जमीन रिकामी असू शकत नाही.";
			}			
			else if(empty($yearly_income))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Yearly incode field can not be blank.";
				$response['message'] 	= "वार्षिक इनकोड फील्ड रिक्त असू शकत नाही.";
			}			
			else if(empty($shg_cast_type))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Cast Type field can not be blank.";
				$response['message'] 	= "कास्ट प्रकार फील्ड रिक्त असू शकत नाही.";
			}
			else if(empty($is_working_category))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Please select currently working category.";
				$response['message'] 	= "कृपया सध्या कार्यरत वर्ग निवडा.";
			}
			/*else if(empty($will_work_category))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Please select next working category.";
				$response['message'] 	= "कृपया पुढील कार्यरत श्रेणी निवडा.";
			}*/
			else 
			{	//$is_working_category $will_work_category
				// Check Ref ID Exist related response id 
				$queryReference = $this->db->query("SELECT ref_id FROM mv_personal_details WHERE ref_id = '".$ref_id."' AND is_deleted = '0'");
				$cntRef = $queryReference->num_rows();
				$ref_res = true;
				
				//if($cntRef == '0')
				//{
					
					if($village_id > 0)
					{
						$other = '';
					}
					
					if($shg_disability == 'No')
					{
						$type_of_disability = '';
					}
					
					/*if($crop_store_in_godown == 'No')
					{	
						$godown_village_id 			= '';
						$godown_own_by 				= 0;
						$select_item_from_godown 	= '';
						$other_any					= '';						
					}*/
					
					if(empty($shg_id) && $cntRef == 0)
					{
					
					$district_id = 0;	
					if($taluka_id > 0)
					{
						$this->db->select('district_id');
						$getDistrict = $this->master_model->getRecords("mv_taluka_master", array("taluka_id" => $taluka_id));
						$district_id = $getDistrict[0]['district_id'];
					}						
					
					$source_of_income = "";	
					// Insert Data
					$dataArr = array('ref_id' 			=> $ref_id,
									'added_date' 		=> $added_date,
									'district_id'       => $district_id,
									'taluka_id' 		=> $taluka_id, 
									'cmrc_id' 			=> $cmrc_id,	
									'village_id' 		=> $village_id,
									'other' 			=> '',		
									'shg_name' 			=> $shg_name,
									'shg_father_name' 	=> $shg_father_name,
									'shg_last_name' 	=> $shg_last_name,
									'mobile_no' 		=> $mobile_no,	
									'shg_unique_id' 	=> $shg_unique_id,
									'emp_of_mahim' 		=> $emp_of_mahim, 
									'shg_marital_status' => $shg_marital_status,
									'shg_disability' 	=> $shg_disability, 
									'type_of_disability' => $type_of_disability,
									'shg_location_lat' 	=> $shg_location_lat, 
									'shg_location_long' => $shg_location_long,
									'shg_area_of_land' 	=> $shg_area_of_land, 
									'land_in_irrigated' => $land_in_irrigated,
									'crop_policy' 		=> '', 
									'crop_store_in_godown'=> '',
									'godown_village_id' => '', 
									'godown_own_by' 	=> '',
									'select_item_from_godown'=> '',
									'other_any'			=> '',	
									'height_in_foot'	=> '',
									'width_in_foot'		=> '',
									'yearly_income'		=> $yearly_income,
									'source_of_income' 	=> '', 
									'shg_cast_type' 	=> $shg_cast_type,
									'is_working_category' => $is_working_category,
									'will_work_category' => $will_work_category,
									'status' 			=> 'Submitted',
									'created_by_id' 	=> $user_id,
									'created_on' 		=> date('Y-m-d H:i:s'));
																		
						$this->master_model->insertRecord('mv_personal_details',$dataArr);
						//echo ">>>".$this->db->last_query();die();
						$response['status']		= "1";
						//$response['message'] 	= "Farmer profile sucessfully created.";
						$response['message'] 	= "शेतकरी प्रोफाइल यशस्वीरित्या तयार केले.";
						$response['data']		= array("ref_id" => $ref_id);
					}
					else 
					{
						// Update Data																
						$dataArr = array('is_working_category' 	=> $is_working_category,
										'will_work_category' 	=> $will_work_category,
										'updated_by_id' 		=> $user_id, 
										'updated_on' 			=> date('Y-m-d H:i:s'));						
						
						
						$this->master_model->updateRecord('mv_personal_details',$dataArr,array('shg_id' => $shg_id));
						$response['status']		= "1";
						//$response['message'] 	= "Farmer profile sucessfully updated.";
						$response['message'] 	= "शेतकरी प्रोफाइल यशस्वीरित्या अद्यतनित केले.";
						$response['data']		= array("ref_id" => $ref_id);	
						//echo "=====".$this->db->last_query();die();
					}
				/*}
				else 
				{
					$response['status'] 	= "0";
					$response['message'] 	= "This Farmer profile registration already exist.";
					$response['data']		= array("ref_id" => $ref_id);							
					$ref_res = false;
				}*/
				
			}	// ELSE Part End
		}
		
		$this->response($response);
		
	}
	
	// Farmer/SHG Listing
	public function shg_listing()
	{
		//error_reporting(0);
		$response = array("status" => 0, "message" => "");
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			//$charSet = $this->db->query("set character_set_results='utf8'");	
			$this->db->select('ref_id, shg_id, added_date, taluka_id, cmrc_id, village_id, shg_name, shg_father_name, shg_last_name, mobile_no, shg_unique_id, emp_of_mahim, shg_marital_status, shg_disability, type_of_disability, shg_location_lat, shg_location_long, shg_area_of_land, land_in_irrigated, yearly_income, shg_cast_type, is_working_category, will_work_category, status');	
			$this->db->order_by("shg_id", "desc");
			$sqlList = $this->master_model->getRecords("mv_personal_details",array('is_deleted' => 0, 'created_by_id' => $user_id)); 
			//echo $this->db->last_query();die();
			if(count($sqlList) > 0)
			{
				foreach($sqlList as $row)
				{
					$this->db->select('village_id, village_code, village_name, village_name_marathi');
					$sqlVillage = $this->master_model->getRecords("mv_village_master",array('is_deleted' => 0, 'village_id' => $row['village_id'], 'status' => 'Active' ));
					//$v_name = @$sqlVillage[0]['village_name'];
					$v_name = @$sqlVillage[0]['village_name_marathi'];
					
					$this->db->select('taluka_id, district_id, taluka_name, taluka_name_marathi');
					$sqlTaluka = $this->master_model->getRecords("mv_taluka_master",array('is_deleted' => 0, 'taluka_id' => $row['taluka_id'], 'status' => 'Active' ));					
					//$taluka_name = isset($sqlTaluka[0]['taluka_name'])? $sqlTaluka[0]['taluka_name']:"";
					$taluka_name = isset($sqlTaluka[0]['taluka_name_marathi'])? $sqlTaluka[0]['taluka_name_marathi']:"";
					
					$this->db->select('cmrc_id, taluka_id, cmrc_code, cmrc_name_english, cmrc_name_marathi');
					$sqlCMRC = $this->master_model->getRecords("mv_cmrc_master",array('cmrc_id' => $row['cmrc_id']));
					$fullname_english = isset($sqlCMRC[0]['cmrc_name_english'])? $sqlCMRC[0]['cmrc_name_english']:"";
					$fullname_marathi = isset($sqlCMRC[0]['cmrc_name_marathi'])? $sqlCMRC[0]['cmrc_name_marathi']:"";
					
					
					
					$response_data[] = array('ref_id' 				=> $row['ref_id'],
											'shg_id' 				=> $row['shg_id'],
											'added_date' 			=> $row['added_date'],
											'taluka_id' 			=> $row['taluka_id'],
											'taluka_name' 			=> $taluka_name,
											'cmrc_id' 				=> $row['cmrc_id'],
											'cmrc_name' 			=> $fullname_marathi,
											'cmrc_name_marathi'		=> $fullname_marathi,	
											'village_id' 			=> $row['village_id'],
											'village_name'			=> @$v_name,		
											'shg_name' 				=> $row['shg_name'],
											'shg_father_name' 		=> $row['shg_father_name'],
											'shg_last_name' 		=> $row['shg_last_name'],
											'mobile_no' 			=> $row['mobile_no'],
											'shg_unique_id' 		=> $row['shg_unique_id'],	
											'emp_of_mahim' 			=> $row['emp_of_mahim'],
											'shg_marital_status' 	=> $row['shg_marital_status'],
											'shg_disability' 		=> $row['shg_disability'],
											'type_of_disability' 	=> $row['type_of_disability'],
											'shg_location_lat' 		=> $row['shg_location_lat'],
											'shg_location_long' 	=> $row['shg_location_long'],
											'shg_area_of_land' 		=> $row['shg_area_of_land'],
											'land_in_irrigated' 	=> $row['land_in_irrigated'],											
											'yearly_income' 		=> $row['yearly_income'],
											'shg_cast_type' 		=> $row['shg_cast_type'],
											'is_working_category' 	=> $row['is_working_category'],
											'will_work_category' 	=> $row['will_work_category'],
											'status' 				=> $row['status']);
				}
				
				$response['status']		= "1";
				//$response['message'] 	= "Farmer listing received successfully.";
				$response['message'] 	= "शेतकरी यादी यशस्वीरित्या प्राप्त झाली.";
				$response['data'] 		= $response_data;
			}
			else 
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "No data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}
		}
		
		$this->response($response);
	}
	
	// function to Get Survey List
	public function get_survey_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
				
		if(!empty($user_id) && is_user_login($user_id))
		{			
							
				$query = $this->db->query("SELECT category_id, category_name_marathi, category_name_english FROM mv_category_master 
				WHERE mv_category_master.is_deleted = '0' AND mv_category_master.status = 'Active' ORDER BY mv_category_master.category_id ASC");
				
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					
					foreach($query->result_array() as $row)
					{
						$data[] = array('category_id' => $row['category_id'],  'category_name_marathi' => $row['category_name_marathi'],  'category_name_english' => $row['category_name_marathi']);
					}
					
					$response['status'] 	= "1";
					//$response['message'] 	= "Survey List Get Successfully.";
					$response['message'] 	= "श्रेणी यादी यशस्वीरित्या प्राप्त झाली";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
		}
		else
		{
			$response['status']		= "0";
			//$response['message']    = "You must be logged in to perform this request.";
			$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
		}
		
		$this->response($response);
	}

	public function get_wearhouse_type_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
				
		if(!empty($user_id) && is_user_login($user_id))
		{			
							
				$query = $this->db->query("SELECT wearhouse_type_id, type_name_english, type_name_marathi FROM mv_wearhouse_type 
				WHERE mv_wearhouse_type.is_deleted = '0' AND mv_wearhouse_type.status = 'Active' ORDER BY mv_wearhouse_type.wearhouse_type_id ASC");
				
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					
					foreach($query->result_array() as $row)
					{
						$data[] = array('wearhouse_type_id' => $row['wearhouse_type_id'], 'type_name_english' => $row['type_name_marathi'],  'type_name_marathi' => $row['type_name_marathi']);
					}
					
					$response['status'] 	= "1";
					//$response['message'] 	= "Survey List Get Successfully.";
					$response['message'] 	= "कोठार प्रकार यादी यशस्वीरित्या प्राप्त झाली";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
		}
		else
		{
			$response['status']		= "0";
			//$response['message']    = "You must be logged in to perform this request.";
			$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
		}
		
		$this->response($response);
	}

	// Wear house Tupe Dump
	public function get_wearhouse_type()
	{
		$response = array("status" => 0, "message" => "");	
							
		$query = $this->db->query("SELECT wearhouse_type_id, type_name_english, type_name_marathi FROM mv_wearhouse_type 
		WHERE mv_wearhouse_type.is_deleted = '0' AND mv_wearhouse_type.status = 'Active' ORDER BY mv_wearhouse_type.wearhouse_type_id ASC");
		
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			
			foreach($query->result_array() as $row)
			{
				$data[] = array('wearhouse_type_id' => $row['wearhouse_type_id'], 'type_name_english' => $row['type_name_marathi'],  'type_name_marathi' => $row['type_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Survey List Get Successfully.";
			$response['message'] 	= "कोठार प्रकार यादी यशस्वीरित्या प्राप्त झाली";
			$response['data'] 		= $data;
		}
		else
		{
			$response['status'] 	= "1";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	public function get_wearhouse_category_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
				
		if(!empty($user_id) && is_user_login($user_id))
		{			
							
				$query = $this->db->query("SELECT wearhouse_cat_id, category_name_english, category_name_marathi FROM mv_wearhouse_category 
				WHERE mv_wearhouse_category.is_deleted = '0' AND mv_wearhouse_category.status = 'Active' ORDER BY mv_wearhouse_category.wearhouse_cat_id ASC");
				
				$cnt = $query->num_rows();
				
				if($cnt > 0)
				{
					
					foreach($query->result_array() as $row)
					{
						$data[] = array('wearhouse_cat_id' => $row['wearhouse_cat_id'],  'category_name_english' => $row['category_name_marathi'],  'category_name_marathi' => $row['category_name_marathi']);
					}
					
					$response['status'] 	= "1";
					//$response['message'] 	= "Survey List Get Successfully.";
					$response['message'] 	= "कोठार श्रेणी यादी यशस्वीरित्या प्राप्त झाली";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
		}
		else
		{
			$response['status']		= "0";
			//$response['message']    = "You must be logged in to perform this request.";
			$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
		}
		
		$this->response($response);
	}

	// Wearhouse category Dump
	public function get_wearhouse_category()
	{
		$response = array("status" => 0, "message" => "");
		
		$query = $this->db->query("SELECT wearhouse_cat_id, category_name_english, category_name_marathi FROM mv_wearhouse_category 
		WHERE mv_wearhouse_category.is_deleted = '0' AND mv_wearhouse_category.status = 'Active' ORDER BY mv_wearhouse_category.wearhouse_cat_id ASC");
		
		$cnt = $query->num_rows();
		
		if($cnt > 0)
		{
			
			foreach($query->result_array() as $row)
			{
				$data[] = array('wearhouse_cat_id' => $row['wearhouse_cat_id'],  'category_name_english' => $row['category_name_marathi'],  'category_name_marathi' => $row['category_name_marathi']);
			}
			
			$response['status'] 	= "1";
			//$response['message'] 	= "Survey List Get Successfully.";
			$response['message'] 	= "कोठार श्रेणी यादी यशस्वीरित्या प्राप्त झाली";
			$response['data'] 		= $data;
		}
		else
		{
			$response['status'] 	= "1";
			//$response['message'] 	= "No data Found.";
			$response['message'] 	= "माहिती आढळली नाही.";
		}
		
		$this->response($response);
	}

	
	// selected Category Listing
	public function get_category_survey_listing()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
				
		if(!empty($user_id) && is_user_login($user_id))
		{			
			
			$shg_id	= isset($_POST['shg_id']) ? $_POST['shg_id'] : '';
			
			if(empty($shg_id))
			{
				$response['status'] 	= "0";
				//$response['message'] 	= "Survey ID can not be blank.";
				$response['message'] 	= "शेतकरी आयडी रिक्त असू शकत नाही.";
			}
			else 
			{
				$this->db->select('is_working_category');
				$selectCat = $this->master_model->getRecords("mv_personal_details", array("shg_id" => $shg_id));
				$listSurvey = @$selectCat[0]['is_working_category'];
				
				if($listSurvey!="")
				{
					$catid = explode(",", $listSurvey);
					
					if(count($catid) > 0)
					{
						$arrayPut = array();
						foreach($catid as $catname) 
						{
							$this->db->like('category_name_marathi', $catname);
							$selectCatname = $this->master_model->getRecords("mv_category_master", array("is_deleted" => 0));
							$categoryId = @$selectCatname[0]['category_id'];
							$categorynameenglish = @$selectCatname[0]['category_desc_english'];
							//array_push($arrayPut, $categoryId);
							
							$data[] = array('category_id' => $categoryId,  'category_name_marathi' => $catname,  'category_name_english' => $catname);
						}
						
						$response['status'] 	= "1";
						//$response['restricted_draft_count'] 	= $this->config->item('RESTRICT_DRAFT_COUNT');
						//$response['message'] 	= "Survey List Get Successfully.";
						$response['message'] 	= "सर्वेक्षण यादी यशस्वीरित्या मिळवा.";
						$response['data'] 		= $data;
					}
				}					
			}			
		}
		else
		{
			$response['status']		= "0";
			//$response['message']    = "You must be logged in to perform this request.";
			$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
		}
		
		$this->response($response);
	}
	
	// Published Survey Section List
	public function publisedSection()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();

		if(!empty($user_id) && is_user_login($user_id))
		{
			
			$survey_id			= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
			
			if(empty($survey_id))
			{
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Survey ID can not be blank.";
				$response['message'] 	= "सर्वेक्षण आयडी रिक्त असू शकत नाही";
			}
			else 
			{
				$surveySectionList = $this->db->query("SELECT DISTINCT `mv_section_questions`.`section_id`, `mv_section_master`.`section_name_marathi`, `mv_section_master`.`section_name_english`, `mv_section_master`.`section_desc_english`, `mv_section_master`.`section_desc_marathi`, `mv_section_master`.`section_img`  FROM `mv_section_questions` JOIN `mv_section_master` ON `mv_section_master`.`section_id` = `mv_section_questions`.`section_id` WHERE `mv_section_questions`.`survey_id` = '".$survey_id."' AND `mv_section_questions`.`is_deleted` = 0 AND `mv_section_master`.`is_deleted` = 0 ORDER BY `mv_section_questions`.`sort_order` IS NOT NULL DESC, `mv_section_questions`.`sort_order` ASC");					
				//echo $this->db->last_query();
				$surveyCount = $surveySectionList->num_rows();
				
				if($surveyCount > 0)
				{		
					foreach($surveySectionList->result_array()  as $res) 
					{
						$path = @define('SECTION_DIR', 'uploads/Section/thumb/');
						$img_url = base_url().SECTION_DIR.$res['section_img'];
						$data[] = array("section_id" => $res['section_id'], "section_name_marathi" => $res['section_name_marathi'], "section_name_english" => $res['section_name_english'],"section_desc_english" => $res['section_desc_english'], "section_desc_marathi" => $res['section_desc_marathi'], "section_img" => $img_url);
					}

					//print_r($arrayList);
					$response['status'] 	= "1";					
					//$response['message'] 	= "Section List Get Successfully.";
					$response['message'] 	= "सर्वेक्षण यादी यशस्वीरित्या मिळवा.";
					$response['data'] 		= $data;
				}
				else
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
			}			
		}
		else
		{
			$response['status']		= "0";
			//$response['message']    = "You must be logged in to perform this request.";
			$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
		}
		
		$this->response($response);
	}
	
	// Validation Response Type 
	public function validation_response_type($id)
	{
		$responseValiation = $this->db->query("SELECT validation_type FROM mv_response_validation_master WHERE validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$responseData = $responseValiation->row();	
		return $responseData->validation_type;
	}
	
	// Validation Sub Response Type Function
	public function validation_sub_response_type($id)
	{
		$responseSubValiation = $this->db->query("SELECT validation_sb_type FROM mv_response_validation_subtype_master WHERE sub_validation_id = '".$id."' AND is_deleted='0'");
		//$option_count_1 = $responseValiation->num_rows();
		$subresponseData = $responseSubValiation->row();	
		return $subresponseData->validation_sb_type;
	}
	
	// Survey Section List
	public function getSurveySectionList($survey_id)
	{
		$result = array();
		
		$surveySectionList = $this->db->query("SELECT DISTINCT `mv_section_questions`.`section_id`, `mv_section_master`.`section_name_marathi`, `mv_section_master`.`section_name_english`, `mv_section_master`.`section_desc_english`, `mv_section_master`.`section_desc_marathi`, `mv_section_master`.`section_img`  FROM `mv_section_questions` JOIN `mv_section_master` ON `mv_section_master`.`section_id` = `mv_section_questions`.`section_id` WHERE `mv_section_questions`.`survey_id` = '".$survey_id."' AND `mv_section_questions`.`is_deleted` = 0 AND `mv_section_master`.`is_deleted` = 0 ORDER BY `mv_section_questions`.`sort_order` IS NOT NULL DESC, `mv_section_master`.`section_id` ASC");					
		//echo $this->db->last_query();
		$surveyCount = $surveySectionList->num_rows();
		
		if($surveyCount > 0)
		{		
			foreach($surveySectionList->result_array()  as $res) 
			{
				$path = @define('SECTION_DIR', 'uploads/Section/thumb/');
				$img_url = base_url().SECTION_DIR.$res['section_img'];
				$result[] = array("section_id" => $res['section_id'], "section_name_marathi" => $res['section_name_marathi'], "section_name_english" => $res['section_name_english'],"section_desc_english" => $res['section_desc_english'], "section_desc_marathi" => $res['section_desc_marathi'], "section_img" => $img_url);
			}
		}
		
		//print_r($result);
		return $result;
				
	}	
	
	// Survey Question Listing 18June Only Survey ID Parameter With Active Survey Form Again changed as per 13thJuly Meeting with Teams
	public function survey_questionire()
	{
		error_reporting(0);
		// Response
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			// Get Survey ID
			$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
			
			if(empty($survey_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Survey ID is required.";
				$response['message'] 	= "सर्वेक्षण आयडी आवश्यक आहे.";
				
			} 
			else 
			{	
		
				// Get Survey Details 
				$response['status'] 		= "1";
				//$response['message'] 		= "Survey questionire get successfully.";
				$response['message'] 		= "सर्वेक्षण प्रश्नपत्रिका यशस्वीरित्या मिळवा.";
				
				$surveyData = $this->db->query("SELECT * FROM mv_survey_master WHERE survey_id = '".$survey_id."' AND status = 'Publish' AND is_deleted = '0' LIMIT 1 ");
				$surveyResult = $surveyData->row();
				$recordCount = 	$surveyData->num_rows();
				//echo $this->db->last_query();
				//echo "<pre>";print_r($surveyResult);die();
				
				if($recordCount > 0)
				{
					$response['survey_id'] 			= $surveyResult->survey_id;
					$response['survey_title'] 		= $surveyResult->survey_title_marathi; 
					$response['survey_description'] = $surveyResult->survey_description_marathi;
					
					//$response['form_id'] 			= $form_id;
					//$response['season_id'] 			= $season_id;
					
					// Ask Section Question Table For Section & Sub Section			
					$surveyQuestionList = $this->db->query("SELECT * FROM mv_section_questions WHERE survey_id = '".$survey_id."' AND status = 'Active' AND is_deleted = '0' ORDER BY sort_order IS NOT NULL DESC, sort_order ASC");
					//echo $this->db->last_query();
					$sectionQuesitonCount = $surveyQuestionList->num_rows();
					$number_of_row = $this->db->affected_rows();
					if($number_of_row > 0)
					{				
						$section_question = array();
						
						$surveySections = $this->getSurveySectionList($survey_id);
						
						//echo ">>><pre>";print_r($surveySections);
						foreach($surveyQuestionList->result_array() as $res)
						{	
							$querySection = $this->db->query("SELECT section_id, parent_section_id, section_name_english, section_desc_english, section_name_marathi, section_desc_marathi   FROM mv_section_master WHERE section_id = '".$res['section_id']."' AND parent_section_id = 0 AND is_deleted = '0' AND status = 'Active'");
							$sectionData = $querySection->row();	
							//echo $this->db->last_query();
							$sectionArr = array();
							
							if($sectionData->section_id!="") 
							{
								$section_id 			= $sectionData->section_id;	
								$section_name 			= $sectionData->section_name_marathi; 
								$section_description 	= $sectionData->section_desc_marathi;
								
								$section_name_eng 			= $sectionData->section_name_english; 
								$section_description_eng 	= $sectionData->section_desc_english;
							}

							$sub_section_id 			= '';	
							$sub_section_name 			= ''; 
							$sub_section_description 	= '';
							if($res['sub_section_id'] > 0)
							{							
								$querySubSection = $this->db->query("SELECT section_id, parent_section_id, section_name_english, section_desc_english, section_name_marathi, section_desc_marathi FROM mv_section_master WHERE section_id='".$res['sub_section_id']."' AND is_deleted = '0' AND status = 'Active'");
								//echo $this->db->last_query();
								$subSectionData = $querySubSection->row();	
								$sub_section_id 			= @$subSectionData->section_id;	
								$sub_section_name 			= @$subSectionData->section_name_marathi; 
								$sub_section_description 	= @$subSectionData->section_desc_marathi;
								
								$sub_section_name_eng 			= @$subSectionData->section_name_english; 
								$sub_section_description_eng 	= @$subSectionData->section_desc_english;
							}
							
							$if_checked_parent = $this->db->query("SELECT * FROM mv_option_dependent WHERE dependant_ques_id = '".$res['question_id']."' AND is_deleted='0'");
							$if_parents_count = $if_checked_parent->num_rows();
							
							if($if_parents_count == 0)
							{
								$in_arr = array();
								// Global Question 
								/*$global_question_id 		= $res['global_question_id'];
								if($global_question_id > 0)
								{							
									$in_arr['global_question'] = $this->is_global_question($res['question_id'], $global_question_id);
								}*/
								
								$queryQuestion = $this->db->query("SELECT * FROM mv_question_master WHERE question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");								
								$questionData = $queryQuestion->row();	
								$explodemarathi =  array();
								if(@$questionData->salutation_marathi!="")
								{
									$explodemarathi = explode("|", @$questionData->salutation_marathi);
								} 
								
								$explodeEng =  array();
								if(@$questionData->salutation_english!="")
								{
									$explodeEng = explode("|", @$questionData->salutation_english);
								}

									

								$in_arr['section_id'] 			= $section_id;
								$in_arr['section_name'] 		= $section_name;
								$in_arr['section_desc'] 		= $section_description;
								
								$in_arr['sub_section_id'] 		= $sub_section_id;
								$in_arr['sub_section_name'] 	= $sub_section_name;
								$in_arr['sub_section_des'] 		= $sub_section_description;
								
								//$in_arr['form_id'] 				= @$res['form_id'];
								//$in_arr['season_id'] 			= $season_id;
								$in_arr['question_id'] 			= @$questionData->question_id;
								$in_arr['parent_question_id'] 	= @$questionData->parent_question_id;
								$in_arr['sort_order'] 			= @$res['sort_order'];
								$in_arr['question_label_marathi'] 	= @$questionData->question_text_marathi;
								$in_arr['question_label_english'] 	= @$questionData->question_text_english;
								$in_arr['response_type'] 		= $questionData->response_type_id;
								$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
								$in_arr['salutation_english'] 	= json_encode($explodeEng);
								$in_arr['salutation_marathi'] 	= json_encode($explodemarathi);
								$in_arr['question_help_marathi'] 	= $questionData->help_text_marathi;
								$in_arr['question_help_english'] 	= $questionData->help_text_english;
								$in_arr['if_summation']			= 'No';
								
								// Question Form Checkbox Upload File Required
								$in_arr['if_file_required']		= 'No';
								$in_arr['upload_file_required']	= 'No';
								$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
								if($questionData->input_file_required == 'Yes')
								{
									$in_arr['upload_file_required'] 	= $questionData->input_file_required;
								}
								
								if($questionData->if_file_required == 'Yes')
								{
									$in_arr['if_file_required'] 	= $questionData->if_file_required;
									//$in_arr['file_label'] 			= $questionData->file_label;
									$in_arr['file_label_marathi'] 	= $questionData->file_label_marathi;
									$in_arr['file_label_english'] 	= $questionData->file_label_english;
								}
								
								if($questionData->if_summation == 'Yes')
								{
									$in_arr['if_summation'] 		= $questionData->if_summation;
								}													
								
								// Get Sub Question List On Main Question 								
								$subQuestion = $this->db->query("SELECT * FROM mv_question_master WHERE parent_question_id = '".$res['question_id']."' AND is_deleted = '0' AND status = 'Active'");
								$sub_questuon_count = $subQuestion->num_rows();
								//$sub_arr = array();
								if($sub_questuon_count > 0)
								{	
									$sub_arr = array();
									foreach($subQuestion->result_array() as $opt)
									{
										$$tmp_sub_arr = array();
										$tmp_sub_arr['question_id'] 		= $opt['question_id'];
										
										$explodeMarathi =  array();
										if($opt['salutation_marathi']!="")
										{
											$explodeMarathi = explode("|", $opt['salutation_marathi']);

										} 
										else 
										{
											$blank = array();
											$explodeMarathi = $blank;
										}
										
										$explodeEnglish =  array();
										if($opt['salutation_english']!="")
										{
											$explodeEnglish = explode("|", $opt['salutation_english']);

										} 
										else 
										{
											$blankA = array();
											$explodeEnglish = $blankA;
										}
										
										$tmp_sub_arr['section_id'] 			= $section_id;
										$tmp_sub_arr['section_name'] 		= $section_name;
										$tmp_sub_arr['section_desc'] 		= $section_description;
										
										$tmp_sub_arr['sub_section_id'] 		= $sub_section_id;
										$tmp_sub_arr['sub_section_name'] 	= $sub_section_name;
										$tmp_sub_arr['sub_section_des'] 	= $sub_section_description;
										
										//$tmp_sub_arr['form_id'] 			= $res['form_id'];
										//$tmp_sub_arr['season_id'] 			= $season_id;
										$tmp_sub_arr['question_id'] 		= $opt['question_id'];
										$tmp_sub_arr['question_label_marathi'] 	= $opt['question_text_marathi'];
										$tmp_sub_arr['question_label_english'] 	= $opt['question_text_english'];
										$tmp_sub_arr['response_type'] 		= $opt['response_type_id'];
										$tmp_sub_arr['is_mandatory'] 		= $opt['is_mandatory'];
										$tmp_sub_arr['parent_question_id'] 	= $opt['parent_question_id'];
										$tmp_sub_arr['question_help_english'] = $opt['help_text_english'];
										$tmp_sub_arr['question_help_marathi'] = $opt['help_text_marathi'];
										$tmp_sub_arr['salutation_marathi'] 	= json_encode($explodeMarathi);
										$tmp_sub_arr['salutation_english'] 	= json_encode($explodeEnglish);
										$tmp_sub_arr['input_more_than_1'] 	= $opt['input_more_than_1'];
										$tmp_sub_arr['upload_file_required']	= 'No';
										$tmp_sub_arr['if_summation']			= 'No';	
										$tmp_sub_arr['if_file_required']		= 'No';	
																			
										// To handle image for sub question, by Bhagwan, on 02-04-2021
										if($opt['input_file_required'] == 'Yes')
										{
											$tmp_sub_arr['upload_file_required'] 	= $opt['input_file_required'];
										}
										
										if($opt['if_file_required'] == 'Yes')
										{
											$tmp_sub_arr['if_file_required'] 	= $opt['if_file_required'];
											$tmp_sub_arr['file_label_marathi'] 	= $opt['file_label_marathi'];
											$tmp_sub_arr['file_label_english'] 	= $opt['file_label_english'];
										}
										
										if($opt['if_summation'] == 'Yes')
										{
											$tmp_sub_arr['if_summation'] 		= $opt['if_summation'];
										}
										// EOF To handle image for sub question, by Bhagwan, on 02-04-2021

										$quetionOptions = $this->db->query("SELECT * FROM mv_questions_options_master WHERE question_id = '".$opt['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
										$option_count = $quetionOptions->num_rows();	
										if($option_count > 0)
										{
											$optArr = array();										
											foreach($quetionOptions->result_array() as $optSub)
											{
												if($optSub['option_marathi']!="")
												{	
													array_push($optArr, $optSub['option_marathi']);	
												}
														
												$queryMultipleCall = $this->db->query("SELECT * FROM mv_option_dependent WHERE question_id = '".$optSub['question_id']."' AND ques_option_id = '".$optSub['ques_option_id']."' AND is_deleted='0'");
												$multi_option_count = $queryMultipleCall->num_rows();
												if($multi_option_count > 0)
												{ 
													foreach($queryMultipleCall->result_array() as $dpenSub)
													{
														$tmp_sub_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], $optSub['option_marathi']);	
													}
												}
												
												if($optSub['validation_id'] > 0)
												{
													$tmp_sub_arr['validation_type'] 	= $this->validation_response_type($optSub['validation_id']);
													
													if($optSub['sub_validation_id'] > 0)
													{
														$tmp_sub_arr['validation_sub_type'] = $this->validation_sub_response_type($optSub['sub_validation_id']);
													}
												}
												else 
												{
													$tmp_sub_arr['validation_type'] = '';
													$tmp_sub_arr['validation_sub_type'] = '';
												}	
										
												$tmp_sub_arr['min_value'] 		= $optSub['min_value'];
												$tmp_sub_arr['max_value'] 		= $optSub['max_value'];
												$tmp_sub_arr['validation_label']	= $optSub['validation_label'];	
											}						

											if(count($optArr) > 0)
											{
												//$tmp_sub_arr['options'] = json_encode($optArr, JSON_UNESCAPED_UNICODE);
												$tmp_sub_arr['options'] = $optArr;
											}
											else
											{
												$blank = array();
												//$in_arr['options'] = json_encode($blank);
												$in_arr['options'] = $blank;
											}	

											$sub_arr[] = $tmp_sub_arr;
										}

										
											
									}	// Sub Array End
									
									//$in_arr['sub_question'] = $sub_arr;							
									$in_arr['sub_question'][] = $sub_arr;
									
								} // End Sub Question If
								
								
								// Get Parent Question Option 
								$quetionOptions_1 = $this->db->query("SELECT * FROM mv_questions_options_master WHERE question_id = '".$res['question_id']."' AND is_deleted='0' ORDER BY display_order ASC");
								$option_count_1 = $quetionOptions_1->num_rows();	
								
								if($option_count_1 > 0)
								{
									$optArr_1 = array();
									foreach($quetionOptions_1->result_array() as $opt_1)
									{
										if(@$opt_1['option_marathi']!="")
										{
											array_push($optArr_1, @$opt_1['option_marathi']);
										}
																			
										// Multiple Question Loop
										$queryMultipleCall = $this->db->query("SELECT * FROM mv_option_dependent WHERE question_id = '".$opt_1['question_id']."' AND ques_option_id = '".$opt_1['ques_option_id']."' AND is_deleted='0'");
										$multi_option_count = $queryMultipleCall->num_rows();
										if($multi_option_count > 0)
										{ 
											foreach($queryMultipleCall->result_array() as $dpenSub)
											{
												$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($dpenSub['question_id'], $dpenSub['dependant_ques_id'], $dpenSub['ques_option_id'], @$opt_1['option_marathi']);	
											}
										}
								
										if($opt_1['validation_id'] > 0)
										{
											$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
											
											if($opt_1['sub_validation_id'] > 0)
											{
												$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
											}
										}
										else 
										{
											$in_arr['validation_type'] = '';
											$in_arr['validation_sub_type'] = '';
										}	
										
										$in_arr['min_value'] 		= $opt_1['min_value'];
										$in_arr['max_value'] 		= $opt_1['max_value'];
										$in_arr['validation_label']	= $opt_1['validation_label'];
									}										
									
									if(count($optArr_1) > 0)
									{
										//$in_arr['options'] = json_encode($optArr_1, JSON_UNESCAPED_UNICODE);
										$in_arr['options'] = $optArr_1;
									}
									else
									{
										$blank = array();
										//$in_arr['options'] = json_encode($blank);
										$in_arr['options'] = $blank;
									}
									
								} // End Option If End							
								
								$section_question[]	= $in_arr;							
								
							} // Check End Related Option Dependent Has Parent				
							
						} // End Foreach Related Section Question
						
						
						$sectionArr['section_list']		= $surveySections;
						$sectionArr['section_question']	= $section_question;
						
					}			
					else 
					{
						$response['status'] 	= "0";
						//$response['message'] 	= "Survey questionire not found.";
						$response['message'] 	= "सर्वेक्षण प्रश्न सापडला नाही.";
						
					}						
					
					$response['data']['sections'][]	= $sectionArr;
					
				}
				else 
				{
					$response['status'] 	= "0";
					//$response['message'] 	= "Survey questionire not found.";
					$response['message'] 	= "सर्वेक्षण आढळले नाही.";
				}								
			}
		}
		
		$this->response($response);
	}
	
	// Recursive Function
	public function getOptionbaseQuestionMultiple($question_id, $dependant_ques_id, $id, $option)
	{ 
		//ob_start();
		$queryQuestion = $this->db->query("SELECT question_id,question_text_marathi,question_text_english,response_type_id,is_mandatory,parent_question_id,salutation_marathi,salutation_english,input_more_than_1,if_file_required,file_label_marathi,file_label_english,input_file_required,if_summation,help_text_marathi,help_text_english FROM mv_question_master WHERE question_id = '".$dependant_ques_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			$in_arr = array();
			
			$explodeMarathi =  array();
			if($questionData->salutation_marathi!="")
			{
				$explodeMarathi = explode("|", $questionData->salutation_marathi);
			} 
			
			$explodeEnglish =  array();
			if($questionData->salutation_english!="")
			{
				$explodeEnglish = explode("|", $questionData->salutation_english);
			} 
			
			 // get the first row
			//$in_arr['form_id'] 				= $form_id;  
			//$in_arr['season_id'] 			= $season_id; 
			$in_arr['question_id'] 			= $questionData->question_id; 
			$in_arr['question_label_marathi'] 	= $questionData->question_text_marathi;
			$in_arr['question_label_english'] 	= $questionData->question_text_english;
			$in_arr['response_type'] 		= $questionData->response_type_id;
			$in_arr['is_mandatory'] 		= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 	= $questionData->parent_question_id;
			//$in_arr['salutation'] 			= $questionData->salutation;
			$in_arr['question_help_english'] 	= $questionData->help_text_english;
			$in_arr['question_help_marathi'] 	= $questionData->help_text_marathi;
			$in_arr['salutation_marathi'] 	= json_encode($explodeMarathi);
			$in_arr['salutation_english'] 	= json_encode($explodeEnglish);
			$in_arr['input_more_than_1'] 	= $questionData->input_more_than_1;
			$in_arr['if_file_required']		= 'No';
			$in_arr['upload_file_required']	= 'No';
			$in_arr['if_summation']			= 'No';
			
			if($questionData->if_file_required == 'Yes')
			{
				$in_arr['if_file_required'] 	= $questionData->if_file_required;
				$in_arr['file_label_marathi'] 	= $questionData->file_label_marathi;
				$in_arr['file_label_english'] 	= $questionData->file_label_english;
				
			}
			
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}	
			
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT * FROM mv_questions_options_master WHERE question_id = '".$questionData->question_id."' AND is_deleted='0' ORDER BY display_order ASC");
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option_marathi']!="")
					{
						array_push($optArr_1, $opt_1['option_marathi']);
					}
					
					/********* Multiple Option Dependent Question Data Start ************/
				
						$quetionOptionMultipleQuestion = $this->db->query("SELECT * FROM mv_option_dependent WHERE question_id = '".$questionData->question_id."' AND ques_option_id='".$opt_1['ques_option_id']."' AND is_deleted='0'");
						$multipleQuestionCnt = $quetionOptionMultipleQuestion->num_rows();
						if($multipleQuestionCnt > 0)
						{
							foreach($quetionOptionMultipleQuestion->result_array() as $multipleQues)
							{
								$queryMultipleQuestion = $this->db->query("SELECT * FROM mv_questions_options_master WHERE ques_option_id = '".$multipleQues['ques_option_id']."' AND is_deleted = '0' ORDER BY display_order ASC");
								$questionMultipleData = $queryMultipleQuestion->row();
								$getOption = $questionMultipleData->option_marathi;
								
								// Below Code for multiple level array questions
								$in_arr['dependent_question_options'][] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);							
								//$in_arr['dependent_question_options'] = $this->getOptionbaseQuestionMultiple($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption);
							}
							
						}
				
					/*****Multiple Option Dependent Question Data End ************/
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}
					
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}

				if(count($optArr_1) > 0)
				{
					//$in_arr['options'] = json_encode($optArr_1, JSON_UNESCAPED_UNICODE);
					$in_arr['options'] = $optArr_1;
				}
				else
				{
					$blank = array();
					//$in_arr['options'] = json_encode($blank);
					$in_arr['options'] = $blank;
				}	
				//$in_arr['options'] = json_encode($optArr_1);					
			}
			
		}
		
		$dataArr = array("option" => $option, "option_dependent_question" => $in_arr); 		
		//return json_encode($dataArr);
		return $dataArr;
	} // Function End
	
	//Save Response
	public function save_response_dynamic()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		// Post DATA
		$surveyData = $_POST;
		//echo "<pre>";print_r($surveyData);die();
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 			
		{
			if(count($surveyData['data']) > 0)
			{
				$survey_id 			= $surveyData['data']['survey_id'];
				//$form_id 			= $surveyData['data']['form_id'];
				$response_id 		= $surveyData['data']['responses'][0]['response_id'];
				$ref_id 			= $surveyData['data']['responses'][0]['ref_id'];
				$submitted_date 	= $surveyData['data']['responses'][0]['submitted_date'];
				$submited_time 		= $surveyData['data']['responses'][0]['submited_time'];
				//$district_id 		= $surveyData['data']['responses'][0]['district_id'];
				$district_id 		= isset($surveyData['data']['responses'][0]['district_id'])? $surveyData['data']['responses'][0]['district_id']:"0";
				$shg_id 			= $surveyData['data']['shg_id'];
				$header_latitude 	= isset($surveyData['data']['responses'][0]['latitude'])? $surveyData['data']['responses'][0]['latitude']:"";		
				$header_longitude 	= isset($surveyData['data']['responses'][0]['longitude'])? $surveyData['data']['responses'][0]['longitude']:"";
				//$taluka_id 			= $surveyData['data']['responses'][0]['taluka_id'];	
				$taluka_id 		= isset($surveyData['data']['responses'][0]['taluka_id'])? $surveyData['data']['responses'][0]['taluka_id']:"0";
				//$village_id 		= $surveyData['data']['responses'][0]['village_id'];
				$village_id 		= isset($surveyData['data']['responses'][0]['village_id'])? $surveyData['data']['responses'][0]['village_id']:"0";
				$survey_status 		= $surveyData['data']['responses'][0]['status'];
				$submitDate = date('Y-m-d', strtotime($submitted_date));
				
				$start_date_time    = date("Y-m-d H:i:s");
				$end_date_time      = date("Y-m-d H:i:s");	
				if($surveyData['data']['responses'][0]['start_date_time']!="")
				{
					$start_date_time	= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['start_date_time']));
				}
				
				if($surveyData['data']['responses'][0]['end_date_time']!="")
				{
					$end_date_time 		= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['end_date_time']));
				}
				
				// Check ID Exist related response id 
				$query = $this->db->query("SELECT survey_id FROM mv_survey_master WHERE survey_id = '".$survey_id."' AND is_deleted = '0' AND status != 'Draft'");
				
				$surveyCnt = $query->num_rows();
				
				if($surveyCnt > 0) 
				{	
					$query = $this->db->query("SELECT * FROM mv_response_headers WHERE response_id = '".$response_id."' AND is_deleted = '0'");
					$cnt = $query->num_rows();
					$ref_res = true;
					if($cnt == '0') 
					{		
						// Check Ref ID Exist related response id 
						$queryReference = $this->db->query("SELECT * FROM mv_response_headers WHERE ref_id = '".$ref_id."' AND is_deleted = '0'");
						$cntRef = $queryReference->num_rows();
						
						if($cntRef == '0')
						{
							$insertArr = array( "ref_id" 			=> $ref_id, 
												"survey_id" 		=> $survey_id,
												"surveyer_id" 		=> $user_id,
												"shg_id" 			=> $shg_id,
												"district_id" 		=> $district_id,
												"taluka_id" 		=> $taluka_id,
												"village_id" 		=> $village_id,
												"submitted_date" 	=> $submitDate,
												"submited_time" 	=> $submited_time,
												"start_date_time" 	=> $start_date_time,
												"end_date_time" 	=> $end_date_time,
												"latitude" 			=> $header_latitude,
												"longitude" 		=> $header_longitude,
												"status" 			=> $survey_status,
												"created_by_id" 	=> $user_id	
										);
							$surveyQuery = $this->master_model->insertRecord('mv_response_headers',$insertArr);
							$cid = $this->db->insert_id();
							$updateFlag = false;
							//$ref_res = true;
						}
						else 
						{
							//echo "No Sections Found";
							$response['status'] 	= "0";
							//$response['message'] 	= "This survey form already exist.";
							$response['message'] 	= "हा सर्वेक्षण फॉर्म आधीपासून विद्यमान आहे.";
							$response['data']		= array("ref_id" => $ref_id);							
							$ref_res = false;
						}	
					}
					else 
					{
						// Update Record
						$updateArr = array( 
											"status" 		=> $survey_status,
											"updated_on" 	=> date("Y-m-d H:i:s"),
											"updated_by_id" => $user_id		
											);
						$updateQuery = $this->master_model->updateRecord('mv_response_headers',$updateArr,array('survey_id' => $survey_id, 'surveyer_id' => $user_id, "response_id" => $response_id, "form_id"	=> $form_id));					
						$cid = $response_id;
						$updateFlag = true;	
					}
					
					// Actual Response Store Code Start If Ref Id Condition Is True
					if($ref_res)
					{			
						
						if(count($surveyData['data']['responses'][0]['sections']) > 0)
						{
							// Section List Found
							foreach($surveyData['data']['responses'][0]['sections'][0]['section_list'] as $key => $sections)
							{
								$section_id 	= $sections['section_id'];
								$section_name 	= $sections['section_name'];
								$section_desc 	= isset($sections['section_description'])? $sections['section_description']:"";
							}							
							
							// Section Question Found
							
							
							if(count($surveyData['data']['responses'][0]['sections'][0]['section_question']) > 0)
							{
								
								foreach($surveyData['data']['responses'][0]['sections'][0]['section_question'] as $res_val => $return_res)
								{
									// Get Values From Response
									$question_id 		= $return_res['question_id'];
									//$parent_q_id	 	= $return_res['parent_question_id'];
									$parent_q_id	 	= (@$return_res['parent_question_id'])? @$return_res['parent_question_id']:"0";
									$response_type	 	= $return_res['response_type'];
									//$options			= $return_res['options'];
									$inputMoreThanOne 	= $return_res['input_more_than_1'];
									$upload_file_req 	= $return_res['upload_file_required'];
									$if_file_required   = $return_res['if_file_required'];
									$answer 			= (@$return_res['answer'])? @$return_res['answer']:"";
									$latitude 	 		= isset($return_res['latitude'])? $return_res['latitude']:"";
									$longitude 	  = isset($return_res['longitude'])? $return_res['longitude']:"";
									// Sub Question Node
									//$sub_question 		= @$return_res['sub_question'][0];
									$sub_question 		= @$return_res['sub_question'];
									// Dependent Question On Option Related Node
									$dependent_question = @$return_res['dependent_question_options'];									
									// Main Question Anser Related Functionality Start
									if(is_array($answer))
									{
										if(count($answer) > 0) 
										{
											
											$requiredArr = array('survey_id' 		=> $survey_id, 
																'question_id' 		=> $question_id, 
																'parent_question_id'=> 0, 
																'response_type' 	=> $response_type, 
																'upload_file_req' 	=> $upload_file_req, 
																'if_file_required' 	=> $if_file_required, 
																'input_more_than_1' => $inputMoreThanOne, 
																'latitude' 			=> $latitude, 
																'longitude' 		=> $longitude,
																'updateFlag' 		=> $updateFlag,
																'response_id' 		=> $cid,
																'answer' 			=> $answer);
											
											$this->recusiveSaveAnswers($requiredArr);
										}
									}
									else 
									{ 
										if($updateFlag)
										{	
											$this->removeAnswers($question_id, $parentQid = 0, $response_id);
										}										
									} // Main Question Anser Related Functionality End
									
									// Sub Question Related Functionality Start
									if(is_array($sub_question))
									{	
										if(count($sub_question) > 0) 
										{
											$main_question_qid 		= $question_id;
											
											foreach($sub_question as $key => $q_details)
											{
												$parent_question_id 	  	= @$q_details['parent_question_id'];
												$sub_question_qid 			= $q_details['question_id'];
												$sub_ques_response_type 	= $q_details['response_type'];
												$sub_ques_upload_file_req 	= $q_details['upload_file_required'];
												$sub_ques_if_file_required 	= $q_details['if_file_required'];
												$sub_ques_input_more_than_1 = $q_details['input_more_than_1'];
												$sub_ques_answer 			= $q_details['answer'];
												$sub_ques_latitude 			= isset($q_details['latitude'])? $q_details['latitude']:"";
												$sub_ques_longitude 		= isset($q_details['longitude'])? $q_details['longitude']:"";
												
												if(is_array($sub_ques_answer))
												{
													if(count($sub_ques_answer) > 0) 
													{														
														
													$requiredArr = array('survey_id' 		=> $survey_id, 
																		'question_id' 		=> $sub_question_qid, 
																		'parent_question_id'=> $parent_question_id, 
																		'response_type' 	=> $sub_ques_response_type, 
																		'upload_file_req' 	=> $sub_ques_upload_file_req, 
																		'if_file_required' 	=> $sub_ques_if_file_required, 
																		'input_more_than_1' => $sub_ques_input_more_than_1, 
																		'latitude' 			=> $sub_ques_latitude, 
																		'longitude' 		=> $sub_ques_longitude,
																		'updateFlag' 		=> $updateFlag,
																		'response_id' 		=> $cid,
																		'answer' 			=> $sub_ques_answer);
														$this->recusiveSaveAnswers($requiredArr);				
													}
												}
												else 
												{ 
													if($updateFlag)
													{	
														$this->removeAnswers($sub_question_qid,$parent_question_id, $response_id);
													}										
												}
											}	// Foreach End
										}	// Sub Question Count End										
									}	// Sub Question Related Functionality End									
									
									// Option Dependent Functionality Start 
									if(is_array(@$dependent_question))
									{	
										if(count(@$dependent_question) > 0) 
										{
											foreach(@$dependent_question as $opt => $ques_details) 
											{
												$selectedOptionValues 	= $dependent_question[$opt]['option'];
												$answerNodeValues 	  = @$dependent_question[$opt]['answer'];												
												//$p_qid 	= $question_id;
												$q_id 	= $ques_details['option_dependent_question']['question_id'];
												$p_qid 	= $ques_details['option_dependent_question']['parent_question_id'];
												$option_response_type 	= $ques_details['option_dependent_question']['response_type'];
												$option_if_file_required 	= $ques_details['option_dependent_question']['if_file_required'];
												$option_input_more_than_1 = $ques_details['option_dependent_question']['input_more_than_1'];
												$option_upload_file_req = $ques_details['option_dependent_question']['upload_file_required'];
												$q_id 		= $ques_details['option_dependent_question']['question_id'];												
												$option_answer = (@$ques_details['option_dependent_question']['answer'])? @$ques_details['option_dependent_question']['answer']: array();
												
												// Recursive Question With Answer
												$question_recursive = @$ques_details['option_dependent_question']['dependent_question_options'];
								
											$option_latitude  = isset($ques_details['option_dependent_question']['latitude'])? $ques_details['option_dependent_question']['latitude']:"";
											$option_longitude = isset($ques_details['option_dependent_question']['longitude'])? $ques_details['option_dependent_question']['longitude']:"";
											
											
											$requiredArr = array('survey_id' 		=> $survey_id, 
																'question_id' 		=> $q_id, 
																'parent_question_id'=> $p_qid, 
																'response_type' 	=> $option_response_type, 
																'upload_file_req' 	=> $option_upload_file_req, 
																'if_file_required' 	=> $option_if_file_required, 
																'input_more_than_1' => $option_input_more_than_1, 
																'latitude' 			=> $option_latitude, 
																'longitude' 		=> $option_longitude,
																'updateFlag' 		=> $updateFlag,
																'response_id' 		=> $cid,
																'answer' 			=> $option_answer);
											
											$this->recusiveSaveAnswers($requiredArr);
												
												if(is_array($question_recursive))
												{													
													if(count($question_recursive) > 0)
													{
														
														$arrDe = array('survey_id' 	=> $survey_id, 
																	'response_id' 	=> $cid, 
																	'updateFlag'	=> $updateFlag,
																	'question_id'	=> $q_id,
																	'parent_question_id'	=> $p_qid,
																	'question_recursive'=> $question_recursive);
														$this->dependentQuestions($arrDe);			
														
													}
												}
											}
										}
									} // Option Dependent Functionality End 									
								
									$response['status'] 	= "1";
									//$response['message'] 	= "Survey Response Saved Successfully.";
									$response['message'] 	= "सर्वेक्षण प्रतिसाद यशस्वीरित्या जतन केला.";
									$response['data']		= array("response_id" => $cid, "ref_id" =>$ref_id);
								}
							}
							else
							{						
								$response['status'] 	= "0";
								//$response['message'] 	= "No Section Questions Found.";
								$response['message'] 	= "कोणतेही विभाग प्रश्न सापडले नाहीत";
							}


						}
						else
						{							
							$response['status'] 	= "0";
							//$response['message'] 	= "No Sections Found.";
							$response['message'] 	= "कोणतेही विभाग सापडले नाहीत.";
						}
						
					} // Ref ID check End If
					
				}
				else 
				{
					//echo "No Data Found";
					$response['status'] 	= "0";
					//$response['message'] 	= "Survey ID does not exist.";
					$response['message'] 	= "सर्वेक्षण आयडी विद्यमान नाही.";
				}
				
			}
			else
			{				
				$response['status'] 	= "0";
				//$response['message'] 	= "No Data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}	
			
		}
		
		// Return Response
		$this->response($response);
		
	}
	
	// Recursive Dependent Question Answer loop 
	public function dependentQuestions($arr)
	{		
		$survey_id 			= $arr['survey_id'];
		$response_id 		= $arr['response_id'];
		$parent_question_id = $arr['parent_question_id'];
		$updateFlag 		= $arr['updateFlag'];
		$quesArr 			= $arr['question_recursive'];
		//$form_id			= $arr['form_id'];
		
		if(is_array($quesArr))
		{
			if(count($quesArr) > 0)
			{														
				foreach($quesArr as $key => $val)
				{
					$question_id 			= $val['option_dependent_question']['question_id'];
					$dp_question_id 		= $val['option_dependent_question']['parent_question_id'];
					$response_type 			= $val['option_dependent_question']['response_type'];
					$input_more_than_1 		= $val['option_dependent_question']['input_more_than_1'];
					$if_file_required 		= $val['option_dependent_question']['if_file_required'];
					$upload_file_required 	= $val['option_dependent_question']['upload_file_required'];
					$latitude  				= isset($val['option_dependent_question']['latitude'])? $val['option_dependent_question']['latitude']:"";
					$longitude 				= isset($val['option_dependent_question']['longitude'])? $val['option_dependent_question']['longitude']:"";
					//$answer 				= $val['option_dependent_question']['answer']['value'];
					$answer 				= (@$val['option_dependent_question']['answer'])? @$val['option_dependent_question']['answer']: array();
					
					// Recursive Option Dependent Question Node Found 
					$question_recursive 	= @$val['option_dependent_question']['dependent_question_options'];

					if(is_array($question_recursive))
					{
						
						if(count($question_recursive) > 0)
						{
							
							$comArr = array('survey_id' 	=> $survey_id, 
											'response_id' 	=> $response_id, 
											'updateFlag'	=> $updateFlag,
											'question_id'	=> $question_id,
											'parent_question_id' => $dp_question_id,
											'question_recursive'=> $question_recursive);
							//print_r($comArr);
							$this->dependentQuestions($comArr);
						} // END Count IF
					} // END IS Array
					

					// Answer Store Function
					$requiredArr = array('survey_id' 		=> $survey_id, 
										'question_id' 		=> $question_id, 
										'parent_question_id'=> $dp_question_id, 
										'response_type' 	=> $response_type, 
										'upload_file_req' 	=> $upload_file_required, 
										'if_file_required' 	=> $if_file_required, 
										'input_more_than_1' => $input_more_than_1, 
										'latitude' 			=> $latitude, 
										'longitude' 		=> $longitude,
										'updateFlag' 		=> $updateFlag,
										'response_id' 		=> $response_id,
										'answer' 			=> $answer);
					
					$this->recusiveSaveAnswers($requiredArr);
					
				}	// Foreach End
			} // Count If End	
		} // Array Check End			
	} // End Function
	
	// Recursive Save Answer Function 
	public function recusiveSaveAnswers($arr)
	{
		$returnArr			= array();
			
		$survey_id 			= $arr['survey_id'];
		$question_id 		= $arr['question_id'];
		$parent_question_id = $arr['parent_question_id'];
		$response_type 		= $arr['response_type'];
		$upload_file_req 	= $arr['upload_file_req'];
		$if_file_required 	= $arr['if_file_required'];
		$inputMoreThanOne 	= $arr['input_more_than_1'];
		$latitude 			= $arr['latitude'];
		$longitude 			= $arr['longitude'];
		$updateFlag 		= $arr['updateFlag'];
		$response_id 		= $arr['response_id'];
		$answer 			= $arr['answer'];
		
		// First Condition Start
		if($upload_file_req == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($response_type == "textbox" || $response_type == "textarea"))
		{
			$salutation = isset($answer['salutation'])? $answer['salutation']:"";
			$answer_name = isset($answer['value'])? $answer['value']:"";
			
			$insertArr = array( "response_id" 		=> $response_id,
								"parent_question_id"=> $parent_question_id,
								"question_id" 		=> $question_id,
								"answer_value" 		=> $answer_name,
								"salultation_value" => $salutation);
			
			$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");
			
			$cnt = $queryResponse->num_rows();													
			if($cnt > 0){$flag = 1;}else{$flag = 0;}														
			if($flag == 0)
			{
				$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
				$lastQuery = $this->db->last_query();
				$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
			}	
			else 
			{
				if($parent_question_id > 0)
				{
					$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
				}
				else 
				{
					$param = array("response_id" => $response_id, "question_id" => $question_id);
				}
				$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
				$lastQuery = $this->db->last_query();
				$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update',"lastQuery" => $lastQuery);
			}
		} // First Condition End	

		// Second Condition Start
		if($upload_file_req == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($response_type == "textbox" || $response_type == "textarea"))									
		{
			$salutation 	 = isset($answer['salutation'])? $answer['salutation']:"";
			$image_name 	 = isset($answer['image_url'])? $answer['image_url']:"";
			$img_latitude 	 = isset($latitude)? $latitude:"";		
			$img_longitude 	 = isset($longitude)? $longitude:"";
			
			$img_origional_name = '';										
			if($image_name!="") 
			{
				$random_name = $survey_id."_".$question_id."_".time()."_".rand();
				$img_origional_name = $this->generateImage($random_name, $image_name);
			}			
			
			$answer_name = isset($answer['value'])? $answer['value']:"";			
			$insertArr = array( "response_id" 		=> $response_id,
								"question_id" 		=> $question_id,
								"answer_value" 		=> $answer_name,
								"salultation_value" => $salutation,
								"parent_question_id"=> $parent_question_id,
								"file_name" 		=> $img_origional_name,
								"latitude" 			=> $img_latitude,
								"longitude" 		=> $img_longitude);
			
			$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");			
			$cnt = $queryResponse->num_rows();			
			if($cnt > 0){$flag = 1;}else{$flag = 0;}
			
			if($flag == 0)
			{
				$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
				$lastQuery = $this->db->last_query();
				$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
			}	
			else 
			{	
				if($parent_question_id > 0)
				{
					$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
				}
				else 
				{
					$param = array("response_id" => $response_id, "question_id" => $question_id);
				}
				$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
				$lastQuery = $this->db->last_query();
				$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
			}
				
		} // Second Condition End

		// Third Condition Start For Checkbox|Radio|Select Box|File
		if($upload_file_req == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'No' && ($response_type == "radio" || $response_type == "checkbox"  || $response_type == "single_select" || $response_type == "file"))									
		{
			$imgUrl 		= isset($answer['image_url'])? $answer['image_url']:"";
			$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
			$img_latitude 	= isset($latitude)? $latitude:"";		
			$img_longitude 	= isset($longitude)? $longitude:"";
			$multi_img 		= '';
			if($imgUrl!="") 
			{
				$random_name = $survey_id."_".$question_id."_".time()."_".rand();
				$multi_img = $this->generateImage($random_name, $imgUrl);
			}
			
			if($response_type == 'file') 
			{	
				$insertArr = array( "response_id" 		=> $response_id,
									"question_id" 		=> $question_id,
									"answer_value" 		=> $multi_img,
									"parent_question_id"=> $parent_question_id,
									"latitude" 			=> $img_latitude,
									"longitude" 		=> $img_longitude);
				
				$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");
				
				$cnt = $queryResponse->num_rows();												
				
				if($cnt > 0){$flag = 1;}else{$flag = 0;}												
				
				if($flag == 0)
				{
					$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
					$lastQuery = $this->db->last_query();
					$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
				}	
				else 
				{
					if($parent_question_id > 0)
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
					}
					else 
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id);
					}
					$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
					$lastQuery = $this->db->last_query();
					$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
				}
			}
			else 
			{
				$chkArrpush = array();													
				foreach($answer as $key => $value) 
				{
					if($key ==  "image_url")
					{
						
					}
					else 
					{						
						$insertArr = array("response_id" 		=> $response_id,
											"question_id" 		=> $question_id,
											"key_name" 			=> $key,
											"answer_value" 		=> $value,
											"parent_question_id"=> $parent_question_id,
											"file_name" 		=> $multi_img,
											"salultation_value" => $salutation,
											"latitude" 			=> $img_latitude,
											"longitude" 		=> $img_longitude);
						
						if($response_type == "checkbox" || $response_type == "radio" ||  $response_type == 'single_select') 
						{							
							array_push($chkArrpush, $key);
						}	
						
						$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND key_name =  '".$key."'  AND is_deleted = '0'");
						
						$cnt = $queryResponse->num_rows();
						
						if($cnt > 0){$flag = 1;}else{$flag = 0;}
							
						if($flag == 0)
						{
							$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
							$lastQuery = $this->db->last_query();
							$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
						}	
						else 
						{
							if($parent_question_id > 0)
							{
								$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
							}
							else 
							{
								$param = array("response_id" => $response_id, "question_id" => $question_id);
							}
							//$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,array("response_id" => $response_id, "question_id" => $question_id, "key_name" => "'".$key."'"));
							$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
							$lastQuery = $this->db->last_query();
							$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
						}
					}
				}
				
				// Delete Previous Answers regarding Checkbox and Selectbox
				if($updateFlag)
				{
					if(count($chkArrpush) > 0) 
					{
						$updatedAt = date('Y-m-d H:i:s');
						$this->db->where_not_in('key_name', $chkArrpush);
						$updateArr = array("is_deleted" => '1');
						$updateNotQuery = $this->master_model->updateRecord('mv_survey_responses',$updateArr,array("response_id" => $response_id, "question_id" => $question_id));
						//echo $this->db->last_query();
					}													
				}
			}				
		} // Third Condition End
		
		// Fourth Condition Start
		if($upload_file_req == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'No' && ($response_type == "radio" || $response_type == "checkbox"  || $response_type == "single_select" || $response_type == "file"))
		{	
			if($response_type == 'file') 
			{
				$image_name 	= isset($answer['image_url'])? $answer['image_url']:"";						
				$img_latitude 	= isset($latitude)? $latitude:"";		
				$img_longitude 	= isset($longitude)? $longitude:"";		
				$img_origional 	= '';										
				if($image_name!="") 
				{
					$random_name = $survey_id."_".$question_id."_".time()."_".rand();
					$img_origional = $this->generateImage($random_name, $image_name);
				}
				
				$insertArr = array("response_id" 	=> $response_id,
									"question_id" 	=> $question_id,
									"answer_value" 	=> $img_origional,
									"parent_question_id" => $parent_question_id,
									"latitude" 		=> $img_latitude,
									"longitude" 	=> $img_longitude);
									
				$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");				
				$cnt = $queryResponse->num_rows();				
				if($cnt > 0){$flag = 1;}else{$flag = 0;}				
				if($flag == 0)
				{
					$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
					$lastQuery = $this->db->last_query();					
					$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
				}	
				else 
				{
					$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
					$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
					$lastQuery = $this->db->last_query();
					$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
				}				
			}
			else 
			{
				$chkArrpush = array();
				$img_latitude 	= isset($latitude)? $latitude:"";		
				$img_longitude 	= isset($longitude)? $longitude:"";
				foreach($answer as $key => $value) 
				{
					$salutation = isset($answer['salutation'])? $answer['salutation']:"";					
					if($key == "image_url") 
					{						
					}
					else 
					{
						$insertArr = array("response_id" 	=> $response_id,
											"question_id" 	=> $question_id,
											"key_name" 		=> $key,
											"answer_value" 	=> $value,
											"parent_question_id" => $parent_question_id,
											"salultation_value" => $salutation,
											"latitude" 		=> $img_latitude,
											"longitude" 	=> $img_longitude);
						//print_r($insertArr);
						if($response_type == "checkbox" || $response_type == "radio" ||  $response_type == 'single_select') 
						{			
							array_push($chkArrpush, $key);
						}						
						$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND key_name='".$key."' AND is_deleted = '0' AND parent_question_id = '".$question_id."'");
						$cnt = $queryResponse->num_rows();						
						if($cnt > 0){$flag = 1;}else{$flag = 0;}						
						if($flag == 0)
						{
							$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
							$lastQuery = $this->db->last_query();
							$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
						}	
						else 
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
							$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
							$lastQuery = $this->db->last_query();
							$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
						}	
					}														
				}
				
				if($updateFlag)
				{
					if(count($chkArrpush) > 0) 
					{
						$updatedAt = date('Y-m-d H:i:s');
						$this->db->where_not_in('key_name', $chkArrpush);
						$updateArr = array("is_deleted" => '1');
						$updateNotQuery = $this->master_model->updateRecord('mv_survey_responses',$updateArr,array("response_id" => $response_id, "form_id" => $form_id, "question_id" => $question_id));
					}														
				}
			} // Else Part Of If Not File			
		} // Fourth Condition End
	
		 // Five Condtion Start
		if($upload_file_req == 'Yes' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($response_type == "textbox" || $response_type == "textarea"))
		{
			foreach($answer['value'] as $key => $value) 
			{										
				$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
				$image_name 	= isset($answer['image_url'])? $answer['image_url']:"";
				$img_latitude 	= isset($latitude)? $latitude:"";		
				$img_longitude 	= isset($longitude)? $longitude:"";
				$img_origional  = '';										
				if($image_name!="") 
				{
					$random_name = $survey_id."_".$question_id."_".time()."_".rand();
					$img_origional = $this->generateImage($random_name, $image_name);
				}	
				
				$insertArr = array("response_id" 		=> $response_id,
									"question_id" 		=> $question_id,
									"answer_value" 		=> $value['value'],
									"parent_question_id" => $parent_question_id,
									"salultation_value" => $salutation,
									"input_file_required" => $img_origional,
									"latitude" 			=> $img_latitude,
									"longitude" 		=> $img_longitude);
							
				$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");
				
				$cnt = $queryResponse->num_rows();	
				
				if($cnt > 0){$flag = 1;}else{$flag = 0;}
				
				if($flag == 0)
				{
					$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
					$lastQuery = $this->db->last_query();
					$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
				}	
				else 
				{
					if($parent_question_id > 0)
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
					}
					else 
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id);
					}
					$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
					$lastQuery = $this->db->last_query();
					$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
				}	
			}
		} // Five Condtion End	
		
		// Six Condition Start
		if($upload_file_req == 'No' && $if_file_required == 'No' && $inputMoreThanOne == 'Yes' && ($response_type == "textbox" || $response_type == "textarea"))
		{
			$image_name 		= isset($answer['image_url'])? $answer['image_url']:"";
			//$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
			$chkArrpush = array(); //echo "<pre>";print_r($answer);
			foreach($answer as $key => $value) 
			{	
				
				$image_name = "";
				if($key == "image_url") 
				{
					
				}
				else 
				{	
					$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
					$img_latitude 	= isset($latitude)? $latitude:"";		
					$img_longitude 	= isset($longitude)? $longitude:"";
					
					array_push($chkArrpush, $key);
					$insertArr = array( "response_id" 		=> $response_id,
										"question_id" 		=> $question_id,
										"key_name" 			=> $key,
										"answer_value"		=> $value,
										"file_name" 		=> $image_name,
										"parent_question_id" => $parent_question_id,
										"salultation_value" => $salutation,
										"latitude" 			=> $img_latitude,
										"longitude" 		=> $img_longitude);
					
					$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = ".$response_id." AND question_id = ".$question_id." AND parent_question_id = '".$parent_question_id."' AND key_name='".$key."' AND is_deleted = '0'");
					
					$cnt = $queryResponse->num_rows();
					
					if($cnt > 0){$flag = 1;}else{$flag = 0;}
					//echo $question_id."====".$flag;
					if($flag == 0)
					{
						$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
						$lastQuery = $this->db->last_query();
						$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
					}	
					else 
					{ 
						if($parent_question_id > 0)
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
						}
						else 
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id);
						}
						$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
						
						$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
					}
				}
			}
			// Delete Code
			if($updateFlag)
			{
				if(count($chkArrpush) > 0) 
				{
					$updatedAt = date('Y-m-d H:i:s');
					$this->db->where_not_in('key_name', $chkArrpush);
					$updateArr = array("is_deleted" => '1');
					$updateNotQuery = $this->master_model->updateRecord('mv_survey_responses',$updateArr,array("response_id" => $response_id, "question_id" => $question_id));
					
				}														
			}
		}	// Six Condition End
	
		// Seven Condition Start
		if($upload_file_req == 'No' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($response_type == "textbox" || $response_type == "textarea"))
		{	
			$image_name 	= isset($answer['image_url'])? $answer['image_url']:"";	
			$img_origional 	= '';										
			if($image_name!="") 
			{
				$random_name = $survey_id."_".$question_id."_".time()."_".rand();
				$img_origional = $this->generateImage($random_name, $image_name);
			}	
			$img_latitude 	= isset($latitude)? $latitude:"0.00";		
			$img_longitude 	= isset($longitude)? $longitude:"0.00";
			
			/**********Code Comment Due To Node not Added In Mobile App********/
			/*if(!is_array($answer['value']))
			{  
				foreach($answer as $key => $value) 
				{	
					$salutation = isset($answer['salutation'])? $answer['salutation']:"";
					
					$insertArr = array("response_id" 		=> $response_id,
										"question_id" 		=> $question_id,
										"key_name" 			=> $key,
										"answer_value" 		=> $value,
										"parent_question_id" => $parent_question_id,
										"file_name" 		=> $img_origional,
										"salultation_value" => $salutation,
										"latitude" 			=> $img_latitude,
										"longitude" 		=> $img_longitude);
								
					$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");
					
					$cnt = $queryResponse->num_rows();														
					if($cnt > 0){$flag = 1;}else{$flag = 0;}																
					if($flag == 0)
					{
						$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
						$lastQuery = $this->db->last_query();
						$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
					}	
					else 
					{
						if($parent_question_id > 0)
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
						}
						else 
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id);
						}
						$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
						$lastQuery = $this->db->last_query();
						$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
					}
				}
			}
			else 
			{	
				foreach($answer['value'] as $key => $value) 
				{	
					$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
					$img_latitude 	= isset($latitude)? $latitude:"";		
					$img_longitude 	= isset($longitude)? $longitude:"";	
					
					$insertArr = array("response_id" 		=> $response_id,
										"question_id" 		=> $question_id,
										"key_name" 			=> $key,
										"answer_value" 		=> $value,
										"file_name" 		=> $img_origional,
										"parent_question_id" => $parent_question_id,
										"salultation_value" => $salutation,
										"latitude" 			=> $img_latitude,
										"longitude" 		=> $img_longitude);
								
					$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = ".$response_id." AND question_id = ".$question_id." AND parent_question_id = '".$parent_question_id."' AND key_name='".$key."' AND is_deleted = '0'");
					
					$cnt = $queryResponse->num_rows();														
					if($cnt > 0){$flag = 1;}else{$flag = 0;}																
					if($flag == 0)
					{
						$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
						$lastQuery = $this->db->last_query();
						$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
					}	
					else 
					{
						if($parent_question_id > 0)
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
						}
						else 
						{
							$param = array("response_id" => $response_id, "question_id" => $question_id);
						}
						$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
						$lastQuery = $this->db->last_query();
						$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
					}
				}
			}	// Else End
			*/
			
			/**********End Code Comment Due To Node not Added In Mobile App********/

			
			$chkArrpush = array(); 
			//echo "<pre>";print_r($answer);
			foreach($answer as $key => $value) 
			{	
				
				
				if($key == "image_url") 
				{
					
				}
				else 
				{	
					$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
					$img_latitude 	= isset($latitude)? $latitude:"";		
					$img_longitude 	= isset($longitude)? $longitude:"";
					
					array_push($chkArrpush, $key);
					$insertArr = array( "response_id" 		=> $response_id,
										"question_id" 		=> $question_id,
										"key_name" 			=> $key,
										"answer_value"		=> $value,
										"file_name" 		=> $img_origional,
										"parent_question_id" => $parent_question_id,
										"salultation_value" => $salutation,
										"latitude" 			=> $img_latitude,
										"longitude" 		=> $img_longitude);
					//echo "<pre>";print_r($insertArr);
					$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = ".$response_id." AND question_id = ".$question_id." AND parent_question_id = '".$parent_question_id."' AND key_name='".$key."' AND is_deleted = '0'");
					
					$cnt = $queryResponse->num_rows();
					
					if($cnt > 0){$flag = 1;}else{$flag = 0;}
					
					if($flag == 0)
					{
						$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
						$lastQuery = $this->db->last_query();
						$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
					}	
					else 
					{ 
						$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id, "key_name" => "'".$key."'");
						$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr,$param);
						
						$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
					}
				}
			}
			// Delete Code
			if($updateFlag)
			{
				if(count($chkArrpush) > 0) 
				{
					$updatedAt = date('Y-m-d H:i:s');
					$this->db->where_not_in('key_name', $chkArrpush);
					$updateArr = array("is_deleted" => '1');
					$updateNotQuery = $this->master_model->updateRecord('mv_survey_responses',$updateArr,array("response_id" => $response_id, "question_id" => $question_id));
					
				}														
			}
			

		}	// Seven Condition End
	
		// Eight Condition Start
		if($upload_file_req == 'Yes' && $if_file_required == 'Yes' && $inputMoreThanOne == 'Yes' && ($response_type == "textbox" || $response_type == "textarea"))
		{
			$salutation 	= isset($answer['salutation'])? $answer['salutation']:"";
			$image_name 	= isset($answer['image_url'])? $answer['image_url']:"";
			$img_latitude 	= isset($latitude)? $latitude:"";		
			$img_longitude 	= isset($longitude)? $longitude:"";
			
			$img_origional = '';										
			if($image_name!="") 
			{
				$random_name 	= $survey_id."_".$question_id."_".time()."_".rand();
				$img_origional  = $this->generateImage($random_name, $image_name);
			}												
			foreach($answer['value'] as $key => $value) 
			{												
				$img_multiple = '';
				if($value['upload_file']!="") 
				{
					$random_name  = $survey_id."_".$question_id."_".time()."_".rand();
					$img_multiple = $this->generateImage($random_name, $value['upload_file']);
				}													
				
				$insertArr = array("response_id" 		=> $response_id,
									"question_id" 		=> $question_id,
									"answer_value" 		=> $value['value'],
									"file_name" 		=> $img_origional,
									"parent_question_id" => $parent_question_id,
									"salultation_value" => $salutation,
									"input_file_required" => $img_multiple,
									"latitude" 			=> $img_latitude,
									"longitude" 		=> $img_longitude);
						
				$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE response_id = '".$response_id."' AND question_id = '".$question_id."' AND parent_question_id = '".$parent_question_id."' AND is_deleted = '0'");

				$cnt = $queryResponse->num_rows();
				
				if($cnt > 0){$flag = 1;}else{$flag = 0;}
				
				if($flag == 0)
				{
					$insertQuery = $this->master_model->insertRecord('mv_survey_responses',$insertArr);
					$lastQuery = $this->db->last_query();
					$returnArr = array("lastQuery" => $lastQuery, "return_msg" => 'Success', "action" => 'Insert');
				}	
				else 
				{
					if($parent_question_id > 0)
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_question_id);
					}
					else 
					{
						$param = array("response_id" => $response_id, "question_id" => $question_id);
					}
					$updateQuery = $this->master_model->updateRecord('mv_survey_responses',$insertArr, $param);
					$lastQuery = $this->db->last_query();
					$returnArr = array("question_id" => $question_id, "return_msg" => 'Success', "action" => 'Update');
				}									
			}
		} // End Eight Condition
		
		return $returnArr;
	}
	
	// Remove Answer When Re-submit 
	public function removeAnswers($question_id, $parent_id, $response_id)
	{
		$updateArr = array('is_deleted' => 1);
		$updateNotQuery = $this->master_model->updateRecord('mv_response_headers',$updateArr,array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parent_id));
	}
	
	// response List Pagination Function
	public function response_list_pagination()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		$status			= isset($_POST['status']) ? $_POST['status'] : '';
		$currentpage	= isset($_POST['page']) ? $_POST['page'] : '1';
		
		//$rowsperpage = 5;
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			if(empty($status))
			{
			
				// request not be null -
				$response['status']		= "0";
				//$response['message']    = "Status can not be blank.";
				$response['message']    = "स्थिती रिक्त असू शकत नाही.";
			
			} 
			else if(empty($survey_id))
			{
				// request not be null -
				$response['status']		= "0";
				//$response['message']    = "Survey ID can not be blank.";
				$response['message']    = "सर्वेक्षण आयडी रिक्त असू शकत नाही.";
			}
			else 
			{
		
				$statusWhr = '';
				if($status == "returned")
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Returned')";
				}
				if($status == "Inconsistent")
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Inconsistent')";
				}
				else if($status == "approved") 
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Approved')";
				}
				else if($status == "submitted" || $status == "Submitted") 
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Submitted' OR `mv_agro_response_headers`.`status` = 'Re-Submitted')";
				}
				
				
				
				
				/***********************/
				
				$responseList = $this->db->query("SELECT mv_agro_response_headers.response_id FROM mv_agro_response_headers WHERE mv_agro_response_headers.`survey_id` = '".$survey_id."' ".$statusWhr." AND mv_agro_response_headers.`is_deleted` = '0' AND mv_agro_response_headers.`surveyor_id` = '".$user_id."' ORDER BY mv_agro_response_headers.response_id DESC");	
				//echo $this->db->last_query();die();	
				$numrows = $responseList->num_rows();
				//$resultdata = $responseList->result_array()
				//$numrows = $resultdata['TOTALNUMBER'];
				
				
				$rowsperpage	= isset($_POST['rowsperpage']) ? $_POST['rowsperpage'] : '2';
				
				// find out total pages
				$totalpages = ceil($numrows / $rowsperpage);
				
				///Checkeing for page requested
				/*if (isset($_POST['currentpage']) && is_numeric($_POST['currentpage'])) {
					$currentpage = (int) $_POST['currentpage'];
				} else {
					$currentpage = 1;  // default page number
				}*/
				
				// if current page is greater than total pages
				if ($currentpage > $totalpages) {
				// set current page to last page
					$currentpage = $totalpages;
				}
				// if current page is less than first page
				if ($currentpage < 1) {
				// set current page to first page
					$currentpage = 1;
				}
				// the offset of the list, based on current page
				$offset = ($currentpage - 1) * $rowsperpage;	
				
				$sql_limit = " LIMIT " . $rowsperpage . " OFFSET " . $offset;
				
				/***********************/				

				$surveyResponseList = $this->db->query("SELECT mv_agro_response_headers.response_id, mv_agro_response_headers.ref_id, mv_agro_response_headers.taluka_id, mv_agro_response_headers.village_id, mv_agro_response_headers.submitted_date, mv_agro_response_headers.submited_time, mv_agro_response_headers.shg_id,  mv_agro_response_headers.start_date_time, mv_agro_response_headers.end_date_time, mv_agro_response_headers.status FROM mv_agro_response_headers WHERE mv_agro_response_headers.`survey_id` = '".$survey_id."' ".$statusWhr." AND mv_agro_response_headers.`is_deleted` = '0' AND mv_agro_response_headers.`surveyor_id` = '".$user_id."' ORDER BY mv_agro_response_headers.response_id DESC  ".$sql_limit."");
				//echo $this->db->last_query();
				$cntRow = $surveyResponseList->num_rows();			
				$response_data = array();
				if($cntRow > 0)
				{ 				
					foreach($surveyResponseList->result_array() as $row)
					{	
						$this->db->select('taluka_name, taluka_name_marathi');
						$taluka_master = $this->master_model->getRecords("mv_taluka_master",array('taluka_id' => $row['taluka_id']));
						$taluka_name_en   = isset($taluka_master[0]['taluka_name']) ?$taluka_master[0]['taluka_name'] : '';
						$taluka_name_mr   = isset($taluka_master[0]['taluka_name_marathi']) ?$taluka_master[0]['taluka_name_marathi'] : '';
						
						$this->db->select('village_name, village_name_marathi, cmrc_id');
						$village_master = $this->master_model->getRecords("mv_village_master",array('village_id' => $row['village_id']));
						//$village_name_en   = isset($village_master[0]['village_name']) ?$village_master[0]['village_name'] : '';
						$village_name_en   = isset($village_master[0]['village_name_marathi']) ?$village_master[0]['village_name_marathi'] : '';
						$village_name_mr   = isset($village_master[0]['village_name_marathi']) ?$village_master[0]['village_name_marathi'] : '';
						$cmrc_id   		   = isset($village_master[0]['cmrc_id']) ?$village_master[0]['cmrc_id'] : '';
						//$cmrc_detail = $this->master_model->getRecords("mv_cmrc_village",array('village_id' => $row['village_id']));
						// commented 04-Oct-2021
						//$cmrc_detail = $this->master_model->getRecords("mv_village_master",array('village_id' => $row['village_id']));
						//$cmrc_id   = $cmrc_detail[0]['cmrc_id'];
						
						$this->db->select('cmrc_name_marathi, cmrc_name_marathi, cmrc_id');
						$cmrc_master = $this->master_model->getRecords("mv_cmrc_master",array('cmrc_id' => $cmrc_id));
						$cmrc_name_en   = isset($cmrc_master[0]['cmrc_name_marathi']) ?$cmrc_master[0]['cmrc_name_marathi'] : '';
						$cmrc_name_mr   = isset($cmrc_master[0]['cmrc_name_marathi']) ?$cmrc_master[0]['cmrc_name_marathi'] : '';
						
						$this->db->select('shg_name, shg_father_name, shg_last_name');
						$shg_details = $this->master_model->getRecords("mv_personal_details",array('shg_id' => $row['shg_id']));
						$firstname   = isset($shg_details[0]['shg_name']) ?$shg_details[0]['shg_name'] : '';
						$middlename   = isset($shg_details[0]['shg_father_name']) ?$shg_details[0]['shg_father_name'] : '';
						$lastname   = isset($shg_details[0]['shg_last_name']) ?$shg_details[0]['shg_last_name'] : '';
						
						
						$response_data[] = array('response_id' => $row['response_id'], 'ref_id' => $row['ref_id'], 'shg_id' => $row['shg_id'], 'first_name' => $firstname, 'middle_name' => $middlename, 'last_name' => $lastname, 'taluka_id' => $row['taluka_id'], 'taluka_name_english' => $taluka_name_en, 'taluka_name_marathi' => $taluka_name_mr, 'village_id' => $row['village_id'], 'village_name_english' => $village_name_mr, 'village_name_marathi' => $village_name_mr, 'cmrc_id' => $cmrc_id, 'cmrc_name_english' => $cmrc_name_en, 'cmrc_name_marathi' => $cmrc_name_mr, 'submitted_date' => $row['submitted_date'], 'submited_time' => $row['submited_time'], 'start_date_time' => $row['start_date_time'], 'end_date_time' => $row['end_date_time'], 'status' => $row['status']);
						
					}
					
					$response['status']		= "1";
					//$response['message'] 	= "Data sucessfully received.";
					$response['message'] 	= "डेटा यशस्वीरित्या प्राप्त झाला";
					$response['page'] 		= $currentpage;
					$response['total_page'] = $numrows;
					$response['data'] 		= $response_data;
				}
				else
				{
					$response['status'] 		= "0";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
			}
		}
		
		$this->response($response);
	}
	
	// Response Listing 
	public function response_list()
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id	= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		$status		= isset($_POST['status']) ? $_POST['status'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 
		{
			if(empty($status))
			{
			
				// request not be null -
				$response['status']		= "0";
				//$response['message']    = "Status can not be blank.";
				$response['message']    = "स्थिती रिक्त असू शकत नाही.";
			
			} 
			else if(empty($survey_id))
			{
				// request not be null -
				$response['status']		= "0";
				//$response['message']    = "Survey ID can not be blank.";
				$response['message']    = "सर्वेक्षण आयडी रिक्त असू शकत नाही.";
			}
			else 
			{			
				$statusWhr = '';
				if($status == "returned")
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Returned')";
				}
				if($status == "Inconsistent")
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Inconsistent')";
				}
				else if($status == "approved") 
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Approved')";
				}
				else if($status == "Submitted") 
				{
					$statusWhr = " AND (`mv_agro_response_headers`.`status` = 'Submitted' OR `mv_agro_response_headers`.`status` = 'Re-Submitted')";
				}

					$surveyResponseList = $this->db->query("SELECT mv_agro_response_headers.response_id, mv_agro_response_headers.ref_id, mv_agro_response_headers.taluka_id, mv_agro_response_headers.village_id, mv_agro_response_headers.submitted_date, mv_agro_response_headers.submited_time, mv_agro_response_headers.shg_id,  mv_agro_response_headers.start_date_time, mv_agro_response_headers.end_date_time, mv_agro_response_headers.status FROM mv_agro_response_headers WHERE mv_agro_response_headers.`survey_id` = '".$survey_id."' ".$statusWhr." AND mv_agro_response_headers.`is_deleted` = '0' AND mv_agro_response_headers.`surveyor_id` = '".$user_id."' ORDER BY mv_agro_response_headers.response_id DESC");
					//echo $this->db->last_query();
					$cntRow = $surveyResponseList->num_rows();			
					$response_data = array();
					if($cntRow > 0)
					{ 				
						foreach($surveyResponseList->result_array() as $row)
						{	
							$this->db->select('taluka_name, taluka_name_marathi');
							$taluka_master = $this->master_model->getRecords("mv_taluka_master",array('taluka_id' => $row['taluka_id']));
							$taluka_name_en   = isset($taluka_master[0]['taluka_name']) ?$taluka_master[0]['taluka_name'] : '';
							$taluka_name_mr   = isset($taluka_master[0]['taluka_name_marathi']) ?$taluka_master[0]['taluka_name_marathi'] : '';
							
							$this->db->select('village_name, village_name_marathi, cmrc_id');
							$village_master = $this->master_model->getRecords("mv_village_master",array('village_id' => $row['village_id']));
							//$village_name_en   = isset($village_master[0]['village_name']) ?$village_master[0]['village_name'] : '';
							$village_name_en   = isset($village_master[0]['village_name_marathi']) ?$village_master[0]['village_name_marathi'] : '';
							$village_name_mr   = isset($village_master[0]['village_name_marathi']) ?$village_master[0]['village_name_marathi'] : '';
							$cmrc_id   		   = isset($village_master[0]['cmrc_id']) ?$village_master[0]['cmrc_id'] : '';
							//$cmrc_detail = $this->master_model->getRecords("mv_cmrc_village",array('village_id' => $row['village_id']));
							// commented 04-Oct-2021
							//$cmrc_detail = $this->master_model->getRecords("mv_village_master",array('village_id' => $row['village_id']));
							//$cmrc_id   = $cmrc_detail[0]['cmrc_id'];
							
							$this->db->select('cmrc_name_marathi, cmrc_name_marathi, cmrc_id');
							$cmrc_master = $this->master_model->getRecords("mv_cmrc_master",array('cmrc_id' => $cmrc_id));
							$cmrc_name_en   = isset($cmrc_master[0]['cmrc_name_marathi']) ?$cmrc_master[0]['cmrc_name_marathi'] : '';
							$cmrc_name_mr   = isset($cmrc_master[0]['cmrc_name_marathi']) ?$cmrc_master[0]['cmrc_name_marathi'] : '';
							
							$this->db->select('shg_name, shg_father_name, shg_last_name');
							$shg_details = $this->master_model->getRecords("mv_personal_details",array('shg_id' => $row['shg_id']));
							$firstname   = isset($shg_details[0]['shg_name']) ?$shg_details[0]['shg_name'] : '';
							$middlename   = isset($shg_details[0]['shg_father_name']) ?$shg_details[0]['shg_father_name'] : '';
							$lastname   = isset($shg_details[0]['shg_last_name']) ?$shg_details[0]['shg_last_name'] : '';
							
							
							$response_data[] = array('response_id' => $row['response_id'], 'ref_id' => $row['ref_id'], 'shg_id' => $row['shg_id'], 'first_name' => $firstname, 'middle_name' => $middlename, 'last_name' => $lastname, 'taluka_id' => $row['taluka_id'], 'taluka_name_english' => $taluka_name_en, 'taluka_name_marathi' => $taluka_name_mr, 'village_id' => $row['village_id'], 'village_name_english' => $village_name_mr, 'village_name_marathi' => $village_name_mr, 'cmrc_id' => $cmrc_id, 'cmrc_name_english' => $cmrc_name_en, 'cmrc_name_marathi' => $cmrc_name_mr, 'submitted_date' => $row['submitted_date'], 'submited_time' => $row['submited_time'], 'start_date_time' => $row['start_date_time'], 'end_date_time' => $row['end_date_time'], 'status' => $row['status']);
							
						}
						
						$response['status']		= "1";
						//$response['message'] 	= "Data sucessfully received.";
						$response['message'] 	= "डेटा यशस्वीरित्या प्राप्त झाला";
						$response['data'] 		= $response_data;
					}
					else
					{
						$response['status'] 		= "0";
						//$response['message'] 	= "No data Found.";
						$response['message'] 	= "माहिती आढळली नाही.";
					}
			}
		}
		
		$this->response($response);
	}

	// Response Details
	public function survey_response_details()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// Get Survey ID
		$response_id	= isset($_POST['response_id']) ? $_POST['response_id'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही";
			
		} 
		else 
		{
			if(empty($response_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				//$response['message'] 	= "Response id is required.";
				$response['message'] 	= "प्रतिसाद आयडी आवश्यक आहे";
				
			} 
			else 
			{
				// Check Surveyor Is Match
				$this->db->select('survey_id, ref_id');
				$check_record = $this->master_model->getRecords("mv_agro_response_headers",array('surveyor_id'=>$user_id, "response_id" => $response_id));
				//$survey_id = $check_record[0]['survey_id'];
				//$ref_id = $check_record[0]['ref_id'];				
				$survey_id = isset($check_record[0]['survey_id']) ? $check_record[0]['survey_id'] : "";
				$ref_id = isset($check_record[0]['ref_id']) ? $check_record[0]['ref_id'] : "";
				
				if(count($check_record) > 0)
				{
					// Get Survey Details 
					$response['status'] 		= "1";
					//$response['message'] 		= "Survey response details get successfully.";	
					$response['message'] 		= "सर्वेक्षण प्रतिसाद तपशील यशस्वीरित्या मिळत";
					
						$response['response_id'] = $response_id;
						$response['ref_id'] = $ref_id;
						
						//$productList = $this->db->query("SELECT * FROM mv_agro_response_details WHERE response_id = '".$response_id."'  AND status = 'Active' AND is_deleted = '0' ORDER BY product_category_id ASC");
						
						$productList = $this->db->query("SELECT `detail_id`, `response_id`, `product_category_id`, `product_id`, `variety_id`, `other_specify`, `product_timeline`, `product_shelf_life`, `measurement_type`, `product_quantity`, `available_land_size`, `season_name`, `after_product_category_id`, `after_product_id`, `after_variety_id`, `after_other_specify`, `after_cutting_period`, `after_measurement_type`, `after_quantity`, `after_land_size`, `latitude`, `longitude`, `status` FROM mv_agro_response_details WHERE response_id = '".$response_id."'  AND status = 'Active' AND is_deleted = '0' ORDER BY product_category_id ASC");							
						//echo $this->db->last_query();die();
						$productCount = $productList->num_rows();
						
						if($productCount > 0)
						{				
							$mergeArr = array();
							foreach($productList->result_array() as $res)
							{	
								$resArr = array();
								
								$resArr['id'] 		= $res['detail_id'];
								// Category Name
								$queryCategory = $this->db->query("SELECT product_cat_id, p_category_name_english, p_category_name_marathi FROM mv_product_category WHERE product_cat_id = '".$res['product_category_id']."'");
								$productCatData = $queryCategory->row();								
								//$resArr['product_category_id'] 		= @$productCatData->product_cat_id;	
								//$resArr['category_name_english']	= @$productCatData->p_category_name_english; 
								//$resArr['category_name_marathi']	= @$productCatData->p_category_name_marathi; 								
								$resArr['product_category_id'] 		= isset($productCatData->product_cat_id) ? $productCatData->product_cat_id : '';
								$resArr['category_name_english'] 	= isset($productCatData->p_category_name_english) ? $productCatData->p_category_name_english : '';
								$resArr['category_name_marathi'] 	= isset($productCatData->p_category_name_marathi) ? $productCatData->p_category_name_marathi : '';
								
								// Product Name
								$queryProduct = $this->db->query("SELECT product_id, product_name_marathi, product_name_english FROM mv_product_master WHERE product_id = '".$res['product_id']."'");
								$productData = $queryProduct->row();								
								//$resArr['product_id'] 			= @$productData->product_id;	
								//$resArr['product_name_english']	= @$productData->product_name_marathi; 
								//$resArr['product_name_marathi']	= @$productData->product_name_english; 
								$resArr['product_id'] 			= isset($productData->product_id) ? $productData->product_id : '';	
								$resArr['product_name_english']	= isset($productData->product_name_marathi) ? $productData->product_name_marathi : '';	
								$resArr['product_name_marathi']	= isset($productData->product_name_english) ? $productData->product_name_english : '';	
																
								
								// Variety Name
								$queryVariety = $this->db->query("SELECT variety_id, product_id, variety_name_english, variety_name_marathi FROM mv_variety_master WHERE variety_id='".$res['variety_id']."'");
								$varietyData = $queryVariety->row();								
								//$resArr['variety_id'] 			= @$varietyData->variety_id;	
								//$resArr['variety_name_marathi']	= @$varietyData->variety_name_marathi; 
								//$resArr['variety_name_english']	= @$varietyData->variety_name_english;	
								$resArr['variety_id'] 			= isset($varietyData->variety_id) ? $varietyData->variety_id : '';		
								$resArr['variety_name_marathi']	= isset($varietyData->variety_name_marathi) ? $varietyData->variety_name_marathi : '';	
								$resArr['variety_name_english']	= isset($varietyData->variety_name_english) ? $varietyData->variety_name_english : '';	
								
								$resArr['otherSpecify'] 			=	$res['other_specify'];
								$resArr['product_timeline'] 		=	$res['product_timeline'];
								$resArr['product_shelf_life'] 		=	$res['product_shelf_life'];
								$resArr['measurement_type'] 		=	$res['measurement_type'];
								$resArr['product_quantity'] 		=	$res['product_quantity'];
								$resArr['available_land_size'] 		=	$res['available_land_size'];
								$resArr['season_name'] 				=	$res['season_name'];
								
								
								// After Crop Category Name
								$cropCategory = $this->db->query("SELECT product_cat_id, p_category_name_english, p_category_name_marathi FROM mv_product_category WHERE product_cat_id = '".$res['after_product_category_id']."'");
								$cropProductCatData 		= $cropCategory->row();								
								//$resArr['after_category_id'] 			= @$cropProductCatData->product_cat_id;	
								//$resArr['after_category_name_english']	= @$cropProductCatData->p_category_name_english; 
								//$resArr['after_category_name_marathi']	= @$cropProductCatData->p_category_name_marathi; 
								$resArr['after_category_id'] 			= isset($cropProductCatData->product_cat_id) ? $cropProductCatData->product_cat_id : '';		
								$resArr['after_category_name_english']	= isset($cropProductCatData->p_category_name_english) ? $cropProductCatData->p_category_name_english : '';
								$resArr['after_category_name_marathi']	= isset($cropProductCatData->p_category_name_marathi) ? $cropProductCatData->p_category_name_marathi : '';
								
								// Crop Product Name
								$cropProduct = $this->db->query("SELECT product_id, product_name_marathi, product_name_english FROM mv_product_master WHERE product_id = '".$res['after_product_id']."'");
								$cropProductData = $cropProduct->row();									
								//$resArr['after_product_id'] 			= @$cropProductData->product_id;	
								//$resArr['after_product_name_english']	= @$cropProductData->product_name_marathi; 
								//$resArr['after_product_name_marathi']	= @$cropProductData->product_name_english;
								$resArr['after_product_id'] 			= isset($cropProductData->product_id) ? $cropProductData->product_id : '';
								$resArr['after_product_name_english']	= isset($cropProductData->product_name_marathi) ? $cropProductData->product_name_marathi : '';
								$resArr['after_product_name_marathi']	= isset($cropProductData->product_name_english) ? $cropProductData->product_name_english : '';
								
								// Crop Variety Name
								$cropVariety = $this->db->query("SELECT variety_id, product_id, variety_name_english, variety_name_marathi FROM mv_variety_master WHERE variety_id='".$res['after_variety_id']."'");
								$cropVarietyData = $cropVariety->row();								
								//$resArr['after_Variety_id'] 			= @$cropVarietyData->variety_id;	
								//$resArr['after_Variety_name_marathi']	= @$cropVarietyData->variety_name_marathi; 
								//$resArr['after_Variety_name_english']	= @$cropVarietyData->variety_name_english;
								$resArr['after_Variety_id'] 			= isset($cropVarietyData->variety_id) ? $cropVarietyData->variety_id : '';	
								$resArr['after_Variety_name_marathi']	= isset($cropVarietyData->variety_name_marathi) ? $cropVarietyData->variety_name_marathi : '';	 
								$resArr['after_Variety_name_english']	= isset($cropVarietyData->variety_name_english) ? $cropVarietyData->variety_name_english : '';	
								
								$resArr['after_other_specify']	 	=	$res['after_other_specify'];
								$resArr['after_cutting_period'] 	=	$res['after_cutting_period'];
								$resArr['after_measurement_type'] 	=	$res['after_measurement_type'];
								$resArr['after_quantity'] 			=	$res['after_quantity'];
								$resArr['after_land_size'] 			=	$res['after_land_size'];
								
								// Remark If added by CMRC
								/*$arrRemark = array("survey_id" => $survey_id, "response_id" =>  $response_id, 
													"product_id" =>  $res['product_id']);								
								$remarkContent = $this->remarkContent($arrRemark);
								$resArr['remark'] = $remarkContent;*/
								
								$mergeArr[] = $resArr;
								
							} // End Foreach Related Section Question
							
							$responseDetails['response_details']		= @$mergeArr;
						}			
						else 
						{
							$response['status'] 	= "0";
							//$response['message'] 	= "No Question Found.";
							$response['message'] 	= "कोणताही प्रतिसाद सापडला नाही.";
						}
						
						//$response['data']['products'][]	= $responseDetails;
						$response['data'][]	= $responseDetails;
				}
				else 
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "Invalid Response ID.";
					$response['message'] 	= "अवैध प्रतिसाद आयडी";
				}
			}
		}
		
		$this->response($response);
	}
	
	//Remark Showing In Questionaire 
	public function remarkContent($arr)
	{
		$product_id 	= $arr['product_id']; 
		$survey_id 	 	= $arr['survey_id']; 
		$response_id 	= $arr['response_id'];	
		$this->db->select('remark');	
		//$this->db->order_by('status_id', 'desc');
		$this->db->order_by('remark_id', 'desc');
		$this->db->limit(1);
		$returnContent = $this->master_model->getRecords("mv_response_remark",array('survey_id' => $survey_id, 'response_id' => $response_id, 'product_id' => $product_id));
		$return_text = "";
		if(count($returnContent) > 0)
		{
			$return_text = $returnContent[0]['remark'];
		}
		return $return_text;	
	}

	// Return Survey Section List
	public function getReturnSurveySectionList($survey_id)
	{
			$result = array();
			
			$surveySectionList = $this->db->query("SELECT DISTINCT `mv_section_questions`.`section_id`, `mv_section_master`.`section_name_marathi`, `mv_section_master`.`section_desc_marathi` FROM `mv_section_questions` JOIN `mv_section_master` ON `mv_section_master`.`section_id` = `mv_section_questions`.`section_id` WHERE `mv_section_questions`.`survey_id` = '".$survey_id."' AND `mv_section_questions`.`is_deleted` = 0 AND `mv_section_master`.`is_deleted` = 0 ORDER BY `mv_section_questions`.`sort_order` IS NOT NULL DESC, `mv_section_questions`.`sort_order` ASC");					
			//echo $this->db->last_query();
			$surveyCount = $surveySectionList->num_rows();
			
			if($surveyCount > 0)
			{		
				foreach($surveySectionList->result_array()  as $res) 
				{
					$result[] = array("section_id" => $res['section_id'], "section_name" => $res['section_name_marathi'], "section_description" => $res['section_desc_marathi']);
				}
			}
			
			return $result;
				
	}

	// response recursive function
	public function getOptionbaseResponse($question_id, $dependant_ques_id, $id, $option, $survey_id, $response_id)
	{ 
		//ob_start();		
		$queryQuestion = $this->db->query("SELECT question_id,question_text_marathi, question_text_english,response_type_id,is_mandatory,parent_question_id,salutation_english,salutation_marathi,input_more_than_1,if_file_required,file_label, input_file_required,if_summation,help_text_english,help_text_marathi FROM mv_question_master WHERE question_id = '".$dependant_ques_id."' AND is_deleted = '0' AND status = 'Active'");
		$questionData = $queryQuestion->row();				
		$num_rows = $queryQuestion->num_rows();
		if($num_rows > 0)
		{	
			$in_arr = array();
			$explodeSub =  array();
			if($questionData->salutation_marathi!="")
			{
				$explodeSub = explode("|", $questionData->salutation_marathi);
			} 
			 // get the first row
			$in_arr['question_id'] 				= $questionData->question_id; 
			$in_arr['question_label_marathi'] 	= $questionData->question_text_marathi;
			$in_arr['question_label_english'] 	= $questionData->question_text_english;
			$in_arr['response_type'] 			= $questionData->response_type_id;
			$in_arr['is_mandatory'] 			= $questionData->is_mandatory;
			$in_arr['parent_question_id'] 		= $questionData->parent_question_id;
			$in_arr['question_help_english'] 	= $questionData->help_text_english;
			$in_arr['question_help_marathi'] 	= $questionData->help_text_marathi;
			$in_arr['salutation'] 				= json_encode($explodeSub);
			$in_arr['input_more_than_1'] 		= $questionData->input_more_than_1;
			$in_arr['if_file_required']			= 'No';
			$in_arr['upload_file_required']		= 'No';
			$in_arr['if_summation']				= 'No';
			$in_arr['is_correction_required']	= 'Yes';
			//$arrRemark = array("survey_id" => $survey_id, "response_id" =>  $response_id, "question_id" =>  $questionData->question_id);
			//$remarkContent = $this->remarkContent($arrRemark);
			//$in_arr['remark'] = $remarkContent;
			if($questionData->if_file_required == 'Yes')
			{
				$in_arr['if_file_required'] 	= $questionData->if_file_required;
				$in_arr['file_label'] 			= $questionData->file_label;
			}
			
			if($questionData->input_file_required == 'Yes')
			{
				$in_arr['upload_file_required'] 	= $questionData->input_file_required;
			}
			
			if($questionData->if_summation == 'Yes')
			{
				$in_arr['if_summation'] 		= $questionData->if_summation;
			}	
			
			
			
			// Get Option 
			$quetionOptions_1 = $this->db->query("SELECT *  FROM mv_questions_options_master WHERE question_id = '".$questionData->question_id."' AND is_deleted='0' ORDER BY display_order ASC");
			$option_count_1 = $quetionOptions_1->num_rows();
			$optArr_1 = array();	
			if($option_count_1 > 0)
			{				
				foreach($quetionOptions_1->result_array() as $opt_1)
				{
					if($opt_1['option_marathi']!="")
					{
						array_push($optArr_1, $opt_1['option_marathi']);
					}
					
					
					/***** Multiple Option Dependent Question Data Start ******/
				
						$quetionOptionMultipleQuestion = $this->db->query("SELECT * FROM mv_option_dependent WHERE question_id = '".$questionData->question_id."' AND ques_option_id='".$opt_1['ques_option_id']."' AND is_deleted='0'");
						$multipleQuestionCnt = $quetionOptionMultipleQuestion->num_rows();
						if($multipleQuestionCnt > 0)
						{
							foreach($quetionOptionMultipleQuestion->result_array() as $multipleQues)
							{
								$queryMultipleQuestion = $this->db->query("SELECT * FROM mv_questions_options_master WHERE ques_option_id = '".$multipleQues['ques_option_id']."' AND is_deleted = '0' ORDER BY display_order ASC");
								$questionMultipleData = $queryMultipleQuestion->row();
								$getOption = $questionMultipleData->option_marathi;
								
								// Below Code for multiple level array questions
								$in_arr['dependent_question_options'][] = $this->getOptionbaseResponse($multipleQues['question_id'], $multipleQues['dependant_ques_id'], $multipleQues['ques_option_id'], $getOption, $survey_id, $response_id);								
								
							}
							
						}
				
					/***** Multiple Option Dependent Question Data Start ******/
					
					if($opt_1['validation_id'] > 0)
					{
						$in_arr['validation_type'] 	= $this->validation_response_type($opt_1['validation_id']);
						
						if($opt_1['sub_validation_id'] > 0)
						{
							$in_arr['validation_sub_type'] = $this->validation_sub_response_type($opt_1['sub_validation_id']);
						}
					}
					else 
					{
						$in_arr['validation_type'] = '';
						$in_arr['validation_sub_type'] = '';
					}
					//$in_arr['validation_id'] 	= $opt_1['min_value'];
					//$in_arr['sub_validation_id'] = $opt_1['sub_validation_id'];
					$in_arr['min_value'] 		= $opt_1['min_value'];
					$in_arr['max_value'] 		= $opt_1['max_value'];
					$in_arr['validation_label']	= $opt_1['validation_label'];	
				}

				if(count($optArr_1) > 0)
				{
					$in_arr['options'] = json_encode($optArr_1);
				}
				else
				{
					$blank = array("");
					$in_arr['options'] = json_encode($blank);
					//$in_arr['options'] = '[]';
				}	
				//$in_arr['options'] = json_encode($optArr_1);

				$queryResponse = $this->db->query("SELECT * FROM mv_survey_responses WHERE question_id = '".$dependant_ques_id."' AND parent_question_id = '".$question_id."' AND response_id='".$response_id."' AND is_deleted = '0' ORDER BY id DESC");
				//echo $this->db->last_query();
				$responseData = $queryResponse->row();
				//print_r($responseData);	
				if(count($responseData) > 0)
				{
					
					// Add Answer Node 	
					//$answerNode 	= $this->addAnswer($response_id, $responseData->question_id, $questionData->response_type_id, $questionData->input_more_than_1, $questionData->if_file_required, $questionData->input_file_required, $salutationArr, $responseData->parent_question_id);	
					
					$salutationArr		= $explodeSub;
					$answerArr = array( "response_id"			=> $response_id,
										"question_id"			=> $responseData->question_id,
										"response_type"			=> $questionData->response_type_id,
										"input_more_than_one"	=> $questionData->input_more_than_1,
										"if_file_required"		=> $questionData->if_file_required,
										"upload_file"			=> $questionData->input_file_required,
										"salutation"			=> $salutationArr,
										"parent_question_id"	=> $responseData->parent_question_id);
					$answerNode 	= $this->addAnswer($answerArr);	
					$in_arr['answer']  =  $answerNode;
				}
			}
		}
		
		$dataArr = array("option" => $option, "option_dependent_question" => $in_arr); 		
		//return json_encode($dataArr);
		return $dataArr;
	} // Function End

	//Add Answer Node To Responses 
	public function addAnswer($arr)
	{
		
		$response_id		= $arr['response_id'];		
		$question_id		= $arr['question_id'];
		$type				= $arr['response_type'];
		$inputMore			= $arr['input_more_than_one'];
		$fileRequired		= $arr['if_file_required'];
		$uploadRequired		= $arr['upload_file'];
		$salutation			= $arr['salutation'];
		$parentQuestionID	= $arr['parent_question_id'];		

		
		$getResponse = $this->master_model->getRecords("mv_survey_responses", array("response_id" => $response_id, "question_id" => $question_id, "parent_question_id" => $parentQuestionID, "is_deleted" => 0),'',array("id" => 'DESC'));
		//echo $this->db->last_query();
		
		$response = array();
		if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="Yes" && $uploadRequired=="Yes")
		{
			//if(count($salutation) > 0)
			//{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}				
			//}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
				
				if($getResponse[0]['input_file_required']!="")
				{
					$response['value']['upload_file'] = $this->convertImageToBase64encode($getResponse[0]['input_file_required']);
				}
				else 
				{
					$response['value']['upload_file'] = "";
				}
			}
			$response['value'] = $str;			
		}
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			//if(count($salutation) > 0)
			//{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			//}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
			}
			$response['value'] = $str;		
			
		}
		// Condition added to handle single textfield + image, by Bhagwan, on 02-04-2021
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="No" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			//if(count($salutation) > 0)
			//{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			//}
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}
			
			$response['value'] = $getResponse[0]['answer_value'];		
			
		} // EOF condition added to handle single textfield + image, by Bhagwan, on 02-04-2021
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="Yes" && $fileRequired=="No" && $uploadRequired=="No")
		{
			//if(count($salutation) > 0)
			//{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			//}
			
			$str = array();
			if(count($getResponse) > 0)
			{
				foreach($getResponse as $key => $values)
				{
					$str[$values['key_name']] = $values['answer_value'];
				}
				$response = $str;
			}
		}
		else if(($type=="textbox" || $type=="textarea") && $inputMore=="No" && $fileRequired=="No" && $uploadRequired=="No")
		{
				
			//if(count($salutation) > 0)
			//{
				if($getResponse[0]['salultation_value']!="")
				{
					$response['salutation'] = $getResponse[0]['salultation_value'];
				}
				else 
				{
					$response['salutation'] = "";
				}
			//}
			
			$response["value"] = $getResponse[0]['answer_value'];

		}
		else if(($type=="checkbox" || $type=="radio" || $type=="single_select" || $type=="file") && $inputMore=="No" && $fileRequired=="No" && $uploadRequired=="No")
		{
			$str = array();
			
			if($type=="file")
			{	
				if($getResponse[0]['answer_value']!="")
				{
					//$response['value'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
					$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
				}
				else 
				{
					//$response['value'] = "";
					$response['image_url'] = "";
				}				
			}
			else if($type=="radio" || $type=="single_select")
			{
				$str = @$getResponse[0]['answer_value'];

				$response["value"] = $str;
			}
			else if($type=="checkbox")
			{	
				//echo "RESULT<pre>";print_r($getResponse);
				if(count($getResponse) > 0)
				{	
					foreach($getResponse as $key => $values)
					{	//echo ">>>>".$values['answer_value'];
						$str[$values['key_name']] = $values['answer_value'];
					}
				}
				
				$response = $str;
			}
			//$response['value'] = $str;		
		}
		else if(($type=="checkbox" || $type=="radio" || $type=="single_select" || $type=="file") && $inputMore=="No" && $fileRequired=="Yes" && $uploadRequired=="No")
		{
			if($getResponse[0]['file_name']!="")
			{
				$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['file_name']);
			}
			else 
			{
				$response['image_url'] = "";
			}	
			
			$str = array();
			
			if($type=="file")
			{	
				if($getResponse[0]['answer_value']!="")
				{
					//$response['value'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
					$response['image_url'] = $this->convertImageToBase64encode($getResponse[0]['answer_value']);
				}
				else 
				{
					//$response['value'] = "";
					$response['image_url'] = "";
				}				
			}
			else if($type=="radio" || $type=="single_select")
			{
				$response["value"] = $getResponse[0]['answer_value'];
			}
			else if($type=="checkbox")
			{
				if(count($getResponse) > 0)
				{
					foreach($getResponse as $key => $values)
					{
						$str[$values['key_name']] = $values['answer_value'];
					}
				}
				
				$response = $str;
			}	
		}		
		return $response;
	}
	
	//Save Response For Static Survey
	public function save_response()
	{
		// Response
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		// Post DATA
		$surveyData = $_POST;
		//echo "<pre>";print_r($surveyData);die();
		if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			//$response['message']    = "User ID can not be blank.";
			$response['message']    = "वापरकर्ता आयडी रिक्त असू शकत नाही.";
			
		} 
		else 			
		{
			if(count($surveyData['data']) > 0)
			{
				//$survey_id 			= $surveyData['data']['responses'][0]['survey_id'];
				$survey_id 			= isset($surveyData['data']['responses'][0]['survey_id'])? $surveyData['data']['responses'][0]['survey_id']:"0";
				//$survey_id 			= isset($surveyData['data']['survey_id'])? $surveyData['data']['survey_id']:"0";	
				$response_id 		= $surveyData['data']['responses'][0]['response_id'];
				$ref_id 			= $surveyData['data']['responses'][0]['ref_id'];
				//$submitted_date 	= $surveyData['data']['responses'][0]['submitted_date'];
				$submitted_date 	= isset($surveyData['data']['responses'][0]['submitted_date'])? $surveyData['data']['responses'][0]['submitted_date']:"";
				$submited_time 		= $surveyData['data']['responses'][0]['submited_time'];
				$shg_id 			= @$surveyData['data']['responses'][0]['shg_id'];
				//$shg_id 			= @$surveyData['data']['shg_id'];
				$header_latitude 	= isset($surveyData['data']['responses'][0]['latitude'])? $surveyData['data']['responses'][0]['latitude']:"";		
				$header_longitude 	= isset($surveyData['data']['responses'][0]['longitude'])? $surveyData['data']['responses'][0]['longitude']:"";				
				$survey_status 		= $surveyData['data']['responses'][0]['status'];
				
				// New Values Added By Vicky 19th Aug 2021
				$crop_policy 			= isset($surveyData['data']['responses'][0]['crop_policy'])? $surveyData['data']['responses'][0]['crop_policy']:"No";	
				$crop_store_in_godown 	= isset($surveyData['data']['responses'][0]['crop_store_in_godown'])? $surveyData['data']['responses'][0]['crop_store_in_godown']:"No";
				
				
				if($crop_store_in_godown == 'Yes')
				{
					$godown_village_id 		= isset($surveyData['data']['responses'][0]['godown_village_id'])? $surveyData['data']['responses'][0]['godown_village_id']:"";
					$godown_own_by 			= isset($surveyData['data']['responses'][0]['godown_own_by'])? $surveyData['data']['responses'][0]['godown_own_by']:"";
					$select_item_from_godown= isset($surveyData['data']['responses'][0]['select_item_from_godown'])? $surveyData['data']['responses'][0]['select_item_from_godown']:"";
					$other_any 				= isset($surveyData['data']['responses'][0]['other_any'])? $surveyData['data']['responses'][0]['other_any']:"";
				}
				else 
				{
					$godown_village_id = "";
					$godown_own_by = "";
					$select_item_from_godown = "";
					$other_any = "";
				}
				
				if($submitted_date!="")
				{
					$submitDate 		= date('Y-m-d', strtotime($submitted_date));
				}
				else {
					$submitDate 		= date('Y-m-d');
				}
				//$submitDate 		= date('Y-m-d', strtotime($submitted_date));
				
				//echo "<pre>";print_r($surveyData['data']);die();
				
				$start_date_time    = date("Y-m-d H:i:s");
				$end_date_time      = date("Y-m-d H:i:s");	
				if($surveyData['data']['responses'][0]['start_date_time']!="")
				{
					$start_date_time	= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['start_date_time']));
				}
				
				if($surveyData['data']['responses'][0]['end_date_time']!="")
				{
					$end_date_time 		= date("Y-m-d H:i:s", strtotime($surveyData['data']['responses'][0]['end_date_time']));
				}
				
				$getShgDetails = $this->master_model->getRecords("mv_personal_details", array("shg_id" => $shg_id));
				if(count($getShgDetails) > 0)
				{
					$district_id = $getShgDetails[0]['district_id'];
					$taluka_id = $getShgDetails[0]['taluka_id'];
					$village_id = $getShgDetails[0]['village_id'];
				}
				else 
				{
					$district_id = 0;
					$taluka_id = 0;
					$village_id = 0;
				}
				
					$query = $this->db->query("SELECT * FROM mv_agro_response_headers WHERE response_id = '".$response_id."' AND is_deleted = '0'");
					$cnt = $query->num_rows();
					$ref_res = true;
					if($cnt == '0') 
					{		
						// Check Ref ID Exist related response id 
						$queryReference = $this->db->query("SELECT * FROM mv_agro_response_headers WHERE ref_id = '".$ref_id."' AND is_deleted = '0'");
						$cntRef = $queryReference->num_rows();
						
						if($cntRef == '0')
						{
							$insertArr = array( "ref_id" 			=> $ref_id, 
												"survey_id" 		=> $survey_id,
												"surveyor_id" 		=> $user_id,
												"shg_id" 			=> $shg_id,
												"district_id" 		=> $district_id,
												"taluka_id" 		=> $taluka_id,
												"village_id" 		=> $village_id,
												"submitted_date" 	=> $submitDate,
												"submited_time" 	=> $submited_time,
												"start_date_time" 	=> $start_date_time,
												"end_date_time" 	=> $end_date_time,
												"latitude" 			=> $header_latitude,
												"longitude" 		=> $header_longitude,
												"crop_policy" 		=> $crop_policy,
												"crop_store_in_godown" => $crop_store_in_godown,
												"godown_village_id" => $godown_village_id,
												"godown_own_by" 	=> $godown_own_by,
												"select_item_from_godown" 	=> $select_item_from_godown,
												"other_any" 		=> $other_any,
												"status" 			=> $survey_status,
												"created_by_id" 	=> $user_id,
												"created_on"		=> $start_date_time	
										);
							$surveyQuery = $this->master_model->insertRecord('mv_agro_response_headers',$insertArr);
							$cid = $this->db->insert_id();
							$updateFlag = false;
							//$ref_res = true;
						}
						else 
						{
							//echo "No Sections Found";
							$response['status'] 	= "0";
							//$response['message'] 	= "This survey form already exist.";
							$response['message'] 	= "हा सर्वेक्षण फॉर्म आधीपासून विद्यमान आहे.";
							$response['data']		= array("ref_id" => $ref_id);							
							$ref_res = false;
						}	
					}
					else 
					{
						// Update Record
						$updateArr = array( 
											"status" 		=> $survey_status,
											"updated_on" 	=> date("Y-m-d H:i:s"),
											"updated_by_id" => $user_id		
											);
						$updateQuery = $this->master_model->updateRecord('mv_agro_response_headers',$updateArr,array('survey_id' => $survey_id, 'surveyor_id' => $user_id, "response_id" => $response_id));					
						$cid = $response_id;
						$updateFlag = true;	
						
						$changeArr = array('is_deleted' => 1, 'deleted_by_id' => $user_id, 'deleted_on' => date("Y-m-d H:i:s"));
						$updateQuery_res = $this->master_model->updateRecord('mv_agro_response_details',$changeArr,array("response_id" => $response_id));					
					}
					
					// Actual Response Store Code Start If Ref Id Condition Is True
					if($ref_res)
					{
						if(count($surveyData['data']['responses'][0]['products']) > 0)
						{	
							
							//echo "<pre>";print_r($surveyData['data']['responses'][0]['products'][0]['current']);
							$current_count = count($surveyData['data']['responses'][0]['products'][0]['current']);
							$after_count = count($surveyData['data']['responses'][0]['products'][0]['after']);
							//echo "===".$current_count.">>>".$after_count;die();
							
							
							foreach($surveyData['data']['responses'][0]['products'][0]['current'] as $res_val => $return_res)
							{
								// Get Values From Response
								$product_category_id 	= (@$return_res['product_category_id'])? @$return_res['product_category_id']:"0";
								$product_id	 			= (@$return_res['product_id'])? @$return_res['product_id']:"0";
								$variety_id	 			= (@$return_res['varity_id'])? @$return_res['varity_id']:"0";
								$other_specify			= (@$return_res['other_specify'])? @$return_res['other_specify']:"";
								$product_timeline 		= (@$return_res['product_duration_for_mavim'])? @$return_res['product_duration_for_mavim']:"";
								$product_shelf_life		= (@$return_res['product_shelf_life'])? @$return_res['product_shelf_life']:"";
								$measurement_type 	 	= isset($return_res['measurement_type'])? $return_res['measurement_type']:"";
								$product_quantity   	= (@$return_res['product_quantity'])? @$return_res['product_quantity']:"";
								$available_land_size	= (@$return_res['remaining_land_area'])? @$return_res['remaining_land_area']:"";
								$latitude 	 			= isset($return_res['latitude'])? $return_res['latitude']:"";
								$longitude 	  			= isset($return_res['longitude'])? $return_res['longitude']:"";
								$season_name			= "";
								$after_other_specify	= "";
								$after_cutting_period	= "";
								$after_measurement_type	= "";
								$after_quantity			= "";
								$after_land_size		= "";
								if($updateFlag)
								{
									$detailsArr = array('response_id' 			=> $cid, 
														'product_category_id' 	=> $product_category_id, 
														'product_id'			=> $product_id,  
														'variety_id' 			=> $variety_id, 
														'other_specify' 		=> $other_specify, 
														'product_timeline' 		=> $product_timeline, 
														//'product_shelf_life' 	=> '1',
														'product_shelf_life' 	=> $product_shelf_life,
														'measurement_type' 		=> $measurement_type, 
														'product_quantity' 		=> $product_quantity, 
														//'available_land_size' 	=> $available_land_size,
														'season_name' 			=> $season_name,	
														'after_product_category_id' => '0',
														'after_product_id' 		=> '0',
														'after_variety_id' 		=> '0',
														'after_other_specify' 	=> $after_other_specify,
														'after_cutting_period' 	=> $after_cutting_period,
														'after_measurement_type'=> $after_measurement_type,
														'after_quantity' 		=> $after_quantity,
														'after_land_size' 		=> $after_land_size,
														'latitude' 				=> $latitude,
														'longitude' 			=> $longitude,
														'updated_on'			=> date('Y-m-d H:i:s'),
														'updated_by_id' 		=> $user_id);
									$surveyQuery = $this->master_model->insertRecord('mv_agro_response_details',$detailsArr);
												
								}								
								else 
								{
									$detailsArr = array('response_id' 			=> $cid, 
														'product_category_id' 	=> $product_category_id, 
														'product_id'			=> $product_id,  
														'variety_id' 			=> $variety_id, 
														'other_specify' 		=> $other_specify, 
														'product_timeline' 		=> $product_timeline, 
														//'product_shelf_life' 	=> '1',
														'product_shelf_life' 	=> $product_shelf_life,
														'measurement_type' 		=> $measurement_type, 
														'product_quantity' 		=> $product_quantity, 
														//'available_land_size' 	=> $available_land_size,
														'season_name' 			=> $season_name,	
														'after_product_category_id' => '0',
														'after_product_id' 		=> '0',
														'after_variety_id' 		=> '0',
														'after_other_specify' 	=> $after_other_specify,
														'after_cutting_period' 	=> $after_cutting_period,
														'after_measurement_type'=> $after_measurement_type,
														'after_quantity' 		=> $after_quantity,
														'after_land_size' 		=> $after_land_size,
														'latitude' 				=> $latitude,
														'longitude' 			=> $longitude,
														'updated_on'			=> date('Y-m-d H:i:s'),
														'updated_by_id' 		=> $user_id);
									$surveyQuery = $this->master_model->insertRecord('mv_agro_response_details',$detailsArr);										
								}
							} // Current Foreach End 
							
							foreach($surveyData['data']['responses'][0]['products'][0]['after'] as $val => $current)
							{
								// Get Values From Response
								$season_select 			= (@$current['season_select'])? @$current['season_select']:"0";
								$after_category_id		= (@$current['after_product_category_id'])? @$current['after_product_category_id']:"0";
								$after_product_id	 	= (@$current['after_product_id'])? @$current['after_product_id']:"0";
								$after_variety_id		= (@$current['after_variety_id'])? @$current['after_variety_id']:"";
								$after_other_specify 	= (@$current['after_other_specify'])? @$current['after_other_specify']:"";
								$after_cutting_period	= (@$current['after_cutting_period'])? @$current['after_cutting_period']:"";
								$after_measurement_type = isset($current['after_measurement_type'])? $current['after_measurement_type']:"";
								$after_quantity   		= (@$current['after_quantity'])? @$current['after_quantity']:"";
								$after_land_size		= (@$current['after_land_size'])? @$current['after_land_size']:"";
								$latitude 	 			= isset($current['latitude'])? $current['latitude']:"";
								$longitude 	  			= isset($current['longitude'])? $current['longitude']:"";
								
								$other_specify			= "";
								$product_timeline		= "";
								$product_shelf_life		= "";
								$measurement_type		= "";
								$product_quantity		= "";
								$available_land_size	= "";
								
								if($updateFlag)
								{
									$detailsArr = array('response_id' 			=> $cid, 
														'product_category_id' 	=> '0', 
														'product_id'			=> '0',  
														'variety_id' 			=> '0', 
														'other_specify' 		=> $other_specify, 
														'product_timeline' 		=> $product_timeline, 
														'product_shelf_life' 	=> $product_shelf_life,
														'measurement_type' 		=> $measurement_type, 
														'product_quantity' 		=> $product_quantity, 
														'available_land_size' 	=> $available_land_size, 
														'season_name' 			=> $season_select, 
														'after_product_category_id' => $after_category_id,
														'after_product_id' 		=> $after_product_id,
														'after_variety_id' 		=> $after_variety_id,
														'after_other_specify' 	=> $after_other_specify,
														'after_cutting_period' 	=> $after_cutting_period,
														'after_measurement_type'=> $after_measurement_type,
														'after_quantity' 		=> $after_quantity,
														'after_land_size' 		=> $after_land_size,
														'latitude' 				=> $latitude,
														'longitude' 			=> $longitude,
														'updated_on'			=> date('Y-m-d H:i:s'),
														'updated_by_id' 		=> $user_id);
									$surveyQuery = $this->master_model->insertRecord('mv_agro_response_details',$detailsArr);
												
								}								
								else 
								{
									$detailsArr = array('response_id' 			=> $cid, 
														'product_category_id' 	=> '0', 
														'product_id'			=> '0',  
														'variety_id' 			=> '0', 
														'other_specify' 		=> $other_specify, 
														'product_timeline' 		=> $product_timeline, 
														'product_shelf_life' 	=> $product_shelf_life,
														'measurement_type' 		=> $measurement_type, 
														'product_quantity' 		=> $product_quantity, 
														'available_land_size' 	=> $available_land_size, 
														'season_name' 			=> $season_select, 
														'after_product_category_id' => $after_category_id,
														'after_product_id' 		=> $after_product_id,
														'after_variety_id' 		=> $after_variety_id,
														'after_other_specify' 	=> $after_other_specify,
														'after_cutting_period' 	=> $after_cutting_period,
														'after_measurement_type'=> $after_measurement_type,
														'after_quantity' 		=> $after_quantity,
														'after_land_size' 		=> $after_land_size,
														'latitude' 				=> $latitude,
														'longitude' 			=> $longitude,
														'updated_on'			=> date('Y-m-d H:i:s'),
														'updated_by_id' 		=> $user_id);
									$surveyQuery = $this->master_model->insertRecord('mv_agro_response_details',$detailsArr);	
									//echo ">Insert>>";print_r($detailsArr);
									//echo "+++++++".$this->db->last_query(); 
								}
							} // Current Foreach End 
							
							//echo "===".$current_count.">>>".$after_count;die();
							if($current_count > 0 && $after_count > 0)
							{
								$response['status'] 	= "1";
								//$response['message'] 	= "Survey Response Saved Successfully.";
								$response['message'] 	= "सर्वेक्षण प्रतिसाद यशस्वीरित्या जतन केला.";
								$response['data']		= array("response_id" => $cid, "ref_id" =>$ref_id);
							}
							else if($current_count > 0 && $after_count == 0)
							{
								$response['status'] 	= "1";
								//$response['message'] 	= "Survey Response Saved Successfully.";
								$response['message'] 	= "सर्वेक्षण प्रतिसाद यशस्वीरित्या जतन केला.";
								$response['data']		= array("response_id" => $cid, "ref_id" =>$ref_id);
							}							
						}
						else
						{						
							$response['status'] 	= "0";
							//$response['message'] 	= "No Section Questions Found.";
							$response['message'] 	= "कोणतेही विभाग प्रश्न सापडले नाहीत";
						}
						
					} // Ref ID check End If
			}
			else
			{				
				$response['status'] 	= "0";
				//$response['message'] 	= "No Data Found.";
				$response['message'] 	= "माहिती आढळली नाही.";
			}	
			
		}
		
		// Return Response
		$this->response($response);
		
	}

	public function get_min_max()
    {
        $response = array("status" => 0, "message" => "");
        $shg_id    = isset($_POST['shg_id']) ? $_POST['shg_id'] : '';
        //$password     = isset($_POST['password']) ? $_POST['password'] : '';    
        if(empty($shg_id))
        {
            // request not be null -
            $response['status']     = "0";
            //$response['message']     = "Village Code or Password can not be blank.";
            $response['message']     = "कृपया शेतकरी ID प्रविष्ट करा.";
        }
        else
        {
            if(is_numeric($shg_id)){

                $get_shg_data = $this->master_model->getRecords("mv_personal_details", array("shg_id" => $shg_id));
                    
                if(count($get_shg_data) > 0)
                {
                    $response['status']     = "1";
                    $response['message']     = "Minimum & Maximum Values Received Successfully.";
                    $response['data'] = array("min_value" => 1, "max_value" => $get_shg_data[0]['land_in_irrigated']);
                }        
                
            }
            else{
                // request not be null -
                $response['status']     = "0";
                //$response['message']     = "Village Code or Password can not be blank.";
                $response['message']     = "कृपया शेतकरी ID संख्या प्रविष्ट करा.";
            }
        }

        // Return Response
        $this->response($response);
    }
	/*// Get Min/Max 
	public function get_min_max()
	{
		$response = array("status" => 0, "message" => "");
		
		if($this->config->item('minimum')!="" && $this->config->item('maximum')!="")
		{
			$response['status'] 	= "1";
			$response['message'] 	= "Minimum & Maximum Values Received Successfully.";
			$response['data'] = array("min_value" => $this->config->item('minimum'), "max_value" => $this->config->item('maximum'));
		}		
		
		// Return Response
		$this->response($response);
		
	}*/
	
	// Survey Statics Updated
	public function survey_statistics() 
	{
		$response = array("status" => 0, "message" => "");
		
		// get POST data -
		$survey_id		= isset($_POST['survey_id']) ? $_POST['survey_id'] : '';
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		if(!empty($user_id) && is_user_login($user_id))
        {		
			if(empty($survey_id)){
				
				// request not be null -
				$response['status'] 	= "0";
				$response['message'] 	= "Survey ID is required.";
				
			} else {

				
				// Draft Survey Count
				$rejectedSurvey = $this->db->query("SELECT count(response_id) AS DRAFT_SURVEY FROM mv_agro_response_headers WHERE survey_id='".$survey_id."' AND status='Rejected' AND is_deleted='0' AND surveyor_id = '".$user_id."'");
				$rejectData 	= $rejectedSurvey->row();
				$rejectCount = $rejectData->DRAFT_SURVEY;	
				
				// Approved Survey Count
				$approvedSurvey = $this->db->query("SELECT count(response_id) AS APPROVED_SURVEY FROM mv_agro_response_headers WHERE survey_id='".$survey_id."' AND (status='Level 1 Approved' OR status='Level 2 Approved') AND is_deleted='0' AND surveyor_id = '".$user_id."'");				
				$approvedData 	= $approvedSurvey->row();
				$approvedCount	= $approvedData->APPROVED_SURVEY;
				
				// Return Survey Count
				$returnSurvey = $this->db->query("SELECT count(response_id) AS RETURN_SURVEY FROM mv_agro_response_headers WHERE survey_id='".$survey_id."' AND status='Returned' AND is_deleted='0' AND surveyor_id = '".$user_id."'");				
				$returnData 	= $returnSurvey->row();
				$returnCount	= $returnData->RETURN_SURVEY;				
				
				// Publish Survey Count
				$publishSurvey = $this->db->query("SELECT count(response_id) AS TOTAL_SUBMITTED_SURVEY FROM mv_agro_response_headers WHERE survey_id='".$survey_id."' AND (status='Submitted' OR status='Re-Submitted') AND is_deleted='0' AND surveyor_id = '".$user_id."'");
				//$publishCount = $publishSurvey->num_rows();
				$publishData 	= $publishSurvey->row();
				$publishCount	= $publishData->TOTAL_SUBMITTED_SURVEY;
				
				$response['status'] 	= "1";
				$response['message'] 	= "Survey statistic get successfully.";
				//$response['data'] 		= array("Draft" => $draftCount, "Cancel" => $cancelCount, "Close" => $closeCount, "Total Submitted" => $publishCount);
				
				$response['data'] = array("rejected" => $rejectCount, "returned" => $returnCount, "submitted" => $publishCount);
				
			}
		}
		else
		{
			$response['status']		= "0";
			$response['message']    = "You must be logged in to perform this request.";
		}
		
		$this->response($response);
	}
	
	// Post Data Clean function
	public function sql_clean($con, $arr)
	{
		$raw_post = array();
		foreach ($arr as $key => $value)// loop out array
		{
			if(!is_array($value))
			{
				//$str_tmp = html_entity_decode($value);
				//$str_tmp = stripcslashes ($str_tmp);
				//$str_tmp = addslashes ($str_tmp);
				//$str_tmp = htmlentities($str_tmp);
				$raw_post[$key] = @mysqli_real_escape_string($con, $str_tmp);
				//$raw_post[$key] = strip_tags($str_tmp);
			}
			else
				$raw_post[$key] = $value;
		}
		return $raw_post;
	}
	
	// Encode Image with Base64 Encode Format
	public function convertImageToBase64encode($image_name)
	{
		
		$img = file_get_contents('uploads/'.$image_name);
		  
		// Encode the image string data into base64
		$data = base64_encode($img);
		  
		// Display the output
		return $data;
	}
	
	// function to user logout
	public function logout()
	{
		$response = array("status" => 0, "message" => "");
		
		// get logged in user id
		$user_id = getUserIdForSessionToken();
		
		/*if(empty($user_id)){
			
			// request not be null -
			$response['status']		= "0";
			$response['message']    = "All fields are mandatory.";
			
		} else {*/			
			if(!empty($user_id) && is_user_login($user_id))
            {
				$updated_date = date("Y-m-d H:i:s");

				$sql = "UPDATE mv_assign_login_details SET is_login = '0', updated_on = '".$updated_date."' WHERE aid = '".$user_id."'";
				if($result = $this->db->query($sql))
				{
					$response['status']		= "1";
					//$response['message'] 	= "User logout successfully.";
					$response['message'] 	= "वापरकर्ता लॉगआउट यशस्वीरित्या.";
				}
				else
				{
					$response['status'] 	= "1";
					//$response['message'] 	= "No data Found.";
					$response['message'] 	= "माहिती आढळली नाही.";
				}
			}
			else
			{
				$response['status']		= "0";
				//$response['message']    = "You must be logged in to perform this request.";
				$response['message']    = "ही विनंती करण्यासाठी तुम्ही लॉग इन केलेला असणे आवश्यक आहे.";
			}
		//}
		
		$this->response($response);
	}
	
	// Get Image Name
	public function generateImage($random_name, $image_name)
	{
		@define('UPLOAD_DIR', 'uploads/survey/');
		
		//Survey_id_response_id_question_id
		$imgName = $random_name.'.jpg';
		$file_name = UPLOAD_DIR.$imgName;
		
		if (base64_encode(base64_decode($image_name, true)) === $image_name) {
			$image_base64 = base64_decode($image_name);
			//$file_name = UPLOAD_DIR . 'new.jpg';
			file_put_contents($file_name, $image_base64);	
			//echo "++".$imgName;die();
			return $imgName;
		}
		else 
		{
			$imgName = '';
			return $imgName;
		}	
		
		/*$image_base64 = base64_decode($image_name);
		//$file_name = UPLOAD_DIR . 'new.jpg';
		file_put_contents($file_name, $image_base64);		
		return $imgName;		
		return 1;*/
	}
	
	// function to send JSON response -
	public function response($response)
	{
		// log api access -
		$req = array();
		$this->api_log($req, $response, TRUE, $this->log_id);
		
		@header('Content-type: application/json');
		echo json_encode($response);
		
		die();
	}
	
	// function to log api access
	public function api_log($req, $res, $is_update = FALSE, $log_id = NULL)
	{
		// get api user agent -
		$user_agent = @$_SERVER['HTTP_USER_AGENT'];
		
		$req = json_encode($req);
		$res = json_encode($res);
		
		$access_date = date("Y-m-d H:i:s");
		
		//$query = $this->db->query("INSERT INTO api_log(user_agent, req, res, access_date) VALUES('$user_agent', '$req', '$res', '$access_date')");
		
		$headers = "";
		foreach (@getallheaders() as $name => $value) { 
			//echo "$name: $value \n";
			$headers .= "$name: $value \n";
		}
		
		// $_SERVER
		$server = print_r($_SERVER, TRUE);
		
		/*$myfile = @fopen("logs_".date("dmY").".txt", "a") or die("Unable to open file!");
		$txt = "
		=================== ".$access_date." ====================
					
		# Headers:
		".$headers."
		
		# Server:
		".$server."
		
		# Request:
		".$req."
		
		# Response:
		".$res."
		";
		@fwrite($myfile, $txt);
		@fclose($myfile);*/
		
		// =====
		
		$request_headers = array();
		$request_headers["Headers"] = $headers;
		$request_headers["Server"] = $server;
		
		//$request_headers_json_str = json_encode($request_headers);
		$request_headers_str = print_r($request_headers, TRUE);
		
		$ip = @isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $this->input->ip_address();
		
		// If update
		if($is_update == TRUE)
		{
			$query = $this->db->query("UPDATE `mv_api_log` SET response = '$res', response_timestamp = '$access_date', edited_date = '$access_date' WHERE id = " . $log_id);
		}
		else
		{
			$query = $this->db->query("INSERT INTO `mv_api_log` (`request`, `request_timestamp`, `request_headers`, `ip`, `user_agent`, `added_date`) VALUES ('$req', '$access_date', '$request_headers_str', '$ip', '$user_agent', '$access_date')");
			
			$log_id = $this->db->insert_id();
		}
		
		return $log_id;
	}
	
	
} // Class End
